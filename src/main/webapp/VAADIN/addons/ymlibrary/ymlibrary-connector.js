/**
 * JavaScript connector for YandexMapComponent
 */

ru_telros_brigades_client_ui_maplayout_YandexMapComponent = function () {
    var t = this;
    var e = t.getElement(); // DIV
    var s = t.getState(); // state of component
    var o = s.options; // options
    var d = ymlibrary.debug; // debug mode flag
    var c; // component
    var component_id = s.uid; //

    /*** Split options into separate objects ***/
    // RPC options
    var
        rpco = {
            initializedRpc: s.initializedRpc,
            markerClickedRpc: s.markerClickedRpc,
            boundsChangedRpc: s.boundsChangedRpc
        };

    // Component options & initial state
    var co = {
        uid: s.uid, // [s.uid|ymlibrary.getUid()] string
        type: s.type, // string
        zoom: s.zoom, // int
        centerLatitude: s.centerLatitude, // double
        centerLongitude: s.centerLongitude, // double
        controls: s.controls, // array
        markers: s.markers, // markers
        polylines: s.polylines // polylines
    };

    /*** Debugging ***/
    if (d) {
        dumpState();
    }

    function dumpState() {
        console.log('Dumping shared state information....');

        console.log('YMLibrary: ');
        console.log('  apiInitialized: ' + ymlibrary.apiInitialized);
        console.log('  components.length: ' + ymlibrary.components.length);

        ymlibrary.dumpObject('State', s);
        ymlibrary.dumpObject('Options', o);
        ymlibrary.dumpObject('Component options', co);

        //console.log('Enabled RPC Calls:');
        //console.log('  Initialized: ' + s.initializedRpc);
        //console.log('  Clicked on Marker: ' + s.markerClickedRpc);
    }

    /*** Initializing ***/
    if (d) console.log('Creating component: ' + component_id);
    //var x = ymlibrary.findComponentById(co.uid);
    //if (typeof x == 'undefined' || !x) {
    c = new ymlibrary.component(t, e, o, co, rpco);
    ymlibrary.addComponent(c);
    //}
    //else {
    //    c = x;
    //    if (d) console.log('Component ' + x.getComponentId() + ' initialized already!');
    //}

    // Handle changes from the server-side
    t.onStateChange = function () {
        if (d) console.log('Received onChangeState');
    };

    /*** Update shared state of component ***/
    t.updateState = function () {
        if (d) console.log('Updating shared state');

        s.initialized = Boolean(c.getInitialized());
        s.type = String(c.getMapType());
        s.controls = c.getControls() || [];
        s.zoom = Number(c.getZoom());
        var center = c.getCenter();
        s.centerLatitude = Number(center[0]);
        s.centerLongitude = Number(center[1]);

        t.updateSharedState(
            s.initialized,
            s.type,
            s.controls,
            s.zoom,
            s.centerLatitude,
            s.centerLongitude
        );
    };

    /*** RPC calls from server ***/
    /* Set map center */
    t.setCenter = function (latitude, longitude) {
        if (d) console.log('Received setCenter(' + latitude + ',' + longitude + ') RPC call');
        c.setCenter(latitude, longitude)
    };

    t.panTo = function (latitude, longitude, delay) {
        if (d) console.log('Received panTo(' + latitude + ',' + longitude + ') RPC call');
        c.panTo(latitude, longitude, delay)
    };

    t.setMapType = function (maptype) {
        if (d) console.log('Received setMapType(' + maptype + ') RPC call');
        c.setMapType(maptype)
    };

    t.setControls = function (controls) {
        if (d) console.log('Received setControls(' + controls + ') RPC call');
        c.setControls(controls)
    };

    t.setZoom = function (zoom) {
        if (d) console.log('Received setZoom(' + zoom + ') RPC call');
        c.setZoom(zoom);
    };

    t.addMarkerFromJSON = function (str) {
        if (d) console.log('addMarkerFromJSON(): ' + str);
        c.addMarkerFromJSON(str);
    };

    t.addPolylineFromJSON = function (str) {
        if (d) console.log('addPolylineFromJSON(): ' + str);
        c.addPolylineFromJSON(str);
    };

    t.addPolyline = function (element) {
        if (d) ymlibrary.dumpObject('addPolyline(): ', element);
        c.addPolyline(element);
    };

    t.removeMarker = function (uid) {
        if (d) console.log('Remove Marker(): ' + uid);
        c.removeMarker(uid);
    };

    t.removePolyline = function (uid) {
        if (d) console.log('Remove Polyline(): ' + uid);
        c.removePolyline(uid);
    };

    t.removeAllMarkers = function () {
        if (d) console.log('Remove All Markers():');
        c.removeAllMarkers();
    };

    t.removeAllPolylines = function () {
        if (d) console.log('Remove All Polylines():');
        c.removeAllPolylines();
    };

    t.showBalloon = function (latitude, longitude, content) {
        if (d) console.log('Received showBalloon(): ' + content);
        c.showBalloon(latitude, longitude, content);
    };

    t.addRoute = function (routeArray) {
        if (d) console.log('Add Route(): ' + routeArray);
        c.addRoute(routeArray);
    };

    /* Extend ymlibrary.component class */
    ymlibrary.component.prototype.notifyInitialized = function () {
        t.updateState();
        t.notifyInitialized(); // Processed in Java
    };

    ymlibrary.component.prototype.markerClicked = function (uid) {
        t.updateState();
        t.markerClicked(uid); // Processed in Java
    };

    ymlibrary.component.prototype.boundsChanged = function () {
        t.updateState();
        t.boundsChanged(); // Process in Java
    }

};
