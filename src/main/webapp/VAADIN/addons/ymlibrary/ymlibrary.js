/**
 * Library for work with YandexMap API
 * @type {{}}
 */

// Include YandexMaps API Script in <HEAD> of HTML
// Parameters:
// - lang: ru_RU|en_US|ru_UA|uk_UA|tr_TR
// - apikey: API key (only for commercial versions)
// - coordorder: latlong|longlat

//var ymScript = document.createElement('script');
//ymScript.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
//ymScript.type = 'text/javascript';
//document.getElementsByTagName('head')[0].appendChild(ymScript);

var ymlibrary = ymlibrary || {};

ymlibrary.debug = true;
ymlibrary.components = []; // Array of components for initialize
ymlibrary.apiInitialized = false;
ymlibrary.uid = 0;

ymlibrary.getUid = function () {
    this.uid++;
    return 'ymmap_' + this.uid;
};

ymlibrary.addComponent = function (c) {
    if (typeof c === 'undefined' || !c)
        return;
    if (this.debug) {
        console.log("Execute addComponent: " + c.getComponentId());
    }
    if (!this.apiInitialized) {
        this.components.push(c);
    }
    else {
        c.initializeComponent();
    }
    return c;
};

ymlibrary.findComponentById = function (id) {
    var component;
    if (this.debug) {
        console.log("Execute findComponentById: id=" + id + ', length=' + this.components.length);
    }
    for (var i = 0; i < this.components.length; i++) {
        var x = this.components[i];
        if (typeof x === 'undefined' || !x)
            continue;
        console.log("Matching " + id + ' and' + x.getComponentId() + '...');
        if (x.getComponentId() == id) {
            component = x;
            break;
        }
    }
    return component;
};

ymlibrary.initAllComponents = function () {
    if (this.debug) {
        console.log("Execute initAllComponents: " + "components.length=" + this.components.length + ", " + "apiInitialized=" + this.apiInitialized);
    }
    this.apiInitialized = true;
    for (var i = 0; i < this.components.length; i++) {
        this.components[i].initializeComponent();
    }
    this.components = [];
};

ymlibrary.dumpObject = function (prompt, o) {
    if (!this.debug)
        return;
    var s = prompt + ': ';
    if (typeof o === 'undefined') {
        s = s + '[null]'
        console.log(prompt);
        return;
    }
    s = s + '[length=' + o.length + ']';
    console.log(prompt);
    var i = 0;
    for (var x in o) {
        i++;
        if (typeof x === 'undefined' || !x) {
            console.log('  ' + x + ': null');

        }
        else {
            console.log('  ' + x + ': ' + o[x]);
        }
    }
};

ymlibrary.component = function (parent, element, options, compOptions, rpcOptions) {

    var t = this, // Link to this
        d = ymlibrary.debug; // debug mode flag

    // Link to YandexMap
    var ymap;
    // Link to Clusterer
    var clusterer;
    // Link to Collection
    var markerCollection, polylineCollection;
    // Array of markers
    var markers = [];
    // Array of polylines
    var polylines = [];
    // Array of possible controls
    var ycontrols = ["mapTools", "miniMap", "scaleLine", "searchControl", "trafficControl", "typeSelector", "zoomControl", "smallZoomControl"];
    // State of component
    var state = {
        initialized: false,
        type: compOptions.type || 'yandex#map',
        zoom: compOptions.zoom || 12,
        centerLatitude: compOptions.centerLatitude || 55.7,
        centerLongitude: compOptions.centerLongitude || 37.6,
        controls: compOptions.controls || ycontrols,
        markers: compOptions.markers || [],
        polylines: compOptions.polylines || []
    };

    options.success = function (element, domObject) {
        /* Add RPC event listeners */
        if (rpcOptions.initializedRpc) {
            element.addEventListener('initialized', initialized, false);
        }
        if (rpcOptions.markerClickedRpc) {
            element.addEventListener('markerClicked', markerClicked, false);
        }
        if (rpcOptions.boundsChangedRpc) {
            element.addEventListener('boundsChanged', boundsChanged, false);
        }
    };

    /* Create the HTML element */
    function initializeDOM() {
        if (d) console.log('Creating an component');
        element.id = compOptions.uid;
    }

    /* Initialize conmponent */
    t.initializeComponent = function () {
        if (typeof ymap === 'undefined' || !ymap) {
            if (d) {
                console.log('Init an component: ');
                console.log('  element=' + element.id);
                console.log('  type=' + state.type);
                console.log('  controls=' + state.controls);
                console.log('  zoom=' + state.zoom);
                console.log('  center=(' + state.centerLatitude + ',' + state.centerLongitude + ')');
                console.log('  markers=(' + state.markers + ')');
                console.log('  polylines=(' + state.polylines + ')');
            }
            try {
                ymap = new ymaps.Map(element.id, {
                    zoom: Number(state.zoom),
                    center: [Number(state.centerLatitude), Number(state.centerLongitude)],
                    type: String(state.type)
                }, {
                    searchControlProvider: 'yandex#search',
                    autoFitToViewport: 'always'
                });
                clusterer = new ymaps.Clusterer({
                    clusterIconLayout: 'default#pieChart',
                    clusterDisableClickZoom: false,
                    clusterHideIconOnBalloonOpen: false
                });
                markerCollection = new ymaps.GeoObjectCollection();
                polylineCollection = new ymaps.GeoObjectCollection();
            }
            catch (e) {
                console.log('Exception: ' + e.name);
            }

            state.initialized = true;

            t.setControls(state.controls);

            for (var i = 0; i < state.markers.length; i++) {
                t.addMarker(state.markers[i]);
            }

            for (var i = 0; i < state.polylines.length; i++) {
                t.addPolyline(state.polylines[i]);
            }

            ymap.geoObjects.add(markerCollection);
            ymap.geoObjects.add(polylineCollection);
            ymap.geoObjects.add(clusterer);

            /*
             var ctrl = ymap.controls.get('zoomControl');
             if (!(typeof ctrl == 'undefined' || !ctrl)) {
             ymlibrary.dumpObject('ZoomControl: ', ctrl);
             ctrl.events.add(['optionschange', 'zoomchange', 'mapchange'], function (event) {
             if (d) {
             //console.log('YandexMap event: ' + event.get('type'));
             ymlibrary.dumpObject('ZoomControl event: ', event);
             }
             });
             }

             var ctrl = ymap.controls.get('geolocationControl');
             if (!(typeof ctrl == 'undefined' || !ctrl)) {
             ymlibrary.dumpObject('GeolocationControl: ', ctrl);
             ctrl.events.add(['locationchange', 'click', 'optionschange'], function (event) {
             if (d) {
             //console.log('YandexMap event: ' + event.get('type'));
             ymlibrary.dumpObject('GeolocationControl event: ', event);
             }
             });
             }
             */

            ymap.events.add([/*'click',*/ 'boundschange'], function (event) {
                if (d) {
                    console.log('YandexMap event: ' + event.get('type'));
                }
                doBoundsChange();
                //ymap.balloon.open(event.get('coords'), {
                //    contentHeader: 'Мышка',
                //    contentBody: 'Щелк!'
                //});
            });

            /*
             ymap.events.add('dblclick', function (event) {
             if (d) {
             console.log('YandexMap event: ' + event.get('type'));
             }
             });
             ymap.events.add(['wheel', 'typechange', 'sizechange', 'multitouchstart', 'multitouchmove', 'multitouchend', 'mousedown', 'mouseup'], function (event) {
             if (d) {
             console.log('YandexMap event: ' + event.get('type'));
             }
             });
             ymap.events.add('optionschange', function (event) {
             if (d) {
             //console.log('YandexMap event: ' + event.get('type'));
             ymlibrary.dumpObject('YandexMap event: ', event)
             }
             });
             ymap.events.add('zoomchange', function (event) {
             if (d) {
             //console.log('YandexMap event: ' + event.get('type'));
             ymlibrary.dumpObject('YandexMap event: ', event)
             }
             });
             ymap.zoomRange.events.add('zoomchange', function (event) {
             if (d) {
             //console.log('YandexMap event: ' + event.get('type'));
             ymlibrary.dumpObject('YandexMap event: ', event)
             }
             });
             */

            doInitialized();
        }
    };

    // Create element and style it
    initializeDOM();

    // Return element id
    t.getComponentId = function () {
        return element.id;
    };

    // Return state
    t.getState = function () {
        return state;
    };

    // Return link to map
    t.getMap = function () {
        return ymap;
    };

    // Check component initialized
    t.getInitialized = function () {
        return (typeof ymap !== 'undefined');
    };

    // Set type of map (map,hybrid,etc.)
    t.setMapType = function (maptype) {
        state.type = maptype;
        if (t.getInitialized())
            ymap.setType(maptype);
    };

    // Get type of map
    t.getMapType = function (maptype) {
        if (t.getInitialized()) return ymap.getType();
        return state.type;
    };

    // Get center coordinates of map
    t.getCenter = function () {
        if (t.getInitialized()) return ymap.getCenter();
        return [state.centerLatitude, state.centerLongitude];
    };

    t.getCenterLatitude = function () {
        var center = t.getCenter();
        return c[0];
    };

    t.getCenterLatitude = function () {
        var center = t.getCenter();
        return c[1];
    };

    // Set center coordinates of map
    t.setCenter = function (latitude, longitude) {
        state.centerLatitude = latitude;
        state.centerLongitude = longitude;
        if (t.getInitialized())
            ymap.setCenter([latitude, longitude]);
    };

    // Pan map to coordinates
    t.panTo = function (latitude, longitude, delay) {
        state.centerLatitude = latitude;
        state.centerLongitude = longitude;
        if (t.getInitialized())
            ymap.panTo([latitude, longitude], {delay: Number(delay)})
    };

    // Get zoom factor
    t.getZoom = function () {
        if (t.getInitialized()) return ymap.getZoom();
        return state.zoom;
    };

    // Set zoom factor of map
    t.setZoom = function (zoom) {
        state.zoom = zoom;
        if (t.getInitialized())
            ymap.setZoom(zoom);
    };

    // Set or remove controls of map
    t.setControls = function (controls) {
        state.controls = controls;
        if (t.getInitialized())
            for (var c in controls) {
                if (ycontrols.indexOf(c) >= 0) {
                    ymap.controls.add(c);
                }
                else {
                    ymap.controls.remove(c);
                }
            }
    };

    // Get array of controls
    t.getControls = function () {
        if (t.getInitialized()) {
            var controls = [];
            ymap.controls.each(function (c) {
                controls.push(c.options.getName());
            });
            return controls;
        }
        else {
            return state.controls;
        }
    };

    // Search element by uid
    t.findElementByUid = function (elements, uid) {
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].uid === uid)
                return i;
        }
        return -1;
    };

    t.isAssigned = function (v) {
        return !(typeof(v) === 'unassigned' || v == null || v === '');
    };

    // Add marker from JSON string
    t.addMarkerFromJSON = function (str) {
        var element = JSON.parse(str);
        t.addMarker(element);
    };

    // Add marker
    t.addMarker = function (element) {
        var i = -1;

        if (typeof element.uid !== 'unassigned') {
            i = t.findElementByUid(markers, String(element.uid));
        }
        if (t.getInitialized()) {
            if (i >= 0 && typeof markers[i].object$ === ymaps.GeoObject.type) {
                //ymap.geoObjects.remove(markers[i].object$);
                markerCollection.remove(markers[i].object$);
                markers.splice(i, 1);
            }

            // Координаты [+] 17/06/2016
            var e_coord = element.coordinates;

            // Свойства метки
            var e_prop = {
                // UID маркера
                placeId: String(element.uid)
            };
            if (t.isAssigned(element.caption)) {
                // Содержание балуна. Можно использовать html
                //e_prop.balloonContentHeader = String(element.caption);
                // Содержимое иконки. Текст выводится над иконкой маркера
                e_prop.iconContent = String(element.caption);
            }
            if (t.isAssigned(element.description)) {
                // Содержание балуна. Можно использовать html
                //e_prop.balloonContentBody = String(element.description);
                // Содержание хинта. Можно использовать html
                e_prop.hintContent = String(element.description);
            }

            // Опции
            var e_opt = {
                // включена ли кнопка закрытия балуна
                // balloonCloseButton: true,
                // включено ли открытия и закрытие балуна кликом на иконку маркера
                // hideIconOnBalloonOpen: false
                hintZIndex: 5,
                hintPane: 'hint',
                hintFitPane: true
            };
            if (t.isAssigned(element.uri)) {
                // Своё изображение иконки метки
                e_opt.iconLayout = 'default#image';
                e_opt.iconImageHref = String(element.uri);
            }
            if (t.isAssigned(element.imageSizeX) && t.isAssigned(element.imageSizeY)) {
                // Размеры метки
                e_opt.iconImageSize = [Number(element.imageSizeX), Number(element.imageSizeY)];
            }
            if (t.isAssigned(element.imageOffsetX) && t.isAssigned(element.imageOffsetY)) {
                // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки)
                e_opt.iconImageOffset = [Number(element.imageOffsetX), Number(element.imageOffsetY)];
            }
            if (t.isAssigned(element.iconPreset)) {
                // Тип иконки
                e_opt.preset = element.iconPreset;
            }
            if (t.isAssigned(element.iconColor)) {
                // Цвет иконки
                e_opt.iconColor = element.iconColor;
            }
            if (t.isAssigned(element.draggable)) {
                // Возможность перемещения иконки
                e_opt.draggable = Boolean(element.draggable);
            }
            else {
                e_opt.draggable = false;
            }

            var e = new ymaps.Placemark([Number(e_coord.lat), Number(e_coord.lon)], e_prop, e_opt);
            markerCollection.add(e);
            //ymap.geoObjects.add(e);
            //clusterer.add(e);

            e.events.add(['click'], function (event) {
                console.log('Placemark event: ' + event.get('type'));
                //ymlibrary.dumpObject('Placemark event: ', event)
                var object$ = event.get('target');
                var uid$ = object$.properties.get('placeId');
                if (typeof uid$ !== 'unassigned') {
                    doMarkerClicked(uid$);
                }
                event.stopPropagation();
            });

            element.object$ = e;

            markers.push(element);
        }
        else {
            if (i >= 0) {
                markers.splice(i, 1);
                markers.push(element);
            }
        }
    };

    // Add polyline from JSON
    t.addPolylineFromJSON = function (str) {
        var element = JSON.parse(str);
        t.addPolyline(element);
    };

    // Add polyline
    t.addPolyline = function (element) {
        var i = -1;

        if (typeof element.uid !== 'unassigned') {
            i = t.findElementByUid(polylines, String(element.uid));
        }
        if (t.getInitialized()) {
            if (i >= 0 && typeof polylines[i].object$ === ymaps.GeoObject.type) {
                //ymap.geoObjects.remove(polylines[i].object$);
                polylineCollection.remove(polylines[i].object$);
                polylines.splice(i, 1);
            }

            var e_coord = [];
            for (var i = 0; i < element.points.length; i++) {
                p = element.points[i];
                e_coord.push([Number(p.lat), Number(p.lon)]);
            }

            // Геометрия (geometry)
            //var e_geometry = {
            //    // Тип фигуры
            //    type: "Polygon",
            //    // Координаты точек
            //    coordinates: e_coord
            //};

            // Свойства (properties)
            var e_prop = {
                // UID маркера
                placeId: String(element.uid)
            };
            if (t.isAssigned(element.description)) {
                // Содержание балуна. Можно использовать html
                //e_prop.balloonContent = String(element.description);
                // Содержание хинта. Можно использовать html
                e_prop.hintContent = String(element.description);
            }

            // Опции (options)
            var e_opt = {};
            if (t.isAssigned(element.draggable)) {
                // Возможность перемещения
                e_opt.draggable = Boolean(element.draggable);
            }
            else {
                e_opt.draggable = false;
            }
            if (t.isAssigned(element.strokeColor)) {
                // Цвет границы
                e_opt.strokeColor = element.strokeColor;
            }
            if (t.isAssigned(element.strokeWidth)) {
                // Ширина границы
                e_opt.strokeWidth = element.strokeWidth;
            }
            if (t.isAssigned(element.strokeStyle)) {
                // Стиль границы
                e_opt.strokeStyle = element.strokeStyle;
            }
            if (t.isAssigned(element.strokeOpacity)) {
                // Прозрачность
                e_opt.opacity = element.strokeOpacity;
            }

            var e = new ymaps.Polyline(e_coord, e_prop, e_opt);
            polylineCollection.add(e);
            //ymap.geoObjects.add(e);

            element.object$ = e;

            polylines.push(element);
        }
        else {
            if (i >= 0) {
                polylines.splice(i, 1);
                polylines.push(element);
            }
        }
    };

    t.removeMarker = function (uid) {
        if (d) console.log('Remove Marker(): ' + uid);
        if (t.getInitialized()) {
            var i = t.findElementByUid(markers, String(uid));
            markerCollection.remove(markers[i].object$);
            clusterer.remove(markers[i].object$);
            markers.splice(i, 1);
        }
    };

    t.removePolyline = function (uid) {
        if (d) console.log('Remove Polyline(): ' + uid);
        if (t.getInitialized()) {
            var i = t.findElementByUid(polylines, String(uid));
            polylineCollection.remove(polylines[i].object$);
            polylines.splice(i, 1);
        }
    };

    // Remove All Markers
    t.removeAllMarkers = function () {
        if (t.getInitialized()) {
            console.log('State markers: ' + state.markers.length + ', markers: ' + markers.length);
            markerCollection.removeAll();
            clusterer.removeAll();
            markers.splice(0, markers.length);
            console.log('State markers: ' + state.markers.length + ', markers: ' + markers.length);
            /*for (var i = 0; i < markers.length; i++) {
                console.log('Remove Marker ' + i + ':' + markers[i].uid);
                ymap.geoObjects.remove(markers[i].object$);
                markers.splice(i, 1);
            }*/
        }
    };

    // Remove All Polylines
    t.removeAllPolylines = function () {
        if (t.getInitialized()) {
            console.log('State polylines: ' + state.polylines.length + ', polylines: ' + polylines.length);
            polylineCollection.removeAll();
            polylines.splice(0, polylines.length);
            console.log('State polylines: ' + state.polylines.length + ', polylines: ' + polylines.length);
            /*for (var i = 0; i < polylines.length; i++) {
                console.log('Remove Polyline ' + i + ':' + polylines[i].uid);
                ymap.geoObjects.remove(polylines[i].object$);
                polylines.splice(i, 1);
            }*/
        }
    };

    // Show Balloon
    t.showBalloon = function (latitude, longitude, content) {
        if (t.getInitialized()) {
            var e_coord = new Array(Number(latitude), Number(longitude));
            //ymap.setCenter(e_coord);
            var e_opt = {
                // Сдвигать карту, чтобы отобразить открывшийся балун.
                autoPan: true,
                autoPanDuration: 400,
                // Опция: показываем кнопку закрытия.
                closeButton: true
            };
            ymap.balloon.open(e_coord, String(content), e_opt)
        }
    };

    t.addRoute = function (routeArray) {
        if (d) console.log('Add Route(): ' + routeArray);
        for (var i = 0; i < routeArray.length; i++) {
            for (var j = 0; j < routeArray[i].length; j++) {
                console.log('Argument [' + i + ', ' + j + ']: ' + routeArray[i][j]);
            }
        }
        if (t.getInitialized()) {
            ymaps.route(routeArray, {
                mapStateAutoApply: true
            }).then(function (route) {
                route.getPaths().options.set({
                    // в балуне выводим только информацию о времени движения с учетом пробок
                    balloonContentBodyLayout: ymaps.templateLayoutFactory.createClass('$[properties.humanJamsTime]'),
                    // можно выставить настройки графики маршруту
                    strokeColor: '0000ffff',
                    opacity: 0.9
                });
                route.getWayPoints().options.set({
                    preset: 'islands#circleDotIcon'
                });
                route.getWayPoints().options.set({
                    preset: 'islands#circleDotIcon'
                });
                // добавляем маршрут на карту
                ymap.geoObjects.add(route);
            });
        }
    };

    /* RPC calls to server (client-side events) */
    function doInitialized() {
        if (d) console.log('Client notification: initialized');
        t.notifyInitialized();
    }

    function doMarkerClicked(uid) {
        if (d) console.log('Client notification: marker clicked');
        t.markerClicked(uid);
    }

    function doBoundsChange() {
        if (d) console.log('Client notification: bounds change');
        t.boundsChanged();
    }

}
;

ymaps.ready(function () {
    ymlibrary.initAllComponents();
});

/*
 ymaps.Map.prototype.setZoom(z, options)
 {
 if (ymlibrary.debug)
 console.log('YandexMap setZoom: ' + z);
 setZoom(z, options);
 }

 */