function playVideo(videoId, source) {
    //'https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8'
    var video = document.getElementById(videoId);
    console.log('Video ID ' + videoId);
    if (Hls.isSupported()) {
        console.log('HLS is supported');
        var hls = new Hls();
        hls.loadSource(source);
        console.log('Load source ' + source);
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, function () {
            console.log('Playing video');
            video.play();
        });
    } else if (video.canPlayType('application/vnd.apple.mpegurl')) {
        console.log('HLS is NOT supported');
        video.src = source;
        console.log('Source ' + source);
        video.addEventListener('loadedmetadata', function () {
            console.log('Playing video');
            video.play();
        });
    }
}

function stopVideo(videoId) {
    //'https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8'
    var video = document.getElementById(videoId);
    console.log('Video ID ' + videoId);
    video.src = null;
    console.log('Video source is null');
}