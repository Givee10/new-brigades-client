//package ru.telros.brigades.client.excelparsers;
//
//import jxl.Range;
//import jxl.Sheet;
//import jxl.Workbook;
//import jxl.format.Border;
//import jxl.format.Colour;
//import jxl.read.biff.BiffException;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
//public class ZayavkaNaTransport {
//    private int sheetColumnsAmount;
//    private int sheetRowsAmount;
//    private int tableStart;
//    private int tableEnd;
//    Workbook workbook;
//    Sheet sheet;
//    Map<Integer,String> TUVRows;
//    public ZayavkaNaTransport() {
//        File file = new File("zayavka.xls");
//        Workbook workbook = null;
//        try {
//            // column row
//            workbook = Workbook.getWorkbook(file);
//
//            sheet = workbook.getSheet(0);
//            sheetColumnsAmount = sheet.getSettings().getPrintArea().getBottomRight().getColumn();
//            sheetRowsAmount = sheet.getSettings().getPrintArea().getBottomRight().getRow();
//            tableStart = getTableHeaderRow();
//            tableEnd = getTableFooterRow();
//            TUVRows = getTUVRows();
//            List<Integer> excludedLines = new ArrayList<>(TUVRows.keySet());
//            Map<Integer, Request> requestMap = new HashMap<>();
//            fillMainColumns(requestMap,excludedLines);
//            fillMergedColumns(requestMap,excludedLines);
//            fillTUV(TUVRows,requestMap);
//
//
//
//            File file1  = new File("data.html");
//            StringBuilder sb = new StringBuilder("<!doctype html>\n" +
//                    "\n" +
//                    "<html lang=\"en\">\n" +
//                    "<head>\n" +
//                    "  <meta charset=\"utf-8\">\n" +
//                    "\n" +
//                    "  <title>The HTML5 Herald</title> \n" +
//                    " \n" +
//                    "\n" +
//                    "</head>\n" +
//                    "\n" +
//                    "<body> \n" +
//                    "<table border=1px solid black>\n" +
//                    "\t<tr>\n" +
//                    "\t\t<td># колонки</td>\n" +
//                    "\t\t<td>Камера №</td>\n" +
//                    "\t\t<td>Вид   работ</td>\n" +
//                    "\t\t<td>Адрес  работ</td>\n" +
//                    "\t\t<td>Тип   техники</td>\n" +
//                    "\t\t<td>Гос. №      а/м</td>\n" +
//                    "\t\t<td>Адрес  подачи</td>\n" +
//                    "\t\t<td>комментарий</td>\n" +
//                    "\t\t<td>Время подачи</td>\n" +
//                    "\t\t<td>примечание</td>\n" +
//                    "\t\t<td>Ответств.</td>\n" +
//                    "\t\t<td>ТУВ</td>\n" +
//                    "\t\t\t\t\t \t\t\t \t\t\n" +
//                    "\n" +
//                    "\t</tr>");
//                    List<Integer> rows = requestMap.keySet().stream().sorted().collect(Collectors.toList());
//                    rows.stream().forEach(e->{sb.append(requestMap.get(e).toTableRow());});
//                    sb.append("</table>\n" +
//                            "</body>\n" +
//                            "</html>");
//
//            BufferedWriter writer = new BufferedWriter(new FileWriter(file1));
//            writer.write(sb.toString());
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (BiffException e) {
//            e.printStackTrace();
//        } finally {
//            if (workbook != null) {
//                workbook.close();
//            }
//
//        }
//
//    }
//    private int getTableHeaderRow(){
//        for (int i = 0; i < sheetRowsAmount; i++) {
//            if (sheet.getCell(0,i).getContents().contains("Камера")){
//
//                return i;
//            }
//        }
//        return -1;
//    }
//    private int getTableFooterRow(){
//        for (int i = 0; i < sheetRowsAmount; i++) {
//            if (
//                    (sheet.getCell(0, i).getCellFormat().getBorder(Border.LEFT).getDescription().equals("medium")) &&
//                    (sheet.getCell(0, i).getCellFormat().getBorder(Border.BOTTOM).getDescription().equals("medium")) &&
//                    (sheet.getCell(0, i+1).getCellFormat().getBorder(Border.TOP).getDescription().equals("none")) &&
//                    (sheet.getCell(0, i+1).getCellFormat().getBorder(Border.RIGHT).getDescription().equals("none"))
//            ) {
//                return i;
//            }
//        }
//        return -1;
//    }
//    private void fillMergedColumns(Map<Integer, Request> requestMap, List<Integer> excludedLines) {
//        Range[] mergedCells = sheet.getMergedCells();
//        for (int i = 0; i < mergedCells.length; i++) {
//
//            int from = mergedCells[i].getTopLeft().getRow();
//            int to = mergedCells[i].getBottomRight().getRow();
//            if (from >= tableStart && to <= tableEnd && from != to && !excludedLines.contains(from)) {
//
//                for (int j = from; j <= to; j++) {
//                    if (requestMap.get(j) != null) {
//                        if (mergedCells[i].getTopLeft().getColumn() == 1) {
//                            requestMap.get(j).setWorkType(sheet.getCell(1, from).getContents());
//                        } else if (mergedCells[i].getTopLeft().getColumn() == 2) {
//                            requestMap.get(j).setWorkAddress(sheet.getCell(2, from).getContents());
//                        } else if (mergedCells[i].getTopLeft().getColumn() == 9) {
//                            requestMap.get(j).setResponsible(sheet.getCell(9, from).getContents());
//                        }
//                    }
//                }
//            }
//        }
//    }
//    private void fillTUV(Map<Integer,String> TUVRows, Map<Integer, Request> requestMap){
//        List<Integer> sortedTUVRows = TUVRows.keySet().stream().sorted().collect(Collectors.toList());
////      sortedTUVRows = [4, 24, 32, 41, 65, 79, 92, 105, 118]
//        TUVRows.entrySet().stream().forEach(e->{
//        });
//        requestMap.entrySet().stream().forEach(e->{
//            for (int i = sortedTUVRows.size()-1; i >= 0 ; i--) {
//                if (e.getKey()>sortedTUVRows.get(i)){
//                    e.getValue().setTUV(TUVRows.get(sortedTUVRows.get(i)));
//                    break;
//                }
//            }
//
//        });
//
//
//    }
//
//    private void fillMainColumns(Map<Integer, Request> requestMap, List<Integer> excludedLines){
//        for (int row = tableStart+1; row <= tableEnd; row++) {
//            if (!excludedLines.contains(row)){
//                String text = sheet.getCell(3,row).getContents();
//                if (text.length()>0){
//                    if (requestMap.get(row)==null){
//                        requestMap.put(row,new Request());
//                    }
//
//                    requestMap.get(row).setTableRow((row+1)+"");
//                    requestMap.get(row).setTechType(text);
//                    requestMap.get(row).setCamera(sheet.getCell(0,row).getContents());
//                    requestMap.get(row).setWorkType(sheet.getCell(1,row).getContents());
//                    requestMap.get(row).setWorkAddress(sheet.getCell(2,row).getContents());
//                    requestMap.get(row).setGosNumber(sheet.getCell(4,row).getContents());
//                    requestMap.get(row).setFeedAddress(sheet.getCell(5,row).getContents());
//                    requestMap.get(row).setComment(sheet.getCell(6,row).getContents());
//                    requestMap.get(row).setFeedTime(sheet.getCell(7,row).getContents());
//                    requestMap.get(row).setNote(sheet.getCell(8,row).getContents());
//                    requestMap.get(row).setResponsible(sheet.getCell(9,row).getContents());
//                }
//            }
//        }
//    }
//
//    private Map<Integer,String> getTUVRows(){
//        Map<Integer,String> rowToTitle = new HashMap<>();
//        for (int row = 0; row <= sheetRowsAmount; row++) {
//            int counter = sheetColumnsAmount+1;
//            Colour colour = sheet.getCell(0,row).getCellFormat().getBackgroundColour();
//            StringBuilder sb = new StringBuilder();
//            for (int column = 0; column <= sheetColumnsAmount; column++) {
//                if (
//                    (sheet.getCell(column,row).getCellFormat().getBackgroundColour().getDescription().equals("default background")) ||
//                    (sheet.getCell(column,row).getCellFormat().getBackgroundColour().getDescription().equals("white")) ||
//                    (!sheet.getCell(column,row).getCellFormat().getBackgroundColour().equals(colour))){
//                    break;
//                }
//                sb.append(sheet.getCell(column,row).getContents());
//                counter--;
//            }
//            if (counter==0){
//                rowToTitle.put(row,sb.toString().replaceAll("\\s{2,}"," "));
//            }
//        }
//        return rowToTitle;
//    }
//
//
//    private class Request {
//        public String getTableRow() {
//            return tableRow;
//        }
//
//        public void setTableRow(String tableRow) {
//            this.tableRow = tableRow;
//        }
//
//        //Камера
//        public String tableRow;
//        public String camera;
//
//
//        //вид работ
//        public String workType;
//        //адрес работ
//        public String workAddress;
//        //тип техники
//        public String techType;
//        //гос. номер
//        public String gosNumber;
//        //адрес подачи
//        public String feedAddress;
//        //комментарий
//        public String comment;
//        //время подачи
//        public String feedTime;
//        //примечание
//        public String note;
//        //ответственный
//        public String responsible;
//        public String TUV;
//        public Request( ) {
//
//        }
//        private String toTableRow(){
//            StringBuilder sb = new StringBuilder();
//            sb.
//                    append("<tr>")
//                    .append("<td>").append(this.getTableRow()).append("</td>")
//                    .append("<td>").append(this.getCamera()).append("</td>")
//                    .append("<td>").append(this.getWorkType()).append("</td>")
//                    .append("<td>").append(this.workAddress).append("</td>")
//                    .append("<td>").append(this.techType).append("</td>")
//                    .append("<td>").append(this.gosNumber).append("</td>")
//                    .append("<td>").append(this.feedAddress).append("</td>")
//                    .append("<td>").append(this.comment).append("</td>")
//                    .append("<td>").append(this.feedTime).append("</td>")
//                    .append("<td>").append(this.note).append("</td>")
//                    .append("<td>").append(this.responsible).append("</td>")
//                    .append("<td>").append(this.TUV).append("</td>")
//                    .append("/<tr>");
//            return sb.toString();
//        }
//
//        public String getCamera() {
//            return camera;
//        }
//
//        public String getTUV() {
//            return TUV;
//        }
//
//        public void setTUV(String TUV) {
//            this.TUV = TUV;
//        }
//
//        public void setCamera(String camera) {
//            this.camera = camera;
//        }
//
//
//        public String getWorkType() {
//            return workType;
//        }
//
//        public void setWorkType(String workType) {
//            this.workType = workType;
//        }
//
//        public String getWorkAddress() {
//            return workAddress;
//        }
//
//        public void setWorkAddress(String workAddress) {
//            this.workAddress = workAddress;
//        }
//
//        public String getTechType() {
//            return techType;
//        }
//
//        public void setTechType(String techType) {
//            this.techType = techType;
//        }
//
//        public String getGosNumber() {
//            return gosNumber;
//        }
//
//        public void setGosNumber(String gosNumber) {
//            this.gosNumber = gosNumber;
//        }
//
//        public String getFeedAddress() {
//            return feedAddress;
//        }
//
//        public void setFeedAddress(String feedAddress) {
//            this.feedAddress = feedAddress;
//        }
//
//        public String getComment() {
//            return comment;
//        }
//
//        public void setComment(String comment) {
//            this.comment = comment;
//        }
//
//        public String getFeedTime() {
//            return feedTime;
//        }
//
//        public void setFeedTime(String feedTime) {
//            this.feedTime = feedTime;
//        }
//
//        public String getNote() {
//            return note;
//        }
//
//        public void setNote(String note) {
//            this.note = note;
//        }
//
//        public String getResponsible() {
//            return responsible;
//        }
//
//        public void setResponsible(String responsible) {
//            this.responsible = responsible;
//        }
//    }
//}
