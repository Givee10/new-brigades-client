package ru.telros.brigades.client.ui.component;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.time.ZonedDateTime;

/**
 * Класс для отображения свойств и их значений
 */
public class PropertyLabel extends Label {
	private final static String NULL_REPRESENTATION = "";
	private final static int MAX_WIDTH = 80;
	private final String name;
	private String valueStyleName;
	private String nullRepresentation = NULL_REPRESENTATION;
	private int maxWidth = MAX_WIDTH;

	public PropertyLabel(String name) {
		this(name, null, NULL_REPRESENTATION);
	}

	public PropertyLabel(String name, Object value) {
		this(name, value, NULL_REPRESENTATION);
	}

	public PropertyLabel(String name, Object value, String nullRepresentation) {
		this.name = name;
		this.nullRepresentation = nullRepresentation;
		setContentMode(ContentMode.HTML);
		setValueObject(value);
	}

	public void setValueObject(Object value) {
		String v = nullRepresentation;
		if (value != null) {
			if (value.getClass().isAssignableFrom(ZonedDateTime.class)) {
				v = DatesUtil.formatZoned((ZonedDateTime) value);
			} else {
				v = String.valueOf(value);
			}
		}
		super.setValue(getNamePart().concat(": ").concat(getValuePart(v)));
	}

	private String getNamePart() {
		return ("<B>").concat(name).concat("</B>");
	}

	private String getValuePart(String value) {
		StringBuilder s = new StringBuilder();
		if (valueStyleName != null) {
			s.append("<SPAN class=\"").append(valueStyleName).append("\">");
		} else {
			s.append("<SPAN>");
		}
		s.append(StringUtil.abbreviate(value, this.maxWidth));
		s.append("</SPAN>");
		return s.toString();
	}

	public String getName() {
		return name;
	}

	public String getNullRepresentation() {
		return nullRepresentation;
	}

	public int getMaxWidth() {
		return maxWidth;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public String getValueStyleName() {
		return valueStyleName;
	}

	public void setValueStyleName(String valueStyleName) {
		this.valueStyleName = valueStyleName;
	}
}
