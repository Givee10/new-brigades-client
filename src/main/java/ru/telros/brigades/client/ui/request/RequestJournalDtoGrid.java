package ru.telros.brigades.client.ui.request;

import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.RequestJournalDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.ArrayList;
import java.util.List;

public class RequestJournalDtoGrid extends BrigadesGrid<RequestJournalDto> {
	public static RequestJournalDtoGrid currentlyOpenedGrid;
	public RequestJournalDtoGrid() {
		this(new ArrayList<>());
	}

	public RequestJournalDtoGrid(List<RequestJournalDto> items) {
		super(RequestConstants.getJournalColumns(), items);
		setStyleGenerator(item -> (item != null) ? RequestConstants.getStyle(item.getStatus()) : null);
		currentlyOpenedGrid = this;
	}

	@Override
	protected void onClick(RequestJournalDto entity, Column column) {

	}


	@Override
	protected void onDoubleClick(RequestJournalDto entity, Column column) {
		if (entity != null) {
			RequestDto request = BrigadesUI.getRestTemplate().getRequest(entity.getId());
			RequestLayout.showRequest(request);
		}
	}
}
