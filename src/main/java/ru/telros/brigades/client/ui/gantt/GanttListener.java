package ru.telros.brigades.client.ui.gantt;

import ru.telros.brigades.client.dto.GanttItemDto;

public interface GanttListener {
	void stepModified(GanttItemDto step);

	void stepDeleted(GanttItemDto step);

	void stepMoved(GanttItemDto step, int newStepIndex, int oldStepIndex);
}
