package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.PagesDto;
import ru.telros.brigades.client.dto.RequestJournalDto;

import java.util.stream.Stream;

public class RequestDataProvider extends AbstractBackEndDataProvider<RequestJournalDto, String> implements HasLogger {
	@Override
	protected Stream<RequestJournalDto> fetchFromBackEnd(Query<RequestJournalDto, String> query) {
		getLogger().debug("fetchFromBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		query.getSortOrders().forEach(querySortOrder -> getLogger().debug("Sorted: {}, Direction {}",
				querySortOrder.getSorted(), querySortOrder.getDirection()));
		PagesDto<RequestJournalDto> journal = BrigadesUI.getRestTemplate().getRequestsForJournal(query.getOffset(), query.getLimit());
		return journal.getCurrentContent().stream();
	}

	@Override
	protected int sizeInBackEnd(Query<RequestJournalDto, String> query) {
		getLogger().debug("sizeInBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		return BrigadesUI.getRestTemplate().countRequests().intValue();
	}

	@Override
	public Object getId(RequestJournalDto item) {
		return item.getId();
	}
}
