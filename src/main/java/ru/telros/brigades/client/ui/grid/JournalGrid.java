package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

import java.util.List;

import static ru.telros.brigades.client.ui.grid.GridColumn.COLUMN_WIDTH;
import static ru.telros.brigades.client.ui.grid.GridColumn.LAYOUT_WIDTH;

public abstract class JournalGrid<T> extends Grid<T> {
	private final AbstractBackEndDataProvider<T, String> dataProvider;

	public JournalGrid(List<GridColumn<T>> columns, AbstractBackEndDataProvider<T, String> dataProvider) {
		this.dataProvider = dataProvider;
		setSizeFull();
		setDataProvider(dataProvider);
		setSelectionMode(SelectionMode.SINGLE);
		setColumnReorderingAllowed(true);
		setColumnResizeMode(ColumnResizeMode.ANIMATED);
		columns.forEach(gridColumn -> {
			if (gridColumn.isComponent()) {
				Column<T, ?> col = addComponentColumn((ValueProvider<T, Component>) gridColumn.getValueProvider())
						.setId(gridColumn.getId()).setCaption(gridColumn.getCaption());
				col.setWidth(LAYOUT_WIDTH);
				col.setResizable(false);
				col.setSortable(false);
			} else {
				Column<T, ?> col = addColumn(gridColumn.getValueProvider()).setId(gridColumn.getId()).setCaption(gridColumn.getCaption());
				col.setWidth(COLUMN_WIDTH);
				col.setSortable(false);
				getDefaultHeaderRow().getCell(col).setDescription(col.getCaption());
				if (gridColumn.isDate())
					col.setComparator(new BrigadesDateComparator<>(gridColumn.getValueProvider()));
			}
		});

		addItemClickListener(event -> {
			if (event.getMouseEventDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT)) {
				if (event.getMouseEventDetails().isDoubleClick()) {
					onDoubleClick(event.getItem(), event.getColumn());
				}
			}
		});
	}

	protected abstract void onDoubleClick(T item, Column column);

	public void refreshAll() {
		dataProvider.refreshAll();
	}

	public T getSelectedRow() {
		return getSelectedItems().stream().findFirst().orElse(null);
	}
}
