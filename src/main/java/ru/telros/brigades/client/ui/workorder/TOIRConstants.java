package ru.telros.brigades.client.ui.workorder;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.CssLayout;
import ru.telros.brigades.client.dto.EODto;
import ru.telros.brigades.client.dto.MetricDto;
import ru.telros.brigades.client.dto.OperationDto;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public abstract class TOIRConstants {
	public static final GridColumn<EODto> EO_TYPE = GridColumn.createColumn("eoType", "Тип ЭО", EODto::getEoType);
	public static final GridColumn<EODto> EO_DESC = GridColumn.createColumn("eoDescription", "Наименование", EODto::getEoDescription);
	public static final GridColumn<EODto> EO_LENGTH = GridColumn.createColumn("eoLength", "Длина", EODto::getEoLength);
	public static final GridColumn<EODto> EO_DIAMETER = GridColumn.createColumn("eoDiametr", "Диаметр", EODto::getEoDiametr);
	public static final GridColumn<EODto> EO_MATERIAL = GridColumn.createColumn("eoMatType", "Материал", EODto::getEoMatType);
	public static final GridColumn<EODto> EO_LENGTH_ = GridColumn.createComponentColumn("_eoLength", "Длина", TOIRConstants::createLengthComponent);
	public static final GridColumn<EODto> EO_DIAMETER_ = GridColumn.createComponentColumn("_eoDiametr", "Диаметр", TOIRConstants::createDiameterComponent);
	public static final GridColumn<EODto> EO_MATERIAL_ = GridColumn.createComponentColumn("_eoMatType", "Материал", TOIRConstants::createMaterialComponent);

	public static final GridColumn<MetricDto> M_CODE = GridColumn.createColumn("metricsCode", "Код метрики", MetricDto::getMetricsCode);
	public static final GridColumn<MetricDto> M_QUANTITY = GridColumn.createColumn("quantity", "Количество", MetricDto::getQuantity);
	public static final GridColumn<MetricDto> M_QUANTITY_ = GridColumn.createColumn("quantity_", "Количество", TOIRConstants::createQuantityComponent);

	public static final GridColumn<OperationDto> O_SEQ_ID = GridColumn.createColumn("sequence", "Номер", OperationDto::getOperationSequenceId);
	public static final GridColumn<OperationDto> O_CODE = GridColumn.createColumn("code", "Код", OperationDto::getOperationCode);
	public static final GridColumn<OperationDto> O_NAME = GridColumn.createColumn("name", "Название этапа", OperationDto::getOperationName);
	public static final GridColumn<OperationDto> O_RESULT = GridColumn.createColumn("result", "Норматив", OperationDto::getCalcResult);

	private static CssLayout createLengthComponent(EODto eoDto) {
		return createCssLayout(eoDto.getEoLength());
	}

	private static CssLayout createDiameterComponent(EODto eoDto) {
		return createCssLayout(eoDto.getEoDiametr());
	}

	private static CssLayout createMaterialComponent(EODto eoDto) {
		return createCssLayout(eoDto.getEoMatType());
	}

	private static CssLayout createQuantityComponent(MetricDto metricDto) {
		return createCssLayout(metricDto.getQuantity());
	}

	private static CssLayout createCssLayout(String value) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		CustomTextField textField = new CustomTextField();
		textField.setWidth(90, Sizeable.Unit.PERCENTAGE);
		textField.setValue(value);
		layout.addComponent(textField);
		return layout;
	}

	public static List<GridColumn<EODto>> getEOColumns() {
		return Arrays.asList(EO_TYPE, EO_DESC, EO_LENGTH, EO_DIAMETER, EO_MATERIAL);
	}

	public static List<GridColumn<MetricDto>> getMetricColumns() {
		return Arrays.asList(M_CODE, M_QUANTITY);
	}

	public static List<GridColumn<OperationDto>> getOperationColumns() {
		return Arrays.asList(O_SEQ_ID, O_CODE, O_NAME, O_RESULT);
	}
}
