package ru.telros.brigades.client.ui.component;

import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.ui.Component;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;

import java.util.ArrayList;
import java.util.List;

public class PanelWithMenuLayout extends HeaderLayout {
	private static final int DEFAULT_ROWS = 2;

	private final List<HorizontalLayout> rows = new ArrayList<>();
	private final List<BrigadesPanel> panels = new ArrayList<>();
	//private final AbsoluteLayout absoluteLayout = new AbsoluteLayout();
	private MenuBar.MenuItem settings;

	public PanelWithMenuLayout(final String caption) {
		addToHeader(new Title(caption));
		addToHeader(buildMenuBar());
		initRows(DEFAULT_ROWS);
	}

	private Component buildMenuBar() {
		MenuBar menuBar = new MenuBar();
		settings = menuBar.addItem("Панели", null);
		return menuBar;
	}

	private void initRows(final int rows) {
		for (int i = 0; i < rows; i++) {
			HorizontalLayout row = new HorizontalLayout();
			row.setSizeFull();
			row.setSpacing(true);

			this.rows.add(row);

			DragAndDropWrapper rowWrapper = new DragAndDropWrapper(row);
//rowWrapper.setDragStartMode(DragAndDropWrapper.DragStartMode.COMPONENT);
			rowWrapper.setSizeFull();
			rowWrapper.addStyleName("no-vertical-drag-hints");
			rowWrapper.addStyleName("no-horizontal-drag-hints");
			rowWrapper.addStyleName("no-box-drag-hints");
			rowWrapper.setDropHandler(new RowDropHandler(row));

			addToLayout(rowWrapper);
		}
	}

	public BrigadesPanel addPanel(final String caption, final Component content, final int row, final boolean visible) {
		final BrigadesPanel panel = new BrigadesPanel(caption, content);
		panel.setMinMaxVisible(true);
		panel.addToggleMaximizedListener(this::toggleMaximized);
		if (!visible) panel.toggleShow();
		panel.setVisible(visible);
		panels.add(panel);

		MenuBar.MenuItem item = settings.addItem(caption, selectedItem -> panel.toggleShow());
		item.setCheckable(true);
		item.setChecked(visible);

		rows.get(row).addComponent(panel);
		return panel;
	}

	private void toggleMaximized(final BrigadesPanel panel, final boolean maximized) {
		for (HorizontalLayout row : rows) {
			row.getParent().setVisible(!maximized);
		}
		for (Component component : panels) {
			component.setVisible(!maximized);
		}
		if (maximized) {
			panel.setVisible(true);
			panel.getParent().getParent().setVisible(true);
		}
	}

	private class RowDropHandler implements DropHandler {
		private final HorizontalLayout row;

		private RowDropHandler(final HorizontalLayout row) {
			this.row = row;
		}

		@Override
		public AcceptCriterion getAcceptCriterion() {
			return AcceptAll.get();
		}

		@Override
		public void drop(final DragAndDropEvent dropEvent) {
			DragAndDropWrapper sourceWrapper = (DragAndDropWrapper) dropEvent.getTransferable().getSourceComponent();
			Component panel = sourceWrapper.getDragImageComponent();

			if (panel != null && panel.getParent() instanceof HorizontalLayout) {
				HorizontalLayout sourceRow = (HorizontalLayout) panel.getParent();

				int index = 0;
//if (sourceRow != row) {
				DragAndDropWrapper.WrapperTargetDetails targetDetails = (DragAndDropWrapper.WrapperTargetDetails) dropEvent.getTargetDetails();
				sourceRow.removeComponent(panel);
				switch (targetDetails.getHorizontalDropLocation()) {
					case LEFT:
						index = 0;
						break;
					case RIGHT:
						index = row.getComponentCount();
						break;
					default:
						if (row.getComponentCount() > 1) {
							index = row.getComponentCount() - 1;
						} else {
							index = row.getComponentCount();
						}
				}
				row.addComponent(panel, index);
//}
			}
		}
	}
}
