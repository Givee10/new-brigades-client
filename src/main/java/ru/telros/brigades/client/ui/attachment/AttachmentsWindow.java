package ru.telros.brigades.client.ui.attachment;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.ui.component.PropertyLabel;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

/**
 * Window for show attachments
 */
@SuppressWarnings("serial")
public class AttachmentsWindow extends WindowWithButtons {
	private final List<AttachmentDto> attachments;
	private final WorkDto workOrder;
	private AttachmentGrid attachmentIncomingTable;
	private AttachmentGrid attachmentOutgoingTable;

	private AttachmentsWindow(final Collection<AttachmentDto> attachments) {
		super("Вложения по работе", 800, -1);
		this.attachments = new ArrayList<>();
		this.attachments.addAll(attachments);
		this.attachmentIncomingTable = new AttachmentGrid(AttachmentColumn.getMainColumns(), new ArrayList<>());
		this.attachmentIncomingTable.setHeight(250, Sizeable.Unit.PIXELS);
		this.attachmentOutgoingTable = new AttachmentGrid(AttachmentColumn.getMainColumns(), new ArrayList<>());
		this.attachmentOutgoingTable.setHeight(250, Sizeable.Unit.PIXELS);

		if (this.attachments.size() > 0) {
			workOrder = BrigadesUI.getRestTemplate().getWork(this.attachments.get(0).getId());
			initAttachmentTables();
		} else {
			workOrder = null;
		}

		initButtonBar();
		initContents();

		// Отключение ESC
		removeAllCloseShortcuts();
	}

	public static void open(final Collection<AttachmentDto> attachments) {
		final Window window = new AttachmentsWindow(attachments);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	public static void open(final RequestDto request) {
		open(BrigadesUI.getRestTemplate().getRequestAttachments(request.getId()));
	}

	public static void open(final WorkDto workOrder) {
		open(BrigadesUI.getRestTemplate().getWorkAttachments(workOrder.getId()));
	}

	private void initButtonBar() {
		addToButtonBar(msg("button.close"), s -> close());
	}

	private void initContents() {
		final Component workOrderInfo = buildWorkOrderInfo();
		addToContent(workOrderInfo, 1);
		final Component tabSheet = buildTabSheet();
		addToContent(tabSheet, 1);
	}

	private Component buildWorkOrderInfo() {
		VerticalLayout layout = new VerticalLayout(
				new PropertyLabel("Адрес", workOrder.getAddress()),
				new PropertyLabel("Описание работы", workOrder.getDescription()),
				new PropertyLabel("Бригада", workOrder.getBrigade() != null ? workOrder.getBrigade() : "-"));
		layout.setSizeFull();
		layout.setSpacing(true);
		layout.setMargin(true);

		return CompsUtil.getGroupWrapper("Данные по работе", layout);
	}

	private Component buildTabSheet() {
		final TabSheet tabSheet = new TabSheet();
		tabSheet.addStyleName(BrigadesTheme.TABSHEET_FRAMED);

		if (attachmentIncomingTable.getItems().size() > 0) {
			Component attachmentIncomingTab = buildAttachmentTab(attachmentIncomingTable);
			tabSheet.addTab(attachmentIncomingTab, "Входящие");
		}

		if (attachmentOutgoingTable.getItems().size() > 0) {
			Component attachmentOutgoingTab = buildAttachmentTab(attachmentOutgoingTable);
			tabSheet.addTab(attachmentOutgoingTab, "Исходящие");
		}

		return tabSheet;
	}

	private Component buildAttachmentTab(AttachmentGrid attachmentTable) {
		final HorizontalLayout layout = new HorizontalLayout();
		layout.setWidth(100, Sizeable.Unit.PERCENTAGE);
		layout.setMargin(true);
		layout.setSpacing(true);
		layout.addComponent(attachmentTable);
		layout.setExpandRatio(attachmentTable, 1);
		return layout;
	}

	private void initAttachmentTables() {
		List<AttachmentDto> incoming = new ArrayList<>();
		List<AttachmentDto> outgoing = new ArrayList<>();
		attachments.forEach(attachment -> {
			switch (attachment.getType()) {
				case "IN":
					incoming.add(attachment);
					break;
				case "OUT":
					outgoing.add(attachment);
					break;
				default:
					break;
			}
		});
		attachmentIncomingTable.setItems(incoming);
		attachmentOutgoingTable.setItems(outgoing);
	}
}
