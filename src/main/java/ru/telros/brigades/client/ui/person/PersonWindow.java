package ru.telros.brigades.client.ui.person;

import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
import ru.telros.brigades.client.ui.component.CustomComboBox;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import static ru.telros.brigades.client.MessageManager.msg;

@SuppressWarnings("serial")
public class PersonWindow extends WindowWithButtons {
	private EmployeeBrigadeDto person;
	private Binder<EmployeeBrigadeDto> binder;

	private CustomTextField firstName = new CustomTextField(msg("table.person.firstName"));
	private CustomTextField middleName = new CustomTextField(msg("table.person.middleName"));
	private CustomTextField lastName = new CustomTextField(msg("table.person.lastName"));
	private CustomTextField phoneNumber = new CustomTextField(msg("table.person.phoneNumber"));
	private CustomTextField post = new CustomTextField(msg("table.person.post"));
	private CustomComboBox<String> positionComboBox = new CustomComboBox<>(msg("table.person.position"));
	private CustomComboBox<BrigadeDto> workGroupComboBox = new CustomComboBox<>(msg("table.workgroup.caption"));
	private Button openWorkGroupButton = new Button(BrigadesTheme.ICON_WORKGROUP);

	private boolean isEditable;

	private PersonWindow(EmployeeBrigadeDto person, boolean isEditable) {
		super(person.getIdEmployee() == null ? msg("personwindow.caption.new")
				: isEditable ? msg("personwindow.caption.edit") : msg("personwindow.caption.view"), 500, -1);
		this.person = person;
		this.binder = new Binder<>();
		this.isEditable = isEditable;
		build();
	}

	public static void open(EmployeeBrigadeDto person, boolean isEditable) {
		Window window = new PersonWindow(person, isEditable);
		UI.getCurrent().addWindow(window);
		window.setModal(true);
		window.focus();
	}

	private void build() {
		initMenu();
		initFields();
		addToContent(buildContent());
		setItem(person);

		firstName.setEnabled(isEditable);
		middleName.setEnabled(isEditable);
		lastName.setEnabled(isEditable);
		post.setEnabled(isEditable);
		phoneNumber.setEnabled(isEditable);
		positionComboBox.setEnabled(isEditable);
		workGroupComboBox.setEnabled(isEditable);
		openWorkGroupButton.setEnabled(!workGroupComboBox.isEmpty());

		// Отключение ESC
		removeAllCloseShortcuts();
	}

	private void initMenu() {
		//String content = (person.getId() == null) ? "PERSON: NEW" : "PERSON: " + person.getId().toString();
		//addToCaptionBar(content);
		if (isEditable) {
			addToButtonBar(msg("button.save"), $ -> {});
			addToButtonBar(msg("button.cancel"), $ -> close());
		} else {
			addToButtonBar(msg("button.close"), $ -> close());
		}
	}

	private void initFields() {
		String requireErrorPrefix = "Не заполнено поле ";

		lastName.setRequiredIndicatorVisible(true);
		firstName.setRequiredIndicatorVisible(true);
		middleName.setRequiredIndicatorVisible(true);
		post.setRequiredIndicatorVisible(true);

		openWorkGroupButton.setDescription("Просмотр бригады");
		//openWorkGroupButton.addClickListener($ -> WorkGroupLayout.showWorkGroup((WorkGroup) workGroupComboBox.getValue()));
	}

	private Component buildContent() {
		GridLayout layout = new GridLayout(2, 1);
		layout.setSpacing(true);
		layout.setSizeFull();
		VerticalLayout vll = new VerticalLayout(lastName, firstName, middleName, phoneNumber);
		// vll.setMargin(true);
		vll.setSpacing(true);
		layout.addComponent(vll, 0, 0);
		HorizontalLayout hl = new HorizontalLayout();
		hl.setSpacing(true);
		hl.setSizeFull();
		hl.addComponent(workGroupComboBox);
		hl.setExpandRatio(workGroupComboBox, 4);
		hl.addComponent(openWorkGroupButton);
		hl.setComponentAlignment(openWorkGroupButton, Alignment.BOTTOM_LEFT);
		hl.setExpandRatio(openWorkGroupButton, 1);
		VerticalLayout vlr = new VerticalLayout(post, positionComboBox, hl);
		vlr.setSpacing(true);
		layout.addComponent(vlr, 1, 0);
		return layout;
	}

	private void setItem(final EmployeeBrigadeDto person) {
		binder.setBean(person);
		//binder.bindInstanceFields(this);
	}
}
