package ru.telros.brigades.client.ui.window;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.GeocodeDto;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.component.ReadOnlyTextField;
import ru.telros.brigades.client.ui.grid.BrigadesFilterGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;
import ru.telros.brigades.client.ui.utils.CompsUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

public class GeocodeWindow extends WindowWithButtons {
	private GridColumn<GeocodeDto> NAME = GridColumn.createColumn("name", "Название", GeocodeDto::getName);
	private GridColumn<GeocodeDto> DESC = GridColumn.createColumn("desc", "Описание", GeocodeDto::getDescription);
	private GridColumn<GeocodeDto> LONG = GridColumn.createColumn("long", "", GeocodeDto::getLongitude);
	private GridColumn<GeocodeDto> LAT = GridColumn.createColumn("lat", "", GeocodeDto::getLatitude);

	private GeocodeDto address;
	private String strAddress;
	private GeocodeGrid geocodeGrid;
	private boolean modalResult = false;
	private List<GeocodeDto> addressList = new ArrayList<>();
	private CustomTextField addressSearchField = new CustomTextField(msg("table.address"));
	private ReadOnlyTextField cityTextField = new ReadOnlyTextField(msg("table.address.city"));
	private ReadOnlyTextField districtTextField = new ReadOnlyTextField(msg("table.address.district"));
	private ReadOnlyTextField streetTextField = new ReadOnlyTextField(msg("table.address.street"));

	public GeocodeWindow(String strAddress) {
		super("Поиск адреса", 800, -1);
		this.strAddress = strAddress;
		this.geocodeGrid = new GeocodeGrid();
		geocodeGrid.setHeightByRows(10);
		geocodeGrid.setCaption(msg("addressselectwindow.caption.addresslist"));
		geocodeGrid.addSelectionListener(selectionEvent -> {
			GeocodeDto geocodeDto = selectionEvent.getFirstSelectedItem().orElse(null);
			setAddress(geocodeDto);
			changeItems();
		});
		init();
	}

	private void init() {
		addToButtonBar(msg("button.save"), $ -> saveAddress());
		setButtonEnabled(0, false);
		addToButtonBar(msg("button.close"), $ -> close());

		Button searchButton = new Button(msg("button.search"), VaadinIcons.SEARCH);
		searchButton.setWidthUndefined();
		searchButton.addClickListener($ -> doSearch());

		HorizontalLayout hl = CompsUtil.getHorizontalWrapperNoMargin(addressSearchField, searchButton);
		hl.setExpandRatio(addressSearchField, 1);
		hl.setComponentAlignment(searchButton, Alignment.BOTTOM_RIGHT);
		addToContent(hl);
		addToContent(geocodeGrid);
		geocodeGrid.setWidth("100%");
		this.getContentLayout().setExpandRatio(geocodeGrid,1);
		addToContent(cityTextField);
		addToContent(districtTextField);
		addToContent(streetTextField);

		if (strAddress != null) {
			addressSearchField.setValue(strAddress);
		}
		addressSearchField.addShortcutListener(new ShortcutListener("Execute", ShortcutAction.KeyCode.ENTER, null) {
			@Override
			public void handleAction(Object sender, Object target) {
				String criteria = addressSearchField.getValue();
				if (criteria != null && !criteria.isEmpty()) {
					doSearch();
				}
			}
		});

	}

	/**
	 * Открытие окна для поиска адреса по строке
	 *
	 * @param address
	 */
	public static GeocodeWindow open(String address) {
		GeocodeWindow window = new GeocodeWindow(address);
		UI.getCurrent().addWindow(window);
		window.focus();
		window.doSearch();
		return window;
	}

	private void changeItems() {
		modalResult = address != null;
		setButtonEnabled(0, modalResult);

		if (modalResult) {
			cityTextField.setValue(address.getDescription());
			districtTextField.setValue("");
			streetTextField.setValue(address.getName());
		} else {
			cityTextField.clear();
			districtTextField.clear();
			streetTextField.clear();
		}
	}

	private void saveAddress() {
		if (address == null) {
			Notification.show("Не выбран адрес!", Notification.Type.ERROR_MESSAGE);
		} else {
			/*for (GeocodeDto a : addressList) {
				if (a.getDescription().equals(address.getDescription())
						&& a.getLatitude().equals(address.getLatitude())
						&& a.getLongitude().equals(address.getLongitude())) {
					setAddress(a);
					modalResult = true;
					addressFound = true;
					break;
				}
			}
			if (!addressFound) {
				//setAddress(restClient.saveAddress(address));
				modalResult = true;
			}*/
			if (!modalResult) return;
			close();
		}
	}

	public void doSearch() {
		if (!addressSearchField.isEmpty()) {
			addressList.clear();
			addressList.addAll(BrigadesUI.getRestTemplate().getGeocodeAddress(addressSearchField.getValue()));
			geocodeGrid.setItems(addressList);
		}
	}

	public void setAddress(GeocodeDto address) {
		this.address = address;
	}

	public GeocodeDto getAddress() {
		return address;
	}

	public boolean isModalResult() {
		return modalResult;
	}

	public List<GridColumn<GeocodeDto>> getMainColumns() {
		return Arrays.asList(NAME, DESC, LONG, LAT);
	}

	public List<GridColumn<GeocodeDto>> getGeocodeColumns() {
		return Arrays.asList(NAME, DESC);
	}

	private class GeocodeGrid extends BrigadesFilterGrid<GeocodeDto> {
		public GeocodeGrid() {
			super(getGeocodeColumns(), new ArrayList<>());
			getColumns().stream().forEach(e->{
				e.setExpandRatio(1);
				e.setWidth(300);
			});
			setSizeFull();
			removeFilterRow();
		}
	}
}
