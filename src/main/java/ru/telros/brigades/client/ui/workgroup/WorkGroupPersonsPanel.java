package ru.telros.brigades.client.ui.workgroup;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
import ru.telros.brigades.client.ui.person.PersonColumn;
import ru.telros.brigades.client.ui.person.PersonGrid;
import ru.telros.brigades.client.ui.person.PersonWindow;
import ru.telros.brigades.client.ui.window.YesNoWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.MessageManager.msg;

class WorkGroupPersonsPanel extends HorizontalLayout {
	private PersonGrid persons;
	private BrigadeDto workGroup;
	private Button viewPersonButton, addPersonButton, deletePersonButton;
	private List<EmployeeBrigadeDto> workers, workGroupWorkers;
	private boolean isDeleteMode;

	@SuppressWarnings("unchecked")
	WorkGroupPersonsPanel(BrigadeDto workGroup, List<EmployeeBrigadeDto> workers, boolean isDeleteMode) {
		this.workGroup = workGroup;
		this.persons = new PersonGrid(PersonColumn.getMainColumns(), new ArrayList<>());
		this.workGroupWorkers = new ArrayList<>();
		if (workGroup.getId() != null) {
			List<EmployeeBrigadeDto> personsList = BrigadesUI.getRestTemplate().getBrigadeEmployees(workGroup.getId());
			List<EmployeeBrigadeDto> entities = personsList.stream()
					.filter(brigadeEmployeeDto -> !brigadeEmployeeDto.getPost().equalsIgnoreCase("Мастер"))
					.collect(Collectors.toList());
			//UnsupportedOperationException:
			//personsList.removeIf(p -> p.getPost().equalsIgnoreCase("Мастер"));
			this.persons.setItems(entities);
			//this.persons.sort(PersonTable.COLUMN_FULLNAME, SortDirection.ASCENDING);
			this.workGroupWorkers.addAll(persons.getItems());
		}
		this.workers = new ArrayList<>(workers);
		/*Set<EmployeeDto> toRemove = this.workers.stream().filter(
		Person -> workGroup.getPersonsId().contains(Person.getId())).collect(Collectors.toSet());
		this.workers.removeAll(toRemove);*/
		this.isDeleteMode = isDeleteMode;
		build();
	}

	void setWorkGroup(BrigadeDto workGroup) {
		this.workGroup = workGroup;
	}

	List<EmployeeBrigadeDto> getWorkGroupWorkers() {
		return workGroupWorkers;
	}

	private void build() {
		setWidth(100, Unit.PERCENTAGE);
		setMargin(false);
		setSpacing(false);
		setSizeFull();
		buildTable();
		//buildControls();
	}

	private void buildTable() {
		persons.hideColumns(PersonColumn.ID.getId(), PersonColumn.GROUP.getId());
		persons.setSizeFull();
		persons.setHeight(200, Unit.PIXELS);
		//persons.addSelectionListener($ -> onSelectRow());
		addComponent(persons);
		setExpandRatio(persons, 1);
	}

	private void buildControls() {
		VerticalLayout controls = new VerticalLayout();
		controls.setWidthUndefined();
		controls.setSpacing(true);

		viewPersonButton = new Button();
		viewPersonButton.setIcon(BrigadesTheme.ICON_PERSON);
		viewPersonButton.setDescription("Просмотр");
		viewPersonButton.addClickListener($ -> onClickViewPerson());
		viewPersonButton.setSizeFull();
		viewPersonButton.setEnabled(false);
		controls.addComponent(viewPersonButton);

		addPersonButton = new Button();
		addPersonButton.setIcon(BrigadesTheme.ICON_ADD);
		addPersonButton.setDescription("Добавить сотрудника");
		addPersonButton.addClickListener($ -> onClickAddPerson());
		addPersonButton.setSizeFull();
		addPersonButton.setEnabled(!isDeleteMode);
		controls.addComponent(addPersonButton);

		deletePersonButton = new Button();
		deletePersonButton.setIcon(BrigadesTheme.ICON_DELETE);
		deletePersonButton.setDescription("Удалить сотрудника");
		deletePersonButton.addClickListener($ -> onClickDeletePerson());
		deletePersonButton.setSizeFull();
		deletePersonButton.setEnabled(false);
		controls.addComponent(deletePersonButton);

		addComponent(controls);
	}

	private void onSelectRow() {
		EmployeeBrigadeDto selectedPerson = persons.getSelectedRow();
		if (selectedPerson == null) {
			viewPersonButton.setEnabled(false);
			deletePersonButton.setEnabled(false);
		} else {
			viewPersonButton.setEnabled(true);
			if (!selectedPerson.getPost().equals("Мастер")) {
				deletePersonButton.setEnabled(!isDeleteMode);
			} else {
				deletePersonButton.setEnabled(false);
			}
		}
	}

	private void onClickViewPerson() {
		EmployeeBrigadeDto selectedPerson = persons.getSelectedRow();
		if (selectedPerson != null) {
			PersonWindow.open(selectedPerson, false);
		}
	}

	@SuppressWarnings("unchecked")
	private void onClickAddPerson() {
		WorkGroupSelectPersonWindow workGroupAddPersonWindow = WorkGroupSelectPersonWindow.open(workGroup, workers);
		workGroupAddPersonWindow.addCloseListener((Window.CloseListener) e -> {
			if (((WorkGroupSelectPersonWindow) e.getWindow()).hasPersonsAdded()) {
				workGroupAddPersonWindow.getAddedPersons().forEach(p -> persons.getItems().add(p));
				workGroupWorkers.clear();
				workGroupWorkers.addAll(persons.getItems());
				//persons.sort(PersonTable.COLUMN_FULLNAME, SortDirection.ASCENDING);
			}
		});
	}

	private void onClickDeletePerson() {
		EmployeeBrigadeDto selectedPerson = persons.getSelectedRow();
		if (selectedPerson != null) {
			if (selectedPerson.getPost().equals("Мастер")) {
				Notification.show("Мастер бригады не может быть удалён", Notification.Type.ERROR_MESSAGE);
			} else {
				YesNoWindow confirmationDialog = YesNoWindow
						.open(msg("warning"), msg("workgroupwindow.msg.deletepersonwarning",
								new Object[]{selectedPerson.getFullName(), workGroup.getName()}),
								true);
				confirmationDialog.addCloseListener((Window.CloseListener) e -> {
					if (((YesNoWindow) e.getWindow()).isModalResult()) {
						//selectedPerson.setWorkGroup(null);
						//restClient.updatePerson(selectedPerson, selectedPerson.getId());
						persons.getItems().remove(selectedPerson);
					}
				});
			}
		} else {
			Notification.show("Сотрудник не выбран", Notification.Type.WARNING_MESSAGE);
		}
	}
}
