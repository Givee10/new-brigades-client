package ru.telros.brigades.client.ui.camera;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.CameraDto;
import ru.telros.brigades.client.dto.CameraTokenDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.FullSizeVerticalLayout;

import java.util.LinkedHashMap;

import static ru.telros.brigades.client.MessageManager.msg;

public class CameraLayout extends FullSizeVerticalLayout {
	private final LinkedHashMap<String, String> legend = new LinkedHashMap<String, String>();

	private Image imageLocal = new Image();
	private Video videoLocal = new Video();
	private ExternalResource videoResource = null;
	private CameraDto camera = null;
	private CameraTokenDto tokenDto = new CameraTokenDto();

	public CameraLayout() {
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		tokenDto.setContainer("mjpeg");
		setMargin(false);
		imageLocal.setSizeFull();
		//videoLocal.setSizeFull();
		//videoLocal.setId("video-camera");
		//videoLocal.setShowControls(false);
		//videoLocal.setSource(new ExternalResource("https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8"));
		//videoLocal.play();
		addComponent(imageLocal);
		//addComponent(videoLocal);

		//update();
	}

	public void update() {

	}

	public Component getComponent() {
		return this;
	}

	public void setResource(CameraDto camera) {
		this.camera = camera;
		tokenDto.setChannel(camera.getExtChannel());
		tokenDto.setStream(camera.getStream());
		String videoURL = BrigadesUI.getRestTemplate().getCameraVideo(camera.getId(), tokenDto);
		if (videoURL != null) {
			videoResource = new ExternalResource(videoURL);
			//videoLocal.setDescription(camera.getDescription());
			//JavaScript.getCurrent().execute("playVideo('video-camera', '" + videoURL + "');");
			imageLocal.setDescription(camera.getName());
			imageLocal.setSource(videoResource);
		} else {
			unsetResource();
			Notification.show("Ошибка: камера не подключена (нет сигнала)", Notification.Type.ERROR_MESSAGE);
		}
	}

	public void unsetResource() {
		this.camera = null;
		videoResource = null;
		//videoLocal.setDescription("");
		//videoLocal.setSource(videoResource);
		//videoLocal.pause();
		//com.vaadin.ui.JavaScript.getCurrent().execute("stopVideo('video-camera');");
		imageLocal.setDescription("");
		imageLocal.setSource(videoResource);
	}

	public ExternalResource getVideoResource() {
		return videoResource;
	}

	public CameraDto getCamera() {
		return camera;
	}

	public void addToolBarItems(BrigadesPanel panel) {
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, legend);
		panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH,
				(MenuBar.Command) menuItem -> update());
	}
}
