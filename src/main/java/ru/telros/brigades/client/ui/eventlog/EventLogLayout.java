package ru.telros.brigades.client.ui.eventlog;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.server.Responsive;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.MenuBar;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.EventWithActionDto;
import ru.telros.brigades.client.dto.PagesDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.eventlog.AcceptEventLogEvent;
import ru.telros.brigades.client.event.eventlog.UpdateEventLogEvent;
import ru.telros.brigades.client.event.request.SelectRequestEvent;
import ru.telros.brigades.client.event.workorder.SelectWorkOrderEvent;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.FullSizeVerticalLayout;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.util.ArrayList;

import static ru.telros.brigades.client.MessageManager.msg;

public class EventLogLayout extends FullSizeVerticalLayout implements HasLogger {
	// таблица журнала событий
	private final EventGrid eventGrid = new EventGrid(new ArrayList<>());
	// контекстное меню для журнала событий
	private final GridContextMenu<EventWithActionDto> contextMenu = new GridContextMenu<>(eventGrid);
	private MenuBar.MenuItem miUpdate, miLegend;
	private Boolean updateVisible = true;

	public EventLogLayout() {
		setMargin(false);
		setSpacing(false);

		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		contextMenu.addGridHeaderContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridBodyContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridFooterContextMenuListener(event -> selectGridItem(event.getItem()));

		// Создание таблицы событий
		eventGrid.sort(EventConstants.DATE.getId(), SortDirection.DESCENDING);
		addComponent(eventGrid);
		setExpandRatio(eventGrid, 1);
		// Обновление журнала
		//updateAll();
	}

	public static void refresh() {
		BrigadesEventBus.post(new UpdateEventLogEvent(null));
	}

	public static void acceptEvent(EventWithActionDto event) {
		if (event != null) {
			BrigadesUI.getRestTemplate().acceptEvent(event);
			BrigadesEventBus.post(new AcceptEventLogEvent(event));
		}
	}

	public static void showAttachments(EventWithActionDto event) {
		/*List<AttachmentWorkDto> attachments = BrigadesUI.getRestTemplate().getEventAttachments(event);
		if (attachments != null && !attachments.isEmpty()) {
			AttachmentsWindow.open(attachments);
		}*/
	}

	private void selectGridItem(EventWithActionDto eventEntity) {
		contextMenu.removeItems();
		MenuBar.Command commandRefresh = $ -> onRefresh();
		if (eventEntity != null) {
			eventGrid.select(eventEntity);

			RequestDto request = findRequest(eventEntity);
			WorkDto workOrder = findWorkOrder(eventEntity);

			MenuBar.Command commandShowRequest = $ -> onShowRequest(findRequest(eventEntity));
			MenuBar.Command commandShowWorkOrder = $ -> onShowWorkOrder(findWorkOrder(eventEntity));
			//MenuBar.Command commandShowWorkGroup = $ -> onShowWorkGroup(findWorkGroup(eventEntity));
			MenuBar.Command commandFollow = $ -> {
				if (EventConstants.isRequestSource(eventEntity)) {
					if (request != null) {
						BrigadesEventBus.post(new SelectRequestEvent(request));
						return;
					}
				}
				if (EventConstants.isWorkSource(eventEntity)) {
					if (workOrder != null) {
						BrigadesEventBus.post(new SelectWorkOrderEvent(workOrder));
					}
				}
			};

			if (request != null || workOrder != null) {
				contextMenu.addItem(msg("menu.gotoaddress"), BrigadesTheme.ICON_MAP, commandFollow);
			}
			if (request != null) {
				contextMenu.addItem(msg("eventlog.menu.request.state"), BrigadesTheme.ICON_REQUEST, commandShowRequest);
			}
			if (workOrder != null) {
				contextMenu.addItem(msg("eventlog.menu.workorder.state"), BrigadesTheme.ICON_WORKORDER, commandShowWorkOrder);
			}
			contextMenu.addSeparator();
		} else eventGrid.deselectAll();
		contextMenu.addItem(msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, commandRefresh);
	}

	/**
	 * Полное обновление журнала событий
	 */
	public void updateAll() {
		if (updateVisible) {
			Long countEvents = BrigadesUI.getRestTemplate().countEvents();
			if (countEvents != null && countEvents != 0) {
				PagesDto<EventWithActionDto> events = BrigadesUI.getRestTemplate().getEventsWithActions(0, countEvents.intValue());
				eventGrid.update(EventConstants.filterEventsForGrid(events.getCurrentContent()));
			}
		}
	}

	/**
	 * Добавление кнопок меню в заголовок окна
	 */
	public void addToolBarItems(BrigadesPanel panel) {
		panel.addIconLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_INFO, EventConstants.iconEvents);
		miLegend = panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, EventConstants.eventStyles);
		miUpdate = panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, (MenuBar.Command) menuItem -> updateAll());
	}

	public static RequestDto findRequest(EventWithActionDto event) {
		if (event != null && EventConstants.isRequestSource(event)) {
			return BrigadesUI.getRestTemplate().getRequestByGuid(event.getObjectGuid());
		}
		return null;
	}

	public static WorkDto findWorkOrder(EventWithActionDto event) {
		if (event != null && EventConstants.isWorkSource(event)) {
			return BrigadesUI.getRestTemplate().getWorkByGuid(event.getObjectGuid());
		}
		return null;
	}

	private void onRefresh() {
		updateAll();
	}

	private void onShowRequest(RequestDto requestDto) {
		RequestLayout.showRequest(requestDto);
	}
/*
	private void onShowWorkGroup(BrigadeDto brigadeDto) {
		WorkGroupLayout.showWorkGroup(brigadeDto);
	}
*/
	private void onShowWorkOrder(WorkDto workDto) {
		WorkOrderLayout.showWorkOrder(workDto);
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		updateAll();
	}
}
