package ru.telros.brigades.client.ui.maplayout;

import com.vaadin.shared.ui.JavaScriptComponentState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * State for YandexMapComponent
 */
@SuppressWarnings("serial")
public class YandexMapComponentState extends JavaScriptComponentState {
	public static final long serialVersionUID = 1443920175588070872L;

	// Component options
	public Map<String, Object> options = new HashMap<>();

	// ID of element
	public String uid;

	// Enable or disable RPC calls from client
	public boolean initialized = false;
	public boolean markerClickedRpc = false;
	public boolean initializedRpc = false;
	public boolean boundsChangedRpc = false;

	// Center of map
	public double centerLatitude, centerLongitude;
	// Zoom factor
	public int zoom;
	// Map controls
	public String[] controls;
	// Map type
	public String type;

	// Коллекция маркеров
	public Collection<Marker> markers = new ArrayList<>();
	// Коллекция полилиний
	public Collection<Polyline> polylines = new ArrayList<>();
}
