package ru.telros.brigades.client.ui.gantt;

import org.tltv.gantt.Gantt;
import org.tltv.gantt.client.shared.Resolution;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;

import java.time.ZonedDateTime;
import java.util.Locale;
import java.util.TimeZone;

public class CustomGantt extends Gantt implements HasLogger {
	private TimeZone defaultTimeZone;

	public CustomGantt() {
		super();
		initGantt();
	}

	private void initGantt() {
		//setTimeZone(TimeZone.getTimeZone("Indian/Mayotte"));
		defaultTimeZone = TimeZone.getTimeZone(BrigadesUI.getRestTemplate().getGanttTimezone());
		setTimeZone(defaultTimeZone);
		setLocale(Locale.forLanguageTag("US"));
//		setTimeZone(TimeZone.getTimeZone("Europe/Zaporozhye"));
//		setTimeZone(getDefaultTimeZone());
		setWidth(100, Unit.PERCENTAGE);
		setHeight(100, Unit.PERCENTAGE);
		setShowCurrentTime(true);
		setResizableSteps(false);
		setMovableSteps(false);
		setMonthsVisible(false);
		setYearsVisible(false);
		setTimelineDayFormat("dd.MM.yyyy");
		setTimelineHourFormat("HH:00");
		setResolution(Resolution.Hour);
		updateCurrentTime();
		//addClickListener((Gantt.MoveListener) event -> Notification.show("Clicked " + event.getStep().getCaption()));
		//addMoveListener((Gantt.MoveListener) event -> Notification.show("Moved " + event.getStep().getCaption()));
		//addResizeListener((Gantt.ResizeListener) event -> Notification.show("Resized " + event.getStep().getCaption()));
	}

	@Override
	public TimeZone getTimeZone(){
		return defaultTimeZone;
	}

	private TimeZone getDefaultTimeZone() {
		//Gantt.getSupportedTimeZoneIDs().forEach(id -> getLogger().debug("Supported Time Zone: {}", id));
		return TimeZone.getTimeZone("Europe/Moscow");
		/*if (defaultTimeZone != null) {
			//getLogger().debug("getDefaultTimeZone(): tz != null -> {}", defaultTimeZone.getID());
			return defaultTimeZone;
		}
		TimeZone tz = TimeZone.getDefault();
		//getLogger().debug("getDefaultTimeZone(): TimeZone.getDefault -> {}", tz.getID());
		if (Gantt.getSupportedTimeZoneIDs().contains(tz.getID())) {
			defaultTimeZone = tz;
			//getLogger().debug("getDefaultTimeZone(): tz is in supported -> {}", defaultTimeZone.getID());
		} else {
			//defaultTimeZone = TimeZone.getTimeZone("Europe/Moscow");
			//defaultTimeZone = TimeZone.getTimeZone("Europe/Minsk");
			defaultTimeZone = TimeZone.getTimeZone(BrigadesUI.getRestTemplate().getGanttTimezone());
			//getLogger().debug("getDefaultTimeZone(): tz is not in supported -> {}", defaultTimeZone.getID());
		}
//		return defaultTimeZone;
		return TimeZone.getTimeZone(ZonedDateTime.now().getZone());*/
	}

	public void updateCurrentTime() {
		getState().timestamp = ZonedDateTime.now(defaultTimeZone.toZoneId()).toInstant().toEpochMilli();
		//this.updateCurrentDateAndTime();
		//this.getState().currentDate = this.currentDateFormatter.format(LocalDate.now(this.getTimeZone().toZoneId()));
		//this.getState().currentHour = this.currentHourFormatter.format(LocalDateTime.now(this.getTimeZone().toZoneId()));
		//this.getState().timestamp = ZonedDateTime.now(this.getTimeZone().toZoneId()).toInstant().toEpochMilli();
//		ZonedDateTime now = ZonedDateTime.now();
//		this.getState().currentDate = this.currentDateFormatter.format(now);
//		this.getState().currentHour = this.currentHourFormatter.format(now);
//		this.getState().timestamp = now.toInstant().toEpochMilli();
	}
}
