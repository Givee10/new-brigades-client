package ru.telros.brigades.client.ui.component;

import com.vaadin.ui.ListSelect;

public class CustomListSelect<T> extends ListSelect<T> {
	public CustomListSelect() {
		this(null);
	}

	public CustomListSelect(String caption) {
		super(caption);
		setSizeFull();
		setResponsive(true);
	}
}
