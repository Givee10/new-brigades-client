package ru.telros.brigades.client.ui.workgroup;

import ru.telros.brigades.client.dto.MechanizationBrigadePlanDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public class MechanizationGrid extends BrigadesGrid<MechanizationBrigadePlanDto> {
	public static final GridColumn<MechanizationBrigadePlanDto> ID_ = GridColumn.createColumn("id", "№", MechanizationBrigadePlanDto::getId);
	public static final GridColumn<MechanizationBrigadePlanDto> CODE = GridColumn.createColumn("code", "Код", MechanizationBrigadePlanDto::getCode);
	public static final GridColumn<MechanizationBrigadePlanDto> DESCRIPTION = GridColumn.createColumn("desc", "Описание", MechanizationBrigadePlanDto::getDescription);
	public static final GridColumn<MechanizationBrigadePlanDto> NUMBER = GridColumn.createColumn("number", "Номер", MechanizationBrigadePlanDto::getNumber);

	public MechanizationGrid(List<MechanizationBrigadePlanDto> items) {
		super(Arrays.asList(CODE, DESCRIPTION, NUMBER), items);
	}

	@Override
	protected void onDoubleClick(MechanizationBrigadePlanDto entity, Column column) {

	}

	@Override
	protected void onClick(MechanizationBrigadePlanDto entity, Column column) {

	}
}
