package ru.telros.brigades.client.ui.person;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.server.Responsive;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.FullSizeVerticalLayout;
import ru.telros.brigades.client.ui.window.YesNoWindow;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static ru.telros.brigades.client.MessageManager.msg;

public class PersonLayout extends FullSizeVerticalLayout {
	private MenuBar.MenuItem miAdd, miEdit, miView, miDelete, miUpdate;

	private PersonGrid personGrid = new PersonGrid(PersonColumn.getMainColumns(), new ArrayList<>()) {
		@Override
		protected void onClick(EmployeeBrigadeDto entity, Column column) {
			if (entity != null) onSelectPerson(entity);
		}

		@Override
		protected void onDoubleClick(EmployeeBrigadeDto entity, Column column) {
			if (entity != null) showPerson(entity);
		}
	};
	private GridContextMenu<EmployeeBrigadeDto> contextMenu = new GridContextMenu<>(personGrid);

	public PersonLayout() {
		setMargin(false);
		setSpacing(false);

		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		contextMenu.addGridHeaderContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridBodyContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridFooterContextMenuListener(event -> selectGridItem(event.getItem()));

		addComponentAndRatio(personGrid, 1);
		update();
	}

	public static void showPerson(EmployeeBrigadeDto person) {
		if (person != null) {
			PersonWindow.open(person, false);
		}
	}

	public static void editPerson(EmployeeBrigadeDto person) {
		if (person != null) {
			PersonWindow.open(person, true);
		}
	}

	public static void createPerson() {
		PersonWindow.open(new EmployeeBrigadeDto(), true);
	}

	private void selectGridItem(EmployeeBrigadeDto person) {
		contextMenu.removeItems();

		MenuBar.Command commandAdd = (MenuBar.Command) menuItem -> createPerson();
		MenuBar.Command commandRefresh = (MenuBar.Command) menuItem -> onClickRefreshList();

		if (person != null) {
			MenuBar.Command commandShow = (MenuBar.Command) menuItem -> showPerson(person);
			MenuBar.Command commandEdit = (MenuBar.Command) menuItem -> editPerson(person);
			MenuBar.Command commandAddToWorkGroup = (MenuBar.Command) menuItem -> addPersonToWorkGroup(person);
			MenuBar.Command commandRemove = (MenuBar.Command) menuItem -> deletePerson(person);
			MenuBar.Command commandRemoveFromWorkGroup = (MenuBar.Command) menuItem -> removePersonFromWorkGroup(person);

			contextMenu.addItem(msg("menu.open"), BrigadesTheme.ICON_PERSON, commandShow);
			contextMenu.addItem(msg("menu.person.edit"), BrigadesTheme.ICON_EDIT, commandEdit);
			contextMenu.addItem(msg("menu.person.delete"), BrigadesTheme.ICON_CLOSE, commandRemove);
			contextMenu.addSeparator();
			contextMenu.addItem("Добавить в бригаду...", BrigadesTheme.ICON_ADD_PERSON, commandAddToWorkGroup);
			contextMenu.addItem("Удалить из бригады", BrigadesTheme.ICON_REMOVE_PERSON, commandRemoveFromWorkGroup);
			contextMenu.addSeparator();
		} else personGrid.deselectAll();
		contextMenu.addItem(msg("menu.person.new"), BrigadesTheme.ICON_ADD, commandAdd);
		contextMenu.addItem(msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, commandRefresh);
	}

	public void select(EmployeeBrigadeDto person) {
		personGrid.select(person);
	}

	public void update() {
		try {
			//personGrid.update(BrigadesUI.getRestTemplate().getEmployees());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PersonGrid getComponent() {
		return personGrid;
	}

	public void addToolBarItems(BrigadesPanel panel) {
		miAdd = panel.addToolbarItem(
				"", msg("menu.person.new"), BrigadesTheme.ICON_ADD,
				(MenuBar.Command) menuItem -> onClickCreatePerson());
		miEdit = panel.addToolbarItem(
				"", msg("menu.person.edit"), BrigadesTheme.ICON_EDIT,
				(MenuBar.Command) menuItem -> onClickEditPerson());
		miEdit.setEnabled(false);
		miView = panel.addToolbarItem(
				"", msg("menu.open"), BrigadesTheme.ICON_PERSON,
				(MenuBar.Command) menuItem -> onClickViewPerson());
		miView.setEnabled(false);
		miDelete = panel.addToolbarItem(
				"", msg("menu.person.delete"), BrigadesTheme.ICON_CLOSE,
				(MenuBar.Command) menuItem -> onClickDeletePerson());
		miDelete.setEnabled(false);
		miUpdate = panel.addToolbarItem(
				"", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH,
				(MenuBar.Command) menuItem -> onClickRefreshList());
		miUpdate.setEnabled(personGrid.getItems().size() > 0);
	}

	private void addPersonToWorkGroup(EmployeeBrigadeDto person) {
		// Gotta be something
		String caption = "Add " + person.getFullName() + " to WorkGroup";
		Notification.show(caption, Notification.Type.ERROR_MESSAGE);
	}

	private void removePersonFromWorkGroup(EmployeeBrigadeDto person) {
		// Gotta be something
		//String caption = "Remove " + person.getFullName() + " from WorkGroup " + person.getWorkGroupId();
		//Notification.show(caption, Notification.Type.ERROR_MESSAGE);
	}

	private void deletePerson(EmployeeBrigadeDto person) {
		if (person.getPost().equals("Мастер")) {
			/*BrigadeDto workGroup = BrigadesUI.getRestTemplate().findWorkGroupByManager(person);
			if (workGroup != null) {
			String s = "Мастер бригады не может быть удалён. Замените мастера в бригадах " + workGroup.getName();
			Notification.show(s, Notification.Type.ERROR_MESSAGE);
			} else {
			removePerson(person);
			}*/
		} else {
			removePerson(person);
		}
	}

	private void removePerson(EmployeeBrigadeDto person) {
		if (person != null) {
			String prompt = "Вы действительно хотите удалить сотрудника " + person.getFullName() + "?";
			YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
			confirmationDialog.addCloseListener((Window.CloseListener) e -> {
				if (((YesNoWindow) e.getWindow()).isModalResult()) {
					BrigadesUI.getRestTemplate().deleteEmployee(person.getIdEmployee());
					update();
				}
			});
		}
	}

	@PostConstruct
	private void init() {
		update();
		BrigadesEventBus.register(this);
	}

	private void onSelectPerson(EmployeeBrigadeDto person) {
		miEdit.setEnabled(person != null);
		miView.setEnabled(person != null);
		miDelete.setEnabled(person != null);
	}

	private void onClickCreatePerson() {
		createPerson();
	}

	private void onClickEditPerson() {
		EmployeeBrigadeDto person = personGrid.getSelectedRow();
		if (person != null) {
			editPerson(person);
		}
	}

	private void onClickViewPerson() {
		EmployeeBrigadeDto person = personGrid.getSelectedRow();
		if (person != null) {
			showPerson(person);
		}
	}

	private void onClickDeletePerson() {
		EmployeeBrigadeDto person = personGrid.getSelectedRow();
		if (person != null) {
			deletePerson(person);
		}
	}

	private void onClickRefreshList() {
		update();
	}

}
