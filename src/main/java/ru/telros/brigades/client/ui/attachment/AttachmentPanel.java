package ru.telros.brigades.client.ui.attachment;

import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.AttachmentContentDto;
import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.event.BroadcastMessageEvent;
import ru.telros.brigades.client.event.Broadcaster;
import ru.telros.brigades.client.ui.component.CustomButton;
import ru.telros.brigades.client.ui.component.CustomTextArea;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.FileUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.window.YesNoWindow;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static ru.telros.brigades.client.MessageManager.msg;

public class AttachmentPanel extends HorizontalLayout implements HasLogger {
	private List<AttachmentDto> itemAttachments;
	private AttachmentGrid attachmentGrid;
	private boolean hasChanged;
	private Map<Class, Long> sourceMap;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	public AttachmentPanel(List<AttachmentDto> attachments, Map<Class, Long> sourceMap) {
		this.itemAttachments = new ArrayList<>();
		this.itemAttachments.addAll(attachments);
		this.attachmentGrid = new AttachmentGrid(AttachmentColumn.getMainColumns(), itemAttachments) {
			@Override
			protected void onDoubleClick(AttachmentDto entity, Column column) {
				select(entity);
				showAttachment();
			}
		};
		this.attachmentGrid.setHeight(300, Unit.PIXELS);
		this.attachmentGrid.sort(AttachmentColumn.DATE.getId(), SortDirection.DESCENDING);
		this.hasChanged = false;
		this.sourceMap = sourceMap;
		build();
	}

	private void build() {
		setWidth(100, Unit.PERCENTAGE);
		setMargin(false);
		setSpacing(false);
		setSizeFull();
		buildGrid();
	}

	private void buildGrid() {
		CustomButton showButton = new CustomButton(BrigadesTheme.ICON_SHOW, msg("button.show"), $ -> showAttachment());
		CustomButton editButton = new CustomButton(BrigadesTheme.ICON_EDIT, msg("button.edit"), $ -> editAttachment());
		CustomButton downloadButton = new CustomButton(BrigadesTheme.ICON_DOWNLOAD, msg("button.download"), $ -> downloadAttachment());
		CustomButton uploadButton = new CustomButton(BrigadesTheme.ICON_UPLOAD, msg("button.upload"), $ -> uploadAttachment());
		CustomButton deleteButton = new CustomButton(BrigadesTheme.ICON_DELETE, msg("button.delete"), $ -> deleteAttachment());
		CustomButton copyButton = new CustomButton(BrigadesTheme.ICON_COPY, msg("button.copy"), $ -> copyAttachment());
		CustomButton pasteButton = new CustomButton(BrigadesTheme.ICON_PASTE, msg("button.paste"), $ -> pasteAttachment());

		addComponent(CompsUtil.buildGridWithButtons(attachmentGrid, showButton, uploadButton, deleteButton, copyButton, pasteButton));
	}

	private void saveAttachment(String fileName, String filePath, String description) {
		AttachmentDto attachment = new AttachmentDto();
		attachment.setType("OUT");
		attachment.setDescription(description);
		attachment.setPath(filePath);
		attachment.setFileName(fileName);
		attachment.setFileType(FileUtil.detectFileMIMEType(filePath));
		itemAttachments.add(attachment);
		attachmentGrid.update(itemAttachments);
		hasChanged = true;
	}

	private void downloadAttachment() {
		AttachmentDto attachment = attachmentGrid.getSelectedRow();
		if (attachment != null) {
			/*AttachmentContentDto attachmentContent = sourceMap.containsKey(WorkDto.class) ?
					restTemplate.getWorkAttachmentContent(sourceMap.get(WorkDto.class), attachment.getId()) :
					restTemplate.getRequestAttachmentContent(sourceMap.get(RequestDto.class), attachment.getId());
			StreamResource.StreamSource streamSource = (StreamResource.StreamSource) () ->
					(attachmentContent == null) ? null : new ByteArrayInputStream(attachmentContent.getContent());

			StreamResource streamResource = new StreamResource(streamSource, attachment.getFileName());
			if (streamSource.getStream() != null) {
				Button btn = new Button("Download");
				btn.setVisible(false);
				FileDownloader fd = new FileDownloader(streamResource);
				fd.extend(btn);
				addComponent(btn);
				btn.click();
			} else {
				Notification.show("Произошла ошибка при получении данных вложения", Notification.Type.WARNING_MESSAGE);
			}*/
		} else {
			Notification.show("Вложение не выбрано", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void uploadAttachment() {
		if (sourceMap.values().contains(null))
			Notification.show("Приложение можно добавить только после сохранения работы", Notification.Type.ERROR_MESSAGE);
		else
			AttachmentUploadWindow.open(sourceMap);
	}

	private void editAttachment() {
		AttachmentDto attachment = attachmentGrid.getSelectedRow();
		if (attachment != null) {
			editAttachmentDescription(attachment);
			attachmentGrid.deselectAll();
			hasChanged = true;
		} else {
			Notification.show("Вложение не выбрано", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void showAttachment() {
		AttachmentDto attachment = attachmentGrid.getSelectedRow();
		if (attachment != null) {
			AttachmentShowWindow.open(attachment, sourceMap);
			//attachmentGrid.deselectAll();
			//hasChanged = true;
		} else {
			Notification.show("Вложение не выбрано", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void copyAttachment() {
		AttachmentDto attachment = attachmentGrid.getSelectedRow();
		if (attachment != null) {
			BrigadesUI.setCurrentAttachment(attachment);
			Notification.show("Вложение скопировано", Notification.Type.TRAY_NOTIFICATION);
		} else {
			Notification.show("Вложение не выбрано", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void pasteAttachment() {
		AttachmentDto currentAttachment = BrigadesUI.getCurrentAttachment();
		if (currentAttachment != null) {
			AttachmentContentDto attachmentContent = sourceMap.containsKey(WorkDto.class) ?
					restTemplate.getWorkAttachmentContent(sourceMap.get(WorkDto.class), currentAttachment.getId()) :
					restTemplate.getRequestAttachmentContent(sourceMap.get(RequestDto.class), currentAttachment.getId());
			if (attachmentContent != null) {
				String fileName = currentAttachment.getFileName();
				File file = new File(FileUtil.getDirectoryFileName(fileName));
				getLogger().debug("Name: {}, Path: {}", file.getName(), file.getPath());
				try {
					OutputStream os = new FileOutputStream(file);
					os.write(attachmentContent.getContent());
					os.close();

					String type = currentAttachment.getType();
					String description = currentAttachment.getDescription();
					Boolean mobile = currentAttachment.getMobile();
					AttachmentDto attachmentDto = (sourceMap.containsKey(WorkDto.class)) ?
							restTemplate.postWorkAttachment(sourceMap.get(WorkDto.class), file, fileName, type, description, mobile) :
							restTemplate.postRequestAttachment(sourceMap.get(RequestDto.class), file, fileName, type, description, mobile);
					if (attachmentDto != null) {
						if (sourceMap.containsKey(WorkDto.class)) {
							WorkOrderLayout.refresh();
						} else {
							RequestLayout.refresh();
						}
						return;
					}
				} catch (IOException e) {
					getLogger().error(e.getMessage());
				} finally {
					getLogger().debug("File deleted: {}", file.delete());
					BrigadesUI.setCurrentAttachment(null);
				}
			}
			Notification.show("Произошла ошибка при отправке файла на сервер", Notification.Type.ERROR_MESSAGE);
		} else {
			Notification.show("Вложение не было скопировано", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void deleteAttachment() {
		AttachmentDto attachment = attachmentGrid.getSelectedRow();
		if (attachment != null) {
			if (attachment.getType().equals("IN")) {
				Notification.show("Входящее вложение не может быть удалено", Notification.Type.ERROR_MESSAGE);
			} else {
				String prompt = "Вы действительно хотите удалить вложение " + attachment.getFileName() + "?";
				YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
				confirmationDialog.addCloseListener((Window.CloseListener) e -> {
					if (((YesNoWindow) e.getWindow()).isModalResult()) {
						if (sourceMap.containsKey(WorkDto.class)) {
							restTemplate.deleteWorkAttachment(sourceMap.get(WorkDto.class), attachment.getId());
							WorkOrderLayout.refresh();
						} else {
							restTemplate.deleteRequestAttachment(sourceMap.get(RequestDto.class), attachment.getId());
							RequestLayout.refresh();
						}
						hasChanged = true;
						Notification.show("Вложение " + attachment.getFileName() + " удалено", Notification.Type.WARNING_MESSAGE);
					}
				});
			}
		} else {
			Notification.show("Вложение не выбрано", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void editAttachmentDescription(AttachmentDto attachment) {
		WindowWithButtons window = new WindowWithButtons("Изменение описания вложения", 600, -1);
		window.addToContent(new Label("Вложение: " + attachment.getFileName()));
		CustomTextArea description = new CustomTextArea();
		description.setValue(attachment.getDescription());
		window.addToContent(description);
		window.addToButtonBar(msg("button.save"), clickEvent -> {
			if (description.getValue().isEmpty()) {
				Notification.show("Описание вложения не может быть пустым", Notification.Type.ERROR_MESSAGE);
			} else {
				itemAttachments.get(itemAttachments.indexOf(attachment)).setDescription(description.getValue());
				attachmentGrid.update(itemAttachments);
				Broadcaster.broadcast(new BroadcastMessageEvent("Описание вложения " + attachment.getFileName() + " изменено"));
				window.close();
			}
		});
		window.addToButtonBar(msg("button.close"), clickEvent -> window.close());
		UI.getCurrent().addWindow(window);
		window.setResizable(false);
		window.focus();
	}

	public List<AttachmentDto> getAttachments() {
		return attachmentGrid.getItems();
	}

	public void setAttachments(Collection<AttachmentDto> attachments) {
		itemAttachments.clear();
		itemAttachments.addAll(attachments);
		attachmentGrid.update(itemAttachments);
	}

	public boolean hasChanged() {
		return hasChanged;
	}

	public void setSourceMap(Map<Class, Long> sourceMap) {
		this.sourceMap = sourceMap;
	}
}
