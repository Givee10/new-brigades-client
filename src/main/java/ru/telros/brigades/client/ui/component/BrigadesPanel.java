package ru.telros.brigades.client.ui.component;

import com.vaadin.server.Resource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesIcons;
import ru.telros.brigades.client.BrigadesTheme;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BrigadesPanel extends CustomComponent {
	private final VerticalLayout layout = new FullSizeVerticalLayout();

	private final String caption;
	private final Component content;
	private final HorizontalLayout header = new HorizontalLayout();

	private boolean show = true;
	private boolean maximized = false;
	private MenuBar toolBar = new MenuBar();
	private MenuBar.MenuItem maxItem;

	private List<ToggleMaximizedListener> listeners = new ArrayList<>();
	private List<ToggleVisibleListener> visibleListeners = new ArrayList<>();

	public BrigadesPanel(String caption, Component content) {
		this.caption = caption;
		this.content = content;

		setSizeFull();
		initContent();

		setCompositionRoot(layout);
	}

	private void initContent() {
		layout.addStyleName(BrigadesTheme.LAYOUT_WELL);
		layout.addComponent(buildHeader());
		layout.addComponent(content);
		layout.setExpandRatio(content, 1);
		layout.setMargin(false);
		layout.setSpacing(false);
	}

	private Component buildHeader() {
		header.setWidth(100, Unit.PERCENTAGE);
		header.addStyleName(BrigadesTheme.PANEL_CAPTION);

		Label title = new Label(caption);
		title.addStyleName(BrigadesTheme.LABEL_COLORED);

		header.addComponent(title);
		header.setExpandRatio(title, 1);

		header.addComponent(buildToolbar());

		DragAndDropWrapper headerWrapper = new DragAndDropWrapper(header);
		headerWrapper.setDragStartMode(DragAndDropWrapper.DragStartMode.COMPONENT_OTHER);
		headerWrapper.setDragImageComponent(this);

		return headerWrapper;
	}

	private Component buildToolbar() {
		toolBar.addStyleName(BrigadesTheme.MENUBAR_BORDERLESS);

		maxItem = toolBar.addItem("", BrigadesTheme.ICON_EXPAND, selectedItem -> {
			if (maximized) {
				maximized = false;
				toggleMaximized(false);
				selectedItem.setIcon(BrigadesTheme.ICON_EXPAND);
			} else {
				maximized = true;
				toggleMaximized(true);
				selectedItem.setIcon(BrigadesTheme.ICON_COMPRESS);
			}
			maxItem.setDescription(maximized ? "Свернуть" : "Развернуть");
		});
		maxItem.setStyleName(BrigadesTheme.BUTTON_ICON_ONLY);
		maxItem.setDescription("Развернуть");

		return toolBar;
	}

	private void toggleMaximized(boolean b) {
		listeners.forEach(l -> l.toggleMaximized(this, b));
	}

	public void addToggleMaximizedListener(ToggleMaximizedListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	public void removeToggleMaximizedListener(ToggleMaximizedListener listener) {
		listeners.remove(listener);
	}

	public void setMinMaxVisible(boolean isVisible) {
		maxItem.setVisible(isVisible);
	}


	public MenuBar.MenuItem addToolbarItem(String caption, String description, Resource icon, MenuBar.Command command) {
		MenuBar.MenuItem item = toolBar.addItemBefore(caption, icon, command, maxItem);
		item.setDescription(description);
		item.setStyleName(BrigadesTheme.BUTTON_ICON_ONLY);
		return item;
	}

	public MenuBar.MenuItem addFilterItem(String caption, String description, Resource icon, LinkedHashMap<String, MenuBar.Command> filterItems) {
		MenuBar.MenuItem item = addToolbarItem(caption, description, icon, null);
		for (Map.Entry<String, MenuBar.Command> legendItem : filterItems.entrySet()) {
			MenuBar.MenuItem item2 = item.addItem(legendItem.getKey(), legendItem.getValue());
			item2.setCheckable(true);
			item2.setChecked(true);
		}
		return item;
	}

	public MenuBar.MenuItem addLegendItem(String caption, String description, Resource icon, LinkedHashMap<String, String> legendItems) {
		MenuBar.MenuItem item = addToolbarItem(caption, description, icon, null);
		for (Map.Entry<String, String> legendItem : legendItems.entrySet()) {
			MenuBar.MenuItem item2 = item.addItem(legendItem.getKey(), BrigadesTheme.ICON_LEGEND_ITEM, null);
			//item2.setCheckable(true);
			//item2.setChecked(true);
			item2.setStyleName(legendItem.getValue());
		}
		return item;
	}

	public MenuBar.MenuItem addIconLegendItem(String caption, String description, Resource icon, LinkedHashMap<String, BrigadesIcons> legendItems) {
		MenuBar.MenuItem item = addToolbarItem(caption, description, icon, null);
		for (Map.Entry<String, BrigadesIcons> legendItem : legendItems.entrySet()) {
			if (legendItem.getValue() != null) {
				MenuBar.MenuItem item2 = item.addItem(legendItem.getKey(), legendItem.getValue(), null);
				item2.setDescription(legendItem.getValue().getHtml(), ContentMode.HTML);
				//item2.setCheckable(true);
				//item2.setChecked(true);
			} else item.addSeparator();
		}
		return item;
	}

	public MenuBar.MenuItem addMapLegendItem(String caption, String description, Resource icon, Map<String, MenuBar.Command> items) {
		MenuBar.MenuItem item = addToolbarItem(caption, description, icon, null);
		for (Map.Entry<String, MenuBar.Command> legendItem : items.entrySet()) {
			MenuBar.MenuItem item2 = item.addItem(legendItem.getKey(), BrigadesTheme.ICON_LEGEND_ITEM, legendItem.getValue());
			//item2.setCheckable(true);
			//item2.setChecked(true);
		}
		return item;
	}

	public void toggleShow() {
		show = !show;
		super.setVisible(show);
		visibleListeners.forEach(l -> l.toggleVisible(this, show));
	}

	public void addToggleVisibleListener(ToggleVisibleListener listener) {
		if (!visibleListeners.contains(listener)) {
			visibleListeners.add(listener);
		}
	}

	public void removeToggleVisibleListener(ToggleVisibleListener listener) {
		visibleListeners.remove(listener);
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible && show);
	}

	/**
	 * Класс для подписчиков на событие изменения размера панели
	 */
	public interface ToggleMaximizedListener {
		void toggleMaximized(final BrigadesPanel panel, final boolean maximized);
	}

	public interface ToggleVisibleListener {
		void toggleVisible(final BrigadesPanel panel, final boolean visible);
	}

	public MenuBar getToolBar() {
		return toolBar;
	}
}
