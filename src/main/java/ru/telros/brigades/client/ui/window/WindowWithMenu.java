package ru.telros.brigades.client.ui.window;

import com.vaadin.server.Resource;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;

public class WindowWithMenu extends ModalWindow {
	// Меню
	private final MenuBar menu = new MenuBar();
	private final VerticalLayout content = new VerticalLayout();

	public WindowWithMenu() {
		this(null);
	}

	public WindowWithMenu(String caption) {
		super(caption);
		initContent();
	}

	public WindowWithMenu(final int width, final int height) {
		this(null, width, height);
	}

	public WindowWithMenu(String caption, final int width, final int height) {
		super(caption, width, height);
		initContent();
	}

	private void initContent() {
		final VerticalLayout internalContent = new VerticalLayout();

		menu.setWidth(100, Sizeable.Unit.PERCENTAGE);

		content.setMargin(true);
		content.setSpacing(true);

		internalContent.addComponent(menu);
		internalContent.addComponent(content);
		internalContent.setExpandRatio(content, 1);

		setContent(internalContent);
	}

	public void addToMenu(final String caption, final MenuBar.Command command) {
		addToMenu(caption, null, command);
	}

	public void addToMenu(final String caption, final Resource icon, final MenuBar.Command command) {
		menu.addItem(caption, icon, command);
	}

	public void addToContent(final Component component) {
		content.addComponent(component);
	}
}
