package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.BrigadeJournalDto;
import ru.telros.brigades.client.dto.PagesDto;

import java.util.stream.Stream;

public class BrigadeDataProvider extends AbstractBackEndDataProvider<BrigadeJournalDto, String> implements HasLogger {
	@Override
	protected Stream<BrigadeJournalDto> fetchFromBackEnd(Query<BrigadeJournalDto, String> query) {
		getLogger().debug("fetchFromBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		query.getSortOrders().forEach(querySortOrder -> getLogger().debug("Sorted: {}, Direction {}",
				querySortOrder.getSorted(), querySortOrder.getDirection()));
		PagesDto<BrigadeJournalDto> journal = BrigadesUI.getRestTemplate().getBrigadesForJournal(query.getOffset(), query.getLimit());
		return journal.getCurrentContent().stream();
	}

	@Override
	protected int sizeInBackEnd(Query<BrigadeJournalDto, String> query) {
		getLogger().debug("sizeInBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		return BrigadesUI.getRestTemplate().countBrigades().intValue();
	}

	@Override
	public Object getId(BrigadeJournalDto item) {
		return item.getId();
	}
}
