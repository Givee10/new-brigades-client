package ru.telros.brigades.client.ui.request;

import ru.telros.brigades.client.dto.CommentDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class CommentaryGrid extends BrigadesGrid<CommentDto> {
	public CommentaryGrid(List<GridColumn<CommentDto>> columns, List<CommentDto> items) {
		super(columns, items);
	}


	@Override
	protected void onClick(CommentDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(CommentDto entity, Column column) {

	}


}
