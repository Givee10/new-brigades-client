package ru.telros.brigades.client.ui.maplayout;

/**
 * Listener for click on map marker
 */
public interface MapMarkerClickListener {
	void markerClicked(Object marker);
}
