package ru.telros.brigades.client.ui.workgroup;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.UI;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
import ru.telros.brigades.client.event.BroadcastMessageEvent;
import ru.telros.brigades.client.event.Broadcaster;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.person.PersonColumn;
import ru.telros.brigades.client.ui.person.PersonGrid;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.util.ArrayList;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

class WorkGroupSelectPersonWindow extends WindowWithButtons {
	private BrigadeDto workGroup;
	private List<EmployeeBrigadeDto> workers;
	private PersonGrid personTable;
	private List<EmployeeBrigadeDto> addedPersons;
	private boolean hasPersonsAdded;

	private WorkGroupSelectPersonWindow(BrigadeDto workGroup, List<EmployeeBrigadeDto> workers) {
		super(msg("workgroupselectpersonwindow.caption"), 600, -1);
		this.workGroup = workGroup;
		this.workers = workers;
		this.hasPersonsAdded = false;
		build();
	}

	static WorkGroupSelectPersonWindow open(BrigadeDto workGroup, List<EmployeeBrigadeDto> workers) {
		WorkGroupSelectPersonWindow workGroupAddPersonWindow = new WorkGroupSelectPersonWindow(workGroup, workers);
		UI.getCurrent().addWindow(workGroupAddPersonWindow);
		workGroupAddPersonWindow.setModal(true);
		workGroupAddPersonWindow.focus();
		return workGroupAddPersonWindow;
	}

	boolean hasPersonsAdded() {
		return hasPersonsAdded;
	}

	List<EmployeeBrigadeDto> getAddedPersons() {
		return addedPersons;
	}

	private void build() {
		DisabledTextField workGroupName = new DisabledTextField(msg("table.workgroup.caption"));
		if (workGroup.getId() == null) {
			workGroupName.setValue("");
		} else {
			workGroupName.setValue(workGroup.toString());
		}
		addToContent(workGroupName);

		personTable = new PersonGrid(PersonColumn.getMainColumns(), workers);
		personTable.setCaption(msg("workgroupselectpersonwindow.caption.personslist"));
		personTable.hideColumns(PersonColumn.ID.getId(), PersonColumn.PHONE.getId(), PersonColumn.POST.getId());
		personTable.setSizeFull();
		personTable.setHeight(400, Sizeable.Unit.PIXELS);
		if (workGroup.getId() != null) {
			//workers.removeAll(BrigadesUI.getRestTemplate().getBrigadeEmployees(workGroup.getId()));
		}
		personTable.setItems(workers);
		personTable.addSelectionListener(listener -> {
			EmployeeBrigadeDto selectedPerson = personTable.getSelectedRow();
			setButtonEnabled(0, selectedPerson != null);
		});
		//personTable.sort("lastName");
		addToContent(personTable);

		addToButtonBar(msg("button.add"), clickEvent -> onClickAddButton());
		setButtonEnabled(0, false);

		addToButtonBar(msg("button.close"), clickEvent -> close());
	}

	private void onClickAddButton() {
		EmployeeBrigadeDto selectedPerson = personTable.getSelectedRow();
		if (selectedPerson != null) {
			/*if (selectedPerson.getWorkGroupId() != null) {
			YesNoWindow confirmationDialog = YesNoWindow.open(
			msg("warning"),
			msg("workgroupselectpersonwindow.msg.movepersonwarning",
			new Object[] { selectedPerson.getFullName(), selectedPerson.getWorkGroup(), workGroup.getName() }),
			  true);
			confirmationDialog.addCloseListener((Window.CloseListener) e -> {
			if (((YesNoWindow)e.getWindow()).isModalResult()) {
			addPerson(selectedPerson);
			}
			});
			} else {
			addPerson(selectedPerson);
			}*/
		}
	}

	private void addPerson(EmployeeBrigadeDto selectedPerson) {
		if (workGroup.getId() != null) {
			//selectedPerson.setWorkGroup(workGroup.toString());
			//selectedPerson.setWorkGroupId(workGroup.getId());
			//BrigadesUI.getDataService().updatePerson(selectedPerson, selectedPerson.getId());
		}
		if (addedPersons == null) {
			addedPersons = new ArrayList<>();
		}
		addedPersons.add(selectedPerson);
		hasPersonsAdded = true;
		personTable.deselectAll();
		Broadcaster.broadcast(new BroadcastMessageEvent(
				msg("workgroupselectpersonwindow.msg.addpersonnotification",
						new Object[]{selectedPerson.getFullName(), workGroup.getName()})));
	}

}
