//package ru.telros.brigades.client.ui.workorder;
//
//import com.vaadin.shared.ui.window.WindowMode;
//import com.vaadin.ui.Alignment;
//import com.vaadin.ui.Button;
//import com.vaadin.ui.HorizontalLayout;
//import com.vaadin.ui.VerticalLayout;
//import ru.telros.brigades.client.BrigadesTheme;
//import ru.telros.brigades.client.BrigadesUI;
//import ru.telros.brigades.client.dto.RequestDto;
//import ru.telros.brigades.client.dto.WorkDto;
//import ru.telros.brigades.client.ui.component.CustomComboBox;
//import ru.telros.brigades.client.ui.component.DisabledTextArea;
//import ru.telros.brigades.client.ui.component.DisabledTextField;
//import ru.telros.brigades.client.ui.request.RequestLayout;
//import ru.telros.brigades.client.ui.utils.CompsUtil;
//
//import java.util.List;
//
//import static ru.telros.brigades.client.MessageManager.msg;
//
//class WorkOrderRequestInfoPanel extends VerticalLayout {
//	private WorkDto workOrder;
//	private RequestDto request;
//	private WorkOrderWindow workOrderWindow;
//
//	private CustomComboBox<RequestDto> sourceComboBox = new CustomComboBox<>(msg("table.request.source"));
//	private Button clearRequestSelectionButton = new Button(BrigadesTheme.ICON_CLOSE);
//	private Button openRequestButton = new Button(msg("button.open"), BrigadesTheme.ICON_REQUEST);
//	private DisabledTextField numberTextField = new DisabledTextField("Номер");
//	private DisabledTextField typeTextField = new DisabledTextField(msg("table.request.type"));
//	private DisabledTextArea descriptionTextArea = new DisabledTextArea(msg("table.request.description"));
//	private DisabledTextField sourceTextField = new DisabledTextField("Источник для ТОиР");
//	private DisabledTextField methodTextField = new DisabledTextField("Способ формирования");
//	private DisabledTextField workTextField = new DisabledTextField("Связанные работы");
//	private boolean isOnLoading;
//
//	WorkOrderRequestInfoPanel(WorkDto workOrder, WorkOrderWindow workOrderWindow) {
//		this.workOrder = workOrder;
//		this.workOrderWindow = workOrderWindow;
//		isOnLoading = true;
//		if (workOrder.getId() != null) {
//			List<RequestDto> workRequests = BrigadesUI.getRestTemplate().getWorkRequests(workOrder.getId());
//			if (workRequests.size() > 0)
//				request = workRequests.get(0);
//		}
//		build();
//		isOnLoading = false;
//	}
//
//	private void build() {
//		initFields();
//		buildContent();
//	}
//
//	private void initFields() {
//		List<RequestDto> requests = BrigadesUI.getRestTemplate().getRequests();
//		//sourceComboBox.setItemCaptionGenerator(RequestDto::getDescription);
//		sourceComboBox.setItems(requests);
//		sourceComboBox.setPlaceholder("Выберите заявку");
//		workOrderWindow.addWindowModeChangeListener(event -> sourceComboBox.setPopupWidth(
//				event.getWindowMode().equals(WindowMode.NORMAL) ? "300%" : "100%"));
//		sourceComboBox.addValueChangeListener($ -> onChangeRequest());
//		if (workOrder != null && request != null) {
//			sourceComboBox.setValue(request);
//		}
//	}
//
//	private void buildContent() {
//		clearRequestSelectionButton.addClickListener((Button.ClickListener) $ -> onClearRequest());
//		clearRequestSelectionButton.setDescription(msg("button.clearfield.description", new Object[]{sourceComboBox.getCaption()}));
//		clearRequestSelectionButton.setEnabled(!sourceComboBox.isEmpty());
//
//		openRequestButton.addClickListener((Button.ClickListener) $ -> onOpenRequest());
//		openRequestButton.setEnabled(!sourceComboBox.isEmpty());
//
//		HorizontalLayout buttons = CompsUtil.getHorizontalWrapperNoMargin(clearRequestSelectionButton, openRequestButton);
//		buttons.setWidthUndefined();
//		//GridLayout line = new GridLayout(2,1);
//		HorizontalLayout line = new HorizontalLayout();
//		line.addComponent(sourceComboBox);
//		line.addComponent(buttons);
//		line.setComponentAlignment(buttons, Alignment.BOTTOM_LEFT);
//		line.setExpandRatio(sourceComboBox, 1);
//		line.setSpacing(true);
//		line.setSizeFull();
//		HorizontalLayout line2 = CompsUtil.getHorizontalWrapperNoMargin(numberTextField, typeTextField);
//		HorizontalLayout line3 = CompsUtil.getHorizontalWrapperNoMargin(sourceTextField, methodTextField);
//		VerticalLayout layout = CompsUtil.getVerticalWrapperWithMargin(line, line2, descriptionTextArea, line3, workTextField);
//		setMargin(false);
//		setSpacing(false);
//		addComponent(CompsUtil.getGroupWrapper("Информация об источнике работы", CompsUtil.getVerticalWrapperNoMargin(layout)));
//	}
//
//	private void onChangeRequest() {
//		request = sourceComboBox.getValue();
//		if (request != null) {
//			numberTextField.setValue(request.getNumber());
//			sourceTextField.setValue(request.getSource());
//			typeTextField.setValue(request.getProblemCode());
//			descriptionTextArea.setValue(request.getDescription());
//		} else {
//			numberTextField.clear();
//			sourceTextField.clear();
//			typeTextField.clear();
//			descriptionTextArea.clear();
//		}
//		clearRequestSelectionButton.setEnabled(!sourceComboBox.isEmpty());
//		openRequestButton.setEnabled(!sourceComboBox.isEmpty());
//	}
//
//	private void onClearRequest() {
//		sourceComboBox.clear();
//	}
//
//	private void onOpenRequest() {
//		RequestLayout.showRequest(request);
//	}
//
//	public RequestDto getRequest() {
//		return request;
//	}
//}
