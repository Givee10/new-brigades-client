package ru.telros.brigades.client.ui.eventlog;

import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class EventLogNotification extends Notification {

	public EventLogNotification(String caption, String style) {
		super(caption, "", Type.HUMANIZED_MESSAGE);
		setDelayMsec(20000);
		setPosition(Position.BOTTOM_RIGHT);
		setStyleName(style);
	}

	public static EventLogNotification open(String caption, String style) {
		EventLogNotification n = new EventLogNotification(caption, style);
		n.show(Page.getCurrent());
		return n;
	}

}
