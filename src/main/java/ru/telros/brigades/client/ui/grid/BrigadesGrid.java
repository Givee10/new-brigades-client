package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.shared.ui.grid.ScrollDestination;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.AbstractEntity;
import ru.telros.brigades.client.dto.AbstractEntityGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.ui.grid.GridColumn.COLUMN_WIDTH;
import static ru.telros.brigades.client.ui.grid.GridColumn.LAYOUT_WIDTH;

public abstract class BrigadesGrid<T extends AbstractEntity> extends Grid<T> implements HasLogger {
	private final List<GridColumn<T>> columns;
	private final AbstractEntityGroup<T> entityGroup;
	private final ListDataProvider<T> dataProvider;

	public BrigadesGrid(List<GridColumn<T>> columns, List<T> items) {
		this.columns = columns;
		this.entityGroup = new AbstractEntityGroup<>(items);
		this.dataProvider = new ListDataProvider<T>(entityGroup.values()) {
			@Override
			public Object getId(T item) {
				return item.getId();
			}
		};
		setSizeFull();
		setDataProvider(dataProvider);
		setItems(entityGroup.values());
		setSelectionMode(SelectionMode.SINGLE);
		setColumnReorderingAllowed(true);
		setColumnResizeMode(ColumnResizeMode.ANIMATED);
		initColumns();
		/*addItemClickListener(itemClick -> {
			if (itemClick.getMouseEventDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT)) {
				if (itemClick.getMouseEventDetails().isDoubleClick()) {
					onDoubleClick(itemClick.getItem(), itemClick.getRowIndex());
				} else {
					onClick(itemClick.getItem(), itemClick.getRowIndex());
				}
			}
		});*/
		addItemClickListener(event -> {
			if (event.getMouseEventDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT)) {
				if (event.getMouseEventDetails().isDoubleClick()) {
					onDoubleClick(event.getItem(), event.getColumn());
				} else {
					onClick(event.getItem(), event.getColumn());
				}
			}
		});
	}

	private void initColumns() {
		for (GridColumn<T> column : columns) {
			if (column.isComponent()) {
				Column<T, ?> col = addComponentColumn((ValueProvider<T, Component>) column.getValueProvider())
						.setId(column.getId()).setCaption(column.getCaption());
				col.setWidth(LAYOUT_WIDTH);
				col.setResizable(false);
				col.setSortable(false);
				//((Column<T, Component>) col).setRenderer(new ComponentRenderer());
			} else {
				Column<T, ?> col = addColumn(column.getValueProvider()).setId(column.getId()).setCaption(column.getCaption());
				col.setWidth(COLUMN_WIDTH);
				col.setDescriptionGenerator(column.getDescriptionGenerator());
				if (column.isDate())
					col.setComparator(new BrigadesDateComparator<>(column.getValueProvider()));
				getDefaultHeaderRow().getCell(col).setDescription(column.getCaption());
			}
		}
	}

	public void removeFilterRow() {
		removeHeaderRow(1);
	}

	public void hideColumns(String... columnIds) {
		for (String id : columnIds) {
			getColumn(id).setHidden(true);
		}
	}
	public void hideColumns(List<String> columnIds) {
		for (String id : columnIds) {
			getColumn(id).setHidden(true);
		}
	}
	public void hideColumns(){
		getColumns().stream().forEach(e->{
			e.setHidden(true);
		});
	}
	public void showColumns(){
		getColumns().stream().forEach(e->{
			e.setHidden(false);
		});
	}
	//Оставить показанными только колонки, переданные в параметре в параметре
	public void leaveShownOnly(Set<GridColumn<T>> columnsToShow) {
		List<GridColumn<T>> columnsToHide = new ArrayList<>(columns);
		columnsToHide.removeAll(columnsToShow);
		hideColumns(columnsToHide.stream().map(e->{return e.getId();}).collect(Collectors.toList()));
		showColumns(columnsToShow.stream().map(e->{return e.getId();}).collect(Collectors.toList()));
	}
	public void showColumns(List<String> columnIds) {
		for (String id : columnIds) {
			getColumn(id).setHidden(false);
		}
	}

	public void showColumns(String... columnIds) {
		for (String id : columnIds) {
			getColumn(id).setHidden(false);
		}
	}

	public void update(List<T> items) {
		entityGroup.update(items);
		dataProvider.refreshAll();
	}
	public List<Grid.Column<T, ?>> getShownColumns() {
		return getColumns().stream().filter(e->e.isHidden()==false).collect(Collectors.toList());
	}

	public void refreshItem(T item) {
		entityGroup.update(item);
		dataProvider.refreshItem(item);
	}
	public void refreshItem(Long itemId) {
		T item = entityGroup.findById(itemId);
		entityGroup.update(item);
		dataProvider.refreshItem(item);
	}
	public T getItem(Long itemId) {
		T item = entityGroup.findById(itemId);
		return item;
	}
	public void scrollTo(T item) {
		if (item != null) {
			int i = getItems().indexOf(item);
			if (i >= 0)
				scrollTo(i, ScrollDestination.MIDDLE);
		}
	}

	public void scrollTo(Long itemId) {
		scrollTo(entityGroup.findById(itemId));
	}

	public void select(Long itemId) {
		select(entityGroup.findById(itemId));
	}

	public void refreshAll() {
		dataProvider.refreshAll();
	}

	public T getSelectedRow() {
		return getSelectedItems().stream().findFirst().orElse(null);
	}

	public List<T> getItems() {
		return new ArrayList<>(dataProvider.getItems());
	}

	protected abstract void onDoubleClick(final T entity, final Column column);

	protected abstract void onClick(final T entity, final Column column);
	protected void onDoubleClick(final T entity, final int rowIndex){

	}
	protected  void onClick(final T entity, final int rowIndex){}

	@Override
	public void setItems(Collection<T> items) {
		//super.setItems(items);
		update(new ArrayList<>(items));
	}
}
