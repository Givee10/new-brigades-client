package ru.telros.brigades.client.ui.eventlog;

import com.google.common.eventbus.Subscribe;
import com.vaadin.server.Resource;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import de.codecamp.vaadin.webnotifications.WebNotifications;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.EventWithActionDto;
import ru.telros.brigades.client.dto.PagesDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.PlayAudioEvent;
import ru.telros.brigades.client.event.UpdateBadgeEvent;
import ru.telros.brigades.client.event.eventlog.UpdateEventLogEvent;
import ru.telros.brigades.client.view.DashboardView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Панель для отображения последних событий и действий на событие
 */
public class EventsPanel extends Panel {
	private static final int DEFAULT_BUTTON_WIDTH = 100;
	private static final String STYLE_EVENT_COUNTER = "et_counter";
	private static final String STYLE_EVENT_TEXT = "et_text";
	private static final String STYLE_EVENT_ICON = "et_icon";
	private static final String STYLE_EVENT_PANEL = "et_panel";

	private final List<EventWithActionDto> eventList = Collections.synchronizedList(new ArrayList<>());

	private final HorizontalLayout layout = new HorizontalLayout(); // панель
	private final CssLayout actionBar = new CssLayout(); // панель кнопок-действий
	private final MenuBar navBar = new MenuBar(); // панель навигации "вверх-вниз"
	private final Label eventCounter = new Label(); // счетчик событий
	private final Label eventIcon = new Label(); // иконка события
	private final Label eventText = new Label(); // текст события
	private EventWithActionDto lastSelectedEvent; // проверка
	private int lastSelectedIndex = -1; // индекс последнего выбранного элемента
	private final MenuBar.MenuItem btnDown = navBar.addItem("", BrigadesTheme.ICON_MOVE_DOWN, item -> moveDown()); // кнопка вниз
	private final MenuBar.MenuItem btnUp = navBar.addItem("", BrigadesTheme.ICON_MOVE_UP, item -> moveUp()); // кнопка вверх

	public EventsPanel() {
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		setStyleName(STYLE_EVENT_PANEL);
		layout.setSpacing(true);
		layout.setSizeFull();
		layout.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

		// счетчик событий
		eventCounter.setWidth(25, Unit.PIXELS);
		eventCounter.setHeight(25, Unit.PIXELS);
		eventCounter.addStyleName(STYLE_EVENT_COUNTER);
		layout.addComponent(eventCounter);

		// иконка события
		eventIcon.setWidth(25, Unit.PIXELS);
		eventIcon.setHeight(25, Unit.PIXELS);
		eventIcon.addStyleName(STYLE_EVENT_ICON);
		eventIcon.setContentMode(ContentMode.HTML);
		layout.addComponent(eventIcon);

		// описание события
		eventText.addStyleName(STYLE_EVENT_TEXT);
		layout.addComponent(eventText);

		// кнокпи-действия
		actionBar.setHeight(100, Unit.PERCENTAGE);
		Button button = new Button("", BrigadesTheme.ICON_OK);
		button.setDescription("Квитировать событие");
		button.setWidth(DEFAULT_BUTTON_WIDTH, Unit.PIXELS);
		button.addClickListener($ -> doAcceptMessage());
		actionBar.addComponent(button);
		layout.addComponent(actionBar);

		// навигация "ввверх-вниз"
		btnUp.setDescription("Предыдущее событие");
		btnDown.setDescription("Следующее событие");
		layout.addComponent(navBar);

		setContent(layout);
		layout.setExpandRatio(eventText, 1);
		layout.setVisible(false);

		//updateAll();
	}

	/**
	 * Квитирование сообщения
	 */
	private void doAcceptMessage() {
		EventLogLayout.acceptEvent(getSelectedEvent());
		updateAll();
	}

	/**
	 * Перемещение вверх по списку
	 */
	private void moveUp() {
		if (lastSelectedIndex > 0 && eventList.size() > 0) {
			selectEvent(eventList.get(lastSelectedIndex - 1));
		}
	}

	/**
	 * Перемещение вниз по списку
	 */
	private void moveDown() {
		int size = eventList.size();
		if (size > 0 && lastSelectedIndex + 1 < size) {
			selectEvent(eventList.get(lastSelectedIndex + 1));
		}
	}

	/**
	 * Выделенние события и обновление счетчика событий
	 */
	public void updateSelection() {
		EventWithActionDto e = null;
		if (eventList.size() == 0) {
			// список событий пуст
			lastSelectedEvent = null;
			lastSelectedIndex = -1;
		}
		if (lastSelectedEvent == null || !eventList.contains(lastSelectedEvent)) {
			// ничего не было выделено - выделение верхнего события
			lastSelectedEvent = null;
			if (eventList.size() > 0) {
				e = eventList.get(0);
			}
		} else if (lastSelectedEvent != null) {
			// ранее было выделено событие
			if (lastSelectedIndex == 0) {
				if (eventList.size() > 0) {
					e = eventList.get(0);
				}
			} else {
				e = lastSelectedEvent;
			}
		}
		selectEvent(e);
		int size = eventList.size();
		layout.setVisible(size >= 0);
		eventCounter.setValue(String.valueOf(size));
		BrigadesEventBus.post(new UpdateBadgeEvent(DashboardView.BADGE, String.valueOf(size)));
	}

	/**
	 * Добавление событий в список
	 */
	public void addEvents(List<EventWithActionDto> events) {
		events.forEach(e -> addEvent(e, false));
		updateSelection();
	}

	/**
	 * Добавление события в список событий
	 *
	 * @param event
	 */
	public void addEvent(EventWithActionDto event) {
		addEvent(event, true);
	}

	/**
	 * Добавление события в начало списка
	 *
	 * @param event
	 * @param isUpdateSelection
	 */
	private void addEvent(EventWithActionDto event, boolean isUpdateSelection) {
		if (!eventList.contains(event)) {
			eventList.add(0, event);
			WebNotifications.create(BrigadesUI.getCurrent(), "Уведомление")
					.body(event.getMessage()).icon("theme://img/telros_logo_small.png").tag("update")
					.onClick(() -> Notification.show("Переход от уведомления")).show();
			if (EventConstants.isSoundEnable(event)) {
				BrigadesEventBus.post(new PlayAudioEvent(new ThemeResource("sound/notify.wav")));
			}
		}
		if (isUpdateSelection) {
			updateSelection();
		}
	}

	/**
	 * Удаление события из списка
	 *
	 * @param event
	 */
	public void deleteEvent(EventWithActionDto event) {
		deleteEvent(event, true);
	}

	protected void deleteEvent(EventWithActionDto event, boolean isUpdateSelection) {
		// если удаляемое событие - выделено в текущий момент
		if (lastSelectedEvent.equals(event)) {
			lastSelectedEvent = null;
		}
		// удаление и обновление
		eventList.remove(event);
		if (isUpdateSelection) {
			updateSelection();
		}
	}

	/**
	 * Очистка очереди событий
	 */
	public void clearEvents() {
		lastSelectedEvent = null;
		lastSelectedIndex = -1;
		eventList.clear();
		updateSelection();
	}

	/**
	 * Обновление кнопок-действий
	 */
	private void updateActions(EventWithActionDto event) {
		actionBar.setWidth(DEFAULT_BUTTON_WIDTH * actionBar.getComponentCount(), Unit.PIXELS);
		actionBar.setVisible(event != null);
	}

	/**
	 * Возвращает текущее отображаемое событие
	 */
	private EventWithActionDto getSelectedEvent() {
		return (eventList.size() > 0) ? lastSelectedEvent : null;
	}

	/**
	 * Выделение события event в списке событий
	 *
	 * @param event
	 */
	public void selectEvent(EventWithActionDto event) {
		// сохранение индекса последнего выделенного элемента
		if (event != null) {
			lastSelectedIndex = eventList.indexOf(event);
		}
		// обновление кнопок навигации
		int size = eventList.size();
		if (size == 0) {
			btnUp.setEnabled(false);
			btnDown.setEnabled(false);
		} else {
			btnUp.setEnabled(lastSelectedIndex > 0);
			btnDown.setEnabled(lastSelectedIndex + 1 < size);
		}
		// проверка необходимости обновления
		if (lastSelectedEvent != null && lastSelectedEvent.equals(event)) return;
		// обновление информации о событии
		lastSelectedEvent = event;
		String caption = null;
		String style = null;
		//String style = EventInfo.getEventTypeStyle(EventInfo.TYPE_NORMAL);
		String icon = null;
		Resource sound = null;
		if (event != null) {
			/*caption = EventInfo.getEventDescription(event, true);
			style = EventInfo.getEventStyle(event);
			EventItem eventItem = EventLogLayout.findEventItem(event);
			if (eventItem != null && eventItem.getIcon() != null) {
				VaadinIcons fa = VaadinIcons.valueOf(eventItem.getIcon());
				if (fa != null) {
					icon = fa.getHtml();
				}
			}
			// воспроизведение связанного звука
			if (eventItem != null) {
				EventType eventType = EventLogLayout.findEventType(eventItem);
				if (eventType != null && eventType.getSound() != null) {
					try {
						sound = new ThemeResource(eventType.getSound());
					} catch (Exception e) {
						sound = null;
					}
				}
			}*/
			caption = EventConstants.createMessage(event);
		}
		updateActions(event);
		layout.removeStyleName(layout.getStyleName());
		layout.setStyleName(style);
		eventIcon.setValue(icon);
		eventText.setValue(caption);
	}

	/**
	 * Обрабатываем события обновления журнала
	 *
	 * @param event
	 */
	@Subscribe
	public void handleUpdateEventLog(UpdateEventLogEvent event) {
		updateAll();
	}

	/**
	 * Обновление всего списка событий
	 */
	public void updateAll() {
		eventList.clear();
		getEvents().forEach(this::addEvent);
		updateSelection();
	}

	/**
	 * Получение последних неквитированных событий из БД
	 *
	 * @return
	 */
	public Collection<EventWithActionDto> getEvents() {
		Long countEvents = BrigadesUI.getRestTemplate().countEvents();
		if (countEvents != null && countEvents != 0) {
			PagesDto<EventWithActionDto> events = BrigadesUI.getRestTemplate().getEventsWithActions(0, countEvents.intValue());
			return EventConstants.filterEventsForPanel(events.getCurrentContent());
		}
		return Collections.emptyList();
	}

	/**
	 * Handler for UserEventLogEvent
	 *
	 * @param event
	 *//*
	@Subscribe
	public void handleUserEventLogEvent(final UserEventLogEvent event) {
		if ("refresh".equalsIgnoreCase(event.getEvent())) {
			updateAll();
		}
	}*/

}
