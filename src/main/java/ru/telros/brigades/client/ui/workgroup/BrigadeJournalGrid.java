package ru.telros.brigades.client.ui.workgroup;

import ru.telros.brigades.client.dto.BrigadeJournalDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.ArrayList;
import java.util.List;

public class BrigadeJournalGrid extends BrigadesGrid<BrigadeJournalDto> {
	public BrigadeJournalGrid() {
		this(new ArrayList<>());
	}

	public BrigadeJournalGrid(List<BrigadeJournalDto> items) {
		super(WorkGroupColumn.getJournalColumns(), items);
	}

	@Override
	protected void onClick(BrigadeJournalDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(BrigadeJournalDto entity, Column column) {
		if (entity != null) {
			WorkGroupLayout.showWorkGroup(entity);
		}
	}
}
