package ru.telros.brigades.client.ui.component;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.ComboBox;

public class CustomComboBox<T> extends ComboBox<T> {
	public CustomComboBox() {
		this(null);
	}

	public CustomComboBox(String caption) {
		super(caption);
		setWidth(100, Sizeable.Unit.PERCENTAGE);
		setEmptySelectionAllowed(false);
		setTextInputAllowed(true);
		setResponsive(true);
	}
}
