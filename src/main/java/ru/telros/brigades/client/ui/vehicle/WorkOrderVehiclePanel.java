package ru.telros.brigades.client.ui.vehicle;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.EquipmentWorkDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.window.YesNoWindow;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

public class WorkOrderVehiclePanel extends HorizontalLayout {
	private WorkDto workOrder;
	private WorkOrderVehicleGrid vehicleGrid;
	private List<EquipmentWorkDto> vehicleList;
	private GridContextMenu<EquipmentWorkDto> contextMenu;

	public WorkOrderVehiclePanel(WorkDto workOrder, List<EquipmentWorkDto> vehicleList) {
		this.workOrder = workOrder;
		this.vehicleList = vehicleList;
		this.vehicleGrid = new WorkOrderVehicleGrid(VehicleColumn.getWorkVehicleColumns(), vehicleList);
		this.vehicleGrid.setHeight(300, Unit.PIXELS);
		//this.vehicleGrid.setItems(this.workOrder.getVehicles());
		this.contextMenu = new GridContextMenu<>(vehicleGrid);
		build();
	}

	private void build() {
		BrigadesEventBus.register(this);
		setSizeFull();
		setMargin(false);
		setSpacing(false);
		buildTable();
		buildButtons();
	}

	private void buildTable() {
		addComponent(vehicleGrid);
		setExpandRatio(vehicleGrid, 1);
		contextMenu.addGridHeaderContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridBodyContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridFooterContextMenuListener(event -> selectGridItem(event.getItem()));
	}

	private void buildButtons() {
		final VerticalLayout content = new VerticalLayout();
		content.setWidthUndefined();
		content.setMargin(true);
		content.setSpacing(true);
		Button addButton = new Button();
		addButton.setDescription("Создать");
		addButton.setIcon(BrigadesTheme.ICON_ADD);
		addButton.addClickListener($ -> addVehicle());
		addButton.setSizeFull();
		content.addComponent(addButton);

		Button editButton = new Button();
		editButton.setDescription("Изменить");
		editButton.setIcon(BrigadesTheme.ICON_EDIT);
		editButton.addClickListener($ -> editVehicle());
		editButton.setSizeFull();
		//content.addComponent(editButton);

		Button deleteButton = new Button();
		deleteButton.setDescription("Удалить");
		deleteButton.setIcon(BrigadesTheme.ICON_DELETE);
		deleteButton.addClickListener($ -> deleteVehicle());
		deleteButton.setSizeFull();
		content.addComponent(deleteButton);
		addComponent(content);
	}

	private void selectGridItem(EquipmentWorkDto item) {
		contextMenu.removeItems();
		if (item != null) {
			vehicleGrid.select(item);

			MenuBar.Command commandFollow = (MenuBar.Command) menuItem -> showOnMap(item);
			contextMenu.addItem(msg("menu.gotoaddress"), BrigadesTheme.ICON_MAP, commandFollow);
		}
	}

	private static void showOnMap(EquipmentWorkDto item) {
		VehicleLayout.showOnMap(item.getType(), item.getModel(), item.getNumber());
	}

	private void addVehicle() {
		Long id = workOrder.getId();
		if (id != null) {
			EquipmentWorkDto equipmentWorkDto = new EquipmentWorkDto();
			equipmentWorkDto.setIdWork(id);
			WorkOrderVehicleWindow.open(equipmentWorkDto, BrigadesUI.getRestTemplate().getEquipments());
		} else {
			Notification.show("Спецтехнику можно добавить только после сохранения работы", Notification.Type.ERROR_MESSAGE);
		}
	}

	private void editVehicle() {
		if (vehicleGrid.getSelectedRow() != null) {
			EquipmentWorkDto vehicle = vehicleGrid.getSelectedRow();
			vehicleGrid.deselectAll();
			WorkOrderVehicleWindow.open(vehicle, BrigadesUI.getRestTemplate().getEquipments());
		} else {
			Notification.show("Спецтехника не выбрана", Notification.Type.WARNING_MESSAGE);
		}
	}

	private void deleteVehicle() {
		if (vehicleGrid.getSelectedRow() != null) {
			EquipmentWorkDto vehicle = vehicleGrid.getSelectedRow();
			vehicleGrid.deselectAll();
			String prompt = "Вы действительно хотите удалить " + vehicle.getNumber() + " из работы?";
			YesNoWindow confirmationDialog = YesNoWindow.open("Внимание", prompt, true);
			confirmationDialog.addCloseListener((Window.CloseListener) e -> {
				if (((YesNoWindow)e.getWindow()).isModalResult()) {
					//vehicleList.remove(vehicle);
					//vehicleGrid.update(vehicleList);
					BrigadesUI.getRestTemplate().detachVehicleFromWork(vehicle);
					WorkOrderLayout.refresh();
					Notification.show("Спецтехника " + vehicle.getNumber() + " удалена", Notification.Type.WARNING_MESSAGE);
				}
			});
		}
	}

	public List<EquipmentWorkDto> getVehicles() {
		return vehicleList;
	}

	public void setVehicles(List<EquipmentWorkDto> vehicles) {
		vehicleList.clear();
		vehicleList.addAll(vehicles);
		vehicleGrid.update(vehicleList);
	}
}
