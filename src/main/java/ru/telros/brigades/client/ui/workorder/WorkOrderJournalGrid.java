package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.dto.WorkJournalDto;
import ru.telros.brigades.client.ui.grid.JournalGrid;
import ru.telros.brigades.client.ui.grid.WorkDataProvider;

public class WorkOrderJournalGrid extends JournalGrid<WorkJournalDto> {
	private static WorkDataProvider dataProvider = new WorkDataProvider();

	public WorkOrderJournalGrid() {
		super(WorkOrderConstants.getJournalColumns(), dataProvider);
		setStyleGenerator(row -> (row != null) ? WorkOrderConstants.getEventStyle(row) : null);
	}

	@Override
	protected void onDoubleClick(WorkJournalDto item, Column column) {
		if (item != null) {
			WorkDto work = BrigadesUI.getRestTemplate().getWork(item.getId());
			WorkOrderLayout.showWorkOrder(work);
		}
	}
}
