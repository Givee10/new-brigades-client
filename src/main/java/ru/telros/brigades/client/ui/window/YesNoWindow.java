package ru.telros.brigades.client.ui.window;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.ui.*;

/**
 * Окно с кнопками "Да" и "Нет"
 */
public class YesNoWindow extends Window {
	private static final int DEFAULT_WIDTH_PIXELS = 600;
	private static final int DEFAULT_HEIGHT_PIXELS = -1;
	private static final String BUTTON_YES = "Да";
	private static final String BUTTON_NO = "Нет";
	private boolean modalResult = false;

	public YesNoWindow(final String caption, final String prompt, final boolean contentAsHtml) {
		setCaption(caption);
		setModal(true);
		setDraggable(false);
		setResizable(false);
		setWindowMode(WindowMode.NORMAL);
		setClosable(true);
		removeAllCloseShortcuts();
		setWidth(DEFAULT_WIDTH_PIXELS, Unit.PIXELS);
		setHeight(DEFAULT_HEIGHT_PIXELS, Unit.PIXELS);

		VerticalLayout content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);

		// Описание
		Label label = new Label(prompt, contentAsHtml ? ContentMode.HTML : ContentMode.TEXT);
		label.setWidth(100, Unit.PERCENTAGE);
		content.addComponent(label);
		content.setExpandRatio(label, 1);

		// Кнопки
		HorizontalLayout buttons = new HorizontalLayout();
		buttons.setSpacing(true);
		buttons.setMargin(true);
		buttons.setWidth(100, Unit.PERCENTAGE);
		buttons.setDefaultComponentAlignment(Alignment.BOTTOM_RIGHT);
		Button button = new Button(BUTTON_YES);
		button.addClickListener((Button.ClickListener) clickEvent -> {
			modalResult = true;
			close();
		});
		buttons.addComponent(button);
		button = new Button(BUTTON_NO);
		button.addClickListener((Button.ClickListener) clickEvent -> {
			modalResult = false;
			close();
		});
		buttons.addComponent(button);
		buttons.setMargin(true);
		Label expander = new Label();
		buttons.addComponent(expander, 0);
		buttons.setExpandRatio(expander, 1);
		content.addComponent(buttons);

		setContent(content);
	}

	public static YesNoWindow open(String caption, String prompt, boolean captionAsHtml) {
		YesNoWindow window = new YesNoWindow(caption, prompt, captionAsHtml);
		UI.getCurrent().addWindow(window);
		window.center();
		return window;

	}

	public static YesNoWindow open(String caption, String prompt) {
		return open(caption, prompt, false);
	}

	public boolean isModalResult() {
		return modalResult;
	}
}
