package ru.telros.brigades.client.ui.maplayout;

import java.io.Serializable;

/**
 * Хранение информации о маркере
 */
@SuppressWarnings("serial")
public class Marker extends MapElement implements Serializable {

	// Размер картинки [YandexMaps]
	private int imageSizeX, imageSizeY;
	// Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки) [YandexMaps]
	private int imageOffsetX, imageOffsetY;
	// Наименование маркера (текст внутри) [YandexMaps]
	private String caption;
	// Описание маркера (хинт) [YandexMaps, GoogleMaps]
	private String description;
	// Иконка маркера (URI) [YandexMaps, GoogleMaps]
	private String uri;
	// Коорднаты маркера [YandexMaps, GoogleMaps]
	private Coordinates coordinates = new Coordinates();
	// Тип маркера и цвет [YandexMaps]
	private String iconPreset;
	private String iconColor;
	// Возможность перемещения маркера [YandexMaps, GoogleMaps]
	private boolean draggable = false;

	public Marker() {
		super();
	}

	public Marker(double latitude, double longitude) {
		super();
		this.coordinates.set(latitude, longitude);
	}

	public Marker(String caption, String description, String uri, double latitude, double longitude) {
		super(caption, description);
		this.uri = uri;
		this.coordinates = new Coordinates(latitude, longitude);
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public String getIconPreset() {
		return iconPreset;
	}

	public void setIconPreset(String iconPreset) {
		this.iconPreset = iconPreset;
	}

	public String getIconColor() {
		return iconColor;
	}

	public void setIconColor(String iconColor) {
		this.iconColor = iconColor;
	}

	public boolean isDraggable() {
		return draggable;
	}

	public void setDraggable(boolean draggable) {
		this.draggable = draggable;
	}

	public int getImageSizeX() {
		return imageSizeX;
	}

	public void setImageSizeX(int imageSizeX) {
		this.imageSizeX = imageSizeX;
	}

	public void setImageSize(int imageSizeX, int imageSizeY) {
		this.imageSizeX = imageSizeX;
		this.imageSizeY = imageSizeY;
	}

	public int getImageSizeY() {
		return imageSizeY;
	}

	public void setImageSizeY(int imageSizeY) {
		this.imageSizeY = imageSizeY;
	}

	public void setImageOffset(int imageOffsetX, int imageOffsetY) {
		this.imageOffsetX = imageOffsetX;
		this.imageOffsetY = imageOffsetY;
	}

	public int getImageOffsetX() {
		return imageOffsetX;
	}

	public void setImageOffsetX(int imageOffsetX) {
		this.imageOffsetX = imageOffsetX;
	}

	public int getImageOffsetY() {
		return imageOffsetY;
	}

	public void setImageOffsetY(int imageoffsetY) {
		this.imageOffsetY = imageoffsetY;
	}

	@Override
	protected String getPrefix() {
		return "pm_";
	}


}
