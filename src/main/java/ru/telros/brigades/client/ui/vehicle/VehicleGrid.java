package ru.telros.brigades.client.ui.vehicle;

import ru.telros.brigades.client.dto.VehicleDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class VehicleGrid extends BrigadesGrid<VehicleDto> {
	public VehicleGrid(List<GridColumn<VehicleDto>> gridColumns, List<VehicleDto> items) {
		super(gridColumns, items);
	}

	@Override
	protected void onClick(VehicleDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(VehicleDto entity, Column column) {

	}
}
