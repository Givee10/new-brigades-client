package ru.telros.brigades.client.ui.workgroup;

import com.vaadin.ui.VerticalLayout;
import ru.telros.brigades.client.ui.workorder.WorkOrderGrid;

@SuppressWarnings("serial")
public class WorkGroupWorkOrdersPanel extends VerticalLayout {
	private WorkOrderGrid workOrderGrid;

	WorkGroupWorkOrdersPanel(WorkOrderGrid workOrderGrid) {
		this.workOrderGrid = workOrderGrid;
		build();
	}

	private void build() {
		setSizeFull();
		setSpacing(false);
		setMargin(false);
		buildTable();
	}

	private void buildTable() {
		addComponent(workOrderGrid);
		setExpandRatio(workOrderGrid, 1);
	}
}
