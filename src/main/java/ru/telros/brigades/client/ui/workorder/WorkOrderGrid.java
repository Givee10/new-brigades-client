package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class WorkOrderGrid extends BrigadesGrid<WorkDto> {
	public WorkOrderGrid(List<GridColumn<WorkDto>> columns, List<WorkDto> items) {
		super(columns, items);
		// Раскраска строк таблицы
		setStyleGenerator(rowReference -> (rowReference != null) ? WorkOrderConstants.getEventStyle(rowReference) : null);
	}

	@Override
	protected void onClick(WorkDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(WorkDto entity, Column column) {
		if (entity != null) {
			WorkDto work = BrigadesUI.getRestTemplate().getWork(entity.getId());
			WorkOrderLayout.showWorkOrder(work);
		}
	}

}
