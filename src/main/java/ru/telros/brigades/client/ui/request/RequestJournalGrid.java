package ru.telros.brigades.client.ui.request;

import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.RequestJournalDto;
import ru.telros.brigades.client.ui.grid.JournalGrid;
import ru.telros.brigades.client.ui.grid.RequestDataProvider;

public class RequestJournalGrid extends JournalGrid<RequestJournalDto> {
	private static RequestDataProvider dataProvider = new RequestDataProvider();

	public RequestJournalGrid() {
		super(RequestConstants.getJournalColumns(), dataProvider);
		setStyleGenerator(row -> (row != null) ? RequestConstants.getStyle(row.getStatus()) : null);
	}

	@Override
	protected void onDoubleClick(RequestJournalDto item, Column column) {
		if (item != null) {
			RequestDto request = BrigadesUI.getRestTemplate().getRequest(item.getId());
			RequestLayout.showRequest(request);
		}
	}
}
