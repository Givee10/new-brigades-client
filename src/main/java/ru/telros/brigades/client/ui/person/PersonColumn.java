package ru.telros.brigades.client.ui.person;

import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
import ru.telros.brigades.client.dto.EmployeeDto;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public abstract class PersonColumn {
	public static final GridColumn<EmployeeBrigadeDto> ID = GridColumn.createColumn("id", "№", EmployeeBrigadeDto::getIdEmployee);
	public static final GridColumn<EmployeeBrigadeDto> NAME = GridColumn.createColumn("name", "Полное имя", EmployeeBrigadeDto::getFullName);
	public static final GridColumn<EmployeeBrigadeDto> PHONE = GridColumn.createColumn("phone", "Телефон", EmployeeBrigadeDto::getPhone);
	public static final GridColumn<EmployeeBrigadeDto> POST = GridColumn.createColumn("post", "Должность", EmployeeBrigadeDto::getPost);
	public static final GridColumn<EmployeeBrigadeDto> GROUP = GridColumn.createColumn("group", "Бригада", EmployeeBrigadeDto::getIdBrigade);
	public static final GridColumn<EmployeeBrigadeDto> ABSENCE = GridColumn.createColumn("absence", "Причина отсутствия", EmployeeBrigadeDto::getAbsenceReason);

	public static final GridColumn<EmployeeDto> ID_ = GridColumn.createColumn("id", "№", EmployeeDto::getId);
	public static final GridColumn<EmployeeDto> LAST_NAME = GridColumn.createColumn("lastName", "Фамилия", EmployeeDto::getLastName);
	public static final GridColumn<EmployeeDto> FIRST_NAME = GridColumn.createColumn("firstName", "Имя", EmployeeDto::getFirstName);
	public static final GridColumn<EmployeeDto> MIDDLE_NAME = GridColumn.createColumn("middleName", "Отчество", EmployeeDto::getMiddleName);

	public static List<GridColumn<EmployeeBrigadeDto>> getMainColumns() {
		return Arrays.asList(ID, NAME, PHONE, POST, ABSENCE, GROUP);
	}

	public static List<GridColumn<EmployeeDto>> getTestColumns() {
		return Arrays.asList(ID_, LAST_NAME, FIRST_NAME, MIDDLE_NAME);
	}
}
