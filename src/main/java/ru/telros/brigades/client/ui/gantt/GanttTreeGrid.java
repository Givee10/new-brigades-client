package ru.telros.brigades.client.ui.gantt;

import ru.telros.brigades.client.dto.GanttItemDto;
import ru.telros.brigades.client.ui.grid.BrigadesTreeGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.ArrayList;
import java.util.List;

public class GanttTreeGrid extends BrigadesTreeGrid<GanttItemDto> {
	private static final List<GridColumn<GanttItemDto>> gridColumns = new ArrayList<GridColumn<GanttItemDto>>() {{
		add(GridColumn.createColumn("name", "Работа / этап", GanttItemDto::getName));
	}};

	public GanttTreeGrid() {
		super(gridColumns);
	}

}
