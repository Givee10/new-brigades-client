package ru.telros.brigades.client.ui.workorder;

import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import ru.telros.brigades.client.BrigadesIcons;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.dto.WorkItemDto;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public class WorkItemColumn {
	public static final GridColumn<WorkItemDto> FLAG_DONE = GridColumn.createComponentColumn("_done", "", WorkItemColumn::createDoneFlag);
	public static final GridColumn<WorkItemDto> NAME = GridColumn.createColumn("name", "Наименование работы / детали", WorkItemDto::getDescription);
	public static final GridColumn<WorkItemDto> START_PLAN = GridColumn.createDateColumn("startDatePlan", "Начало по плану", WorkItemDto::getStartDatePlan);
	public static final GridColumn<WorkItemDto> FINISH_PLAN = GridColumn.createDateColumn("finishDatePlan", "Окончание по плану", WorkItemDto::getFinishDatePlan);
	public static final GridColumn<WorkItemDto> START_FACT = GridColumn.createDateColumn("startDateFact", "Начало факт", WorkItemDto::getStartDate);
	public static final GridColumn<WorkItemDto> FINISH_FACT = GridColumn.createDateColumn("finishDateFact", "Окончание факт", WorkItemDto::getFinishDate);
	public static final GridColumn<WorkItemDto> DURATION_MAIN = GridColumn.createColumn("duration_main", "Длительность назнач.", WorkItemDto::getDuration);
	public static final GridColumn<WorkItemDto> DURATION_TWO = GridColumn.createColumn("duration_two", "Длительность рассчит. (2 чел.)", GridColumn::nullProvider);
	public static final GridColumn<WorkItemDto> DURATION_THREE = GridColumn.createColumn("duration_three", "Длительность рассчит. (3 чел.)", GridColumn::nullProvider);
	public static final GridColumn<WorkItemDto> DURATION_FOUR = GridColumn.createColumn("duration_four", "Длительность рассчит. (4 чел.)", WorkItemDto::getDurationCalc);
	public static final GridColumn<WorkItemDto> REQUIRED_MAIN = GridColumn.createComponentColumn("_required", "", WorkItemColumn::createRequired);
	public static final GridColumn<WorkItemDto> REQUIRED_PHOTO = GridColumn.createComponentColumn("_required_photo", "Ф", WorkItemColumn::createPhoto);
	public static final GridColumn<WorkItemDto> REQUIRED_PHONE = GridColumn.createComponentColumn("_required_phone", "З", WorkItemColumn::createCall);
	public static final GridColumn<WorkItemDto> REQUIRED_ACT = GridColumn.createComponentColumn("_required_act", "Д", WorkItemColumn::createAct);

	private static CssLayout createPhoto(WorkItemDto workItemDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		Label label = new Label();
		label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
		label.setContentMode(ContentMode.HTML);
		label.setSizeFull();
		String content = BrigadesTheme.ICON_ATTACHMENT.getHtml();
		if (workItemDto.getPhotoRequire() != null && workItemDto.getPhotoRequire()) {
			label.setValue(content);
		}
		layout.addComponent(label);
		if (!WorkItemDto.Status.DONE.getCode().equals(workItemDto.getStatus())) {
			layout.addLayoutClickListener(event -> {
				//logger.debug(event.getButtonName());
				if (event.getMouseEventDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT) && event.isDoubleClick()) {
					//logger.debug("workItemDto.getPhotoRequire(): " + workItemDto.getPhotoRequire());
					if (workItemDto.getPhotoRequire() == null) workItemDto.setPhotoRequire(true);
					else workItemDto.setPhotoRequire(!workItemDto.getPhotoRequire());
					//logger.debug("workItemDto.getPhotoRequire(): " + workItemDto.getPhotoRequire());
					label.setValue(workItemDto.getPhotoRequire() ? content : null);
				}
			});
		}
		return layout;
	}

	private static CssLayout createAct(WorkItemDto workItemDto) {
		//Logger logger = LoggerFactory.getLogger(WorkItemColumn.class);
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		Label label = new Label();
		label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
		label.setContentMode(ContentMode.HTML);
		label.setSizeFull();
		String content = BrigadesTheme.ICON_FILE.getHtml();
		if (workItemDto.getActRequire() != null && workItemDto.getActRequire()) {
			label.setValue(content);
		}
		layout.addComponent(label);
		if (!WorkItemDto.Status.DONE.getCode().equals(workItemDto.getStatus())) {
			layout.addLayoutClickListener(event -> {
				//logger.debug(event.getButtonName());
				if (event.getMouseEventDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT) && event.isDoubleClick()) {
					//logger.debug("workItemDto.getActRequire(): " + workItemDto.getActRequire());
					if (workItemDto.getActRequire() == null) workItemDto.setActRequire(true);
					else workItemDto.setActRequire(!workItemDto.getActRequire());
					//logger.debug("workItemDto.getActRequire(): " + workItemDto.getActRequire());
					label.setValue(workItemDto.getActRequire() ? content : null);
				}
			});
		}
		return layout;
	}

	private static CssLayout createCall(WorkItemDto workItemDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		Label label = new Label();
		label.setContentMode(ContentMode.HTML);
		label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
		label.setSizeFull();
		String content = BrigadesTheme.ICON_REQUEST.getHtml();
		if (workItemDto.getCallRequire() != null && workItemDto.getCallRequire()) {
			label.setValue(content);
		}
		layout.addComponent(label);
		if (!WorkItemDto.Status.DONE.getCode().equals(workItemDto.getStatus())) {
			layout.addLayoutClickListener(event -> {
				//logger.debug(event.getButtonName());
				if (event.getMouseEventDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT) && event.isDoubleClick()) {
					//logger.debug("workItemDto.getCallRequire(): " + workItemDto.getCallRequire());
					if (workItemDto.getCallRequire() == null) workItemDto.setCallRequire(true);
					else workItemDto.setCallRequire(!workItemDto.getCallRequire());
					//logger.debug("workItemDto.getCallRequire(): " + workItemDto.getCallRequire());
					label.setValue(workItemDto.getCallRequire() ? content : null);
				}
			});
		}
		return layout;
	}

	private static CssLayout createRequired(WorkItemDto workItemDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		return layout;
	}

	private static CssLayout createDoneFlag(WorkItemDto workItemDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (WorkItemDto.Status.DONE.getCode().equals(workItemDto.getStatus())) {
			BrigadesIcons icons = new BrigadesIcons(BrigadesTheme.ICON_OK, BrigadesTheme.COLOR_SUCCESS);
			Label label = new Label(icons.getHtml(), ContentMode.HTML);
			label.setDescription(workItemDto.getStatus());
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			label.setSizeFull();
			layout.addComponent(label);
		}
		return layout;
	}

	public static List<GridColumn<WorkItemDto>> getStagesColumns() {
		return Arrays.asList(FLAG_DONE, NAME, START_PLAN, FINISH_PLAN,
				//DURATION_MAIN, DURATION_TWO, DURATION_THREE, DURATION_FOUR,
				REQUIRED_PHOTO, REQUIRED_PHONE, REQUIRED_ACT, START_FACT, FINISH_FACT);
	}

	public static List<String> getGeneratedColumnIds() {
		return Arrays.asList(REQUIRED_PHOTO.getId(), REQUIRED_PHONE.getId(), REQUIRED_ACT.getId());
	}
}
