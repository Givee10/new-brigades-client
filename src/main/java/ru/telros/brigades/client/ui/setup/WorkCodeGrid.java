package ru.telros.brigades.client.ui.setup;

import ru.telros.brigades.client.dto.SlaWorkCodeDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class WorkCodeGrid extends BrigadesGrid<SlaWorkCodeDto> {
	public WorkCodeGrid(List<SlaWorkCodeDto> items) {
		super(SetupConstants.getWorkCodeColumns(), items);
		//removeFilterRow();
		setSelectionMode(SelectionMode.NONE);
	}

	@Override
	protected void onClick(SlaWorkCodeDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(SlaWorkCodeDto entity, Column column) {

	}
}
