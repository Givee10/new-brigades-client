package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.ui.Component;
import org.vaadin.addons.filteringgrid.FilterGrid;
import org.vaadin.addons.filteringgrid.filters.InMemoryFilter;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.ui.component.CustomTextField;

import java.util.List;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.ui.grid.GridColumn.COLUMN_WIDTH;
import static ru.telros.brigades.client.ui.grid.GridColumn.LAYOUT_WIDTH;

public abstract class BrigadesFilterGrid<T> extends FilterGrid<T> implements HasLogger {
	public BrigadesFilterGrid(List<GridColumn<T>> columns, List<T> items) {
		setSizeFull();
		setSelectionMode(SelectionMode.SINGLE);
		setColumnReorderingAllowed(true);
		setColumnResizeMode(ColumnResizeMode.ANIMATED);

		for (GridColumn<T> column : columns) {
			if (column.isComponent()) {
				Column<T, ?> col = addComponentColumn((ValueProvider<T, Component>) column.getValueProvider())
						.setId(column.getId()).setCaption(column.getCaption());
				col.setWidth(LAYOUT_WIDTH);
				col.setResizable(false);
				//((Column<T, Component>) col).setRenderer(new ComponentRenderer());
			} else {
				Column<T, ?> col = addColumn(column.getValueProvider()).setId(column.getId()).setCaption(column.getCaption());
				col.setWidth(COLUMN_WIDTH);
				CustomTextField textField = new CustomTextField();
				textField.setHeight(80, Unit.PERCENTAGE);
				col.setFilter(textField, InMemoryFilter.StringComparator.containsIgnoreCase());
				col.setDescriptionGenerator(column.getDescriptionGenerator());
				if (column.isDate())
					col.setComparator(new BrigadesDateComparator<>(column.getValueProvider()));
				getDefaultHeaderRow().getCell(col).setDescription(column.getCaption());
			}
		}
		setItems(items);
	}

	public void removeFilterRow() {
		removeHeaderRow(1);
	}

	public void refreshAll() {
		getDataProvider().refreshAll();
	}

	public List<T> getItems() {
		return getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
	}

	public T getSelectedRow() {
		return getSelectedItems().stream().findFirst().orElse(null);
	}
}
