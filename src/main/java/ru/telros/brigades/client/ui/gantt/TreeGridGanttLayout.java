package ru.telros.brigades.client.ui.gantt;

import com.vaadin.server.Responsive;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import org.tltv.gantt.Gantt;
import org.tltv.gantt.client.shared.AbstractStep;
import org.tltv.gantt.client.shared.Step;
import org.tltv.gantt.client.shared.SubStep;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.GanttItemDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.workorder.WorkOrderConstants;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.telros.brigades.client.MessageManager.msg;

public class TreeGridGanttLayout extends HorizontalLayout implements HasLogger {
	private GanttTreeGrid ganttTreeGrid = new GanttTreeGrid();
	private CustomGantt gantt = new CustomGantt();
	private Map<AbstractStep, WorkDto> ganttData = new HashMap<>();
	private Boolean updateVisible = true;
	private Map<GanttItemDto, Boolean> isExpandedMap = new HashMap<>();
	public TreeGridGanttLayout() {
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		setSizeFull();
		setMargin(false);
		setSpacing(false);

		ganttTreeGrid.setHeight(100, Unit.PERCENTAGE);
		ganttTreeGrid.setWidth(200, Unit.PIXELS);
		ganttTreeGrid.getColumn("name").setWidth(200);

		ganttTreeGrid.addExpandListener(expandEvent -> {
			isExpandedMap.put(expandEvent.getExpandedItem(), true);
//			if (expandEvent.isUserOriginated()) {
				expandRecursively(expandEvent.getExpandedItem());
//			}
		});
		ganttTreeGrid.addCollapseListener(collapseEvent -> {
			isExpandedMap.put(collapseEvent.getCollapsedItem(), false);
//			if (collapseEvent.isUserOriginated()) {
				collapseRecursively(collapseEvent.getCollapsedItem());
//			}
		});

		gantt.setSizeFull();
//		gantt.setTimeZone(TimeZone.getTimeZone(ZonedDateTime.now().getZone()));
//		gantt.setTimeZone();
		gantt.setVerticalScrollDelegateTarget(ganttTreeGrid);

		addComponent(ganttTreeGrid);
		addComponent(gantt);
		setExpandRatio(gantt, 1);

		gantt.addClickListener((Gantt.ClickListener) event -> {
			if (event.getDetails().isDoubleClick() && event.getDetails().getButton().equals(MouseEventDetails.MouseButton.LEFT)) {
				if (ganttData.containsKey(event.getStep())) {
					WorkOrderLayout.showWorkOrder(ganttData.get(event.getStep()));
					//Notification.show("Clicked " + ganttData.get(event.getStep()).getNumber(), Notification.Type.TRAY_NOTIFICATION);
				}
			}
		});

		ganttTreeGrid.setColumnResizeMode(ColumnResizeMode.SIMPLE);
		ganttTreeGrid.collapseAll();
		update();
	}

	public void update() {
		//Arrays.asList(TimeZone.getAvailableIDs()).stream().map(e->TimeZone.getTimeZone(e)).sorted((e1,e2)->e1.getRawOffset()-e2.getRawOffset()).forEach(e-> getLogger().debug(e));

		if (updateVisible) {
			gantt.removeSteps();
			ganttTreeGrid.clearItems();
			/*
		sun.util.calendar.ZoneInfo[id="Africa/Addis_Ababa",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Asmara",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Asmera",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Dar_es_Salaam",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Djibouti",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Juba",offset=10800000,dstSavings=0,useDaylight=false,transitions=36,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Kampala",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Mogadishu",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Africa/Nairobi",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Antarctica/Syowa",offset=10800000,dstSavings=0,useDaylight=false,transitions=3,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Aden",offset=10800000,dstSavings=0,useDaylight=false,transitions=3,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Baghdad",offset=10800000,dstSavings=0,useDaylight=false,transitions=55,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Bahrain",offset=10800000,dstSavings=0,useDaylight=false,transitions=4,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Istanbul",offset=10800000,dstSavings=0,useDaylight=false,transitions=116,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Kuwait",offset=10800000,dstSavings=0,useDaylight=false,transitions=3,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Qatar",offset=10800000,dstSavings=0,useDaylight=false,transitions=4,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Asia/Riyadh",offset=10800000,dstSavings=0,useDaylight=false,transitions=3,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Etc/GMT-3",offset=10800000,dstSavings=0,useDaylight=false,transitions=0,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Europe/Istanbul",offset=10800000,dstSavings=0,useDaylight=false,transitions=116,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Europe/Kirov",offset=10800000,dstSavings=0,useDaylight=false,transitions=65,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Europe/Minsk",offset=10800000,dstSavings=0,useDaylight=false,transitions=69,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Europe/Moscow",offset=10800000,dstSavings=0,useDaylight=false,transitions=79,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Europe/Simferopol",offset=10800000,dstSavings=0,useDaylight=false,transitions=76,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Indian/Antananarivo",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Indian/Comoro",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]
		sun.util.calendar.ZoneInfo[id="Indian/Mayotte",offset=10800000,dstSavings=0,useDaylight=false,transitions=6,lastRule=null]

			 */
			ZoneId zoneID = gantt.getTimeZone().toZoneId();
			ZonedDateTime timeNow = ZonedDateTime.now().withZoneSameLocal(zoneID);
//			ZonedDateTime timeNow = ZonedDateTime.of(2019,11,17,12,0,0,0,ZonedDateTime.now().getZone());

			List<GanttItemDto> ganttBrigades = BrigadesUI.getRestTemplate().getGanttBrigades();
			for (GanttItemDto brigade : ganttBrigades) {
				if (brigade.getChildren() != null) {
					brigade.setStartDatePlan(brigade.getStartDatePlan().withZoneSameInstant(zoneID));
					brigade.setFinishDatePlan(brigade.getFinishDatePlan().withZoneSameInstant(zoneID));
					final Step brigadeStep = createBrigadeStep(brigade);
					gantt.addStep(brigadeStep);
					brigade.setStep(brigadeStep);
					brigade.setStepIndex(gantt.getStepIndex(brigadeStep));
					if (!isExpandedMap.containsKey(brigade)) {
						expandRecursively(brigade);
					}
					ganttTreeGrid.setRootItem(brigade);
					//if (start.isAfter(brigade.getStartDate())) start = brigade.getStartDate();
					//if (finish.isBefore(brigade.getFinishDate())) finish = brigade.getFinishDate();
					for (GanttItemDto work : brigade.getChildren()) {
						if (work.getStartDatePlan() != null && work.getFinishDatePlan() != null) {
							work.setStartDatePlan(work.getStartDatePlan().withZoneSameInstant(zoneID));
							work.setFinishDatePlan(work.getFinishDatePlan().withZoneSameInstant(zoneID));
							final SubStep subStep = createBrigadeSubStep(work);
							brigadeStep.addSubStep(subStep);
							final Step workStep = createStep(work);
							gantt.addStep(workStep);
							work.setStep(workStep);
							work.setStepIndex(gantt.getStepIndex(workStep));
							ganttTreeGrid.setParentItem(brigade, work);
							//ganttData.put(workStep, BrigadesUI.getRestTemplate().getWork(work.getId()));
							if (work.getChildren() != null) {
								for (GanttItemDto child : work.getChildren()) {
									if (child.getStartDateFact() != null && child.getFinishDateFact() != null) {
										child.setStartDateFact(child.getStartDateFact().withZoneSameInstant(zoneID));
										child.setFinishDateFact(child.getFinishDateFact().withZoneSameInstant(zoneID));
										final Step workItemStep = createItemStep(child);
										gantt.addStep(workItemStep);
										child.setStep(workItemStep);
										child.setStepIndex(gantt.getStepIndex(workItemStep));
										ganttTreeGrid.setParentItem(work, child);
									}
								}
							}
						}
					}
				}
				if (!isExpandedMap.containsKey(brigade)) {
					collapseRecursively(brigade);
				} else {
					if (isExpandedMap.get(brigade)){
						expandRecursively(brigade);
					} else {
						collapseRecursively(brigade);
					}
				}
			}
			ZonedDateTime start = timeNow.withHour(8);
			ZonedDateTime finish = timeNow.withHour(20);

			//Date s = new Date();
			//s.setHours(8);
			//Date f = new Date();
			//f.setHours(20);
			if (DatesUtil.isBetween(start, finish, timeNow)) {
				gantt.setStartDate(start);
				gantt.setEndDate(finish);
			} else {
				gantt.setStartDate(finish);
				gantt.setEndDate(start.plusDays(1));
			}
			//gantt.setStartDate(start);
			//gantt.setEndDate(finish);

			//gantt.getState().timestamp = ZonedDateTime.now(zoneID).toInstant().toEpochMilli();
			gantt.updateCurrentTime();
			ganttTreeGrid.refreshAll();
			//ganttTreeGrid.collapseAll();
		}
	}

	public void updateCurrentTime() {
		gantt.updateCurrentTime();
	}

	private void expandRecursively(GanttItemDto expandedItem) {
		if (ganttTreeGrid.getDataCommunicator().hasChildren(expandedItem)) {
			for (GanttItemDto child : ganttTreeGrid.getChildren(expandedItem)) {
				gantt.addStep(child.getStepIndex(), child.getStep());
				expandRecursively(child);
			}
		}
	}

	private void collapseRecursively(GanttItemDto collapsedItem) {
		if (ganttTreeGrid.getDataCommunicator().hasChildren(collapsedItem)) {
			for (GanttItemDto child : ganttTreeGrid.getChildren(collapsedItem)) {
				gantt.removeStep(child.getStep());
				collapseRecursively(child);
			}
		}
	}

	private Step createStep(GanttItemDto item) {
		final Step step = new Step();
		step.setCaption(item.getName());
		step.setDescription(item.getDescription());
		step.setBackgroundColor(WorkOrderConstants.getGanttColor(item, true));
		step.setStartDate(DatesUtil.zonedDateTimeToLong(item.getStartDatePlan()));
		step.setEndDate(DatesUtil.zonedDateTimeToLong(item.getFinishDatePlan()));
		return step;
	}

	private Step createFactStep(GanttItemDto item) {
		final Step step = new Step();
		step.setCaption(item.getName());
		step.setDescription(item.getDescription());
		step.setBackgroundColor(WorkOrderConstants.getGanttColor(item, true));
		step.setStartDate(DatesUtil.zonedDateTimeToLong(item.getStartDateFact()));
		step.setEndDate(DatesUtil.zonedDateTimeToLong(item.getFinishDateFact()));
		return step;
	}

	private Step createItemStep(GanttItemDto item) {
		final Step step = new Step();
		step.setCaption(item.getName());
		step.setDescription(item.getDescription());
		step.setBackgroundColor(WorkOrderConstants.getGanttColor(item, false));
		step.setStartDate(DatesUtil.zonedDateTimeToLong(item.getStartDateFact()));
		step.setEndDate(DatesUtil.zonedDateTimeToLong(item.getFinishDateFact()));
		return step;
	}

	private Step createBrigadeStep(GanttItemDto item) {
		final Step step = new Step();
		//step.setBackgroundColor(BrigadesTheme.COLOR_NORMAL);
		step.setStartDate(DatesUtil.zonedDateTimeToLong(item.getStartDatePlan()));
		step.setEndDate(DatesUtil.zonedDateTimeToLong(item.getFinishDatePlan()));
		return step;
	}

	private SubStep createBrigadeSubStep(GanttItemDto item) {
		final SubStep subStep = new SubStep();
		subStep.setCaption(item.getCaption());
		subStep.setDescription(item.getCaption());
		subStep.setBackgroundColor(WorkOrderConstants.getGanttColor(item, true));
		subStep.setStartDate(DatesUtil.zonedDateTimeToLong(item.getStartDatePlan()));
		subStep.setEndDate(DatesUtil.zonedDateTimeToLong(item.getFinishDatePlan()));
		return subStep;
	}

	private SubStep createFactSubStep(GanttItemDto item) {
		final SubStep subStep = new SubStep();
		subStep.setCaption(item.getCaption());
		subStep.setDescription(item.getDescription());
		subStep.setBackgroundColor(WorkOrderConstants.getGanttColor(item, false));
		subStep.setStartDate(DatesUtil.zonedDateTimeToLong(item.getStartDateFact()));
		subStep.setEndDate(DatesUtil.zonedDateTimeToLong(item.getFinishDateFact()));
		return subStep;
	}

	public void addToolBarItems(BrigadesPanel panel) {
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, WorkOrderConstants.workStyles);
		panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, (MenuBar.Command) menuItem -> update());
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		update();
	}
}
