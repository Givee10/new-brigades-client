package ru.telros.brigades.client.ui.eventlog;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import ru.telros.brigades.client.BrigadesIcons;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.dto.EventWithActionDto;
import ru.telros.brigades.client.ui.grid.GridColumn;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class EventConstants {
	public static final GridColumn<EventWithActionDto> FLAG = GridColumn.createComponentColumn("_flag", "", EventConstants::createFlag);
	public static final GridColumn<EventWithActionDto> LABEL = GridColumn.createComponentColumn("_label", "", EventConstants::createLabel);
	public static final GridColumn<EventWithActionDto> ID = GridColumn.createColumn("id", "№", EventWithActionDto::getId);
	public static final GridColumn<EventWithActionDto> DATE = GridColumn.createDateColumn("date", "Время события", EventWithActionDto::getDate);
	public static final GridColumn<EventWithActionDto> MESSAGE = GridColumn.createColumn("message", "Событие", EventConstants::createMessage);
	public static final GridColumn<EventWithActionDto> TYPE = GridColumn.createColumn("type", "Тип", EventWithActionDto::getType);
	public static final GridColumn<EventWithActionDto> SOURCE = GridColumn.createColumn("source", "Источник", EventWithActionDto::getSource);
	public static final GridColumn<EventWithActionDto> ADDRESS = GridColumn.createColumn("address", "Адрес", EventWithActionDto::getAddress);
	public static final GridColumn<EventWithActionDto> LEVEL = GridColumn.createColumn("level", "Уровень", EventWithActionDto::getLevel);
	public static final GridColumn<EventWithActionDto> HL_DATE = GridColumn.createDateColumn("hlDate", "Дата из ГЛ", EventWithActionDto::getFromHlDate);
	public static final GridColumn<EventWithActionDto> ACTION_USER = GridColumn.createColumn("actionUser", "Экшн Пользователь", eventWithActionDto -> {
		if (eventWithActionDto.getActionUser()!=null){
			if (eventWithActionDto.getActionUser().getFullName()!=null){
				return eventWithActionDto.getActionUser().getFullName();
			}
		}
		return " - ";
	});
	public static final GridColumn<EventWithActionDto> USER = GridColumn.createColumn("User", "Пользователь", eventWithActionDto -> {
		if (eventWithActionDto.getUser()!=null){
			if (eventWithActionDto.getUser().getFullName()!=null){
				return eventWithActionDto.getUser().getFullName();
			}
		}
		return " - ";
	});
	public static final String EVENT_TYPE_01 = "Поступление заявки";
	public static final String EVENT_TYPE_01_1 = "Создание заявки";
	public static final String EVENT_TYPE_01_2 = "Изменение заявки";
	public static final String EVENT_TYPE_02 = "Создание/изменение заявки";
	public static final String EVENT_TYPE_03 = "Нарушение сроков закрытия заявки";
	public static final String EVENT_TYPE_04 = "Нарушение сроков реагирования диспетчером";
	public static final String EVENT_TYPE_05 = "Нарушение сроков реагирования ст. мастером";
	public static final String EVENT_TYPE_06 = "СОУ – ответственный по заявке";
	public static final String EVENT_TYPE_07 = "Поручена РВ";
	public static final String EVENT_TYPE_08 = "РВ – ответственный по заявке";
	public static final String EVENT_TYPE_09 = "Создание/изменение действия или комментария по заявке";
	public static final String EVENT_TYPE_09_1 = "Создание действий или комментария по заявке";
	public static final String EVENT_TYPE_09_2 = "Изменение действий или комментария по заявке";
	public static final String EVENT_TYPE_10 = "Закрытие в СОУ заявки";

	public static final String EVENT_TYPE_11 = "Создание/изменение работы";
	public static final String EVENT_TYPE_11_1 = "Создание работы";
	public static final String EVENT_TYPE_11_2 = "Изменение работы";
	public static final String EVENT_TYPE_12 = "Отправка бригаде работы";
	public static final String EVENT_TYPE_13 = "Подтверждение получения бригадой работы";
	public static final String EVENT_TYPE_14 = "Нет подтверждения получения бригадой работы";
	public static final String EVENT_TYPE_15 = "Прибытие бригады на адрес";
	public static final String EVENT_TYPE_16 = "Убытие бригады с адреса";
	public static final String EVENT_TYPE_17 = "Нет прибытия бригады на адрес";
	public static final String EVENT_TYPE_18 = "Начало выполнения работы на адресе";
	public static final String EVENT_TYPE_19 = "Окончание выполнения (детали) работы на адресе";
	public static final String EVENT_TYPE_20 = "Поступление фото/документов по работе на адресе";
	public static final String EVENT_TYPE_20_1 = "Поступление фото по работе на адресе";
	public static final String EVENT_TYPE_20_2 = "Поступление документов по работе на адресе";
	public static final String EVENT_TYPE_21 = "Создание работы мастером бригады";
	public static final String EVENT_TYPE_22 = "Завершение работы на адресе";
	public static final String EVENT_TYPE_23 = "Нарушение сроков выполнения работы";
	public static final String EVENT_TYPE_24 = "Не предоставлены фотоматериалы по работе адресу бригадой";
	public static final String EVENT_TYPE_25 = "Нет информации о работе бригады";

	public static final String EVENT_TYPE_26 = "Бригада зарегистрирована в АИС БРИГАДЫ";
	public static final String EVENT_TYPE_26_1 = "Диспетчер зарегистрирован в АИС БРИГАДЫ";
	public static final String EVENT_TYPE_27 = "Бригада завершила смену";
	public static final String EVENT_TYPE_27_1 = "Выход пользователя из системы";

	public static final String EVENT_TYPE_28 = "Связь с бригадой потеряна";
	public static final String EVENT_TYPE_29 = "Связь с бригадой восстановлена";
	public static final String EVENT_TYPE_34 = "Проблемы со связью";

	public static final String EVENT_TYPE_30 = "Выезд бригады за пределы зоны обслуживания";
	public static final String EVENT_TYPE_31 = "Прибытие на адрес ТС";
	public static final String EVENT_TYPE_32 = "Завершение работы на адресе ТС";
	public static final String EVENT_TYPE_33 = "Отсутствие ТС гос. номер на адресе";

	public static final LinkedHashMap<String, String> eventStyles = new LinkedHashMap<String, String>() {
		{
			put("Информационные", BrigadesTheme.STYLE_ET_NORMAL);
			put("Проблемы со связью", BrigadesTheme.STYLE_ET_INFO);
			put("Тревожные", BrigadesTheme.STYLE_ET_WARNING);
		}
	};

	private static final List<String> requestEvents = new ArrayList<>(Arrays.asList(
			EVENT_TYPE_01,
			EVENT_TYPE_01_1,
			EVENT_TYPE_01_2,
			EVENT_TYPE_02,
			EVENT_TYPE_03,
			EVENT_TYPE_04,
			EVENT_TYPE_05,
			EVENT_TYPE_06,
			EVENT_TYPE_07,
			EVENT_TYPE_08,
			EVENT_TYPE_09,
			EVENT_TYPE_09_1,
			EVENT_TYPE_09_2,
			EVENT_TYPE_10
	));

	private static final List<String> workOrderEvents = new ArrayList<>(Arrays.asList(
			EVENT_TYPE_11,
			EVENT_TYPE_11_1,
			EVENT_TYPE_11_2,
			EVENT_TYPE_12,
			EVENT_TYPE_13,
			EVENT_TYPE_14,
			EVENT_TYPE_15,
			EVENT_TYPE_16,
			EVENT_TYPE_17,
			EVENT_TYPE_18,
			EVENT_TYPE_19,
			EVENT_TYPE_20,
			EVENT_TYPE_20_1,
			EVENT_TYPE_20_2,
			EVENT_TYPE_21,
			EVENT_TYPE_22,
			EVENT_TYPE_23,
			EVENT_TYPE_24,
			EVENT_TYPE_25
	));

	public static final LinkedHashMap<String, BrigadesIcons> iconEvents = new LinkedHashMap<String, BrigadesIcons>() {
		{
			//put(EVENT_TYPE_01, new BrigadesIcons(BrigadesTheme.ICON_EVENT_01, null));
			put(EVENT_TYPE_01_1, new BrigadesIcons(BrigadesTheme.ICON_EVENT_01, null));
			put(EVENT_TYPE_01_2, new BrigadesIcons(BrigadesTheme.ICON_EVENT_02, null));
			//put(EVENT_TYPE_02, new BrigadesIcons(BrigadesTheme.ICON_EVENT_02, null));
			put(EVENT_TYPE_03, new BrigadesIcons(BrigadesTheme.ICON_EVENT_03, BrigadesTheme.COLOR_EMERGENCY));
//			put(EVENT_TYPE_04, new BrigadesIcons(BrigadesTheme.ICON_EVENT_04, BrigadesTheme.COLOR_EMERGENCY));
//			put(EVENT_TYPE_05, new BrigadesIcons(BrigadesTheme.ICON_EVENT_05, BrigadesTheme.COLOR_EMERGENCY));
//			put(EVENT_TYPE_06, new BrigadesIcons(BrigadesTheme.ICON_EVENT_06, null));
//			put(EVENT_TYPE_07, new BrigadesIcons(BrigadesTheme.ICON_EVENT_07, null));
//			put(EVENT_TYPE_08, new BrigadesIcons(BrigadesTheme.ICON_EVENT_08, BrigadesTheme.COLOR_SUCCESS));
			//put(EVENT_TYPE_09, new BrigadesIcons(BrigadesTheme.ICON_EVENT_09, null));
			put(EVENT_TYPE_09_1, new BrigadesIcons(BrigadesTheme.ICON_EVENT_09, null));
			put(EVENT_TYPE_09_2, new BrigadesIcons(BrigadesTheme.ICON_EVENT_09, null));
//			put(EVENT_TYPE_10, new BrigadesIcons(BrigadesTheme.ICON_EVENT_10, null));
			put("WORK", null);
			put(EVENT_TYPE_11, new BrigadesIcons(BrigadesTheme.ICON_EVENT_11, null));
			put(EVENT_TYPE_11_1, new BrigadesIcons(BrigadesTheme.ICON_EVENT_11, null));
			put(EVENT_TYPE_11_2, new BrigadesIcons(BrigadesTheme.ICON_EVENT_11, null));
			put(EVENT_TYPE_12, new BrigadesIcons(BrigadesTheme.ICON_EVENT_12, null));
			put(EVENT_TYPE_13, new BrigadesIcons(BrigadesTheme.ICON_EVENT_13, BrigadesTheme.COLOR_SUCCESS));
//			put(EVENT_TYPE_14, new BrigadesIcons(BrigadesTheme.ICON_EVENT_14, BrigadesTheme.COLOR_EMERGENCY));
			put(EVENT_TYPE_15, new BrigadesIcons(BrigadesTheme.ICON_EVENT_15, null));
			put(EVENT_TYPE_16, new BrigadesIcons(BrigadesTheme.ICON_EVENT_16, null));
//			put(EVENT_TYPE_17, new BrigadesIcons(BrigadesTheme.ICON_EVENT_17, BrigadesTheme.COLOR_EMERGENCY));
			put(EVENT_TYPE_18, new BrigadesIcons(BrigadesTheme.ICON_EVENT_18, null));
			put(EVENT_TYPE_19, new BrigadesIcons(BrigadesTheme.ICON_EVENT_19, null));
			//put(EVENT_TYPE_20, new BrigadesIcons(BrigadesTheme.ICON_EVENT_20, null));
			put(EVENT_TYPE_20_1, new BrigadesIcons(BrigadesTheme.ICON_EVENT_20, null));
			put(EVENT_TYPE_20_2, new BrigadesIcons(BrigadesTheme.ICON_EVENT_20, null));
			put(EVENT_TYPE_21, new BrigadesIcons(BrigadesTheme.ICON_EVENT_21, null));
			put(EVENT_TYPE_22, new BrigadesIcons(BrigadesTheme.ICON_EVENT_22, null));
			put(EVENT_TYPE_23, new BrigadesIcons(BrigadesTheme.ICON_EVENT_23, BrigadesTheme.COLOR_EMERGENCY));
//			put(EVENT_TYPE_24, new BrigadesIcons(BrigadesTheme.ICON_EVENT_24, BrigadesTheme.COLOR_EMERGENCY));
//			put(EVENT_TYPE_25, new BrigadesIcons(BrigadesTheme.ICON_EVENT_25, null));
			put("LOGIN", null);
			put(EVENT_TYPE_26, new BrigadesIcons(BrigadesTheme.ICON_EVENT_26, null));
			put(EVENT_TYPE_26_1, new BrigadesIcons(BrigadesTheme.ICON_EVENT_26, null));
			put(EVENT_TYPE_27_1, new BrigadesIcons(BrigadesTheme.ICON_EVENT_27, null));
			put("NETWORK", null);
			//put(EVENT_TYPE_34, new BrigadesIcons(BrigadesTheme.ICON_EVENT_28, BrigadesTheme.COLOR_EMERGENCY));
			put(EVENT_TYPE_28, new BrigadesIcons(BrigadesTheme.ICON_EVENT_28, BrigadesTheme.COLOR_EMERGENCY));
			put(EVENT_TYPE_29, new BrigadesIcons(BrigadesTheme.ICON_EVENT_29, BrigadesTheme.COLOR_SUCCESS));
//			put("VEHICLES", null);
//			put(EVENT_TYPE_30, new BrigadesIcons(BrigadesTheme.ICON_EVENT_30, BrigadesTheme.COLOR_EMERGENCY));
//			put(EVENT_TYPE_31, new BrigadesIcons(BrigadesTheme.ICON_EVENT_31, null));
//			put(EVENT_TYPE_32, new BrigadesIcons(BrigadesTheme.ICON_EVENT_32, null));
//			put(EVENT_TYPE_33, new BrigadesIcons(BrigadesTheme.ICON_EVENT_33, BrigadesTheme.COLOR_EMERGENCY));
		}
	};

	public static final LinkedHashMap<String, Boolean> actionEvents = new LinkedHashMap<String, Boolean>() {
		{
			put(EVENT_TYPE_01, true);
			put(EVENT_TYPE_01_1, true);
			put(EVENT_TYPE_01_2, false);
			put(EVENT_TYPE_02, false);
			put(EVENT_TYPE_03, true);
			put(EVENT_TYPE_04, false);
			put(EVENT_TYPE_05, false);
			put(EVENT_TYPE_06, false);
			put(EVENT_TYPE_07, false);
			put(EVENT_TYPE_08, false);
			put(EVENT_TYPE_09, false);
			put(EVENT_TYPE_09_1, false);
			put(EVENT_TYPE_09_2, false);
			put(EVENT_TYPE_10, false);

			put(EVENT_TYPE_11, false);
			put(EVENT_TYPE_11_1, false);
			put(EVENT_TYPE_11_2, false);
			put(EVENT_TYPE_12, false);
			put(EVENT_TYPE_13, true);
			put(EVENT_TYPE_14, true);
			put(EVENT_TYPE_15, true);
			put(EVENT_TYPE_16, true);
			put(EVENT_TYPE_17, true);
			put(EVENT_TYPE_18, false);
			put(EVENT_TYPE_19, false);
			put(EVENT_TYPE_20, true);
			put(EVENT_TYPE_20_1, true);
			put(EVENT_TYPE_20_2, true);
			put(EVENT_TYPE_21, true);
			put(EVENT_TYPE_22, true);
			put(EVENT_TYPE_23, true);
			put(EVENT_TYPE_24, true);
			put(EVENT_TYPE_25, false);

			put(EVENT_TYPE_26, false);
			put(EVENT_TYPE_26_1, false);
			put(EVENT_TYPE_27, false);
			put(EVENT_TYPE_27_1, false);

			//put(EVENT_TYPE_34, false);
			put(EVENT_TYPE_28, true);
			put(EVENT_TYPE_29, false);

			put(EVENT_TYPE_30, false);
			put(EVENT_TYPE_31, false);
			put(EVENT_TYPE_32, false);
			put(EVENT_TYPE_33, false);
		}
	};

	public static final LinkedHashMap<String, Boolean> soundEvents = new LinkedHashMap<String, Boolean>() {
		{
			put(EVENT_TYPE_01, true);
			put(EVENT_TYPE_01_1, true);
			put(EVENT_TYPE_01_2, false);
			put(EVENT_TYPE_02, false);
			put(EVENT_TYPE_03, true);
			put(EVENT_TYPE_04, false);
			put(EVENT_TYPE_05, false);
			put(EVENT_TYPE_06, true);
			put(EVENT_TYPE_07, false);
			put(EVENT_TYPE_08, true);
			put(EVENT_TYPE_09, false);
			put(EVENT_TYPE_09_1, false);
			put(EVENT_TYPE_09_2, false);
			put(EVENT_TYPE_10, false);

			put(EVENT_TYPE_11, true);
			put(EVENT_TYPE_11_1, true);
			put(EVENT_TYPE_11_2, true);
			put(EVENT_TYPE_12, true);
			put(EVENT_TYPE_13, true);
			put(EVENT_TYPE_14, true);
			put(EVENT_TYPE_15, true);
			put(EVENT_TYPE_16, true);
			put(EVENT_TYPE_17, true);
			put(EVENT_TYPE_18, false);
			put(EVENT_TYPE_19, false);
			put(EVENT_TYPE_20, true);
			put(EVENT_TYPE_20_1, true);
			put(EVENT_TYPE_20_2, true);
			put(EVENT_TYPE_21, true);
			put(EVENT_TYPE_22, true);
			put(EVENT_TYPE_23, true);
			put(EVENT_TYPE_24, true);
			put(EVENT_TYPE_25, false);

			put(EVENT_TYPE_26, false);
			put(EVENT_TYPE_26_1, false);
			put(EVENT_TYPE_27, false);
			put(EVENT_TYPE_27_1, false);

			//put(EVENT_TYPE_34, false);
			put(EVENT_TYPE_28, true);
			put(EVENT_TYPE_29, false);

			put(EVENT_TYPE_30, false);
			put(EVENT_TYPE_31, true);
			put(EVENT_TYPE_32, true);
			put(EVENT_TYPE_33, true);
		}
	};

	public static Boolean isActionable(EventWithActionDto eventDto) {
		String type = eventDto.getType();
		return type != null && actionEvents.containsKey(type) && actionEvents.get(type);
	}

	public static Boolean isSoundEnable(EventWithActionDto eventDto) {
		String type = eventDto.getType();
		return type != null && soundEvents.containsKey(type) && soundEvents.get(type);
	}

	public static Boolean isGridShown(EventWithActionDto eventDto) {
		String type = eventDto.getType();
		return type != null && iconEvents.containsKey(type);
	}

	public static Boolean isPanelShown(EventWithActionDto eventDto) {
		return eventDto.getIdAction() == null && isActionable(eventDto) && isGridShown(eventDto);
	}

	public static Boolean isRequestSource(EventWithActionDto eventDto) {
		String type = eventDto.getType();
		return type != null && requestEvents.contains(type);
	}

	public static Boolean isWorkSource(EventWithActionDto eventDto) {
		String type = eventDto.getType();
		return type != null && workOrderEvents.contains(type);
	}

	public static List<EventWithActionDto> filterEventsForGrid(List<EventWithActionDto> events) {
		return events.stream().filter(EventConstants::isGridShown).collect(Collectors.toList());
	}

	public static List<EventWithActionDto> filterEventsForPanel(List<EventWithActionDto> events) {
		return events.stream().filter(EventConstants::isPanelShown).collect(Collectors.toList());
	}

	public static String createMessage(EventWithActionDto eventDto) {
		String message = eventDto.getMessage();
		int i = message.indexOf("|");
		return i == -1 ? message : message.substring(0, i);
	}

	private static CssLayout createLabel(EventWithActionDto eventDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();

		if (isGridShown(eventDto)) {
			String type = eventDto.getType();
			BrigadesIcons icon = iconEvents.get(type);
			Label label = new Label(icon.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			label.addStyleName(BrigadesTheme.CLICKABLE);
			label.setDescription(type);
			layout.addComponent(label);
			if (isWorkSource(eventDto))
				layout.addLayoutClickListener(clickEvent -> WorkOrderLayout.showWorkOrder(EventLogLayout.findWorkOrder(eventDto)));
			if (isRequestSource(eventDto))
				layout.addLayoutClickListener(clickEvent -> RequestLayout.showRequest(EventLogLayout.findRequest(eventDto)));
		}
		return layout;
	}

	private static CssLayout createFlag(EventWithActionDto eventDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();

		Label label = new Label();
		label.setSizeFull();
		label.setContentMode(ContentMode.HTML);
		label.addStyleName(BrigadesTheme.STYLE_ET_ICON);

		String content, description;
		if (isActionable(eventDto)) {
			if (eventDto.getIdAction() != null/* && eventDto.getUser().equals(BrigadesUI.getCurrentUser())*/) {
				content = BrigadesTheme.ICON_ACCEPTED.getHtml();
				description = "Событие квитировано";
				if (eventDto.getLastChangeDateAction() != null) {
					description = description.concat("\nВремя: ").concat(DatesUtil.formatZoned(eventDto.getLastChangeDateAction()));
					//description = description.concat("<BR>Пользователь: ").concat(actionDto.getUser().getFullName());
				}
			} else {
				content = BrigadesTheme.ICON_NOT_ACCEPTED.getHtml();
				description = "Событие не квитировано";
				label.addStyleName(BrigadesTheme.CLICKABLE);
				layout.addLayoutClickListener(layoutClickEvent -> EventLogLayout.acceptEvent(eventDto));
			}
		} else {
			content = BrigadesTheme.ICON_ACCEPTED.getHtml();
			description = "Событие квитировано автоматически";
		}
		label.setValue(content);
		label.setDescription(description);
		layout.addComponent(label);
		return layout;
	}

	public static List<GridColumn<EventWithActionDto>> getMainColumns() {
		return Arrays.asList(FLAG, LABEL, DATE, ADDRESS, MESSAGE, HL_DATE);
	}
	public static List<GridColumn<EventWithActionDto>> getReportColumns() {
		return Arrays.asList(FLAG, LABEL, DATE, ADDRESS, MESSAGE, HL_DATE, ACTION_USER, USER);
	}

	public static String getStyle(String status) {
		return (status != null) ? eventStyles.get(status) : BrigadesTheme.STYLE_ET_EMERGENCY;
	}
}
