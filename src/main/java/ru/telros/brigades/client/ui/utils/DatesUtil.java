package ru.telros.brigades.client.ui.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.telros.brigades.client.DateInterval;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

public class DatesUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(DatesUtil.class);
	//public static final String GRID_DATE_FORMAT = "%1$td.%1$tm.%1$tY %1$tH:%1$tM:%1$tS";
	private static final String FILE_DATE_FORMAT = "yyyy-MM-dd";
	//извиняюсь
	private static final String KESHADATEFORMAT1 = "dd-MM-yyyy";
	private static final String KESHADATEFORMAT2 = "HH:mm";
	public static final String GRID_DATE_FORMAT = "HH:mm dd.MM.yyyy";
	public static final String JSON_DATE_FORMAT = "dd.MM.yyyy_HH:mm:ss";
	public static final String NORMAL_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";
	public static final String STANDARD_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	public static final String TABLE_DATE_FORMAT = "HH:mm dd.MM.yyyy 'г.'";

	public static String formatFile(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, FILE_DATE_FORMAT);
	}

	public static String formatGrid(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, GRID_DATE_FORMAT);
	}

	public static String formatJSON(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, JSON_DATE_FORMAT);
	}

	public static String formatNormal(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, NORMAL_DATE_FORMAT);
	}

	public static String formatStandard(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, STANDARD_DATE_FORMAT);
	}

	public static String formatTable(LocalDateTime dateToConvert) {
		return localDateTimeToString(dateToConvert, TABLE_DATE_FORMAT);
	}

	public static String formatToOracle(ZonedDateTime dateToConvert) {
		DateTimeFormatterBuilder formatterBuilder = new DateTimeFormatterBuilder();
		DateTimeFormatter df = formatterBuilder.parseCaseInsensitive().appendPattern("dd-MMM-yy").toFormatter(Locale.ENGLISH);
		return df.format(dateToConvert);
	}

	public static String formatZoned(ZonedDateTime dateToConvert) {
		return zonedDateTimeToString(dateToConvert, NORMAL_DATE_FORMAT);
	}
	public static String formatZonedtoDDMMYYY(ZonedDateTime dateToConvert) {
		return zonedDateTimeToString(dateToConvert, KESHADATEFORMAT1);
	}
	public static String formatZonedtoMMHH(ZonedDateTime dateToConvert) {
		return zonedDateTimeToString(dateToConvert, KESHADATEFORMAT2);
	}

	public static String formatToUTC(ZonedDateTime dateToConvert) {
		return zonedDateTimeToString(dateToConvert, NORMAL_DATE_FORMAT, ZoneOffset.UTC);
	}

	public static LocalDateTime stringToLocal(String dateToConvert) {
		return StringUtil.isNull(dateToConvert) ? null : LocalDateTime.parse(dateToConvert, DateTimeFormatter.ofPattern(GRID_DATE_FORMAT));
	}

	public static DateInterval getViewDataPeriod() {
		return new DateInterval(setNullDateTime(), setTomorrowNullTime());
		//ZonedDateTime start = setStartDateTime();
		//ZonedDateTime finish = setTomorrowNullTime();
		//return new DateInterval(start, finish);
	}

	//находится ли введенное время внутри интервала
	public static boolean isBetween(LocalDateTime start, LocalDateTime end, LocalDateTime time){
		return (time.isAfter(start)) && (time.isBefore(end));
	}

	//находится ли введенное время внутри интервала
	public static boolean isBetween(ZonedDateTime start, ZonedDateTime end, ZonedDateTime time){
		return (time.isAfter(start)) && (time.isBefore(end));
	}

	public static ZonedDateTime setStartDateTime() {
		//return ZonedDateTime.of(2019, 9, 1, 0, 0, 0, 0, ZoneId.systemDefault());
		return ZonedDateTime.of(2000, 10, 10, 10, 10, 0, 0, ZoneId.systemDefault());
	}

	public static ZonedDateTime setNullDateTime() {
		return ZonedDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
	}

	public static ZonedDateTime setTomorrowNullTime() {
		return ZonedDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
	}

	public static ZonedDateTime setHourInDate(ZonedDateTime date, Integer hour) {
		return date.withHour(hour).withMinute(0).withSecond(0).withNano(0);
	}

	public static LocalDateTime setHourInDate(LocalDateTime date, Integer hour) {
		return date.withHour(hour).withMinute(0).withSecond(0).withNano(0);
	}

	public static ZonedDateTime setCurrentDate(ZonedDateTime date) {
		ZonedDateTime now = ZonedDateTime.now();
		return ZonedDateTime.of(now.getYear(), now.getMonth().getValue(), now.getDayOfMonth(),
				date.getHour(), date.getMinute(), date.getSecond(), date.getNano(), ZoneId.systemDefault());
	}

	public static LocalDateTime convertFromUTC(LocalDateTime date) {
		return date == null ? null : date.atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static LocalDateTime convertToUTC(LocalDateTime date) {
		return date == null ? null : date.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
	}

	public static ZonedDateTime localToZoned(LocalDateTime date) {
		return date == null ? null : date.atZone(ZoneId.systemDefault());
	}

	public static LocalDateTime zonedToLocal(ZonedDateTime date) {
		return date == null ? null : date.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static ZonedDateTime longToZonedDateTime(Long dateToConvert) {
		return ZonedDateTime.ofInstant(Instant.ofEpochMilli(dateToConvert), ZoneId.systemDefault());
	}

	public static LocalDateTime longToLocalDateTime(Long dateToConvert) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(dateToConvert), ZoneId.systemDefault());
	}

	public static Long zonedDateTimeToLong(ZonedDateTime dateToConvert) {
		return dateToConvert == null ? -1L :
//				dateToConvert.withZoneSameLocal(TimeZone.getDefault().toZoneId()).toEpochSecond();
				dateToConvert.toInstant().toEpochMilli();
	}

	public static Long localDateTimeToLong(LocalDateTime dateToConvert) {
		return dateToConvert == null ? -1L :
				dateToConvert.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}

	public static String zonedDateTimeToString(ZonedDateTime dateToConvert, String pattern) {
		return zonedDateTimeToString(dateToConvert, pattern, ZoneId.systemDefault());
	}

	public static String zonedDateTimeToString(ZonedDateTime dateToConvert, String pattern, ZoneId zoneId) {
		return dateToConvert == null ? "" :
				dateToConvert.withZoneSameInstant(zoneId).format(DateTimeFormatter.ofPattern(pattern));
	}

	public static String localDateTimeToString(LocalDateTime dateToConvert, String pattern) {
		return dateToConvert == null ? "" : dateToConvert.format(DateTimeFormatter.ofPattern(pattern));
	}
	public static String secondsToHHMM(long seconds){
		long minutes = (seconds/60)%60;
		long hours = (seconds/60)/60;
		LOGGER.debug(hours+":"+minutes);
		return hours+":"+minutes;
	}
	public static String minutesToHHMM(long minutes){
		return secondsToHHMM(minutes*60);
	}
	public static long HHMMToMinutes(String HHMM){
		LOGGER.debug(HHMM);
		if (!HHMM.matches("\\d{1,}:\\d{1,2}")) {
			throw new NumberFormatException();
		}
		return Integer.parseInt(HHMM.split(":")[0])*60+Integer.parseInt(HHMM.split(":")[1]);
	}
	public static String nowAsHH_MM_DD_MM_YYYY(){
		ZonedDateTime now = ZonedDateTime.now();
		return new StringBuilder()
				.append(now.getHour())
				.append('_')
				.append(now.getMinute())
				.append('_')
				.append(now.getDayOfMonth())
				.append('_')
				.append(now.getMonthValue())
				.append('_')
				.append(now.getYear()).toString();
	}
}
