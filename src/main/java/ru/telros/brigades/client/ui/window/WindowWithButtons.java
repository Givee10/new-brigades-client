package ru.telros.brigades.client.ui.window;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.Resource;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.*;

/**
 * Окно с кнопками в нижней части
 */
@SuppressWarnings("serial")
public class WindowWithButtons extends ModalWindow {
	private final HorizontalLayout bottomBar = new HorizontalLayout();
	private final HorizontalLayout buttonBar = new HorizontalLayout();
	private final MenuBar menuBar = new MenuBar();
	private final VerticalLayout content = new VerticalLayout();

	public WindowWithButtons() {
		this(null);
	}

	public WindowWithButtons(String caption) {
		super(caption);
		initContent();
	}

	public VerticalLayout getContentLayout() {
		return content;
	}

	public WindowWithButtons(final int width, final int height) {
		this(null, width, height);
	}

	public WindowWithButtons(String caption, int width, int height) {
		super(caption, width, height);
		initContent();
	}

	protected void initContent() {
		final VerticalLayout internalContent = new VerticalLayout();

		menuBar.setWidth(100, Sizeable.Unit.PERCENTAGE);
		menuBar.setVisible(false);

		content.setMargin(true);
		content.setSpacing(true);

		bottomBar.setWidth(100, Sizeable.Unit.PERCENTAGE);
		bottomBar.setSpacing(true);
		bottomBar.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
		bottomBar.setVisible(false);

		buttonBar.setMargin(true);
		buttonBar.setSpacing(true);
		buttonBar.setDefaultComponentAlignment(Alignment.BOTTOM_RIGHT);
		buttonBar.setVisible(false);

		bottomBar.addComponent(buttonBar);
		bottomBar.setComponentAlignment(buttonBar, Alignment.BOTTOM_RIGHT);

		internalContent.addComponent(menuBar);
		internalContent.addComponent(content);
		internalContent.addComponent(bottomBar);
		internalContent.setComponentAlignment(bottomBar, Alignment.BOTTOM_LEFT);
		internalContent.setExpandRatio(content, 1);

		setContent(internalContent);
	}

	public void addToMenu(final String caption, final MenuBar.Command command) {
		addToMenu(caption, null, command);
	}

	public void addToMenu(final String caption, final Resource icon, final MenuBar.Command command) {
		menuBar.setVisible(true);
		menuBar.addItem(caption, icon, command);
	}

	public void addToContent(final Component component) {
		content.addComponent(component);
	}

	public void addToContent(final Component component, final float expandRatio) {
		content.addComponent(component);
		content.setExpandRatio(component, expandRatio);
	}

	public Button addToButtonBar(final String caption, final Button.ClickListener listener) {
		return addToButtonBar(caption, null, listener);
	}

	public Button addToButtonBar(final String caption, final Resource icon, final Button.ClickListener listener) {
		bottomBar.setVisible(true);
		buttonBar.setVisible(true);
		Button btn = new Button(caption);
		btn.addClickListener(listener);
		btn.setIcon(icon);
		buttonBar.addComponent(btn);
		return btn;
	}
	public Button addToButtonBar(final String caption) {
		bottomBar.setVisible(true);
		buttonBar.setVisible(true);
		Button btn = new Button(caption);
		buttonBar.addComponent(btn);
		return btn;
	}

	protected Button addDownloadToButtonBar(final String caption, final Resource resource) {
		bottomBar.setVisible(true);
		buttonBar.setVisible(true);
		Button btn = new Button(caption);
		FileDownloader fd = new FileDownloader(resource);
		fd.extend(btn);
		buttonBar.addComponent(btn);
		return btn;
	}

	protected void addSpaceToButtonBar() {
		Label empty = new Label();
		empty.setWidth(20, Unit.PIXELS);
		buttonBar.addComponent(empty);
	}

	public void setButtonVisible(int buttonIdx, boolean isVisible) {
		try {
			Button button = (Button) buttonBar.getComponent(buttonIdx);
			if (button != null) {
				button.setVisible(isVisible);
			}
		} catch (Exception e) {
		}
	}

	public void setButtonEnabled(int buttonIdx, boolean isEnabled) {
		try {
			Button button = (Button) buttonBar.getComponent(buttonIdx);
			if (button != null) {
				button.setEnabled(isEnabled);
			}
		} catch (Exception e) {
		}
	}

}
