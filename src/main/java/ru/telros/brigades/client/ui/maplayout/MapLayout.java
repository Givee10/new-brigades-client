package ru.telros.brigades.client.ui.maplayout;

/**
 * MapLayout interface
 */
public interface MapLayout {
	Coordinates getCenter();

	void setCenter(Coordinates coordinates);

	int getZoom();

	void setZoom(int zoom);

	Object addMarker(Coordinates coordinates, String caption, String description, String iconUrl);

	Object addMarker(Marker marker);

	void addMarkerClickListener(MapMarkerClickListener listener);

	void addBoundsChangedListener(MapNotifyEventListener listener);

	void removeMarker(Object marker);

	void removeAllMarkers();

	Object addPolyline(Polyline polyline);

	void removePolyline(Object polyline);

	void removeAllPolylines();

	void goToLocation(Coordinates coordinates, String locationName);

	void setVisibleLayout(boolean visible, int width, double lat, double lon);
}
