package ru.telros.brigades.client.ui.setup;

import com.vaadin.data.provider.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.data.provider.HierarchicalQuery;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.TypeWorkDto;
import ru.telros.brigades.client.dto.TypeWorkItemDto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WorkStagesGrid extends TreeGrid<WorkStagesGrid.MergedTypeWork> {
    public WorkStagesGrid() {
//        setItems(root);
        initColumns();
        setDataProvider(getProvider());
        setSizeFull();
    }

    private void initColumns() {
        this.addColumn(e -> (e.getDescription())).setCaption("Описание").setId("desc");
        this.addComponentColumn(e -> {
            if (e.getType().equals(Type.CATEGORY) == false) {
                return null;
            } else {
                HorizontalLayout l = new HorizontalLayout();
                TextField text = new TextField();
                text.setValue(e.getNorm().toString());
                text.setWidth("100%");
                text.setEnabled(false);
                Button button = new Button(VaadinIcons.EDIT);
                button.addClickListener(clickEvent -> {
                    if (text.isEnabled()) {
                        Double value;
                        try {
                            value = Double.parseDouble(text.getValue());
                            e.setNorm(value);
                            button.setIcon(VaadinIcons.EDIT);
                            text.setEnabled(false);
                            BrigadesUI.getRestTemplate().updateTypeWork(e.toTypeWorkDto());
                        } catch (NumberFormatException x) {
                            Notification.show("Поле должно содержать число", Notification.Type.ERROR_MESSAGE);
                        }
                    } else {
                        button.setIcon(VaadinIcons.UPLOAD_ALT);
                        text.setEnabled(true);
                    }
                });
                text.setWidth("45px");
                l.addComponents(text, button);
                l.setMargin(false);
                l.setSpacing(false);
                l.setWidth("100%");
                l.setExpandRatio(text, 1);
                l.setExpandRatio(button, 1);
                return l;
            }

        }).setCaption("Норма, час").setId("Norm").setWidth(100);
        this.addComponentColumn(e -> {
            if (e.getType().equals(Type.DETAIL) == false) {
                return null;
            } else {
                CheckBox photoCheckbox = new CheckBox();
                photoCheckbox.setValue(e.getPhotoRequire());
                photoCheckbox.addValueChangeListener(valueChangeEvent -> {
                    e.setPhotoRequire(valueChangeEvent.getValue());
                    TypeWorkItemDto item = e.toTypeWorkItemDto();
                    BrigadesUI.getRestTemplate().updateTypeWorkItem(item, e.getParentId());
                });
                return photoCheckbox;
            }

        }).setCaption("Фото").setId("photoReq");

        addComponentColumn(e -> {
            if (e.getType().equals(Type.DETAIL) == false) {
                return null;
            } else {
                CheckBox callCheckbox = new CheckBox();
                callCheckbox.setValue(e.getCallRequire());
                callCheckbox.addValueChangeListener(valueChangeEvent -> {
                    e.setCallRequire(valueChangeEvent.getValue());
                    TypeWorkItemDto item = e.toTypeWorkItemDto();
                    BrigadesUI.getRestTemplate().updateTypeWorkItem(item, e.getParentId());
                });
                return callCheckbox;
            }
        }).setCaption("Звонок").setId("callReq");
        this.addComponentColumn(e -> {
            if (e.getType().equals(Type.DETAIL) == false) {
                return null;
            } else {
                CheckBox actCheckbox = new CheckBox();
                actCheckbox.setValue(e.getActRequire());
                actCheckbox.addValueChangeListener(valueChangeEvent -> {
                    e.setActRequire(valueChangeEvent.getValue());
                    TypeWorkItemDto item = e.toTypeWorkItemDto();
                    BrigadesUI.getRestTemplate().updateTypeWorkItem(item, e.getParentId());
                });
                return actCheckbox;
            }
        }).setCaption("Документ").setId("actReq");

        this.setHierarchyColumn("desc");
    }

    private AbstractBackEndHierarchicalDataProvider<MergedTypeWork, Void> getProvider() {
        return new AbstractBackEndHierarchicalDataProvider<MergedTypeWork, Void>() {
            @Override
            public boolean hasChildren(MergedTypeWork mergedTypeWork) {
                return mergedTypeWork.getType().equals(Type.CATEGORY) || mergedTypeWork.getType().equals(Type.SUBCATEGORY);
            }

            @Override
            public int getChildCount(HierarchicalQuery hierarchicalQuery) {
                return (int) fetchChildrenFromBackEnd(hierarchicalQuery).count();
            }

            @Override
            public Stream fetchChildren(HierarchicalQuery hierarchicalQuery) {

                return fetchChildrenFromBackEnd(hierarchicalQuery);
            }

            @Override
            protected Stream<MergedTypeWork> fetchChildrenFromBackEnd(HierarchicalQuery<MergedTypeWork, Void> hierarchicalQuery) {
                MergedTypeWork typeWork = hierarchicalQuery.getParent();

                if (typeWork != null) {
                    if (typeWork.getType().equals(Type.CATEGORY)) {
                        List<TypeWorkItemDto> rawList = BrigadesUI.getRestTemplate().getTypeWorkItems(typeWork.getId());
                        HashMap<Integer, MergedTypeWork> subcategoryCodesToSubcategories = new HashMap<>();
                        rawList.stream().
                                filter(e -> e.getDescription().equals(e.getCode())).
                                forEach(e -> {
                                    subcategoryCodesToSubcategories.put(Integer.parseInt(e.getCode()), new MergedTypeWork(e, typeWork.getId()));
                                });

                        HashSet<MergedTypeWork> output = rawList.stream().map(e -> {
                            Integer curItemCategoryCode = Integer.parseInt(e.getCode()) / 100 * 100;
//                            getLogger().debug("parsed "+curItemCategoryCode);
                            if (subcategoryCodesToSubcategories.keySet().contains(curItemCategoryCode)) {
                                return subcategoryCodesToSubcategories.get(curItemCategoryCode);
                            } else {
                                return new MergedTypeWork(e, typeWork.getId());
                            }
                        }).collect(Collectors.toCollection(HashSet::new));
                        return output.stream().sorted((o1, o2) -> o1.getCode().compareTo(o2.getCode()));
                        /*return BrigadesUI.getRestTemplate().getTypeWorkItems(typeWork.getId()).stream().map(e->{
                            return new MergedTypeWork(e,typeWork.getId());});*/
                    } else if (typeWork.getType().equals(Type.SUBCATEGORY)) {
//                        getLogger().debug(" - "+typeWork.getCode());
                        return BrigadesUI.getRestTemplate().getTypeWorkItems(typeWork.getParentId()).stream().filter(e -> {

                            Integer parent = Integer.parseInt(typeWork.getCode());
                            Integer child = Integer.parseInt(e.getCode());

                            if (!(e.getCode().equals(e.getDescription())) && (parent / 100 == child / 100)) {
                                return true;
                            } else {
                                return false;
                            }
                        }).map(e -> {
                            return new MergedTypeWork(e, typeWork.getId());
                        });
                    }
                }
                return BrigadesUI.getRestTemplate().getTypeWorks().stream().map(e -> new MergedTypeWork(e));
            }
        };
    }

    public enum Type {
        CATEGORY, SUBCATEGORY, DETAIL
    }

    //прочитал на форуме, что это единственный способ засунуть два класса в один грид
    public static class MergedTypeWork extends TypeWorkItemDto {

        Type type;
        long parentId;

        public MergedTypeWork() {

        }

        public TypeWorkDto toTypeWorkDto() {
            TypeWorkDto typeWorkDto = new TypeWorkDto();
            typeWorkDto.setId(this.getId());
            typeWorkDto.setGuid(this.getGuid());
            typeWorkDto.setCode(this.getCode());
            typeWorkDto.setDescription(this.getDescription());
            typeWorkDto.setNorm(this.getNorm());
            return typeWorkDto;
        }

        public TypeWorkItemDto toTypeWorkItemDto() {
            TypeWorkItemDto typeWorkItemDto = new TypeWorkItemDto();
            typeWorkItemDto.setId(this.getId());
            typeWorkItemDto.setGuid(this.getGuid());
            typeWorkItemDto.setCode(this.getCode());
            typeWorkItemDto.setDescription(this.getDescription());
            typeWorkItemDto.setNorm(this.getNorm());
            typeWorkItemDto.setActRequire(this.getActRequire());
            typeWorkItemDto.setCallRequire(this.getCallRequire());
            typeWorkItemDto.setPhotoRequire(this.getPhotoRequire());
            return typeWorkItemDto;
        }

        public MergedTypeWork(TypeWorkDto typeWorkDto) {

            setId(typeWorkDto.getId());
            setType(Type.CATEGORY);
            setGuid(typeWorkDto.getGuid());
            setCode(typeWorkDto.getCode());
            setDescription(typeWorkDto.getDescription());
            setNorm(typeWorkDto.getNorm());
            setPhotoRequire(null);
            setCallRequire(null);
            setActRequire(null);
        }

        public MergedTypeWork(TypeWorkItemDto typeWorkItemDto, long parentId) {
            setId(typeWorkItemDto.getId());
            setParentId(parentId);

            setGuid(typeWorkItemDto.getGuid());
            setCode(typeWorkItemDto.getCode());
            setDescription(typeWorkItemDto.getDescription());
            setNorm(typeWorkItemDto.getNorm());
            setPhotoRequire(typeWorkItemDto.getPhotoRequire());
            setCallRequire(typeWorkItemDto.getCallRequire());
            setActRequire(typeWorkItemDto.getActRequire());
            if (this.getCode().equals(this.getDescription())) {
                setType(Type.SUBCATEGORY);
            } else {
                setType(Type.DETAIL);
            }
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public long getParentId() {
            return parentId;
        }

        public void setParentId(long parentId) {
            this.parentId = parentId;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MergedTypeWork)) return false;
            if (!super.equals(o)) return false;
            MergedTypeWork that = (MergedTypeWork) o;
            return getId() == that.getId();
        }

        @Override
        public int hashCode() {
            return Math.toIntExact(this.getId());
        }
    }

}
