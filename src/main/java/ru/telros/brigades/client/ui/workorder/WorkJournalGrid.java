package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.dto.WorkJournalDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.ArrayList;
import java.util.List;

public class WorkJournalGrid extends BrigadesGrid<WorkJournalDto> {
	public WorkJournalGrid() {
		this(new ArrayList<>());
	}

	public WorkJournalGrid(List<WorkJournalDto> items) {
		super(WorkOrderConstants.getJournalColumns(), items);
		setStyleGenerator(item -> (item != null) ? WorkOrderConstants.getEventStyle(item) : null);
	}

	@Override
	protected void onClick(WorkJournalDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(WorkJournalDto entity, Column column) {
		if (entity != null) {
			WorkDto work = BrigadesUI.getRestTemplate().getWork(entity.getId());
			WorkOrderLayout.showWorkOrder(work);
		}
	}
}
