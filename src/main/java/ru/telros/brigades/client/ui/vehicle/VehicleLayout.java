package ru.telros.brigades.client.ui.vehicle;

import com.vaadin.server.Responsive;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.ExcelDataList;
import ru.telros.brigades.client.dto.VehicleDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.vehicle.ShowVehicleEvent;
import ru.telros.brigades.client.event.vehicle.UpdateVehicleEvent;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.BrigadesStatistic;
import ru.telros.brigades.client.ui.component.FullSizeVerticalLayout;
import ru.telros.brigades.client.ui.component.ReadOnlyTextField;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static ru.telros.brigades.client.MessageManager.msg;

public class VehicleLayout extends FullSizeVerticalLayout {
	private final LinkedHashMap<String, String> legend = new LinkedHashMap<String, String>();
	private final VehicleGrid vanGrid = new VehicleGrid(VehicleColumn.getVanColumns(), new ArrayList<>());
	private final VehicleGrid specialVehicleGrid = new VehicleGrid(VehicleColumn.getSpecialColumns(), new ArrayList<>());
	private final ExcelDataGrid excelDataGrid = new ExcelDataGrid(VehicleColumn.getSpecialVehicleColumns(), new ArrayList<>());
	private ReadOnlyTextField number = new ReadOnlyTextField("Число ТС (План АИС «Бригады»)");
	private ReadOnlyTextField address = new ReadOnlyTextField("На адресах ТС");
	private ReadOnlyTextField vans = new ReadOnlyTextField("Фургоны");
	private ReadOnlyTextField rest = new ReadOnlyTextField("Другие ТС");
	private BrigadesStatistic statistic = new BrigadesStatistic(number, address, vans, rest);
	private Boolean updateVisible = true;

	public VehicleLayout() {
		setMargin(false);
		setSpacing(false);

		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		TabSheet tabSheet = new TabSheet();
		tabSheet.setSizeFull();
		tabSheet.addTab(vanGrid, "Фургоны");
		//tabSheet.addTab(specialVehicleGrid, "Специальные и строительные ТС");
		tabSheet.addTab(excelDataGrid, "Специальные и строительные ТС");

		statistic.isMaximized(false);
		//addComponent(statistic);
		addComponentAndRatio(tabSheet, 1);

		//update();
	}

	public void update() {
		if (updateVisible) {
			//vanGrid.update(BrigadesUI.getRestTemplate().getVehicles());
			//specialVehicleGrid.update(BrigadesUI.getRestTemplate().getVehicles());
			ExcelDataList currentExcelDataList = BrigadesUI.getCurrentExcelDataList();
			if (currentExcelDataList != null) {
				excelDataGrid.setItems(currentExcelDataList.getList());
			}
		}
	}

	public static void showOnMap(String type, String model, String number) {
		VehicleDto vehicleDto = new VehicleDto();
		vehicleDto.setType(type);
		vehicleDto.setModel(model);
		vehicleDto.setNumber(number);
		BrigadesEventBus.post(new ShowVehicleEvent(vehicleDto, UI.getCurrent()));
	}

	public static void showOnMap(VehicleDto vehicleDto) {
		BrigadesEventBus.post(new ShowVehicleEvent(vehicleDto, UI.getCurrent()));
	}

	public static void refresh() {
		BrigadesEventBus.post(new UpdateVehicleEvent(UI.getCurrent()));
	}

	public void setStatisticVisible(boolean visible) {
		statistic.isMaximized(visible);
	}

	public void addToolBarItems(BrigadesPanel panel) {
		panel.addToolbarItem("", "Добавить спецтехнику", BrigadesTheme.ICON_ADD, (MenuBar.Command) menuItem -> addVehicle());
		panel.addToolbarItem("", "Загрузить план из файла", BrigadesTheme.ICON_UPLOAD, (MenuBar.Command) menuItem -> createPlan());
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, legend);
		panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, (MenuBar.Command) menuItem -> update());
	}

	private void addVehicle() {

	}

	private void createPlan() {
		EquipmentExcelWindow.open();
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		update();
	}
}
