package ru.telros.brigades.client.ui.attachment;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.ui.component.CustomComboBox;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.component.CustomUpload;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.FileUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static ru.telros.brigades.client.MessageManager.msg;

public class AttachmentUploadWindow extends WindowWithButtons implements HasLogger {
	private File file;
	private Map<Class, Long> sourceMap;

	private CustomComboBox<AttachmentTypeDto> type = new CustomComboBox<>("Тип");
	private CustomTextField field = new CustomTextField("Описание");
	private CheckBox mobile = new CheckBox("Передать на планшет");
	private CheckBox hl = new CheckBox("Передать в ГЛ");
	private CustomUpload customUpload = new CustomUpload();
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	public AttachmentUploadWindow(Map<Class, Long> sourceMap) {
		super("Загрузка вложения", 800, -1);
		this.sourceMap = sourceMap;
		type.setItems(restTemplate.getAttachmentTypes());
		type.setRequiredIndicatorVisible(true);
		setResizable(false);

		HorizontalLayout components = CompsUtil.getHorizontalWrapperNoMargin(field, type);
		HorizontalLayout checkBoxes = CompsUtil.getHorizontalWrapperNoMargin(hl, mobile);
		checkBoxes.setComponentAlignment(mobile, Alignment.BOTTOM_LEFT);
		checkBoxes.setComponentAlignment(hl, Alignment.BOTTOM_LEFT);
		addToContent(CompsUtil.getVerticalWrapperNoMargin(customUpload, components, checkBoxes));
		addToButtonBar(msg("button.send"), event -> send());
		addToButtonBar(msg("button.close"), event -> close());
		addCloseListener(closeEvent -> {
			if (file != null)
				getLogger().debug("File deleted: {}", file.delete());
		});
	}

	private void send() {
		file = customUpload.getFile();
		String fileName = customUpload.getFileName();
		if (file != null) {
			AttachmentTypeDto typeValue = type.getValue();
			if (typeValue != null) {
				String type = typeValue.getName();
				String description = field.getValue();
				Boolean mobile = this.mobile.getValue();
				AttachmentDto attachmentDto = (sourceMap.containsKey(WorkDto.class)) ?
						restTemplate.postWorkAttachment(sourceMap.get(WorkDto.class), file, fileName, type, description, mobile) :
						restTemplate.postRequestAttachment(sourceMap.get(RequestDto.class), file, fileName, type, description, mobile);
				if (attachmentDto != null) {
					if (sourceMap.containsKey(WorkDto.class)) {
						if (hl.getValue()) {
							RequestDto requestDto = restTemplate.getWorkRequest(sourceMap.get(WorkDto.class));
							if (requestDto != null) {
								sendAttachmentToHL(requestDto, attachmentDto);
							}
						}
						WorkOrderLayout.refresh();
					} else {
						if (hl.getValue()) {
							RequestDto requestDto = restTemplate.getRequest(sourceMap.get(RequestDto.class));
							if (requestDto != null) {
								sendAttachmentToHL(requestDto, attachmentDto);
							}
						}
						RequestLayout.refresh();
					}
					close();
				} else Notification.show("Произошла ошибка при отправке файла на сервер", Notification.Type.ERROR_MESSAGE);
			} else Notification.show("Выберите тип приложения", Notification.Type.ERROR_MESSAGE);
		} else Notification.show("Файл не выбран", Notification.Type.ERROR_MESSAGE);
	}

	private void sendAttachmentToHL(RequestDto requestDto, AttachmentDto attachmentDto) {
		if (!StringUtil.isNull(requestDto.getExternalNumber())) {
			try {
				IncAttach incAttach = new IncAttach();
				incAttach.setpIncidentId(requestDto.getNumber());
				incAttach.setpIncidentNumber(requestDto.getExternalNumber());
				incAttach.setpFileName(attachmentDto.getFileName());
				incAttach.setpFileDescr(attachmentDto.getDescription());
				incAttach.setpMimeType(FileUtil.detectFileMIMEType(file));
				incAttach.setpFileData(StringUtil.encodeFileToBase64(file));
				restTemplate.sendAttToHotLine(incAttach);
				attachmentDto.setState(AttachmentDto.Status.SENT.getCode());
				restTemplate.updateAttachment(attachmentDto);
			} catch (IOException e) {
				getLogger().error(e.getMessage());
			}
		}
	}

	public static void open(Map<Class, Long> sourceMap) {
		final Window window = new AttachmentUploadWindow(sourceMap);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}
}
