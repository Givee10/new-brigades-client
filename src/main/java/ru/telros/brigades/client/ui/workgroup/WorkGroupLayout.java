package ru.telros.brigades.client.ui.workgroup;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.server.Responsive;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.BrigadeJournalDto;
import ru.telros.brigades.client.dto.PagesDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.workgroup.UpdateWorkGroupsEvent;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.BrigadesStatistic;
import ru.telros.brigades.client.ui.component.FullSizeVerticalLayout;
import ru.telros.brigades.client.ui.component.ReadOnlyTextField;
import ru.telros.brigades.client.ui.workplan.WorkGroupPlanWindow;

import java.util.LinkedHashMap;

import static ru.telros.brigades.client.MessageManager.msg;

public class WorkGroupLayout extends FullSizeVerticalLayout {
	private final LinkedHashMap<String, String> legend = new LinkedHashMap<String, String>();

	private MenuBar.MenuItem miAdd, miEdit, miView, miDelete, miUpdate;
/*
	private WorkGroupGrid workGroupGrid = new WorkGroupGrid(WorkGroupColumn.getMainColumns(), new ArrayList<>()) {
		@Override
		protected void onClick(BrigadeDto entity, FilterGrid.Column column) {
			onSelectWorkGroup(entity);
		}

		@Override
		protected void onDoubleClick(BrigadeDto entity, FilterGrid.Column column) {
			showWorkGroup(entity);
		}
	};
	private GridContextMenu<BrigadeDto> contextMenu = new GridContextMenu<>(workGroupGrid);
*/
	private BrigadeJournalGrid journalGrid = new BrigadeJournalGrid();
	//private WorkGroupJournalGrid journalGrid = new WorkGroupJournalGrid();
	private GridContextMenu<BrigadeJournalDto> contextMenu = new GridContextMenu<>(journalGrid);

	private ReadOnlyTextField working = new ReadOnlyTextField("Число работающих бригад");
	private ReadOnlyTextField address = new ReadOnlyTextField("На адресах");
	private ReadOnlyTextField camera = new ReadOnlyTextField("Работает МАВР");
	private BrigadesStatistic statistic = new BrigadesStatistic(working, address, camera);
	private Boolean updateVisible = true;

	public WorkGroupLayout() {
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		setMargin(false);
		setSpacing(false);

		contextMenu.addGridHeaderContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridBodyContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridFooterContextMenuListener(event -> selectGridItem(event.getItem()));

		statistic.isMaximized(false);
		//addComponent(statistic);

		//addComponentAndRatio(workGroupGrid, 1);
		journalGrid.sort(journalGrid.getColumn(WorkGroupColumn.getNameId()));
		journalGrid.setStyleGenerator(item -> (item != null) ? WorkGroupColumn.getEventStyle(item) : null);
		addComponentAndRatio(journalGrid, 1);

		//update();
	}

	public static void createWorkGroup() {
		WorkGroupWindow.open(new BrigadeJournalDto());
	}

	public static void showWorkGroup(BrigadeJournalDto workGroup) {
		if (workGroup != null) {
			WorkGroupWindow.open(workGroup);
		} else {
			Notification.show("Ошибка при получении данных бригады", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void showPlan(BrigadeJournalDto workGroup) {
		if (workGroup != null) {
			BrigadeDto brigade = BrigadesUI.getRestTemplate().getBrigade(workGroup.getId());
			WorkGroupPlanWindow.open(brigade, true);
		} else {
			Notification.show("Ошибка при получении данных бригады", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void showPlan(BrigadeDto workGroup) {
		if (workGroup != null) {
			WorkGroupPlanWindow.open(workGroup, true);
		} else {
			Notification.show("Ошибка при получении данных бригады", Notification.Type.ERROR_MESSAGE);
		}
	}

	public void update() {
		//journalGrid.refreshAll();
		if (updateVisible) {
			Long countBrigades = BrigadesUI.getRestTemplate().countBrigades();
			if (countBrigades != null && countBrigades != 0) {
				PagesDto<BrigadeJournalDto> brigadesForJournal = BrigadesUI.getRestTemplate().getBrigadesForJournal(0, countBrigades.intValue());
				journalGrid.update(brigadesForJournal.getCurrentContent());
			}
		}
	}

	private void selectGridItem(BrigadeJournalDto journalDto) {
		contextMenu.removeItems();
		MenuBar.Command commandAdd = (MenuBar.Command) menuItem -> createWorkGroup();
		MenuBar.Command commandRefresh = (MenuBar.Command) menuItem -> update();

		if (journalDto != null) {
			journalGrid.select(journalDto);
			MenuBar.Command commandShow = (MenuBar.Command) menuItem -> showPlan(journalDto);
			MenuBar.Command commandEdit = (MenuBar.Command) menuItem -> showWorkGroup(journalDto);
			//MenuBar.Command commandDelete = (MenuBar.Command) menuItem -> deleteWorkGroup(workGroup);

			contextMenu.addItem(msg("menu.workgroup.edit"), BrigadesTheme.ICON_EDIT, commandEdit);
			contextMenu.addItem(msg("workgroupwindow.button.workplan"), BrigadesTheme.ICON_WORKPLAN, commandShow);
			//contextMenu.addSeparator();
			//contextMenu.addItem(msg("menu.workgroup.delete"), BrigadesTheme.ICON_CLOSE, commandDelete);
			contextMenu.addSeparator();

		} else journalGrid.deselectAll();
		//contextMenu.addItem(msg("menu.workgroup.new"), BrigadesTheme.ICON_ADD, commandAdd);
		//contextMenu.addSeparator();
		contextMenu.addItem(msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, commandRefresh);
	}

	public void addToolBarItems(BrigadesPanel panel) {
		//miAdd = panel.addToolbarItem("", msg("menu.workgroup.new"), BrigadesTheme.ICON_ADD, (MenuBar.Command) menuItem -> createWorkGroup());
		miEdit = panel.addToolbarItem("", msg("menu.workgroup.edit"), BrigadesTheme.ICON_EDIT, (MenuBar.Command) menuItem -> doEditWorkGroup());
		//miEdit.setEnabled(false);
		miView = panel.addToolbarItem("", msg("workgroupwindow.button.workplan"), BrigadesTheme.ICON_WORKPLAN, (MenuBar.Command) menuItem -> doShowPlan());
		//miView.setEnabled(false);
		//miDelete = panel.addToolbarItem("", msg("menu.workgroup.delete"), BrigadesTheme.ICON_CLOSE, (MenuBar.Command) menuItem -> doDeleteWorkGroup());
		//miDelete.setEnabled(false);
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, legend);
		miUpdate = panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, (MenuBar.Command) menuItem -> update());
		//miUpdate.setEnabled(workGroupGrid.getItems().size() > 0);
	}

	private void deleteWorkGroup(BrigadeJournalDto workGroup) {
		if (workGroup != null) {
			WorkGroupWindow workGroupWindow = WorkGroupWindow.open(workGroup, true);
			workGroupWindow.addCloseListener((Window.CloseListener) e -> {
				if (workGroupWindow.isDeletionConfirmed()) {
					BrigadesUI.getRestTemplate().deleteBrigade(workGroup.getId());
				}
			});
			update();
		} else {
			Notification.show("Ошибка при получении данных бригады", Notification.Type.ERROR_MESSAGE);
		}
	}

	private void doEditWorkGroup() {
		BrigadeJournalDto selectedRow = journalGrid.getSelectedRow();
		if (selectedRow != null) {
			showWorkGroup(selectedRow);
		}
	}

	private void doShowPlan() {
		BrigadeJournalDto selectedRow = journalGrid.getSelectedRow();
		if (selectedRow != null) {
			showPlan(selectedRow);
		}
	}

	private void doDeleteWorkGroup() {
		BrigadeJournalDto selectedRow = journalGrid.getSelectedRow();
		if (selectedRow != null) {
			deleteWorkGroup(selectedRow);
		}
	}

	private void refreshList() {
		BrigadesEventBus.post(new UpdateWorkGroupsEvent());
		//update();
	}

	public void setStatisticVisible(boolean visible) {
		statistic.isMaximized(visible);
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		update();
	}
}
