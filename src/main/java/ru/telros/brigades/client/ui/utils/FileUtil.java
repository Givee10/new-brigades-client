package ru.telros.brigades.client.ui.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class FileUtil {
	private final static Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);
	private final static String filesDirectory = "./files/";
	public final static String reportsDirectory = filesDirectory + "reports/";

	public static String detectFileMIMEType(MultipartFile mf) {
		String mimeType = mf.getContentType();
		try {
			TikaConfig tika = new TikaConfig();
			Detector detector = tika.getDetector();
			Metadata metadata = new Metadata();
			metadata.set(Metadata.RESOURCE_NAME_KEY, mf.getOriginalFilename());
			MediaType detect = detector.detect(TikaInputStream.get(mf.getInputStream()), metadata);
			mimeType = detect.getBaseType().toString();
		} catch (TikaException | IOException e) {
			LOGGER.error(e.getMessage());
		}
		if (mimeType == null) {
			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			mimeType = mimeTypesMap.getContentType(mf.getOriginalFilename());
		}
		return mimeType;
	}

	public static String detectFileMIMEType(File file) {
		String mimeType = null;
		try {
			TikaConfig tika = new TikaConfig();
			Detector detector = tika.getDetector();
			Metadata metadata = new Metadata();
			metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
			MediaType detect = detector.detect(TikaInputStream.get(file.toPath()), metadata);
			mimeType = detect.getBaseType().toString();
		} catch (TikaException | IOException e) {
			LOGGER.error(e.getMessage());
		}
		if (mimeType == null) {
			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			mimeType = mimeTypesMap.getContentType(file.getName());
		}
		return mimeType;
	}

	public static String detectFileMIMEType(String filePath) {
		return detectFileMIMEType(new File(filePath));
	}

	public static String detectFileMIMEType(InputStream is, String fileName) {
		String mimeType = null;
		try {
			TikaConfig tika = new TikaConfig();
			Detector detector = tika.getDetector();
			Metadata metadata = new Metadata();
			metadata.set(Metadata.RESOURCE_NAME_KEY, fileName);
			MediaType detect = detector.detect(TikaInputStream.get(is), metadata);
			mimeType = detect.getBaseType().toString();
		} catch (TikaException | IOException e) {
			LOGGER.error(e.getMessage());
		}
		if (mimeType == null) {
			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			mimeType = mimeTypesMap.getContentType(fileName);
		}
		return mimeType;
	}

	public static FileReceiver getReceiver() {
		return new FileReceiver(getDirectory());
	}

	public static Path getDirectory() {
		String currentDate = DatesUtil.formatFile(LocalDateTime.now());
		Path directory = Paths.get(filesDirectory, currentDate);
		if (Files.notExists(directory)) {
			try {
				Files.createDirectory(directory);
			} catch (IOException e) {
				LOGGER.error("Cannot create directory: {}", directory);
			}
		}
		return directory;
	}

	public static String getDirectoryFileName(String fileName) {
		return Paths.get(getDirectory().toString(), changeFileName(fileName)).toString();
	}

	public static String getReportFileName(String fileName) {
		return Paths.get(reportsDirectory, fileName).toString();
	}

	public static String changeFileName(String fileName) {
		return System.currentTimeMillis() + "." + FilenameUtils.getExtension(fileName);
	}

	public static enum MIMEType {
		TEXT_PLAIN("text/"), IMAGE("image/"), VIDEO("video/"), PDF("application/pdf"), ZIP("application/zip"),
		MSWORD("application/msword"), MSWORDX("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
		MSEXCEL("application/vnd.ms-excel"), MSEXCELX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		private String mimeType;

		private MIMEType(String mimeType) {
			this.mimeType = mimeType;
		}

		public String getMIMEType() {
			return mimeType;
		}
	}
}
