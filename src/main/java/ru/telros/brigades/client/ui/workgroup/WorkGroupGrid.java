package ru.telros.brigades.client.ui.workgroup;

import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class WorkGroupGrid extends BrigadesGrid<BrigadeDto> {
	public WorkGroupGrid(List<GridColumn<BrigadeDto>> columns, List<BrigadeDto> items) {
		super(columns, items);
	}

	@Override
	protected void onClick(BrigadeDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(BrigadeDto entity, Column column) {

	}
}
