package ru.telros.brigades.client.ui.workorder;

import com.vaadin.data.BeanValidationBinder;
import ru.telros.brigades.client.dto.EODto;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.util.ArrayList;

public class EOGrid extends BrigadesGrid<EODto> {
	public EOGrid() {
		super(TOIRConstants.getEOColumns(), new ArrayList<>());
		setHeightByRows(5);
	}

	protected void withoutEditor() {
		getColumn(TOIRConstants.EO_LENGTH_.getId()).setWidth(100);
		getColumn(TOIRConstants.EO_DIAMETER_.getId()).setWidth(100);
		getColumn(TOIRConstants.EO_MATERIAL_.getId()).setWidth(100);
	}

	protected void enableEditor() {
		DisabledTextField eoType = new DisabledTextField("Тип ЭО");
		CustomTextField eoLength = new CustomTextField("Длина ЭО");
		CustomTextField eoDiameter = new CustomTextField("Диаметр ЭО");
		CustomTextField eoMatType = new CustomTextField("Тип материала ЭО");

		BeanValidationBinder<EODto> binder = new BeanValidationBinder<>(EODto.class);
		binder.bind(eoType, EODto::getEoType, EODto::setEoType);
		binder.bind(eoLength, EODto::getEoLength, EODto::setEoLength);
		binder.bind(eoDiameter, EODto::getEoDiametr, EODto::setEoDiametr);
		binder.bind(eoMatType, EODto::getEoMatType, EODto::setEoMatType);

		getEditor().setBinder(binder).setEnabled(true).setSaveCaption("Сохранить").setCancelCaption("Отмена");
		getEditor().setErrorGenerator((map, binderValidationStatus) -> "Ошибка при введении данных");
		getEditor().addSaveListener(editorSaveEvent -> {
			EODto bean = editorSaveEvent.getBean();
			getLogger().debug(StringUtil.writeValueAsString(bean));
		});

		getColumn(TOIRConstants.EO_LENGTH.getId()).setEditorComponent(eoLength);
		getColumn(TOIRConstants.EO_DIAMETER.getId()).setEditorComponent(eoDiameter);
		getColumn(TOIRConstants.EO_MATERIAL.getId()).setEditorComponent(eoMatType);
	}

	@Override
	protected void onDoubleClick(EODto entity, Column column) {

	}

	@Override
	protected void onClick(EODto entity, Column column) {

	}
}
