package ru.telros.brigades.client.ui.maplayout;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.VerticalLayout;

import java.util.Timer;
import java.util.TimerTask;

public class YandexMapLayout extends CssLayout implements MapLayout {
	private static final String URL_BALTIKA = "http://172.16.126.17/WebUip/mapforms/telros/Default.aspx";
	//private static final String URL_BALTIKA_USERNAME_E = "AIS_Brigady";
	//private static final String URL_BALTIKA_PASSWORD_E = "!qB%404NuY17as%40";
	private static final String URL_BALTIKA_USERNAME_E = "K8z8YJozETc9349aLLa43YneURtTQMHePEmrFpZfCVC%2F%2Bg%2FE9dpjBTUu%2FKBWgxHftM09vrxrIY0%2Bbn3BUTaHXrnKoTW4vlYxyyDqGAbXNdzXtt5TWWycqI1g4z5Yy37bpoNwvnCQC8NAyb7959JfMnT6eNxchWbF0ZVAOVvozKY%3D";
	private static final String URL_BALTIKA_PASSWORD_E = "Gfa1rJSlAagwncC11rWatzsevO3yG6MFCjSZw6lhpWVt0TS1osxudrj3sB2gbFy3i7MlaQ%2F9AqQclbWX3gxsRcKzd5IjpNIjsQuxVAMSgP55xpETof3RzInAyZ5J6lrXKeyVRjWR0olawFkp4vrL2uboh9KkA%2FCFcQrgmMxOByk%3D";
	private final YandexMapComponent map = new YandexMapComponent();
	private final VerticalLayout layout = new VerticalLayout();
	private final BrowserFrame frame = new BrowserFrame();
	private Timer locationTimer = null;

	public YandexMapLayout(int zoom) {
		this(null, zoom);
	}

	public YandexMapLayout(String caption, int zoom) {
		super();
		setCaption(caption);
		setSizeFull();
		//addComponent(map);
		frame.setSizeFull();
		layout.addComponentsAndExpand(frame);
		layout.setSizeFull();
		layout.setMargin(false);
		layout.setSpacing(false);
		layout.setVisible(false);
		AbsoluteLayout absoluteLayout = new AbsoluteLayout();
		absoluteLayout.addComponent(map);
		absoluteLayout.addComponent(layout);
		addComponent(absoluteLayout);
		map.setZoom(zoom);
		map.setCenter(new Coordinates(59.853582, 30.255025));
		map.setSizeFull();
	}

	@Override
	public int getZoom() {
		return map.getZoom();
	}

	@Override
	public void setZoom(int zoom) {
		map.setZoom(zoom);
	}

	@Override
	public Coordinates getCenter() {
		return map.getCenter();
	}

	@Override
	public void setCenter(Coordinates coordinates) {
		map.setCenter(coordinates);
	}

	@Override
	public Object addMarker(Coordinates coordinates, String caption, String description, String iconUrl) {
		Marker m = new Marker(caption, description, iconUrl, coordinates.getLat(), coordinates.getLon());
		m.setIconPreset("islands#icon");
		return this.addMarker(m);
	}

	@Override
	public Object addMarker(Marker marker) {
		map.addMarker(marker);
		return marker;
	}

	@Override
	public void addMarkerClickListener(MapMarkerClickListener listener) {
		map.addMarkerClickListener(listener);
	}

	@Override
	public void addBoundsChangedListener(MapNotifyEventListener listener) {
		map.addBoundsChangedListener(listener);
	}

	@Override
	public void removeMarker(Object marker) {
		if (marker instanceof Marker) {
			map.removeMarker((Marker) marker);
		}
	}

	@Override
	public void removeAllMarkers() {
		map.removeAllMarkers();
	}

	@Override
	public Object addPolyline(Polyline polyline) {
		return map.addPolyline(polyline);
	}

	@Override
	public void removePolyline(Object polyline) {
		if (polyline instanceof Polyline) {
			map.removePolyline((Polyline) polyline);
		}
	}

	@Override
	public void removeAllPolylines() {
		map.removeAllPolylines();
	}

	@Override
	public void goToLocation(final Coordinates coordinates, final String locationName) {
		map.showBalloon(coordinates, locationName);
		if (locationTimer != null) {
			locationTimer.cancel();
			locationTimer = null;
		}
		locationTimer = new Timer();
		locationTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				map.panTo(coordinates, 500);
			}
		}, 200);
	}

	@Override
	public void setVisibleLayout(boolean visible, int width, double lat, double lon) {
		layout.setVisible(visible);
		String url = URL_BALTIKA + "?lat=" + lat + "&lon=" + lon + "&width=" + width + "&UserNameE=" + URL_BALTIKA_USERNAME_E + "&PasswordE=" + URL_BALTIKA_PASSWORD_E;
		frame.setSource(new ExternalResource(url));
	}

	public YandexMapComponent getMap() {
		return map;
	}

}

