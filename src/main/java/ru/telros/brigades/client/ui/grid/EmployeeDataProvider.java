package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.EmployeeDto;

import java.util.stream.Stream;

public class EmployeeDataProvider extends AbstractBackEndDataProvider<EmployeeDto, String> implements HasLogger {
	private static final long serialVersionUID = 1L;

	@Override
	protected Stream<EmployeeDto> fetchFromBackEnd(Query<EmployeeDto, String> query) {
		getLogger().debug("fetchFromBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		query.getSortOrders().forEach(querySortOrder -> getLogger().debug("Sorted {}, Direction {}",
				querySortOrder.getSorted(), querySortOrder.getDirection()));
		return BrigadesUI.getRestTemplate().getEmployees(query.getOffset(), query.getLimit()).stream();
	}

	@Override
	protected int sizeInBackEnd(Query<EmployeeDto, String> query) {
		getLogger().debug("sizeInBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		return BrigadesUI.getRestTemplate().countEmployees();
	}

	@Override
	public Object getId(EmployeeDto item) {
		return item.getId();
	}
}
