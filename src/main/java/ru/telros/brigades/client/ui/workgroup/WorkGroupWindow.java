package ru.telros.brigades.client.ui.workgroup;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.data.Binder;
import com.vaadin.server.SerializablePredicate;
import com.vaadin.ui.*;
import org.vaadin.inputmask.InputMask;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.event.workgroup.UpdateWorkGroupsEvent;
import ru.telros.brigades.client.ui.component.CustomComboBox;
import ru.telros.brigades.client.ui.component.CustomTextArea;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.person.PersonColumn;
import ru.telros.brigades.client.ui.person.PersonGrid;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.vehicle.VehicleLayout;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.window.YesNoWindow;
import ru.telros.brigades.client.ui.workorder.WorkOrderConstants;
import ru.telros.brigades.client.ui.workorder.WorkOrderGrid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

public class WorkGroupWindow extends WindowWithButtons implements HasLogger {
	private BrigadeJournalDto workGroup;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	private Binder<BrigadeDto> brigadeBinder = new Binder<>();
	private Binder<BrigadeJournalDto> binder = new Binder<>();
	private DisabledTextField name = new DisabledTextField("Тип");
	private CustomComboBox<OrganizationDto> filial = new CustomComboBox<>("Филиал");
	private CustomComboBox<OrganizationDto> territory = new CustomComboBox<>("ТУВ");
	private CustomComboBox<OrganizationDto> region = new CustomComboBox<>("РВ");
	private CustomTextArea description = new CustomTextArea(msg("table.workgroup.description"));
	private DisabledTextField vehicle = new DisabledTextField(msg("table.vehicle"));
	private DisabledTextField manager = new DisabledTextField(msg("table.workgroup.manager"));
	private CustomTextField phone = new CustomTextField("Телефон мастера");
	private CustomComboBox<EmployeeDto> managersCombo = new CustomComboBox<>(msg("table.workgroup.manager"));

	private boolean isDeleteMode, isDeletionConfirmed;

	private WorkGroupWindow(BrigadeJournalDto workGroup, boolean isDeleteMode) {
		super("Бригада", 1000, -1);
		this.workGroup = workGroup;

		if (workGroup.getId() == null) {
			List<EmployeeDto> employees = restTemplate.getAllEmployees();
			managersCombo.setItems(employees);
		}

		this.isDeleteMode = isDeleteMode;

		initMenu();
		initFields();
		addToContent(buildContent());
		bindFields();

		// Отключение ESC
		removeAllCloseShortcuts();
	}

	public static void open(BrigadeJournalDto workGroup) {
		Window window = new WorkGroupWindow(workGroup, false);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	public static WorkGroupWindow open(BrigadeJournalDto workGroup, boolean isDeleteMode) {
		WorkGroupWindow window = new WorkGroupWindow(workGroup, isDeleteMode);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
		return window;
	}

	public boolean isDeletionConfirmed() {
		return isDeletionConfirmed;
	}

	private void initMenu() {
		addToButtonBar(msg("workgroupwindow.button.workplan"), $ -> openWorkGroupPlan());
		//addToButtonBar("Проинформировать бригаду", $ -> sendMessage());
		addSpaceToButtonBar();
		//setButtonVisible(0, (workGroup.getId() != null && !workGroup.getWorkOrdersId().isEmpty()));
		if (isDeleteMode) {
			addToButtonBar(msg("button.delete"), $ -> onClickDeleteButton());
			addToButtonBar(msg("button.cancel"), $ -> close());
		} else {
			addToButtonBar(msg("button.save"), $ -> save());
			addToButtonBar(msg("button.close"), $ -> close());
		}
	}

	private void onClickDeleteButton() {
		YesNoWindow confirmation = YesNoWindow.open(msg("warning"),
				msg("workgroupwindow.msg.deleteworkgroupwarning", new Object[]{workGroup.toString()}));
		confirmation.addCloseListener((Window.CloseListener) e -> {
			if (confirmation.isModalResult()) {
				isDeletionConfirmed = true;
			}
			close();
		});
	}

	private void initFields() {
		InputMask.addTo(phone, "(999) 999-99-99");
		name.setEnabled(!isDeleteMode);
		description.setEnabled(!isDeleteMode);
		manager.setEnabled(!isDeleteMode);
		vehicle.setEnabled(!isDeleteMode);
		filial.setEnabled(!isDeleteMode);
		territory.setEnabled(!isDeleteMode);
		region.setEnabled(!isDeleteMode);

		description.setEnabled(!isDeleteMode);
		initLists();
	}

	private void initLists() {
		List<OrganizationDto> filials = restTemplate.getOrganizationsByType("FILIAL");
		filial.setItems(filials);
		filial.addValueChangeListener(valueChangeEvent -> {
			OrganizationDto value = valueChangeEvent.getValue();
			territory.setItems(Collections.emptyList());
			territory.clear();
			region.setItems(Collections.emptyList());
			region.clear();
			if (value != null) {
				List<OrganizationDto> children = restTemplate.getChildOrganizations(value.getId(), "TERRITORY");
				territory.setItems(children);
			}
		});
		territory.addValueChangeListener(valueChangeEvent -> {
			OrganizationDto value = valueChangeEvent.getValue();
			region.setItems(Collections.emptyList());
			region.clear();
			if (value != null) {
				List<OrganizationDto> children = restTemplate.getChildOrganizations(value.getId(), "REGION");
				region.setItems(children);
			}
		});

		List<OrganizationDto> organizations = restTemplate.getOrganizations();
		OrganizationDto brigadeFilial = organizations.stream().filter(organizationDto ->
				organizationDto.getName().equals(workGroup.getFilial())).findFirst().orElse(null);
		OrganizationDto brigadeTerritory = organizations.stream().filter(organizationDto ->
				organizationDto.getName().equals(workGroup.getTerritory())).findFirst().orElse(null);
		OrganizationDto brigadeRegion = organizations.stream().filter(organizationDto ->
				organizationDto.getName().equals(workGroup.getRegion())).findFirst().orElse(null);
		filial.setValue(brigadeFilial);
		territory.setValue(brigadeTerritory);
		region.setValue(brigadeRegion);
	}

	private Component buildContent() {
		VerticalLayout content = CompsUtil.getVerticalWrapperNoMargin();
		content.addComponent(CompsUtil.getHorizontalWrapperNoMargin(name, vehicle));
		content.addComponent(CompsUtil.getHorizontalWrapperNoMargin(filial, territory, region));
		content.addComponent(CompsUtil.getHorizontalWrapperNoMargin(
				workGroup.getId() == null ? managersCombo : manager, phone));

		TabSheet tabSheet = new TabSheet();
		tabSheet.addStyleName(BrigadesTheme.TABSHEET_FRAMED);
		tabSheet.addTab(buildPersonGrid(), "Бригада");
		tabSheet.addTab(buildWorkOrderGrid(), "Работы");
		tabSheet.addTab(buildEquipmentGrid(), "Спецтехника");
		tabSheet.addTab(buildMechanizationGrid(), "Средства малой механизации");
		content.addComponent(tabSheet);
		return content;
	}
	//TODO-k СПРОСИТЬ НАСЧЁТ Случая отсутствия плана
	private Component buildPersonGrid() {
		List<EmployeeBrigadeDto> personsList = workGroup.getId() != null && workGroup.getIdBrigadePlan() != null ?
				restTemplate.getBrigadeEmployeesFromPlan(workGroup.getId(), workGroup.getIdBrigadePlan()) :
				restTemplate.getBrigadeEmployees(workGroup.getId());
		PersonGrid personGrid = new PersonGrid(PersonColumn.getMainColumns(), personsList);
		personGrid.hideColumns(PersonColumn.ID.getId(), PersonColumn.GROUP.getId());
		personGrid.setHeight(200, Unit.PIXELS);
		return personGrid;
	}

	private Component buildWorkOrderGrid() {
		List<WorkDto> brigadeWorks = workGroup.getId() == null ? new ArrayList<>() :
				restTemplate.getBrigadeWorks(workGroup.getId());
		WorkOrderGrid workOrderGrid = new WorkOrderGrid(WorkOrderConstants.getWorkGroupColumns(), brigadeWorks);
		workOrderGrid.setHeight(200, Unit.PIXELS);
		return workOrderGrid;
	}

	private Component buildEquipmentGrid() {
		List<EquipmentBrigadePlanDto> brigadeEquipment = workGroup.getIdBrigadePlan() != null ?
				restTemplate.getBrigadeEquipments(workGroup.getIdBrigadePlan()) : new ArrayList<>();
		EquipmentGrid equipmentGrid = new EquipmentGrid(brigadeEquipment);
		equipmentGrid.setHeight(200, Unit.PIXELS);
		GridContextMenu<EquipmentBrigadePlanDto> contextMenu = new GridContextMenu<>(equipmentGrid);
		contextMenu.addGridHeaderContextMenuListener(event -> contextMenu.removeItems());
		contextMenu.addGridBodyContextMenuListener(event -> {
			contextMenu.removeItems();
			EquipmentBrigadePlanDto item = event.getItem();
			if (item != null) {
				equipmentGrid.select(item);

				MenuBar.Command commandFollow = (MenuBar.Command) menuItem -> showOnMap(item);
				contextMenu.addItem(msg("menu.gotoaddress"), BrigadesTheme.ICON_MAP, commandFollow);
			}
		});
		contextMenu.addGridFooterContextMenuListener(event -> contextMenu.removeItems());
		return equipmentGrid;
	}

	private Component buildMechanizationGrid() {
		List<MechanizationBrigadePlanDto> brigadeMechanization = workGroup.getIdBrigadePlan() != null ?
				restTemplate.getBrigadeMechanization(workGroup.getIdBrigadePlan()) : new ArrayList<>();
		MechanizationGrid mechanizationGrid = new MechanizationGrid(brigadeMechanization);
		mechanizationGrid.setHeight(200, Unit.PIXELS);
		return mechanizationGrid;
	}

	private static void showOnMap(EquipmentBrigadePlanDto item) {
		VehicleLayout.showOnMap(item.getType(), item.getModel(), item.getNumber());
	}

	private void bindFields() {
		String nullError = "Значение не может быть нулевым";
		SerializablePredicate<String> nullPredicate = StringUtil.nullPredicate();
		binder.forField(name).asRequired(nullError).withValidator(nullPredicate, nullError).bind(BrigadeJournalDto::getName, BrigadeJournalDto::setName);
		binder.bind(phone, BrigadeJournalDto::getPhone, BrigadeJournalDto::setPhone);
		binder.bind(manager, BrigadeJournalDto::getMaster, BrigadeJournalDto::setMaster);
		binder.bind(vehicle, BrigadeJournalDto::getVehicle, BrigadeJournalDto::setVehicle);

		binder.setBean(workGroup);

		BrigadeDto brigade = restTemplate.getBrigade(workGroup.getId());
		brigadeBinder.bind(phone, BrigadeDto::getPhone, BrigadeDto::setPhone);

		brigadeBinder.setBean(brigade);
	}

	private void openWorkGroupPlan() {
		WorkGroupLayout.showPlan(workGroup);
	}

	private void sendMessage() {
		BrigadeJournalDto bean = binder.getBean();
		getLogger().debug("Binder is valid: " + binder.validate().isOk());
		getLogger().debug(StringUtil.writeValueAsString(bean));
	}

	private void save() {
		if (binder.validate().isOk()) {
			getLogger().debug("Before save: " + StringUtil.writeValueAsString(brigadeBinder.getBean()));
			BrigadeDto savedBrigade = restTemplate.saveBrigade(brigadeBinder.getBean());
			getLogger().debug("After save: " + StringUtil.writeValueAsString(savedBrigade));
			BrigadesEventBus.post(new UpdateWorkGroupsEvent(savedBrigade));
			brigadeBinder.setBean(savedBrigade);
			Notification.show("Бригада сохранена", Notification.Type.TRAY_NOTIFICATION);
			//close();
		} else {
			Notification.show("Ошибка при сохранении данных", Notification.Type.ERROR_MESSAGE);
		}
	}
}
