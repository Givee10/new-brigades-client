package ru.telros.brigades.client.ui.component;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.OrganizationDto;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.utils.CompsUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FilialFilterLayout extends HorizontalLayout {
//	private String filialCaption = "Филиал/ЦРИП";
//	private String territoryCaption = "ТУВ/ТКВ/Управление";
//	private String regionCaption = "РВ/РЭСВ (РЭТК)/Служба";
	private String filialCaption = "Филиал";
	private String territoryCaption = "ТУВ";
	private String regionCaption = "РВ";

	private CustomComboBox<OrganizationDto> filialCB = new CustomComboBox<>(filialCaption);
	private CustomComboBox<OrganizationDto> territoryCB = new CustomComboBox<>(territoryCaption);
	private CustomComboBox<OrganizationDto> regionCB = new CustomComboBox<>(regionCaption);
	private MenuBar statusFilter = initStatusSelection();
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	public FilialFilterLayout() {
		setWidth(100, Unit.PERCENTAGE);
		setHeight(40, Unit.PIXELS);
		setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
		setSpacing(true);
		setMargin(new MarginInfo(false, true));

		List<OrganizationDto> filialList = restTemplate.getOrganizationsByType("FILIAL");
		OrganizationDto mainFilial = filialList.stream().filter(filial ->
				"Филиал Водоснабжение".equalsIgnoreCase(filial.getName())).findFirst().orElse(null);
		if (mainFilial != null) {
			List<OrganizationDto> territoryList = restTemplate.getChildOrganizations(mainFilial.getId(), "TERRITORY");
			OrganizationDto mainTerritory = territoryList.stream().filter(territory ->
					"ТУВ Юго-Восточное".equalsIgnoreCase(territory.getName())).findFirst().orElse(null);
			territoryCB.setItems(mainTerritory);
			territoryCB.addValueChangeListener(valueChangeEvent -> {
				OrganizationDto value = valueChangeEvent.getValue();
				regionCB.setItems(Collections.emptyList());
				regionCB.clear();
				if (value != null) {
					List<OrganizationDto> children = restTemplate.getChildOrganizations(value.getId(), "REGION");
					regionCB.setItems(children);
					OrganizationDto mainRegion = children.stream().filter(territory ->
							"РВ Юго-Западный".equalsIgnoreCase(territory.getName())).findFirst().orElse(null);
					regionCB.setValue(mainRegion);
				}
			});
			territoryCB.setValue(mainTerritory);
			regionCB.addValueChangeListener(valueChangeEvent -> RequestLayout.refresh());
		}
//		addComponent(CompsUtil.getStatisticWrapper(filialCB));
		addComponent(CompsUtil.getStatisticWrapper(territoryCB));
		addComponent(CompsUtil.getStatisticWrapper(regionCB));
		//addComponent(CompsUtil.getStatisticWrapper(statusFilter));
	}

	//Сделать фильтр в журнале заявок по статусам из ГЛ (по умолчанию должны выводиться заявки со статусом "В работе"
	private MenuBar initStatusSelection(){
		MenuBar valueSelect = new MenuBar();
		MenuBar.MenuItem statusCaption = valueSelect.addItem("Статус");
		//TODO-k СПРОСИТЬ насчёт несоответствия в базе и в статусах
//		BrigadesUI.getRestTemplate().getHLStatuses().stream().forEach(
		Arrays.asList("В работе", "Новая", "Закрытая").stream().forEach(
				statusDto -> {
					MenuBar.MenuItem item = statusCaption.addItem(statusDto/*.getName()*/,menuItem -> {
						RequestLayout.refresh();
					});
					item.setCheckable(true);
//					if (item.getText().equals("Находится в работе")){
					if (item.getText().equals("В работе")){
						item.setChecked(true);
						RequestLayout.refresh();
					}
				}
		);
		return valueSelect;
	}

	public OrganizationDto getRegionValue() {
		return regionCB.getValue();
	}

	public List<String> getSelectedStatuses() {
		return statusFilter.getItems().get(0).getChildren().stream().
				filter(e->e.isChecked()).map(e->e.getText()).collect(Collectors.toList());
	}
}
