package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.dto.WorkItemWorkDto;
import ru.telros.brigades.client.ui.grid.BrigadesTreeGrid;

public class WorkOrderTreeGrid extends BrigadesTreeGrid<WorkItemWorkDto> {
	public WorkOrderTreeGrid() {
		this(null);
	}

	public WorkOrderTreeGrid(String caption) {
		super(WorkOrderConstants.getTreeGridColumns());
		setCaption(caption);
		// Раскраска ячеек таблицы
		setStyleGenerator((workOrder) -> {
			String style = WorkOrderConstants.getEventStyle(workOrder);
			if (workOrder.getId() != null)
				style += " " + BrigadesTheme.STYLE_HAS_CHILD;
			return style;
		});
	}
}
