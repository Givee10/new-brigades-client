package ru.telros.brigades.client.ui.workorder;

import com.vaadin.data.BeanValidationBinder;
import ru.telros.brigades.client.dto.MetricDto;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.util.ArrayList;

public class MetricGrid extends BrigadesGrid<MetricDto> {
	public MetricGrid() {
		super(TOIRConstants.getMetricColumns(), new ArrayList<>());
		setHeightByRows(5);
	}

	protected void enableEditor() {
		DisabledTextField metricsCode = new DisabledTextField("Код метрики");
		CustomTextField quantity = new CustomTextField("Количество");

		BeanValidationBinder<MetricDto> binder = new BeanValidationBinder<>(MetricDto.class);
		binder.bind(metricsCode, MetricDto::getMetricsCode, MetricDto::setMetricsCode);
		binder.bind(quantity, MetricDto::getQuantity, MetricDto::setQuantity);

		getEditor().setBinder(binder).setEnabled(true).setSaveCaption("Сохранить").setCancelCaption("Отмена");
		getEditor().setErrorGenerator((map, binderValidationStatus) -> "Ошибка при введении данных");
		getEditor().addSaveListener(editorSaveEvent -> {
			MetricDto bean = editorSaveEvent.getBean();
			getLogger().debug(StringUtil.writeValueAsString(bean));
		});

		getColumn(TOIRConstants.M_QUANTITY.getId()).setEditorComponent(quantity);
	}

	@Override
	protected void onDoubleClick(MetricDto entity, Column column) {

	}

	@Override
	protected void onClick(MetricDto entity, Column column) {

	}
}
