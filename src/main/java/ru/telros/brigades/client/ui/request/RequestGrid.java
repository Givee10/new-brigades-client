package ru.telros.brigades.client.ui.request;

import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class RequestGrid extends BrigadesGrid<RequestDto> {
	public RequestGrid(List<GridColumn<RequestDto>> columns, List<RequestDto> items) {
		super(columns, items);
		// Раскраска строк таблицы
		setStyleGenerator(rowReference -> {
			String status = rowReference.getStatus();
			return (status != null) ? RequestConstants.getStyle(status) : null;
		});
	}

	@Override
	protected void onClick(RequestDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(RequestDto entity, Column column) {
		if (entity != null) {
			RequestDto request = BrigadesUI.getRestTemplate().getRequest(entity.getId());
			RequestLayout.showRequest(request);
		}
	}
}
