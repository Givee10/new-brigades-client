package ru.telros.brigades.client.ui.workplan;

import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.dto.WorkItemDto;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

@SuppressWarnings("serial")
public class WorkGroupPlanWindow extends WindowWithButtons implements HasLogger {
	private BrigadeDto workGroup;
	private WorkPlan workPlan;
	private Boolean isEdited;
	private Boolean hasUpdated;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	private WorkGroupPlanWindow(BrigadeDto workGroup, Boolean isEdited) {
		super(null, 800, -1);
		this.workGroup = workGroup;
		this.isEdited = isEdited;
		this.hasUpdated = false;
		this.workPlan = new WorkPlan("План работ", isEdited);
		setCaption("Адресно-временной план: " + workGroup.toString());
		build();
	}

	public static void open(BrigadeDto workGroup, Boolean isEdited) {
		Window window = new WorkGroupPlanWindow(workGroup, isEdited);
		UI.getCurrent().addWindow(window);
		window.focus();
	}

	private void build() {
		VerticalLayout layout = CompsUtil.getVerticalWrapperNoMargin();
		//ReadOnlyTextField workGroupLabel = new ReadOnlyTextField("Бригада");
		//workGroupLabel.setValue(workGroup.getNameWithManager());
		//layout.addComponent(workGroupLabel);

		workPlan.setWorkOrders(restTemplate.getBrigadeWorks(workGroup.getId()));
		layout.addComponent(workPlan);

		addToContent(layout);

		addToButtonBar("Отправить бригаде", $ -> send());
		addToButtonBar(msg("button.save"), $ -> save());
		addToButtonBar(msg("button.close"), $ -> close());
		setButtonEnabled(0, false);
		setButtonVisible(1, isEdited);
	}

	private void send() {
	}

	private void save() {
		Collection<WorkDto> workOrders = workPlan.getWorkOrders();
		for (WorkDto workOrder : workOrders) {
			// Get workOrder for correct saving
			WorkDto withAll = restTemplate.getWork(workOrder.getId());
			ZonedDateTime newStartDatePlan = workOrder.getStartDatePlan();
			ZonedDateTime oldStartDatePlan = withAll.getStartDatePlan();
			int compare = newStartDatePlan.compareTo(oldStartDatePlan);
			if (compare != 0) {
				// Get difference from dates
				Long diff = ChronoUnit.SECONDS.between(newStartDatePlan, oldStartDatePlan);
				List<WorkItemDto> children = restTemplate.getWorkItems(withAll.getId());
				children.forEach(child -> {
					// Set new start date for child
					if (child.getStartDatePlan() != null) {
						ZonedDateTime childStartDatePlan = child.getStartDatePlan().plusSeconds(diff);
						child.setStartDatePlan(childStartDatePlan);
					}
					// Set new finish date for child
					if (child.getFinishDatePlan() != null) {
						ZonedDateTime childFinishDatePlan = child.getFinishDatePlan().plusSeconds(diff);
						child.setFinishDatePlan(childFinishDatePlan);
					}
					//restTemplate.saveWorkItem(child, workOrder.getId());
				});
				restTemplate.updateWorkItems(children, withAll.getId());
				withAll.setStartDatePlan(workOrder.getStartDatePlan());
				withAll.setFinishDatePlan(workOrder.getFinishDatePlan());
				getLogger().debug("Before save: " + StringUtil.writeValueAsString(withAll));
				WorkDto savedWork = restTemplate.saveWork(withAll);
				getLogger().debug("After save:  " + StringUtil.writeValueAsString(savedWork));
				WorkOrderLayout.refresh();
				WorkOrderLayout.update(savedWork);
			}
		}
		hasUpdated = true;
		close();
	}

}
