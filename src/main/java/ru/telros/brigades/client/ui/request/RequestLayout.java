package ru.telros.brigades.client.ui.request;

import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.server.Responsive;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.event.request.SelectRequestEvent;
import ru.telros.brigades.client.event.request.UpdateRequestsEvent;
import ru.telros.brigades.client.ui.component.*;
import ru.telros.brigades.client.ui.window.YesNoWindow;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;
import ru.telros.brigades.client.view.reports.util.GridWithButtonLayout;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.MessageManager.msg;

public class RequestLayout extends FullSizeVerticalLayout {
	//private RequestGrid requestGrid = new RequestGrid(RequestConstants.getMainColumns(), new ArrayList<>());
	//private GridContextMenu<RequestDto> contextMenu = new GridContextMenu<>(requestGrid);
	private RequestJournalDtoGrid journalGrid = new RequestJournalDtoGrid();
	//private RequestJournalGrid journalGrid = new RequestJournalGrid();
	private GridContextMenu<RequestJournalDto> contextMenu = new GridContextMenu<>(journalGrid);
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	private ReadOnlyTextField fWere = new ReadOnlyTextField("Было");
	private ReadOnlyTextField fNew = new ReadOnlyTextField("Поступило");
	private ReadOnlyTextField fClosed = new ReadOnlyTextField("Выполнено");
	private ReadOnlyTextField fRest = new ReadOnlyTextField("Остаток");
	private ReadOnlyTextField fInWork = new ReadOnlyTextField("В работе");
	private ReadOnlyTextField fWaiting = new ReadOnlyTextField("Ждут");
	private BrigadesStatistic statistic = new BrigadesStatistic(fWere, fNew, fClosed, fRest, fInWork, fWaiting);
	private FilialFilterLayout filterLayout = new FilialFilterLayout();
	private Boolean updateVisible = true;
	private MenuBar.MenuItem filter;
	private final LinkedHashMap<String, MenuBar.Command> filterMap = new LinkedHashMap<>();
	private List<String> selectedStatuses = new ArrayList<>();
	private List<StatusDto> hlStatuses = restTemplate.getHLStatuses();

	public RequestLayout() {
		setMargin(false);
		setSpacing(false);
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		//filterLayout.setVisible(false);
		statistic.addSummary(4);
		statistic.isMaximized(false);
		contextMenu.addGridHeaderContextMenuListener(this::filterGridItem);
		contextMenu.addGridBodyContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridFooterContextMenuListener(event -> selectGridItem(event.getItem()));

		addComponent(filterLayout);
		//addComponent(statistic);
		//addComponentAndRatio(requestGrid, 1);

		journalGrid.sort(RequestConstants.REGISTRATION_DATE.getId(), SortDirection.DESCENDING);
		addComponentAndRatio(journalGrid, 1);

		//update();
	}

	public static void refresh() {
		BrigadesEventBus.post(new UpdateRequestsEvent());
	}

	public static void addRequest() {
		final RequestDto request = new RequestDto();
		request.setRegistrationDate(ZonedDateTime.now());
		request.setRegisteredBy(BrigadesUI.getCurrentUser().getFullName());
		request.setStatus(RequestDto.Status.NEW.getCode());
		RequestWindow.open(request);
	}

	public static void deleteRequest(RequestDto request) {
		if (request != null) {
			if (request.getId() != null && RequestDto.Status.NEW.getCode().equals(request.getStatus())) {
				BrigadesUI.getRestTemplate().deleteRequest(request.getId());
			} else {
				Notification.show(msg("error"), "Невозможно удалить заявку " + request.toString(), Notification.Type.ERROR_MESSAGE);
			}
		} else {
			Notification.show("Ошибка при получении данных заявки", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static  void showRequest(RequestDto request) {
		if (request != null) {
			//RequestStateWindow.open(request);
			  RequestWindow.open(request);
		} else {
			Notification.show("Ошибка при получении данных заявки", Notification.Type.ERROR_MESSAGE);
		}
	}

	private static void addWork(RequestDto request) {
		if (request != null) {
			WorkOrderLayout.createWorkOrder(request);
		} else {
			Notification.show("Ошибка при получении данных заявки", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void selectRequest(RequestDto request) {
		if (request != null) {
			BrigadesEventBus.post(new SelectRequestEvent(request));
		} else {
			Notification.show("Ошибка при получении данных заявки", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void closeRequest(RequestDto request) {
		if (request != null) {
			if (RequestConstants.canCloseRequest(request.getStatus())) {
				String prompt = "Вы действительно хотите закрыть заявку " + request.getExternalNumber() + " в СОУ?";
				YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
				confirmationDialog.addCloseListener((Window.CloseListener) e -> {
					if (((YesNoWindow) e.getWindow()).isModalResult()) {
						RequestDto requestDto = BrigadesUI.getRestTemplate().closeRequest(request);
						if (requestDto != null) {
							Notification.show("Заявка " + requestDto.getExternalNumber() + " закрыта в СОУ");
							RequestLayout.refresh();
						} else {
							Notification.show("Невозможно закрыть заявку", Notification.Type.ERROR_MESSAGE);
						}
					}
				});
			} else {
				Notification.show(msg("error"), "Невозможно закрыть заявку " + request.getExternalNumber(), Notification.Type.ERROR_MESSAGE);
			}
		}
	}

	private void filterGridItem(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent<RequestJournalDto> event) {
		//contextMenu.removeItems();

		//MenuBar.Command commandFilter = $ -> doFilterRequest();
		//contextMenu.addItem("Открыть фильтр", BrigadesTheme.ICON_FILTER, commandFilter);
	}

	private void selectGridItem(RequestJournalDto journalDto) {
		contextMenu.removeItems();

		MenuBar.Command commandRefresh = $ -> update();
		MenuBar.Command commandAdd = $ -> addRequest();

		if (journalDto != null) {
			journalGrid.select(journalDto);
			RequestDto request = restTemplate.getRequest(journalDto.getId());
			MenuBar.Command commandFollow = $ -> selectRequest(request);
			MenuBar.Command commandEdit = $ -> showRequest(request);
			MenuBar.Command commandAddWork = $ -> addWork(request);
			MenuBar.Command commandClose = $ -> closeRequest(request);
			MenuBar.Command commandDelete = $ -> deleteRequest(request);

			contextMenu.addItem(msg("menu.gotoaddress"), BrigadesTheme.ICON_MAP, commandFollow);
			contextMenu.addSeparator();
			contextMenu.addItem(msg("menu.request.edit"), BrigadesTheme.ICON_EDIT, commandEdit);
			contextMenu.addSeparator();
			contextMenu.addItem(msg("menu.workorder.new"), BrigadesTheme.ICON_WORKORDER, commandAddWork);
			contextMenu.addSeparator();
			//contextMenu.addItem(msg("menu.request.delete"), BrigadesTheme.ICON_DELETE, commandDelete);
			//item = contextMenu.addItem(msg("menu.request.close"), BrigadesTheme.ICON_CLOSE, commandClose);
			//item.setEnabled(!"CLOSE".equals(request.getStatus()));
			//contextMenu.addSeparator();
		} else journalGrid.deselectAll();
		contextMenu.addItem(msg("menu.request.new"), BrigadesTheme.ICON_ADD, commandAdd);
		contextMenu.addSeparator();
		contextMenu.addItem(msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, commandRefresh);
	}

	public void addToolBarItems(BrigadesPanel panel) {
		//panel.addToolbarItem("", msg("menu.request.filter"), BrigadesTheme.ICON_FILTER, $ -> doFilterRequest());

		for (StatusDto statusDto : hlStatuses) {
			//selectedStatuses.add(statusDto.getName());
			filterMap.put(statusDto.getName(), menuItem -> toggleStatuses(menuItem.getText(), menuItem.isChecked()));
		}

		panel.addToolbarItem("", msg("menu.request.add"), BrigadesTheme.ICON_ADD, $ -> doAddRequest());
		panel.addToolbarItem("", msg("menu.request.search"), BrigadesTheme.ICON_SEARCH, $ -> doSearchRequest());
		panel.addToolbarItem("", msg("menu.request.edit"), BrigadesTheme.ICON_EDIT, $ -> doEditRequest());
		filter = panel.addFilterItem("", msg("menu.request.filter"), BrigadesTheme.ICON_FILTER, filterMap);
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, RequestConstants.statusStyles);
		panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, $ -> update());

		for (MenuBar.MenuItem menuItem : filter.getChildren()) {
			if (menuItem.getText().equalsIgnoreCase("Находится в работе")) {
				menuItem.setChecked(true);
				selectedStatuses.add(menuItem.getText());
			} else {
				menuItem.setChecked(false);
				//selectedStatuses.remove(menuItem.getText());
			}
		}
	}

	private void toggleStatuses(String text, boolean checked) {
		if (checked) {
			selectedStatuses.add(text);
		} else {
			selectedStatuses.remove(text);
		}
		update();
	}

	public void update() {
		if (updateVisible) {
			//requestGrid.update(restTemplate.getRequests());
			//journalGrid.refreshAll();
			OrganizationDto regionValue = filterLayout.getRegionValue();
			Long countRequests;
			if (regionValue != null)
				countRequests = restTemplate.countRequests(regionValue.getName());
			else
				countRequests = restTemplate.countRequests();
			if (countRequests != null && countRequests != 0) {
				List<RequestJournalDto> requestsForJournal = new ArrayList<>();
				if (regionValue != null) {
					if (hlStatuses.size() == selectedStatuses.size()) {
						PagesDto<RequestJournalDto> pagesDto = restTemplate.getRequestsForJournal(0, countRequests.intValue(), regionValue.getName(), "");
						requestsForJournal.addAll(pagesDto.getCurrentContent());
					} else {
						for (String s : selectedStatuses) {
							PagesDto<RequestJournalDto> pagesDto = restTemplate.getRequestsForJournal(0, countRequests.intValue(), regionValue.getName(), s);
							requestsForJournal.addAll(pagesDto.getCurrentContent());
						}
					}
				} else {
					PagesDto<RequestJournalDto> pagesDto = restTemplate.getRequestsForJournal(0, countRequests.intValue());
					requestsForJournal.addAll(pagesDto.getCurrentContent());
				}

				journalGrid.update(requestsForJournal);
			} else {
				journalGrid.update(new ArrayList<>());
			}
		}
	}

	private void onRefresh() {
		refresh();
	}

	private void doAddRequest() {
		addRequest();
	}

	private void doDeleteRequest() {
//		RequestDto request = requestGrid.getSelectedRow();
//		if (request != null) {
//			deleteRequest(request);
//		}
		RequestJournalDto selectedRow = journalGrid.getSelectedRow();
		if (selectedRow != null) {
			RequestDto request = restTemplate.getRequest(selectedRow.getId());
			deleteRequest(request);
		}
	}

	private void doEditRequest() {
//		RequestDto request = requestGrid.getSelectedRow();
//		if (request != null) {
//			editRequest(request);
//		}
		RequestJournalDto selectedRow = journalGrid.getSelectedRow();
		if (selectedRow != null) {
			RequestDto request = restTemplate.getRequest(selectedRow.getId());
			showRequest(request);
		}
	}
	private void doSearchRequest(){
		new SearchWindow();
	}

	private void doFilterRequest() {
	}

	public void setStatisticVisible(boolean visible) {
		//filterLayout.setVisible(visible);
		statistic.isMaximized(visible);
	}

	@Subscribe
	public void handleSelectRequestEvent(final SelectRequestEvent event) {
		journalGrid.select(event.getEntity().getId());
		journalGrid.scrollTo(event.getEntity().getId());
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		update();
	}
	private class SearchWindow extends Window{
		VerticalLayout mainLayout;
		ReportDateTimeLayout requestRegDateRange;
		ReportDateTimeLayout requestCloseDateRange;
		GridWithButtonLayout problemCodeLayout;
		Grid<ProblemDto> problemCodeGrid;
		GridWithButtonLayout HLStatusLayout;
		Grid<StatusDto> HLStatusGrid;
		TextField requestNumber;
		TextField filialRequestNumber;
		RequestGrid resultGrid;
		Button searchButton;
		public SearchWindow(){
			requestCloseDateRange = new ReportDateTimeLayout();
			requestCloseDateRange.setCaption("Заявка закрыта");
            requestCloseDateRange.setEmpty();

			requestRegDateRange = new ReportDateTimeLayout();
			requestRegDateRange.setCaption("Заявка принята");
            requestRegDateRange.setEmpty();

			problemCodeGrid = new Grid<>();
			problemCodeGrid.setItems(BrigadesUI.getRestTemplate().getProblems());
			problemCodeGrid.addColumn(problemDto -> problemDto.getId()+" "+problemDto.getName());
			problemCodeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
			problemCodeLayout = new GridWithButtonLayout(problemCodeGrid, new Button("Код проблемы"));

			HLStatusGrid = new Grid<>();
			List<StatusDto> hlstatuses = BrigadesUI.getRestTemplate().getHLStatuses().stream().sorted(Comparator.comparing(AbstractEntity::getId)).collect(Collectors.toList());
			HLStatusGrid.setItems(hlstatuses);
			HLStatusGrid.addColumn(statusDto -> statusDto.getName());
			HLStatusGrid.setSelectionMode(Grid.SelectionMode.MULTI);
			HLStatusLayout = new GridWithButtonLayout(HLStatusGrid, new Button("Статус ГЛ"));

			requestNumber = new TextField();
			requestNumber.setCaption("Номер заявки");

			filialRequestNumber = new TextField();
			filialRequestNumber.setCaption("Номер заявки в филиале");

			resultGrid = new RequestGrid(RequestConstants.getMainColumnsAndStatus(), new ArrayList<>());
			resultGrid.setStyleGenerator(item -> (item != null) ? RequestConstants.getStyle(item.getStatus()) : null);

			searchButton = new Button("Поиск");
			searchButton.addClickListener(clickEvent -> {
				ZonedDateTime from = ZonedDateTime.now().withYear(2008);
				ZonedDateTime to = ZonedDateTime.now();
				if (!requestRegDateRange.isEmpty()) {
					from = requestRegDateRange.getStart();
				}
				if (!requestCloseDateRange.isEmpty()){
					to = requestCloseDateRange.getEnd();
				}
				List<RequestDto> items = BrigadesUI.getRestTemplate().getRequests(from,to).stream()
						.filter(requestDto -> {
                            boolean HLStatus = true;
                            boolean problemCode = true;
                            boolean reqNumber = true;
                            boolean filialReqNumber = true;


                            HLStatus = HLStatusGrid.getSelectedItems().size()==0 ? true: requestDto.getStatusHL()!= null && HLStatusGrid.getSelectedItems().stream().map(e->e.getName()).collect(Collectors.toList()).contains(requestDto.getStatusHL());
                            problemCode = (problemCodeGrid.getSelectedItems().size()==0)? true: requestDto.getProblemCode()!=null && problemCodeGrid.getSelectedItems().stream().map(e->e.getId()+"").collect(Collectors.toList()).contains(requestDto.getProblemCode());
							reqNumber = requestNumber.isEmpty() ? true : requestDto.getExternalNumber()!=null && requestDto.getExternalNumber().equals(requestNumber.getValue());
							filialReqNumber = filialRequestNumber.isEmpty() ? true: requestDto.getAccessNumber()!=null && requestDto.getAccessNumber().equals(filialRequestNumber.getValue());
							 return	HLStatus && problemCode && reqNumber && filialReqNumber;

						}).collect(Collectors.toList());
				resultGrid.setItems(items);

			});
			HorizontalLayout paramLayout = new HorizontalLayout();
			mainLayout = new VerticalLayout(paramLayout, resultGrid);
			requestCloseDateRange.setHeight("100px");
			requestRegDateRange.setHeight("100px");
			paramLayout.setHeight("100px");
			paramLayout.addComponents(requestRegDateRange,requestCloseDateRange,HLStatusLayout, problemCodeLayout, requestNumber, filialRequestNumber, searchButton);
			setContent(mainLayout);
			mainLayout.setHeight("100%");
			mainLayout.setExpandRatio(paramLayout,2);
			mainLayout.setExpandRatio(resultGrid,8);
			resultGrid.setWidth("100%");
			setSizeFull();
			getUI().getCurrent().addWindow(this);
		}
	}
}
