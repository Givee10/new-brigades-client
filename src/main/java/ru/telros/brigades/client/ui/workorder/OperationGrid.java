package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.dto.OperationDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.ArrayList;

public class OperationGrid extends BrigadesGrid<OperationDto> {
	public OperationGrid() {
		super(TOIRConstants.getOperationColumns(), new ArrayList<>());
		setHeightByRows(5);
	}

	@Override
	protected void onDoubleClick(OperationDto entity, Column column) {

	}

	@Override
	protected void onClick(OperationDto entity, Column column) {

	}
}
