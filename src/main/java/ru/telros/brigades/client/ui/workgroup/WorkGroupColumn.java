package ru.telros.brigades.client.ui.workgroup;

import com.vaadin.ui.CssLayout;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.BrigadeJournalDto;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public abstract class WorkGroupColumn {
	private static final GridColumn<BrigadeDto> FLAG = GridColumn.createComponentColumn("_flag", "", WorkGroupColumn::createFlag);
	private static final GridColumn<BrigadeDto> TYPE = GridColumn.createColumn("type", "Тип бригады", BrigadeDto::getName);
	private static final GridColumn<BrigadeDto> MANAGER = GridColumn.createColumn("manager", "Мастер", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> PHONE = GridColumn.createColumn("phone", "Телефон", BrigadeDto::getPhone);
	private static final GridColumn<BrigadeDto> REGISTRY = GridColumn.createColumn("registry", "Регистр АИС Бригады", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> ADDRESS = GridColumn.createColumn("address", "Текущий адрес работ", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> WORK_ORDER = GridColumn.createColumn("workOrder", "Текущая работа", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> WORK_TIME = GridColumn.createColumn("workTime", "Время выполнения", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> VEHICLE_NUMBER = GridColumn.createColumn("vehicleNumber", "Гос. номер фургона", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> CAMERA = GridColumn.createColumn("camera", "Работа с МАВР", WorkGroupColumn::createMavr);
	private static final GridColumn<BrigadeDto> INFO_DATE = GridColumn.createDateColumn("infoDate", "Время последнего поступл. информации", BrigadeDto::getLastNotifyDate);
	private static final GridColumn<BrigadeDto> SHIFT = GridColumn.createColumn("shift", "Смена", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> FILIAL = GridColumn.createColumn("filial", "Филиал", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> TERRITORY = GridColumn.createColumn("territory", "ТУВ", GridColumn::nullProvider);
	private static final GridColumn<BrigadeDto> REGION = GridColumn.createColumn("region", "РВ", GridColumn::nullProvider);

	private static final GridColumn<BrigadeJournalDto> ICON = GridColumn.createComponentColumn("_icon", "", WorkGroupColumn::createIcon);
	private static final GridColumn<BrigadeJournalDto> ID = GridColumn.createColumn("id", "№", BrigadeJournalDto::getId);
	private static final GridColumn<BrigadeJournalDto> NAME = GridColumn.createColumn("type", "Тип бригады", BrigadeJournalDto::getName);
	private static final GridColumn<BrigadeJournalDto> MASTER = GridColumn.createColumn("manager", "Мастер", BrigadeJournalDto::getMaster);
	private static final GridColumn<BrigadeJournalDto> PHONE_NUMBER = GridColumn.createColumn("phone", "Телефон", BrigadeJournalDto::getPhone);
	private static final GridColumn<BrigadeJournalDto> REGISTER = GridColumn.createColumn("registry", "Регистр АИС Бригады", BrigadeJournalDto::getRegister);
	private static final GridColumn<BrigadeJournalDto> CURRENT_ADDRESS = GridColumn.createColumn("address", "Текущий адрес работ", BrigadeJournalDto::getAddress);
	private static final GridColumn<BrigadeJournalDto> CURRENT_WORK_ORDER = GridColumn.createColumn("workOrder", "Текущая работа", BrigadeJournalDto::getTypeWork);
	private static final GridColumn<BrigadeJournalDto> CURRENT_WORK_TIME = GridColumn.createColumn("workTime", "Время выполнения", BrigadeJournalDto::getTimeWork);
	private static final GridColumn<BrigadeJournalDto> VEHICLE = GridColumn.createColumn("vehicleNumber", "Гос. номер фургона", BrigadeJournalDto::getVehicle);
	private static final GridColumn<BrigadeJournalDto> MAVR = GridColumn.createBooleanColumn("camera", "Работа с МАВР", BrigadeJournalDto::getMavr, "+");
	private static final GridColumn<BrigadeJournalDto> LAST_INFO_DATE = GridColumn.createDateColumn("infoDate", "Время последнего поступл. информации", BrigadeJournalDto::getLastNotifyDate);
	private static final GridColumn<BrigadeJournalDto> _SHIFT = GridColumn.createColumn("shift", "Смена", BrigadeJournalDto::getShift);
	private static final GridColumn<BrigadeJournalDto> _FILIAL = GridColumn.createColumn("filial", "Филиал", BrigadeJournalDto::getFilial);
	private static final GridColumn<BrigadeJournalDto> _TERRITORY = GridColumn.createColumn("territory", "ТУВ", BrigadeJournalDto::getTerritory);
	private static final GridColumn<BrigadeJournalDto> _REGION = GridColumn.createColumn("region", "РВ", BrigadeJournalDto::getRegion);

	private static CssLayout createFlag(BrigadeDto workGroup) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		return layout;
	}

	private static CssLayout createIcon(BrigadeJournalDto brigade) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		return layout;
	}

	private static String createMavr(BrigadeDto workGroup) {
		return (workGroup.getMavr() != null && workGroup.getMavr()) ? "+" : "";
	}

	public static String getEventStyle(BrigadeJournalDto brigadeDto) {
		Objects.requireNonNull(brigadeDto);
		if (brigadeDto.getIdBrigadePlan() == null)
			return BrigadesTheme.STYLE_ET_NORMAL;
		else
			return BrigadesTheme.STYLE_ET_INWORK;
	}

	public static List<GridColumn<BrigadeDto>> getMainColumns() {
		return Arrays.asList(FLAG, TYPE, MANAGER, PHONE, REGISTRY, ADDRESS, WORK_ORDER, WORK_TIME,
				VEHICLE_NUMBER, CAMERA, INFO_DATE, SHIFT, FILIAL, TERRITORY, REGION);
	}

	public static List<GridColumn<BrigadeJournalDto>> getJournalColumns() {
		return Arrays.asList(ICON, NAME, MASTER, PHONE_NUMBER, REGISTER, CURRENT_ADDRESS, CURRENT_WORK_ORDER,
				CURRENT_WORK_TIME, VEHICLE, MAVR, LAST_INFO_DATE, _SHIFT, _FILIAL, _TERRITORY, _REGION);
	}

	public static String getNameId() {
		return NAME.getId();
	}
}
