package ru.telros.brigades.client.ui.window;

import com.vaadin.ui.Window;

public class ModalWindow extends Window {
	private static final int DEFAULT_WIDTH_PIXELS = 1000;
	private static final int DEFAULT_HEIGHT_PIXELS = -1;

	public ModalWindow(String caption) {
		this(caption, DEFAULT_WIDTH_PIXELS, DEFAULT_HEIGHT_PIXELS);
	}

	public ModalWindow(String caption, final int width, final int height) {
		super(caption);
		setWidth(width, Unit.PIXELS);
		setHeight(height, Unit.PIXELS);

		//setResizable(false);
		setModal(true);
	}
}
