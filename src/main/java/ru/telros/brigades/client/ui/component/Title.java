package ru.telros.brigades.client.ui.component;

import com.vaadin.ui.Label;
import ru.telros.brigades.client.BrigadesTheme;

public class Title extends Label {
	public Title(final String content) {
		super(content);

		setWidthUndefined();
		addStyleName(BrigadesTheme.LABEL_H1);
		addStyleName(BrigadesTheme.LABEL_NO_MARGIN);
	}
}
