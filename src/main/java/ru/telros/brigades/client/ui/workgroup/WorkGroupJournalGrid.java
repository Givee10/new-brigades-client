package ru.telros.brigades.client.ui.workgroup;

import ru.telros.brigades.client.dto.BrigadeJournalDto;
import ru.telros.brigades.client.ui.grid.BrigadeDataProvider;
import ru.telros.brigades.client.ui.grid.JournalGrid;

public class WorkGroupJournalGrid extends JournalGrid<BrigadeJournalDto> {
	private static BrigadeDataProvider dataProvider = new BrigadeDataProvider();

	public WorkGroupJournalGrid() {
		super(WorkGroupColumn.getJournalColumns(), dataProvider);
	}

	@Override
	protected void onDoubleClick(BrigadeJournalDto item, Column column) {
		if (item != null) {
			WorkGroupLayout.showWorkGroup(item);
		}
	}
}
