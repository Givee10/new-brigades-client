package ru.telros.brigades.client.ui.maplayout;

import com.google.common.eventbus.Subscribe;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.event.camera.ShowCameraEvent;
import ru.telros.brigades.client.event.vehicle.ShowVehicleEvent;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.request.RequestConstants;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.workorder.WorkOrderConstants;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.util.*;

import static ru.telros.brigades.client.MessageManager.msg;

public class MapViewLayout implements HasLogger {
	public static final String SECTION = "map";
	public static final String PAR_MAP_ZOOM = "map.zoom";
	public static final String PAR_MAP_CENTER_LAT = "map.center.lat";
	public static final String PAR_MAP_CENTER_LON = "map.center.lon";
	public static final String PAR_MAP_TYPE = "map.type";

	private static final int IMG_WORK_WIDTH = 48;
	private static final int IMG_WORK_HEIGHT = 44;
	private static final int IMG_WORK_OFFSET_X = (int) (-IMG_WORK_WIDTH / 1.5);
	private static final int IMG_WORK_OFFSET_Y = -IMG_WORK_HEIGHT / 2;
	private static final int IMG_CAR_WIDTH = 48;
	private static final int IMG_CAR_HEIGHT = 31;
	private static final int IMG_CAR_OFFSET_X = 0;
	private static final int IMG_CAR_OFFSET_Y = -IMG_CAR_HEIGHT / 2;
	private static final int IMG_TRUCK_WIDTH = 32;
	private static final int IMG_TRUCK_HEIGHT = 32;
	private static final int IMG_TRUCK_OFFSET_X = 0;
	private static final int IMG_TRUCK_OFFSET_Y = -IMG_TRUCK_HEIGHT / 2;
	private static final int IMG_CAMERA_WIDTH = 40;
	private static final int IMG_CAMERA_HEIGHT = 40;
	private static final int IMG_CAMERA_OFFSET_X = -IMG_CAMERA_WIDTH / 2;
	private static final int IMG_CAMERA_OFFSET_Y = -IMG_CAMERA_HEIGHT / 2;

	private static final String URL_IMAGE_DEFAULT_PATH = "/VAADIN/themes/brigades/img/";
	private static final String URL_IMAGE_BULLDOZER_DEFAULT = URL_IMAGE_DEFAULT_PATH + "bd.png";
	private static final String URL_IMAGE_CAR_DEFAULT = URL_IMAGE_DEFAULT_PATH + "car_small.png";
	private static final String URL_IMAGE_CRANE_DEFAULT = URL_IMAGE_DEFAULT_PATH + "crane.png";
	private static final String URL_IMAGE_DUMP_DEFAULT = URL_IMAGE_DEFAULT_PATH + "gs.png";
	private static final String URL_IMAGE_GARBAGE_DEFAULT = URL_IMAGE_DEFAULT_PATH + "dump_truck.png";
	private static final String URL_IMAGE_GBA_DEFAULT = URL_IMAGE_DEFAULT_PATH + "gba.png";
	private static final String URL_IMAGE_EXCAVATOR_DEFAULT = URL_IMAGE_DEFAULT_PATH + "ex.png";
	private static final String URL_IMAGE_TRACTOR_DEFAULT = URL_IMAGE_DEFAULT_PATH + "tractor.png";
	private static final String URL_IMAGE_TRUCK_DEFAULT = URL_IMAGE_DEFAULT_PATH + "truck.svg";
	private static final String URL_IMAGE_VAN_DEFAULT = URL_IMAGE_DEFAULT_PATH + "van.png";
	private static final String URL_IMAGE_WORK_DEFAULT = URL_IMAGE_DEFAULT_PATH + "work_small.png";
	private static final String URL_IMAGE_REQUEST = URL_IMAGE_DEFAULT_PATH + "request_small.png";
	private static final String URL_IMAGE_CAMERA = URL_IMAGE_DEFAULT_PATH + "camera.png";
	private static final String URL_IMAGE_CAMERA_RED = URL_IMAGE_DEFAULT_PATH + "camera_red.png";
	private static final String URL_IMAGE_CAMERA_GREEN = URL_IMAGE_DEFAULT_PATH + "camera_green.png";

	private static final int DEFAULT_ZOOM = 12;
	private static final long SAVE_SETTINGS_INTERVAL = 1500; // Задержка на сохранение состояния
	private final LinkedHashMap<String, String> legend = new LinkedHashMap<String, String>();
	private final LinkedHashMap<String, MenuBar.Command> filterMap = new LinkedHashMap<String, MenuBar.Command>() {{
		put("Заявки", $ -> toggleRequests());
		put("Работы", $ -> toggleWorks());
		put("Техника", $ -> toggleEquipments());
	}};
	private final LinkedHashMap<String, String> carIcons = new LinkedHashMap<String, String>() {{
		put("Аварийная с ЭЭК", URL_IMAGE_GARBAGE_DEFAULT);
		put("Автобусы", URL_IMAGE_GARBAGE_DEFAULT);
		put("Автогидроподъемник", URL_IMAGE_CRANE_DEFAULT);
		put("Автокраны", URL_IMAGE_CRANE_DEFAULT);
		put("Автопогрузчик", URL_IMAGE_CRANE_DEFAULT);
		put("Автоэвакуатор", URL_IMAGE_CRANE_DEFAULT);
		put("Бара", URL_IMAGE_TRACTOR_DEFAULT);
		put("Бетоносмесители", URL_IMAGE_GARBAGE_DEFAULT);
		put("Бульдозер", URL_IMAGE_BULLDOZER_DEFAULT);
		put("Бурильная установка", URL_IMAGE_GARBAGE_DEFAULT);
		put("Вакуумная установка", URL_IMAGE_GARBAGE_DEFAULT);
		put("Грузовые бортовые автомобили", URL_IMAGE_GBA_DEFAULT);
		put("Грузовые самосвалы", URL_IMAGE_DUMP_DEFAULT);
		put("Грузопассажирские фургоны", URL_IMAGE_VAN_DEFAULT);
		put("грунтоотогрев", URL_IMAGE_GARBAGE_DEFAULT);
		put("ДГУ/ ДЭС", URL_IMAGE_GARBAGE_DEFAULT);
		put("Дробильная установка", URL_IMAGE_GARBAGE_DEFAULT);
		put("Лаборатория", URL_IMAGE_GARBAGE_DEFAULT);
		put("Легковые автомобили", URL_IMAGE_GARBAGE_DEFAULT);
		put("мал.мех.", URL_IMAGE_GARBAGE_DEFAULT);
		put("Манипуляторы", URL_IMAGE_CRANE_DEFAULT);
		put("Маслосборщик", URL_IMAGE_GARBAGE_DEFAULT);
		put("Машины АВР", URL_IMAGE_GARBAGE_DEFAULT);
		put("Машины илососные", URL_IMAGE_GARBAGE_DEFAULT);
		put("Машины комбинированные", URL_IMAGE_GARBAGE_DEFAULT);
		put("Мусоровоз", URL_IMAGE_GARBAGE_DEFAULT);
		put("Парогенератор", URL_IMAGE_GARBAGE_DEFAULT);
		put("Передвижной пункт питания", URL_IMAGE_GARBAGE_DEFAULT);
		put("Прицепы", URL_IMAGE_GARBAGE_DEFAULT);
		put("сан.гигиенич", URL_IMAGE_GARBAGE_DEFAULT);
		put("Седельные тягачи", URL_IMAGE_GARBAGE_DEFAULT);
		put("Тех.помощь", URL_IMAGE_GARBAGE_DEFAULT);
		put("топливозаправщик (только ГУП)", URL_IMAGE_GARBAGE_DEFAULT);
		put("Трактор", URL_IMAGE_TRACTOR_DEFAULT);
		put("Тракторный погрузчик", URL_IMAGE_TRACTOR_DEFAULT);
		put("Уборочная техника", URL_IMAGE_GARBAGE_DEFAULT);
		put("Фургоны", URL_IMAGE_VAN_DEFAULT);
		put("Цистерна (только ГУП)", URL_IMAGE_GARBAGE_DEFAULT);
		put("Экскаваторы", URL_IMAGE_EXCAVATOR_DEFAULT);
	}};
	private final Polyline mapRoute = new Polyline();
	private final Polyline mapRegion = new Polyline();
	private final Map<GeocodeDto, Marker> mapWorkOrderGeoCodeMarkers = new HashMap<>();
	private final Map<GeocodeDto, Marker> mapRequestGeoCodeMarkers = new HashMap<>();
	private final Map<Marker, WorkMapDto> mapWorkOrderMarkers = new HashMap<>();
	private final Map<Marker, RequestMapDto> mapRequestMarkers = new HashMap<>();
	private final Map<Marker, BrigadeMapDto> mapWorkGroupMarkers = new HashMap<>();
	private final Map<Marker, CameraDto> mapCameraMarkers = new HashMap<>();
	private final Map<Marker, EquipmentMapDto> mapEquipmentMarkers = new HashMap<>();
	private final Map<Marker, VehicleMapDto> mapVehicleMarkers = new HashMap<>();
	private final Map<Marker, PositionDto> mapRouteMarkers = new HashMap<>();

	private final GeocodeMapDto<WorkMapDto> geocodeMapWorks = new GeocodeMapDto<>();
	private final GeocodeMapDto<RequestMapDto> geocodeMapRequests = new GeocodeMapDto<>();
	// Интерфейс для отображения карты
	private final MapLayout mapLayout;
	private boolean useYandexMapPictures = false;
	private boolean showBaltikaMap = false;
	private Boolean updateVisible = true;
	private VehicleMapDto selectedVehicle = null;
	private EquipmentMapDto selectedEquipment = null;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();
	// Таймер для сохранения настроек
	private TaskTimer.Task saveSettingsTask = () -> {
		/*BrigadesUI.setCurrentSetting(PAR_MAP_ZOOM, String.valueOf(getMapLayout().getZoom()));
		Coordinates center = getMapLayout().getCenter();
		BrigadesUI.setCurrentSetting(PAR_MAP_CENTER_LAT, String.valueOf(center.getLat()));
		BrigadesUI.setCurrentSetting(PAR_MAP_CENTER_LON, String.valueOf(center.getLon()));*/
	};
	private TaskTimer saveSettingsTimer = new TaskTimer(saveSettingsTask, SAVE_SETTINGS_INTERVAL);
	private MenuBar.MenuItem filter;
	/**
	 * Обработка клика мышкой на маркере на карте
	 */
	private MapMarkerClickListener mapMarkerClickListener = marker -> {
		WorkMapDto workMapDto = mapWorkOrderMarkers.get(marker);
		if (workMapDto != null) {
			WorkDto work = restTemplate.getWork(workMapDto.getId());
			//BrigadesEventBus.post(new SelectWorkOrderEvent(work, marker));
			WorkOrderLayout.showWorkOrder(work);
			return;
		}
		RequestMapDto requestMapDto = mapRequestMarkers.get(marker);
		if (requestMapDto != null) {
			RequestDto request = restTemplate.getRequest(requestMapDto.getId());
			//BrigadesEventBus.post(new SelectRequestEvent(request, marker));
			RequestLayout.showRequest(request);
			return;
		}
		BrigadeMapDto brigadeMapDto = mapWorkGroupMarkers.get(marker);
		if (brigadeMapDto != null) {
			//RequestDto request = restTemplate.getRequest(requestMapDto.getId());
			//BrigadesEventBus.post(new SelectRequestEvent(request, marker));
			return;
		}
		CameraDto camera = mapCameraMarkers.get(marker);
		if (camera != null) {
			BrigadesEventBus.post(new ShowCameraEvent(camera, marker));
			return;
		}
		VehicleMapDto vehicle = mapVehicleMarkers.get(marker);
		if (vehicle != null) {
			showVehicleTrack(vehicle);
			return;
		}
		EquipmentMapDto equipmentMapDto = mapEquipmentMarkers.get(marker);
		if (equipmentMapDto != null) {
			showVehicleTrack(equipmentMapDto);
		}
	};

	/**
	 * Обработка и сохранение изменений центра карты, масштаба и т.п.
	 */
	private MapNotifyEventListener mapBoundsChangedListener = object -> saveSettings();

	public MapViewLayout() {
		this.mapLayout = new YandexMapLayout(DEFAULT_ZOOM);

		BrigadesEventBus.register(this);
		mapLayout.addMarkerClickListener(mapMarkerClickListener);
		mapLayout.addBoundsChangedListener(mapBoundsChangedListener);
		readSettings();

		//update();
	}

	/**
	 * Чтение настроек из БД
	 */
	private void readSettings() {
		/*String z = BrigadesUI.getCurrentSettingValue(PAR_MAP_ZOOM);
		if (z != null) {
			mapLayout.setZoom(Integer.valueOf(z));
		}
		String lat = BrigadesUI.getCurrentSettingValue(PAR_MAP_CENTER_LAT);
		String lon = BrigadesUI.getCurrentSettingValue(PAR_MAP_CENTER_LON);
		if (lat != null && lon != null) {
			mapLayout.setCenter(new Coordinates(Double.valueOf(lat), Double.valueOf(lon)));
		}*/

		mapRegion.getPoints().clear();
		restTemplate.getMapRegion().forEach(r -> mapRegion.getPoints().add(new Coordinates(r.getLatitude(), r.getLongitude())));
		mapRegion.setStrokeColor("#2c3e50"); // цвет
		mapRegion.setStrokeWidth(3); // ширина в пикселях
		mapRegion.setStrokeStyle("4 2"); // тип линии
		mapLayout.addPolyline(mapRegion);
	}

	/**
	 * Запись настроек в БД (отложенная)
	 */
	private void saveSettings() {
		saveSettingsTimer.call();
	}

	/**
	 * Update map
	 */
	public void update() {
		String strDescription, strUri;
		Marker marker;
		GeocodeDto geocode;
		//mapLayout.removeAllMarkers();
		//mapLayout.removeAllPolylines();

		if (updateVisible) {
			List<Marker> markersToRemove = new ArrayList<>();
			EntityMapCollectionDto mapEntities = restTemplate.getMapEntities();
			/*for (BrigadeMapDto brigadeMapDto : mapEntities.getBrigades()) {
				geocode = brigadeMapDto.getGeocode();
				if (geocode != null && !mapWorkGroupMarkers.containsValue(brigadeMapDto)) {
					strDescription = createDescriptionForMarker(brigadeMapDto);
					strUri = URL_IMAGE_CAR_DEFAULT;
					marker = new Marker(null, strDescription, strUri, geocode.getLatitude(), geocode.getLongitude());
					if (mapLayout instanceof YandexMapLayout) {
						if (useYandexMapPictures) {
							marker.setUri(null);
							marker.setIconPreset("islands#carIcon");
							marker.setIconColor("blue");
						} else {
							marker.setImageSize(IMG_CAR_WIDTH, IMG_CAR_HEIGHT);
							marker.setImageOffset(IMG_CAR_OFFSET_X, IMG_CAR_OFFSET_Y);
						}
					}
					mapLayout.addMarker(marker);
					mapWorkGroupMarkers.put(marker, brigadeMapDto);
				} else if (geocode != null && mapWorkGroupMarkers.containsValue(brigadeMapDto)) {
					marker = (Marker) getKey(mapWorkGroupMarkers, brigadeMapDto);
					if (marker != null) {
						mapWorkGroupMarkers.remove(marker);
						marker.setCoordinates(new Coordinates(geocode.getLatitude(), geocode.getLongitude()));
						marker.setDescription(createDescriptionForMarker(brigadeMapDto));
						mapLayout.addMarker(marker);
						mapWorkGroupMarkers.put(marker, brigadeMapDto);
					}
				}
			}
			for (BrigadeMapDto brigadeMapDto : mapWorkGroupMarkers.values()) {
				if (!mapEntities.getBrigades().contains(brigadeMapDto)) {
					marker = (Marker) getKey(mapWorkGroupMarkers, brigadeMapDto);
					markersToRemove.add(marker);
				}
			}*/
			for (RequestMapDto requestMapDto : mapEntities.getRequests()) {
				geocode = requestMapDto.getGeocode();
				if (geocode != null) {
					geocodeMapRequests.addGeocodeValue(geocode, requestMapDto);
					/*if (mapRequestMarkers.containsValue(requestMapDto)) {
						marker = (Marker) getKey(mapRequestMarkers, requestMapDto);
						if (marker != null) {
							mapRequestMarkers.remove(marker);
							marker.setCoordinates(new Coordinates(geocode.getLatitude(), geocode.getLongitude()));
							marker.setDescription(createDescriptionForMarker(requestMapDto));
							marker.setIconPreset(RequestConstants.getYandexIcon(requestMapDto.getStatus()));
							marker.setIconColor(RequestConstants.getYandexIconColor(requestMapDto.getStatus()));
							mapLayout.addMarker(marker);
							mapRequestMarkers.put(marker, requestMapDto);
						}
					} else {
						strDescription = createDescriptionForMarker(requestMapDto);
						strUri = (mapLayout instanceof YandexMapLayout) ? null : URL_IMAGE_REQUEST;
						marker = new Marker(requestMapDto.getProblemCode(), strDescription, strUri, geocode.getLatitude(), geocode.getLongitude());
						if (mapLayout instanceof YandexMapLayout) {
							marker.setIconPreset(RequestConstants.getYandexIcon(requestMapDto.getStatus()));
							marker.setIconColor(RequestConstants.getYandexIconColor(requestMapDto.getStatus()));
						}
						mapLayout.addMarker(marker);
						mapRequestMarkers.put(marker, requestMapDto);
					}*/
				}
			}
			for (WorkMapDto workMapDto : mapEntities.getWorks()) {
				geocode = workMapDto.getGeocode();
				//boolean isEmergency = WorkOrderConstants.isEmergency(workMapDto);
				if (geocode != null) {
					geocodeMapWorks.addGeocodeValue(geocode, workMapDto);
					/*if (mapWorkOrderMarkers.containsValue(workMapDto)) {
						marker = (Marker) getKey(mapWorkOrderMarkers, workMapDto);
						if (marker != null) {
							mapWorkOrderMarkers.remove(marker);
							marker.setCoordinates(new Coordinates(geocode.getLatitude(), geocode.getLongitude()));
							marker.setDescription(createDescriptionForMarker(workMapDto));
							marker.setUri(URL_IMAGE_DEFAULT_PATH + WorkOrderConstants.getStatusIconFileName(workMapDto.getStatus(), isEmergency));
							mapLayout.addMarker(marker);
							mapWorkOrderMarkers.put(marker, workMapDto);
						}
					} else {
						strDescription = createDescriptionForMarker(workMapDto);

						// отображение иконки "работа"
						strUri = WorkOrderConstants.getStatusIconFileName(workMapDto.getStatus(), isEmergency);
						strUri = (strUri != null) ? URL_IMAGE_DEFAULT_PATH + strUri : URL_IMAGE_WORK_DEFAULT;
						marker = new Marker(null, strDescription, strUri, geocode.getLatitude(), geocode.getLongitude());
						if (mapLayout instanceof YandexMapLayout) {
							if (useYandexMapPictures) {
								marker.setUri(null);
								marker.setIconPreset("islands#workshopIcon");
								marker.setIconColor(WorkOrderConstants.getStatusColor(workMapDto.getStatus(), isEmergency));
							} else {
								marker.setImageSize(IMG_WORK_WIDTH, IMG_WORK_HEIGHT);
								marker.setImageOffset(IMG_WORK_OFFSET_X, IMG_WORK_OFFSET_Y);
							}
						}
						mapLayout.addMarker(marker);
						mapWorkOrderMarkers.put(marker, workMapDto);
					}*/
				}
			}
			/*for (VehicleMapDto vehicleMapDto : mapEntities.getVehicles()) {
				geocode = vehicleMapDto.getGeocode();
				if (geocode != null && !mapVehicleMarkers.containsValue(vehicleMapDto)) {
					Double latitude = geocode.getLatitude();
					Double longitude = geocode.getLongitude();
					if (latitude != null && longitude != null && latitude != 0 && longitude != 0) {
						strDescription = createDescriptionForMarker(vehicleMapDto);
						strUri = vehicleMapDto.getType().equalsIgnoreCase("экскаватор") ? URL_IMAGE_TRACTOR_DEFAULT : URL_IMAGE_TRUCK_DEFAULT;
						marker = new Marker(vehicleMapDto.getNumber(), strDescription, strUri, latitude, longitude);
						if (mapLayout instanceof YandexMapLayout) {
							if (useYandexMapPictures) {
								marker.setUri(null);
								marker.setIconPreset("islands#truckIcon");
								marker.setIconColor("blue");
							} else {
								marker.setImageSize(IMG_TRUCK_WIDTH, IMG_TRUCK_HEIGHT);
								marker.setImageOffset(IMG_TRUCK_OFFSET_X, IMG_TRUCK_OFFSET_Y);
							}
						}
						mapLayout.addMarker(marker);
						mapVehicleMarkers.put(marker, vehicleMapDto);
					}
				} else if (geocode != null && mapVehicleMarkers.containsValue(vehicleMapDto)) {
					marker = (Marker) getKey(mapVehicleMarkers, vehicleMapDto);
					if (marker != null) {
						mapVehicleMarkers.remove(marker);
						Double latitude = geocode.getLatitude();
						Double longitude = geocode.getLongitude();
						marker.setCoordinates(new Coordinates(latitude, longitude));
						marker.setDescription(createDescriptionForMarker(vehicleMapDto));
						mapLayout.addMarker(marker);
						mapVehicleMarkers.put(marker, vehicleMapDto);
					}
				}
			}*/
			for (EquipmentMapDto equipmentMapDto : mapEntities.getEquipments()) {
				geocode = equipmentMapDto.getGeocode();
				if (geocode != null) {
					Double latitude = geocode.getLatitude();
					Double longitude = geocode.getLongitude();
					strDescription = createDescriptionForMarker(equipmentMapDto);
					if (mapEquipmentMarkers.containsValue(equipmentMapDto)) {
						marker = (Marker) getKey(mapEquipmentMarkers, equipmentMapDto);
						if (marker != null) {
							mapEquipmentMarkers.remove(marker);
							marker.setCoordinates(new Coordinates(latitude, longitude));
							marker.setDescription(strDescription);
							mapLayout.addMarker(marker);
							mapEquipmentMarkers.put(marker, equipmentMapDto);
						}
					} else {
						if (latitude != null && longitude != null && latitude != 0 && longitude != 0) {
							strUri = carIcons.getOrDefault(equipmentMapDto.getType(), URL_IMAGE_TRUCK_DEFAULT);
							marker = new Marker(equipmentMapDto.getNumber(), strDescription, strUri, latitude, longitude);
							if (mapLayout instanceof YandexMapLayout) {
								if (useYandexMapPictures) {
									marker.setUri(null);
									marker.setIconPreset("islands#truckIcon");
									marker.setIconColor("blue");
								} else {
									marker.setImageSize(IMG_TRUCK_WIDTH, IMG_TRUCK_HEIGHT);
									marker.setImageOffset(IMG_TRUCK_OFFSET_X, IMG_TRUCK_OFFSET_Y);
								}
							}
							mapLayout.addMarker(marker);
							mapEquipmentMarkers.put(marker, equipmentMapDto);
						}
					}
				}
			}
			for (EquipmentMapDto equipmentMapDto : mapEquipmentMarkers.values()) {
				if (!mapEntities.getEquipments().contains(equipmentMapDto)) {
					marker = (Marker) getKey(mapEquipmentMarkers, equipmentMapDto);
					markersToRemove.add(marker);
				}
			}
			//TODO-G  "Убрать значки камер с карты"
			/*
			List<CameraDto> cameras = restTemplate.getCameras();
			for (CameraDto camera : cameras) {
				boolean red = camera.getStatus().equals(CameraDto.Status.UNAVAILABLE.name());
				if (camera.getAddressId() != null && !mapCameraMarkers.containsValue(camera)) {
					CameraAddressDto address = restTemplate.getCameraAddress(camera.getAddressId());
					if (address != null) {
						strDescription = createDescriptionForMarker(camera);
						strUri = URL_IMAGE_CAMERA;
						marker = new Marker(String.valueOf(camera.getName()), strDescription, strUri, address.getLatitude(), address.getLongitude());
						marker.setImageSize(IMG_CAMERA_WIDTH, IMG_CAMERA_HEIGHT);
						marker.setImageOffset(IMG_CAMERA_OFFSET_X, IMG_CAMERA_OFFSET_Y);
						marker.setUri(red ? URL_IMAGE_CAMERA_RED : URL_IMAGE_CAMERA_GREEN);
						marker.setIconColor(red ? "red" : "green");
						mapLayout.addMarker(marker);
						mapCameraMarkers.put(marker, camera);
					}
				} else if (camera.getAddressId() != null && mapCameraMarkers.containsValue(camera)) {
					marker = (Marker) getKey(mapCameraMarkers, camera);
					CameraAddressDto address = restTemplate.getCameraAddress(camera.getAddressId());
					if (marker != null && address != null) {
						mapCameraMarkers.remove(marker);
						Double latitude = address.getLatitude();
						Double longitude = address.getLongitude();
						marker.setCoordinates(new Coordinates(latitude, longitude));
						marker.setDescription(createDescriptionForMarker(camera));
						marker.setUri(red ? URL_IMAGE_CAMERA_RED : URL_IMAGE_CAMERA_GREEN);
						marker.setIconColor(red ? "red" : "green");
						mapLayout.addMarker(marker);
						mapCameraMarkers.put(marker, camera);
					}
				} else if (camera.getAddressId() == null && mapCameraMarkers.containsValue(camera)) {
					marker = (Marker) getKey(mapCameraMarkers, camera);
					if (marker != null) markersToRemove.add(marker);
				}
			}*/
			for (GeocodeDto geocodeDto : geocodeMapRequests.getGeocodeMap().keySet()) {
				List<RequestMapDto> geocodeValues = geocodeMapRequests.getGeocodeValues(geocodeDto);
				strDescription = createReqDescForMarker(geocodeDto, geocodeValues);
				strUri = (mapLayout instanceof YandexMapLayout) ? null : URL_IMAGE_REQUEST;
				if (mapRequestGeoCodeMarkers.containsKey(geocodeDto)) {
					marker = mapRequestGeoCodeMarkers.get(geocodeDto);
					marker.setDescription(strDescription);
					marker.setUri(strUri);
					marker.setCoordinates(new Coordinates(geocodeDto.getLatitude(), geocodeDto.getLongitude()));
				} else {
					marker = new Marker(null, strDescription, strUri, geocodeDto.getLatitude(), geocodeDto.getLongitude());
				}
				if (geocodeValues.size() == 1) {
					RequestMapDto requestMapDto = geocodeValues.get(0);
					marker.setCaption(requestMapDto.getProblemCode());
					marker.setIconPreset(RequestConstants.getYandexIcon(requestMapDto.getStatus()));
					marker.setIconColor(RequestConstants.getYandexIconColor(requestMapDto.getStatus()));
					mapRequestMarkers.put(marker, requestMapDto);
				}
				mapRequestGeoCodeMarkers.put(geocodeDto, marker);
				mapLayout.addMarker(marker);
			}
			for (GeocodeDto geocodeDto : geocodeMapWorks.getGeocodeMap().keySet()) {
				List<WorkMapDto> geocodeValues = geocodeMapWorks.getGeocodeValues(geocodeDto);
				strDescription = createWorkDescForMarker(geocodeDto, geocodeValues);
				strUri = URL_IMAGE_WORK_DEFAULT;
				if (mapWorkOrderGeoCodeMarkers.containsKey(geocodeDto)) {
					marker = mapWorkOrderGeoCodeMarkers.get(geocodeDto);
					marker.setUri(strUri);
					marker.setDescription(strDescription);
					marker.setCoordinates(new Coordinates(geocodeDto.getLatitude(), geocodeDto.getLongitude()));
				} else {
					marker = new Marker(null, strDescription, strUri, geocodeDto.getLatitude(), geocodeDto.getLongitude());
				}
				if (mapLayout instanceof YandexMapLayout) {
					marker.setImageSize(IMG_WORK_WIDTH, IMG_WORK_HEIGHT);
					marker.setImageOffset(IMG_WORK_OFFSET_X, IMG_WORK_OFFSET_Y);
				}
				if (geocodeValues.size() == 1) {
					WorkMapDto workMapDto = geocodeValues.get(0);
					boolean emergency = WorkOrderConstants.isEmergency(workMapDto);
					strUri = WorkOrderConstants.getStatusIconFileName(workMapDto.getStatus(), emergency);
					strUri = (strUri != null) ? URL_IMAGE_DEFAULT_PATH + strUri : URL_IMAGE_WORK_DEFAULT;
					marker.setUri(strUri);
					marker.setIconColor(WorkOrderConstants.getStatusColor(workMapDto.getStatus(), emergency));
					mapWorkOrderMarkers.put(marker, workMapDto);
				}
				mapWorkOrderGeoCodeMarkers.put(geocodeDto, marker);
				mapLayout.addMarker(marker);
			}
			for (RequestMapDto requestMapDto : mapRequestMarkers.values()) {
				if (!mapEntities.getRequests().contains(requestMapDto)) {
					marker = (Marker) getKey(mapRequestMarkers, requestMapDto);
					markersToRemove.add(marker);
				}
			}
			for (WorkMapDto workMapDto : mapWorkOrderMarkers.values()) {
				if (!mapEntities.getWorks().contains(workMapDto)) {
					marker = (Marker) getKey(mapWorkOrderMarkers, workMapDto);
					markersToRemove.add(marker);
				}
			}
			for (Marker removeMarker : markersToRemove) {
				mapLayout.removeMarker(removeMarker);
				mapWorkGroupMarkers.remove(removeMarker);
				mapRequestMarkers.remove(removeMarker);
				mapWorkOrderMarkers.remove(removeMarker);
				mapCameraMarkers.remove(removeMarker);
				mapVehicleMarkers.remove(removeMarker);
				mapEquipmentMarkers.remove(removeMarker);
			}
			toggleRequests();
			toggleWorks();
			toggleEquipments();
		}
	}

	private  <K, V> K getKey(Map<K, V> map, V value) {
		for (Map.Entry<K, V> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	private String createReqDescForMarker(GeocodeDto geocode, List<RequestMapDto> requests) {
		String toFormatMain = "<STRONG>Адрес:</STRONG> %s %s";
		String toFormatReq = "\n<BR>\n<BR><STRONG>Заявка:</STRONG> %s" +
				"\n<BR><STRONG>Код проблемы:</STRONG> %s" +
				"\n<BR><STRONG>Тип заявки:</STRONG> %s" +
				"\n<BR><STRONG>Статус:</STRONG> %s";
		String reqValue = "";
		String address = StringUtil.returnString(geocode.getName());
		for (RequestMapDto requestMapDto : requests) {
			String desc = StringUtil.returnString(requestMapDto.getDescription());
			String code = StringUtil.returnString(requestMapDto.getProblemCode());
			String type = StringUtil.returnString(requestMapDto.getProblemName());
			String status = StringUtil.returnString(requestMapDto.getStatus());
			reqValue = reqValue.concat(String.format(toFormatReq, desc, code, type, status));
		}
		return String.format(toFormatMain, address, reqValue);
	}

	private String createWorkDescForMarker(GeocodeDto geocode, List<WorkMapDto> works) {
		String toFormatMain = "<STRONG>Адрес работы:</STRONG> %s %s";
		String toFormatWork = "\n<BR>\n<BR><STRONG>Бригада:</STRONG> %s" +
				"\n<BR><STRONG>Вид работы:</STRONG> %s" +
				"\n<BR><STRONG>Статус:</STRONG> %s" +
				"\n<BR><STRONG>Время по плану:</STRONG> %s - %s %s";
		String factTimeFormat = "\n<BR><STRONG>Время по факту:</STRONG> %s - %s";
		String factTime = "";
		String workValue = "";
		String address = StringUtil.returnString(geocode.getName());
		for (WorkMapDto workMapDto : works) {
			String brigade = workMapDto.getBrigade() == null ? "(не назначена)" : workMapDto.getBrigade();
			String status = StringUtil.returnString(workMapDto.getStatus());
			String type = StringUtil.returnString(workMapDto.getType());
			String strStartTime = DatesUtil.formatZoned(workMapDto.getStartDatePlan());
			String strEndTime = DatesUtil.formatZoned(workMapDto.getFinishDatePlan());
			if (!WorkOrderConstants.isWorkStatusEmergency(workMapDto.getStatus()) &&
					workMapDto.getStartDateFact() != null && workMapDto.getFinishDateFact() != null) {
				String strStartTimeFact = DatesUtil.formatZoned(workMapDto.getStartDateFact());
				String strEndTimeFact = DatesUtil.formatZoned(workMapDto.getFinishDateFact());
				factTime = String.format(factTimeFormat, strStartTimeFact, strEndTimeFact);
			}
			workValue = workValue.concat(String.format(toFormatWork, brigade, type, status, strStartTime, strEndTime, factTime));
		}
		return String.format(toFormatMain, address, workValue);
	}

	private String createDescriptionForMarker(BrigadeMapDto brigadeMapDto) {
		String toFormat = "<STRONG>Бригада:</STRONG> %s" +
								"\n<BR><STRONG>Адрес:</STRONG> %s" +
								"\n<BR><STRONG>Время по плану:</STRONG> %s - %s";
		String name = StringUtil.returnString(brigadeMapDto.getName());
		String address = StringUtil.returnString(brigadeMapDto.getAddress());
		String strStartTime = DatesUtil.formatZoned(brigadeMapDto.getStartDatePlanWork());
		String strEndTime = DatesUtil.formatZoned(brigadeMapDto.getFinishDatePlanWork());
		return String.format(toFormat, name, address, strStartTime, strEndTime);
	}

	private String createDescriptionForMarker(RequestMapDto requestMapDto) {
		String toFormat = "<STRONG>Заявка:</STRONG> %s" +
				"\n<BR><STRONG>Адрес:</STRONG> %s" +
				"\n<BR><STRONG>Тип заявки:</STRONG> %s" +
				"\n<BR><STRONG>Статус:</STRONG> %s";
		String desc = StringUtil.returnString(requestMapDto.getDescription());
		String address = StringUtil.returnString(requestMapDto.getAddress());
		String type = StringUtil.returnString(requestMapDto.getProblemName());
		String status = StringUtil.returnString(requestMapDto.getStatus());
		//getLogger().debug("Request: {}, description length: {}", requestMapDto.getId(), desc.length());
		/*try {
			getLogger().debug(StringUtil.splitHtmlString(desc, 100));
		} catch (Exception e) {
			getLogger().error(e.getMessage());
		}*/
		return String.format(toFormat, desc, address, type, status);
	}

	private String createDescriptionForMarker(WorkMapDto workMapDto) {
		String toFormat = "<STRONG>Бригада:</STRONG> %s" +
				"\n<BR><STRONG>Адрес работы:</STRONG> %s" +
				"\n<BR><STRONG>Вид работы:</STRONG> %s" +
				"\n<BR><STRONG>Статус:</STRONG> %s" +
				"\n<BR><STRONG>Время по плану:</STRONG> %s - %s %s";
		String factTimeFormat = "\n<BR><STRONG>Время по факту:</STRONG> %s - %s";
		String factTime = "";
		String brigade = workMapDto.getBrigade() == null ? "(не назначена)" : workMapDto.getBrigade();
		String address = StringUtil.returnString(workMapDto.getAddress());
		String status = StringUtil.returnString(workMapDto.getStatus());
		String type = StringUtil.returnString(workMapDto.getType());
		String strStartTime = DatesUtil.formatZoned(workMapDto.getStartDatePlan());
		String strEndTime = DatesUtil.formatZoned(workMapDto.getFinishDatePlan());
		if (!WorkOrderConstants.isWorkStatusEmergency(workMapDto.getStatus()) &&
				workMapDto.getStartDateFact() != null && workMapDto.getFinishDateFact() != null) {
			String strStartTimeFact = DatesUtil.formatZoned(workMapDto.getStartDateFact());
			String strEndTimeFact = DatesUtil.formatZoned(workMapDto.getFinishDateFact());
			factTime = String.format(factTimeFormat, strStartTimeFact, strEndTimeFact);
		}
		return String.format(toFormat, brigade, address, type, status, strStartTime, strEndTime, factTime);
	}

	private String createDescriptionForMarker(VehicleMapDto vehicleMapDto) {
		String toFormat = "<STRONG>Номер машины: </STRONG> %s" +
				"\n<BR><STRONG>Тип машины: </STRONG> %s" +
				"\n<BR><STRONG>Время обновления: </STRONG> %s" +
				"\n<BR><STRONG>Состояние машины: </STRONG> %s";
		String number = StringUtil.returnString(vehicleMapDto.getNumber());
		String type = StringUtil.returnString(vehicleMapDto.getType());
		String updated = DatesUtil.formatZoned(vehicleMapDto.getLastUpdated());
		String state = StringUtil.returnString(vehicleMapDto.getState());
		return String.format(toFormat, number, type, updated, state);
	}

	private String createDescriptionForMarker(EquipmentMapDto equipmentMapDto) {
		String toFormat = "<STRONG>Номер машины: </STRONG> %s" +
				"\n<BR><STRONG>Тип машины: </STRONG> %s" +
				"\n<BR><STRONG>Марка: </STRONG> %s" +
				"\n<BR><STRONG>Бригада: </STRONG> %s" +
				"\n<BR><STRONG>Время обновления: </STRONG> %s" +
				"\n<BR><STRONG>Состояние машины: </STRONG> %s";
		String number = StringUtil.returnString(equipmentMapDto.getNumber());
		String type = StringUtil.returnString(equipmentMapDto.getType());
		String model = StringUtil.returnString(equipmentMapDto.getModel());
		String brigade = StringUtil.returnString(equipmentMapDto.getBrigade());
		String updated = DatesUtil.formatZoned(equipmentMapDto.getLastUpdated());
		String state = StringUtil.returnString(equipmentMapDto.getState());
		return String.format(toFormat, number, type, model, brigade, updated, state);
	}

	private String createDescriptionForMarker(CameraDto cameraDto) {
		String toFormat = "<STRONG>Камера: </STRONG> %s" +
				"\n<BR><STRONG>Адрес: </STRONG> %s";
		String name = StringUtil.returnString(cameraDto.getName());
		String address = StringUtil.returnString(cameraDto.getAddress());
		return String.format(toFormat, name, address);
	}

	public Component getComponent() {
		return (Component) mapLayout;
	}

	public void setCenter(Coordinates coordinates) {
		mapLayout.setCenter(coordinates);
	}

	public void goToCoordinates(Coordinates coordinates, String locationName) {
		mapLayout.goToLocation(coordinates, locationName);
	}

	public MapLayout getMapLayout() {
		return mapLayout;
	}

	/**
	 * Добавление кнопок меню в заголовок окна
	 */
	public void addToolBarItems(BrigadesPanel panel) {
		//panel.addToolbarItem("", "Отобразить все работающие бригады", BrigadesTheme.ICON_WORKGROUP, $ -> doShowAllWorkGroups());
		//filter = initMapFilter(panel.getToolBar());
		filter = panel.addFilterItem("", "Фильтр", BrigadesTheme.ICON_FILTER, filterMap);
		panel.addToolbarItem("", "Слой Балтика", BrigadesTheme.ICON_LAYOUT, $ -> showBaltikaLayout());
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, legend);
		panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, $ -> update());
	}

	private void toggleRequests() {
		MenuBar.MenuItem item = filter.getChildren().stream().filter(menuItem ->
				"Заявки".equalsIgnoreCase(menuItem.getText())).findFirst().orElse(null);
		boolean itemChecked = true;
		if (item != null) {
			itemChecked = item.isChecked();
		}
		for (Marker marker : mapRequestGeoCodeMarkers.values()) {
			if (itemChecked) {
				mapLayout.addMarker(marker);
			} else {
				mapLayout.removeMarker(marker);
			}
		}
	}

	private void toggleWorks() {
		MenuBar.MenuItem item = filter.getChildren().stream().filter(menuItem ->
				"Работы".equalsIgnoreCase(menuItem.getText())).findFirst().orElse(null);
		boolean itemChecked = true;
		if (item != null) {
			itemChecked = item.isChecked();
		}
		for (Marker marker : mapWorkOrderGeoCodeMarkers.values()) {
			if (itemChecked) {
				mapLayout.addMarker(marker);
			} else {
				mapLayout.removeMarker(marker);
			}
		}
	}

	private void toggleEquipments() {
		MenuBar.MenuItem item = filter.getChildren().stream().filter(menuItem ->
				"Техника".equalsIgnoreCase(menuItem.getText())).findFirst().orElse(null);
		boolean itemChecked = true;
		if (item != null) {
			itemChecked = item.isChecked();
		}
		for (Marker marker : mapEquipmentMarkers.keySet()) {
			if (itemChecked) {
				mapLayout.addMarker(marker);
			} else {
				mapLayout.removeMarker(marker);
			}
		}
	}

	private void showBaltikaLayout() {
		showBaltikaMap = !showBaltikaMap;
		mapLayout.setVisibleLayout(showBaltikaMap, 1000, mapLayout.getCenter().getLat(), mapLayout.getCenter().getLon());
	}

	private void showVehicleTrack(VehicleMapDto vehicleDto) {
		getLogger().debug("showVehicleTrack({});", vehicleDto.debugInfo());
		mapRouteMarkers.keySet().forEach(mapLayout::removeMarker);
		mapRouteMarkers.clear();
		if (!mapRoute.getPoints().isEmpty()) {
			mapLayout.removePolyline(mapRoute);
			mapRoute.getPoints().clear();
		}
		if (!mapVehicleMarkers.containsValue(vehicleDto)) return;
		if (selectedVehicle != null && vehicleDto.equals(selectedVehicle)) {
			selectedVehicle = null;
			return;
		} else {
			selectedVehicle = vehicleDto;
		}
		VehicleDto vehicleByNumber = restTemplate.getVehicleByNumber(vehicleDto.getNumber());

		if (vehicleByNumber != null) {
			List<PositionDto> vehicleTracks = restTemplate.getVehicleTracks(vehicleByNumber.getId());

			if (vehicleTracks.isEmpty()) return;
			showRoute(vehicleTracks);
		}
	}

	private void showVehicleTrack(EquipmentMapDto equipmentMapDto) {
		getLogger().debug("showEquipmentTrack({});", StringUtil.writeValueAsString(equipmentMapDto));
		mapRouteMarkers.keySet().forEach(mapLayout::removeMarker);
		mapRouteMarkers.clear();
		if (!mapRoute.getPoints().isEmpty()) {
			mapLayout.removePolyline(mapRoute);
			mapRoute.getPoints().clear();
		}
		if (!mapEquipmentMarkers.containsValue(equipmentMapDto)) return;
		if (selectedEquipment != null && equipmentMapDto.equals(selectedEquipment)) {
			selectedEquipment = null;
			return;
		} else {
			selectedEquipment = equipmentMapDto;
		}

		if (equipmentMapDto != null) {
			List<PositionDto> vehicleTracks = restTemplate.getVehicleTracks(equipmentMapDto.getNumber());

			if (vehicleTracks.isEmpty()) return;
			showRoute(vehicleTracks);
		}
	}

	private void showRoute(List<PositionDto> positionDtoList) {
		for (PositionDto route : positionDtoList) {
			Float lat = route.getX();
			Float lon = route.getY();
			if (lat != null && lon != null && lat != 0 && lon != 0) {
				Coordinates coordinates = new Coordinates(lat, lon);
				mapRoute.getPoints().add(coordinates);
				String strDescription = "<STRONG>Тип машины: </STRONG>".concat(route.getModel()).concat("<BR>")
						.concat("<STRONG>Номер машины: </STRONG>").concat(route.getNumber()).concat("<BR>")
						.concat("<STRONG>Время обновления: </STRONG>").concat(DatesUtil.formatZoned(route.getGmt())).concat("<BR>")
						.concat("<STRONG>Состояние машины: </STRONG>").concat(route.getCondition()).concat("<BR>");
				Marker marker = new Marker();
				marker.setCaption(null);
				marker.setUri(null);
				marker.setDescription(strDescription);
				marker.setCoordinates(coordinates);
				if (route.getCondition().equalsIgnoreCase("остановка"))
					marker.setIconPreset("islands#greenParkingCircleIcon");
				else marker.setIconPreset("islands#greenCircleIcon");
				//marker.setIconColor("green");
				mapLayout.addMarker(marker);
				mapRouteMarkers.put(marker, route);
			}
		}
		mapRoute.setStrokeColor("#2c3e50"); // цвет
		mapRoute.setStrokeWidth(2); // ширина в пикселях
		mapRoute.setStrokeStyle("2 1"); // тип линии
		mapLayout.addPolyline(mapRoute);
		//mapLayout.addRoute(carRoute);
	}

	private EquipmentMapDto findEquipmentByNumber(String number) {
		return mapEquipmentMarkers.values().stream()
				.filter(equipmentMapDto -> equipmentMapDto.getNumber().equalsIgnoreCase(number))
				.findFirst().orElse(null);
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		update();
	}

	@Subscribe
	public void handleShowVehicleEvent(final ShowVehicleEvent event) {
		VehicleDto vehicleDto = event.getEntity();
		if (vehicleDto == null) return;
		//workOrderLayout.select(vehicleDto);
		if (event.getSender() instanceof Marker) return;
		EquipmentMapDto equipmentMapDto = findEquipmentByNumber(vehicleDto.getNumber());
		if (equipmentMapDto == null) return;
		GeocodeDto geocode = equipmentMapDto.getGeocode();
		if (geocode == null) return;
		goToCoordinates(new Coordinates(geocode.getLatitude(), geocode.getLongitude()), vehicleDto.getNumber());
	}
}
