package ru.telros.brigades.client.ui.vehicle;

import com.vaadin.ui.CssLayout;
import ru.telros.brigades.client.dto.EquipmentWorkDto;
import ru.telros.brigades.client.dto.ExcelData;
import ru.telros.brigades.client.dto.VehicleDto;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public abstract class VehicleColumn {
	private static final GridColumn<VehicleDto> FLAG = GridColumn.createComponentColumn("_flag", "", VehicleColumn::createFlag);
	private static final GridColumn<VehicleDto> WORK_ORDER_ADDRESS = GridColumn.createColumn("woAddress", "Адрес работ", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> WORK_ORDER = GridColumn.createColumn("workOrder", "Работа", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> TYPE = GridColumn.createColumn("type", "Тип ТС", VehicleDto::getType);
	private static final GridColumn<VehicleDto> NUMBER = GridColumn.createColumn("number", "Гос. номер", VehicleDto::getNumber);
	private static final GridColumn<VehicleDto> WORK_GROUP = GridColumn.createColumn("workGroup", "Бригада", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> MANAGER = GridColumn.createColumn("manager", "Мастер / Ответственный", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> PHONE = GridColumn.createColumn("phone", "Телефон", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> ADDRESS = GridColumn.createColumn("address", "Адрес подачи", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> COMMENT = GridColumn.createColumn("comment", "Комментарий", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> FILIAL = GridColumn.createColumn("filial", "Филиал", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> TERRITORY = GridColumn.createColumn("territory", "ТУВ", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> REGION = GridColumn.createColumn("region", "РВ", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> START_TIME_PLAN = GridColumn.createColumn("startTimePlan", "Время подачи план", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> START_TIME_FACT = GridColumn.createColumn("startTimeFact", "Время подачи факт", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> END_TIME_PLAN = GridColumn.createColumn("endTimePlan", "Время конца работ план", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> END_TIME_FACT = GridColumn.createColumn("endTimeFact", "Время конца работ факт", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> NOTE = GridColumn.createColumn("note", "Примечание", GridColumn::nullProvider);

	private static final GridColumn<VehicleDto> NUMBER_PLAN = GridColumn.createColumn("numberPlan", "Гос. номер фургона ПЛАН", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> NUMBER_FACT = GridColumn.createColumn("numberFact", "Гос. номер фургона ФАКТ", GridColumn::nullProvider);
	private static final GridColumn<VehicleDto> TIME = GridColumn.createColumn("time", "Время подачи", GridColumn::nullProvider);

	private static final GridColumn<EquipmentWorkDto> TYPE_ = GridColumn.createColumn("_type", "Тип", EquipmentWorkDto::getType);
	private static final GridColumn<EquipmentWorkDto> NUMBER_ = GridColumn.createColumn("_num", "Гос. номер", EquipmentWorkDto::getNumber);
	private static final GridColumn<EquipmentWorkDto> START_PLAN = GridColumn.createDateColumn("startDatePlan", "Время подачи плановое", EquipmentWorkDto::getStartDatePlan);
	private static final GridColumn<EquipmentWorkDto> FINISH_PLAN = GridColumn.createDateColumn("finishDatePlan", "Время окончания работы плановое", EquipmentWorkDto::getFinishDatePlan);
	private static final GridColumn<EquipmentWorkDto> START_FACT = GridColumn.createDateColumn("startDateFact", "Время подачи фактическое", EquipmentWorkDto::getStartDateFact);
	private static final GridColumn<EquipmentWorkDto> FINISH_FACT = GridColumn.createDateColumn("finishDateFact", "Время окончания работы фактическое", EquipmentWorkDto::getFinishDateFact);
	private static final GridColumn<EquipmentWorkDto> COMMENTS = GridColumn.createColumn("comments", "Комментарии", EquipmentWorkDto::getComments);

	private static final GridColumn<ExcelData> TYPE_WORK = GridColumn.createColumn("type", "Вид работ", ExcelData::getTypeWork);
	private static final GridColumn<ExcelData> ADDRESS_ = GridColumn.createColumn("address", "Адрес работ", ExcelData::getAddress);
	private static final GridColumn<ExcelData> EQUIPMENT = GridColumn.createColumn("equipment", "Тип техники", ExcelData::getEquipment);
	private static final GridColumn<ExcelData> GOS_NUMBER = GridColumn.createColumn("number", "Гос. № а/м", ExcelData::getNumber);
	private static final GridColumn<ExcelData> SERVICE = GridColumn.createColumn("service", "Адрес подачи", ExcelData::getService);
	private static final GridColumn<ExcelData> COMMENT_ = GridColumn.createColumn("comment", "Комментарий", ExcelData::getComment);
	private static final GridColumn<ExcelData> TIME_ = GridColumn.createColumn("time", "Время подачи", ExcelData::getTime);
	private static final GridColumn<ExcelData> NOTE_ = GridColumn.createColumn("note", "Примечание", ExcelData::getNote);
	private static final GridColumn<ExcelData> RESPONSIBLE = GridColumn.createColumn("responsible", "Ответственный", ExcelData::getResponsible);

	private static CssLayout createFlag(VehicleDto vehicle) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		return layout;
	}

	public static List<GridColumn<VehicleDto>> getSpecialColumns() {
		return Arrays.asList(FLAG, WORK_ORDER_ADDRESS, WORK_ORDER, TYPE, NUMBER, WORK_GROUP, MANAGER, PHONE, ADDRESS,
				COMMENT, FILIAL, TERRITORY, REGION, START_TIME_PLAN, START_TIME_FACT, END_TIME_PLAN, END_TIME_FACT, NOTE);
	}

	public static List<GridColumn<VehicleDto>> getVanColumns() {
		return Arrays.asList(FLAG, FILIAL, TERRITORY, REGION, WORK_GROUP, MANAGER, PHONE,
				NUMBER_PLAN, NUMBER_FACT, ADDRESS, TIME, COMMENT);
	}

	public static List<GridColumn<EquipmentWorkDto>> getWorkVehicleColumns() {
		return Arrays.asList(TYPE_, NUMBER_, START_PLAN, START_FACT, FINISH_FACT, COMMENTS);
	}

	public static List<GridColumn<ExcelData>> getSpecialVehicleColumns() {
		return Arrays.asList(TYPE_WORK, ADDRESS_, EQUIPMENT, GOS_NUMBER, SERVICE, COMMENT_, TIME_, NOTE_, RESPONSIBLE);
	}
}
