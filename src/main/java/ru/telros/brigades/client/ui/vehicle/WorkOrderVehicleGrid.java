package ru.telros.brigades.client.ui.vehicle;

import ru.telros.brigades.client.dto.EquipmentWorkDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class WorkOrderVehicleGrid extends BrigadesGrid<EquipmentWorkDto> {
	public WorkOrderVehicleGrid(List<GridColumn<EquipmentWorkDto>> columns, List<EquipmentWorkDto> items) {
		super(columns, items);
	}

	@Override
	protected void onClick(EquipmentWorkDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(EquipmentWorkDto entity, Column column) {

	}
}
