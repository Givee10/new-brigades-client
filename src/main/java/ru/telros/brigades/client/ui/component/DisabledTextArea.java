package ru.telros.brigades.client.ui.component;

import ru.telros.brigades.client.ui.utils.StringUtil;

/**
 * ReadOnly (disabled) TextField
 */
@SuppressWarnings("serial")
public class DisabledTextArea extends CustomTextArea {

	public DisabledTextArea(final String caption) {
		super(caption);
		super.setEnabled(false);
	}

	@Override
	public void setValue(final String newValue) {
		try {
			super.setValue(StringUtil.returnString(newValue));
		} finally {
			super.setEnabled(false);
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(false);
	}
}
