package ru.telros.brigades.client.ui.setup;

import com.vaadin.data.Binder;
import com.vaadin.ui.TextField;
import ru.telros.brigades.client.dto.SlaUserActionDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class UserActionGrid extends BrigadesGrid<SlaUserActionDto> {
	public UserActionGrid(List<SlaUserActionDto> items) {
		super(SetupConstants.getUserActionColumns(), items);
		//removeFilterRow();
		setSelectionMode(SelectionMode.NONE);


		Binder<SlaUserActionDto> binder = this.getEditor().getBinder();
		TextField field = new TextField();

		this.getColumn("value").setEditorBinding(binder.bind(field,SlaUserActionDto::getValue,SlaUserActionDto::setValue));
		this.getEditor().setSaveCaption("Сохранить");
		this.getEditor().addSaveListener(e->{
			//Вписать логику
		});
		this.getEditor().setCancelCaption("Отменить");
//


		this.getEditor().setEnabled(true);
	}

	@Override
	protected void onClick(SlaUserActionDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(SlaUserActionDto entity, Column column) {

	}
}
