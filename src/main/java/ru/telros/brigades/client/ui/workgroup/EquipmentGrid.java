package ru.telros.brigades.client.ui.workgroup;

import ru.telros.brigades.client.dto.EquipmentBrigadePlanDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public class EquipmentGrid extends BrigadesGrid<EquipmentBrigadePlanDto> {
	public static final GridColumn<EquipmentBrigadePlanDto> ID_ = GridColumn.createColumn("id", "№", EquipmentBrigadePlanDto::getId);
	public static final GridColumn<EquipmentBrigadePlanDto> TYPE = GridColumn.createColumn("type", "Тип", EquipmentBrigadePlanDto::getType);
	public static final GridColumn<EquipmentBrigadePlanDto> MODEL = GridColumn.createColumn("model", "Марка", EquipmentBrigadePlanDto::getModel);
	public static final GridColumn<EquipmentBrigadePlanDto> NUMBER = GridColumn.createColumn("number", "Номер", EquipmentBrigadePlanDto::getNumber);

	public EquipmentGrid(List<EquipmentBrigadePlanDto> items) {
		super(Arrays.asList(TYPE, MODEL, NUMBER), items);
	}

	@Override
	protected void onDoubleClick(EquipmentBrigadePlanDto entity, Column column) {

	}

	@Override
	protected void onClick(EquipmentBrigadePlanDto entity, Column column) {

	}
}
