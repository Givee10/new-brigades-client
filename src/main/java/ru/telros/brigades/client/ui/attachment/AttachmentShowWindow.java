package ru.telros.brigades.client.ui.attachment;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.ui.utils.FileUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.io.ByteArrayInputStream;
import java.util.Map;

import static ru.telros.brigades.client.MessageManager.msg;

public class AttachmentShowWindow extends WindowWithButtons implements HasLogger {
	private AttachmentDto attachment;
	private Map<Class, Long> sourceMap;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	public AttachmentShowWindow(AttachmentDto attachment, Map<Class, Long> sourceMap) {
		super(attachment.getFileName(), 720, -1);
		this.attachment = attachment;
		this.sourceMap = sourceMap;

		initContents();
		initButtonBar();

		removeAllCloseShortcuts();
	}

	public static void open(final AttachmentDto attachment, final Map<Class, Long> sourceMap) {
		final Window window = new AttachmentShowWindow(attachment, sourceMap);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	private void initContents() {
		VerticalLayout verticalLayout = new VerticalLayout();
		AttachmentContentDto attachmentContent = sourceMap.containsKey(WorkDto.class) ?
				restTemplate.getWorkAttachmentContent(sourceMap.get(WorkDto.class), attachment.getId()) :
				restTemplate.getRequestAttachmentContent(sourceMap.get(RequestDto.class), attachment.getId());
		StreamResource.StreamSource streamSource = (StreamResource.StreamSource) () ->
				(attachmentContent == null) ? null : new ByteArrayInputStream(attachmentContent.getContent());

		String fileName = attachment.getFileName();
		StreamResource streamResource = new StreamResource(streamSource, fileName);
		if (streamSource.getStream() != null) {
			String mimeType = FileUtil.detectFileMIMEType(streamSource.getStream(), fileName);
			if (mimeType.startsWith(FileUtil.MIMEType.IMAGE.getMIMEType())) {
				Image image = new Image();
				image.setSource(streamResource);
				image.setSizeFull();
				verticalLayout.addComponentsAndExpand(image);
			} else {
				Label frame = new Label("Вложение недоступно для предпросмотра");
				verticalLayout.addComponentsAndExpand(frame);
			}
			addDownloadToButtonBar(msg("button.download"), streamResource);

			if (!AttachmentDto.Status.SENT.getCode().equalsIgnoreCase(attachment.getState()))
				addToButtonBar("Отправить в ГЛ", $ -> sendAttachmentToHL(
						attachmentContent.getContent(), mimeType, fileName, attachment.getDescription()));
		} else {
			Label label = new Label("Произошла ошибка при получении данных вложения");
			verticalLayout.addComponentsAndExpand(label);
		}
		verticalLayout.setSizeFull();
		verticalLayout.setMargin(false);
		verticalLayout.setSpacing(false);

		addToContent(verticalLayout, 1);
	}

	private void sendAttachmentToHL(byte[] content, String mime, String name, String desc) {
		String data = StringUtil.encodeFileToBase64(content);
		RequestDto requestDto;
		if (sourceMap.containsKey(WorkDto.class)) {
			requestDto = restTemplate.getWorkRequest(sourceMap.get(WorkDto.class));
		} else {
			requestDto = restTemplate.getRequest(sourceMap.get(RequestDto.class));
		}
		if (requestDto != null) {
			sendAttachmentToHL(requestDto, data, mime, name, desc);
		}
	}

	private void sendAttachmentToHL(RequestDto requestDto, String data, String mime, String name, String desc) {
		if (!StringUtil.isNull(requestDto.getExternalNumber())) {
			IncAttach incAttach = new IncAttach();
			incAttach.setpIncidentId(requestDto.getNumber());
			incAttach.setpIncidentNumber(requestDto.getExternalNumber());
			incAttach.setpFileName(name);
			incAttach.setpFileDescr(desc);
			incAttach.setpMimeType(mime);
			incAttach.setpFileData(data);
			restTemplate.sendAttToHotLine(incAttach);
			attachment.setState(AttachmentDto.Status.SENT.getCode());
			restTemplate.updateAttachment(attachment);
		}
	}

	private void initButtonBar() {
		addToButtonBar(msg("button.close"), s -> close());
	}
}
