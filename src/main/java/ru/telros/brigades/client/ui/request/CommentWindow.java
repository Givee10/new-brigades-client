package ru.telros.brigades.client.ui.request;

import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.ui.component.CustomComboBox;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ru.telros.brigades.client.MessageManager.msg;

public class CommentWindow extends WindowWithButtons implements HasLogger {
	private Map<Class, Long> sourceMap;
	private CommentDto comment;
	private Binder<CommentDto> binder = new Binder<>();
	private List<WorkItemDto> stageList = new ArrayList<>();

	//private CustomTextField stage = new CustomTextField("Этап");
	private CustomComboBox<WorkItemDto> stage = new CustomComboBox<>("Этап");
	private CustomTextField content = new CustomTextField("Содержание");
	private CheckBox note = new CheckBox("Передать в ГЛ");
	private CustomTextField source = new CustomTextField("От кого");
	private DisabledTextField registeredBy = new DisabledTextField("Зарегистрировал");

	public CommentWindow(Map<Class, Long> sourceMap, CommentDto comment) {
		super("Комментарии", 800, -1);
		this.sourceMap = sourceMap;
		this.comment = comment;

		buildFields();
		bindFields();

		removeAllCloseShortcuts();
	}

	private void buildFields() {
		//List<WorkDto> requestWorks = BrigadesUI.getRestTemplate().getRequestWorks(sourceMap.get(RequestDto.class));
		//requestWorks.forEach(workDto -> stageList.addAll(BrigadesUI.getRestTemplate().getWorkItems(workDto.getId())));
		//stage.setItems(stageList);
		note.setEnabled(sourceMap.containsKey(RequestDto.class));

		addToContent(CompsUtil.getVerticalWrapperNoMargin(stage, content, note, source, registeredBy), 1);
		addToButtonBar(msg("button.save"), $ -> save());
		addToButtonBar(msg("button.cancel"), $ -> close());
	}

	private void save() {
		initRestValues();
		if (binder.validate().isOk()) {
			getLogger().debug("Before save: " + StringUtil.writeValueAsString(binder.getBean()));
			List<CommentDto> savedComments = (sourceMap.containsKey(WorkDto.class)) ?
					BrigadesUI.getRestTemplate().saveWorkComment(sourceMap.get(WorkDto.class), binder.getBean()) :
					BrigadesUI.getRestTemplate().saveRequestComment(sourceMap.get(RequestDto.class), binder.getBean());
			if (savedComments != null && !savedComments.isEmpty()) {
				//savedComments.forEach(rc -> getLogger().debug(rc.debugInfo()));
				CommentDto savedComment = savedComments.get(0);
				getLogger().debug("After save: " + StringUtil.writeValueAsString(savedComment));
				if (sourceMap.containsKey(WorkDto.class))
					WorkOrderLayout.refresh();
				else {
					IncNote incNote = createNote(binder.getBean());
					if (/*RequestConstants.isTestRequest(incNote.getpIncidentNumber()) && */note.getValue())
						BrigadesUI.getRestTemplate().sendNoteToHotLine(incNote);
					RequestLayout.refresh();
				}
				close();
				return;
			}
		}
		Notification.show("Ошибка при сохранении данных", Notification.Type.ERROR_MESSAGE);
	}

	private void initRestValues() {
		ZonedDateTime now = ZonedDateTime.now();
		if (comment.getCreateDate() == null) comment.setCreateDate(now);
		comment.setChangeDate(now);
	}

	private void bindFields() {
		binder.bind(stage, getStage(), setStage());
		binder.bind(content, CommentDto::getContent, CommentDto::setContent);
		binder.bind(source, CommentDto::getSource, CommentDto::setSource);
		binder.bind(registeredBy, CommentDto::getRegisteredBy, CommentDto::setRegisteredBy);
		binder.bind(note, getNote(), setNote());

		binder.setBean(comment);
	}

	private ValueProvider<CommentDto, WorkItemDto> getStage() {
		return (ValueProvider<CommentDto, WorkItemDto>) request -> request.getStage() == null ? null :
				stageList.stream().filter(w -> w.getDescription().equals(request.getStage())).findFirst().orElse(null);
	}

	private Setter<CommentDto, WorkItemDto> setStage() {
		return (Setter<CommentDto, WorkItemDto>) (request, stage) ->
				request.setStage(stage != null ? stage.getDescription() : null);
	}

	private ValueProvider<CommentDto, Boolean> getNote() {
		return (ValueProvider<CommentDto, Boolean>) request -> request.getNote() != null;
	}

	private Setter<CommentDto, Boolean> setNote() {
		return (Setter<CommentDto, Boolean>) (request, note) -> request.setNote(note ? "+" : null);
	}

	public static void open(Map<Class, Long> sourceMap, CommentDto comment) {
		final Window window = new CommentWindow(sourceMap, comment);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	private IncNote createNote(CommentDto comment) {
		RequestDto request = BrigadesUI.getRestTemplate().getRequest(sourceMap.get(RequestDto.class));
		IncNote incNote = new IncNote();
		incNote.setpIncidentId(request.getNumber());
		incNote.setpIncidentNumber(request.getExternalNumber());
		incNote.setpNotesText(comment.getContent());
		incNote.setpUpdatedByFio(BrigadesUI.getCurrentUser().getFullName());
		return incNote;
	}
}
