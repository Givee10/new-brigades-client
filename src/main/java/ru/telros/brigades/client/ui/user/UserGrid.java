package ru.telros.brigades.client.ui.user;

import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.ArrayList;
import java.util.List;

public class UserGrid extends BrigadesGrid<UserDto> {
	private static final List<GridColumn<UserDto>> columns = new ArrayList<GridColumn<UserDto>>() {{
		add(GridColumn.createColumn("id", "№", UserDto::getId));
		add(GridColumn.createColumn("login", "Логин", UserDto::getLogin));
		add(GridColumn.createColumn("email", "E-mail", UserDto::getEmail));
		add(GridColumn.createColumn("person", "Пользователь", GridColumn::nullProvider));
		add(GridColumn.createColumn("group", "Группа", GridColumn::nullProvider));
	}};

	public UserGrid(List<UserDto> items) {
		super(columns, items);
	}

	@Override
	protected void onClick(UserDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(UserDto entity, Column column) {

	}
}
