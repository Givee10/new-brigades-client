package ru.telros.brigades.client.ui.setup;

import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public abstract class SetupConstants {
	private static final GridColumn<SlaControlAccuracyDto> CONTROL_ACCURACY_ID = GridColumn.createColumn("id", "№", SlaControlAccuracyDto::getId);
	private static final GridColumn<SlaControlAccuracyDto> CONTROL_ACCURACY_NAME = GridColumn.createColumn("name", "Название", SlaControlAccuracyDto::getName);
	private static final GridColumn<SlaControlAccuracyDto> CONTROL_ACCURACY_VALUE = GridColumn.createColumn("value", "Значение, мин.", SlaControlAccuracyDto::getValue);

	private static final GridColumn<SlaReactionTimeDto> REACTION_TIME_ID = GridColumn.createColumn("id", "№", SlaReactionTimeDto::getId);
	private static final GridColumn<SlaReactionTimeDto> REACTION_TIME_CODE = GridColumn.createColumn("code", "Код ГЛ", SlaReactionTimeDto::getCodeHl);
	private static final GridColumn<SlaReactionTimeDto> REACTION_TIME = GridColumn.createColumn("time", "Значение, мин.", SlaReactionTimeDto::getTimeWorkByRequestSou);

	private static final GridColumn<SlaUserActionDto> USER_ACTION_ID = GridColumn.createColumn("id", "№", SlaUserActionDto::getId);
	private static final GridColumn<SlaUserActionDto> USER_ACTION_NAME = GridColumn.createColumn("name", "Название", SlaUserActionDto::getName);
	private static final GridColumn<SlaUserActionDto> USER_ACTION_VALUE = GridColumn.createColumn("value", "Значение, ч.", SlaUserActionDto::getValue);

	private static final GridColumn<SlaWorkCodeDto> WORK_CODE_ID = GridColumn.createColumn("id", "№", SlaWorkCodeDto::getId);
	private static final GridColumn<SlaWorkCodeDto> WORK_CODE = GridColumn.createColumn("code", "Код ГЛ", SlaWorkCodeDto::getIdCodeHl);
	private static final GridColumn<SlaWorkCodeDto> WORK_CODE_NAME = GridColumn.createColumn("work", "Название", SlaWorkCodeDto::getNameWork);

	private static final GridColumn<BrigadeWorkPriorityDto> WORK_TYPE_ID = GridColumn.createColumn("id", "№ п/п", BrigadeWorkPriorityDto::getId);
	private static final GridColumn<BrigadeWorkPriorityDto> WORK_TYPE_ID_HL = GridColumn.createColumn("idCodeHl", "Вид работ в ТОиР", BrigadeWorkPriorityDto::getIdCodeHl);
	private static final GridColumn<BrigadeWorkPriorityDto> YOR_BRIGADE = GridColumn.createColumn("YOR", "Бр. УОР", BrigadeWorkPriorityDto::getYOR);
	private static final GridColumn<BrigadeWorkPriorityDto> LY_BRIGADE = GridColumn.createColumn("LY", "Бр. ЛУ", BrigadeWorkPriorityDto::getLY);
	private static final GridColumn<BrigadeWorkPriorityDto> BTO_BRIGADE = GridColumn.createColumn("BTO", "БТО", BrigadeWorkPriorityDto::getBTO);
	private static final GridColumn<BrigadeWorkPriorityDto> FLYING_BRIGADE = GridColumn.createColumn("flying", "\"Летучая\" бригада", BrigadeWorkPriorityDto::getFlying);
	private static final GridColumn<BrigadeWorkPriorityDto> LINEAR_BRIGADE = GridColumn.createColumn("linear", "Линейная дополнит.", BrigadeWorkPriorityDto::getLinear);

	public static List<GridColumn<BrigadeWorkPriorityDto>> getBrigadeWorkPriorityColumns() {
		return Arrays.asList(WORK_TYPE_ID, WORK_TYPE_ID_HL, YOR_BRIGADE, LY_BRIGADE, BTO_BRIGADE, FLYING_BRIGADE, LINEAR_BRIGADE);
	}

	public static List<GridColumn<SlaControlAccuracyDto>> getControlAccuracyColumns() {
		return Arrays.asList(CONTROL_ACCURACY_ID, CONTROL_ACCURACY_NAME, CONTROL_ACCURACY_VALUE);
	}

	public static List<GridColumn<SlaReactionTimeDto>> getReactionTimeColumns() {
		return Arrays.asList(REACTION_TIME_ID, REACTION_TIME_CODE, REACTION_TIME);
	}

	public static List<GridColumn<SlaUserActionDto>> getUserActionColumns() {
		return Arrays.asList(USER_ACTION_ID, USER_ACTION_NAME, USER_ACTION_VALUE);
	}

	public static List<GridColumn<SlaWorkCodeDto>> getWorkCodeColumns() {
		return Arrays.asList(WORK_CODE_ID, WORK_CODE, WORK_CODE_NAME);
	}
}
