package ru.telros.brigades.client.ui.workplan;

import com.vaadin.ui.CustomComponent;
import org.vaadin.addon.calendar.Calendar;
import org.vaadin.addon.calendar.handler.BasicDateClickHandler;
import org.vaadin.addon.calendar.item.BasicItem;
import org.vaadin.addon.calendar.item.BasicItemProvider;
import org.vaadin.addon.calendar.ui.CalendarComponentEvents;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.ui.workorder.WorkOrderConstants;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Locale;

/**
 * Component for show/edit of working plan
 */
@SuppressWarnings("serial")
public class WorkPlan extends CustomComponent {
	private static final int HEIGHT_PIXELS = 800;

	private final Calendar<WorkOrderEvent> calendar = new Calendar<>();
	private WorkOrderDataProvider workOrderDataProvider = new WorkOrderDataProvider();
	private Collection<WorkDto> workOrders;

	public WorkPlan(final String caption, final Boolean isEdited) {
		setCaption(caption);
		setWidth(100, Unit.PERCENTAGE);
		setHeight(HEIGHT_PIXELS, Unit.PIXELS);
		setLocale(Locale.forLanguageTag("RU"));

		initCalendar(isEdited);
		setCompositionRoot(calendar);
	}

	private void initCalendar(Boolean isEdited) {
		calendar.setSizeFull();
		calendar.setDataProvider(workOrderDataProvider);
		calendar.setZoneId(ZoneId.systemDefault());

		ZonedDateTime startDate = ZonedDateTime.now().minusDays(1);
		ZonedDateTime endDate = ZonedDateTime.now().plusDays(1);
		showInterval(startDate, endDate);

		calendar.setHandler((BasicDateClickHandler) null); // disable click on date
		calendar.setHandler((CalendarComponentEvents.EventResizeHandler) null); // disable resize
		if (!isEdited) calendar.setHandler((CalendarComponentEvents.ItemMoveHandler) null); // disable move
		calendar.setHandler((CalendarComponentEvents.ForwardHandler) event -> {
			ZonedDateTime start = calendar.getStartDate().plusDays(1);
			ZonedDateTime end = calendar.getEndDate().plusDays(1);
			showInterval(start, end);
		});
		calendar.setHandler((CalendarComponentEvents.BackwardHandler) event -> {
			ZonedDateTime start = calendar.getStartDate().minusDays(1);
			ZonedDateTime end = calendar.getEndDate().minusDays(1);
			showInterval(start, end);
		});

		calendar.setCaptionAsHtml(true);
	}

	private void showInterval(ZonedDateTime startDate, ZonedDateTime endDate) {
		calendar.setStartDate(startDate);
		calendar.setEndDate(endDate);
	}

	public Collection<WorkDto> getWorkOrders() {
		return workOrders;
	}

	public void setWorkOrders(final Collection<WorkDto> workOrders) {
		this.workOrders = workOrders;
		for (WorkDto workOrder : workOrders) {
			WorkOrderEvent event = new WorkOrderEvent(workOrder);
			workOrderDataProvider.addItem(event);
		}
	}

	private class WorkOrderEvent extends BasicItem {
		private static final String CAPTION_FORMAT = "%tR\n%s\n%s";

		private final WorkDto workOrder;

		WorkOrderEvent(final WorkDto workOrder) {
			this.workOrder = workOrder;
			setCaptionAsHtml(true);
		}

		@Override
		public ZonedDateTime getStart() {
			return workOrder.getStartDatePlan().withZoneSameInstant(ZoneId.systemDefault());
		}

		@Override
		public void setStart(final ZonedDateTime start) {
			workOrder.setStartDatePlan(start);
			fireEventChange();
		}

		@Override
		public ZonedDateTime getEnd() {
			ZonedDateTime finishDatePlan = workOrder.getFinishDatePlan();
			return finishDatePlan == null ? getStart() : finishDatePlan.withZoneSameInstant(ZoneId.systemDefault());
		}

		@Override
		public void setEnd(final ZonedDateTime end) {
			workOrder.setFinishDatePlan(end);
			fireEventChange();
		}

		@Override
		public String getCaption() {
			return String.format(CAPTION_FORMAT, getStart(), workOrder.getAddress(), workOrder.getDescription());
		}

		@Override
		public String getDescription() {
			return workOrder.getDescription();
		}

		@Override
		public String getStyleName() {
			return WorkOrderConstants.getEventStyle(workOrder);
		}

		@Override
		public boolean isAllDay() {
			return false;
		}
	}

	private final class WorkOrderDataProvider extends BasicItemProvider<WorkOrderEvent> {
		void removeAllEvents() {
			this.itemList.clear();
			fireItemSetChanged();
		}
	}
}
