package ru.telros.brigades.client.ui.maplayout;

/**
 * Simple notification event interface
 */
public interface MapNotifyEventListener {
	void notify(Object object);
}
