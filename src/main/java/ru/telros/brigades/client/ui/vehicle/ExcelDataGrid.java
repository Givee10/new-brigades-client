package ru.telros.brigades.client.ui.vehicle;

import ru.telros.brigades.client.dto.ExcelData;
import ru.telros.brigades.client.ui.grid.BrigadesFilterGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class ExcelDataGrid extends BrigadesFilterGrid<ExcelData> {
	public ExcelDataGrid(List<GridColumn<ExcelData>> gridColumns, List<ExcelData> items) {
		super(gridColumns, items);
		removeFilterRow();
	}
}
