package ru.telros.brigades.client.ui.setup;

import com.vaadin.data.Binder;
import ru.telros.brigades.client.dto.BrigadeWorkPriorityDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class BrigadeWorkPriorityGrid extends BrigadesGrid<BrigadeWorkPriorityDto>  {


    public BrigadeWorkPriorityGrid(List<BrigadeWorkPriorityDto> items) {
        super(SetupConstants.getBrigadeWorkPriorityColumns(), items);
        //removeFilterRow();
        setSelectionMode(SelectionMode.NONE);


        Binder<BrigadeWorkPriorityDto> binder = this.getEditor().getBinder();

        this.getEditor().setSaveCaption("Сохранить");
        this.getEditor().addSaveListener(e->{
            //Вписать логику
        });
        this.getEditor().setCancelCaption("Отменить");
//


        this.getEditor().setEnabled(true);
        this.getColumns().stream().forEach(e->e.setExpandRatio(1));
    }




    @Override
    protected void onDoubleClick(BrigadeWorkPriorityDto entity, Column column) {

    }

    @Override
    protected void onClick(BrigadeWorkPriorityDto entity, Column column) {

    }

}
