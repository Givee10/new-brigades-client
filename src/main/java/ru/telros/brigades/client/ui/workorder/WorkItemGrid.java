package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.dto.WorkItemDto;
import ru.telros.brigades.client.ui.grid.BrigadesFilterGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class WorkItemGrid extends BrigadesFilterGrid<WorkItemDto> {
	public WorkItemGrid(List<GridColumn<WorkItemDto>> gridColumns, List<WorkItemDto> items) {
		super(gridColumns, items);
	}
}
