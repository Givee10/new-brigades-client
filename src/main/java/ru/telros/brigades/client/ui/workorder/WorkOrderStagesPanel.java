//package ru.telros.brigades.client.ui.workorder;
//
//import com.vaadin.ui.*;
//import ru.telros.brigades.client.BrigadesTheme;
//import ru.telros.brigades.client.BrigadesUI;
//import ru.telros.brigades.client.HasLogger;
//import ru.telros.brigades.client.dto.TypeWorkDto;
//import ru.telros.brigades.client.dto.TypeWorkItemDto;
//import ru.telros.brigades.client.dto.WorkDto;
//import ru.telros.brigades.client.dto.WorkItemDto;
//import ru.telros.brigades.client.ui.component.TimeField;
//import ru.telros.brigades.client.ui.window.YesNoWindow;
//
//import java.time.ZonedDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.List;
//
//import static ru.telros.brigades.client.MessageManager.msg;
//
//@SuppressWarnings("serial")
//class WorkOrderStagesPanel extends HorizontalLayout implements HasLogger {
//	private WorkDto workOrder;
//	private WorkOrderWindow workOrderWindow;
//	private WorkItemGrid stagesTable;
//	private TimeField norm;
//
//	@SuppressWarnings("unchecked")
//	WorkOrderStagesPanel(WorkDto workOrder, WorkOrderWindow workOrderWindow) {
//		this.workOrder = workOrder;
//		this.workOrderWindow = workOrderWindow;
//		List<WorkItemDto> workItems = new ArrayList<>();
//		if (workOrder.getId() != null)
//			workItems.addAll(BrigadesUI.getRestTemplate().getWorkItems(this.workOrder.getId()));
//		stagesTable = new WorkItemGrid(WorkItemColumn.getStagesColumns(), workItems);
//		stagesTable.getColumns().forEach(column -> column.setSortable(false));
//		stagesTable.setHeight(250, Unit.PIXELS);
//		stagesTable.addSelectionListener($ -> changeNorm());
//		stagesTable.removeFilterRow();
//		build();
//	}
//
//	List<WorkItemDto> getWorkOrderStages() {
//		return stagesTable.getItems();
//	}
//
//	void setWorkOrderStages(List<WorkItemDto> stages) {
//		stagesTable.setItems(stages);
//	}
//
//	ZonedDateTime updateStages(TypeWorkDto typeWorkDto, ZonedDateTime startDatePlan) {
//		// обновление этапов по шаблону
//		stagesTable.deselectAll();
//		changeNorm();
//		if (typeWorkDto != null) {
//			List<TypeWorkItemDto> workItems = BrigadesUI.getRestTemplate().getTypeWorkItems(typeWorkDto.getId());
//			List<WorkItemDto> stages = WorkOrderConstants.createWorkItemStages(workItems);
//			return updateStagesTime(stages, startDatePlan);
//		}
//		return null;
//	}
//
//	ZonedDateTime updateStagesTime(ZonedDateTime startDate) {
//		return updateStagesTime(getWorkOrderStages(), startDate);
//	}
//
//	boolean hasAssignedStages() {
//		return stagesTable.getItems().size() > 0;
//	}
//
//	private void build() {
//		setMargin(false);
//		setSpacing(false);
//		setSizeFull();
//		addComponent(stagesTable);
//		setExpandRatio(stagesTable, 1);
//		addComponent(buildControls());
//	}
//
//	private Component buildControls() {
//		VerticalLayout controls = new VerticalLayout();
//		controls.setWidthUndefined();
//		controls.setSpacing(true);
//
//		Button moveStageUpButton = new Button();
//		moveStageUpButton.setIcon(BrigadesTheme.ICON_MOVE_UP);
//		moveStageUpButton.setDescription(msg("button.up"));
//		moveStageUpButton.addClickListener($ -> moveStage(true));
//		moveStageUpButton.setSizeFull();
//		controls.addComponent(moveStageUpButton);
//
//		Button moveStageDownButton = new Button();
//		moveStageDownButton.setIcon(BrigadesTheme.ICON_MOVE_DOWN);
//		moveStageDownButton.setDescription(msg("button.down"));
//		moveStageDownButton.addClickListener($ -> moveStage(false));
//		moveStageDownButton.setSizeFull();
//		controls.addComponent(moveStageDownButton);
//
//		Button deleteStageButton = new Button();
//		deleteStageButton.setIcon(BrigadesTheme.ICON_DELETE);
//		deleteStageButton.setDescription(msg("button.delete"));
//		deleteStageButton.addClickListener($ -> deleteStage());
//		deleteStageButton.setSizeFull();
//		controls.addComponent(deleteStageButton);
//
//		norm = new TimeField(msg("workorderwindow.stages.edit.stageTimeCaption"));
//		controls.addComponent(norm);
//
//		Button editStageButton = new Button();
//		editStageButton.setIcon(BrigadesTheme.ICON_OK);
//		editStageButton.setDescription(msg("button.edit"));
//		editStageButton.addClickListener($ -> editItem());
//		editStageButton.setSizeFull();
//
//		controls.addComponent(editStageButton);
//		controls.setVisible(workOrder.getIdBrigade() == null);
//		return controls;
//	}
//
//	private void changeNorm() {
//		WorkItemDto selectedRow = stagesTable.getSelectedRow();
//		if (selectedRow != null) {
//			ZonedDateTime startDatePlan = selectedRow.getStartDatePlan();
//			ZonedDateTime finishDatePlan = selectedRow.getFinishDatePlan();
//			if (startDatePlan != null && finishDatePlan != null)
//				norm.setValue(startDatePlan, finishDatePlan);
//		} else {
//			norm.clear();
//		}
//	}
//
//	private void moveStage(boolean up) {
//		if (stagesTable.getSelectedRow() != null) {
//			WorkItemDto row = stagesTable.getSelectedRow();
//			//if (row.getRequired()) {
//			//	Notification.show(msg("workorderwindow.stages.shift.stageShiftDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
//			//} else {
//			int intItem = stagesTable.getItems().indexOf(row);
//			int changeItem = up ? intItem - 1 : intItem + 1;
//			if (changeItem > 0 && changeItem != stagesTable.getItems().size()) {
//				//WorkItemDto changeRow = stagesTable.getAllItems().get(changeItem);
//				//if (changeRow.getRequired()) {
//				//	Notification.show(msg("workorderwindow.stages.shift.stageShiftDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
//				//} else {
//				List<WorkItemDto> stages = new ArrayList<>(getWorkOrderStages());
//				Collections.swap(stages, intItem, changeItem);
//				updateStagesTime(stages, workOrderWindow.returnStartDatePlan());
//				stagesTable.select(row);
//				//}
//			} else {
//				Notification.show(msg("workorderwindow.stages.shift.stageShiftDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
//			}
//			//}
//		} else {
//			Notification.show(msg("workorderwindow.stages.stageNotSelectedMessage"), Notification.Type.WARNING_MESSAGE);
//		}
//	}
//
//	private void editItem() {
//		if (stagesTable.getSelectedRow() != null) {
//			try {
//				WorkItemDto row = stagesTable.getSelectedRow();
//				//getLogger().debug("Value: {}, Converted: {}", norm.getValue(), norm.convertValue());
//				ZonedDateTime endDate = row.getStartDatePlan().plusMinutes(norm.convertValue());
//				row.setFinishDatePlan(endDate);
//				ZonedDateTime newFinishDate = updateStagesTime(getWorkOrderStages(), workOrderWindow.returnStartDatePlan());
//				//workOrderWindow.updateFinishDatePlan(newFinishDate);
//				changeNorm();
//			} catch (Throwable throwable) {
//				Notification.show(msg("workorderwindow.stages.edit.stageWrongTimeFormatMessage"), Notification.Type.ERROR_MESSAGE);
//			}
//		} else {
//			Notification.show(msg("workorderwindow.stages.stageNotSelectedMessage"), Notification.Type.WARNING_MESSAGE);
//		}
//	}
//
//	private void deleteStage() {
//		if (stagesTable.getSelectedRow() != null) {
//			WorkItemDto row = stagesTable.getSelectedRow();
//			//if (row.getAttributes().contains("REQUIRED")) {
//			//	Notification.show(msg("workorderwindow.stages.delete.stageDeletionDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
//			//} else {
//				String prompt = "Вы действительно хотите удалить этап " + row.getDescription() + "?";
//				YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
//				confirmationDialog.addCloseListener((Window.CloseListener) e -> {
//					if (((YesNoWindow)e.getWindow()).isModalResult()) {
//						int intItem = stagesTable.getItems().indexOf(row);
//						int nextItem = intItem + 1;
//						WorkItemDto nextRow = stagesTable.getItems().get(nextItem);
//						stagesTable.getItems().remove(row);
//						ZonedDateTime newFinishDate = updateStagesTime(getWorkOrderStages(), workOrderWindow.returnStartDatePlan(), null);
//						//workOrderWindow.updateFinishDatePlan(newFinishDate);
//						stagesTable.select(nextRow);
//						Notification.show(msg("workorderwindow.stages.delete.stageDeletedMessage",
//						new String[] { row.getDescription() }), Notification.Type.WARNING_MESSAGE);
//					}
//				});
//			//}
//		} else {
//			Notification.show(msg("workorderwindow.stages.stageNotSelectedMessage"), Notification.Type.WARNING_MESSAGE);
//		}
//	}
//
//	/**
//	 * Обновление времени начала/окончания этапов, начиная со времени startDate и этапа fromStage
//	 *
//	 * @param stages    Перечень этапов
//	 * @param startDate Начальная дата/время
//	 * @param fromStage Начальный этап (null - для всех)
//	 * @return Конечная дата
//	 */
//	private ZonedDateTime updateStagesTime(final Collection<WorkItemDto> stages, ZonedDateTime startDate, WorkItemDto fromStage) {
//		if (stages == null || stages.size() == 0)
//			return null;
//
//		List<WorkItemDto> resultStages = new ArrayList<>(stages);
//		boolean isStarted = (fromStage == null);
//		//long startTime = DatesUtil.localDateTimeToLong(startDate);
//		//long endTime = 0;
//		Long deltaMinutes;
//		ZonedDateTime endDate = null;
//
//		for (WorkItemDto stage : resultStages) {
//			isStarted = isStarted || (stage.equals(fromStage));
//			if (isStarted) {
//				// по умолчанию длительность этапа - 0.5 часа
//				deltaMinutes = 0L;
//				// определяем длину этапа
//				if (stage.getStartDatePlan() != null && stage.getFinishDatePlan() != null) {
//					// если время у этапа присутствует, значит берем разницу во времени из его данных
//					// т.к. время этапа могло быть отредактировано вручную
//					//Long startFact = DatesUtil.localDateTimeToDate(stage.getStartDatePlan()).getTime();
//					//Long finishFact = DatesUtil.localDateTimeToDate(stage.getFinishDatePlan()).getTime();
//					deltaMinutes = ChronoUnit.MINUTES.between(stage.getStartDatePlan(), stage.getFinishDatePlan());
//				} else if (stage.getDuration() != null) {
//					// если какое либо время null, то взять по нормативу
//					deltaMinutes = new Double(stage.getDuration() * 60).longValue();
//				}
//				// получаем конечное время
//				endDate = startDate.plusMinutes(deltaMinutes);
//				//endTime = (long) ((double) startTime + deltaMinutes);
//				// обновляем время и статус у этапа
//				stage.setStartDatePlan(startDate);
//				stage.setFinishDatePlan(endDate);
//				stage.setStartDate(null);
//				stage.setFinishDate(null);
//				//stage.setStatus("Новая");
//				// сохраняем начальное время
//				startDate = endDate;
//			}
//		}
//		if (endDate == null) return null;
//		// Обновление этапов работы
//		setWorkOrderStages(resultStages);
//		return endDate;
//	}
//
//	/**
//	 * Обновление времен начала/окончания этапов, начиная со времени startDate
//	 *
//	 * @param stages    Перечень этапов
//	 * @param startDate Начальная дата/время
//	 * @return
//	 */
//	private ZonedDateTime updateStagesTime(final Collection<WorkItemDto> stages, ZonedDateTime startDate) {
//		if (stages == null || stages.size() == 0) {
//			return null;
//		}
//		return updateStagesTime(stages, startDate, stages.iterator().next());
//	}
//
//}
