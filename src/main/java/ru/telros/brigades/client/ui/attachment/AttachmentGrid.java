package ru.telros.brigades.client.ui.attachment;

import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class AttachmentGrid extends BrigadesGrid<AttachmentDto> {
	public AttachmentGrid(List<GridColumn<AttachmentDto>> gridColumns, List<AttachmentDto> items) {
		super(gridColumns, items);
	}

	@Override
	protected void onClick(AttachmentDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(AttachmentDto entity, Column column) {

	}
}
