package ru.telros.brigades.client.ui.setup;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.AttachmentTypeDto;
import ru.telros.brigades.client.dto.ProblemAttachmentDto;
import ru.telros.brigades.client.dto.ProblemDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReactionTimeGrid extends Grid<ProblemDto> {
	public ReactionTimeGrid() {

		List<ProblemDto> list = BrigadesUI.getRestTemplate().getProblems();
		Map<String, String> HLCodeToTime = BrigadesUI.getRestTemplate().getSlaReactionTime().
				stream().collect(Collectors.toMap(slaReactionTimeDto -> slaReactionTimeDto.getCodeHl(),
				slaReactionTimeDto -> slaReactionTimeDto.getTimeWorkByRequestSou()==null ? "":slaReactionTimeDto.getTimeWorkByRequestSou()));


		addColumn(e->e.getCode()).setCaption("Код ГЛ").setId("HLCode");
		addColumn(e->e.getName()).setCaption("Наименование").setId("Name");
		addColumn(e->HLCodeToTime.get(e.getCode())).setCaption("Значение, мин").setId("Value");
		initDocColumns();

		setColumnOrder("HLCode", "Name", "Value", "5", "1", "9", "14", "7", "10", "8", "18", "19", "15", "13", "12", "11", "6", "2", "3", "4", "16", "17");
		//removeFilterRow();
		setSelectionMode(SelectionMode.NONE);
		setItems(list);
		setWidth("100%");
		setHeight("100%");
		setSizeFull();
		/*Binder<SlaWorkCodeDto> binder = this.getEditor().getBinder();
		TextField field = new TextField();
		this.getColumn("time").setEditorBinding(binder.bind(field,SlaReactionTimeDto::getTimeWorkByRequestSou,SlaReactionTimeDto::setTimeWorkByRequestSou));
		this.getEditor().setSaveCaption("Сохранить");
		this.getEditor().addSaveListener(e->{
			//Вписать логику
		});
		this.getEditor().setCancelCaption("Отменить");
//


		this.getEditor().setEnabled(true);*/
	}
	private void initDocColumns() {
		//Проблема - Map необходимых документов

		Map<Long, Map<String,ProblemAttachmentDto>> problemsAttachments = BrigadesUI.getRestTemplate().getProblems().stream().
				collect(Collectors.toMap(problemDto -> problemDto.getId(), problemDto -> new HashMap<>()));
		BrigadesUI.getRestTemplate().getProblemsAttachments().stream().forEach(problemAttachmentDto -> {
			problemsAttachments.get(problemAttachmentDto.getProblemCode()).put(problemAttachmentDto.getAttachmentType(),problemAttachmentDto);
		});
		/*System.out.println("Проблема - Список необходимых документов");
		problemsAttachments.entrySet().stream().forEach(longMapEntry -> {
			System.out.println("Problem id: "+longMapEntry.getKey()+"; doc list: "+
					longMapEntry.getValue().entrySet().stream().map(stringProblemAttachmentDtoEntry ->
							stringProblemAttachmentDtoEntry.getValue().getAttachmentType()+ ":"+stringProblemAttachmentDtoEntry.getValue().getRequire()).collect(Collectors.joining()));
		});*/
		List<AttachmentTypeDto> attachmentTypeDtos = BrigadesUI.getRestTemplate().getAttachmentTypes();
		/*List<AttachmentTypeDto> columnsOrder = new ArrayList<>();
		columnsOrder.addAll(attachmentTypeDtos.stream().filter(attachmentTypeDto -> attachmentTypeDto.getGroup().equals("документ")).collect(Collectors.toList()));
		attachmentTypeDtos*/
		attachmentTypeDtos.
				forEach(e -> this.addComponentColumn(problemDto -> {
					ComboBox<String> comboBox = new ComboBox();
					comboBox.setItems("Обязательно", "При необходимости", "-");
					Long curProblemId = Long.parseLong(problemDto.getCode());
					String curAttachment =e.getName();
					if (problemsAttachments.get(curProblemId)!=null &&
							problemsAttachments.get(curProblemId).get(curAttachment)!=null){
						String value = problemsAttachments.get(curProblemId).get(curAttachment).getRequire();
						comboBox.setSelectedItem(value);
					}else{
						comboBox.setValue("-");
					}
					comboBox.setTextInputAllowed(false);
					comboBox.setEmptySelectionAllowed(false);
					comboBox.addValueChangeListener(valueChangeEvent -> {
						BrigadesUI.getRestTemplate().sendProblemAttachments(curProblemId,curAttachment,valueChangeEvent.getValue());
					});
					comboBox.setWidth("100%");

					return comboBox;
				}).setCaption(e.getName()).setId(e.getId()+""));

	}


}
