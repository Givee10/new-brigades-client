package ru.telros.brigades.client.ui.request;

import com.google.common.eventbus.Subscribe;
import com.lowagie.text.DocumentException;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Responsive;
import com.vaadin.server.SerializablePredicate;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import org.vaadin.inputmask.InputMask;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.event.UpdateViewEvent;
import ru.telros.brigades.client.event.request.UpdateRequestsEvent;
import ru.telros.brigades.client.reportbuilders.PDFRequestBuilder;
import ru.telros.brigades.client.ui.attachment.AttachmentPanel;
import ru.telros.brigades.client.ui.component.*;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.FileUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.window.GeocodeWindow;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.window.YesNoWindow;
import ru.telros.brigades.client.ui.workorder.WorkOrderConstants;
import ru.telros.brigades.client.ui.workorder.WorkOrderGrid;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.telros.brigades.client.MessageManager.msg;
import static ru.telros.brigades.client.ui.grid.GridColumn.dateGetter;
import static ru.telros.brigades.client.ui.grid.GridColumn.dateSetter;

public class RequestWindow extends WindowWithButtons implements HasLogger {
	private static final int WIDTH_PIXELS = 1200;
	private static final int HEIGHT_PIXELS = -1;
	private List<RoleDto> curUserRoles;
	private Boolean isNew;
	private RequestDto request;
	private Binder<RequestDto> binder = new Binder<>();
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	private DisabledTextField requestNum = new DisabledTextField(msg("requestwindow.requestnum"));
	private CustomTextArea description = new CustomTextArea(msg("requestwindow.description"));
	private CustomTextField operator = new CustomTextField(msg("requestwindow.operator"));
	private CustomTextField company = new CustomTextField(msg("requestwindow.company"));
	private CustomTextField phone = new CustomTextField(msg("requestwindow.phone"));
	private CustomTextField info = new CustomTextField("Доп. инфо");
	private CustomDateTimeField registrationDate = new CustomDateTimeField("Зарегистрирована");

	private CustomDateTimeField closeDate = new CustomDateTimeField(msg("table.request.closeDate"));
	private CustomDateTimeField problemDate = new CustomDateTimeField(msg("table.request.problemDate"));
	private CustomTextField street = new CustomTextField(msg("requestwindow.street"));
	private CustomTextField city = new CustomTextField(msg("requestwindow.city"));
	private CustomTextField district = new CustomTextField(msg("requestwindow.district"));
	private CustomTextField addressDescription = new CustomTextField("Уточн. адреса");
	private CustomTextField owner = new CustomTextField(msg("requestwindow.owner"));
	private CustomTextField room = new CustomTextField(msg("requestwindow.room"));

	private DisabledTextField reqType = new DisabledTextField(msg("requestwindow.reqtype"));
	private DisabledTextField status = new DisabledTextField(msg("table.request.status"));
	private CustomTextField accessNumber = new CustomTextField("Номер в филиале");
	//private CustomTextField importance = new CustomTextField(msg("requestwindow.urgency"));
	private DisabledTextField user = new DisabledTextField("Зарегистрировал");
	private CustomTextField appointed = new CustomTextField("Назначено");

	//private Button clearParentRequestSelectionButton = new Button(BrigadesTheme.ICON_CLOSE);
	//private Button openParentRequestButton = new Button(msg("button.open"), BrigadesTheme.ICON_REQUEST);
	private Button addressCommit = new Button(msg("requestwindow.commit"));
	private Boolean addressChanged = false;
	//private CustomComboBox<RequestDto> parentRequestComboBox = new CustomComboBox<>(msg("requestwindow.reqparent"));
	private CustomComboBox<SourceDto> sourceComboBox = new CustomComboBox<>(msg("requestwindow.source"));
	private CustomComboBox<ProblemDto> problemComboBox = new CustomComboBox<>(msg("requestwindow.type"));
	private CustomComboBox<UrgencyDto> urgencyComboBox = new CustomComboBox<>(msg("requestwindow.urgency"));
	private CustomDateTimeField completionDate = new CustomDateTimeField("Планируемые дата / время закрытия");
	private CustomTextField commentPortal = new CustomTextField();
	private Button portal = new Button();

	private List<SourceDto> sources = restTemplate.getRequestSources();
	private List<ProblemDto> problems = restTemplate.getProblems();
	private List<UrgencyDto> urgencyList = restTemplate.getUrgencyList();

	private AttachmentPanel attachmentPanel;
	private CommentaryGrid commentaryGrid;
	private WorkOrderGrid workOrderGrid;
	private Map<Class, Long> map = new HashMap<>();
	//ЗАКРЫТЬ ЗАЯВКУ В СОУ
	private Button button1;
	//ПРИНЯТЬ К ИСПОЛНЕНИЮ
	Button button2;
	// ПОРУЧИТЬ РВ
	private Button button3;

	public RequestWindow(final RequestDto request) {
		super("Заявка", WIDTH_PIXELS, HEIGHT_PIXELS);
		curUserRoles = new ArrayList<>(restTemplate.getCurrentUserRoles());
		curUserRoles.add(new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));
		this.request = request;
		this.isNew = request.getId() == null;

		List<AttachmentDto> requestAttachments = isNew ? new ArrayList<>() : restTemplate.getRequestAttachments(request.getId());
		List<CommentDto> requestComments = isNew ? new ArrayList<>() : restTemplate.getRequestComments(request.getId());
		map.put(RequestDto.class, request.getId());
		this.attachmentPanel = new AttachmentPanel(requestAttachments, map);
		this.commentaryGrid = new CommentaryGrid(RequestConstants.getCommentColumns(), requestComments);
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		buildFields();
		bindFields();

		removeAllCloseShortcuts();
		addCloseListener(closeEvent -> BrigadesEventBus.unregister(this));
	}

	public static void open(final RequestDto request) {
		Window window = new RequestWindow(request);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	private void buildFields() {
		InputMask.addTo(phone, "(999) 999-99-99");
		List<SlaReactionTimeDto> reactionTimeDtoList = restTemplate.getSlaReactionTime();
		sourceComboBox.setItems(sources);
		sourceComboBox.addValueChangeListener(event -> {
			SourceDto value = event.getValue();
			ProblemDto notHL = problems.stream().filter(problemDto -> problemDto.getId().equals(0L)).findFirst().orElse(null);
			if (value != null && !value.getName().equals("Горячая линия")) {
				// Close problem combobox
				problemComboBox.clear();
				problemComboBox.setItems(notHL);
				problemComboBox.setValue(notHL);
			} else {
				problemComboBox.clear();
				problemComboBox.setItems(getHLProblems());
			}
		});
		problemComboBox.setItems(getHLProblems());
		problemComboBox.addValueChangeListener(event -> {
			ProblemDto value = event.getValue();
			if (event.isUserOriginated() && value != null) {
				// Calculate time to finish date plan
				SlaReactionTimeDto reactionTimeDto = reactionTimeDtoList.stream().filter(slaReactionTimeDto ->
						slaReactionTimeDto.getCodeHl().equals(value.getId().toString())).findFirst().orElse(null);
				if (reactionTimeDto != null) {
					String time = reactionTimeDto.getTimeWorkByRequestSou();
					if (time != null)
						completionDate.setValue(registrationDate.getValue().plusMinutes(Long.parseLong(time)));
				}
			}
		});
		urgencyComboBox.setItems(urgencyList);
		addressCommit.addClickListener(event -> {
			GeocodeWindow geocodeWindow = GeocodeWindow.open(request.getAddress());
			geocodeWindow.addCloseListener(closeEvent -> {
				if (geocodeWindow.isModalResult()) {
					GeocodeDto geocodeDto = geocodeWindow.getAddress();
					addressChanged = true;
					street.setValue(geocodeDto.getName());
				}
			});
		});
		street.addValueChangeListener(valueChangeEvent -> {
			addressCommit.setIcon(valueChangeEvent.getValue() != null && addressChanged ? BrigadesTheme.ICON_OK : BrigadesTheme.ICON_WARNING);
			addressChanged = false;
		});

		FormLayout firstColumnLayout = new FormLayout(requestNum, operator, company, district, street, addressDescription, urgencyComboBox);
		FormLayout secondColumnLayout = new FormLayout(accessNumber, phone, owner, city, room, addressCommit, reqType);
		FormLayout thirdColumnLayout = new FormLayout(status, problemDate, registrationDate, user, closeDate, info, appointed);
		addToContent(CompsUtil.getHorizontalWrapperNoMargin(firstColumnLayout, secondColumnLayout, thirdColumnLayout));
		addToContent(buildTabSheet());
		Button pdf = addToButtonBar("Создать PDF");
	 	boolean b = !isNew && restTemplate.getRequestAttachments(this.request.getId()).stream().noneMatch(e -> e.getFileName().contains((this.request.getId() + ".pdf")));
			pdf.setEnabled(b);

		pdf.addClickListener(clickEvent -> {
			File file = null;
			try {
				String filename = request.getId() + ".pdf";
				PDFRequestBuilder pdfRequestBuilder = new PDFRequestBuilder(request);
				file = pdfRequestBuilder.saveToFile(FileUtil.getReportFileName(filename));
				AttachmentDto attachmentDto = restTemplate.postRequestAttachment(
						this.request.getId(), file, filename, "иной документ", "Описание заявки №" + request.getId(), true);
				if (attachmentDto != null) {
					Notification.show("Вложение создано", Notification.Type.TRAY_NOTIFICATION);
					pdf.setEnabled(false);
					RequestLayout.refresh();
				} else {
					Notification.show("Произошла ошибка при отправке файла на сервер", Notification.Type.ERROR_MESSAGE);
				}
			} catch (IOException | DocumentException e) {
				getLogger().error(e.getMessage());
				Notification.show("Произошла ошибка при создании вложения", Notification.Type.WARNING_MESSAGE);
			} finally {
				if (file != null)
					getLogger().debug("File deleted: {}", file.delete());
			}
		});
		addToButtonBar("Создать работу", $ -> WorkOrderLayout.createWorkOrder(request));
		if (hasRole("Диспетчер СОУ")){
			button1 = addToButtonBar("Закрыть заявку в СОУ", $ -> closeRequest());

		}
		//addToButtonBar("Квитировать", $ -> acceptEvent());
		addSpaceToButtonBar();
		if (hasRole("Диспетчер СОУ") || hasRole("Старший мастер РВ")) {

			if (hasRole("Диспетчер СОУ")){
				button2 = addToButtonBar("Принять к выполнению", $ -> accept("Диспетчер СОУ"));
				if (!(request.getStatus()!=null && (request.getStatus().equals("Принято в СОУ")||request.getStatus().equals("Отправлено в СОУ")))){
					button2.setEnabled(false);
					if (button3!=null) button3.setEnabled(false);
				}
			}else if (hasRole("Старший мастер РВ")){
				button2 = addToButtonBar("Принять к выполнению", $ -> accept("Старший мастер РВ"));
				if (!(request.getStatus()!=null && (request.getResponsible().equals("РВ")))){
					button2.setEnabled(false);
				}
			}

		}
		if (hasRole("Диспетчер СОУ")){
			button3 = addToButtonBar("Поручить РВ", $ -> charge());
			getLogger().debug(request.getResponsible());
			if (request.getResponsible()!=null && request.getResponsible().contains("РВ")){
				button3.setEnabled(false);
			}
		}
//		Button button4 = addToButtonBar("Назначить исполнителя", $ -> assign());
		addSpaceToButtonBar();
		Button button5 = addToButtonBar(msg("button.save"), $ -> save());
		Button button6 = addToButtonBar(msg("button.close"), $ -> close());



	}

	private List<ProblemDto> getHLProblems() {
		List<ProblemDto> list = new ArrayList<>(problems);
		list.removeIf(problemDto -> problemDto.getId().equals(0L));
		return list;
	}

	private Component buildTabSheet() {
		TabSheet tabSheet = new TabSheet();
		tabSheet.addStyleName(BrigadesTheme.TABSHEET_FRAMED);

		tabSheet.addTab(buildInfoTab(), "Информация");
		tabSheet.addTab(buildCommentTab(), "Действия / Комментарии");
		tabSheet.addTab(buildWorkTab(), "Работы");
		tabSheet.addTab(buildRequestTab(), "Связанные заявки");
		tabSheet.addTab(attachmentPanel, "Приложения");

		return tabSheet;
	}

	private Component buildWorkTab() {
		List<WorkDto> requestWorks = isNew ? new ArrayList<>() : restTemplate.getRequestWorks(request.getId());
		workOrderGrid = new WorkOrderGrid(WorkOrderConstants.getRequestColumns(), requestWorks);
		workOrderGrid.setHeight(200, Unit.PIXELS);
		return workOrderGrid;
	}

	private Component buildCommentTab() {
		commentaryGrid.setHeight(200, Unit.PIXELS);

		CustomButton addButton = new CustomButton(BrigadesTheme.ICON_ADD, msg("button.add"), $ -> addCommentary());
		CustomButton editButton = new CustomButton(BrigadesTheme.ICON_EDIT, msg("button.edit"), $ -> editCommentary());

		return CompsUtil.buildGridWithButtons(commentaryGrid, addButton, editButton);
	}

	private Component buildInfoTab() {
		portal.setCaption(request.getPortal() == null ? "-" : "+");
		portal.addClickListener(clickEvent -> portal.setCaption(portal.getCaption().equals("+") ? "-" : "+"));
		HorizontalLayout layout = CompsUtil.getHorizontalWrapperNoMargin(portal, commentPortal);
		layout.setCaption("Портал «Наш Петербург»");
		layout.setComponentAlignment(portal, Alignment.BOTTOM_LEFT);
		layout.setExpandRatio(commentPortal, 1);
		return CompsUtil.getHorizontalWrapperWithMargin(
				CompsUtil.getVerticalWrapperNoMargin(sourceComboBox, problemComboBox, completionDate),
				CompsUtil.getVerticalWrapperNoMargin(layout, description)
		);
	}

	private Component buildRequestTab() {
		RequestGrid requestGrid = new RequestGrid(RequestConstants.getMainColumns(), new ArrayList<>());
		requestGrid.setHeight(200, Unit.PIXELS);
		return requestGrid;
	}

	private void addCommentary() {
		if (isNew)
			Notification.show("Комментарий можно добавить только после сохранения заявки", Notification.Type.ERROR_MESSAGE);
		else {
			CommentDto commentDto = new CommentDto();
			commentDto.setRegisteredBy(BrigadesUI.getCurrentUser().getFullName());
			CommentWindow.open(map, commentDto);
		}
	}

	private void editCommentary() {
		CommentDto selectedRow = commentaryGrid.getSelectedRow();
		if (selectedRow != null) {
			CommentWindow.open(map, selectedRow);
		}
	}

	private void closeRequest() {
		if (RequestConstants.canCloseRequest(request.getStatus())) {
			String prompt = "Вы действительно хотите закрыть заявку " + request.getExternalNumber() + " в СОУ?";
			YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
			confirmationDialog.addCloseListener((Window.CloseListener) e -> {
				if (((YesNoWindow) e.getWindow()).isModalResult()) {
					RequestDto requestDto = restTemplate.closeRequest(request);
					if (requestDto != null) {
						Notification.show("Заявка " + requestDto.getExternalNumber() + " закрыта в СОУ");
						setBeanAndFields(requestDto);
						RequestLayout.refresh();
						//close();
					} else {
						Notification.show("Невозможно закрыть заявку", Notification.Type.ERROR_MESSAGE);
					}
				}
			});
		} else {
			Notification.show(msg("error"), "Невозможно закрыть заявку " + request.getExternalNumber(), Notification.Type.ERROR_MESSAGE);
		}
	}

	private void assign() {

	}

	private void accept(String role) {
		//todo-k Назначить ответсовенного по заявке в бд
		// Будет добавлена, когда Рустам скажет необходимые функции

			request.setResponsible("СОУ, "+BrigadesUI.getCurrentUser().getFullName());
			RequestJournalDtoGrid.currentlyOpenedGrid.getItem(request.getId()).setResponsible("СОУ, "+BrigadesUI.getCurrentUser().getFullName());
			RequestJournalDtoGrid.currentlyOpenedGrid.refreshItem(request.getId());
			button2.setEnabled(false);
			button3.setEnabled(false);

		getLogger().debug("Binder is valid: " + binder.validate().isOk());
		getLogger().debug(StringUtil.writeValueAsString(binder.getBean()));
	}

	private void charge() {
		//todo-k Назначить ответсовенного по заявке в бд
		// Будет добавлена, когда Рустам скажет необходимые функции
		request.setResponsible("РВ");
 		RequestJournalDtoGrid.currentlyOpenedGrid.getItem(request.getId()).setResponsible("РВ");
		RequestJournalDtoGrid.currentlyOpenedGrid.refreshItem(request.getId());
		if (request.getResponsible()!=null && request.getResponsible().contains("РВ")){
			button3.setEnabled(false);
		}
	}

	private void save() {
		if (binder.validate().isOk()) {
			getLogger().debug("Before save: " + StringUtil.writeValueAsString(binder.getBean()));
			RequestDto savedRequest = restTemplate.saveRequest(binder.getBean());
			if (savedRequest != null) {
				getLogger().debug("After save: " + StringUtil.writeValueAsString(savedRequest));
				setBeanAndFields(savedRequest);
				IncInfo incInfo = restTemplate.createInfo(savedRequest, false);
				if (RequestConstants.isTestRequest(incInfo.getpIncidentNumber()))
					restTemplate.sendInfoToHotLine(incInfo);
				RequestLayout.refresh();
				Notification.show("Заявка сохранена", Notification.Type.TRAY_NOTIFICATION);
				//close();
				return;
			}
		}
		Notification.show("Ошибка при сохранении данных", Notification.Type.ERROR_MESSAGE);
	}

	private void setBeanAndFields(RequestDto requestDto) {
		binder.setBean(requestDto);
		request = binder.getBean();
		isNew = request.getId() == null;
		boolean enabled = !RequestDto.Status.CLOSED.getCode().equalsIgnoreCase(request.getStatus());
		setButtonEnabled(0, enabled && !isNew);
		setButtonEnabled(6, enabled);

		addressCommit.setEnabled(isNew);
		//addressCommit.setEnabled(false);
		addressCommit.setIcon(request.getAddress() != null ? BrigadesTheme.ICON_OK : BrigadesTheme.ICON_WARNING);
		portal.setCaption(request.getPortal() == null ? "-" : "+");
		portal.setEnabled(isNew);
		registrationDate.setEnabled(isNew);
		problemDate.setEnabled(isNew);
		map.put(RequestDto.class, request.getId());
		attachmentPanel.setSourceMap(map);

		LocalDateTime now = LocalDateTime.now();
		registrationDate.setRangeEnd(now);
		problemDate.setRangeEnd(now);
	}

	private void bindFields() {
		String nullError = "Значение не может быть нулевым";
		SerializablePredicate<String> nullPredicate = StringUtil.nullPredicate();

		binder.bind(requestNum, RequestDto::getExternalNumber, RequestDto::setExternalNumber);
		//binder.bind(description, RequestDto::getDescription, RequestDto::setDescription);
		binder.forField(description).asRequired(nullError).withValidator(nullPredicate, nullError).bind(RequestDto::getDescription, RequestDto::setDescription);
		binder.bind(operator, RequestDto::getAuthor, RequestDto::setAuthor);
		binder.bind(company, RequestDto::getCompany, RequestDto::setCompany);
		binder.bind(phone, RequestDto::getFeedback, RequestDto::setFeedback);
		binder.bind(info, RequestDto::getInfo, RequestDto::setInfo);
		binder.bind(commentPortal, RequestDto::getCommentPortal, RequestDto::setCommentPortal);
		binder.bind(city, RequestDto::getCity, RequestDto::setCity);
		binder.bind(district, RequestDto::getDistrict, RequestDto::setDistrict);
		//binder.bind(street, RequestDto::getAddress, RequestDto::setAddress);
		binder.forField(street).asRequired(nullError).withValidator(nullPredicate, nullError).bind(RequestDto::getAddress, RequestDto::setAddress);
		binder.bind(addressDescription, RequestDto::getUpdatedAddress, RequestDto::setUpdatedAddress);
		binder.bind(owner, RequestDto::getOwner, RequestDto::setOwner);
		binder.bind(room, RequestDto::getFlat, RequestDto::setFlat);
		binder.bind(accessNumber, RequestDto::getAccessNumber, RequestDto::setAccessNumber);
		binder.bind(appointed, RequestDto::getResponsible, RequestDto::setResponsible);
		binder.bind(reqType, RequestDto::getType, RequestDto::setType);
		binder.bind(status, RequestDto::getStatusHL, RequestDto::setStatusHL);
		binder.bind(user, RequestDto::getRegisteredBy, RequestDto::setRegisteredBy);

		binder.forField(registrationDate).asRequired(nullError).bind(dateGetter(RequestDto::getRegistrationDate), dateSetter(RequestDto::setRegistrationDate));
		binder.bind(closeDate, dateGetter(RequestDto::getCloseDate), dateSetter(RequestDto::setCloseDate));
		binder.forField(problemDate).asRequired(nullError).bind(dateGetter(RequestDto::getIncidentDate), dateSetter(RequestDto::setIncidentDate));
		binder.forField(completionDate).asRequired(nullError).bind(dateGetter(RequestDto::getFinishDatePlan), dateSetter(RequestDto::setFinishDatePlan));

		//binder.bind(sourceComboBox, getSource(), setSource());
		//binder.bind(problemComboBox, getProblem(), setProblem());
		//binder.bind(urgencyComboBox, getUrgency(), setUrgency());
		binder.forField(sourceComboBox).asRequired(nullError).bind(getSource(), setSource());
		binder.forField(problemComboBox).asRequired(nullError).bind(getProblem(), setProblem());
		binder.forField(urgencyComboBox).asRequired(nullError).bind(getUrgency(), setUrgency());

		setBeanAndFields(request);
	}

	private ValueProvider<RequestDto, ProblemDto> getProblem() {
		return (ValueProvider<RequestDto, ProblemDto>) request -> request.getProblem() == null ? null :
				problems.stream().filter(s -> s.getName().equals(request.getProblem())).findFirst().orElse(null);
	}

	private Setter<RequestDto, ProblemDto> setProblem() {
		return (Setter<RequestDto, ProblemDto>) (request, problem) -> {
			String problemName = problem == null ? null : problem.getName();
			request.setProblem(problemName);
		};
	}

	private ValueProvider<RequestDto, SourceDto> getSource() {
		return (ValueProvider<RequestDto, SourceDto>) request -> request.getSource() == null ? null :
				sources.stream().filter(s -> s.getName().equals(request.getSource())).findFirst().orElse(null);
	}

	private Setter<RequestDto, SourceDto> setSource() {
		return (Setter<RequestDto, SourceDto>) (request, source) -> {
			String sourceName = source == null ? null : source.getName();
			request.setSource(sourceName);
		};
	}

	private ValueProvider<RequestDto, UrgencyDto> getUrgency() {
		return (ValueProvider<RequestDto, UrgencyDto>) request -> request.getUrgency() == null ? null :
				urgencyList.stream().filter(s -> s.getName().equals(request.getUrgency())).findFirst().orElse(null);
	}

	private Setter<RequestDto, UrgencyDto> setUrgency() {
		return (Setter<RequestDto, UrgencyDto>) (request, urgency) -> {
			String urgencyName = urgency == null ? null : urgency.getName();
			request.setUrgency(urgencyName);
		};
	}

	private IncInfo createInfo(RequestDto requestDto) {
		IncInfo incInfo = new IncInfo();
		incInfo.setpIncidentId(requestDto.getNumber());
		incInfo.setpIncidentNumber(requestDto.getExternalNumber());
		incInfo.setpStatusName(requestDto.getStatusHL());
		incInfo.setpFilialNum(requestDto.getAccessNumber());
		incInfo.setpAddAdrInfo(requestDto.getUpdatedAddress());
		if (requestDto.getCloseDate() != null)
			incInfo.setpClosedDate(DatesUtil.formatZoned(requestDto.getCloseDate()));
		incInfo.setpUpdatedByFio(BrigadesUI.getCurrentUser().getFullName());
		return incInfo;
	}

	private void updateWindow() {
		if (!isNew) {
			commentaryGrid.update(restTemplate.getRequestComments(request.getId()));
			workOrderGrid.update(restTemplate.getRequestWorks(request.getId()));
			attachmentPanel.setAttachments(restTemplate.getRequestAttachments(request.getId()));
		}
	}
	private boolean hasRole(RoleDto roleDto){
		return curUserRoles.contains(roleDto);
	}
	private boolean hasRole(String roleDto){
		return curUserRoles.contains(new RoleDto(roleDto, roleDto));
	}

	@Subscribe
	public void updateRequestsEvent(final UpdateRequestsEvent event) {
		updateWindow();
	}

	@Subscribe
	public void updateViewEvent(final UpdateViewEvent event) {
		updateWindow();
	}
}
