package ru.telros.brigades.client.ui.component;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.ui.utils.FileReceiver;
import ru.telros.brigades.client.ui.utils.FileUtil;

import java.io.File;

public class CustomUpload extends HorizontalLayout implements HasLogger {
	private File file;
	private String fileName;

	public CustomUpload() {
		FileReceiver fileReceiver = FileUtil.getReceiver();
		ProgressBar progressBar = new ProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setVisible(false);
		Label iconLabel = new Label();
		iconLabel.setWidthUndefined();
		iconLabel.setContentMode(ContentMode.HTML);
		Label textLabel = new Label();
		textLabel.setSizeFull();
		Upload upload = new Upload();
		upload.setWidthUndefined();
		upload.setButtonCaption("Выберите файл");
		upload.setReceiver(fileReceiver);
		upload.addStartedListener(startedEvent -> {
			getLogger().debug("START - Name: {}, MIME: {}", startedEvent.getFilename(), startedEvent.getMIMEType());
			progressBar.setVisible(true);
			iconLabel.setValue(null);
			textLabel.setValue(startedEvent.getFilename());
		});
		upload.addFinishedListener(finishedEvent -> {
			progressBar.setVisible(false);
			getLogger().debug("FINISH - Name: {}, MIME: {}", finishedEvent.getFilename(), finishedEvent.getMIMEType());
		});
		upload.addSucceededListener(succeededEvent -> {
			getLogger().debug("SUCCESS - Name: {}, MIME: {}", succeededEvent.getFilename(), succeededEvent.getMIMEType());
			iconLabel.setValue(BrigadesTheme.ICON_OK.getHtml());
			upload.setEnabled(false);
			file = new File(fileReceiver.getFilePath());
			fileName = succeededEvent.getFilename();
			if (file.exists()) {
				getLogger().debug("Name: {}, Path: {}", file.getName(), file.getPath());
			} else {
				getLogger().error("File doesn't exists");
			}
		});
		upload.addFailedListener(failedEvent -> {
			iconLabel.setValue(BrigadesTheme.ICON_WARNING.getHtml());
			getLogger().error("FAIL - Error: {}", failedEvent.getReason().getMessage());
		});

		setSizeFull();
		setSpacing(true);
		setMargin(false);
		addComponents(upload, progressBar, iconLabel, textLabel);
		setExpandRatio(textLabel, 1);
		setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
	}

	public File getFile() {
		return file;
	}

	public String getFileName() {
		return fileName;
	}
}
