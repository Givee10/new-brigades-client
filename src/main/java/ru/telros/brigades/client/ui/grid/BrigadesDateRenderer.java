package ru.telros.brigades.client.ui.grid;

import com.vaadin.ui.renderers.LocalDateTimeRenderer;
import ru.telros.brigades.client.ui.utils.DatesUtil;

public class BrigadesDateRenderer extends LocalDateTimeRenderer {
	public BrigadesDateRenderer() {
		super(DatesUtil.GRID_DATE_FORMAT);
	}
}
