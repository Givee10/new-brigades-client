package ru.telros.brigades.client.ui.maplayout;

import java.io.Serializable;
import java.util.Collection;

/**
 * Базовый класс для элементов карты
 */
public abstract class MapElement implements Serializable {
//public static final long serialVersionUID = 1443920175588070872L; //434066435674155085L

	private final static String DEFUALT_PREFIX = "el_";

	// глобальный ид-р элемента (числовая часть)
	private static int elementUid = 0;
	// Уникальный идентификатор элемента [YandexMaps]
	private String uid;
	// Наименование элемента
	private String caption;
	// Описание элемента
	private String description;

	public MapElement() {
		this(null, null);
	}

	public MapElement(String caption, String description) {
		this.uid = getPrefix() + String.valueOf(getNextUid());
		this.caption = caption;
		this.description = description;
	}

	/**
	 * Генерирует уникальный ид-р маркера
	 *
	 * @return
	 */
	private static synchronized int getNextUid() {
		return ++elementUid;
	}

	/**
	 * Поиск UID в коллекции элементов
	 *
	 * @param elements
	 * @param uid
	 * @return
	 */
	public static MapElement findByUid(Collection<? extends MapElement> elements, String uid) {
		for (MapElement mapElement : elements) {
			if (mapElement.getUid().equals(uid)) {
				return (MapElement) mapElement;
			}
		}
		return null;
	}

	public static void clearElementUid() {
		elementUid = 0;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// Префикс элемента
	protected String getPrefix() {
		return DEFUALT_PREFIX;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MapElement mapElement = (MapElement) o;

		if (uid != null ? !uid.equals(mapElement.uid) : mapElement.uid != null) return false;
		return caption != null ? caption.equals(mapElement.caption) : mapElement.caption == null;

	}

	@Override
	public int hashCode() {
		int result = uid != null ? uid.hashCode() : 0;
		result = 31 * result + (caption != null ? caption.hashCode() : 0);
		return result;
	}
}
