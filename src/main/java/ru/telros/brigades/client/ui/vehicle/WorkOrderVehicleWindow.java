package ru.telros.brigades.client.ui.vehicle;

import com.vaadin.data.Binder;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.EquipmentDto;
import ru.telros.brigades.client.dto.EquipmentTypeDto;
import ru.telros.brigades.client.dto.EquipmentWorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.CustomComboBox;
import ru.telros.brigades.client.ui.component.CustomDateTimeField;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.util.List;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.MessageManager.msg;
import static ru.telros.brigades.client.ui.grid.GridColumn.dateGetter;
import static ru.telros.brigades.client.ui.grid.GridColumn.dateSetter;

public class WorkOrderVehicleWindow extends WindowWithButtons implements HasLogger {
	private static final int WIDTH_PIXELS = 600;
	private static final int HEIGHT_PIXELS = -1;

	private EquipmentWorkDto workOrderVehicle;
	private Binder<EquipmentWorkDto> binder = new Binder<>();
	private List<EquipmentDto> equipmentList;

	private CustomDateTimeField startDatePlan = new CustomDateTimeField("План прибытие");
	private CustomDateTimeField finishDatePlan = new CustomDateTimeField("План убытие");
	private CustomDateTimeField startDateFact = new CustomDateTimeField("Факт прибытие");
	private CustomDateTimeField finishDateFact = new CustomDateTimeField("Факт убытие");
	private CustomTextField comments = new CustomTextField("Комментарии");

	private CustomComboBox<EquipmentTypeDto> typeComboBox = new CustomComboBox<>("Тип техники");
	private CustomComboBox<EquipmentDto> numberComboBox = new CustomComboBox<>("Номер");

	public WorkOrderVehicleWindow(EquipmentWorkDto workOrderVehicle, List<EquipmentDto> equipmentList) {
		super("Спецтехника", WIDTH_PIXELS, HEIGHT_PIXELS);
		this.workOrderVehicle = workOrderVehicle;
		this.equipmentList = equipmentList;
		build();
		setItem();
		setModal(true);
		BrigadesEventBus.register(this);
		addCloseListener(closeEvent -> BrigadesEventBus.unregister(this));
	}

	private void build() {
		typeComboBox.setItems(BrigadesUI.getRestTemplate().getEquipmentTypes());
		typeComboBox.setItemCaptionGenerator(EquipmentTypeDto::getType);
		typeComboBox.setPlaceholder("Выберите тип спецтехники");
		typeComboBox.addValueChangeListener($ -> changeNumberSet());

		//numberComboBox.setItems(equipmentList);
		numberComboBox.setItemCaptionGenerator(EquipmentDto::getNumber);
		numberComboBox.setPlaceholder("Выберите номер спецтехники");
		numberComboBox.addValueChangeListener($ -> changeVehicle());

		comments.setSizeFull();

		addToContent(CompsUtil.getHorizontalWrapperNoMargin(typeComboBox, numberComboBox));
		addToContent(CompsUtil.getHorizontalWrapperNoMargin(comments));
		addToContent(CompsUtil.getHorizontalWrapperNoMargin(startDatePlan, finishDatePlan));
		addToContent(CompsUtil.getHorizontalWrapperNoMargin(startDateFact, finishDateFact));

		addToButtonBar(msg("button.save"), $ -> onClickSaveButton());
		addToButtonBar(msg("button.cancel"), $ -> close());
	}

	private void onClickSaveButton() {
		if (binder.validate().isOk()) {
			getLogger().debug("Before save: " + StringUtil.writeValueAsString(binder.getBean()));
			List<EquipmentWorkDto> savedEquipments = BrigadesUI.getRestTemplate().attachVehicleToWork(binder.getBean());
			if (savedEquipments != null && !savedEquipments.isEmpty()) {
				EquipmentWorkDto saved = savedEquipments.get(0);
				getLogger().debug("After save:  " + StringUtil.writeValueAsString(saved));
				WorkOrderLayout.refresh();
				close();
				return;
			}
		}
		Notification.show("Ошибка при сохранении данных", Notification.Type.ERROR_MESSAGE);
	}

	private void changeNumberSet() {
		numberComboBox.clear();
		EquipmentTypeDto type = typeComboBox.getValue();
		if (type != null) {
			List<EquipmentDto> list = equipmentList.stream()
					.filter(equipmentDto -> equipmentDto.getType().equals(type.getType())).collect(Collectors.toList());
			numberComboBox.setItems(list);
		}
	}

	private void changeVehicle() {
		EquipmentDto vehicle = numberComboBox.getValue();
		if (vehicle != null) {
			workOrderVehicle.setId(vehicle.getId());
			workOrderVehicle.setGuid(vehicle.getGuid());
			workOrderVehicle.setModel(vehicle.getModel());
			workOrderVehicle.setNumber(vehicle.getNumber());
			workOrderVehicle.setType(vehicle.getType());
			workOrderVehicle.setUnit(vehicle.getUnit());
			workOrderVehicle.setCode(vehicle.getCode());
			workOrderVehicle.setDescription(vehicle.getDescription());
		} else {
			workOrderVehicle.setId(null);
			workOrderVehicle.setGuid(null);
			workOrderVehicle.setModel(null);
			workOrderVehicle.setNumber(null);
			workOrderVehicle.setType(null);
			workOrderVehicle.setUnit(null);
			workOrderVehicle.setCode(null);
			workOrderVehicle.setDescription(null);
		}
	}

	private void setItem() {
		String nullError = "Значение не может быть нулевым";

		binder.bind(startDateFact, dateGetter(EquipmentWorkDto::getStartDateFact), dateSetter(EquipmentWorkDto::setStartDateFact));
		binder.bind(finishDateFact, dateGetter(EquipmentWorkDto::getFinishDateFact), dateSetter(EquipmentWorkDto::setFinishDateFact));
		binder.forField(startDatePlan).asRequired(nullError).bind(dateGetter(EquipmentWorkDto::getStartDatePlan), dateSetter(EquipmentWorkDto::setStartDatePlan));
		binder.forField(finishDatePlan).asRequired(nullError).bind(dateGetter(EquipmentWorkDto::getFinishDatePlan), dateSetter(EquipmentWorkDto::setFinishDatePlan));
		binder.bind(comments, EquipmentWorkDto::getComments, EquipmentWorkDto::setComments);
		binder.setBean(workOrderVehicle);
	}

	public static WorkOrderVehicleWindow open(EquipmentWorkDto workOrderVehicle, List<EquipmentDto> vehicleList) {
		WorkOrderVehicleWindow window = new WorkOrderVehicleWindow(workOrderVehicle, vehicleList);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
		return window;
	}
}
