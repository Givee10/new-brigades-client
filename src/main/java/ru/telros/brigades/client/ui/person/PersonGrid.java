package ru.telros.brigades.client.ui.person;

import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class PersonGrid extends BrigadesGrid<EmployeeBrigadeDto> {
	public PersonGrid(List<GridColumn<EmployeeBrigadeDto>> columns, List<EmployeeBrigadeDto> items) {
		super(columns, items);
	}

	@Override
	protected void onClick(EmployeeBrigadeDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(EmployeeBrigadeDto entity, Column column) {

	}
}
