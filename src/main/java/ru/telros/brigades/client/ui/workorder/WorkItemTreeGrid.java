package ru.telros.brigades.client.ui.workorder;

import ru.telros.brigades.client.dto.WorkItemDto;
import ru.telros.brigades.client.ui.grid.BrigadesTreeGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.List;

public class WorkItemTreeGrid extends BrigadesTreeGrid<WorkItemDto> {
	public WorkItemTreeGrid(List<GridColumn<WorkItemDto>> gridColumns) {
		super(gridColumns);
	}
}
