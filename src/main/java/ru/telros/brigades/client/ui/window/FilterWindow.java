package ru.telros.brigades.client.ui.window;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.ui.component.CustomTextField;
import ru.telros.brigades.client.ui.utils.CompsUtil;

import static ru.telros.brigades.client.MessageManager.msg;

public class FilterWindow<T> extends WindowWithButtons implements HasLogger {
	private final Grid<T> grid;
	private boolean saved = false;

	private FilterWindow(Grid<T> grid, int width, int height) {
		super("Фильтр таблицы", width, height);
		this.grid = grid;

		initComponents();
	}

	private void initComponents() {
		VerticalLayout layout = CompsUtil.getVerticalWrapperNoMargin();
		grid.getColumns().forEach(column -> {
			CheckBox checkBox = new CheckBox(column.getCaption(), !column.isHidden());
			checkBox.setSizeFull();
			CustomTextField textField = new CustomTextField();
			textField.setValue(String.valueOf(column.getWidth()));
			layout.addComponent(CompsUtil.getHorizontalWrapperNoMargin(checkBox, textField));
		});
		addToContent(layout, 1);
		addToButtonBar(msg("button.save"), event -> commit());
		addToButtonBar(msg("button.close"), event -> close());
	}

	private void commit() {
		saved = true;
		close();
	}

	public boolean isSaved() {
		return saved;
	}

	public static <T> FilterWindow open(Grid<T> grid, int width, int height) {
		FilterWindow<T> window = new FilterWindow<>(grid, width, height);
		UI.getCurrent().addWindow(window);
		window.focus();
		window.getLogger().debug("{}, {} : {}", grid.getBeanType(), width, height);
		return window;
	}
}
