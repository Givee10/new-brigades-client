package ru.telros.brigades.client.ui.workorder;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import ru.telros.brigades.client.BrigadesIcons;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.ui.grid.GridColumn;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;
import java.util.*;

import static ru.telros.brigades.client.MessageManager.msg;

public abstract class WorkOrderConstants {
	public static final GridColumn<WorkDto> FLAG = GridColumn.createComponentColumn("_flag", "", WorkOrderConstants::createFlag);
	public static final GridColumn<WorkDto> COMMENT = GridColumn.createComponentColumn("_comment", "", WorkOrderConstants::createComment);
	public static final GridColumn<WorkDto> FLAG_CONFIRMED = GridColumn.createComponentColumn("_flag_confirmed", "", WorkOrderConstants::createFlagConfirmed);
	public static final GridColumn<WorkDto> NAME = GridColumn.createColumn("name", "Наименование работы / детали", WorkDto::getDescription);
	public static final GridColumn<WorkDto> SOURCE = GridColumn.createColumn("source", "Источник", WorkDto::getSource);
	public static final GridColumn<WorkDto> CODE = GridColumn.createColumn("code", "Код проблемы", WorkDto::getCode);
	public static final GridColumn<WorkDto> TYPE = GridColumn.createColumn("type", "Вид работы", WorkOrderConstants::typeWorkProvider);
	public static final GridColumn<WorkDto> DESCRIPTION = GridColumn.createColumn("desc", "Описание", WorkDto::getDescription);
	public static final GridColumn<WorkDto> ADDRESS = GridColumn.createColumn("address", "Адрес", WorkDto::getAddress);
	public static final GridColumn<WorkDto> EXECUTOR = GridColumn.createColumn("executor", "Исполнитель", WorkDto::getBrigade);
	public static final GridColumn<WorkDto> GROUP = GridColumn.createColumn("group", "Бригада", WorkDto::getBrigade);
	public static final GridColumn<WorkDto> REGION = GridColumn.createColumn("region", "Район РВ", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> DISTRICT = GridColumn.createColumn("district", "Админ. район", WorkDto::getLocation);
	public static final GridColumn<WorkDto> ATTACHMENTS = GridColumn.createColumn("attachments", "Приложения", WorkOrderConstants::attachmentProvider);
	public static final GridColumn<WorkDto> START_PLAN = GridColumn.createDateColumn("startDatePlan", "Начало по плану", WorkDto::getStartDatePlan);
	public static final GridColumn<WorkDto> FINISH_PLAN = GridColumn.createDateColumn("finishDatePlan", "Окончание по плану", WorkDto::getFinishDatePlan);
	public static final GridColumn<WorkDto> START_FACT = GridColumn.createDateColumn("startDateFact", "Начало факт", WorkDto::getStartDate);
	public static final GridColumn<WorkDto> FINISH_FACT = GridColumn.createDateColumn("finishDateFact", "Окончание факт", WorkDto::getFinishDate);

	public static final GridColumn<WorkDto> BREAK_COUNTER = GridColumn.createColumn("break", "Суммарная продолж. перерывов, мин", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> RELATED = GridColumn.createColumn("related", "Связан.", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> REQUEST = GridColumn.createColumn("request_number", "№ заявки", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> TOIR_NUMBER = GridColumn.createColumn("toir_number", "№ работы ТОиР", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> AIS_NUMBER = GridColumn.createColumn("ais_number", "№ работы АИС", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> CREATE_DATE = GridColumn.createColumn("create_date", "Дата создания", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> SPEC_TECHNIC = GridColumn.createColumn("spec_technic", "Наличие спецтехники", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> DISABLE = GridColumn.createColumn("disable", "Работа с откл. Потребителей", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> GATI = GridColumn.createColumn("gati", "Работа по ордеру ГАТИ", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> GRAPH = GridColumn.createColumn("graph", "Работа по Графику", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> SCHEDULE = GridColumn.createColumn("schedule", "Работа по Регламенту", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> MOTION_RESTRICTION = GridColumn.createColumn("motion", "Огранич. движения а/транс.", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> MAVR = GridColumn.createColumn("mavr", "Наличие МАВР", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> CREATE_BY_MASTER = GridColumn.createColumn("master", "Создание работы мастером", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> BRIGADE_ADDRESS = GridColumn.createColumn("brigade_address", "Бригада на адресе", GridColumn::nullProvider);
	public static final GridColumn<WorkDto> BRIGADE_NAME = GridColumn.createColumn("brigade_text", "Бригада", e->BrigadesUI.getRestTemplate().getBrigade(e.getIdBrigade()));
	public static final GridColumn<WorkDto> START_DER = GridColumn.createColumn("start_der", "Отклонение начало", e->{
		if (e.getStartDate()!=null && e.getStartDatePlan()!=null){
			if (e.getStartDate().isAfter(e.getStartDatePlan())){
				return "Начали позже на " + DatesUtil.secondsToHHMM(e.getStartDate().toEpochSecond()-e.getStartDatePlan().toEpochSecond());
			}else {
				return "Начали раньше на " + DatesUtil.secondsToHHMM(e.getStartDatePlan().toEpochSecond()-e.getStartDate().toEpochSecond());
			}
		}else {
			return " - ";
		}
	});
	public static final GridColumn<WorkDto> FINISH_DER = GridColumn.createColumn("finish_der", "Отклонение окончание", e->{
		if (e.getFinishDate()!=null && e.getFinishDatePlan()!=null){
			if (e.getFinishDate().isAfter(e.getFinishDatePlan())){
				return "Закончили позже на " + DatesUtil.secondsToHHMM(e.getFinishDate().toEpochSecond()-e.getFinishDatePlan().toEpochSecond());
			}else {
				return "Закончили раньше на " + DatesUtil.secondsToHHMM(e.getFinishDatePlan().toEpochSecond()-e.getFinishDate().toEpochSecond());
			}
		}else {
			return " - ";
		}
	});
	public static final GridColumn<WorkDto> CREATED_BY = GridColumn.createColumn("created_by", "Создатель работы", e->e.getAuthor().getFullName());
	public static final GridColumn<WorkDto> FACT_DURATION = GridColumn.createColumn("fact_duration", "Фактическая продолжительность", e->{
		if (e.getStartDate()!=null && e.getFinishDate()!=null){
			return DatesUtil.secondsToHHMM(e.getFinishDate().toEpochSecond()-e.getStartDate().toEpochSecond());
		}else {
			return " - ";
		}
	});


	public static final GridColumn<WorkItemWorkDto> NAME_ = GridColumn.createColumn("name", "Наименование работы / детали", WorkItemWorkDto::getDescription);
	public static final GridColumn<WorkItemWorkDto> WORK_ADDRESS = GridColumn.createColumn("address", "Адрес", WorkItemWorkDto::getAddress);
	public static final GridColumn<WorkItemWorkDto> START_PLAN_ = GridColumn.createDateColumn("startDatePlan", "Начало по плану", WorkItemWorkDto::getStartDatePlan);
	public static final GridColumn<WorkItemWorkDto> FINISH_PLAN_ = GridColumn.createDateColumn("finishDatePlan", "Окончание по плану", WorkItemWorkDto::getFinishDatePlan);
	public static final GridColumn<WorkItemWorkDto> START_FACT_ = GridColumn.createDateColumn("startDateFact", "Начало факт", WorkItemWorkDto::getStartDate);
	public static final GridColumn<WorkItemWorkDto> FINISH_FACT_ = GridColumn.createDateColumn("finishDateFact", "Окончание факт", WorkItemWorkDto::getFinishDate);

	public static final GridColumn<WorkJournalDto> FLAG_ = GridColumn.createComponentColumn("_flag", "", WorkOrderConstants::createJournalFlag);
	public static final GridColumn<WorkJournalDto> ICON = GridColumn.createComponentColumn("_icon", "", WorkOrderConstants::createJournalIcon);
	public static final GridColumn<WorkJournalDto> ID = GridColumn.createColumn("id", "№", WorkJournalDto::getId);
	public static final GridColumn<WorkJournalDto> SOURCE_ = GridColumn.createColumn("source", "Источник", WorkJournalDto::getSource);
	public static final GridColumn<WorkJournalDto> PROBLEM_CODE = GridColumn.createColumn("problemCode", "Код проблемы", WorkJournalDto::getProblemCode);
	public static final GridColumn<WorkJournalDto> TYPE_ = GridColumn.createColumn("type", "Вид работы", WorkJournalDto::getType);
	public static final GridColumn<WorkJournalDto> DESCRIPTION_ = GridColumn.createColumn("desc", "Описание", WorkJournalDto::getDescription);
	public static final GridColumn<WorkJournalDto> ADDRESS_ = GridColumn.createColumn("address", "Адрес", WorkJournalDto::getAddress);
	public static final GridColumn<WorkJournalDto> BRIGADE = GridColumn.createColumn("brigade", "Исполнитель", WorkJournalDto::getBrigade);
	public static final GridColumn<WorkJournalDto> REGION_ = GridColumn.createColumn("region", "Район РВ", WorkJournalDto::getRegion);
	public static final GridColumn<WorkJournalDto> LOCATION = GridColumn.createColumn("district", "Админ. район", WorkJournalDto::getLocation);
	public static final GridColumn<WorkJournalDto> HAS_ATTACHMENTS = GridColumn.createBooleanColumn("attachments", "Приложения", WorkJournalDto::getHasAttachment, "Есть");
	public static final GridColumn<WorkJournalDto> START_DATE_PLAN = GridColumn.createDateColumn("startDatePlan", "Начало по плану", WorkJournalDto::getStartDatePlan);
	public static final GridColumn<WorkJournalDto> FINISH_DATE_PLAN = GridColumn.createDateColumn("finishDatePlan", "Окончание по плану", WorkJournalDto::getFinishDatePlan);
	public static final GridColumn<WorkJournalDto> START_DATE_FACT = GridColumn.createDateColumn("startDateFact", "Начало факт", WorkJournalDto::getStartDateFact);
	public static final GridColumn<WorkJournalDto> FINISH_DATE_FACT = GridColumn.createDateColumn("finishDateFact", "Окончание факт", WorkJournalDto::getFinishDateFact);

	public static final GridColumn<WorkJournalDto> TIMEOUT = GridColumn.createColumn("break", "Суммарная продолж. перерывов, мин", WorkJournalDto::getTimeout);
	public static final GridColumn<WorkJournalDto> LINKED = GridColumn.createColumn("related", "Связан.", WorkJournalDto::getLinked);
	public static final GridColumn<WorkJournalDto> NUMBER_REQUEST = GridColumn.createColumn("request_number", "№ заявки", WorkJournalDto::getNumberRequest);
	public static final GridColumn<WorkJournalDto> NUMBER_TOIR = GridColumn.createColumn("toir_number", "№ работы ТОиР", WorkJournalDto::getNumberToir);
	public static final GridColumn<WorkJournalDto> NUMBER_AIS = GridColumn.createColumn("ais_number", "№ работы АИС", WorkJournalDto::getNumberAis);
	public static final GridColumn<WorkJournalDto> REGISTRATION_DATE = GridColumn.createDateColumn("create_date", "Дата создания", WorkJournalDto::getRegistrationDate);
	public static final GridColumn<WorkJournalDto> EQUIPMENT = GridColumn.createBooleanColumn("spec_technic", "Наличие спецтехники", WorkJournalDto::getHasEquipment, "+");
	public static final GridColumn<WorkJournalDto> CONSUMER_OFF = GridColumn.createBooleanColumn("disable", "Работа с откл. Потребителей", WorkJournalDto::getConsumerOff, "+");
	public static final GridColumn<WorkJournalDto> GATI_WORK = GridColumn.createBooleanColumn("gati", "Работа по ордеру ГАТИ", WorkJournalDto::getGati, "+");
	public static final GridColumn<WorkJournalDto> SCHEDULED = GridColumn.createBooleanColumn("graph", "Работа по Графику", WorkJournalDto::getScheduled, "+");
	public static final GridColumn<WorkJournalDto> REGULATED = GridColumn.createBooleanColumn("schedule", "Работа по Регламенту", WorkJournalDto::getRegulated, "+");
	public static final GridColumn<WorkJournalDto> TRAFFIC_OFF = GridColumn.createBooleanColumn("motion", "Огранич. движения а/транс.", WorkJournalDto::getTrafficOff, "+");
	public static final GridColumn<WorkJournalDto> MAVR_ = GridColumn.createBooleanColumn("mavr", "Наличие МАВР", WorkJournalDto::getMavr, "+");
	public static final GridColumn<WorkJournalDto> CREATE_BY_BRIGADE = GridColumn.createBooleanColumn("master", "Создание работы мастером", WorkJournalDto::getCreatedByBrigade, "+");
	public static final GridColumn<WorkJournalDto> BRIGADE_ON_ADDRESS = GridColumn.createBooleanColumn("brigade_address", "Бригада на адресе", WorkJournalDto::getBrigadeOnAddress, "+");

	public static final LinkedHashMap<String, String> workStyles = new LinkedHashMap<String, String>() {{
		put(WorkDto.Status.NEW.getCode(), BrigadesTheme.STYLE_ET_NORMAL);
		put(WorkDto.Status.ASSIGNED.getCode(), BrigadesTheme.STYLE_ET_PLAN);
		put(WorkDto.Status.IN_PROGRESS.getCode(), BrigadesTheme.STYLE_ET_INWORK);
		put("Нарушены сроки", BrigadesTheme.STYLE_ET_EMERGENCY); //workStyleEmergency
		put(WorkDto.Status.COMPLETE.getCode(), BrigadesTheme.STYLE_ET_SUCCESS);
		put(WorkDto.Status.CLOSED.getCode(), BrigadesTheme.STYLE_ET_INFO);
	}};
	private static final String workStyleEmergency = BrigadesTheme.STYLE_ET_EMERGENCY;

	private static final LinkedHashMap<String, String> taskStyles = new LinkedHashMap<String, String>() {{
		put(WorkDto.Status.NEW.getCode(), BrigadesTheme.STYLE_TASK_WARNING);
		put(WorkDto.Status.ASSIGNED.getCode(), BrigadesTheme.STYLE_TASK_PLAN);
		put(WorkDto.Status.IN_PROGRESS.getCode(), BrigadesTheme.STYLE_TASK_INWORK);
		put(WorkDto.Status.COMPLETE.getCode(), BrigadesTheme.STYLE_TASK_SUCCESS);
		put(WorkDto.Status.CLOSED.getCode(), BrigadesTheme.STYLE_TASK_INFO);
	}};
	private static final String taskStyleEmergency = BrigadesTheme.STYLE_TASK_EMERGENCY;

	private static final LinkedHashMap<String, String> statusIconFileNames = new LinkedHashMap<String, String>() {{
		put(WorkDto.Status.NEW.getCode(), "work_warning.png");
		put(WorkDto.Status.ASSIGNED.getCode(), "work_plan.png");
		put(WorkDto.Status.IN_PROGRESS.getCode(), "work_inwork.png");
		put(WorkDto.Status.COMPLETE.getCode(), "work_success.png");
		put(WorkDto.Status.CLOSED.getCode(), "work_info.png");
	}};
	private static final String statusIconFileNameEmergency = "work_emergency.png";

	private static final LinkedHashMap<String, String> statusColors = new LinkedHashMap<String, String>() {{
		put(WorkDto.Status.NEW.getCode(), "White");
		put(WorkDto.Status.ASSIGNED.getCode(), "Peru");
		put(WorkDto.Status.IN_PROGRESS.getCode(), "LightBlue");
		put(WorkDto.Status.COMPLETE.getCode(), "Green");
		put(WorkDto.Status.CLOSED.getCode(), "LightGray");
	}};
	private static final String statusColorEmergency = "Red";

	private static final LinkedHashMap<String, String> ganttColors = new LinkedHashMap<String, String>() {{
		put(WorkDto.Status.NEW.getCode(), BrigadesTheme.COLOR_NORMAL);
		put(WorkDto.Status.ASSIGNED.getCode(), BrigadesTheme.COLOR_PLAN);
		put(WorkDto.Status.IN_PROGRESS.getCode(), BrigadesTheme.COLOR_INWORK);
		put(WorkDto.Status.COMPLETE.getCode(), BrigadesTheme.COLOR_SUCCESS);
		put(WorkDto.Status.CLOSED.getCode(), BrigadesTheme.COLOR_INFO);
		put(WorkItemDto.Status.NOT_DONE.getCode(), BrigadesTheme.COLOR_INWORK);
		put(WorkItemDto.Status.DONE.getCode(), BrigadesTheme.COLOR_SUCCESS);
	}};
	private static final String ganttColorEmergency = BrigadesTheme.COLOR_EMERGENCY;

	private static String attachmentProvider(WorkDto workOrder) {
		List<AttachmentDto> attachments = BrigadesUI.getRestTemplate().getWorkAttachments(workOrder.getId());
		return attachments.size() > 0 ? "Есть" : "";
	}

	private static String typeWorkProvider(WorkDto workOrder) {
		TypeWorkDto typeWork = BrigadesUI.getRestTemplate().getTypeWork(workOrder.getIdTypeWork());
		return typeWork == null ? null : typeWork.getDescription();
	}

	private static CssLayout createFlag(WorkDto workOrder) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (isEmergency(workOrder)) {
			BrigadesIcons icon = new BrigadesIcons(BrigadesTheme.ICON_ERROR, BrigadesTheme.COLOR_EMERGENCY);
			Label label = new Label(icon.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			//label.addStyleName(BrigadesTheme.STYLE_ET_ICON_EMERGENCY);
			label.setDescription("Нарушены сроки выполнения работы");
			layout.addComponent(label);
			//layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		}
		return layout;
	}

	private static CssLayout createComment(WorkDto workOrder) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (WorkDto.Status.IN_PROGRESS.getCode().equals(workOrder.getStatus())) {
			Label label = new Label(BrigadesTheme.ICON_WORKGROUP.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			label.setDescription("Нахождение бригады на адресе");
			layout.addComponent(label);
			//layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		}
		return layout;
	}

	private static CssLayout createFlagConfirmed(WorkDto workOrder) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		Label label = new Label();
		label.setSizeFull();
		label.setContentMode(ContentMode.HTML);
		label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
		if (workOrder.getStatus() != null) {
			switch (workOrder.getStatus()) {
				case "CONFIRMED":
					label.setValue(BrigadesTheme.ICON_ACCEPTED.getHtml());
					label.setDescription("Подтверждено получение работы бригадой");
					break;
				default:
					label.setValue(BrigadesTheme.ICON_UNKNOWN.getHtml());
					label.setDescription("Получение работы бригадой не подтверждено");
					break;
			}
		}
		layout.addComponent(label);
		return layout;
	}

	private static CssLayout createJournalFlag(WorkJournalDto workJournalDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (isEmergency(workJournalDto)) {
			BrigadesIcons icon = new BrigadesIcons(BrigadesTheme.ICON_ERROR, BrigadesTheme.COLOR_EMERGENCY);
			Label label = new Label(icon.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			label.setDescription("Нарушены сроки выполнения работы");
			layout.addComponent(label);
		}
		return layout;
	}

	private static CssLayout createJournalIcon(WorkJournalDto workJournalDto) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (workJournalDto.getBrigadeOnAddress()) {
			Label label = new Label(BrigadesTheme.ICON_WORKGROUP.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			label.setDescription("Нахождение бригады на адресе");
			layout.addComponent(label);
		}
		return layout;
	}

	public static List<GridColumn<WorkDto>> getMainColumns() {
		return Arrays.asList(FLAG, COMMENT, ADDRESS, CODE, TYPE, DESCRIPTION, EXECUTOR, SOURCE,
				REGION, DISTRICT, ATTACHMENTS, START_PLAN, FINISH_PLAN, START_FACT, FINISH_FACT,
				BREAK_COUNTER, RELATED, REQUEST, TOIR_NUMBER, AIS_NUMBER, CREATE_DATE, SPEC_TECHNIC,
				DISABLE, GATI, GRAPH, SCHEDULE, MOTION_RESTRICTION, MAVR, CREATE_BY_MASTER, BRIGADE_ADDRESS);
	}
	/**
	* Для отчета по работам
	 * {@link ru.telros.brigades.client.view.reports.WorksReportWindow#WorksReportWindow(String)}
	 */
	public static List<GridColumn<WorkDto>> getReportColumns() {

		return Arrays.asList(ADDRESS, CODE, TYPE, DESCRIPTION, SOURCE, BRIGADE_NAME,
				REGION, DISTRICT, ATTACHMENTS, START_PLAN, START_FACT,START_DER, FINISH_PLAN,FINISH_FACT, FINISH_DER, FACT_DURATION,
				REQUEST,  AIS_NUMBER, CREATE_DATE, CREATED_BY, SPEC_TECHNIC,
				 MAVR, CREATE_BY_MASTER);
	}

	public static List<GridColumn<WorkDto>> getRequestColumns() {
		return Arrays.asList(GROUP, NAME, ADDRESS, START_PLAN, FINISH_PLAN, START_FACT, FINISH_FACT);
	}

	public static List<GridColumn<WorkDto>> getWorkGroupColumns() {
		return Arrays.asList(NAME, ADDRESS, START_PLAN, FINISH_PLAN, START_FACT, FINISH_FACT);
	}

	public static List<GridColumn<WorkItemWorkDto>> getTreeGridColumns() {
		return Arrays.asList(WORK_ADDRESS, NAME_, START_PLAN_, FINISH_PLAN_, START_FACT_, FINISH_FACT_);
	}
	//ЕВ попросила убрать эти колонки пока что
	public static List<GridColumn<WorkJournalDto>> getJournalColumns() {
		return Arrays.asList(FLAG_, ICON, ADDRESS_, PROBLEM_CODE, TYPE_, DESCRIPTION_, BRIGADE, SOURCE_, REGION_,
				LOCATION, HAS_ATTACHMENTS, START_DATE_PLAN, FINISH_DATE_PLAN, START_DATE_FACT, FINISH_DATE_FACT,
				/*TIMEOUT, LINKED,*/ NUMBER_REQUEST, NUMBER_TOIR, NUMBER_AIS, REGISTRATION_DATE, EQUIPMENT/*, CONSUMER_OFF,
				/*GATI_WORK, SCHEDULED, REGULATED,*//* TRAFFIC_OFF, MAVR_,*//* CREATE_BY_BRIGADE, BRIGADE_ON_ADDRESS*/);
	}

	/**
	 * Return whether state of WorkOrder is emergency or not
	 *
	 * @param workOrder
	 * @return
	 */
	public static boolean isEmergency(WorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		ZonedDateTime now = ZonedDateTime.now();
		if (workOrder.getFinishDatePlan() != null && workOrder.getFinishDatePlan().isBefore(now))
			if (workOrder.getFinishDate() == null)
				return isWorkStatusEmergency(workOrder.getStatus());
		return false;
	}

	public static boolean isEmergency(WorkItemDto workItem) {
		Objects.requireNonNull(workItem);
		ZonedDateTime now = ZonedDateTime.now();
		if (workItem.getFinishDatePlan() != null && workItem.getFinishDatePlan().isBefore(now))
			if (workItem.getFinishDate() == null)
				return isWorkItemStatusEmergency(workItem.getStatus());
		return false;
	}

	public static boolean isEmergency(WorkItemWorkDto workItem) {
		Objects.requireNonNull(workItem);
		ZonedDateTime now = ZonedDateTime.now();
		if (workItem.getFinishDatePlan() != null && workItem.getFinishDatePlan().isBefore(now))
			if (workItem.getFinishDate() == null)
				return isWorkItemStatusEmergency(workItem.getStatus());
		return false;
	}

	public static boolean isEmergency(WorkJournalDto workJournal) {
		Objects.requireNonNull(workJournal);
		ZonedDateTime now = ZonedDateTime.now();
		if (workJournal.getFinishDatePlan() != null && workJournal.getFinishDatePlan().isBefore(now))
			if (workJournal.getFinishDateFact() == null)
				return isWorkStatusEmergency(workJournal.getStatus());
		return false;
	}

	public static boolean isEmergency(WorkMapDto workMap) {
		Objects.requireNonNull(workMap);
		ZonedDateTime now = ZonedDateTime.now();
		if (workMap.getFinishDatePlan() != null && workMap.getFinishDatePlan().isBefore(now))
			if (workMap.getFinishDateFact() == null)
				return isWorkStatusEmergency(workMap.getStatus());
		return false;
	}

	public static boolean isEmergency(GanttItemDto ganttItem) {
		Objects.requireNonNull(ganttItem);
		ZonedDateTime now = ZonedDateTime.now();
		return (ganttItem.getStartDateFact() == null && ganttItem.getStartDatePlan() != null && ganttItem.getStartDatePlan().isBefore(now)) ||
				(ganttItem.getFinishDateFact() == null && ganttItem.getFinishDatePlan() != null && ganttItem.getFinishDatePlan().isBefore(now));
	}

	public static boolean isEmergency(GanttItemDto ganttItem, Boolean isWork) {
		Objects.requireNonNull(ganttItem);
		if (isWork)
			return isWorkStatusEmergency(ganttItem.getStatus()) && isEmergency(ganttItem);
		else
			return isWorkItemStatusEmergency(ganttItem.getStatus()) && isEmergency(ganttItem);
	}

	public static boolean canDeleteWork(String workStatus) {
		return WorkDto.Status.NEW.getCode().equals(workStatus) || WorkDto.Status.ASSIGNED.getCode().equals(workStatus);
	}

	public static boolean canCloseWork(String workStatus) {
		return WorkDto.Status.COMPLETE.getCode().equals(workStatus);
	}

	public static boolean isWorkStatusEmergency(String workStatus) {
		return !WorkDto.Status.CLOSED.getCode().equals(workStatus) && !WorkDto.Status.COMPLETE.getCode().equals(workStatus);
	}

	public static boolean isWorkItemStatusEmergency(String itemStatus) {
		return !WorkItemDto.Status.DONE.getCode().equals(itemStatus);
	}

	public static String getEventStyle(WorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getEventStyle(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getEventStyle(WorkItemWorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getEventStyle(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getEventStyle(WorkJournalDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getEventStyle(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getEventStyle(String status, boolean isEmergency) {
		return isEmergency ? workStyleEmergency : workStyles.get(status);
	}

	public static String getTaskStyle(String status, boolean isEmergency) {
		return isEmergency ? taskStyleEmergency : taskStyles.get(status);
	}

	public static String getStatusIconFileName(WorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getStatusIconFileName(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getStatusIconFileName(String status, boolean isEmergency) {
		return isEmergency ? statusIconFileNameEmergency : statusIconFileNames.get(status);
	}

	public static String getStatusColor(WorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getStatusColor(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getStatusColor(String status, boolean isEmergency) {
		return isEmergency ? statusColorEmergency : statusColors.get(status);
	}

	public static String getGanttColor(WorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getGanttColor(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getGanttColor(WorkItemDto workItem) {
		Objects.requireNonNull(workItem);
		return getGanttColor(workItem.getStatus(), isEmergency(workItem));
	}

	public static String getGanttColor(GanttItemDto ganttItem, Boolean isWork) {
		Objects.requireNonNull(ganttItem);
		return getGanttColor(ganttItem.getStatus(), isEmergency(ganttItem, isWork));
	}

	public static String getGanttColor(String status, boolean isEmergency) {
		return isEmergency ? ganttColorEmergency : ganttColors.get(status);
	}

	public static String getStatusMsg(String status, boolean isEmergency) {
		//return isEmergency ? msg("workorder.status.emergency") : msg(status.getMsg());
		return isEmergency ? msg("workorder.status.emergency") : status;
	}

	public static String getStatusDescriptionHtml(WorkDto workOrder) {
		Objects.requireNonNull(workOrder);
		return getStatusDescriptionHtml(workOrder.getStatus(), isEmergency(workOrder));
	}

	public static String getStatusDescriptionHtml(String status, boolean isEmergency) {
		return ("&nbsp;<SPAN class='")
				.concat(getEventStyle(status, isEmergency))
				.concat("'>&nbsp;")
				.concat(getStatusMsg(status, isEmergency))
				.concat("&nbsp;</SPAN>");
	}

	/**
	 * Поиск этапа/работы по ид-ру в коллекции
	 *
	 * @param workOrders
	 * @param id
	 * @return
	 */
	public static WorkDto findById(Collection<WorkDto> workOrders, Long id) {
		for (WorkDto workOrder : workOrders) {
			if (workOrder.getId().equals(id)) {
				return workOrder;
			}
		}
		return null;
	}

	public static List<WorkItemWorkDto> createMainWorks(List<WorkDto> works) {
		//(ADDRESS, NAME, START_PLAN, FINISH_PLAN, START_FACT, FINISH_FACT, FLAG_CONFIRMED);
		List<WorkItemWorkDto> result = new ArrayList<>();
		for (WorkDto work : works) {
			WorkItemWorkDto workItemWorkDto = new WorkItemWorkDto();
			workItemWorkDto.setId(work.getId());
			workItemWorkDto.setCode(work.getCode());
			workItemWorkDto.setAddress(work.getAddress());
			workItemWorkDto.setDescription(work.getDescription());
			workItemWorkDto.setStatus(work.getStatus());
			workItemWorkDto.setStartDatePlan(work.getStartDatePlan());
			workItemWorkDto.setFinishDatePlan(work.getFinishDatePlan());
			workItemWorkDto.setStartDate(work.getStartDate());
			workItemWorkDto.setFinishDate(work.getFinishDate());
			result.add(workItemWorkDto);
		}
		return result;
	}

	public static List<WorkItemWorkDto> createWorkStages(List<WorkItemDto> stages) {
		//(ADDRESS, NAME, START_PLAN, FINISH_PLAN, START_FACT, FINISH_FACT, FLAG_CONFIRMED);
		List<WorkItemWorkDto> result = new ArrayList<>();
		for (WorkItemDto stage : stages) {
			WorkItemWorkDto workDto = new WorkItemWorkDto();
			workDto.setId(stage.getId());
			workDto.setCode(stage.getCode());
			workDto.setDescription(stage.getDescription());
			workDto.setStatus(stage.getStatus());
			workDto.setStartDatePlan(stage.getStartDatePlan());
			workDto.setFinishDatePlan(stage.getFinishDatePlan());
			workDto.setStartDate(stage.getStartDate());
			workDto.setFinishDate(stage.getFinishDate());
			result.add(workDto);
		}
		return result;
	}

	public static List<WorkItemDto> createWorkItemStages(List<TypeWorkItemDto> stages) {
		List<WorkItemDto> result = new ArrayList<>();
		for (TypeWorkItemDto item : stages) {
			// инициализация данных этапа
			WorkItemDto stage = new WorkItemDto();
			//stage.setStatus("Новая"); // статус
			//stage.setId(item.getId());
			stage.setCode(item.getCode());
			stage.setDescription(item.getDescription()); // название
			stage.setStartDatePlan(null);
			stage.setFinishDatePlan(null);
			stage.setCallRequire(item.getCallRequire());
			stage.setActRequire(item.getActRequire());
			stage.setPhotoRequire(item.getPhotoRequire());
			stage.setDuration(item.getNorm());
			result.add(stage);
		}
		return result;
	}
}
