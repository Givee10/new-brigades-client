package ru.telros.brigades.client.ui.workorder;

import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Responsive;
import com.vaadin.server.SerializablePredicate;
import com.vaadin.server.Setter;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.event.UpdateViewEvent;
import ru.telros.brigades.client.event.workorder.ChangeWorkOrderEvent;
import ru.telros.brigades.client.event.workorder.UpdateWorkOrdersEvent;
import ru.telros.brigades.client.ui.attachment.AttachmentPanel;
import ru.telros.brigades.client.ui.component.*;
import ru.telros.brigades.client.ui.request.CommentWindow;
import ru.telros.brigades.client.ui.request.CommentaryGrid;
import ru.telros.brigades.client.ui.request.RequestConstants;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.vehicle.WorkOrderVehiclePanel;
import ru.telros.brigades.client.ui.window.GeocodeWindow;
import ru.telros.brigades.client.ui.window.WindowWithButtons;
import ru.telros.brigades.client.ui.window.YesNoWindow;
import ru.telros.brigades.client.ui.workgroup.WorkGroupLayout;
import ru.telros.brigades.client.view.BrigadeAdviserView;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static ru.telros.brigades.client.MessageManager.msg;
import static ru.telros.brigades.client.ui.grid.GridColumn.dateGetter;
import static ru.telros.brigades.client.ui.grid.GridColumn.dateSetter;

public class WorkOrderWindow extends WindowWithButtons implements HasLogger {
	private static final int WIDTH_PIXELS = 1200;
	private static final int HEIGHT_PIXELS = -1;

	private Boolean isNew;
	private Boolean isRequestAttached = false;
	private WorkDto workOrder;
	private Binder<WorkDto> binder = new Binder<>();
	private AttachmentPanel attachmentPanel;
	private CommentaryGrid commentaryGrid;
	//private WorkOrderRequestInfoPanel requestInfoPanel;
	//private WorkOrderWorkGroupPanel workGroupPanel;
	//private WorkOrderStagesPanel stagesPanel;
	private WorkOrderVehiclePanel vehiclePanel;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	private RequestDto request;

	private DisabledTextField workOrderNum = new DisabledTextField(msg("table.workorder.workOrderNum"));
	private DisabledTextField statusField = new DisabledTextField(msg("table.workorder.status"));
	private CustomTextField addressField = new CustomTextField(msg("table.workorder.address"));
	private CustomTextField addressDescription = new CustomTextField("Уточнение адреса");
	private DisabledTextField authorField = new DisabledTextField("Создана");
	//private DisabledTextField workItemTextField = new DisabledTextField("Вид работы");
	private CustomTextArea description = new CustomTextArea(msg("table.workorder.description"));
	private CustomDateTimeField startDatePlan = new CustomDateTimeField(msg("table.workorder.startDatePlan"));
	private CustomDateTimeField finishDatePlan = new CustomDateTimeField(msg("table.workorder.finishDatePlan"));
	private CustomDateTimeField startDateFact = new CustomDateTimeField(msg("table.workorder.startDateFact"));
	private CustomDateTimeField finishDateFact = new CustomDateTimeField(msg("table.workorder.finishDateFact"));
	private CheckBox immediately = new CheckBox(msg("table.workorder.immediately"));
	private CheckBox orderGATI = new CheckBox("Ордер ГАТИ");
	private CheckBox graph = new CheckBox("График");
	private CheckBox regulations = new CheckBox("Регламент");
	private CheckBox disconnection = new CheckBox("Отключение потребителей");
	private CheckBox motion = new CheckBox("Ограничение движения автотранспорта");
	private CustomComboBox<TypeWorkDto> workItemComboBox = new CustomComboBox<>("Вид работы");
	private CustomComboBox<BrigadeDto> workGroupComboBox = new CustomComboBox<>("Бригада");
	private CustomComboBox<OrganizationDto> organizationComboBox = new CustomComboBox<>("РВ");
	private List<OrganizationDto> organizations = restTemplate.getOrganizations();

	private Button addressCommit = new Button("Привязка к карте");
	private Boolean addressChanged = false;

	private CustomComboBox<RequestDto> sourceComboBox = new CustomComboBox<>(msg("table.request.source"));
	private Button clearRequestSelectionButton = new Button(BrigadesTheme.ICON_CLOSE);
	private Button openRequestButton = new Button(msg("button.open"));
	private DisabledTextField numberTextField = new DisabledTextField("Номер");
	private DisabledTextField typeTextField = new DisabledTextField(msg("table.request.type"));
	private DisabledTextArea descriptionTextArea = new DisabledTextArea(msg("table.request.description"));
	private DisabledTextField sourceTextField = new DisabledTextField("Источник для ТОиР");
	private DisabledTextField methodTextField = new DisabledTextField("Способ формирования");
	private DisabledTextField workTextField = new DisabledTextField("Связанные работы");

	private WorkOrderTreeGrid workOrderTreeGrid = new WorkOrderTreeGrid(msg("workorderwindow.assignee.workplan.caption"));
	private WorkItemTreeGrid itemTreeGrid = new WorkItemTreeGrid(WorkItemColumn.getStagesColumns());
	private TimeField norm;

	private Map<Class, Long> map = new HashMap<>();

	public WorkOrderWindow(WorkDto workOrder, RequestDto requestDto) {
		super("Работа", WIDTH_PIXELS, HEIGHT_PIXELS);
		this.workOrder = workOrder;
		this.request = requestDto;
		this.isNew = workOrder.getId() == null;
		List<AttachmentDto> workAttachments = new ArrayList<>();
		List<EquipmentWorkDto> workEquipments = new ArrayList<>();
		List<CommentDto> workComments = new ArrayList<>();
		if (!isNew) {
			workEquipments.addAll(restTemplate.getWorkEquipments(workOrder.getId()));
			workAttachments.addAll(restTemplate.getWorkAttachments(workOrder.getId()));
			//workComments.addAll(restTemplate.getWorkComments(workOrder.getId()));
			request = restTemplate.getWorkRequest(workOrder.getId());
		}
		map.put(WorkDto.class, workOrder.getId());
		this.attachmentPanel = new AttachmentPanel(workAttachments, map);
		this.vehiclePanel = new WorkOrderVehiclePanel(workOrder, workEquipments);
		this.commentaryGrid = new CommentaryGrid(RequestConstants.getCommentColumns(), workComments);
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		buildFields();
		bindFields();

		removeAllCloseShortcuts();
		addCloseListener(closeEvent -> BrigadesEventBus.unregister(this));
	}

	public static void open(final WorkDto workOrder, final RequestDto requestDto) {
		Window window = new WorkOrderWindow(workOrder, requestDto);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	private void buildFields() {
		addToContent(buildMainContent());
		addToContent(buildTabSheet());

		addToButtonBar("Закрыть работу в РВ", $ -> closeWorkOrder());
		addToButtonBar("Проверить работу, созданную мастером", $ -> checkWorkOrder());
		addToButtonBar("Создать связанную работу", $ -> addWorkOrder());
		addSpaceToButtonBar();
		addToButtonBar(msg("button.save"), $ -> save());
		addToButtonBar(msg("button.close"), $ -> close());
		setButtonEnabled(2, false);
		setButtonVisible(1, false);
		setButtonVisible(2, false);
	}

	private Component buildMainContent() {
		addressCommit.addClickListener(event -> {
			GeocodeWindow geocodeWindow = GeocodeWindow.open(workOrder.getAddress());
			geocodeWindow.addCloseListener(closeEvent -> {
				if (geocodeWindow.isModalResult()) {
					GeocodeDto geocodeDto = geocodeWindow.getAddress();
					addressChanged = true;
					addressField.setValue(geocodeDto.getName());
				}
			});
		});
		addressField.addValueChangeListener(valueChangeEvent -> {
			addressCommit.setIcon(valueChangeEvent.getValue() != null && addressChanged ? BrigadesTheme.ICON_OK : BrigadesTheme.ICON_WARNING);
			addressChanged = false;
		});
		startDatePlan.addValueChangeListener(event -> {
			if (event.isUserOriginated()) {
				updateStages();
				updateStagesTime(returnWorkItems(), DatesUtil.localToZoned(event.getValue()));
			}
		});

		Grid.Column<WorkItemDto, ?> actColumn = itemTreeGrid.getColumn(WorkItemColumn.REQUIRED_ACT.getId());
		Grid.Column<WorkItemDto, ?> phoneColumn = itemTreeGrid.getColumn(WorkItemColumn.REQUIRED_PHONE.getId());
		Grid.Column<WorkItemDto, ?> photoColumn = itemTreeGrid.getColumn(WorkItemColumn.REQUIRED_PHOTO.getId());
		itemTreeGrid.getDefaultHeaderRow().getCell(actColumn).setDescription("Действие");
		itemTreeGrid.getDefaultHeaderRow().getCell(phoneColumn).setDescription("Звонок");
		itemTreeGrid.getDefaultHeaderRow().getCell(photoColumn).setDescription("Фотография");

		workItemComboBox.setItems(restTemplate.getTypeWorks());
		workItemComboBox.addValueChangeListener(valueChangeEvent -> {
			if (valueChangeEvent.isUserOriginated()) {
				TypeWorkDto value = workItemComboBox.getValue();
				description.setValue(value.getDescription());
				description.focus();

				updateStages();
				List<TypeWorkItemDto> workItems = restTemplate.getTypeWorkItems(value.getId());
				List<WorkItemDto> stages = WorkOrderConstants.createWorkItemStages(workItems);
				updateStagesTime(stages, returnStartDatePlan());
			}
		});
		immediately.addValueChangeListener(valueChangeEvent -> {
			if (valueChangeEvent.isUserOriginated() && valueChangeEvent.getValue()) {
				startDatePlan.setValue(LocalDateTime.now().plusMinutes(15));
				updateStages();
				updateStagesTime(returnWorkItems(), returnStartDatePlan());
			}
		});

		GridLayout layout = new GridLayout(3, 6);
		layout.setSizeFull();
		layout.setSpacing(true);
		layout.setMargin(false);
		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(workOrderNum), 0, 0);

		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(statusField), 1, 0);

		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(workItemComboBox), 0, 1);
		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(description), 1, 1);

		HorizontalLayout addressLayout = CompsUtil.getHorizontalWrapperNoMargin(addressField, addressCommit);
		addressLayout.setComponentAlignment(addressCommit, Alignment.BOTTOM_LEFT);
		layout.addComponent(addressLayout, 0, 2, 1, 2);

		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(addressDescription), 0, 3);
		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(authorField), 1, 3);

		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(startDatePlan), 0, 4);
		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(finishDatePlan), 1, 4);

		layout.addComponent(CompsUtil.getVerticalWrapperNoMargin(immediately), 0, 5);

		//requestInfoPanel = new WorkOrderRequestInfoPanel(workOrder, this);
		layout.addComponent(buildSourcePanel(), 2, 0, 2, 5);

		layout.setColumnExpandRatio(0, 1);
		layout.setColumnExpandRatio(1, 1);
		layout.setColumnExpandRatio(2, 1);
		return layout;
	}

	private Component buildSourcePanel() {
		List<RequestDto> requests = restTemplate.getRequests();
		//sourceComboBox.setItemCaptionGenerator(RequestDto::getDescription);
		sourceComboBox.setItems(requests);
		sourceComboBox.setPlaceholder("Выберите заявку");
		sourceComboBox.setPopupWidth("250%");
		addWindowModeChangeListener(event -> sourceComboBox.setPopupWidth(
				event.getWindowMode().equals(WindowMode.NORMAL) ? "250%" : "100%"));
		sourceComboBox.addValueChangeListener($ -> onChangeRequest());
		if (workOrder != null && request != null) {
			if (!isNew) isRequestAttached = true;
			sourceComboBox.setValue(request);
		}
		clearRequestSelectionButton.addClickListener((Button.ClickListener) $ -> onClearRequest());
		clearRequestSelectionButton.setDescription(msg("button.clearfield.description", new Object[]{sourceComboBox.getCaption()}));
//		clearRequestSelectionButton.setEnabled(!sourceComboBox.isEmpty());
		clearRequestSelectionButton.setEnabled(true);

		openRequestButton.addClickListener((Button.ClickListener) $ -> onOpenRequest());
//		openRequestButton.setEnabled(!sourceComboBox.isEmpty());
		openRequestButton.setEnabled(true);

		HorizontalLayout buttons = CompsUtil.getHorizontalWrapperNoMargin(clearRequestSelectionButton, openRequestButton);
		buttons.setWidthUndefined();
		//GridLayout line = new GridLayout(2,1);
		HorizontalLayout line = new HorizontalLayout();
		line.addComponent(sourceComboBox);
		line.addComponent(buttons);
		line.setComponentAlignment(buttons, Alignment.BOTTOM_LEFT);
		line.setExpandRatio(sourceComboBox, 1);
		line.setSpacing(true);
		line.setSizeFull();
		HorizontalLayout line2 = CompsUtil.getHorizontalWrapperNoMargin(numberTextField, typeTextField);
		HorizontalLayout line3 = CompsUtil.getHorizontalWrapperNoMargin(/*sourceTextField,*/ methodTextField);
		VerticalLayout layout = CompsUtil.getVerticalWrapperWithMargin(line, line2, descriptionTextArea, line3/*, workTextField*/);
		Panel groupWrapper = CompsUtil.getGroupWrapper("Информация об источнике работы", CompsUtil.getVerticalWrapperNoMargin(layout));
		return CompsUtil.getVerticalWrapperNoMargin(groupWrapper);
	}

	private void onChangeRequest() {
		request = sourceComboBox.getValue();
		if (request != null) {
			addressField.setValue(request.getAddress());
			addressDescription.setValue(request.getUpdatedAddress());
			numberTextField.setValue(request.getExternalNumber() != null ? request.getExternalNumber() : request.getNumber());
			//sourceTextField.setValue(request.getSource());
			typeTextField.setValue(request.getProblemCode());
			descriptionTextArea.setValue(request.getDescription());
		} else {
			isRequestAttached = false;
			addressField.clear();
			addressDescription.clear();
			numberTextField.clear();
			sourceTextField.clear();
			typeTextField.clear();
			descriptionTextArea.clear();
		}
		clearRequestSelectionButton.setEnabled(!sourceComboBox.isEmpty());
//		clearRequestSelectionButton.setEnabled(true);
		openRequestButton.setEnabled(!sourceComboBox.isEmpty());
//		openRequestButton.setEnabled(true);
	}

	private void onClearRequest() {
		sourceComboBox.clear();
	}

	private void onOpenRequest() {
		RequestLayout.showRequest(request);
	}

	private Component buildTabSheet() {
		TabSheet tabSheet = new TabSheet();
		tabSheet.addStyleName(BrigadesTheme.TABSHEET_FRAMED);

		tabSheet.addTab(buildBrigadePanel(), "Исполнитель");
		tabSheet.addTab(buildStagesPanel(), "Детали работы");
		tabSheet.addTab(attachmentPanel, "Приложения");
		tabSheet.addTab(vehiclePanel, "Спецтехника");
		tabSheet.addTab(buildCommentTab(), "Комментарии").setEnabled(false);
		//tabSheet.addTab(buildGATITab(), "ГАТИ").setEnabled(false);
		tabSheet.addTab(buildOtherTab(), "Другое").setEnabled(false);

		return tabSheet;
	}

	private Component buildStagesPanel() {
		itemTreeGrid.setHeight(300, Unit.PIXELS);
		itemTreeGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
		itemTreeGrid.addSelectionListener($ -> changeNorm());
		if (!isNew)
			itemTreeGrid.setItems(restTemplate.getWorkItems(workOrder.getId()));

		CustomButton moveStageUpButton = new CustomButton(BrigadesTheme.ICON_MOVE_UP, msg("button.up"), $ -> moveStage(true));
		CustomButton moveStageDownButton = new CustomButton(BrigadesTheme.ICON_MOVE_DOWN, msg("button.down"), $ -> moveStage(false));
		CustomButton deleteStageButton = new CustomButton(BrigadesTheme.ICON_DELETE, msg("button.delete"), $ -> deleteStage());
		CustomButton editStageButton = new CustomButton(BrigadesTheme.ICON_OK, msg("button.edit"), $ -> editItem());
		CustomButton calcStageButton = new CustomButton(BrigadesTheme.ICON_CALC, "Рассчитать значения", $ -> calcValues());

		norm = new TimeField(msg("workorderwindow.stages.edit.stageTimeCaption"));

		return CompsUtil.buildGridWithButtons(itemTreeGrid, moveStageUpButton, moveStageDownButton, deleteStageButton, norm, editStageButton, calcStageButton);
	}

	private void moveStage(boolean up) {
		WorkItemDto row = itemTreeGrid.getSelectedRow();
		if (row != null) {
			//if (row.getRequired()) {
			//	Notification.show(msg("workorderwindow.stages.shift.stageShiftDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
			//} else {
			int intItem = returnWorkItems().indexOf(row);
			int changeItem = up ? intItem - 1 : intItem + 1;
			if (changeItem > 0 && changeItem != returnWorkItems().size()) {
				//WorkItemDto changeRow = stagesTable.getAllItems().get(changeItem);
				//if (changeRow.getRequired()) {
				//	Notification.show(msg("workorderwindow.stages.shift.stageShiftDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
				//} else {
				List<WorkItemDto> stages = new ArrayList<>(returnWorkItems());
				Collections.swap(stages, intItem, changeItem);
				updateStagesTime(stages, returnStartDatePlan());
				itemTreeGrid.select(row);
				//}
			} else {
				Notification.show(msg("workorderwindow.stages.shift.stageShiftDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
			}
			//}
		} else {
			Notification.show(msg("workorderwindow.stages.stageNotSelectedMessage"), Notification.Type.WARNING_MESSAGE);
		}
	}

	private void deleteStage() {
		WorkItemDto row = itemTreeGrid.getSelectedRow();
		if (row != null) {
			//if (row.getAttributes().contains("REQUIRED")) {
			//	Notification.show(msg("workorderwindow.stages.delete.stageDeletionDisallowedMessage"), Notification.Type.ERROR_MESSAGE);
			//} else {
			String prompt = "Вы действительно хотите удалить этап " + row.getDescription() + "?";
			YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
			confirmationDialog.addCloseListener((Window.CloseListener) e -> {
				if (((YesNoWindow)e.getWindow()).isModalResult()) {
					int intItem = returnWorkItems().indexOf(row);
					int nextItem = intItem + 1;
					WorkItemDto nextRow = returnWorkItems().get(nextItem);
					returnWorkItems().remove(row);
					updateStagesTime(returnWorkItems(), returnStartDatePlan());
					itemTreeGrid.select(nextRow);
					Notification.show(msg("workorderwindow.stages.delete.stageDeletedMessage",
							new String[] { row.getDescription() }), Notification.Type.WARNING_MESSAGE);
				}
			});
			//}
		} else {
			Notification.show(msg("workorderwindow.stages.stageNotSelectedMessage"), Notification.Type.WARNING_MESSAGE);
		}
	}

	private void editItem() {
		WorkItemDto row = itemTreeGrid.getSelectedRow();
		if (row != null) {
			try {
				//getLogger().debug("Value: {}, Converted: {}", norm.getValue(), norm.convertValue());
				ZonedDateTime endDate = row.getStartDatePlan().plusMinutes(norm.convertValue());
				row.setFinishDatePlan(endDate);
				updateStagesTime(returnWorkItems(), returnStartDatePlan());
				changeNorm();
			} catch (Throwable throwable) {
				Notification.show(msg("workorderwindow.stages.edit.stageWrongTimeFormatMessage"), Notification.Type.ERROR_MESSAGE);
			}
		} else {
			Notification.show(msg("workorderwindow.stages.stageNotSelectedMessage"), Notification.Type.WARNING_MESSAGE);
		}
	}

	private void changeNorm() {
		WorkItemDto selectedRow = itemTreeGrid.getSelectedRow();
		if (selectedRow != null) {
			ZonedDateTime startDatePlan = selectedRow.getStartDatePlan();
			ZonedDateTime finishDatePlan = selectedRow.getFinishDatePlan();
			if (startDatePlan != null && finishDatePlan != null)
				norm.setValue(startDatePlan, finishDatePlan);
		} else {
			norm.clear();
		}
	}

	private void calcValues() {
		TypeWorkDto value = workItemComboBox.getValue();
		if (value != null) TOIRWindow.open(value, returnStartDatePlan());
		else Notification.show("Необходимо выбрать Вид работы", Notification.Type.ERROR_MESSAGE);
	}

	private void updateStagesTime(final Collection<WorkItemDto> stages, ZonedDateTime startDate) {
		if (stages == null || stages.size() == 0)
			return;

		List<WorkItemDto> resultStages = new ArrayList<>(stages);
		//long startTime = DatesUtil.localDateTimeToLong(startDate);
		//long endTime = 0;
		Long deltaMinutes;
		ZonedDateTime endDate = null;

		for (WorkItemDto stage : resultStages) {
			// по умолчанию длительность этапа - 0.5 часа
			deltaMinutes = 0L;
			// определяем длину этапа
			if (stage.getStartDatePlan() != null && stage.getFinishDatePlan() != null) {
				// если время у этапа присутствует, значит берем разницу во времени из его данных
				// т.к. время этапа могло быть отредактировано вручную
				//Long startFact = DatesUtil.localDateTimeToDate(stage.getStartDatePlan()).getTime();
				//Long finishFact = DatesUtil.localDateTimeToDate(stage.getFinishDatePlan()).getTime();
				deltaMinutes = ChronoUnit.MINUTES.between(stage.getStartDatePlan(), stage.getFinishDatePlan());
			} else if (stage.getDuration() != null) {
				// если какое либо время null, то взять по нормативу
				deltaMinutes = new Double(stage.getDuration() * 60).longValue();
			}
			// получаем конечное время
			endDate = startDate.plusMinutes(deltaMinutes);
			//endTime = (long) ((double) startTime + deltaMinutes);
			// обновляем время и статус у этапа
			stage.setStartDatePlan(startDate);
			stage.setFinishDatePlan(endDate);
			stage.setStartDate(null);
			stage.setFinishDate(null);
			//stage.setStatus("Новая");
			// сохраняем начальное время
			startDate = endDate;
		}
		if (endDate == null) return;
		// Обновление этапов работы
		itemTreeGrid.setItems(resultStages);
	}

	private Component buildBrigadePanel() {
		final VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		layout.setSpacing(true);

		DisabledTextField workGroupManager = new DisabledTextField(msg("table.workgroup.manager"));
		DisabledTextField workGroupManagerPhone = new DisabledTextField("Телефон мастера");

		final HorizontalLayout line = new HorizontalLayout();
		line.setWidth(100, Unit.PERCENTAGE);
		line.setSpacing(true);

		organizationComboBox.setItems(organizations);
		organizationComboBox.setItemCaptionGenerator(OrganizationDto::getName);
		organizationComboBox.setEnabled(false);

		List<BrigadeDto> workGroups = restTemplate.getBrigades();
		workGroupComboBox.setEmptySelectionAllowed(true);
		workGroupComboBox.setItemCaptionGenerator(BrigadeDto::getName);
		workGroupComboBox.setItems(workGroups);
		workGroupComboBox.addValueChangeListener(e -> {
			BrigadeDto workGroup = e.getValue();
			workOrderTreeGrid.clearItems();
			workGroupManager.clear();
			workGroupManagerPhone.clear();
			if (workGroup != null) {
				updateBrigadeWorks(workGroup);
				EmployeeBrigadeDto manager = restTemplate.getBrigadeMaster(workGroup.getId());
				if (manager != null) {
					workGroupManager.setValue(manager.getFullName());
					workGroupManagerPhone.setValue(manager.getPhone());
				}
			}
		});

		workOrderTreeGrid.setHeight(200, Unit.PIXELS);
		line.addComponent(organizationComboBox);
		line.addComponent(workGroupComboBox);
		line.addComponent(workGroupManager);
		line.addComponent(workGroupManagerPhone);

		Button planButton = new Button("План бригады", clickEvent -> {
			BrigadeDto brigadeDto = workGroupComboBox.getValue();
			if (brigadeDto != null)
				WorkGroupLayout.showPlan(brigadeDto);
		});
		Button chooseButton = new Button("Выбор бригады («советчик»)");
		chooseButton.addClickListener(clickEvent -> {
			BrigadeAdviserView brigadeAdviserView = new BrigadeAdviserView(workOrder);
		});
		chooseButton.setEnabled(true);
		HorizontalLayout buttons = CompsUtil.getHorizontalWrapperNoMargin(planButton, chooseButton);
		buttons.setComponentAlignment(planButton, Alignment.MIDDLE_CENTER);
		buttons.setComponentAlignment(chooseButton, Alignment.MIDDLE_CENTER);
		layout.addComponent(line);
		layout.addComponent(buttons);
		layout.addComponent(workOrderTreeGrid);
		return layout;
	}

	private Component buildCommentTab() {
		commentaryGrid.setHeight(200, Unit.PIXELS);

		CustomButton addButton = new CustomButton(BrigadesTheme.ICON_ADD, msg("button.add"), $ -> addCommentary());
		CustomButton editButton = new CustomButton(BrigadesTheme.ICON_EDIT, msg("button.edit"), $ -> editCommentary());

		return CompsUtil.buildGridWithButtons(commentaryGrid, addButton, editButton);
	}

	private Component buildGATITab() {
		return CompsUtil.getHorizontalWrapperNoMargin(orderGATI, graph, regulations);
	}

	private Component buildOtherTab() {
		return CompsUtil.getHorizontalWrapperNoMargin(disconnection, motion);
	}

	private void addCommentary() {
		if (isNew)
			Notification.show("Комментарий можно добавить только после сохранения работы", Notification.Type.ERROR_MESSAGE);
		else {
			CommentDto commentDto = new CommentDto();
			String fullName = BrigadesUI.getCurrentUser().getFullName();
			commentDto.setSource(fullName);
			commentDto.setRegisteredBy(fullName);
			CommentWindow.open(map, commentDto);
		}
	}

	private void editCommentary() {
		CommentDto selectedRow = commentaryGrid.getSelectedRow();
		if (selectedRow != null) {
			CommentWindow.open(map, selectedRow);
		}
	}

	private void updateStages() {
		TypeWorkDto value = workItemComboBox.getValue();
		ZonedDateTime endDate = null;
		if (value != null && value.getNorm() != null)
			endDate = returnStartDatePlan().plusMinutes((long) (60 * value.getNorm()));
		updateFinishDatePlan(endDate);
	}

	private ZonedDateTime returnStartDatePlan() {
		if (startDatePlan.getValue() == null) {
			startDatePlan.setValue(DatesUtil.zonedToLocal(ZonedDateTime.now()));
		}
		return DatesUtil.localToZoned(startDatePlan.getValue());
	}

	private List<WorkItemDto> returnWorkItems() {
		return itemTreeGrid.getAllItems();
	}

	private void updateFinishDatePlan(ZonedDateTime endDate) {
		finishDatePlan.setValue(DatesUtil.zonedToLocal(endDate));
		finishDatePlan.setRangeStart(startDatePlan.getValue());
	}

	private void checkWorkOrder() {
		initRestValues();
		getLogger().debug("Binder is valid: " + binder.validate().isOk());
		getLogger().debug(StringUtil.writeValueAsString(binder.getBean()));
	}

	private void addWorkOrder() {
		//WorkOrderLayout.createWorkOrder();
	}

	private void closeWorkOrder() {
		if (WorkOrderConstants.canCloseWork(workOrder.getStatus())) {
			String prompt = "Вы действительно хотите закрыть работу " + workOrder.getNumber() + " в РВ?";
			YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
			confirmationDialog.addCloseListener((Window.CloseListener) e -> {
				if (((YesNoWindow) e.getWindow()).isModalResult()) {
					WorkDto workDto = restTemplate.changeWorkStatus(workOrder.getId(), WorkDto.Status.CLOSED.getCode());
					if (workDto != null) {
						Notification.show("Работа " + workDto.getNumber() + " закрыта в РВ");
						setBeanAndFields(workDto);
						WorkOrderLayout.refresh();
						//close();
					} else {
						Notification.show("Произошла ошибка при смене статуса работы", Notification.Type.ERROR_MESSAGE);
					}
				}
			});
		} else {
			Notification.show(msg("error"), "Невозможно закрыть работу " + workOrder.toString(), Notification.Type.ERROR_MESSAGE);
		}
	}

	private void save() {
		if (WorkDto.Status.CLOSED.getCode().equalsIgnoreCase(workOrder.getStatus())) {
			Notification.show("Изменения закрытой работы невозможны", Notification.Type.ERROR_MESSAGE);
			return;
		}
		initRestValues();
		if (binder.validate().isOk()) {
			getLogger().debug("Before save: " + StringUtil.writeValueAsString(binder.getBean()));
			WorkDto savedWork = restTemplate.saveWork(binder.getBean());
			if (savedWork != null) {
				//RequestDto request = requestInfoPanel.getRequest();
				WorkDto changedStatus;
				Long workId = savedWork.getId();
				/*if (isNew) {
					changedStatus = restTemplate.changeWorkStatus(workId, WorkDto.Status.NEW.getCode());
					if (changedStatus != null)
						savedWork = changedStatus;
				}*/
				if (request != null && !isRequestAttached) {
					restTemplate.attachWorkToRequest(savedWork, request);
					restTemplate.changeRequestStatus(request.getId(), RequestDto.Status.IN_WORK.getCode());
				}
				BrigadeDto brigade = workGroupComboBox.getValue();
				if (brigade != null) {
					restTemplate.attachWorkToBrigade(savedWork, brigade);
					if (WorkDto.Status.NEW.getCode().equals(savedWork.getStatus())) {
						changedStatus = restTemplate.changeWorkStatus(workId, WorkDto.Status.ASSIGNED.getCode());
						if (changedStatus != null)
							savedWork = changedStatus;
					}
					/*Long brigadeId = brigade.getId();
					List<BrigadePlanDto> brigadePlans = restTemplate.getBrigadePlans(brigadeId);
					if (brigadePlans.isEmpty()) {
						BrigadePlanDto brigadePlanDto = new BrigadePlanDto();
						brigadePlanDto.setIdBrigade(brigadeId);
						brigadePlanDto.setMavr(false);
						brigadePlanDto.setPhone(brigade.getPhone());
						brigadePlanDto.setBeginDate(DatesUtil.setHourInDate(ZonedDateTime.now(), 8));
						brigadePlanDto.setEndDate(DatesUtil.setHourInDate(ZonedDateTime.now(), 20));
						BrigadePlanDto updatedPlan = restTemplate.saveBrigadePlan(brigadeId, brigadePlanDto);
						if (updatedPlan != null)
							getLogger().debug(updatedPlan.debugInfo());
					}*/
				}
				List<WorkItemDto> stages = returnWorkItems();
				if (isNew) {
					restTemplate.saveWorkItems(stages, workId);
				} else {
					restTemplate.updateWorkItems(stages, workId);
				}
				getLogger().debug("After save:  " + StringUtil.writeValueAsString(savedWork));
				setBeanAndFields(savedWork);
				WorkOrderLayout.refresh();
				Notification.show("Работа сохранена", Notification.Type.TRAY_NOTIFICATION);
				//close();
				return;
			}
		}
		Notification.show("Ошибка при сохранении данных", Notification.Type.ERROR_MESSAGE);
	}

	private void initRestValues() {
		workOrder.setTrafficOff(false);
		workOrder.setConsumerOff(false);
		BrigadeDto brigadeDto = workGroupComboBox.getValue();
		if (brigadeDto != null) workOrder.setIdBrigade(brigadeDto.getId());
		//RequestDto request = requestInfoPanel.getRequest();
		if (request != null) {
			workOrder.setCode(request.getProblemCode());
			workOrder.setSource("Журнал заявок");
		} else workOrder.setSource("Иное");
	}

	private void setBeanAndFields(WorkDto workDto) {
		binder.setBean(workDto);
		workOrder = binder.getBean();
		isNew = workOrder.getId() == null;

		boolean enabled = !WorkDto.Status.CLOSED.getCode().equalsIgnoreCase(workOrder.getStatus());
		setButtonEnabled(0, enabled && !isNew);
		setButtonEnabled(1, enabled && !isNew);
		setButtonEnabled(4, enabled);
		clearRequestSelectionButton.setEnabled(enabled);
//		clearRequestSelectionButton.setEnabled(true);
		sourceComboBox.setEnabled(enabled);
//		sourceComboBox.setEnabled(true);
		workItemComboBox.setEnabled(isNew);
//		workItemComboBox.setEnabled(true);
		workGroupComboBox.setEnabled(enabled);
//		workGroupComboBox.setEnabled(true);
		addressCommit.setEnabled(isNew);
		//addressCommit.setEnabled(false);
		addressCommit.setIcon(workOrder.getAddress() != null ? BrigadesTheme.ICON_OK : BrigadesTheme.ICON_WARNING);
		LocalDateTime now = LocalDateTime.now();
		if (isNew) {
			startDatePlan.setRangeStart(now);
			finishDatePlan.setRangeStart(now);
		} else {
			startDatePlan.setRangeStart(startDatePlan.getValue());
			finishDatePlan.setRangeStart(startDatePlan.getValue());
		}

		if (workOrder.getBrigade() == null && workOrder.getIdBrigade() != null) {
			BrigadeDto brigade = restTemplate.getBrigade(workOrder.getIdBrigade());
			workOrder.setBrigade(brigade);
			workGroupComboBox.setValue(brigade);
		}

		map.put(WorkDto.class, workOrder.getId());
		attachmentPanel.setSourceMap(map);
	}

	private void bindFields() {
		String nullError = "Значение не может быть нулевым";
		SerializablePredicate<String> nullPredicate = StringUtil.nullPredicate();

		binder.bind(workOrderNum, WorkDto::getNumber, WorkDto::setNumber);
		//binder.bind(description, WorkDto::getDescription, WorkDto::setDescription);
		binder.forField(description).asRequired(nullError).withValidator(nullPredicate, nullError).bind(WorkDto::getDescription, WorkDto::setDescription);
		binder.bind(statusField, WorkDto::getStatus, WorkDto::setStatus);
		//binder.bind(workItemComboBox, getTypeWorkId(), setTypeWorkId());
		binder.forField(workItemComboBox).asRequired(nullError).bind(getTypeWorkId(), setTypeWorkId());
		binder.bind(workGroupComboBox, WorkDto::getBrigade, WorkDto::setBrigade);
		binder.bind(immediately, getImmediately(), setImmediately());
		binder.bind(organizationComboBox, getOrganization(), setOrganization());

		//binder.bind(addressField, WorkDto::getAddress, WorkDto::setAddress);
		binder.forField(addressField).asRequired(nullError).withValidator(nullPredicate, nullError).bind(WorkDto::getAddress, WorkDto::setAddress);
		binder.bind(addressDescription, WorkDto::getUpdatedAddress, WorkDto::setUpdatedAddress);
		binder.bind(authorField, getAuthor(), null);
		//binder.forField(workItemTextField).asRequired(nullError).withValidator(nullPredicate, nullError).bind(getTypeWork(), null);

		binder.bind(startDateFact, dateGetter(WorkDto::getStartDate), dateSetter(WorkDto::setStartDate));
		binder.forField(startDatePlan).asRequired(nullError).bind(dateGetter(WorkDto::getStartDatePlan), dateSetter(WorkDto::setStartDatePlan));
		//binder.bind(startDateFact, getStartDate(), setStartDate());
		binder.bind(finishDateFact, dateGetter(WorkDto::getFinishDate), dateSetter(WorkDto::setFinishDate));
		binder.forField(finishDatePlan).asRequired(nullError).bind(dateGetter(WorkDto::getFinishDatePlan), dateSetter(WorkDto::setFinishDatePlan));
		//binder.bind(finishDateFact, getFinishDate(), setFinishDate());

		setBeanAndFields(workOrder);
	}

	private ValueProvider<WorkDto, TypeWorkDto> getTypeWorkId() {
		return (ValueProvider<WorkDto, TypeWorkDto>) workOrder ->
				(workOrder.getIdTypeWork() == null || workOrder.getIdTypeWork() == 0L) ? null :
						restTemplate.getTypeWork(workOrder.getIdTypeWork());
	}

	private Setter<WorkDto, TypeWorkDto> setTypeWorkId() {
		return (Setter<WorkDto, TypeWorkDto>) (workOrder, typeWorkDto) -> {
			Long toSet = typeWorkDto == null ? null : typeWorkDto.getId();
			workOrder.setIdTypeWork(toSet);
		};
	}

	private ValueProvider<WorkDto, String> getAuthor() {
		return (ValueProvider<WorkDto, String>) workOrder ->
				(workOrder.getAuthor() == null) ? "" : workOrder.getAuthor().getFullName();
	}

	private ValueProvider<WorkDto, Boolean> getImmediately() {
		return (ValueProvider<WorkDto, Boolean>) workOrder ->
				workOrder.getUrgency() != null && workOrder.getUrgency().equals("Высокая");
	}

	private Setter<WorkDto, Boolean> setImmediately() {
		return (Setter<WorkDto, Boolean>) (workOrder, immediately) -> {
			if (immediately)
				workOrder.setUrgency("Высокая");
			else
				workOrder.setUrgency("Обычная");
		};
	}

	private ValueProvider<WorkDto, OrganizationDto> getOrganization() {
		return (ValueProvider<WorkDto, OrganizationDto>) workOrder ->
				(workOrder.getOrganization() == null) ? null : organizations.stream().filter(organizationDto ->
						organizationDto.getName().equalsIgnoreCase(workOrder.getOrganization())).findFirst().orElse(null);
	}

	private Setter<WorkDto, OrganizationDto> setOrganization() {
		return (Setter<WorkDto, OrganizationDto>) (workOrder, organizationDto) -> {
			String s = organizationDto == null ? "" : organizationDto.getName();
			workOrder.setOrganization(s);
		};
	}

	private void updateBrigadeWorks(BrigadeDto workGroup) {
		workOrderTreeGrid.clearItems();
		List<WorkDto> workOrders = restTemplate.getBrigadeWorks(workGroup.getId());
		List<WorkItemWorkDto> mainWorks = WorkOrderConstants.createMainWorks(workOrders);
		for (WorkItemWorkDto workDto : mainWorks) {
			workOrderTreeGrid.setRootItem(workDto);
			List<WorkItemDto> stages = restTemplate.getWorkItems(workDto.getId());
			List<WorkItemWorkDto> workStages = WorkOrderConstants.createWorkStages(stages);
			for (WorkItemWorkDto stage : workStages) {
				workOrderTreeGrid.setParentItem(workDto, stage);
			}
		}
		workOrderTreeGrid.collapseAll();
		workOrderTreeGrid.refreshAll();
	}

	private void updateWindow() {
		if (!isNew) {
			//stagesPanel.setWorkOrderStages(restTemplate.getWorkItems(workOrder.getId()));
			itemTreeGrid.setItems(restTemplate.getWorkItems(workOrder.getId()));
			attachmentPanel.setAttachments(restTemplate.getWorkAttachments(workOrder.getId()));
			vehiclePanel.setVehicles(restTemplate.getWorkEquipments(workOrder.getId()));
			BrigadeDto brigadeDto = workGroupComboBox.getValue();
			if (brigadeDto != null)
				updateBrigadeWorks(brigadeDto);
		}
	}

	@Subscribe
	public void updateWorkOrdersEvent(final ChangeWorkOrderEvent event) {
		WorkDto entity = event.getEntity();
		startDatePlan.setValue(DatesUtil.zonedToLocal(entity.getStartDatePlan()));
		finishDatePlan.setValue(DatesUtil.zonedToLocal(entity.getFinishDatePlan()));
	}

	@Subscribe
	public void updateWorkOrdersEvent(final UpdateWorkOrdersEvent event) {
		updateWindow();
	}

	//@Subscribe
	public void updateViewEvent(final UpdateViewEvent event) {
		updateWindow();
	}
}
