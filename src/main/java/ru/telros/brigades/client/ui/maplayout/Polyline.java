package ru.telros.brigades.client.ui.maplayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Хранение информации о полигоне
 */
@SuppressWarnings("serial")
public class Polyline extends MapElement implements Serializable {

    private String strokeColor = "#000000";
    private double strokeOpacity = 1.0;
    private int strokeWidth = 2;
    private String strokeStyle;
    private Collection<Coordinates> points = new ArrayList<>();

    public Polyline() {
        super();
    }

    public Polyline(String caption, String description) {
        super(caption, description);
    }

    public Polyline(String caption, String description, Collection<Coordinates> points) {
        super(caption, description);
        setPoints(points);
    }

    public Collection<Coordinates> getPoints() {
        return points;
    }

    public void setPoints(Collection<Coordinates> points) {
        points.clear();
        points.addAll(points);
    }

    public String getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    public double getStrokeOpacity() {
        return strokeOpacity;
    }

    public void setStrokeOpacity(double strokeOpacity) {
        this.strokeOpacity = strokeOpacity;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public String getStrokeStyle() {
        return strokeStyle;
    }

    public void setStrokeStyle(String strokeStyle) {
        this.strokeStyle = strokeStyle;
    }

    @Override
    protected String getPrefix() {
        return "pp_";
    }
}
