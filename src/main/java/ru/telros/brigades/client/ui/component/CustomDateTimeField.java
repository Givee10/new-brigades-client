package ru.telros.brigades.client.ui.component;

import com.vaadin.shared.ui.datefield.DateTimeResolution;
import com.vaadin.ui.DateTimeField;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.util.Locale;

public class CustomDateTimeField extends DateTimeField {
	public CustomDateTimeField() {
		setWidth(100, Unit.PERCENTAGE);
		setTextFieldEnabled(true);
		setLocale(Locale.forLanguageTag("RU"));
		setResolution(DateTimeResolution.MINUTE);
		setDateFormat(DatesUtil.GRID_DATE_FORMAT);
		setDateOutOfRangeMessage("Дата выходит за допустимые пределы");
	}

	public CustomDateTimeField(final String caption) {
		this();
		setCaption(caption);
	}
}
