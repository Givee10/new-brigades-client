package ru.telros.brigades.client.ui.component;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.*;

public class HeaderLayout extends FullSizeVerticalLayout {
	private final HorizontalLayout header = new HorizontalLayout();
	private final VerticalLayout layout = new FullSizeVerticalLayout();
	private final Label expandingGap = new Label();

	public HeaderLayout() {
		buildHeader();
		buildContent();
	}

	private void buildHeader() {
		header.setWidth(100, Sizeable.Unit.PERCENTAGE);
		header.setSpacing(true);
		header.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

		addHeaderExpandingGap();
	}

	public void addHeaderExpandingGap() {
		if (header.getComponentIndex(expandingGap) == -1) {
			header.addComponent(expandingGap);
			header.setExpandRatio(expandingGap, 1);
		}
	}

	public void removeHeaderExpandingGap() {
		if (header.getComponentIndex(expandingGap) >= 0) {
			header.removeComponent(expandingGap);
		}
	}

	private void buildContent() {
		layout.setSpacing(true);

		setMargin(true);
		setSpacing(true);
		super.addComponent(header);
		super.addComponent(layout);
		super.setExpandRatio(layout, 1);
	}

	public void addToHeader(final Component component) {
		header.addComponent(component, header.getComponentCount() - 1);
	}

	public void addToHeader(final Component component, boolean isExpanded) {
		if (isExpanded) {
			removeHeaderExpandingGap();
		}
		header.addComponent(component,
				(header.getComponentIndex(expandingGap) >= 0) ? header.getComponentCount() - 1 : header.getComponentCount());
		if (isExpanded) {
			header.setExpandRatio(component, 1);
		}
	}

	public void addToLayout(final Component component) {
		layout.addComponent(component);
	}

	public void addComponentAndRatio(Component component, float ratio) {
		layout.addComponent(component);
		layout.setExpandRatio(component, ratio);
	}
}
