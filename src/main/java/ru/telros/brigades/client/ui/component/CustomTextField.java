package ru.telros.brigades.client.ui.component;

import com.vaadin.ui.TextField;
import ru.telros.brigades.client.ui.utils.StringUtil;

@SuppressWarnings("serial")
public class CustomTextField extends TextField {

	public CustomTextField() {
		setWidth(100, Unit.PERCENTAGE);
	}

	public CustomTextField(String caption) {
		this();
		setCaption(caption);
	}

	@Override
	public void setValue(String value) {
		super.setValue(StringUtil.returnString(value));
	}
}
