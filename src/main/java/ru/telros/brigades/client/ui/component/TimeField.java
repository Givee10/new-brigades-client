package ru.telros.brigades.client.ui.component;

import com.vaadin.ui.TextField;
import org.vaadin.inputmask.InputMask;
import ru.telros.brigades.client.HasLogger;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class TimeField extends TextField implements HasLogger {
	public TimeField() {
		setWidth(55, Unit.PIXELS);
		setMaxLength(5);
		InputMask.addTo(this, "99:99");
	}

	public TimeField(final String caption) {
		this();
		setCaption(caption);
	}

	/* Перевод значения в минуты */
	public Integer convertValue() {
		String[] split = getValue().split(":");
		Integer hours = Integer.parseInt(split[0]);
		Integer minutes = Integer.parseInt(split[1]) + hours * 60;
		setValue(minutes);
		return minutes;
	}

	/* Значение норматива из БД (StageData) в часах */
	public void setValue(ZonedDateTime start, ZonedDateTime finish) {
		Long hours = ChronoUnit.HOURS.between(start, finish);
		Long minutes = ChronoUnit.MINUTES.between(start, finish) - hours * 60;
		String newValue = String.format("%02d", hours) + ":" + String.format("%02d", minutes);
		setValue(newValue);
	}

	/* Значение норматива из БД (StageData) в часах */
	public void setValue(Double hours) {
		Double floor = Math.floor(hours);
		Long longValue = Math.round((hours - floor) * 60);
		String newValue = String.format("%02d", floor.intValue()) + ":" + String.format("%02d", longValue.intValue());
		setValue(newValue);
	}

	/* Значение норматива из БД (StageData) в минутах */
	public void setValue(Integer minutes) {
		int hrs = minutes / 60;
		int min = minutes % 60;
		String newValue = String.format("%02d", hrs) + ":" + String.format("%02d", min);
		setValue(newValue);
	}
}
