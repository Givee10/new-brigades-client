package ru.telros.brigades.client.ui.vehicle;

import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.ExcelData;
import ru.telros.brigades.client.dto.ExcelDataList;
import ru.telros.brigades.client.ui.component.CustomUpload;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.FileUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

public class EquipmentExcelWindow extends WindowWithButtons implements HasLogger {
	private File file;
	private CustomUpload customUpload = new CustomUpload();

	public EquipmentExcelWindow() {
		super("Загрузка спецтехники", 800, -1);
		setResizable(false);

		addToContent(CompsUtil.getVerticalWrapperNoMargin(customUpload));
		addToButtonBar(msg("button.send"), event -> send());
		addToButtonBar(msg("button.close"), event -> close());
		addCloseListener(closeEvent -> {
			if (file != null)
				getLogger().debug("File deleted: {}", file.delete());
		});
	}

	private void send() {
		file = customUpload.getFile();
		if (file != null) {
			try {
				String mimeType = FileUtil.detectFileMIMEType(file);
				if (!mimeType.equalsIgnoreCase(FileUtil.MIMEType.MSEXCELX.getMIMEType()) || !mimeType.equalsIgnoreCase(FileUtil.MIMEType.MSEXCELX.getMIMEType())) {
					Notification.show("Неверный тип вложения", Notification.Type.ERROR_MESSAGE);
					return;
				}
				FileInputStream excelFile = new FileInputStream(file);
				Workbook workbook = new XSSFWorkbook(excelFile);
				Sheet datatypeSheet = workbook.getSheetAt(0);
				List<CellRangeAddress> mergedRegions = datatypeSheet.getMergedRegions();
				ExcelDataList excelDataList = new ExcelDataList();
				List<ExcelData> list = new ArrayList<>();

				for (Row currentRow : datatypeSheet) {
					//getLogger().debug("Row {}, PhysicalNumberOfCells: {}", currentRow.getRowNum(), currentRow.getPhysicalNumberOfCells());
					Cell cell = currentRow.getCell(4);
					if (cell != null) {
						//getLogger().debug("Check Cell address {}, Value {}", cell.getAddress(), cell.getStringCellValue());
						if (cell.getCellTypeEnum().equals(CellType.STRING) && !cell.getStringCellValue().isEmpty()
								&& !cell.getStringCellValue().equals(" Гос. №      а/м") && !cell.getStringCellValue().equals("-")) {
							ExcelData excelData = new ExcelData();
							/*excelData.setTypeWork(currentRow.getCell(1).getStringCellValue());
							excelData.setAddress(currentRow.getCell(2).getStringCellValue());
							excelData.setEquipment(currentRow.getCell(3).getStringCellValue());
							excelData.setNumber(currentRow.getCell(4).getStringCellValue());
							excelData.setService(currentRow.getCell(5).getStringCellValue());
							excelData.setComment(currentRow.getCell(6).getStringCellValue());
							excelData.setTime(currentRow.getCell(7).getStringCellValue());
							excelData.setNote(currentRow.getCell(8).getStringCellValue());
							excelData.setResponsible(currentRow.getCell(9).getStringCellValue());*/
							for (Cell currentCell : currentRow) {
								String cellValue = currentCell.getStringCellValue();
								//getLogger().debug("Cell address {}, Value {}", currentCell.getAddress().formatAsString(), cellValue);
								for (CellRangeAddress mergedRegion : mergedRegions) {
									if (mergedRegion.containsColumn(currentCell.getColumnIndex()) && mergedRegion.containsRow(currentCell.getRowIndex())) {
										//getLogger().debug("Parent Merged cell {}", currentCell.getAddress());
										Cell mergeCell = datatypeSheet.getRow(mergedRegion.getFirstRow()).getCell(mergedRegion.getFirstColumn());
										//getLogger().debug("Main Merge cell {}", mergeCell.getAddress());
										cellValue = mergeCell.getStringCellValue();
									}
								}
								switch (currentCell.getColumnIndex()) {
									case 1:
										excelData.setTypeWork(cellValue);
										break;
									case 2:
										excelData.setAddress(cellValue);
										break;
									case 3:
										excelData.setEquipment(cellValue);
										break;
									case 4:
										excelData.setNumber(cellValue);
										break;
									case 5:
										excelData.setService(cellValue);
										break;
									case 6:
										excelData.setComment(cellValue);
										break;
									case 7:
										excelData.setTime(cellValue);
										break;
									case 8:
										excelData.setNote(cellValue);
										break;
									case 9:
										excelData.setResponsible(cellValue);
										break;
								}
							}
							getLogger().debug(excelData.debugInfo());
							list.add(excelData);
						}
					}
				}
				excelDataList.setList(list);
				BrigadesUI.setCurrentExcelDataList(excelDataList);
				VehicleLayout.refresh();
				close();
			} catch (IOException e) {
				getLogger().error(e.getMessage());
			}
		} else Notification.show("Файл не выбран", Notification.Type.ERROR_MESSAGE);
	}

	public static void open() {
		final Window window = new EquipmentExcelWindow();
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}
}
