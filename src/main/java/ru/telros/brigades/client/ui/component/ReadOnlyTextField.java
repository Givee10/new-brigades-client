package ru.telros.brigades.client.ui.component;

import ru.telros.brigades.client.ui.utils.StringUtil;

public class ReadOnlyTextField extends CustomTextField {

	public ReadOnlyTextField(final String caption) {
		super(caption);
		super.setReadOnly(true);
	}

	@Override
	public void setValue(final String newValue) {
		super.setReadOnly(false);
		try {
			super.setValue(StringUtil.returnString(newValue));
		} finally {
			super.setReadOnly(true);
		}
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
	}
}
