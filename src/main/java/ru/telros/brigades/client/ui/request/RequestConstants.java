package ru.telros.brigades.client.ui.request;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import ru.telros.brigades.client.BrigadesIcons;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.dto.CommentDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.RequestJournalDto;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import static ru.telros.brigades.client.MessageManager.msg;

public abstract class RequestConstants {
	public static final GridColumn<RequestDto> FLAG = GridColumn.createComponentColumn("_flag", "", RequestConstants::createFlag);
	public static final GridColumn<RequestDto> COMMENT = GridColumn.createComponentColumn("_comment", "", RequestConstants::createComment);
	public static final GridColumn<RequestDto> CODE = GridColumn.createColumn("code", "Код проблемы", RequestDto::getProblemCode);
	public static final GridColumn<RequestDto> NAME = GridColumn.createColumn("name", "Наименование", RequestDto::getProblem);
	public static final GridColumn<RequestDto> RESPONSIBLE = GridColumn.createColumn("responsible", "Ответств.", GridColumn::nullProvider);
	public static final GridColumn<RequestDto> REGION = GridColumn.createColumn("region", "Район РВ", GridColumn::nullProvider);
	public static final GridColumn<RequestDto> ORGANIZATION = GridColumn.createColumn("organization", "Организация", RequestDto::getOrganization);
	public static final GridColumn<RequestDto> DISTRICT = GridColumn.createColumn("district", "Админ. район", RequestDto::getDistrict);
	public static final GridColumn<RequestDto> NUMBER = GridColumn.createColumn("number", "№ заявки", RequestDto::getNumber);
	public static final GridColumn<RequestDto> EXT_NUMBER = GridColumn.createColumn("ext_number", "Код заявки", RequestDto::getExternalNumber);
	public static final GridColumn<RequestDto> PORTAL = GridColumn.createColumn("portal", "Портал", RequestConstants::portalProvider);
	public static final GridColumn<RequestDto> ADDRESS = GridColumn.createColumn("address", "Адрес", RequestDto::getAddress);
	public static final GridColumn<RequestDto> DESCRIPTION = GridColumn.createColumn("desc", "Описание", RequestDto::getDescription);
	public static final GridColumn<RequestDto> REG_DATE = GridColumn.createDateColumn("reg_date", "Дата/время поступл.", RequestDto::getRegistrationDate);
	public static final GridColumn<RequestDto> CLOSE_DATE = GridColumn.createDateColumn("close_date", "Планир. дата/время закрытия", RequestDto::getFinishDatePlan);
	public static final GridColumn<RequestDto> ATTACHMENTS = GridColumn.createColumn("attachments", "Приложения", RequestConstants::attachmentProvider);
	public static final GridColumn<RequestDto> STATUS = GridColumn.createColumn("status", "Статус", RequestDto::getStatus);
	public static final GridColumn<RequestDto> HLSTATUS = GridColumn.createColumn("hl_status", "Статус ГЛ", RequestDto::getStatusHL);

	public static final GridColumn<RequestJournalDto> FLAG_ = GridColumn.createComponentColumn("_flag", "", RequestConstants::createJournalFlag);
	public static final GridColumn<RequestJournalDto> ICON = GridColumn.createComponentColumn("_icon", "", RequestConstants::createJournalIcon);
	public static final GridColumn<RequestJournalDto> ID = GridColumn.createColumn("id", "№", RequestJournalDto::getId);
	public static final GridColumn<RequestJournalDto> CODE_ = GridColumn.createColumn("code", "Код проблемы", RequestJournalDto::getProblemCode);
	public static final GridColumn<RequestJournalDto> NAME_ = GridColumn.createColumn("name", "Наименование", RequestJournalDto::getProblemName);
	public static final GridColumn<RequestJournalDto> RESPONSIBLE_ = GridColumn.createColumn("responsible", "Ответств.", RequestJournalDto::getResponsible);
	public static final GridColumn<RequestJournalDto> REGION_ = GridColumn.createColumn("region", "Район РВ", RequestJournalDto::getRegion);
	public static final GridColumn<RequestJournalDto> LOCATION = GridColumn.createColumn("district", "Админ. район", RequestJournalDto::getLocation);
	public static final GridColumn<RequestJournalDto> NUMBER_ = GridColumn.createColumn("number", "№ заявки", RequestJournalDto::getNumber);
	public static final GridColumn<RequestJournalDto> PORTAL_ = GridColumn.createBooleanColumn("portal", "Портал", RequestJournalDto::getPortal, "+");
	public static final GridColumn<RequestJournalDto> ADDRESS_ = GridColumn.createColumn("address", "Адрес", RequestJournalDto::getAddress);
	public static final GridColumn<RequestJournalDto> DESCRIPTION_ = GridColumn.createColumn("desc", "Описание", RequestJournalDto::getDescription);
	public static final GridColumn<RequestJournalDto> REGISTRATION_DATE = GridColumn.createDateColumn("reg_date", "Дата/время поступл.", RequestJournalDto::getRegistrationDate);
	public static final GridColumn<RequestJournalDto> FINISH_DATE = GridColumn.createDateColumn("close_date", "Планир. дата/время закрытия", RequestJournalDto::getFinishDatePlan);
	public static final GridColumn<RequestJournalDto> HAS_ATTACHMENTS = GridColumn.createBooleanColumn("attachments", "Приложения", RequestJournalDto::getHasAttachment, "Есть");

	private static final GridColumn<CommentDto> CREATE_DATE = GridColumn.createDateColumn("createDate", "Дата / время записи", CommentDto::getCreateDate);
	private static final GridColumn<CommentDto> CHANGE_DATE = GridColumn.createDateColumn("actionDate", "Дата / время действия", CommentDto::getChangeDate);
	private static final GridColumn<CommentDto> STAGE = GridColumn.createColumn("type", "Этап", CommentDto::getStage);
	private static final GridColumn<CommentDto> CONTENT = GridColumn.createColumn("description", "Содержание", CommentDto::getContent);
	private static final GridColumn<CommentDto> NOTE = GridColumn.createColumn("permission", "Передать в ГЛ", CommentDto::getNote);
	private static final GridColumn<CommentDto> SOURCE = GridColumn.createColumn("source", "От кого", CommentDto::getSource);
	private static final GridColumn<CommentDto> REGISTERED_BY = GridColumn.createColumn("user", "Зарегистрировал", CommentDto::getRegisteredBy);

	public static final LinkedHashMap<String, String> statusStyles = new LinkedHashMap<String, String>() {
		{
			put(RequestDto.Status.NEW.getCode(), BrigadesTheme.STYLE_ET_WARNING);
			put(RequestDto.Status.IN_WORK.getCode(), BrigadesTheme.STYLE_ET_INWORK);
			put(RequestDto.Status.CLOSED.getCode(), BrigadesTheme.STYLE_ET_SUCCESS);
		}
	};

	private static final LinkedHashMap<String, String> statusYandexIcon = new LinkedHashMap<String, String>() {
		{
			put(RequestDto.Status.NEW.getCode(), "islands#orangeIcon");
			put(RequestDto.Status.IN_WORK.getCode(), "islands#lightBlueIcon");
			put(RequestDto.Status.CLOSED.getCode(), "islands#greenIcon");
		}
	};

	private static final LinkedHashMap<String, String> statusYandexIconColor = new LinkedHashMap<String, String>() {
		{
			put(RequestDto.Status.NEW.getCode(), "orange");
			put(RequestDto.Status.IN_WORK.getCode(), "lightBlue");
			put(RequestDto.Status.CLOSED.getCode(), "green");
		}
	};

	private static String attachmentProvider(RequestDto request) {
		List<AttachmentDto> attachments = BrigadesUI.getRestTemplate().getRequestAttachments(request.getId());
		return attachments.size() > 0 ? "Есть" : "";
	}

	private static String portalProvider(RequestDto request) {
		if (request.getPortal() != null) return "+"; else return "";
	}

	private static CssLayout createFlag(RequestDto request) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (request.getAddress() == null) {
			BrigadesIcons icon = new BrigadesIcons(BrigadesTheme.ICON_ERROR, BrigadesTheme.COLOR_EMERGENCY);
			Label label = new Label(icon.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			//label.addStyleName(BrigadesTheme.STYLE_ET_ICON_EMERGENCY);
			label.setDescription(msg("table.request.address.addressnotlinked.description"));
			layout.addLayoutClickListener(event ->
					Notification.show("Clicked: " + request.getId(), Notification.Type.TRAY_NOTIFICATION));
			layout.addComponent(label);
		}
		return layout;
	}

	private static CssLayout createComment(RequestDto request) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		/*if (request.getCloseDate() != null) {
			Label label = new Label(BrigadesTheme.ICON_WORKGROUP.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON_EMERGENCY);
			label.setDescription(msg("table.request.address.addressnotlinked.description"));
			layout.addLayoutClickListener(event ->
					Notification.show("Clicked: " + request.getId(), Notification.Type.TRAY_NOTIFICATION));
			layout.addComponent(label);
		}*/
		return layout;
	}

	private static CssLayout createJournalFlag(RequestJournalDto request) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		if (isEmergency(request)) {
			BrigadesIcons icon = new BrigadesIcons(BrigadesTheme.ICON_ERROR, BrigadesTheme.COLOR_EMERGENCY);
			Label label = new Label(icon.getHtml(), ContentMode.HTML);
			label.setSizeFull();
			label.addStyleName(BrigadesTheme.STYLE_ET_ICON);
			label.setDescription("Нарушены сроки обработки заявки");
			layout.addComponent(label);
		}
		return layout;
	}

	private static CssLayout createJournalIcon(RequestJournalDto request) {
		CssLayout layout = new CssLayout();
		layout.setSizeFull();
		return layout;
	}

	// таблицы по цвету (по просьбе Елены Викторовны). Упасть вроде ничего не должно, но на всякий случай даю тебе знать.
	public static List<GridColumn<RequestDto>> getMainColumns() {
		return Arrays.asList(FLAG, COMMENT, CODE, NAME, ADDRESS, DESCRIPTION, NUMBER, RESPONSIBLE,
				REGION, DISTRICT, PORTAL, REG_DATE, CLOSE_DATE, ATTACHMENTS, STATUS);
	}

	/**
	 * то же самое, что и выше, но со статусом и без пустых колонок (для отчета по заявкам)
	 * {@link ru.telros.brigades.client.view.reports.RequestsReportWindow2#RequestsReportWindow2(String)}
	 */
	public static List<GridColumn<RequestDto>> getMainColumnsAndStatus() {
		return Arrays.asList(CODE, NAME, ADDRESS, DESCRIPTION, EXT_NUMBER, RESPONSIBLE,
				REGION, DISTRICT, ORGANIZATION, PORTAL, REG_DATE, CLOSE_DATE, ATTACHMENTS, STATUS, HLSTATUS);
	}
	//без колонок с пустыми названиями
	public static List<GridColumn<RequestDto>> getMainColumnsWithoutEmpty() {
		return Arrays.asList( CODE, NAME, RESPONSIBLE, REGION, DISTRICT, NUMBER, PORTAL,
				ADDRESS, DESCRIPTION, REG_DATE, CLOSE_DATE, ATTACHMENTS);
	}

	//Для отчета по заявкам РВ Югозападный
	public static List<GridColumn<RequestDto>> getSpecialReportColumns(){
		return Arrays.asList(FLAG, COMMENT, CODE, NAME, RESPONSIBLE, REGION, DISTRICT, NUMBER, PORTAL,
				ADDRESS, DESCRIPTION, REG_DATE, CLOSE_DATE, ATTACHMENTS);
	}

	public static List<GridColumn<RequestJournalDto>> getJournalColumns() {
		return Arrays.asList(FLAG_, ICON, CODE_, NAME_, ADDRESS_, DESCRIPTION_, NUMBER_, RESPONSIBLE_,
				REGION_, LOCATION, PORTAL_, REGISTRATION_DATE, FINISH_DATE, HAS_ATTACHMENTS);
	}

	public static List<GridColumn<CommentDto>> getCommentColumns() {
		return Arrays.asList(CREATE_DATE, CHANGE_DATE, STAGE, CONTENT, NOTE, SOURCE, REGISTERED_BY);
	}

	/**
	 * Стиль по статусу заявки
	 *
	 * @param status
	 * @return
	 */
	public static String getStyle(String status) {
		return (status != null) ? statusStyles.get(status) : BrigadesTheme.STYLE_ET_EMERGENCY;
	}

	/**
	 * Наименование иконки по статусу
	 *
	 * @param status
	 * @return
	 */
	public static String getYandexIcon(String status) {
		return (status != null) ? statusYandexIcon.get(status) : "islands#redIcon";
	}

	/**
	 * Цвет иконки по статусу
	 *
	 * @param status
	 * @return
	 */
	public static String getYandexIconColor(String status) {
		return (status != null) ? statusYandexIconColor.get(status) : "red";
	}

	public static String getStatusDescriptionHtml(String status) {
		//return "&nbsp;<SPAN class='" + getStyle(status) + "'>" + status.getDisplayName() + "</SPAN>";
		return "&nbsp;<SPAN class='" + getStyle(status) + "'>" + status + "</SPAN>";
	}

	public static boolean isEmergency(RequestJournalDto requestJournalDto) {
		Objects.requireNonNull(requestJournalDto);
		ZonedDateTime now = ZonedDateTime.now();
		return requestJournalDto.getFinishDatePlan() != null && requestJournalDto.getFinishDatePlan().isBefore(now) &&
				!RequestDto.Status.CLOSED.getCode().equals(requestJournalDto.getStatus());
	}

	public static boolean isTestRequest(String requestNumber) {
		List<String> stringList = Arrays.asList("1951574", "1922664", "1951590", "1967223", "1969193");
		return stringList.contains(requestNumber);
	}

	public static boolean canCloseRequest(String requestStatus) {
		return !RequestDto.Status.CLOSED.getCode().equalsIgnoreCase(requestStatus);
	}
}
