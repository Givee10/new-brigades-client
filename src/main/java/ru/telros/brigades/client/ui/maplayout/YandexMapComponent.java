package ru.telros.brigades.client.ui.maplayout;

import com.vaadin.annotations.JavaScript;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.JavaScriptFunction;
import elemental.json.JsonArray;
import elemental.json.JsonException;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.PositionDto;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Server-Side part for YandexMapComponent
 */
//@SuppressWarnings({"unchecked", "serial"})
@JavaScript({"http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU&apikey=0926c108-3add-42da-99cd-75b59886c161", "vaadin://addons/ymlibrary/ymlibrary.js", "vaadin://addons/ymlibrary/ymlibrary-connector.js"})
public class YandexMapComponent extends AbstractJavaScriptComponent implements HasLogger {
	private static final MapType DEFAULT_MAPTYPE = MapType.MAP;
	private static final Control[] DEFAULT_CONTROLS = new Control[]{Control.SCALELINE, Control.TYPESELECTOR, Control.ZOOMCONTROL};
	private static final Coordinates DEFAULT_CENTER = new Coordinates(59.983287, 30.351842); // Moscow:(55.7, 37.6), SPb:(59.983287, 30.351842)
	private static final int DEFAULT_ZOOM = 12;

	private static final String JS_FUNC_SETZOOM = "setZoom";
	private static final String JS_FUNC_ADDMARKER = "addMarkerFromJSON";
	private static final String JS_FUNC_SETMAPTYPE = "setMapType";
	private static final String JS_FUNC_SETCONTROLS = "setControls";
	private static final String JS_FUNC_SETCENTER = "setCenter";
	private static final String JS_FUNC_PANTO = "panTo";
	private static final String JS_FUNC_REMOVEMARKER = "removeMarker";
	private static final String JS_FUNC_REMOVEALLMARKERS = "removeAllMarkers";
	private static final String JS_FUNC_UPDATESTATE = "updateState";
	private static final String JS_FUNC_ADDPOLYLINE = "addPolylineFromJSON";
	private static final String JS_FUNC_REMOVEPOLYLINE = "removePolyline";
	private static final String JS_FUNC_REMOVEALLPOLYLINES = "removeAllPolylines";
	private static final String JS_FUNC_SHOWBALLOON = "showBalloon";
	private static final String JS_FUNC_ADDROUTE = "addRoute";

	private static final String RPC_UPDATESHAREDSTATE = "updateSharedState";
	private static final String RPC_NOTIFYINITIALIZED = "notifyInitialized";
	private static final String RPC_MARKERCLICKED = "markerClicked";
	private static final String RPC_BOUNDSCHANGED = "boundsChanged";

	private static int globalUid = 0;
	private boolean callInitRpc = false;

	private List<MapNotifyEventListener> initializedListeners = new ArrayList<>();
	private List<MapNotifyEventListener> boundsChangedListeners = new ArrayList<>();
	private List<MapMarkerClickListener> markerClickedListeners = new ArrayList<>();

	public YandexMapComponent() {
		init(getDefaultOptions());
	}

	/**
	 * Получение уникального ид-ра компонента
	 *
	 * @return
	 */
	private static synchronized int getUid() {
		return ++globalUid;
	}

	/**
	 * Получение опций компонента по умолчанию
	 *
	 * @return
	 */
	public static YandexMapComponentOptions getDefaultOptions() {
		return YandexMapComponentOptions.getDefaultOptions();
	}

	/**
	 * Инициализация компонента YandexMap
	 *
	 * @param options
	 */
	private void init(YandexMapComponentOptions options) {
		if (options == null) throw new NullPointerException("Component Settings cannot be null");

		addRpcFunctions();
		setOptions(options);
		getState().uid = "ymmap_" + getUid();
		getState().controls = controlsToState(DEFAULT_CONTROLS);
		getState().type = DEFAULT_MAPTYPE.getName();
		getState().zoom = DEFAULT_ZOOM;
		getState().centerLatitude = DEFAULT_CENTER.getLat();
		getState().centerLongitude = DEFAULT_CENTER.getLon();
		addRpcUpdateSharedState();
	}

	private YandexMapComponent getMe() {
		return this;
	}

	/**
	 * Getting zoom factor of the map
	 *
	 * @return
	 */
	public int getZoom() {
		updateState();
		return getState().zoom;
	}

	/**
	 * Setting zoom factor of the map
	 *
	 * @param zoom
	 */
	public void setZoom(int zoom) {
		getState().zoom = zoom;
		callFunction(JS_FUNC_SETZOOM, zoom);
	}

	/**
	 * Pan to coordinates with delay
	 *
	 * @param coordinates
	 * @param delay
	 */
	public void panTo(Coordinates coordinates, Integer delay) {
		getState().centerLatitude = coordinates.getLat();
		getState().centerLongitude = coordinates.getLon();
		callFunction(JS_FUNC_PANTO, coordinates.getLat(), coordinates.getLon(), delay);
	}

	/**
	 * Getting center point of map
	 *
	 * @return
	 */
	public Coordinates getCenter() {
		updateState();
		return new Coordinates(getState().centerLatitude, getState().centerLongitude);
	}

	/**
	 * Setting center point of map
	 *
	 * @param coordinates
	 */
	public void setCenter(Coordinates coordinates) {
		getState().centerLatitude = coordinates.getLat();
		getState().centerLongitude = coordinates.getLon();
		callFunction(JS_FUNC_SETCENTER, coordinates.getLat(), coordinates.getLon());
	}

	/**
	 * Отображение Balloon при переходе на карте на адрес
	 *
	 * @param coordinates
	 * @param content
	 */
	public void showBalloon(Coordinates coordinates, String content) {
		callFunction(JS_FUNC_SHOWBALLOON, coordinates.getLat(), coordinates.getLon(), content);
	}

	/**
	 * Отображение пути транспорта
	 *
	 * @param mapRoutes
	 */
	public void addRoute(List<PositionDto> mapRoutes) {
		List<Double[]> list = new ArrayList<>();
		for (PositionDto route : mapRoutes) {
			list.add(new Double[]{route.getY().doubleValue(), route.getX().doubleValue()});
		}
		callFunction(JS_FUNC_ADDROUTE, list.toArray());
	}

	/**
	 * Getting array of component controls
	 *
	 * @return
	 */
	public Control[] getControls() {
		updateState();
		String[] set = getState().controls;
		Control[] controls = new Control[set.length];
		for (int i = 0; i < set.length; i++)
			controls[i] = Control.getByName(set[i]);

		return controls;
	}

	/**
	 * Setting component controls
	 *
	 * @param controls
	 */
	public void setControls(Control[] controls) {
		String set[] = controlsToState(controls);
		getState().controls = set;
		callFunction(JS_FUNC_SETCONTROLS, (Object[]) set);
	}

	/**
	 * Get controls array from JsonArray
	 *
	 * @return
	 */
	public String[] getControls(JsonArray array) {
		String[] set = new String[array.length()];
		for (int i = 0; i < array.length(); i++)
			set[i] = (String) array.getString(i);
		return set;
	}

	/**
	 * Convert Control array to String array
	 *
	 * @param controls
	 * @return
	 */
	private String[] controlsToState(Control[] controls) {
		if (controls == null) {
			controls = DEFAULT_CONTROLS;
		}
		String[] set = new String[controls.length];
		for (int i = 0; i < controls.length; i++)
			set[i] = controls[i].getName();
		return set;
	}

	/**
	 * Получение типа карты
	 */
	public MapType getMapType() {
		updateState();
		String type = getState().type;
		MapType mapType = MapType.getByName(type);
		return (mapType != null) ? mapType : DEFAULT_MAPTYPE;
	}

	/**
	 * Setting type of map
	 *
	 * @param mapType
	 */
	public void setMapType(MapType mapType) {
		if (mapType == null) {
			mapType = DEFAULT_MAPTYPE;
		}
		String type = mapType.getName();
		getState().type = type;
		callFunction(JS_FUNC_SETMAPTYPE, type);
	}

	/**
	 * Добавление маркера
	 *
	 * @param coordinates
	 * @param caption
	 * @return
	 */
	public Marker addMarker(Coordinates coordinates, String caption) {
		return addMarker(coordinates, caption, null, null);
	}

	/**
	 * Добавление маркера
	 *
	 * @param coordinates
	 * @param caption
	 * @param imageUrl
	 */
	public Marker addMarker(Coordinates coordinates, String caption, String imageUrl) {
		return addMarker(coordinates, caption, null, imageUrl);
	}

	/**
	 * Добавление маркера
	 *
	 * @param coordinates
	 * @param caption
	 * @param description
	 * @param imageUrl
	 * @return
	 */
	public Marker addMarker(Coordinates coordinates, String caption, String description, String imageUrl) {
		Marker marker = new Marker(caption, description, imageUrl, coordinates.getLat(), coordinates.getLon());
		addMarker(marker);
		return marker;
	}

	/**
	 * Добавление маркера
	 *
	 * @param marker
	 * @return
	 */
	public Marker addMarker(Marker marker) {
		removeMarker(marker);
		getState().markers.add(marker);
		String strMarker = null;
		try {
			strMarker = StringUtil.writeValueAsString(marker);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (strMarker != null) {
			callFunction(JS_FUNC_ADDMARKER, strMarker);
		}
		return marker;
	}

	/**
	 * Добавление полилинии
	 *
	 * @param polyline
	 * @return
	 */
	public Polyline addPolyline(Polyline polyline) {
		removePolyline(polyline);
		getState().polylines.add(polyline);
		String strPolyline = null;
		try {
			strPolyline = StringUtil.writeValueAsString(polyline);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (strPolyline != null) {
			callFunction(JS_FUNC_ADDPOLYLINE, strPolyline);
		}
		return polyline;
	}

	/**
	 * Удаление маркера
	 *
	 * @param marker
	 */

	public void removeMarker(Marker marker) {
		if (getState().markers.contains(marker)) {
			callFunction(JS_FUNC_REMOVEMARKER, marker.getUid());
			getState().markers.remove(marker);
		}
	}

	/**
	 * Удаление полилинии
	 *
	 * @param polyline
	 */
	public void removePolyline(Polyline polyline) {
		if (getState().polylines.contains(polyline)) {
			callFunction(JS_FUNC_REMOVEPOLYLINE, polyline.getUid());
			getState().polylines.remove(polyline);
		}
	}

	/**
	 * Удаление всех маркеров
	 */
	public void removeAllMarkers() {
		callFunction(JS_FUNC_REMOVEALLMARKERS);
		getState().markers.clear();
		//MapElement.clearElementUid();
	}

	/**
	 * Удаление всех полилиний
	 */
	public void removeAllPolylines() {
		callFunction(JS_FUNC_REMOVEALLPOLYLINES);
		getState().polylines.clear();
	}

	/**
	 * Update remote state of component
	 */
	private void updateState() {
		callFunction(JS_FUNC_UPDATESTATE);
	}

	/**
	 * Return link to component options
	 *
	 * @return
	 */
	public YandexMapComponentOptions getOptions() {
		return (YandexMapComponentOptions) getState().options;
	}

	/**
	 * Setting component options
	 *
	 * @param options
	 */
	public void setOptions(YandexMapComponentOptions options) {
		getState().options = options;
		callInitRpc = true;
	}

	@Override
	protected YandexMapComponentState getState() {
		return (YandexMapComponentState) super.getState();
	}

	// Add Accept-Ranges: bytes to all responses to support see
// king in non-IE browsers
// Catch IOException that is thrown when the player source has changed and the client aborts the previous connection
	@Override
	public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path) throws IOException {
		try {
			getLogger().debug("path=" + path);
			if (path.startsWith("0")) {
				getLogger().debug("Adding Accept-Ranges: bytes header");
				response.setHeader("Accept-Ranges", "bytes");
			}
			return super.handleConnectorRequest(request, response, path);
		} catch (IOException e) {
			return true;
		}
	}

	/*
	 *
	 * Add RPC Functions to Connector
	 *
	 */
	private void addRpcFunctions() {

		addFunction(RPC_NOTIFYINITIALIZED, new JavaScriptFunction() {
			private static final long serialVersionUID = 5490315638431042879L;

			@Override
			public void call(JsonArray a) {
				for (MapNotifyEventListener listener : initializedListeners) {
					listener.notify(getMe());
				}
			}
		});

		addFunction(RPC_MARKERCLICKED, new JavaScriptFunction() {
			private static final long serialVersionUID = 5490315638431042879L;

			@Override
			public void call(JsonArray a) {
				Marker marker = (Marker) MapElement.findByUid(getState().markers, (String) a.getString(0));
				if (marker != null) {
					for (MapMarkerClickListener listener : markerClickedListeners) {
						listener.markerClicked(marker);
					}
				}
			}
		});

		addFunction(RPC_BOUNDSCHANGED, new JavaScriptFunction() {
			private static final long serialVersionUID = 5490315638431042879L;

			@Override
			public void call(JsonArray a) {
				for (MapNotifyEventListener listener : boundsChangedListeners) {
					listener.notify(getMe());
				}
			}
		});
	}


// Initializes YandexMapComponent object on client side
// This is only required if the component hasn't been initialized yet or options have changed
// Call function only once per response, if required
//@Override
//public void beforeClientResponse(boolean initial) {
//if (initial || callInitRpc) {
//callFunction("initComponent");
//callInitRpc = false;
//}
//super.beforeClientResponse(initial);
//}

	/**
	 * Добавление функции RPC_UPDATESHAREDSTATE
	 */
	private void addRpcUpdateSharedState() {

// Connector function to updated shared state from the client side
		addFunction(RPC_UPDATESHAREDSTATE, new JavaScriptFunction() {
			private static final long serialVersionUID = 5490315638431042879L;

			@Override
			public void call(JsonArray a) throws JsonException {
				try {
					/* Use for read parameters: getBoolean(0) / getNumber(1) / getString(2) */
					getState().initialized = (boolean) a.getBoolean(0); // initialized
					getState().type = (String) a.getString(1); // type of map
					getState().controls = getControls((JsonArray) a.getArray(2)); // controls
					getState().zoom = (int) a.getNumber(3); // Zoom factor
					getState().centerLatitude = (double) a.getNumber(4); // Center Lat
					getState().centerLongitude = (double) a.getNumber(5); // Center Lon
				} catch (JsonException e) {
// Ignore
				}
			}
		});

	}

	public void addInitializedListener(MapNotifyEventListener listener) {
		initializedListeners.add(listener);
		if (!getState().initializedRpc) {
			getState().initializedRpc = true;
			callInitRpc = true;
		}
	}

	public void addBoundsChangedListener(MapNotifyEventListener listener) {
		boundsChangedListeners.add(listener);
		if (!getState().boundsChangedRpc) {
			getState().boundsChangedRpc = true;
			callInitRpc = true;
		}
	}

	/*
	 *
	 *  Events Listeners
	 *
	 */

	public void addMarkerClickListener(MapMarkerClickListener listener) {
		markerClickedListeners.add(listener);
		if (!getState().markerClickedRpc) {
			getState().markerClickedRpc = true;
			callInitRpc = true;
		}
	}

	/**
	 * Types of map
	 */
	public enum MapType {
		MAP("yandex#map"),
		SATELLITE("yandex#satellite"),
		HYBRID("yandex#hybrid");

		private String name;

		MapType(String name) {
			this.name = name;
		}

		public static MapType getByName(final String viewName) {
			MapType result = null;
			for (MapType type : values()) {
				if (type.getName().equals(viewName)) {
					result = type;
					break;
				}
			}
			return result;
		}

		public String getName() {
			return name;
		}
	}

	/**
	 * Map controls
	 */
	public enum Control {
		MAPTOOLS("mapTools"),
		MINIMAP("miniMap"),
		SCALELINE("scaleLine"),
		SEARCHCONTROL("searchControl"),
		TRAFFICCONTROL("trafficControl"),
		TYPESELECTOR("typeSelector"),
		ZOOMCONTROL("zoomControl"),
		SMALLZOOMCONTROL("smallZoomControl");

		private String name;

		Control(String name) {
			this.name = name;
		}

		public static Control getByName(final String viewName) {
			Control result = null;
			for (Control control : values()) {
				if (control.getName().equals(viewName)) {
					result = control;
					break;
				}
			}
			return result;
		}

		public String getName() {
			return name;
		}
	}
}