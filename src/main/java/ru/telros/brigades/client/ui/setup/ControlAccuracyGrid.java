package ru.telros.brigades.client.ui.setup;

import com.vaadin.data.Binder;
import com.vaadin.ui.TextField;
import ru.telros.brigades.client.dto.SlaControlAccuracyDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class ControlAccuracyGrid extends BrigadesGrid<SlaControlAccuracyDto> {
	public ControlAccuracyGrid(List<SlaControlAccuracyDto> items) {
		super(SetupConstants.getControlAccuracyColumns(), items);
		//removeFilterRow();
		setSelectionMode(SelectionMode.NONE);
		Binder<SlaControlAccuracyDto> binder = this.getEditor().getBinder();
		TextField field = new TextField();
 			this.getColumn("value").setEditorBinding(binder.bind(field,SlaControlAccuracyDto::getValue,SlaControlAccuracyDto::setValue));
			this.getEditor().setSaveCaption("Сохранить");
			this.getEditor().addSaveListener(e->{
				//TODO-K вписать сюда, в UserActionGrid и в ControlAccuracyGrid логику сохранения
				// измененных настроек в БД, как только появится соотв. методы
			});
			this.getEditor().setCancelCaption("Отменить");
//


		this.getEditor().setEnabled(true);
	}

	@Override
	protected void onDoubleClick(SlaControlAccuracyDto entity, Column column) {

	}

	@Override
	protected void onClick(SlaControlAccuracyDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(SlaControlAccuracyDto entity, int row) {

	}

	@Override
	protected void onClick(SlaControlAccuracyDto entity, int row) {

	}
}
