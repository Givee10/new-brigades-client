package ru.telros.brigades.client.ui.person;

import ru.telros.brigades.client.dto.EmployeeDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class EmployeeGrid extends BrigadesGrid<EmployeeDto> {
	public EmployeeGrid(List<EmployeeDto> items) {
		super(PersonColumn.getTestColumns(), items);
	}

	@Override
	protected void onClick(EmployeeDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(EmployeeDto entity, Column column) {

	}
}
