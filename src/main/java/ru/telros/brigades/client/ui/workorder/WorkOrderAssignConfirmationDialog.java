package ru.telros.brigades.client.ui.workorder;

import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.attachment.AttachmentColumn;
import ru.telros.brigades.client.ui.attachment.AttachmentGrid;
import ru.telros.brigades.client.ui.component.ReadOnlyTextField;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.time.ZonedDateTime;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

/**
 * Окно принятия задания бригаде
 */
@SuppressWarnings("serial")
public class WorkOrderAssignConfirmationDialog extends WindowWithButtons {
	private final WorkDto workOrder;
	private final VerticalLayout layout = new VerticalLayout();
	private boolean isAccepted;

	private WorkOrderAssignConfirmationDialog(WorkDto workOrder) {
		super("Действительно отправить задание бригаде?", 600, -1);
		this.workOrder = workOrder;
		// setCaption("Действительно отправить задание бригаде?");
		addToContent(buildContent());
		// addToButtonBar("Скорректировать", $ -> sendResult(false));
		addToButtonBar("Отправить", $ -> sendResult(true));
		addToButtonBar("Не отправлять", $ -> sendResult(false));

		BrigadesEventBus.register(this);
		addCloseListener(closeEvent -> BrigadesEventBus.unregister(this));
	}

	public static WorkOrderAssignConfirmationDialog open(WorkDto workOrder) {
		WorkOrderAssignConfirmationDialog window = new WorkOrderAssignConfirmationDialog(workOrder);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
		return window;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	private void sendResult(boolean isAccepted) {
		this.isAccepted = isAccepted;
		close();
	}

	private Component buildContent() {
		layout.addComponent(getDataField("Бригада", workOrder.getBrigade()));
		layout.addComponent(getDataField(msg("table.workorder.address"),
				(workOrder.getAddress() != null ? workOrder.getAddress() : "(не указан)")));
		layout.addComponent(getDataField("Выполнить", workOrder.getDescription()));
		layout.addComponent(buildDateTimeFileds());
		layout.addComponent(buildAttachmentsInfo());
		return layout;
	}

	private HorizontalLayout buildDateTimeFileds() {
		HorizontalLayout dateTimeLayout = new HorizontalLayout();
		dateTimeLayout.setSizeFull();
		dateTimeLayout.setSpacing(true);
		dateTimeLayout.addComponent(getDateField(msg("table.workorder.startDatePlan"), workOrder.getStartDatePlan()));
		dateTimeLayout.addComponent(getDateField(msg("table.workorder.finishDatePlan"), workOrder.getFinishDatePlan()));
		// layout.addComponent(getDataField("Окончание", DatesUtil.formatNormal(workOrder.getFinishDatePlan())));
		return dateTimeLayout;
	}

	private VerticalLayout buildAttachmentsInfo() {
		VerticalLayout attachments = new VerticalLayout();
		attachments.setSpacing(true);
		List<AttachmentDto> workAttachments = BrigadesUI.getRestTemplate().getWorkAttachments(workOrder.getId());
		boolean hasAttachments = !workAttachments.isEmpty();
		HorizontalLayout attachmentsRow = new HorizontalLayout();
		attachmentsRow.setSizeFull();
		Component attachmentsCountField = getDataField("Вложения", (hasAttachments ? workAttachments.size() : "(нет)"));
		attachmentsRow.addComponent(attachmentsCountField);
		attachmentsRow.setExpandRatio(attachmentsCountField, 1);

		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setMargin(new MarginInfo(false, false, false, true));
		attachmentsRow.addComponent(buttonLayout);
		attachmentsRow.setComponentAlignment(buttonLayout, Alignment.BOTTOM_LEFT);
		attachmentsRow.setExpandRatio(buttonLayout, 7);

		attachments.addComponent(attachmentsRow);
		if (hasAttachments) {
			AttachmentGrid attachmentTable = new AttachmentGrid(AttachmentColumn.getMainColumns(), workAttachments);
			attachmentTable.setHeight(150, Sizeable.Unit.PIXELS);
			attachmentTable.setVisible(false);

			Button showAttachmentsButton = new Button("Показать");
			showAttachmentsButton.addClickListener(e -> {
				attachmentTable.setVisible(!attachmentTable.isVisible());
				showAttachmentsButton.setCaption(attachmentTable.isVisible() ? "Скрыть" : "Показать");
			});
			buttonLayout.addComponent(showAttachmentsButton);
			buttonLayout.setComponentAlignment(showAttachmentsButton, Alignment.BOTTOM_LEFT);
			attachments.addComponent(attachmentTable);
		}
		return attachments;
	}

	private Component getDataField(String caption, Object value) {
		ReadOnlyTextField textField = new ReadOnlyTextField(caption);
		textField.setValue(String.valueOf(value));
		return textField;
	}

	private Component getDateField(String caption, ZonedDateTime date) {
		DateTimeField dateField = new DateTimeField(caption);
		dateField.setValue(DatesUtil.zonedToLocal(date));
		dateField.setSizeFull();
		dateField.setReadOnly(true);
		return dateField;
	}

}
