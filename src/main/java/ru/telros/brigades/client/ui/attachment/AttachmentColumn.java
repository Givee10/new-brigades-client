package ru.telros.brigades.client.ui.attachment;

import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.ui.grid.GridColumn;

import java.util.Arrays;
import java.util.List;

public class AttachmentColumn {
	public static final GridColumn<AttachmentDto> DATE = GridColumn.createDateColumn("date", "Дата/время добавления", AttachmentDto::getCreateDate);
	public static final GridColumn<AttachmentDto> TYPE = GridColumn.createColumn("type", "Тип", AttachmentDto::getType);
	public static final GridColumn<AttachmentDto> SOURCE = GridColumn.createColumn("source", "Источник", AttachmentDto::getSource);
	public static final GridColumn<AttachmentDto> PERSON = GridColumn.createColumn("person", "ФИО", AttachmentDto::getFullName);
	public static final GridColumn<AttachmentDto> COMMENT = GridColumn.createColumn("comment", "Комментарий", AttachmentDto::getDescription);
	public static final GridColumn<AttachmentDto> FILENAME = GridColumn.createColumn("filename", "Имя файла", AttachmentDto::getFileName);
	public static final GridColumn<AttachmentDto> DEVICE = GridColumn.createBooleanColumn("device", "Планшет бригады", AttachmentDto::getMobile, "+");

	public static List<GridColumn<AttachmentDto>> getMainColumns() {
		return Arrays.asList(DATE, TYPE, SOURCE, PERSON, COMMENT, FILENAME, DEVICE);
	}
}
