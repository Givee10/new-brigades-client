package ru.telros.brigades.client.ui.maplayout;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Таймер, выполняющий определенную задачу (TimerTask)
 * с заданной задержкой и возможностью перезапуска
 * <p>
 * Используется в ситуациях, когда, например, необходимо сохранение состояния карты,
 * но чтобы постоянно не сохраняться при любых манипуляциях с картой (изменение масштаба, координат центра и т.п.),
 * делается задержка delay(мс) на запуск TimerTask, и если в течении времени задержки пользователем никакие манипуляции
 * произведены не были, то задача выполнится; в противном случае таймер перезапустится, и задача будет перенесена
 * снова на время delay, и так далее
 */
public class TaskTimer {

	private final static long DEFAULT_DELAY_MS = 1500;
	private final Task task;
	private final long delay;
	private Timer timer = null;

	public TaskTimer(Task task) {
		this(task, DEFAULT_DELAY_MS);
	}

	public TaskTimer(Task task, long delay) {
		Objects.requireNonNull(task);
		if (delay <= 0) {
			throw new IllegalArgumentException("delay must be greater than 0");
		}
		this.task = task;
		this.delay = delay;
	}

	/**
	 * Прерывание работающего таймера и постановка задачи в очередь выполнения с задержкой
	 */
	public void call() {
		call(false);
	}

	/**
	 * 1) Прерывание работающего таймера
	 * 2) Немедленный запуск задачи (immediately=true)
	 * или постановка задачи в очередь на выполнение с задержкой (immediately=false)
	 *
	 * @param immediately
	 */
	public void call(boolean immediately) {
		if (timer != null) {
			timer.cancel();
		}
		if (!immediately) {
			timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					TaskTimer.this.task.run();
				}
			}, delay);
		} else {
			task.run();
		}
	}

	public interface Task {
		void run();
	}
}
