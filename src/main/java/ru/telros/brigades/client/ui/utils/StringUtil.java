package ru.telros.brigades.client.ui.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.server.SerializablePredicate;
import org.apache.commons.codec.binary.Base64;
import ru.telros.brigades.client.event.BeanUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	public static String abbreviate(String str, int maxWidth) {
		return abbreviate(str, 0, maxWidth);
	}

	public static String abbreviate(String str, int offset, int maxWidth) {
		if (str == null) {
			return null;
		} else if (maxWidth < 4) {
			throw new IllegalArgumentException("Minimum abbreviation width is 4");
		} else if (str.length() <= maxWidth) {
			return str;
		} else {
			if (offset > str.length()) offset = str.length();
			if (str.length() - offset < maxWidth - 3) offset = str.length() - (maxWidth - 3);
			String abrevMarker = "...";
			if (offset <= 4) {
				return str.substring(0, maxWidth - 3) + abrevMarker;
			} else if (maxWidth < 7) {
				throw new IllegalArgumentException("Minimum abbreviation width with offset is 7");
			} else {
				return offset + maxWidth - 3 < str.length() ?
						abrevMarker + abbreviate(str.substring(offset), maxWidth - 3) :
						abrevMarker + str.substring(str.length() - (maxWidth - 3));
			}
		}
	}

	public static SerializablePredicate<String> nullPredicate() {
		return s -> !isNull(s);
	}

	public static Boolean isNull(String s) {
		return s == null || s.trim().isEmpty() || s.equalsIgnoreCase("null");
	}

	public static String writeValueAsString(Object value) {
		ObjectMapper objectMapper = BeanUtil.getBean(ObjectMapper.class);
		try {
			return objectMapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String returnString(String value) {
		//String.valueOf() returns "null", that's bad
		return value == null ? "" : value;
	}

	public static <T> String returnDebugString(Collection<T> collection) {
		if (collection.isEmpty()) return "[]";
		String result = "";
		for (T t : collection) {
			result = result.concat("[").concat(writeValueAsString(t)).concat("] ");
		}
		return result;
	}

	public static String splitHtmlString(String value, Integer size) {
		String result = "";
		Pattern p = Pattern.compile("\\b.{1," + (size-1) + "}\\b\\W?");
		Matcher m = p.matcher(value);
		while(m.find()) {
			result = result.concat("\n<BR>").concat(m.group());
		}
		return result;
	}

	public static void decodeBase64ToFile(String value, String filename) throws IOException {
		byte[] bytes = Base64.decodeBase64(value.getBytes(StandardCharsets.US_ASCII));
		Path destinationFile = Paths.get(FileUtil.getDirectory().toString(), filename);
		Files.write(destinationFile, bytes);
	}

	public static String encodeFileToBase64(String url) throws IOException {
		File file = new File(url);
		return encodeFileToBase64(file);
	}

	public static String encodeFileToBase64(File file) throws IOException {
		byte[] bytes = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
		return new String(bytes, StandardCharsets.US_ASCII);
	}

	public static String encodeFileToBase64(byte[] content) {
		byte[] bytes = Base64.encodeBase64(content);
		return new String(bytes, StandardCharsets.US_ASCII);
	}
}
