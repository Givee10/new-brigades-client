package ru.telros.brigades.client.ui.maplayout;

import java.io.Serializable;
import java.util.HashMap;

public class YandexMapComponentOptions extends HashMap<String, Object> implements Serializable {

	private static String OPTION_MAXZOOM = "maxZoom";
	private static String OPTION_MINZOOM = "minZoom";
	private static String OPTION_AVOIDFRACTIONALZOOM = "avoidFractionalZoom";
	private static String OPTION_RESTRICTMAPAREA = "restrictMapArea";

	public YandexMapComponentOptions() {
	}

	/**
	 * Return default settings of component
	 *
	 * @return
	 */
	public static YandexMapComponentOptions getDefaultOptions() {
		YandexMapComponentOptions opts = new YandexMapComponentOptions();
		opts.put(OPTION_MINZOOM, 0);
		opts.put(OPTION_MAXZOOM, 23);
		opts.put(OPTION_AVOIDFRACTIONALZOOM, true);
		opts.put(OPTION_RESTRICTMAPAREA, false);
		return opts;
	}


}
