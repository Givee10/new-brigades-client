package ru.telros.brigades.client.ui.component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrigadesStatistic extends HorizontalLayout implements HasLogger {
	private Map<String, Component> components = new HashMap<>();
	private Label summary = new Label();
	private String summaryCaption = "Сводка";

	public BrigadesStatistic(ReadOnlyTextField... fields) {
		this(Arrays.asList(fields));
	}

	public BrigadesStatistic(List<ReadOnlyTextField> fields) {
		setWidth(100, Unit.PERCENTAGE);
		setHeight(40, Unit.PIXELS);
		setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
		setSpacing(true);
		setMargin(new MarginInfo(false, true));
		//setColumns(fields.size());
		fields.forEach(field -> {
			components.put(field.getCaption(), field);
			field.setDescription(field.getCaption());
			addComponent(CompsUtil.getStatisticWrapper(field));
		});
		summary.setContentMode(ContentMode.HTML);
		summary.setValue(VaadinIcons.TABLE.getHtml() + VaadinIcons.PIE_CHART.getHtml());
		summary.setCaption(summaryCaption);
		summary.setDescription(summaryCaption);
		summary.addContextClickListener(event -> {
			getLogger().debug("RELATIVE [{}, {}], CLIENT [{}, {}]", event.getRelativeX(), event.getRelativeY(), event.getClientX(), event.getClientY());
			Notification.show("Функция недоступна", Notification.Type.TRAY_NOTIFICATION);
		});
		components.put(summary.getCaption(), summary);
	}

	public void addSummary(int index) {
		addComponent(CompsUtil.getStatisticWrapper(summary), index);
	}

	public void isMaximized(boolean maximized) {
		if (maximized) {
			components.keySet().forEach(key -> components.get(key).setCaption(key));
			for (int i = 0; i < getComponentCount(); i++)
				getComponent(i).setVisible(true);
		} else {
			components.keySet().forEach(key -> components.get(key).setCaption(StringUtil.abbreviate(key, 50 / components.size())));
			for (int i = 5; i < getComponentCount(); i++)
				getComponent(i).setVisible(false);
		}
	}
}
