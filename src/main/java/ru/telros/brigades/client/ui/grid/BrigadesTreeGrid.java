package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.ui.Component;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.renderers.ComponentRenderer;

import java.util.List;

import static ru.telros.brigades.client.ui.grid.GridColumn.COLUMN_WIDTH;
import static ru.telros.brigades.client.ui.grid.GridColumn.LAYOUT_WIDTH;

public abstract class BrigadesTreeGrid<T> extends TreeGrid<T> {
	private final List<GridColumn<T>> columns;
	private TreeData<T> treeData = new TreeData<>();
	private TreeDataProvider<T> treeDataProvider = new TreeDataProvider<>(treeData);

	public BrigadesTreeGrid(List<GridColumn<T>> columns) {
		this.columns = columns;
		setDataProvider(treeDataProvider);
		setSizeFull();
		initColumns();
		setSelectionMode(SelectionMode.NONE);
	}

	public void expandAll() {
		expandRecursively(getRootItems(), 4);
	}

	public void collapseAll() {
		collapseRecursively(getRootItems(), 4);
	}

	public T getSelectedRow() {
		return getSelectedItems().stream().findFirst().orElse(null);
	}

	public List<T> getRootItems() {
		return treeData.getRootItems();
	}

	public List<T> getChildren(T item) {
		return treeData.getChildren(item);
	}

	public List<T> getAllItems() {
		return getDataCommunicator().fetchItemsWithRange(0, getDataCommunicator().getDataProviderSize());
	}

	public void clearItems() {
		treeData.clear();
	}

	public void setRootItem(T item) {
		treeData.addItem(null, item);
	}

	public void setParentItem(T parent, T item) {
		treeData.addItem(parent, item);
	}

	public void refreshItem(T item) {
		treeDataProvider.refreshItem(item);
	}

	public void refreshAll() {
		treeDataProvider.refreshAll();
	}

	private void initColumns() {
		for (GridColumn<T> column : columns) {
			Column<T, ?> col = addColumn(column.getValueProvider()).setId(column.getId()).setCaption(column.getCaption());
			col.setSortable(false);
			if (column.isComponent()) {
				col.setResizable(false);
				col.setWidth(LAYOUT_WIDTH);
				((Column<T, Component>) col).setRenderer(new ComponentRenderer());
			} else {
				col.setWidth(COLUMN_WIDTH);
				col.setDescriptionGenerator(column.getDescriptionGenerator());
				if (column.isDate())
					col.setComparator(new BrigadesDateComparator<>(column.getValueProvider()));
			}
			getDefaultHeaderRow().getCell(col).setDescription(column.getCaption());
		}
	}
}
