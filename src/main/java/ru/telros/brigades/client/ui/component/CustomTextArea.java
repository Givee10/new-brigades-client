package ru.telros.brigades.client.ui.component;

import com.vaadin.ui.TextArea;
import ru.telros.brigades.client.ui.utils.StringUtil;

public class CustomTextArea extends TextArea {

	public CustomTextArea() {
		setWidth(100, Unit.PERCENTAGE);
		setRows(3);
	}

	public CustomTextArea(final String caption) {
		this();
		setCaption(caption);
	}

	@Override
	public void setValue(String value) {
		super.setValue(StringUtil.returnString(value));
	}
}
