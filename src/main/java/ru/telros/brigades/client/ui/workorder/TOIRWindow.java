package ru.telros.brigades.client.ui.workorder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.server.Responsive;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BeanUtil;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.ui.component.CustomButton;
import ru.telros.brigades.client.ui.component.CustomDateTimeField;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static ru.telros.brigades.client.MessageManager.msg;

public class TOIRWindow extends WindowWithButtons implements HasLogger {
	private static final int WIDTH_PIXELS = 800;
	private static final int HEIGHT_PIXELS = -1;

	private ObjectMapper objectMapper = BeanUtil.getBean(ObjectMapper.class);
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();
	private DisabledTextField wipEntityId = new DisabledTextField("Номер работы для ТОИР");
	private DisabledTextField inventoryItemId = new DisabledTextField("Номер инвентаря");
	private CustomDateTimeField scheduleStartDate = new CustomDateTimeField("Дата начала работы");
	private DisabledTextField calcValue = new DisabledTextField("Рассчитаное значение");
	private DisabledTextField postValue = new DisabledTextField("Должность");

	private EOGrid eoGrid = new EOGrid();
	private MetricGrid metricGrid = new MetricGrid();
	private OperationGrid operationGrid = new OperationGrid();
	private WorkHeadDto workHeadDto = new WorkHeadDto();

	private final TypeWorkDto typeWorkDto;
	private final ZonedDateTime startDate;

	public TOIRWindow(TypeWorkDto typeWorkDto, ZonedDateTime startDate) {
		super("Расчет нормативов", WIDTH_PIXELS, HEIGHT_PIXELS);
		this.typeWorkDto = typeWorkDto;
		this.startDate = startDate;

		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		buildFields();
		bindFields();

		removeAllCloseShortcuts();
		addCloseListener(closeEvent -> BrigadesEventBus.unregister(this));
	}

	private void buildFields() {
		eoGrid.setCaption("Элементы обслуживания");
		eoGrid.enableEditor();
		metricGrid.setCaption("Метрики");
		metricGrid.enableEditor();
		operationGrid.setCaption("Детали работы");

		CustomButton addEOButton = new CustomButton(BrigadesTheme.ICON_ADD, msg("button.add"), $ -> addEO());
		CustomButton removeEOButton = new CustomButton(BrigadesTheme.ICON_DELETE, msg("button.delete"), $ -> deleteEO());
		CustomButton editEOButton = new CustomButton(BrigadesTheme.ICON_EDIT, msg("button.edit"), $ -> editEO());

		CustomButton addMetricButton = new CustomButton(BrigadesTheme.ICON_ADD, msg("button.add"), $ -> addMetric());
		CustomButton removeMetricButton = new CustomButton(BrigadesTheme.ICON_DELETE, msg("button.delete"), $ -> deleteMetric());
		CustomButton editMetricButton = new CustomButton(BrigadesTheme.ICON_EDIT, msg("button.edit"), $ -> editMetric());

		CustomButton removeOperationButton = new CustomButton(BrigadesTheme.ICON_DELETE, msg("button.delete"), $ -> deleteOperation());

		HorizontalLayout topLayout = CompsUtil.getHorizontalWrapperNoMargin(wipEntityId, inventoryItemId, scheduleStartDate);
		//Component eoComponent = CompsUtil.buildGridWithButtons(eoGrid, addEOButton, removeEOButton, editEOButton);
		//Component metricComponent = CompsUtil.buildGridWithButtons(metricGrid, addMetricButton, removeMetricButton, editMetricButton);
		//Component operationComponent = CompsUtil.buildGridWithButtons(operationGrid, removeOperationButton);
		HorizontalLayout bottomLayout = CompsUtil.getHorizontalWrapperNoMargin(calcValue, postValue);

		addToContent(CompsUtil.getVerticalWrapperWithMargin(topLayout, eoGrid, metricGrid, operationGrid, bottomLayout), 1);
		addToButtonBar("Рассчитать", $ -> calculate());
		addToButtonBar(msg("button.save"), $ -> close());
		addToButtonBar(msg("button.cancel"), $ -> close());
		setButtonEnabled(1, false);
	}

	private void bindFields() {
		//String workNum = "123456789";
		String workNum = String.valueOf(getRandomNumberInRange(900000000, 999999999));
		TOIRCollectionDto toirCollection = restTemplate.getTOIRCollection(typeWorkDto.getCode());
		if (toirCollection != null) {
			String itemId = toirCollection.getInventoryItemId();
			List<EODto> eoDtoList = toirCollection.getEoDtoList();
			eoDtoList.forEach(eoDto -> eoDto.setWipEntityId(workNum));
			List<MetricDto> metricDtoList = toirCollection.getMetricDtoList();
			metricDtoList.forEach(metricDto -> metricDto.setWipEntityId(workNum));
			List<OperationDto> operationDtoList = toirCollection.getOperationDtoList();
			operationDtoList.forEach(operationDto -> operationDto.setWipEntityId(workNum));
			eoGrid.setItems(eoDtoList);
			metricGrid.setItems(metricDtoList);
			operationGrid.update(operationDtoList);

			inventoryItemId.setValue(itemId);
			wipEntityId.setValue(workNum);
			scheduleStartDate.setValue(DatesUtil.zonedToLocal(startDate));

			workHeadDto.setWipEntityId(workNum);
			workHeadDto.setInventoryItemId(itemId);
			workHeadDto.setScheduledStartDate(DatesUtil.formatToOracle(startDate));
		}
	}

	private static int getRandomNumberInRange(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	private void calculate() {
		String workId = workHeadDto.getWipEntityId();
		restTemplate.sendHeaderToTOIR(workHeadDto);
		eoGrid.getItems().forEach(eoDto -> restTemplate.sendEOToTOIR(eoDto));
		metricGrid.getItems().forEach(metricDto -> restTemplate.sendMetricToTOIR(metricDto));
		operationGrid.getItems().forEach(operationDto -> restTemplate.sendOperationToTOIR(operationDto));
		restTemplate.sendCalcToTOIR(workId);

		String calcNormFromTOIR = restTemplate.getCalcNormFromTOIR(workId);
		if (calcNormFromTOIR != null) {
			try {
				WorkCalcNormDto[] readValue = objectMapper.readValue(calcNormFromTOIR, WorkCalcNormDto[].class);
				List<WorkCalcNormDto> normDtoList = Arrays.asList(readValue);
				if (!normDtoList.isEmpty()) {
					WorkCalcNormDto workCalcNorm = normDtoList.get(0);
					calcValue.setValue(workCalcNorm.getCalcNorm());
					postValue.setValue(workCalcNorm.getResourceCode());
				}
			} catch (IOException e) {
				getLogger().error(e.getMessage());
			}
		}

		String calcNormDetailFromTOIR = restTemplate.getCalcNormDetailFromTOIR(workId);
		if (calcNormDetailFromTOIR != null) {
			try {
				WorkItemCalcNormDto[] result = objectMapper.readValue(calcNormDetailFromTOIR, WorkItemCalcNormDto[].class);
				if (result != null) {
					for (OperationDto operationDto : operationGrid.getItems()) {
						for (WorkItemCalcNormDto itemCalcNorm : result) {
							if (operationDto.getOperationName().equalsIgnoreCase(itemCalcNorm.getOperationDescription())) {
								operationDto.setCalcResult(itemCalcNorm.getCalcNorm());
							}
						}
					}
					operationGrid.refreshAll();
				}
			} catch (IOException e) {
				getLogger().error(e.getMessage());
			}
		}
	}

	private void addEO() {

	}

	private void deleteEO() {

	}

	private void editEO() {

	}

	private void addMetric() {

	}

	private void deleteMetric() {

	}

	private void editMetric() {

	}

	private void deleteOperation() {

	}

	public static void open(final TypeWorkDto typeWorkDto, final ZonedDateTime startDate) {
		Window window = new TOIRWindow(typeWorkDto, startDate);
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}
}
