package ru.telros.brigades.client.ui.workorder;

import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.server.Responsive;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.PagesDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.dto.WorkJournalDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.workorder.ChangeWorkOrderEvent;
import ru.telros.brigades.client.event.workorder.SelectWorkOrderEvent;
import ru.telros.brigades.client.event.workorder.UpdateWorkOrdersEvent;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.BrigadesStatistic;
import ru.telros.brigades.client.ui.component.FullSizeVerticalLayout;
import ru.telros.brigades.client.ui.component.ReadOnlyTextField;
import ru.telros.brigades.client.ui.window.YesNoWindow;

import java.time.ZonedDateTime;

import static ru.telros.brigades.client.MessageManager.msg;

public class WorkOrderLayout extends FullSizeVerticalLayout {
	//private final WorkOrderGrid workOrderGrid = new WorkOrderGrid(WorkOrderConstants.getMainColumns(), new ArrayList<>());
	//private GridContextMenu<WorkDto> contextMenu = new GridContextMenu<>(workOrderGrid);
	private WorkJournalGrid journalGrid = new WorkJournalGrid();
	//private WorkOrderJournalGrid journalGrid = new WorkOrderJournalGrid();
	private GridContextMenu<WorkJournalDto> contextMenu = new GridContextMenu<>(journalGrid);

	private ReadOnlyTextField work = new ReadOnlyTextField("Выполняются бригадами");
	private ReadOnlyTextField exceeded = new ReadOnlyTextField("Нарушены сроки");
	private BrigadesStatistic statistic = new BrigadesStatistic(work, exceeded);
	private Boolean updateVisible = true;

	public WorkOrderLayout() {
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		setMargin(false);
		setSpacing(false);

		contextMenu.addGridHeaderContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridBodyContextMenuListener(event -> selectGridItem(event.getItem()));
		contextMenu.addGridFooterContextMenuListener(event -> selectGridItem(event.getItem()));

		statistic.addSummary(2);
		statistic.isMaximized(false);
		//addComponent(statistic);
		//addComponentAndRatio(workOrderGrid, 1);

		journalGrid.sort(WorkOrderConstants.REGISTRATION_DATE.getId(), SortDirection.DESCENDING);
		addComponentAndRatio(journalGrid, 1);

		//update();
	}

	public static void update(WorkDto workDto) {
		BrigadesEventBus.post(new ChangeWorkOrderEvent(workDto));
	}

	public static void refresh() {
		BrigadesEventBus.post(new UpdateWorkOrdersEvent());
	}

	public static void createWorkOrder(RequestDto requestDto) {
		final WorkDto workOrder = new WorkDto();

		workOrder.setRegistrationDate(ZonedDateTime.now());
		workOrder.setStartDatePlan(ZonedDateTime.now().withSecond(0).withNano(0));
		workOrder.setAuthor(BrigadesUI.getCurrentUser());
		workOrder.setStatus(WorkDto.Status.NEW.getCode());
		workOrder.setUrgency("Обычная");
		if (requestDto != null) {
			workOrder.setAddress(requestDto.getAddress());
			workOrder.setUpdatedAddress(requestDto.getUpdatedAddress());
			workOrder.setOrganization(requestDto.getOrganization());
		} else {
			workOrder.setOrganization("РВ Юго-Западный");
		}
		WorkOrderWindow.open(workOrder, requestDto);
	}

	public static void showWorkOrder(WorkDto workOrder) {
		if (workOrder != null) {
			//WorkOrderStateWindow.open(workOrder);
			WorkOrderWindow.open(workOrder, null);
		} else {
			Notification.show("Ошибка при получении данных работы", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void selectWorkOrder(WorkDto workOrder) {
		if (workOrder != null) {
			BrigadesEventBus.post(new SelectWorkOrderEvent(workOrder));
		} else {
			Notification.show("Ошибка при получении данных работы", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void assignWorkOrder(WorkDto workOrder) {
		if (workOrder != null && workOrder.getBrigade() != null &&
				!WorkDto.Status.ASSIGNED.getCode().equals(workOrder.getStatus()) &&
				!WorkDto.Status.COMPLETE.getCode().equals(workOrder.getStatus())) {
			WorkOrderAssignConfirmationDialog workOrderAssignConfirmationDialog =
					WorkOrderAssignConfirmationDialog.open(workOrder);
			workOrderAssignConfirmationDialog.addCloseListener((Window.CloseListener) e -> {
				if (((WorkOrderAssignConfirmationDialog) e.getWindow()).isAccepted()) {
					//workOrder.setStatus("ASSIGNED");
					/*workOrder.getChildrenId().forEach(id -> {
						WorkDto stage = BrigadesUI.getRestTemplate().getWork(id);
						stage.setStatus("ASSIGNED");
						BrigadesUI.getRestTemplate().saveWork(stage);
					});*/
					WorkDto savedWork = BrigadesUI.getRestTemplate().saveWork(workOrder);
					refresh();
				}
			});
		} else {
			Notification.show("Ошибка при получении данных работы", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void deleteWorkOrder(WorkDto workOrder) {
		if (workOrder != null) {
			if (WorkOrderConstants.canDeleteWork(workOrder.getStatus())) {
				String prompt = "Вы действительно хотите удалить работу " + workOrder.toString() + "?";
				YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
				confirmationDialog.addCloseListener((Window.CloseListener) e -> {
					if (((YesNoWindow) e.getWindow()).isModalResult()) {
						BrigadesUI.getRestTemplate().deleteWork(workOrder.getId());
						refresh();
					}
				});
			} else {
				Notification.show(msg("error"), "Невозможно удалить работу " + workOrder.toString(), Notification.Type.ERROR_MESSAGE);
			}
		} else {
			Notification.show("Ошибка при получении данных работы", Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void closeWorkOrder(WorkDto workOrder) {
		if (workOrder != null) {
			if (WorkOrderConstants.canCloseWork(workOrder.getStatus())) {
				String prompt = "Вы действительно хотите закрыть работу " + workOrder.getNumber() + " в РВ?";
				YesNoWindow confirmationDialog = YesNoWindow.open(msg("warning"), prompt, true);
				confirmationDialog.addCloseListener((Window.CloseListener) e -> {
					if (((YesNoWindow) e.getWindow()).isModalResult()) {
						WorkDto workDto = BrigadesUI.getRestTemplate().changeWorkStatus(workOrder.getId(), WorkDto.Status.CLOSED.getCode());
						if (workDto != null) {
							Notification.show("Работа " + workDto.getNumber() + " закрыта в РВ");
							refresh();
						} else {
							Notification.show("Произошла ошибка при смене статуса работы", Notification.Type.ERROR_MESSAGE);
						}
					}
				});
			} else {
				Notification.show(msg("error"), "Невозможно закрыть работу " + workOrder.toString(), Notification.Type.ERROR_MESSAGE);
			}
		}
	}

	public static void redirectWorkOrder(WorkDto workOrder) {
		if (workOrder != null) {
			//restClient.redirectWorkOrder(workOrder.getId());
			Notification.show(msg("error"), "Невозможно перенаправить работу " + workOrder.toString(), Notification.Type.ERROR_MESSAGE);
		}
	}

	public static void suspendWorkOrder(WorkDto workOrder) {
		if (workOrder != null) {
			//restClient.suspendWorkOrder(workOrder.getId());
			Notification.show(msg("error"), "Невозможно приостановить работу " + workOrder.toString(), Notification.Type.ERROR_MESSAGE);
		}
	}

	private void selectGridItem(WorkJournalDto journalDto) {
		contextMenu.removeItems();
		MenuBar.Command commandRefresh = (MenuBar.Command) menuItem -> update();
		MenuBar.Command commandAdd = (MenuBar.Command) menuItem -> createWorkOrder(null);

		if (journalDto != null) {
			journalGrid.select(journalDto);
			WorkDto workOrder = BrigadesUI.getRestTemplate().getWork(journalDto.getId());
			MenuBar.Command commandFollow = (MenuBar.Command) menuItem -> selectWorkOrder(workOrder);
			//MenuBar.Command commandShow = (MenuBar.Command) menuItem -> showWorkOrder(workOrder);
			MenuBar.Command commandEdit = (MenuBar.Command) menuItem -> showWorkOrder(workOrder);
			MenuBar.Command commandAssign = (MenuBar.Command) menuItem -> assignWorkOrder(workOrder);
			MenuBar.Command commandDelete = (MenuBar.Command) menuItem -> deleteWorkOrder(workOrder);
			//MenuBar.Command commandSuspend = (MenuBar.Command) menuItem -> suspendWorkOrder(workOrder);
			//MenuBar.Command commandRedirect = (MenuBar.Command) menuItem -> redirectWorkOrder(workOrder);
			MenuBar.Command commandClose = (MenuBar.Command) menuItem -> closeWorkOrder(workOrder);

			contextMenu.addItem(msg("menu.gotoaddress"), BrigadesTheme.ICON_MAP, commandFollow);
			contextMenu.addSeparator();
			//contextMenu.addItem(msg("menu.open"), BrigadesTheme.ICON_WORKORDER, commandShow);
			contextMenu.addItem(msg("menu.workorder.edit"), BrigadesTheme.ICON_EDIT, commandEdit);
			contextMenu.addSeparator();
			//contextMenu.addItem(msg("menu.workorder.redirect"), BrigadesTheme.ICON_REDIRECT, commandRedirect);
			//contextMenu.addItem(msg("menu.workorder.suspend"), BrigadesTheme.ICON_SUSPEND, commandSuspend);
			contextMenu.addItem(msg("menu.workorder.close"), BrigadesTheme.ICON_CLOSE, commandClose);
			contextMenu.addItem(msg("menu.workorder.delete"), BrigadesTheme.ICON_DELETE, commandDelete);
			contextMenu.addSeparator();
//			if (workOrder.getBrigade() == null && WorkDto.Status.NEW.getCode().equals(workOrder.getStatus())) {
//				contextMenu.addItem(msg("menu.workorder.assigntoworkgroup"), BrigadesTheme.ICON_WORKGROUP, commandAssign);
//				contextMenu.addSeparator();
//			}
		} else journalGrid.deselectAll();
		contextMenu.addItem(msg("menu.workorder.new"), BrigadesTheme.ICON_ADD, commandAdd);
		contextMenu.addSeparator();
		contextMenu.addItem(msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, commandRefresh);

	}

	public void update() {
		if (updateVisible) {
			//workOrderGrid.update(BrigadesUI.getRestTemplate().getWorks());
			//journalGrid.refreshAll();
			Long countWorks = BrigadesUI.getRestTemplate().countWorks();
			if (countWorks != null && countWorks != 0) {
				PagesDto<WorkJournalDto> worksForJournal = BrigadesUI.getRestTemplate().getWorksForJournal(0, countWorks.intValue());
				journalGrid.update(worksForJournal.getCurrentContent());
			}
		}
	}

	public void addToolBarItems(BrigadesPanel panel) {
		panel.addToolbarItem("", msg("menu.workorder.new"), BrigadesTheme.ICON_ADD, (MenuBar.Command) menuItem -> doAddWorkOrder());
		panel.addToolbarItem("", msg("menu.workorder.edit"), BrigadesTheme.ICON_EDIT, (MenuBar.Command) menuItem -> doEditWorkOrder());
		panel.addLegendItem("", msg("menu.legend"), BrigadesTheme.ICON_LEGEND, WorkOrderConstants.workStyles);
		panel.addToolbarItem("", msg("menu.refresh"), BrigadesTheme.ICON_REFRESH, (MenuBar.Command) menuItem -> update());
	}

	private void doRefresh() {
		refresh();
	}

	private void doAddWorkOrder() {
		createWorkOrder(null);
	}

	private void doEditWorkOrder() {
//		WorkDto workOrder = workOrderGrid.getSelectedRow();
//		if (workOrder != null) {
//			editWorkOrder(workOrder);
//		}
		WorkJournalDto selectedRow = journalGrid.getSelectedRow();
		if (selectedRow != null) {
			WorkDto work = BrigadesUI.getRestTemplate().getWork(selectedRow.getId());
			showWorkOrder(work);
		}
	}

	private RequestDto getWorkOrderRequest(WorkDto workOrder) {
		if (workOrder != null && workOrder.getId() != null) {
			return BrigadesUI.getRestTemplate().getWorkRequest(workOrder.getId());
		}
		return null;
	}

	public void setStatisticVisible(boolean visible) {
		statistic.isMaximized(visible);
	}

	@Subscribe
	public void handleSelectWorkOrder(final SelectWorkOrderEvent event) {
		journalGrid.select(event.getEntity().getId());
		journalGrid.scrollTo(event.getEntity().getId());
	}

	public Boolean getUpdateVisible() {
		return updateVisible;
	}

	public void setUpdateVisible(Boolean updateVisible) {
		this.updateVisible = updateVisible;
		update();
	}
}
