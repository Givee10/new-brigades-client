package ru.telros.brigades.client.ui.vehicle;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.window.WindowWithButtons;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.telros.brigades.client.MessageManager.msg;

public class VehicleDemoExcelWindow extends WindowWithButtons {
	private static final int WIDTH_PIXELS = 1200;
	private static final int HEIGHT_PIXELS = -1;

	public VehicleDemoExcelWindow() {
		super("Адресная программа", WIDTH_PIXELS, HEIGHT_PIXELS);
		// Подразделение/камера
		// Вид работ
		// Адрес работ
		// Тип техники
		// Гос№ а/м
		// Адрес подачи
		// Комментарий
		// Время подачи
		// Примечание
		// Ответственный

		List<BrigadeDto> brigades = BrigadesUI.getRestTemplate().getBrigades();
		//brigades.sort(Comparator.comparing(BrigadeDto::getId));
		brigades.sort(Comparator.comparingInt(o -> o.getId().intValue()));

		List<ExcelData> excelDataList = new ArrayList<>();
		excelDataList.add(new ExcelData("А/Ф", "251", "Арс., 21", "с/к", "09:00", ""));
		excelDataList.add(new ExcelData("JCB+рокс", "36-01", "на адрес", "", "10:30", ""));
		excelDataList.add(new ExcelData("С/свал", "973", "на адрес", "", "10:30", ""));
		excelDataList.add(new ExcelData("С/свал", "", "на адрес", "", "10:30", ""));
		excelDataList.add(new ExcelData("бара", "", "на адрес", "", "по зв.", ""));
		excelDataList.add(new ExcelData("илосос", "599", "на адрес", "", "10:30", ""));
		excelDataList.add(new ExcelData("", "", "", "1.труборез;", "", ""));

		GridLayout gridLayout = new GridLayout();
		gridLayout.setMargin(true);
		gridLayout.setSpacing(true);
		gridLayout.setSizeFull();
		gridLayout.setRows(brigades.size());
		gridLayout.setColumns(2);

		for (BrigadeDto brigadeDto : brigades) {
			DisabledTextField brigadeField = new DisabledTextField("Подразделение");
			brigadeField.setValue(brigadeDto.getName());
			DisabledTextField workField = new DisabledTextField("Вид работ");
			DisabledTextField addressField = new DisabledTextField("Адрес работ");
			DisabledTextField responsibleField = new DisabledTextField("Ответственный");
			List<WorkDto> brigadeWorks = BrigadesUI.getRestTemplate().getBrigadeWorks(brigadeDto.getId());
			if (brigadeWorks.size() > 0) {
				WorkDto workDto = brigadeWorks.get(0);
				workField.setValue(workDto.getDescription());
				workField.setDescription(workDto.getDescription());
				addressField.setValue(workDto.getAddress());
				addressField.setDescription(workDto.getAddress());
				UserDto responsible = workDto.getResponsible();
				if (responsible != null) {
					responsibleField.setValue(responsible.getFullName());
					responsibleField.setDescription(responsible.getFullName());
				}
			}

			FormLayout layout = new FormLayout(brigadeField, workField, addressField, responsibleField);
			gridLayout.addComponent(layout);

			Grid<ExcelData> excelDataGrid = new Grid<>();
			excelDataGrid.setSizeFull();
			excelDataGrid.addColumn(ExcelData::getType).setCaption("Тип техники").setSortable(false);
			excelDataGrid.addColumn(ExcelData::getNumber).setCaption("Гос№ а/м").setSortable(false);
			excelDataGrid.addColumn(ExcelData::getAddress).setCaption("Адрес подачи").setSortable(false);
			excelDataGrid.addColumn(ExcelData::getComment).setCaption("Комментарий").setSortable(false);
			excelDataGrid.addColumn(ExcelData::getTime).setCaption("Время подачи").setSortable(false);
			excelDataGrid.addColumn(ExcelData::getNote).setCaption("Примечание").setSortable(false);
			excelDataGrid.setItems(excelDataList);
			excelDataGrid.setHeightByRows(excelDataList.size());
			gridLayout.addComponent(excelDataGrid);
		}

		addToButtonBar(msg("button.save"), $ -> save());
		addToButtonBar(msg("button.close"), $ -> close());
		addToContent(gridLayout, 1);
		// Отключение ESC
		removeAllCloseShortcuts();
	}

	public static void open() {
		Window window = new VehicleDemoExcelWindow();
		UI.getCurrent().addWindow(window);
		window.setModal(false);
		window.focus();
	}

	private void save() {

	}

	private class ExcelData {
		private String type, number, address, comment, time, note;

		ExcelData(String type, String number, String address, String comment, String time, String note) {
			this.type = type;
			this.number = number;
			this.address = address;
			this.comment = comment;
			this.time = time;
			this.note = note;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}
	}
}
