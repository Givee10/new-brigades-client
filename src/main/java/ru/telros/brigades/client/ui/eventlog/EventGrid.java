package ru.telros.brigades.client.ui.eventlog;

import ru.telros.brigades.client.dto.EventWithActionDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class EventGrid extends BrigadesGrid<EventWithActionDto> {
	public EventGrid(List<EventWithActionDto> items) {
		super(EventConstants.getMainColumns(), items);
		// Раскраска строк таблицы
		setStyleGenerator(rowReference -> {
			String type = rowReference.getType();
			return (type != null) ? EventConstants.getStyle(type) : null;
		});
	}
	public EventGrid(List<EventWithActionDto> items, boolean forReport) {
		super(EventConstants.getReportColumns(), items);
		// Раскраска строк таблицы
		setStyleGenerator(rowReference -> {
			String type = rowReference.getType();
			return (type != null) ? EventConstants.getStyle(type) : null;
		});
	}

	@Override
	protected void onClick(EventWithActionDto entity, Column column) {

	}

	@Override
	protected void onDoubleClick(EventWithActionDto entity, Column column) {

	}
}
