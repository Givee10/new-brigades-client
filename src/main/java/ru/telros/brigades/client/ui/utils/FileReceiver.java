package ru.telros.brigades.client.ui.utils;

import com.vaadin.ui.Upload.Receiver;
import ru.telros.brigades.client.HasLogger;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileReceiver implements Receiver, HasLogger {
    private final Path directory;
    private Path filePath;

    public FileReceiver(final Path directory) {
        this.directory = directory;
    }

    @Override
    public OutputStream receiveUpload(final String filename, final String mimeType) {
        generateFilePath(filename);

        OutputStream outputStream = null;
        try {
            outputStream = Files.newOutputStream(filePath, StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            getLogger().error("Cannot create file: " + filePath, e);
        }
        return outputStream;
    }

    private void generateFilePath(final String fileName) {
        filePath = Paths.get(directory.toString(), FileUtil.changeFileName(fileName));
    }

    public String getFilePath() {
        return filePath.toString();
    }
}
