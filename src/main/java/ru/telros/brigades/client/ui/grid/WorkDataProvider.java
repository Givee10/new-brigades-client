package ru.telros.brigades.client.ui.grid;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.PagesDto;
import ru.telros.brigades.client.dto.WorkJournalDto;

import java.util.stream.Stream;

public class WorkDataProvider extends AbstractBackEndDataProvider<WorkJournalDto, String> implements HasLogger {
	@Override
	protected Stream<WorkJournalDto> fetchFromBackEnd(Query<WorkJournalDto, String> query) {
		getLogger().debug("fetchFromBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		query.getSortOrders().forEach(querySortOrder -> getLogger().debug("Sorted: {}, Direction {}",
				querySortOrder.getSorted(), querySortOrder.getDirection()));
		PagesDto<WorkJournalDto> journal = BrigadesUI.getRestTemplate().getWorksForJournal(query.getOffset(), query.getLimit());
		return journal.getCurrentContent().stream();
	}

	@Override
	protected int sizeInBackEnd(Query<WorkJournalDto, String> query) {
		getLogger().debug("sizeInBackEnd: filter {}, skip {}, limit {}, sorting {}",
				query.getFilter(), query.getOffset(), query.getLimit(), query.getInMemorySorting());
		return BrigadesUI.getRestTemplate().countWorks().intValue();
	}

	@Override
	public Object getId(WorkJournalDto item) {
		return item.getId();
	}
}
