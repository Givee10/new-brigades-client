package ru.telros.brigades.client.reportbuilders;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.vaadin.ui.Grid;
import ru.telros.brigades.client.dto.AbstractEntity;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.List;

/**
 * Created by Innokentiy on 28.08.2019.
 */
public class PDFBuilder<DTO extends AbstractEntity> {
    private Document document;
    private PdfPTable table;
    private Paragraph title;
    private String destination;
    private BaseFont helvetica =
            BaseFont.createFont(
                    "fonts/arial.ttf",
                    "cp1251",
                    BaseFont.EMBEDDED);
    private Font font = new Font(helvetica, 12.0f, 0, Color.BLACK);

    public PDFBuilder(String destination, String title, List<Grid.Column<DTO, ?>> tableHeader, List<? extends DTO> rows) throws IOException, DocumentException {
        this.destination = destination;
        Paragraph paragraph = new Paragraph(title, font);
        paragraph.setSpacingAfter(20f);
        this.title = paragraph;
        table = new PdfPTable(tableHeader.size());
        tableHeader.stream()
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(Color.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setHorizontalAlignment(Element.ALIGN_CENTER);
                    header.setPhrase(new Phrase(columnTitle.getCaption(), font));

                    PDFBuilder.this.table.addCell(header);
                });
        for (int i = 0; i < rows.size(); i++) {
            PdfPCell cell = new PdfPCell();
            for (int j = 0; j < tableHeader.size(); j++) {

                    /*Field field = rows.get(i).getClass().getDeclaredField(tableHeader.get(j).getId());
                    field.setAccessible(true);*/
//                    String fieldValue = (String) field.get(rows.get(i));
//                    cell.setPhrase(new Phrase(fieldValue, font));
                    String value = (String) tableHeader.get(j).getValueProvider().apply(rows.get(i));
                    cell.setPhrase(new Phrase(value, font));
                    table.addCell(cell);

            }
        }


    }

    public void saveToFile() {
        document = new Document();
        document.setPageSize(PageSize.A4.rotate());
        try {
            PdfWriter.getInstance(document, new FileOutputStream(destination));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        try {
            document.add(title);
            document.add(table);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }






}

