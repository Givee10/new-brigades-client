package ru.telros.brigades.client.reportbuilders;


import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.CommentDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.List;

//формирует PDF файл при поступлении заявки
public class PDFRequestBuilder {
    private Document document;
    private PdfPTable table;
    private Paragraph title;
    private List<Paragraph> works;
    private BaseFont helvetica =
            BaseFont.createFont(
                    "fonts/arial.ttf",
                    "cp1251",
                    BaseFont.EMBEDDED);
    private Font font = new Font(helvetica, 12.0f, 0, Color.BLACK);

    public PDFRequestBuilder(RequestDto request) throws IOException, DocumentException {

        Paragraph paragraph = new Paragraph("Заявка ГЛ №"+request.getNumber(), font);
        paragraph.setSpacingAfter(20f);
        this.title = paragraph;
        table = new PdfPTable(2);

        table.addCell(new Phrase("Номер:",font));
        table.addCell(new Phrase(request.getNumber(),font));

        table.addCell(new Phrase("ФИО:",font));
        table.addCell(new Phrase(request.getAuthor(),font));

        table.addCell(new Phrase("Организация:",font));
        table.addCell(new Phrase(request.getOrganization(),font));

        table.addCell(new Phrase("Район:",font));
        table.addCell(new Phrase(request.getDistrict(),font));

        table.addCell(new Phrase("Адрес:",font));
        table.addCell(new Phrase(request.getAddress(),font));

        table.addCell(new Phrase("Уточн. адреса:",font));
        table.addCell(new Phrase(request.getUpdatedAddress(),font));

        table.addCell(new Phrase("Телефон:",font));
        table.addCell(new Phrase(request.getNumber(),font));

        table.addCell(new Phrase("Город:",font));
        table.addCell(new Phrase(request.getCity(),font));

        table.addCell(new Phrase("Квартира:",font));
        table.addCell(new Phrase(request.getFlat(),font));

        table.addCell(new Phrase("Проблема возникла:",font));
        table.addCell(new Phrase(DatesUtil.formatZonedtoDDMMYYY(request.getIncidentDate()),font));

        table.addCell(new Phrase("Код проблемы:",font));
        table.addCell(new Phrase(request.getType(),font));

        table.addCell(new Phrase("Описание:",font));
        table.addCell(new Phrase(request.getDescription(),font));

        table.addCell(new Phrase("Комментарии:",font));
        table.addCell(new Phrase(getRequestDescription(request),font));
        List<WorkDto> workDtos = BrigadesUI.getRestTemplate().getRequestWorks(request.getId());
        if (workDtos!=null && workDtos.size()>0){
            works = new ArrayList<>();
            works.add(new Paragraph("Связанные работы:", font));
            workDtos.stream().forEach(e->{
                String brigadeName =" - ";
                if (e.getBrigade()!=null){
                    brigadeName = e.getBrigade().getName();
                }
                works.add(new Paragraph(String.format("Время начала: %s; Время окончания:%s\n" +
                                "Вид работы: %s;\n Выполнявшая бригада: %s",
                        DatesUtil.formatZonedtoDDMMYYY(e.getStartDate()),
                        DatesUtil.formatZonedtoDDMMYYY(e.getFinishDate()),
                        BrigadesUI.getRestTemplate().getTypeWork(e.getIdTypeWork()).getDescription(),
                        brigadeName), font));
            });
        }

    }
    private PdfPCell getCell(String content){
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(content, font));
        return cell;
    }
    private String getRequestDescription(RequestDto req){
        StringBuilder desc = new StringBuilder("");
        List<CommentDto> comments = BrigadesUI.getRestTemplate().getRequestComments(req.getId());
        try {
            comments.stream().forEach(e->{
                if (!e.getContent().endsWith(".")){
                    desc.append(e.getContent()).append(".");
                }
                desc.append(" ");
            });
        }catch (NullPointerException ex){
        }
        return desc.toString();
    }

    public File saveToFile(String pathToFile) {
        document = new Document();
        document.setPageSize(PageSize.A4);
        File file = new File(pathToFile);
        try {

            PdfWriter.getInstance(document, new FileOutputStream(file));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        try {
            document.add(title);
            document.add(table);
            if (works!=null && works.size()>0){
                works.stream().forEach(e-> {
                    try {
                        document.add(e);
                    } catch (DocumentException ex) {
                        ex.printStackTrace();
                    }
                });
            }
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return file;
    }

}
