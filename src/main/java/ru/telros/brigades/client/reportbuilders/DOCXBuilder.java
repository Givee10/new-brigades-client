package ru.telros.brigades.client.reportbuilders;

import com.vaadin.ui.Grid;
import org.docx4j.jaxb.Context;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.*;
import ru.telros.brigades.client.dto.AbstractEntity;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class DOCXBuilder<DTO extends AbstractEntity>{
    private String destination;
    private String title;
    private Tbl table;
    private WordprocessingMLPackage wordPackage;
    public DOCXBuilder(String destination, String title, List<Grid.Column<DTO, ?>> tableHeader, List<? extends DTO> tableRows){
        this.destination = destination;
        try {
            wordPackage = WordprocessingMLPackage.createPackage(PageSizePaper.A4, true);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        this.title = title;
        ObjectFactory factory = Context.getWmlObjectFactory();
        int writableWidthTwips = wordPackage.getDocumentModel()
                .getSections().get(0).getPageDimensions().getWritableWidthTwips();

        table = TblFactory.createTable(tableRows.size()+1,tableHeader.size(),writableWidthTwips/tableHeader.size());
        List<Object> rows = table.getContent();
        Tr headerRow = (Tr) rows.get(0);
        for (int i = 0; i < headerRow.getContent().size(); i++) {
            P paragraph = factory.createP();
            R run = factory.createR();
            Text text = factory.createText();
            text.setValue(tableHeader.get(i).getCaption());
            run.getContent().add(text);
            paragraph.getContent().add(run);
            Tc curCell = (Tc) headerRow.getContent().get(i);
            curCell.getContent().add(paragraph);
        }
        for (int i = 0; i < tableRows.size(); i++) {
            headerRow = (Tr) rows.get(i+1);
            for (int j = 0; j < headerRow.getContent().size(); j++) {
                P paragraph = factory.createP();
                R run = factory.createR();
                Text text = factory.createText();

//                    Field field = tableRows.get(i).getClass().getDeclaredField(headerIds.get(j));
//                    field.setAccessible(true);
//                    String fieldValue = (String)field.get(tableRows.get(i));
                    String value = (String) tableHeader.get(j).getValueProvider().apply(tableRows.get(i));
                    text.setValue(value);


                run.getContent().add(text);
                paragraph.getContent().add(run);
                Tc curCell = (Tc) headerRow.getContent().get(j);
                curCell.getContent().add(paragraph);
            }
        }

    }
    public void saveToFile(){

        MainDocumentPart mainDocumentPart = wordPackage.getMainDocumentPart();
        String[] titleLines = title.split("\r\n");
        Arrays.stream(titleLines).forEach(e-> mainDocumentPart.addParagraphOfText(e));
        mainDocumentPart.getContent().add(table);
        File exportFile = new File(destination);
        try {
            wordPackage.save(exportFile);
        } catch (Docx4JException e) {
            e.printStackTrace();
        }
    }

    public class Builder{
        private Builder(){}


        public DOCXBuilder.Builder setTable(List<String> tableHeader, List tableRows, List<String> headerIds){


            /*
            for (Object row : rows) {
                Tr tr = (Tr) row;
                List<Object> cells = tr.getContent();
                for(Object cell : cells) {
                    Tc td = (Tc) cell;
                    td.getContent().add(p);
                }
            }
*/
            return this;
        }

        public DOCXBuilder build(){
            return DOCXBuilder.this;
        }
    }
}
