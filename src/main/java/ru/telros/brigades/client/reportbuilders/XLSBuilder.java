package ru.telros.brigades.client.reportbuilders;

import com.vaadin.ui.Grid;
import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.AbstractEntity;
import ru.telros.brigades.client.dto.RequestDto;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class XLSBuilder<DTO extends AbstractEntity> implements HasLogger {

    public XLSBuilder() {

    }
    public void saveToFile(String file, String title, List<Grid.Column<DTO, ?>> tableHeader, List<? extends DTO> tableRows){
        WritableWorkbook workbook = null;
        try {
            workbook = Workbook.createWorkbook(new File(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        WritableSheet sheet = workbook.createSheet(title, 0);

        WritableCellFormat titleFormat = new WritableCellFormat();
        WritableFont font = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
        titleFormat.setFont(font);
        try {
            titleFormat.setAlignment(Alignment.LEFT);
            titleFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
            titleFormat.setBackground(Colour.LIGHT_BLUE);
            titleFormat.setWrap(true);
            titleFormat.setShrinkToFit(false);


        } catch (WriteException e) {
            e.printStackTrace();
        }
        String[] titleLines = title.split("\r\n");
        AtomicInteger counter = new AtomicInteger(0);
        Arrays.stream(titleLines).forEach(e->{
            Label titleLabel = new Label(0, counter.get(), e, titleFormat);
            try {
                sheet.setRowView(counter.get(),titleFormat.getFont().getPointSize()*25);
                sheet.addCell(titleLabel);
                sheet.mergeCells(0,counter.get(),tableHeader.size()-1,counter.get());
                counter.incrementAndGet();
            } catch (WriteException ex) {
                ex.printStackTrace();
            }
        });
        int maxLength = Arrays.stream(titleLines).max(Comparator.comparingInt(String::length)).get().length();
        int requiredColumnlength=maxLength*2;
        getLogger().debug(String.valueOf(maxLength));


        WritableCellFormat headerFormat = new WritableCellFormat();
        font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
        headerFormat.setFont(font);
        try {
            headerFormat.setAlignment(Alignment.CENTRE);
            headerFormat.setBackground(Colour.AQUA);
            headerFormat.setWrap(false);
            headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK);
            headerFormat.setShrinkToFit(true);
        } catch (WriteException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < tableHeader.size(); i++) {
            Label headerLabel = new Label(i, titleLines.length, tableHeader.get(i).getCaption(), headerFormat);
            try {
                sheet.addCell(headerLabel);
            } catch (WriteException e) {
                e.printStackTrace();
            }
        }
        //Пропорционально регулирует ширину колонок относительно длинны заголовка и ширины титульного текста
        resizeHeadersRow(requiredColumnlength,tableHeader,titleLines.length,sheet);
        //добавление данных таблицы
        WritableCellFormat rowFormat = new WritableCellFormat();
        try {
            rowFormat.setWrap(true);
            rowFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        } catch (WriteException e) {
            e.printStackTrace();
        }
        AtomicInteger rowIndex = new AtomicInteger(titleLines.length+1);
        int offset = rowIndex.get();
        tableRows.stream().forEach(e->{
            for (int j = 0; j < tableHeader.size(); j++) {
                try {
//                    Field field = e.getClass().getDeclaredField(headerIds.get(j));
//                    field.setAccessible(true);
                    String value = (String) tableHeader.get(j).getValueProvider().apply(tableRows.get(rowIndex.get() - offset));
                    Label rowLabel = new Label(j, rowIndex.get(), value,rowFormat);
                    sheet.addCell(rowLabel);
                }  catch (RowsExceededException ex) {
                    ex.printStackTrace();
                } catch (WriteException ex) {
                    ex.printStackTrace();
                }
            }
            rowIndex.incrementAndGet();
        });

        try {
            workbook.write();
            workbook.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }
    public void saveRequestReport(String filePath, String title, List<RequestDto> items, List<Grid.Column<RequestDto,?>> columns) throws IOException, WriteException {
        //problem - категория проблемы. Например, "Вытекание на пр. части"
        //Номер проблемы - название проблемы
        Map<Integer, String> problemID_to_problemName = new HashMap<>();
        //Номер проблемы - список заявок
        Map<Integer, List<RequestDto>> problemID_to_problem = new HashMap<>();
        //Номер проблемы - статистика проблемы (было, поступило, выполнено, остаток)
        Map<Integer, int[]> problemID_to_problemStats = new HashMap<>();
        items.stream().forEach(e -> {
            Integer problemID;
            try {
                problemID = Integer.parseInt(e.getProblemCode());
            } catch (NumberFormatException e1) {
                problemID = null;
            }
            problemID_to_problemName.put(problemID, e.getProblem());
            if (problemID_to_problem.get(problemID) == null) {
                problemID_to_problem.put(problemID, new ArrayList<>());
                problemID_to_problemStats.put(problemID, new int[4]);
            }
            problemID_to_problem.get(problemID).add(e);
            switch (e.getStatus()) {
                case "Новая":
                    break;
                case "В работе":
                    break;
                case "Закрытая":
                    break;
            }


        });
        WritableWorkbook workbook = null;
        try {
            workbook = Workbook.createWorkbook(new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        WritableSheet sheet = workbook.createSheet(title, 0);

        WritableCellFormat curFormat = new WritableCellFormat();
        WritableFont curFont = new WritableFont(WritableFont.ARIAL, 36, WritableFont.BOLD);
        curFormat.setFont(curFont);
        curFormat.setAlignment(Alignment.LEFT);
        curFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        curFormat.setWrap(false);
        curFormat.setShrinkToFit(false);


//            sheet.mergeCells(0,1,2,3);
        sheet.addCell(new Label(0, 1, title, curFormat));
        sheet.addCell(new Label(8, 1, "ТУВ \"Северное, Восточное\"", curFormat));
        sheet.addCell(new Label(9, 0, "",getCellFormat(Colour.GREEN, WritableFont.TIMES, 36,false)));
        sheet.addCell(new Label(9, 1, "", getCellFormat(Colour.ORANGE, WritableFont.TIMES, 36,false)));
        sheet.addCell(new Label(9, 2, "", getCellFormat(Colour.YELLOW, WritableFont.TIMES, 36,false)));

        sheet.addCell(new Label(10, 0, "Выполненные заявки", getCellFormat(null, WritableFont.TIMES, 36,false)));
        sheet.addCell(new Label(10, 1, "Поступившие заявки", getCellFormat(null, WritableFont.TIMES, 36,false)));
        sheet.addCell(new Label(10, 2, "Локализация", getCellFormat(null, WritableFont.TIMES, 36,false)));

        sheet.addCell(new Label(0, 3, "Локализация", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(1, 3, "", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(2, 3, "Было", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(3, 3, "Поступило", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(4, 3, "Выполнено", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(5, 3, "Остаток", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(6, 3, "№ заявки", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(7, 3, "Адрес", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(8, 3, "Дата и время поступления", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(9, 3, "Дата и время локализации", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(10, 3, "Комментарий", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(11, 3, "ФОТО", getCellFormat(null, WritableFont.TIMES, 28,true)));
        sheet.addCell(new Label(12, 3, "СХЕМЫ", getCellFormat(null, WritableFont.TIMES, 28,true)));

        workbook.write();
        workbook.close();
        getLogger().debug("Записал");
    }
    private void resizeHeadersRow(int titleRequiredLength, List<Grid.Column<DTO, ?>> tableHeader, int headersRowIndex, WritableSheet sheet ){
        int overallHeadersColumnlength = tableHeader.stream().mapToInt(e->e.getCaption().length()).sum()*2;
        if (titleRequiredLength > overallHeadersColumnlength){
            double multiplier = titleRequiredLength/overallHeadersColumnlength;
            for (int i = 0; i < tableHeader.size(); i++) {
                sheet.setColumnView(i, (int)(tableHeader.get(i).getCaption().length()*2*multiplier));
            }
        }else {
            for (int i = 0; i < tableHeader.size(); i++) {
                sheet.setColumnView(i, tableHeader.get(i).getCaption().length()*2);
            }
        }
    }
    private static WritableCellFormat getCellFormat(Colour colour, WritableFont.FontName font, int size, boolean bold) throws WriteException {
        WritableFont cellFont = new WritableFont(font,size );
        if (bold){
            cellFont.setBoldStyle(WritableFont.BOLD);
        }
        WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
        if (colour!=null) {
            cellFormat.setBackground(colour);
        }
        cellFormat.setWrap(false);
        return cellFormat;
    }
}