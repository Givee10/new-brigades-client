package ru.telros.brigades.client.reportbuilders;

import jxl.Workbook;
import jxl.write.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.AttachmentDto;
import ru.telros.brigades.client.dto.CommentDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class RV_SOUTHWEST implements HasLogger {
    String organizationList;
    private ZonedDateTime start;
    private ZonedDateTime end;
    Map<Integer, Category> categoryID_to_category;
    Map<RequestDto, Integer> requestToPhotoAmount;
    Map<RequestDto,Integer> requestToSchemeAmount;
    Map<RequestDto,String> requestToDescription;
    Set<RequestDto> requestsSet;
    Set<Category> categories;
    //категории по вытеканиям
    Set<Category> leakCategories;
    public RV_SOUTHWEST(ZonedDateTime start,
                        ZonedDateTime end,
                        List<RequestDto> items,
                        String organizationList

                        ){
        this.organizationList = organizationList;
        this.start =start;
        this.end = end;
        categoryID_to_category = new HashMap<>();
        requestsSet = new HashSet<>();
        requestsSet.addAll(items);
        requestToPhotoAmount = new HashMap<>();
        requestToSchemeAmount = new HashMap<>();
        requestToDescription = new HashMap<>();
        requestsSet.stream().forEach(e -> {
            Integer categoryID = null;
            String categoryDesc = null;

            try {
                categoryID = Integer.parseInt(e.getProblemCode());
                categoryDesc = e.getProblem();
            } catch (NumberFormatException e1) {
                categoryID = -1;
            } catch (NullPointerException e2){
                categoryDesc = null;
            }
            if (categoryID_to_category.get(categoryID)==null){
                categoryID_to_category.put(categoryID, new Category(categoryID, categoryDesc));
            }

            categoryID_to_category.get(categoryID).addRequest(e);
        });
        categoryID_to_category.entrySet().forEach(e->{
            getLogger().debug(e.getKey().toString()+" "+e.getValue().toString());
        });
        categories = new LinkedHashSet<>();
        leakCategories = new LinkedHashSet<>();
        List<Integer> leakCategoriesIDs = Arrays.asList(new Integer[]{6,13,22,61});
        for (int i = 0; i < leakCategoriesIDs.size(); i++) {
            if (categoryID_to_category.get(leakCategoriesIDs.get(i))!=null){
                categories.add(categoryID_to_category.get(leakCategoriesIDs.get(i)));
                leakCategories.add(categoryID_to_category.get(leakCategoriesIDs.get(i)));
            }
        }
        /*if (categoryID_to_category.get(6)!=null){
            categories.add(categoryID_to_category.get(6));
            leakCategories.add(categoryID_to_category.get(6));
        }
        if (categoryID_to_category.get(13)!=null){
            categories.add(categoryID_to_category.get(13));
            leakCategories.add(categoryID_to_category.get(13));
        }
        if (categoryID_to_category.get(22)!=null){
            categories.add(categoryID_to_category.get(22));
            leakCategories.add(categoryID_to_category.get(22));
        }
        if (categoryID_to_category.get(61)!=null){
            categories.add(categoryID_to_category.get(61));
            leakCategories.add(categoryID_to_category.get(61));
        }*/

        Category category = new Category(-1000, "ИТОГО по вытеканиям");
        categories.add(category);
//        }
        categories.addAll(categoryID_to_category.values());
    }
    public String openHTMLTable(){
        StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta charset=\"cp1251\">\n"+
                "<style>\n" +
                "\ttable{\n" +
                "\t\tborder-collapse: collapse;\n" +
                "\t}\n" +
                "\tth{\n" +
                "\t\tborder: 2px solid black;\n" +
                "\t\ttext-align : center;\n" +
                "\t\tfont-weight: bold;\n" +
                "\t\t\n" +
                "\t}\n" +
                "\ttd{\n" +
                "\t\tborder: 1px solid black;\n" +
                "\t\ttext-align : center;\n" +
                "\t\tborder-color: black; \n" +
                "\t}\n" +
                "</style>\n" +
                "<head>\n"+
                "<body>\n" +
                "\n" +
                "<h1>Отчёт по заявкам c " + start.format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT))+" - " +
                end.format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)) + organizationList+"</h1>"+
                "\n" +
                "<table style='border: 4px solid black'>\n");
                sb.append("<tr><th style='border:3px solid black'>Код заявки</th><th style='border:3px solid black'></th><th style='border:3px solid black'>Было</th style='border:3px solid black'><th style='border:3px solid black'>Поступило</th><th style='border:3px solid black'>Выполнено</th><th style='border:3px solid black'>Остаток</th><th>Адм. район</th><th style='border:3px solid black'>№ заявки</th><th style='border:3px solid black'>Адрес</th><th style='border:3px solid black'>Дата и время поступления</th><th style='border:3px solid black'>Дата и время локализации</th><th style='border:3px solid black'>Комментарий</th><th style='border:3px solid black'>ФОТО</th><th style='border:3px solid black'>СХЕМЫ</th></tr>");
        Iterator<Category> it = categories.iterator();
        //Т.к. categories - linkedhashset, сначала идут категории вытекания, потом искуственно созданная категория-маркер с кодом - 1000, потом всё остальное
        while (it.hasNext()){
            Category curCategory = it.next();
            if (curCategory.code==-1000) {
                int were = leakCategories.stream().mapToInt(e -> e.getTypeCount("Было")).sum();
                int income = leakCategories.stream().mapToInt(e -> e.getTypeCount("Поступило")).sum();
                int done = leakCategories.stream().mapToInt(e -> e.getTypeCount("Выполнено")).sum();
                int remains = leakCategories.stream().mapToInt(e -> e.getTypeCount("Остаток")).sum();
//                style='border:3px solid black'
                sb.append("<tr style=\"background-color:grey; border:3px solid black\"><td style='border:3px solid black'>ИТОГО по вытеканиям</td><td style='border:3px solid black'></td><td style='border:3px solid black'>" + were + "</td><td style='border:3px solid black'>" + income + "</td><td style='border:3px solid black'>" + done + "</td><td style='border:3px solid black'>" + remains + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");

            }else {
                List<RequestDto> requestsFromCurCategory = curCategory.getRequests();
                int categoryItemsAmount = requestsFromCurCategory.size();
                String style = "";
                if (categoryItemsAmount > 0) {
                    ZonedDateTime curReqCloseTime = requestsFromCurCategory.get(0).getCloseDate();
                    ZonedDateTime curReqRegTime = requestsFromCurCategory.get(0).getRegistrationDate();

                    if ((requestsFromCurCategory.get(0).getCloseDate() != null) && (curReqCloseTime.isAfter(start) && curReqCloseTime.isBefore(end))) {
                        style = "style='background-color:green; border-top:3px solid black' ";
                    } else if (curReqRegTime.isAfter(start) && curReqRegTime.isBefore(end)) {
                        style = "style='background-color:orange; border-top:3px solid black' ";
                    }
                    sb.append("<!--- ").append(requestsFromCurCategory.get(0).debugInfo()).append(" --->");
                    if (categoryItemsAmount==1){
                        sb.append("<tr style='border-bottom: 3px solid black; '>");
                    }else {
                        sb.append("<tr>");
                    }

                    sb.append("<td style='background: white; border:3px solid black' rowspan=" + categoryItemsAmount + ">" + curCategory.getDescription() + "</td><td style='background: white; border:3px solid black' rowspan=" + categoryItemsAmount + ">" + curCategory.getCode() + "</td><td style='background: white; border:3px solid black' rowspan=" + categoryItemsAmount + ">" + curCategory.getTypeCount("Было") + "</td><td style='background: white; border:3px solid black' rowspan=" + categoryItemsAmount + ">" + curCategory.getTypeCount("Поступило") + "</td><td style='background: white; border:3px solid black' rowspan=" + categoryItemsAmount + ">" + curCategory.getTypeCount("Выполнено") + "</td><td style='background: white; border:3px solid black' rowspan=" + categoryItemsAmount + ">" + curCategory.getTypeCount("Остаток") + "</td>" +
                            "<td " + style + "+>" + requestsFromCurCategory.get(0).getDistrict() + "</td>" + "<td " + style + "+>" + requestsFromCurCategory.get(0).getExternalNumber() + "</td>" + "<td " + style + "+>" + requestsFromCurCategory.get(0).getAddress() + "</td>" + "<td " + style + "+>" + DatesUtil.formatZoned(requestsFromCurCategory.get(0).getRegistrationDate()) + "</td>" + "<td " + style + "+>" + "" + "</td>" + "<td " + style + "+>" + requestToDescription.get(requestsFromCurCategory.get(0)) + "</td>" + "<td>" + requestToPhotoAmount.get(requestsFromCurCategory.get(0)) + "</td>" + "<td>" + requestToSchemeAmount.get(requestsFromCurCategory.get(0)) + "</td>" + "</tr>");
                    for (int i = 1; i < requestsFromCurCategory.size(); i++) {

                        if ((requestsFromCurCategory.get(i).getCloseDate() != null) && (requestsFromCurCategory.get(i).getCloseDate().isAfter(start) && requestsFromCurCategory.get(i).getCloseDate().isBefore(end))) {
                            getLogger().debug(requestsFromCurCategory.get(i).debugInfo());
                            style = "style='background-color:green;' ";
                        } else if (curReqRegTime.isAfter(start) && curReqRegTime.isBefore(end)) {
                            style = "style='background-color:orange;' ";
                        }
                    /*if (i==requestsFromCurCategory.size()-1){
                        try {
                            style = style.substring(style.length() - 4) + " border-bottom:3px solid black'";
                        }catch (StringIndexOutOfBoundsException e){
                            style = "style='border-bottom:3px solid black'";
                        }
                    }*/
                        sb.append("<!--- ").append(requestsFromCurCategory.get(i).debugInfo()).append(" --->");
                        if (i==requestsFromCurCategory.size()-1) {
                            if (style.length()>2) {
                                style = style.substring(0, style.length() - 2) + " border-bottom: 3px solid black;'";
                            }else {
                                style= "style='border-bottom: 3px solid black;'";
                            }
                        }
                        sb.append("<tr "+style+"><td>" + requestsFromCurCategory.get(i).getDistrict() + "</td>" + "<td>" + requestsFromCurCategory.get(i).getExternalNumber() + "</td>" + "<td " +  ">" + requestsFromCurCategory.get(i).getAddress() + "</td>" + "<td " + ">" + DatesUtil.formatZoned(requestsFromCurCategory.get(i).getRegistrationDate()) + "</td>" + "<td " + ">" + "" + "</td>" + "<td " + ">" + requestToDescription.get(requestsFromCurCategory.get(i)) + "</td>" + "<td>" + requestToPhotoAmount.get(requestsFromCurCategory.get(i)) + "</td>" + "<td>" + requestToSchemeAmount.get(requestsFromCurCategory.get(i)) + "</td>" + "</tr>");
                        style="";
                    }
                }
            }
        }

                int were = categories.stream().mapToInt(e -> e.getTypeCount("Было")).sum();
                int income = categories.stream().mapToInt(e -> e.getTypeCount("Поступило")).sum();
                int done = categories.stream().mapToInt(e -> e.getTypeCount("Выполнено")).sum();
                int remains = categories.stream().mapToInt(e -> e.getTypeCount("Остаток")).sum();

                sb.append("<tr style=\"background-color:grey\"><td>ИТОГО</td><td></td><td>" + were + "</td><td>" + income + "</td><td>" + done + "</td><td>" + remains + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");

                sb.append("</table>\n" +
                        "</body>\n" +
                        "</html>");
        getLogger().debug(sb.toString());
        return sb.toString();
    }
    public File saveRequestReport(String filePath) throws IOException, WriteException {
        //problem - категория проблемы. Например, "Вытекание на пр. части"
        //Номер проблемы - название проблемы





        WritableWorkbook workbook = null;
        try {
            workbook = Workbook.createWorkbook(new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String title = "Отчет по заявкам с " + start.format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT))
                +" - "+end.format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT))
                +". Районы: "+organizationList;
        WritableSheet sheet = workbook.createSheet(title, 0);

        WritableCellFormat curFormat = new WritableCellFormat();
        WritableFont curFont = new WritableFont(WritableFont.ARIAL, 36, WritableFont.BOLD);
        curFormat.setFont(curFont);
        curFormat.setAlignment(Alignment.LEFT);
        curFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        curFormat.setWrap(false);
        curFormat.setShrinkToFit(false);


//            sheet.mergeCells(0,1,2,3);
        sheet.addCell(new Label(0, 1, title, curFormat));
        sheet.addCell(new Label(8, 1, "ТУВ \"Северное, Восточное\"", curFormat));
        sheet.addCell(new Label(9, 0, "",getStyledCellFormat("color=green size=36")));
        sheet.addCell(new Label(9, 1, "",getStyledCellFormat("color=orange size=36")));
        sheet.addCell(new Label(9, 2, "",getStyledCellFormat("color=yellow size=36")));

        sheet.addCell(new Label(10, 0, "Выполненные заявки", getStyledCellFormat("size=36")));
        sheet.addCell(new Label(10, 1, "Поступившие заявки",getStyledCellFormat("size=36")));
        sheet.addCell(new Label(10, 2, "Локализация",getStyledCellFormat("size=36")));

        sheet.addCell(new Label(0, 3, "Код заявки",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(1, 3, "",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(2, 3, "Было",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(3, 3, "Поступило",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(4, 3, "Выполнено",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(5, 3, "Остаток",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(6, 3, "Адм. район",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(7, 3, "№ заявки",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(8, 3, "Адрес",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(9, 3, "Дата и время поступления",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(10, 3, "Дата и время локализации",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(11, 3, "Комментарий",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(12, 3, "ФОТО",getStyledCellFormat("size=28 bold=true border=thick")));
        sheet.addCell(new Label(13, 3, "СХЕМЫ",getStyledCellFormat("size=28 bold=true border=thick")));
        resizeColumns(sheet, 3);
        int curRow = 4;
        int curColumn = 5;



        Iterator<Category> categoryIterator = categories.iterator();
        List<Integer> titleRows = new ArrayList<>();
        while (categoryIterator.hasNext()) {
            Category curCategory = categoryIterator.next();
            if (!titleRows.contains(curRow)){
                titleRows.add(curRow);
            }
            getLogger().debug("Row: "+curRow);
            if (curCategory.code == -1000) {
                int were = leakCategories.stream().mapToInt(e -> e.getTypeCount("Было")).sum();
                int income = leakCategories.stream().mapToInt(e -> e.getTypeCount("Поступило")).sum();
                int done = leakCategories.stream().mapToInt(e -> e.getTypeCount("Выполнено")).sum();
                int remains = leakCategories.stream().mapToInt(e -> e.getTypeCount("Остаток")).sum();

                sheet.addCell(new Label(0, curRow, curCategory.description, getStyledCellFormat("size=24 bold=false color=grey border=thick align=center")));
                sheet.addCell(new Label(1, curRow, "", getStyledCellFormat("size=24 bold=false border=thick color=grey align=center")));
                sheet.addCell(new Label(2, curRow, were + "", getStyledCellFormat("size=24 bold=false color=grey border=thick align=center")));
                sheet.addCell(new Label(3, curRow, income + "", getStyledCellFormat("size=24 bold=false color=grey border=thick align=center")));
                sheet.addCell(new Label(4, curRow, done + "", getStyledCellFormat("size=24 bold=false color=grey border=thick align=center")));
                sheet.addCell(new Label(5, curRow, remains + "", getStyledCellFormat("size=24 bold=false color=grey border=thick align=center")));
                for (int i = 6; i < 13; i++) {
                    sheet.addCell(new Label(i, curRow, " ", getStyledCellFormat("size=24 bold=false color=grey border=thick,bot")));
                }
                sheet.addCell(new Label(13, curRow, " ", getStyledCellFormat("size=24 bold=false color=grey border=thick,bot,right")));
                curRow++;
                continue;
            }
            List<RequestDto> requests = curCategory.getRequests();
            if (requests.size() != 0) {
                getLogger().debug(curCategory.description + " " + requests.size());
                sheet.addCell(new Label(0, curRow, curCategory.description, getStyledCellFormat("size=24 bold=false border=thick align=center")));
                sheet.addCell(new Label(1, curRow, curCategory.code + "", getStyledCellFormat("size=24 bold=false border=thick align=center")));
                sheet.addCell(new Label(2, curRow, curCategory.getTypeCount("Было") + "", getStyledCellFormat("size=24 bold=false border=thick align=center")));
                sheet.addCell(new Label(3, curRow, curCategory.getTypeCount("Поступило") + "", getStyledCellFormat("size=24 bold=false border=thick align=center")));
                sheet.addCell(new Label(4, curRow, curCategory.getTypeCount("Выполнено") + "", getStyledCellFormat("size=24 bold=false border=thick align=center")));
                sheet.addCell(new Label(5, curRow, curCategory.getTypeCount("Остаток") + "", getStyledCellFormat("size=24 bold=false border=thick align=center")));
                //Example
                //Merge col[0-3] and row[1]
//            sheet.mergeCells(0, 1, 3, 1);
            /*sheet.mergeCells(0, curRow, 0, requests.size()+curRow-1);
            sheet.mergeCells(1, curRow, 1, requests.size()+curRow-1);
            sheet.mergeCells(2, curRow, 2, requests.size()+curRow-1);
            sheet.mergeCells(3, curRow, 3, requests.size()+curRow-1);
            sheet.mergeCells(4, curRow, 4, requests.size()+curRow-1);
            sheet.mergeCells(5, curRow, 5, requests.size()+curRow-1);*/
                for (int row = 0; row < requests.size(); row++) {
                    boolean thickBorder = false;

                    String curStyle;
                    if (row == requests.size() - 1) {
                        curStyle = "size=24 bold=false border=thick,bot";
                    } else {
                        curStyle = "size=24 bold=false border=thin";
                    }
                    //Перенес сюда, т.к. эти две ячейки не имеют цвета, а строка стилей изменяется в середине (невозможно вернуть к исход. виду после)
                    sheet.addCell(new Label(12, curRow + row, requestToPhotoAmount.get(requests.get(row))+"", getStyledCellFormat(curStyle)));
                    sheet.addCell(new Label(13, curRow + row, requestToSchemeAmount.get(requests.get(row))+"", getStyledCellFormat(curStyle + ",right")));
                    //Определение цвета строки
                    if ((requests.get(row).getCloseDate() != null) && ((requests.get(row).getCloseDate().isAfter(start) && requests.get(row).getCloseDate().isBefore(end)))) {
                        curStyle = "color=green " + curStyle;
                    } else if (requests.get(row).getRegistrationDate().isAfter(start)&&requests.get(row).getRegistrationDate().isBefore(end)) {
                        curStyle = "color=orange " + curStyle;
                    }
                    sheet.addCell(new Label(6, curRow + row, requests.get(row).getDistrict(), getStyledCellFormat(curStyle)));
                    sheet.addCell(new Label(7, curRow + row, requests.get(row).getExternalNumber() + "", getStyledCellFormat(curStyle)));
                    sheet.addCell(new Label(8, curRow + row, requests.get(row).getAddress(), getStyledCellFormat(curStyle)));
                    sheet.addCell(new Label(9, curRow + row, requests.get(row).getRegistrationDate().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)), getStyledCellFormat(curStyle)));
                    //TODO-K Добавить время локализации
                    sheet.addCell(new Label(10, curRow + row, " ", getStyledCellFormat(curStyle)));
                    sheet.addCell(new Label(11, curRow + row, requests.get(row).getDescription(), getStyledCellFormat(curStyle)));

                }
                curRow += requests.size();
            }
        }
        mergeCells(sheet,titleRows);
        sheet.addCell(new Label(0, curRow, "ИТОГО", getStyledCellFormat("size=24 bold=true align=center border=thick color=grey")));
        sheet.addCell(new Label(1, curRow, "", getStyledCellFormat("size=24 bold=true align=center border=thick color=grey")));
        sheet.addCell(new Label(2, curRow, categories.stream().mapToInt(e->e.getTypeCount("Было")).sum()+"", getStyledCellFormat("size=24 bold=true align=center border=thick color=grey")));
        sheet.addCell(new Label(3, curRow, categories.stream().mapToInt(e->e.getTypeCount("Поступило")).sum()+"", getStyledCellFormat("size=24 bold=true align=center border=thick color=grey")));
        sheet.addCell(new Label(4, curRow, categories.stream().mapToInt(e->e.getTypeCount("Выполнено")).sum()+"", getStyledCellFormat("size=24 bold=true align=center border=thick color=grey")));
        sheet.addCell(new Label(5, curRow, categories.stream().mapToInt(e->e.getTypeCount("Остаток")).sum()+"", getStyledCellFormat("size=24 bold=true align=center border=thick color=grey")));

        workbook.write();
        workbook.close();
        getLogger().debug("Записал");
        return new File(filePath);
    }
    private void mergeCells(WritableSheet sheet, List<Integer> titleRows){

        for (int i = 0; i < titleRows.size()-1; i++) {
            getLogger().debug(titleRows.get(i)+" "+titleRows.get(i+1));
            try {
                sheet.mergeCells(0,titleRows.get(i),0,titleRows.get(i+1)-1);
                sheet.mergeCells(1,titleRows.get(i),1,titleRows.get(i+1)-1);
                sheet.mergeCells(2,titleRows.get(i),2,titleRows.get(i+1)-1);
                sheet.mergeCells(3,titleRows.get(i),3,titleRows.get(i+1)-1);
                sheet.mergeCells(4,titleRows.get(i),4,titleRows.get(i+1)-1);
                sheet.mergeCells(5,titleRows.get(i),5,titleRows.get(i+1)-1);
            } catch (WriteException e) {
                e.printStackTrace();
            }
        }

    }
    //Получение стиля ячейки
    private static WritableCellFormat getStyledCellFormat(String styleLine) throws WriteException {
        String[] styles = styleLine.split(" ");
        Map<String,String> stylesMap = new HashMap<>();
        Arrays.stream(styles).forEach(e->{
            stylesMap.put(e.split("=")[0], e.split("=")[1]);
        });
        WritableFont.FontName fontName = WritableFont.TIMES;
        WritableFont cellFont = new WritableFont(fontName,Integer.parseInt(stylesMap.get("size")));
        if (stylesMap.get("bold")!=null){
            if (stylesMap.get("bold").equals("true")){
                cellFont.setBoldStyle(WritableFont.BOLD);
            }
        }

        WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
        cellFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        if (stylesMap.get("align")!=null) {
            if (stylesMap.get("align").equals("center")) {
                cellFormat.setAlignment(Alignment.CENTRE);
            }
        }
        if (stylesMap.get("border")!=null) {
            if (stylesMap.get("border").equals("thick")) {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK);
            } else if (stylesMap.get("border").equals("thin")) {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

            }else if (stylesMap.get("border").equals("thin,right")) {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                cellFormat.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THICK);
            } else if (stylesMap.get("border").equals("thick,bot")) {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                cellFormat.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.THICK);
            }else if (stylesMap.get("border").equals("thick,bot,right")) {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                cellFormat.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.THICK);
                cellFormat.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THICK);
            }
            else {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            }
        }
        if (stylesMap.get("color")!=null) {
            switch (stylesMap.get("color")) {
                case "green":
                    cellFormat.setBackground(jxl.format.Colour.GREEN);
                    break;
                case "orange":
                    cellFormat.setBackground(jxl.format.Colour.ORANGE);
                    break;
                case "yellow":
                    cellFormat.setBackground(jxl.format.Colour.YELLOW);
                    break;
                case "grey":
                    cellFormat.setBackground(jxl.format.Colour.GREY_50_PERCENT);
                    break;
                default:

                    break;

            }
        }
        cellFormat.setWrap(true);
        return cellFormat;
    }
    private static WritableCellFormat getCellFormat(jxl.format.Colour colour, WritableFont.FontName font, int size, boolean bold, boolean border, boolean onlyBotBorder, boolean centered) throws WriteException {
        WritableFont cellFont = new WritableFont(font,size );
        if (bold){
            cellFont.setBoldStyle(WritableFont.BOLD);
        }
        WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
        cellFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        if (centered){
            cellFormat.setAlignment(Alignment.CENTRE);
        }
        if (border) {
            //КОСТЫЛЬ
            if (onlyBotBorder){
                cellFormat.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.THICK);
            }else {
                cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK);
            }
        }else {
            cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
        }
        if (colour!=null) {
            cellFormat.setBackground(colour);
        }
        cellFormat.setWrap(false);
        return cellFormat;
    }
    private static void resizeColumns(WritableSheet sheet, int titleRow){
        //  100 пикселей = 100/9 единиц

            sheet.setColumnView(0, 440/9);
            sheet.setColumnView(1, 90/9);
            sheet.setColumnView(2, 90/9);
            sheet.setColumnView(3, 90/9);
            sheet.setColumnView(4, 90/9);
            sheet.setColumnView(5, 90/9);
            sheet.setColumnView(6, 400/9);
            sheet.setColumnView(7, 300/9);
            sheet.setColumnView(8, 900/9);
            sheet.setColumnView(9, 340/9);
            sheet.setColumnView(10, 340/9);
            sheet.setColumnView(11, 900/9);
            sheet.setColumnView(12, 200/9);
            sheet.setColumnView(13, 200/9);
    }
    //класс для хранения данных о категории проблем и относящимся к ним заявкам
    private class Category{
        //код проблемы
        int code;
        String description;

        //название счетчика (было, поступило, выполнено, остаток) к количеству соотв. заявок
        Map<String, List<RequestDto>> typeToRequest;
        public Category(int code, String description){
            this.typeToRequest = new HashMap<>();
            typeToRequest.put("Было", new ArrayList<>());
            typeToRequest.put("Поступило", new ArrayList<>());
            typeToRequest.put("Выполнено", new ArrayList<>());
            typeToRequest.put("Остаток", new ArrayList<>());
            this.code = code;
            this.description = description;
        }
        public List<RequestDto> getRequests(){
            List<RequestDto> requests = new ArrayList<>();

            TreeSet<RequestDto> requestSet = new TreeSet<>(new Comparator<RequestDto>() {
                @Override
                public int compare(RequestDto o1, RequestDto o2) {
                    return o1.getRegistrationDate().compareTo(o2.getRegistrationDate());
                }
            });
            typeToRequest.values().stream().forEach(e->{
                requestSet.addAll(e);
            });
            requests.addAll(requestSet);
            return requests;
        }


        public void addRequest(RequestDto requestDto) {


            //start - 7:00 текущего дня
            //end - текущее время

            //дата завершения должна либо отсутствовать
            // либо быть после старта, дата регистрации перед стартом
            boolean isUsed = false;
            if (requestDto.getRegistrationDate().isBefore(start)&& (requestDto.getCloseDate()==null || requestDto.getCloseDate().isAfter(start))) {
                typeToRequest.get("Было").add(requestDto);
                isUsed=true;
            }
            //дата регистрации между стартом и окончанием
            if (requestDto.getRegistrationDate().isAfter(start)&&requestDto.getRegistrationDate().isBefore(end)){
                typeToRequest.get("Поступило").add(requestDto);
                isUsed=true;
            }

            //дата закрытия не должна быть нулл и должна быть между стартом и окончанием
            if (requestDto.getCloseDate()!=null && requestDto.getCloseDate().isAfter(start) && requestDto.getCloseDate().isBefore(end)){
                typeToRequest.get("Выполнено").add(requestDto);
                isUsed=true;
            }
            //дата закрытия отсутствует,
            // либо дата закрытия после окончания и дата регистрации перед окончанием
            if ((requestDto.getCloseDate()==null || requestDto.getCloseDate().isAfter(end)) && requestDto.getRegistrationDate().isBefore(end)){
                typeToRequest.get("Остаток").add(requestDto);
                isUsed=true;
            }
            if (isUsed){
                getRequestDescription(requestDto);
                getPhotosnSchemesAmount(requestDto);
            }
         }
        public int getTypeCount(String type){
            return typeToRequest.get(type).size();
        }
        public String getDescription(){
            return this.description;
        }

        public int getCode() {
            return code;
        }

        public String toString(){
            StringBuilder sb = new StringBuilder("Код проблемы: "+ code+"\nОписание проблемы: "+description+"\n");
            typeToRequest.entrySet().stream().forEach(e->{sb.append("\t"+e.getKey()+" - "+e.getValue().size()+"\n\n\n\n");});
            return sb.toString();
        }

    }
    public void getRequestDescription(RequestDto req){
            StringBuilder desc = new StringBuilder("");
            List<CommentDto> comments = BrigadesUI.getRestTemplate().getRequestComments(req.getId());
            try {
                comments.stream().forEach(e->{
                    if (!e.getContent().endsWith(".")){
                        desc.append(e.getContent()).append(".");
                    }
                    desc.append(" ");
                });
            }catch (NullPointerException ex){
            }
            requestToDescription.put(req,desc.toString());
    }
    public void getPhotosnSchemesAmount(RequestDto req){
            List<AttachmentDto> attach = BrigadesUI.getRestTemplate().getRequestAttachments(req.getId());
            int photos=0;
            int schemes=0;
            for (int i = 0; i < attach.size(); i++) {
                AttachmentDto curAttach = attach.get(i);
                if(curAttach.getType().contains("схема выключения")) {
                    schemes++;
                }else{
                    photos++;
                }
            }
            requestToSchemeAmount.put(req,schemes);
            requestToPhotoAmount.put(req,photos);
        /*requestToPhotoAmount.entrySet().stream().forEach(e->{
            getLogger().debug(e.getKey().getAddress()+" "+e.getValue());
        });
        requestToSchemeAmount.entrySet().stream().forEach(e->{
            getLogger().debug(e.getKey().getAddress()+" "+e.getValue());
        });*/
    }

}
