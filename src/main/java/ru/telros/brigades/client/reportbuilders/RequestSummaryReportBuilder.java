package ru.telros.brigades.client.reportbuilders;


import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.ProblemDto;
import ru.telros.brigades.client.dto.RequestDto;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

//Класс для построения отчета "Сводка по заявкам" (По файлу "Сводная Северное 25.10.2017.xls")
public class RequestSummaryReportBuilder implements HasLogger {
    private LinkedHashMap<String, String> orderedCodesToDescs;
    List<RequestDto> requests;
    ZonedDateTime currentTime;
    ZonedDateTime fromTime;
    List<Category> toPrint;
    List<Long> idsToExclude;
    String organizations;
    Map<String, Category> categoryIDToCategory;
    public RequestSummaryReportBuilder(List<RequestDto> requests, ZonedDateTime fromTime, ZonedDateTime currentTime, String organizations) {
        idsToExclude = Arrays.asList(0l, 2l,3l,4l,5l,6l,13l,22l,61l,10l,15l,16l,17l,25l,30l,31l,35l
                /*,4001l,4002l,4003l,4004l,4005l,4006l*/);
        this.fromTime = fromTime;
        this.currentTime = currentTime;
        this.requests = requests;
        this.organizations = organizations;
        categoryIDToCategory = new HashMap<>();

        requests.stream().forEach(e -> {
            String problemCode;

            problemCode = e.getProblemCode();
            if (problemCode == null) {
                problemCode = "-1";
            }
            String problemDesc = e.getProblem();
            if (problemDesc == null) {
                problemDesc = "Категория не задана";
            }
            if (!problemDesc.equals("Не ГЛ")) {
                if (categoryIDToCategory.get(problemCode) == null) {
                    categoryIDToCategory.put(problemCode, new Category(problemCode, problemDesc));
                }
                categoryIDToCategory.get(problemCode).addRequest(e);
            }
        });
        //Добавление всех merged категорий
        Category leakings = getMergedCategory("Вытекания", Arrays.asList(6, 13, 22, 61));
        categoryIDToCategory.put(leakings.categoryCode, leakings);
        Category lukeOpenings = getMergedCategory("Открытые крышки колодцев", Arrays.asList(10, 15));
        categoryIDToCategory.put(lukeOpenings.categoryCode, lukeOpenings);
//        Category disables = getMergedCategory("Отключения", Arrays.asList(4001,4002,4003,4004,4005,4006));
//        categoryIDToCategory.put(disables.categoryCode, disables);
        Category sum = getSummary(categoryIDToCategory);
        Category portal = getPortal(categoryIDToCategory);
        categoryIDToCategory.put("sum", sum);
        categoryIDToCategory.put("portal", portal);
        orderedCodesToDescs = initOrderMap();
        toPrint = getRightOrder(orderedCodesToDescs);
    }
    private LinkedHashMap<String, String> initOrderMap(){
        LinkedHashMap<String,String> map = new LinkedHashMap<>();

        // сначала формируется список (код - описание), на него натягиваются заявки из бд
        // до открытых крышек колодцев порядок как в экселе, после - по возрастанию

        //получение списка {код-описание проблемы}
        List<ProblemDto> problemDtos = BrigadesUI.getRestTemplate().getProblems();
        Map<Long,String> sourceIdToDesc = new HashMap<>();
        //список кодов, участвующих в совмещенных ячейках

        problemDtos.stream().forEach(e->{sourceIdToDesc.put(e.getId(),e.getName());});
        map.put("6, 13, 22, 61", "Вытекания");
        map.put("2", sourceIdToDesc.remove(2l));
        map.put("3", sourceIdToDesc.remove(3l));
        map.put("4", sourceIdToDesc.remove(4l));
        map.put("5", sourceIdToDesc.remove(5l));
        map.put("10, 15", "Открытые крышки колодцев");
//        map.put("4001, 4002, 4003, 4004, 4005, 4006", "Отключения");

        sourceIdToDesc.entrySet().stream().filter(e->!idsToExclude.contains(e.getKey())).sorted(Comparator.comparing(Map.Entry::getKey)).forEach(e->{map.put(e.getKey()+"",e.getValue());});
        map.put("sum" , "общ.кол-во заявок (все кода) ");
        map.put("portal" , "портал");
        /*map.put("29", "неисправные в/у");
        map.put("8", "просадка пр.ч.");
        map.put("9", "просадка к-ца пр.ч.");
        map.put("11", "просадка на газоне");
        map.put("12", "просадка к-ца на газоне");
        map.put("20", "благоустройство"	);*/
        map.entrySet().stream().forEach(e-> getLogger().debug(e.toString()));
        return map;
    }
    /**
     * формирование правильного порядка вывода категорий на основе заданной в
     * {@link RequestSummaryReportBuilder#initOrderMap()} Map
     */
    private LinkedList<Category> getRightOrder(Map<String, String> orderMap){
        LinkedList<Category> orderedList = new LinkedList<>();

        Iterator<Map.Entry<String,String>> it = orderMap.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<String,String> curPair = it.next();
            if (!curPair.getKey().equals("sum") && !curPair.getKey().equals("portal")) {
                Category curCategory = categoryIDToCategory.get(curPair.getKey());
                if (curCategory != null) {
                    orderedList.add(curCategory);
                    categoryIDToCategory.remove(curPair.getKey());
                } else {
                    orderedList.add(new Category(curPair.getKey(), curPair.getValue()));
                }
            }
        }
        idsToExclude.stream().forEach(e->{
            categoryIDToCategory.remove(e+"");
        });
        Category left = categoryIDToCategory.remove("-1");
        categoryIDToCategory.entrySet().stream().forEach(e-> {
            if (!e.getKey().equals("sum") && !e.getKey().equals("portal")) {
                if(e.getValue().categoryDesc.equals("Иное")){
                    e.getValue().categoryDesc="";
                }
                orderedList.add(e.getValue());
            }
        });
        //тут выпадает нулл поинтер
        try {
            left.categoryDesc="Код не задан";
            left.categoryCode="";
            orderedList.add(left);
        }catch (NullPointerException e){

        }

        Category sum = categoryIDToCategory.get("sum");
        sum.categoryCode="";
        orderedList.add(sum);
        Category portal = categoryIDToCategory.get("portal");
        portal.categoryCode="";
        orderedList.add(portal);
        return orderedList;
    }
    public String getHtmlText(){
        StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta charset=\"cp1251\">\n"+
                "<style>\n" +
                "\ttable{\n" +
                "border:1px solid black;\n"+
                "\t\tborder-collapse: collapse;\n" +
                "\t}\n" +
                "\tth{\n" +
                "\t\tborder: 2px solid black;\n" +
                "\t\ttext-align : center;\n" +
                "\t\tfont-weight: bold;\n" +
                "\t\t\n" +
                "\t}\n" +
                "\ttd{\n" +
                "\t\tborder: 1px solid black;\n" +
                "\t\ttext-align : center;\n" +
                "\t\tborder-color: black; \n" +
                "\t}\n" +
                "</style>\n"+
                "</head>\n"+
                "<body>\n" +
                "<h1 colspan=6 style=\"text-align:center;\">Сводка по заявкам."+"</h1>\n"+
                "<p>Организации: "+organizations+"</p>\n"+
                "<table style='margin: auto;'>\n" +
                "<th colspan=2>Код заявки</th><th>Было</th><th>Поступило</th><th>Выполнено</th><th>Остаток</th>");
        requests.stream().forEach(e->{
            sb.append("<!--- ");
            sb.append(e.getProblemCode()+"; reg: "+e.getRegistrationDate()+"; close:"+e.getCloseDate());
            sb.append(" --->\n");
        });



        for (int row = 0; row < toPrint.size(); row++) {
            sb.append("<tr>");
            List<String> rowSplitted = getOutputDataList(toPrint.get(row));
            for (int cell = 0; cell < rowSplitted.size(); cell++) {
                boolean isYellow = row%2==0;
                String bgColor = "white";
                if(isYellow==true){
                    bgColor="rgb(255, 255, 153)";
                }
                if(((row==0) && (cell>1))||(cell==rowSplitted.size()-1)){
                    bgColor="rgb(255, 153, 204)";
                }
                if(row==toPrint.size()-2){
                    bgColor="rgb(0, 176, 240)";
                }
                if(row==toPrint.size()-1){
                    bgColor="rgb(255, 192, 0)";
                }
                sb.append("<td style=\"background-color:"+bgColor+";");
                if (cell==0){
                    sb.append(" text-align: left;");
                }
                sb.append("\">");
                sb.append(getOutputDataList(toPrint.get(row)).get(cell));

                sb.append("</td>");

            }
            sb.append("</tr>");
        }

                sb.append("</table>\n"+
                "</body>\n" +
                "</html>");
        return sb.toString();
    }
    //формирование объекта сумм. строки
    private Category getSummary(Map<String, Category> allCategories){
        Category sum = new Category("sum","общ.кол-во заявок (все кода) ");
        allCategories.entrySet().stream().filter(e->{
            try {
                long curCode = Long.parseLong(e.getValue().categoryCode);
                return !idsToExclude.contains(curCode);
            }catch (NumberFormatException ex){}
            return true;
        }).forEach(e->e.getValue().getRequestsMap().entrySet().forEach(entry->{
            sum.addRequestsToState(entry.getKey(),entry.getValue());
        }));
        return sum;
    }
    private Category getPortal(Map<String, Category> allCategories){
        Category portal = new Category("portal","портал");
        allCategories.entrySet().stream().filter(e->{
            try {
                long curCode = Long.parseLong(e.getValue().categoryCode);
                return !idsToExclude.contains(curCode);
            }catch (NumberFormatException ex){}
            return true;
        }).forEach(e->e.getValue().getRequestsMap().entrySet().forEach(entry->{
            portal.addRequestsToState(entry.getKey(),getPortalRequests(entry.getValue()));
        }));
        return portal;
    }
    private List<RequestDto> getPortalRequests(List<RequestDto> input){
        return input.stream().filter(e-> e.getPortal()!=null).collect(Collectors.toList());
    }
    private Category getMergedCategory(String desc, List<Integer> mergingIDs){
        Category category = new Category(desc, mergingIDs);
        List<Category> mergingCategories = new ArrayList<>();
        for (int i = 0; i < mergingIDs.size(); i++) {
            try {
                mergingCategories.add(categoryIDToCategory.get(mergingIDs.get(i)+""));
            }catch (NullPointerException e){

            }
        }
        mergingCategories.stream().forEach(e->{
            try {
                e.getRequestsMap().entrySet().forEach(stateListEntry -> {
                    category.addRequestsToState(stateListEntry.getKey(),stateListEntry.getValue());
                });
            }catch (NullPointerException ex){}

        });
        mergingCategories.stream().forEach(e->{categoryIDToCategory.remove(e);});
        return category;
    }
    //выводит лист строк, которые просто нужно передать в "RowWriter"
    private List<String> getOutputDataList(Category category){
        List<String> data = new ArrayList<>();
//        if (category.categoryCode.equals("-1")){
//            data.add("Вытекания");
//            data.add("6, 13, 22, 61");
//        }else {
            data.add(category.categoryDesc);
            data.add(category.categoryCode);
//        }
        data.add(category.stateToRequestIDs.get(State.WERE).size()+"");
        data.add(category.stateToRequestIDs.get(State.INCOME).size()+"");
        data.add(category.stateToRequestIDs.get(State.DONE).size()+"");
        data.add(category.stateToRequestIDs.get(State.LEFT).size()+"");
        return data;
    }
    private class Category{
        String categoryCode;
        String categoryDesc;
        //Тип к списку заявок
        Map<State,List<RequestDto>> stateToRequestIDs;
        //все заявки в этой категории
        Set<RequestDto> categoryRequests;
        public Category(String categoryCode, String categoryDesc){
            this.categoryCode = categoryCode;
            this.categoryDesc = categoryDesc;
            stateToRequestIDs = new HashMap<>();
            stateToRequestIDs.put(State.WERE, new ArrayList<>());
            stateToRequestIDs.put(State.INCOME, new ArrayList<>());
            stateToRequestIDs.put(State.DONE, new ArrayList<>());
            stateToRequestIDs.put(State.LEFT, new ArrayList<>());
            categoryRequests = new HashSet<>();
        }
        public Category(String categoryDesc,List<Integer> categoryCode){
            stateToRequestIDs = new HashMap<>();
            stateToRequestIDs.put(State.WERE, new ArrayList<>());
            stateToRequestIDs.put(State.INCOME, new ArrayList<>());
            stateToRequestIDs.put(State.DONE, new ArrayList<>());
            stateToRequestIDs.put(State.LEFT, new ArrayList<>());
            categoryRequests = new HashSet<>();
            this.categoryDesc=categoryDesc;
            StringBuilder newCode = new StringBuilder();
            categoryCode.stream().forEach(ints -> {
                newCode.append(ints+", ");
            });
            this.categoryCode = newCode.substring(0,newCode.length()-2);

        }
        public String toString(){
            StringBuilder stringBuilder = new StringBuilder("Category #"+categoryCode+" - "+categoryDesc+"\n");
            stateToRequestIDs.entrySet().forEach(e->{
                stringBuilder.append("\t"+e.getKey().name()+": ");
                e.getValue().stream().forEach(v->stringBuilder.append(v+" "));
                stringBuilder.append("\n");
            });
            return stringBuilder.toString();
        }
        public void addRequestsToState(State state, List<RequestDto> requests){
            this.stateToRequestIDs.get(state).addAll(requests);
            categoryRequests.addAll(requests);
        }
        public Map<State, List<RequestDto>> getRequestsMap(){
            return this.stateToRequestIDs;
        }
        public void addRequest(RequestDto requestDto){
            categoryRequests.add(requestDto);
            List<State> curRequestStates = getRequestsStates(requestDto);
            curRequestStates.stream().forEach(e->stateToRequestIDs.get(e).add(requestDto));
        }
        public List<State> getRequestsStates(RequestDto request){
            ZonedDateTime startTime = fromTime;
            ZonedDateTime endTime = currentTime;
            ZonedDateTime regTime = request.getRegistrationDate();
            ZonedDateTime closeDate = request.getCloseDate();
            List<State> resultStates = new ArrayList<>();
            if (
                    (regTime.isBefore(startTime)) && ((closeDate==null)||(closeDate.isAfter(startTime)))
            ){
                resultStates.add(State.WERE);
            }
            if (
                    (regTime.isAfter(startTime)) && (regTime.isBefore(endTime))
            ){
                resultStates.add(State.INCOME);
            }
            if(
                    (closeDate!=null)&&((closeDate.isAfter(startTime))&&(closeDate.isBefore(endTime)))
            ){
                resultStates.add(State.DONE);
            }
            if(
                    regTime.isBefore(endTime) && ((closeDate==null) || (closeDate.isAfter(endTime)))
            ){
                resultStates.add(State.LEFT);
            }
            return resultStates;
        }
    }
    //было поступило выполнено остаток
    private enum State{WERE, INCOME, DONE, LEFT}


}
