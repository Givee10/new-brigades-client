package ru.telros.brigades.client.event.camera;

import ru.telros.brigades.client.dto.CameraDto;
import ru.telros.brigades.client.event.EntityEvent;

public class UpdateCameraEvent extends EntityEvent<CameraDto> {
	public UpdateCameraEvent(CameraDto entity) {
		super(entity);
	}
}
