package ru.telros.brigades.client.event.vehicle;

import ru.telros.brigades.client.dto.VehicleDto;
import ru.telros.brigades.client.event.EntityEvent;

public class ShowVehicleEvent extends EntityEvent<VehicleDto> {
	private Object sender;

	public ShowVehicleEvent(final VehicleDto vehicle) {
		this(vehicle, null);
	}

	public ShowVehicleEvent(final VehicleDto vehicle, final Object sender) {
		super(vehicle);
		this.sender = sender;
	}

	public Object getSender() {
		return sender;
	}

	public void setSender(Object sender) {
		this.sender = sender;
	}
}
