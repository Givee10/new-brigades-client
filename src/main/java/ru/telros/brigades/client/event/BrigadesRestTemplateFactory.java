package ru.telros.brigades.client.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import ru.telros.brigades.client.dto.LoginDto;

@Component
public class BrigadesRestTemplateFactory {
	@Value("${test.server-ip}")
	private String serverIp;

	@Value("${test.server-port}")
	private String serverPort;

	@Value("${test.server-hl-ip}")
	private String hlIp;

	@Value("${test.server-hl-port}")
	private String hlPort;

	@Value("${test.server-toir-ip}")
	private String toirIp;

	@Value("${test.server-toir-port}")
	private String toirPort;

	@Value("${test.server-tc-ip}")
	private String tcIp;

	@Value("${test.server-tc-port}")
	private String tcPort;

	@Value("${test.server-timezone}")
	private String serverTimeZone;

	@Autowired
	private RestTemplateBuilder builder;

	public RestOperations createRestOperations() {
		return builder.build();
	}

	public RestOperations createRestOperations(LoginDto loginDto) {
		return builder.basicAuthorization(loginDto.getUsername(), loginDto.getPassword()).build();
	}

	public String getServerIp() {
		return serverIp;
	}

	public String getServerPort() {
		return serverPort;
	}

	public String getHlIp() {
		return hlIp;
	}

	public String getHlPort() {
		return hlPort;
	}

	public String getToirIp() {
		return toirIp;
	}

	public String getToirPort() {
		return toirPort;
	}

	public String getTcIp() {
		return tcIp;
	}

	public String getTcPort() {
		return tcPort;
	}

	public String getServerTimeZone() {
		return serverTimeZone;
	}
}
