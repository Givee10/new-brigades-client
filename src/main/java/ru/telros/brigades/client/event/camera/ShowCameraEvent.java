package ru.telros.brigades.client.event.camera;

import ru.telros.brigades.client.dto.CameraDto;
import ru.telros.brigades.client.event.EntityEvent;

public class ShowCameraEvent extends EntityEvent<CameraDto> {

	private Object sender;

	public ShowCameraEvent(final CameraDto camera) {
		this(camera, null);
	}

	public ShowCameraEvent(final CameraDto camera, final Object sender) {
		super(camera);
		this.sender = sender;
	}

	public Object getSender() {
		return sender;
	}

	public void setSender(Object sender) {
		this.sender = sender;
	}
}
