package ru.telros.brigades.client.event;

/**
 * Broadcast message
 * <p>
 * Used for send events and messages between client UIs throw application event bus
 */
public class BroadcastMessageEvent extends ApplicationBaseEvent {

	public BroadcastMessageEvent(final String message) {
		super(message);
	}

	public String getMessage() {
		return (String) getSource();
	}
}
