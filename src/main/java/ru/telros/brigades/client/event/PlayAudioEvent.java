package ru.telros.brigades.client.event;

import com.vaadin.server.Resource;

public class PlayAudioEvent {
	private final Resource resource;

	public PlayAudioEvent(Resource resource) {
		this.resource = resource;
	}

	public Resource getResource() {
		return resource;
	}
}
