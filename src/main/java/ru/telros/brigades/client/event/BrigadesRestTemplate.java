package ru.telros.brigades.client.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.vaadin.ui.UI;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.DateInterval;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.*;

public class BrigadesRestTemplate implements HasLogger {
	private BrigadesRestTemplateFactory factory = BeanUtil.getBean(BrigadesRestTemplateFactory.class);
	private ObjectMapper objectMapper = BeanUtil.getBean(ObjectMapper.class);

	private String SERVER = "http://" + factory.getServerIp() + ":" + factory.getServerPort();
	private String PING_PATH = SERVER + "/ping";
	private String API_PATH = SERVER + "/api/v1";

	private String MAVR_IP = "192.168.221.128";
	private String MAVR_PORT = "80";
	private String MAVR_SERVER = "http://" + MAVR_IP + ":" + MAVR_PORT;
	private String MAVR_TOKEN = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJSb2xlIjoiUk9MRV9VU0VSIiwic3ViIjoiSXZhbm92X0dBIiwiZXhwIjoxNTc4MDAwMDAwLCJpYXQiOjE1NjgwMDAwMDB9.BYFRJwN33KWQ3mvPEHhSXIcXPQuQzjV3MMhnioKmSCD1qtld34wy4CKNq-j8reB83ITh7zkHAUmxcEJNulmlqg";

	private String CHECK_IP = "127.0.0.1";
	private String CHECK_PORT = "80";
	private String CHECK_SERVER = "http://" + CHECK_IP + ":" + CHECK_PORT;
	private String CHECK = "/check";
	private String CHECK_LIST = "/check-list";

	private String actions = API_PATH + "/actions";
	private String assets = API_PATH + "/assets";
	private String attachments = API_PATH + "/attachments";
	private String brigades = API_PATH + "/brigades";
	private String brigadePlans = API_PATH + "/brigades-plans";
	private String cameras = API_PATH + "/cameras";
	private String employees = API_PATH + "/employees";
	private String equipments = API_PATH + "/equipments";
	private String events = API_PATH + "/events";
	private String gantt_brigades = API_PATH + "/gantt/brigades";
	private String geocodes = API_PATH + "/geocodes";
	private String inventory = API_PATH + "/inventory";
	private String locations = API_PATH + "/locations";
	private String maps = API_PATH + "/maps";
	private String mechanization = API_PATH + "/mechanization";
	private String methodsFormations = API_PATH + "/methodsFormations";
	private String organizations = API_PATH + "/organizations";
	private String problems = API_PATH + "/problems";
	private String roles = API_PATH + "/roles";
	private String reports = API_PATH + "/reports";
	private String requests = API_PATH + "/requests";
	private String sessions = API_PATH + "/sessions";
	private String sla = API_PATH + "/sla";
	private String sources = API_PATH + "/sources";
	private String statuses = API_PATH + "/statuses";
	private String types_works = API_PATH + "/types-works";
	private String urgencies = API_PATH + "/urgencies";
	private String users = API_PATH + "/users";
	private String userGroups = API_PATH + "/user-groups";
	private String vehicles = API_PATH + "/vehicles";
	private String works = API_PATH + "/works";
	private String workItems = API_PATH + "/work-items";

	private String action_id = actions + "/%d";
	private String asset_id = assets + "/%d";
	private String attachment_id = attachments + "/%d";
	private String brigade_id = brigades + "/%d";
	private String brigadePlan_id = brigadePlans + "/%d";
	private String camera_id = cameras + "/%d";
	private String employee_id = employees + "/%d";
	private String event_guid = events + "/%s";
	private String organization_id = organizations + "/%d";
	private String problem_id = problems + "/%d";
	private String report_id = reports + "/%d";
	private String request_id = requests + "/%d";
	private String source_id = sources + "/%d";
	private String status_id = statuses + "/%d";
	private String types_work_id = types_works + "/%d";
	private String user_id = users + "/%d";
	private String userGroup_id = userGroups + "/%d";
	private String vehicle_id = vehicles + "/%d";
	private String work_id = works + "/%d";
	private String workItem_id = workItems + "/%d";

	private String asset_atributes = asset_id + "/attributes";
	private String brigade_employees = brigade_id + "/employees";
	private String brigade_works = brigade_id + "/works";
	private String brigade_vehicles = brigade_id + "/vehicles";
	private String brigade_plans = brigade_id + "/plans";
	private String brigadePlan_vehicles = brigadePlan_id + "/vehicles";
	private String brigadePlan_equipments = brigadePlan_id + "/equipments";
	private String brigadePlan_inventory = brigadePlan_id + "/inventory";
	private String brigadePlan_mechanization = brigadePlan_id + "/mechanization";
	private String report_data = report_id + "/data";
	private String request_attachments = request_id + "/attachments";
	private String request_comments = request_id + "/comments";
	private String request_responsible = request_id + "/responsible";
	private String request_status = request_id + "/status";
	private String request_works = request_id + "/works";
	private String types_work_items = types_work_id + "/items";
	private String user_brigades = user_id + "/brigades";
	private String user_groups = user_id + "/groups";
	private String user_organizations = user_id + "/organizations";
	private String user_photo = user_id + "/photo";
	private String user_roles = user_id + "/roles";
	private String user_data = user_id + "/userdata";
	private String userGroup_users = userGroup_id + "/users";
	private String vehicle_position = vehicle_id + "/position";
	private String vehicle_tracks = vehicle_id + "/tracks";
	private String work_brigades = work_id + "/brigades";
	private String work_request = work_id + "/requests";
	private String work_status = work_id + "/status";
	private String work_assets = work_id + "/assets";
	private String work_attachments = work_id + "/attachments";
	private String work_comments = work_id + "/comments";
	private String work_employees = work_id + "/employees";
	private String work_equipments = work_id + "/equipments";
	private String work_elements = work_id + "/elements";
	private String work_inventory = work_id + "/inventory";
	private String work_items = work_id + "/work-items";
	private String work_mechanization = work_id + "/mechanization";
	private String workItem_children = workItem_id + "/childs";

	private String asset_atribute_id = asset_atributes + "/%d";
	private String brigade_plan_id = brigade_plans + "/%d";
	private String request_attachment_id = request_attachments + "/%d";
	private String work_asset_id = work_assets + "/%d";
	private String work_attachment_id = work_attachments + "/%d";
	private String work_employee_id = work_employees + "/%d";
	private String work_equipment_id = work_equipments + "/%d";
	private String work_inventory_id = work_inventory + "/%d";
	private String work_item_id = work_items + "/%d";
	private String work_mechanization_id = work_mechanization + "/%d";
	private String work_request_id = work_request + "/%d";
	private String user_data_key = user_data + "/%s";

	private String brigade_plan_employees = brigade_plan_id + "/employees";
	private String request_attachment_content = request_attachment_id + "/content";
	private String work_attachment_content = work_attachment_id + "/content";
	private String work_item_children = work_item_id + "/childs";

	private String asset_types = assets + "/types";
	private String attachment_types = attachments + "/types";
	private String employee_absence_reasons = employees + "/absence-reasons";
	private String employee_posts = employees + "/posts";
	private String equipment_models = equipments + "/models";
	private String equipment_numbers = equipments + "/numbers";
	private String equipment_types = equipments + "/types";
	private String events_actions = events + "/actions";
	private String event_types = events + "/types";
	private String geocode_district = geocodes + "/district";
	private String inventory_models = inventory + "/models";
	private String inventory_numbers = inventory + "/numbers";
	private String inventory_types = inventory + "/types";
	private String inventory_units = inventory + "/units";
	private String map_entities = maps + "/entities";
	private String mechanization_models = mechanization + "/models";
	private String mechanization_numbers = mechanization + "/numbers";
	private String mechanization_types = mechanization + "/types";
	private String request_statuses = requests + "/statuses";
	private String sla_reaction_time = sla + "/reaction-time";
	private String sla_control_accuracy = sla + "/control-accuracy";
	private String sla_user_action = sla + "/user-action";
	private String sla_work_code = sla + "/work-code";
	private String status_hl = statuses + "/hl";
	private String status_requests = statuses + "/requests";
	private String status_works = statuses + "/works";
	private String sources_requests = sources + "/requests";
	private String sources_toir = sources + "/toir";
	private String sources_works = sources + "/works";
	private String urgency_requests = urgencies + "/requests";
	private String vehicles_models = vehicles + "/models";
	private String vehicles_numbers = vehicles + "/numbers";
	private String vehicles_types = vehicles + "/types";
	private String work_statuses = works + "/statuses";
	private String workItem_statuses = workItems + "/statuses";

	private String asset_type_id = asset_types + "/%d";
	private String asset_type_attributes = asset_type_id + "/attributes";
	private String work_item_roots = work_items + "/roots";

	private String param_from = "from";
	private String param_to = "to";
	private String param_skip = "skip";
	private String param_limit = "limit";
	private String param_on = "on";
	private String param_guid = "guid";
	private String param_objectGuid = "objectGuid";
	private String param_address = "address";
	private String param_latitude = "latitude";
	private String param_longitude = "longitude";
	private String param_login = "login";
	private String param_file = "file";
	private String param_prior = "prior";
	private String param_source = "source";
	private String param_status = "status";
	private String param_state = "state";
	private String param_commentary = "commentary";
	private String param_gati = "gati";
	private String param_regulations = "regulations";
	private String param_calendar = "calendar";
	private String param_description = "description";
	private String param_mobile = "isMobile";
	private String param_organization = "organization";
	private String param_status_hl = "status-hl";
	private String param_type = "type";
	private String param_parent = "parent-id";
	private String param_code = "operCode";
	private String param_number = "number";
	private String param_name = "name";

	private Integer custom_skip = 0;
	private Integer custom_limit = 20;
	private Integer extended_limit = 20000;

	private RestOperations initRestTemplate() {
		LoginDto login = BrigadesUI.getCurrentLogin();
		if (login == null) return factory.createRestOperations();
		else return factory.createRestOperations(login);
	}

	/* MAIN METHODS */

	private void logExceptions(String url, RestClientException e) {
		if (e instanceof RestClientResponseException) {
			getLogger().error("Request {} returns with error {}", url, ((RestClientResponseException)e).getResponseBodyAsString());
		}
		if (e instanceof ResourceAccessException) {
			getLogger().error(e.getMessage());
		}
	}

	public boolean isAuthenticated() {
		PingDto ping = ping();
		if (ping == null) return false;
		getLogger().debug(ping.debugInfo());
		return ping.getAuthorized();
	}

	public ResponseEntity<UserDto> doLogin(LoginDto login) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_login, login.getUsername());
		String url = UriComponentsBuilder.fromUriString(users).queryParams(map).build().toUriString();
		//getLogger().debug("Request: {}, Login: {}", url, login.toString());
		return initRestTemplate().getForEntity(url, UserDto.class);
	}

	public String getGanttTimezone() {
		return factory.getServerTimeZone();
	}

	private String returnDebugString(String url) {
		try {
			ResponseEntity<String> entity = initRestTemplate().getForEntity(url, String.class);
			return entity.getBody();
		} catch (RestClientResponseException e) {
			getLogger().error("Request {} returns with error {}", url, e.getResponseBodyAsString());
			return e.getResponseBodyAsString();
		} catch (ResourceAccessException e) {
			getLogger().error(e.getMessage());
			return null;
		}
	}

	private String returnString(String url) {
		try {
			ResponseEntity<String> entity = initRestTemplate().getForEntity(url, String.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private void deleteEntity(String url) {
		try {
			initRestTemplate().delete(url);
		} catch (RestClientException e) {
			logExceptions(url, e);
		}
	}

	private <T> T makeRequest(String url, HttpMethod method, HttpEntity<T> httpEntity, TypeToken<T> resultTypeToken) {
		ParameterizedTypeReference<T> responseTypeRef = ParameterizedTypeReferenceBuilder.fromTypeToken(
				resultTypeToken.where(new TypeParameter<T>() {}, resultTypeToken));
		ResponseEntity<T> response = initRestTemplate().exchange(url, method, httpEntity, responseTypeRef);
		return response.getBody();
	}

	private <T> T makeEntityRequest(String url, HttpMethod method, HttpEntity<T> httpEntity, Class<T> tClass) {
		try {
			TypeToken<T> resultTypeToken = TypeToken.of(tClass);
			ParameterizedTypeReference<T> responseTypeRef = ParameterizedTypeReferenceBuilder.fromTypeToken(
					resultTypeToken.where(new TypeParameter<T>() {}, tClass));
			ResponseEntity<T> response = initRestTemplate().exchange(url, method, httpEntity, responseTypeRef);
			return response.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private <T> List<T> makeListRequest(String url, HttpMethod method, HttpEntity<List<T>> httpEntity, Class<T> tClass) {
		try {
			TypeToken<T> resultTypeToken = TypeToken.of(tClass);
			ParameterizedTypeReference<List<T>> responseTypeRef = ParameterizedTypeReferenceBuilder.fromTypeToken(
					new TypeToken<List<T>>() {}.where(new TypeParameter<T>() {}, resultTypeToken));
			ResponseEntity<List<T>> response = initRestTemplate().exchange(url, method, httpEntity, responseTypeRef);
			return response.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	public LoginDto checkGetRequest() {
		String url = CHECK_SERVER + CHECK;
		return makeEntityRequest(url, HttpMethod.GET, null, LoginDto.class);
	}

	public List<LoginDto> checkGetListRequest() {
		String url = CHECK_SERVER + CHECK_LIST;
		return makeListRequest(url, HttpMethod.GET, null, LoginDto.class);
	}

	public LoginDto checkPostRequest(LoginDto loginDto) {
		String url = CHECK_SERVER + CHECK;
		return makeEntityRequest(url, HttpMethod.POST, new HttpEntity<>(loginDto), LoginDto.class);
	}

	public List<LoginDto> checkPostListRequest(List<LoginDto> loginDtoList) {
		String url = CHECK_SERVER + CHECK_LIST;
		return makeListRequest(url, HttpMethod.POST, new HttpEntity<>(loginDtoList), LoginDto.class);
	}

	public LoginDto checkPutRequest(LoginDto loginDto) {
		String url = CHECK_SERVER + CHECK;
		return makeEntityRequest(url, HttpMethod.PUT, new HttpEntity<>(loginDto), LoginDto.class);
	}

	public List<LoginDto> checkPutListRequest(List<LoginDto> loginDtoList) {
		String url = CHECK_SERVER + CHECK_LIST;
		return makeListRequest(url, HttpMethod.PUT, new HttpEntity<>(loginDtoList), LoginDto.class);
	}

	public void checkDeleteRequest() {
		String url = CHECK_SERVER + CHECK;
		makeEntityRequest(url, HttpMethod.DELETE, null, Void.class);
	}

	private MultiValueMap<String, String> createMap() {
		return new LinkedMultiValueMap<>();
	}

	private MultiValueMap<String, String> getMap() {
		return getMap(null, null, custom_skip, extended_limit);
	}

	private MultiValueMap<String, String> getMap(Integer skip, Integer limit) {
		return getMap(null, null, skip, limit);
	}

	private MultiValueMap<String, String> getMap(ZonedDateTime start, ZonedDateTime end) {
		return getMap(start, end, custom_skip, extended_limit);
	}

	private MultiValueMap<String, String> getMap(ZonedDateTime start, ZonedDateTime end, Integer skip, Integer limit) {
		MultiValueMap<String, String> map = createMap();
		if (start != null) map.add(param_from, DatesUtil.formatToUTC(start));
		if (end != null) map.add(param_to, DatesUtil.formatToUTC(end));
		map.add(param_skip, skip.toString());
		map.add(param_limit, limit.toString());
		return map;
	}

	private ActionDto postAction(String url, ActionDto actionDto) {
		try {
			ResponseEntity<ActionDto> entity = initRestTemplate().postForEntity(url, actionDto, ActionDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private ActionDto returnAction(String url) {
		try {
			ResponseEntity<ActionDto> entity = initRestTemplate().getForEntity(url, ActionDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<ActionDto> returnActions(String url) {
		try {
			ResponseEntity<ActionDto[]> entity = initRestTemplate().getForEntity(url, ActionDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<AttachmentDto> returnAttachments(String url) {
		try {
			ResponseEntity<AttachmentDto[]> entity = initRestTemplate().getForEntity(url, AttachmentDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private AttachmentContentDto returnAttachmentContent(String url) {
		try {
			ResponseEntity<byte[]> entity = initRestTemplate().getForEntity(url, byte[].class);
			AttachmentContentDto attachmentContentDto = new AttachmentContentDto();
			attachmentContentDto.setContent(entity.getBody());
			return attachmentContentDto;
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private AttachmentDto postAttachment(String url, File file, String fileName, String type, String description, Boolean mobile) {
		HttpHeaders headers = new HttpHeaders();
//
//		if (fileName.contains(".pdf")){
//			headers.setContentType(MediaType.APPLICATION_PDF);
//		} else {
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//		}

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add(param_file, new FileSystemResource(file));
		body.add(param_type, type);
		body.add(param_description, description);
		body.add(param_mobile, mobile);
		body.add(param_source, "");
		body.add(param_name, fileName);
		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(body, headers);
		try {
			ResponseEntity<AttachmentDto> entity = initRestTemplate().postForEntity(url, httpEntity, AttachmentDto.class);
			getLogger().debug(entity.getBody().debugInfo());
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private BrigadeDto returnBrigade(String url) {
		try {
			ResponseEntity<BrigadeDto> entity = initRestTemplate().getForEntity(url, BrigadeDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<BrigadeDto> returnBrigades(String url) {
		try {
			ResponseEntity<BrigadeDto[]> entity = initRestTemplate().getForEntity(url, BrigadeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private PagesDto<BrigadeJournalDto> returnBrigadeJournalPage(String url) {
		try {
			ParameterizedTypeReference<PagesDto<BrigadeJournalDto>> reference = new ParameterizedTypeReference<PagesDto<BrigadeJournalDto>>() {};
			ResponseEntity<PagesDto<BrigadeJournalDto>> entity = initRestTemplate().exchange(url, HttpMethod.GET, null, reference);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			PagesDto<BrigadeJournalDto> pagesDto = new PagesDto<>();
			pagesDto.setCurrentContent(new ArrayList<>());
			pagesDto.setCount(0L);
			return pagesDto;
		}
	}

	private BrigadeDto postBrigade(String url, BrigadeDto brigadeDto) {
		try {
			ResponseEntity<BrigadeDto> entity = initRestTemplate().postForEntity(url, brigadeDto, BrigadeDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private void putWorkBrigade(String url, BrigadeDto brigadeDto) {
		try {
			initRestTemplate().put(url, brigadeDto);
		} catch (RestClientException e) {
			logExceptions(url, e);
		}
	}

	private BrigadeDto putBrigade(String url, BrigadeDto brigadeDto) {
		try {
			HttpEntity<BrigadeDto> httpEntity = new HttpEntity<>(brigadeDto);
			ResponseEntity<BrigadeDto> entity = initRestTemplate().exchange(url, HttpMethod.PUT, httpEntity, BrigadeDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<EmployeeBrigadeDto> returnBrigadeEmployees(String url) {
		try {
			//getLogger().debug(returnDebugString(url));
			ResponseEntity<EmployeeBrigadeDto[]> entity = initRestTemplate().getForEntity(url, EmployeeBrigadeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<BrigadePlanDto> returnBrigadePlans(String url) {
		try {
			//getLogger().debug(returnDebugString(url));
			ResponseEntity<BrigadePlanDto[]> entity = initRestTemplate().getForEntity(url, BrigadePlanDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private BrigadePlanDto postBrigadePlan(String url, BrigadePlanDto planDto) {
		try {
			//getLogger().debug(returnDebugString(url));
			ResponseEntity<BrigadePlanDto> entity = initRestTemplate().postForEntity(url, planDto, BrigadePlanDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private CommentDto returnComment(String url) {
		try {
			ResponseEntity<CommentDto> entity = initRestTemplate().getForEntity(url, CommentDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<CommentDto> returnComments(String url) {
		try {
			ResponseEntity<CommentDto[]> entity = initRestTemplate().getForEntity(url, CommentDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}
	private List<ElementDto> returnElements(String url) {
		try {
			ResponseEntity<ElementDto[]> entity = initRestTemplate().getForEntity(url, ElementDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private CommentDto postComment(String url, CommentRequestDto brigadeDto) {
		try {
			ResponseEntity<CommentDto> entity = initRestTemplate().postForEntity(url, brigadeDto, CommentDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private EmployeeDto returnEmployee(String url) {
		try {
			ResponseEntity<EmployeeDto> entity = initRestTemplate().getForEntity(url, EmployeeDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<EmployeeDto> returnEmployees(String url) {
		try {
			ResponseEntity<EmployeeDto[]> entity = initRestTemplate().getForEntity(url, EmployeeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<EquipmentDto> returnEquipments(String url) {
		try {
			ResponseEntity<EquipmentDto[]> entity = initRestTemplate().getForEntity(url, EquipmentDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private EventDto returnEvent(String url) {
		try {
			ResponseEntity<EventDto> entity = initRestTemplate().getForEntity(url, EventDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<EventDto> returnEvents(String url) {
		try {
			ResponseEntity<EventDto[]> entity = initRestTemplate().getForEntity(url, EventDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private PagesDto<EventWithActionDto> returnEventsWithActions(String url) {
		try {
			ParameterizedTypeReference<PagesDto<EventWithActionDto>> reference = new ParameterizedTypeReference<PagesDto<EventWithActionDto>>() {};
			ResponseEntity<PagesDto<EventWithActionDto>> entity = initRestTemplate().exchange(url, HttpMethod.GET, null, reference);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			PagesDto<EventWithActionDto> pagesDto = new PagesDto<>();
			pagesDto.setCurrentContent(new ArrayList<>());
			pagesDto.setCount(0L);
			return pagesDto;
		}
	}

	private List<GeocodeDto> returnGeocodes(String url) {
		try {
			ResponseEntity<GeocodeDto[]> entity = initRestTemplate().getForEntity(url, GeocodeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<InventoryDto> returnInventory(String url) {
		try {
			ResponseEntity<InventoryDto[]> entity = initRestTemplate().getForEntity(url, InventoryDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<MechanizationDto> returnMechanization(String url) {
		try {
			ResponseEntity<MechanizationDto[]> entity = initRestTemplate().getForEntity(url, MechanizationDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private OrganizationDto returnOrganization(String url) {
		try {
			ResponseEntity<OrganizationDto> entity = initRestTemplate().getForEntity(url, OrganizationDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<OrganizationDto> returnOrganizations(String url) {
		try {
			ResponseEntity<OrganizationDto[]> entity = initRestTemplate().getForEntity(url, OrganizationDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private PositionDto returnPosition(String url) {
		try {
			ResponseEntity<PositionDto> entity = initRestTemplate().getForEntity(url, PositionDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<PositionDto> returnPositions(String url) {
		try {
			ResponseEntity<PositionDto[]> entity = initRestTemplate().getForEntity(url, PositionDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private RequestDto postRequest(String url, RequestDto requestDto) {
		try {
			ResponseEntity<RequestDto> entity = initRestTemplate().postForEntity(url, requestDto, RequestDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private void postRequests(String url, List<RequestDto> requestDtoList) {
		try {
			initRestTemplate().postForEntity(url, requestDtoList, RequestDto.class);
		} catch (RestClientException e) {
			logExceptions(url, e);
		}
	}

	private RequestDto putRequest(String url, RequestDto requestDto) {
		try {
			HttpEntity<RequestDto> httpEntity = new HttpEntity<>(requestDto);
			ResponseEntity<RequestDto> entity = initRestTemplate().exchange(url, HttpMethod.PUT, httpEntity, RequestDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private RequestDto returnRequest(String url) {
		try {
			ResponseEntity<RequestDto> entity = initRestTemplate().getForEntity(url, RequestDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<RequestDto> returnRequests(String url) {
		try {
			ResponseEntity<RequestDto[]> entity = initRestTemplate().getForEntity(url, RequestDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private PagesDto<RequestJournalDto> returnRequestJournalPage(String url) {
		try {
			ParameterizedTypeReference<PagesDto<RequestJournalDto>> reference = new ParameterizedTypeReference<PagesDto<RequestJournalDto>>() {};
			ResponseEntity<PagesDto<RequestJournalDto>> entity = initRestTemplate().exchange(url, HttpMethod.GET, null, reference);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			PagesDto<RequestJournalDto> pagesDto = new PagesDto<>();
			pagesDto.setCurrentContent(new ArrayList<>());
			pagesDto.setCount(0L);
			return pagesDto;
		}
	}

	private List<SourceDto> returnSources(String url) {
		try {
			ResponseEntity<SourceDto[]> entity = initRestTemplate().getForEntity(url, SourceDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<StatusDto> returnStatuses(String url) {
		try {
			ResponseEntity<StatusDto[]> entity = initRestTemplate().getForEntity(url, StatusDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private List<UrgencyDto> returnUrgencies(String url) {
		try {
			ResponseEntity<UrgencyDto[]> entity = initRestTemplate().getForEntity(url, UrgencyDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private UserDto returnUser(String url) {
		try {
			ResponseEntity<UserDto> entity = initRestTemplate().getForEntity(url, UserDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<UserDto> returnUsers(String url) {
		try {
			ResponseEntity<UserDto[]> entity = initRestTemplate().getForEntity(url, UserDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private VehicleDto returnVehicle(String url) {
		try {
			ResponseEntity<VehicleDto> entity = initRestTemplate().getForEntity(url, VehicleDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<VehicleDto> returnVehicles(String url) {
		try {
			ResponseEntity<VehicleDto[]> entity = initRestTemplate().getForEntity(url, VehicleDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private WorkDto postWork(String url, WorkDto workDto) {
		try {
			ResponseEntity<WorkDto> entity = initRestTemplate().postForEntity(url, workDto, WorkDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private WorkDto putWork(String url, WorkDto workDto) {
		try {
			HttpEntity<WorkDto> httpEntity = new HttpEntity<>(workDto);
			ResponseEntity<WorkDto> entity = initRestTemplate().exchange(url, HttpMethod.PUT, httpEntity, WorkDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}
	/**
	 * Сохранение этапа типа работы (обновление существующей)
	 * @param typeWorkItemDto Этап типа работы
	 * @param typeWorkDtoId id типа работы
	 * @return сохраненная этап работы
	 */
	public TypeWorkItemDto[] updateTypeWorkItem(TypeWorkItemDto typeWorkItemDto, long typeWorkDtoId) {
		if (typeWorkItemDto.getId() == null) {
			return null;
		} else {
			String url = types_works+"/"+typeWorkDtoId+"/"+"items";
			return putTypeWorkItem(url, typeWorkItemDto);
		}
	}
	//обновление этапа типа работы на сервере
	private TypeWorkItemDto[] putTypeWorkItem(String url, TypeWorkItemDto typeWorkItemDto) {
		try {
			getLogger().debug(url);
			TypeWorkItemDto[] arr = new TypeWorkItemDto[]{typeWorkItemDto};
			HttpEntity<TypeWorkItemDto[]> httpEntity = new HttpEntity<>(arr);
			ResponseEntity<TypeWorkItemDto[]> entity = initRestTemplate().exchange(url, HttpMethod.PUT, httpEntity, TypeWorkItemDto[].class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}
	/**
	 * Сохранение типа работы (обновление существующего)
	 * @param typeWorkDto тип работы
	 * @return сохраненная этап работы
	 */
	public TypeWorkDto[] updateTypeWork(TypeWorkDto typeWorkDto) {
		if (typeWorkDto.getId() == null) {
			return null;
		} else {
			String url = types_works;
			return putTypeWork(url, typeWorkDto);
		}
	}
	//обновление этапа типа работы на сервере
	private TypeWorkDto[] putTypeWork(String url, TypeWorkDto typeWorkDto) {
		try {
			getLogger().debug(url);
			TypeWorkDto[] arr = new TypeWorkDto[]{typeWorkDto};
			HttpEntity<TypeWorkDto[]> httpEntity = new HttpEntity<>(arr);
			ResponseEntity<TypeWorkDto[]> entity = initRestTemplate().exchange(url, HttpMethod.PUT, httpEntity, TypeWorkDto[].class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private WorkDto returnWork(String url) {
		try {
			ResponseEntity<WorkDto> entity = initRestTemplate().getForEntity(url, WorkDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<WorkDto> returnWorks(String url) {
		try {
			//getLogger().debug(returnDebugString(url));
			ResponseEntity<WorkDto[]> entity = initRestTemplate().getForEntity(url, WorkDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	private PagesDto<WorkJournalDto> returnWorkJournalPage(String url) {
		try {
			ParameterizedTypeReference<PagesDto<WorkJournalDto>> reference = new ParameterizedTypeReference<PagesDto<WorkJournalDto>>() {};
			ResponseEntity<PagesDto<WorkJournalDto>> entity = initRestTemplate().exchange(url, HttpMethod.GET, null, reference);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			PagesDto<WorkJournalDto> pagesDto = new PagesDto<>();
			pagesDto.setCurrentContent(new ArrayList<>());
			pagesDto.setCount(0L);
			return pagesDto;
		}
	}

	private WorkItemDto postWorkItem(String url, WorkItemDto workItemDto) {
		try {
			ResponseEntity<WorkItemDto> entity = initRestTemplate().postForEntity(url, workItemDto, WorkItemDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private void postWorkItems(String url, List<WorkItemDto> workItems) {
		try {
			/*ResponseEntity<WorkItemDto[]> entity = */initRestTemplate().postForEntity(url, workItems, WorkItemDto[].class);
			//return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			//return Collections.emptyList();
		}
	}

	private void putWorkItem(String url, WorkItemDto workItemDto) {
		try {
			initRestTemplate().put(url, workItemDto);
		} catch (RestClientException e) {
			logExceptions(url, e);
		}
	}

	private void putWorkItems(String url, List<WorkItemDto> workItems) {
		try {
			initRestTemplate().put(url, workItems);
		} catch (RestClientException e) {
			logExceptions(url, e);
		}
	}

	private WorkItemDto returnWorkItem(String url) {
		try {
			ResponseEntity<WorkItemDto> entity = initRestTemplate().getForEntity(url, WorkItemDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	private List<WorkItemDto> returnWorkItems(String url) {
		try {
			//getLogger().debug(returnDebugString(url));
			ResponseEntity<WorkItemDto[]> entity = initRestTemplate().getForEntity(url, WorkItemDto[].class);
			List<WorkItemDto> items = Arrays.asList(entity.getBody());
			items.sort(Comparator.comparing(WorkItemDto::getOrder));
			return items;
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	/* PUBLIC METHODS */

	// + + @GetMapping({"ping"})

	public PingDto ping() {
		try {
			ResponseEntity<PingDto> entity = initRestTemplate().getForEntity(PING_PATH, PingDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(PING_PATH, e);
			return null;
		}
	}

	// + + @GetMapping({"actions/{id}"})
	// + + @GetMapping(value = {"actions"}, params = {"from", "to", "skip", "limit"})
	// + + @GetMapping(value = {"actions"}, params = {"type", "state", "from", "to", "skip", "limit"})
	// + + @GetMapping(value = {"actions"}, params = {"objectGuid", "from", "to", "skip", "limit"})
	// + + @PostMapping({"actions"})
	// + - @PutMapping(value = {"actions/{id}"}, params = {"state"}

	/**
	 * Квитирование события
	 * @param eventDto событие
	 */
	public void acceptEvent(EventWithActionDto eventDto) {
		ActionDto actionDto = new ActionDto();
		//actionDto.setActionDate(LocalDateTime.now());
		actionDto.setUser(BrigadesUI.getCurrentUser());
		actionDto.setObjectGuid(eventDto.getObjectGuid());
		actionDto.setEventGuid(eventDto.getGuid());
		actionDto.setState("ACCEPTED");
		actionDto.setMessage(eventDto.getMessage());
		actionDto.setType(eventDto.getType());
		ActionDto action = postAction(actions, actionDto);
		if (action != null) getLogger().debug(action.debugInfo());
	}

	/**
	 * Получение всех действий по событиям за выбранный период
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список действий
	 */
	public List<ActionDto> getActions(ZonedDateTime start, ZonedDateTime end) {
		MultiValueMap<String, String> map = getMap(start, end);
		String url = UriComponentsBuilder.fromUriString(actions).queryParams(map).build().toUriString();
		return returnActions(url);
	}

	/**
	 * Получение всех действий по указанному событию за выбранный период
	 * @param start начальная дата
	 * @param end конечная дата
	 * @param guid guid события
	 * @return список действий
	 */
	public List<ActionDto> getActions(ZonedDateTime start, ZonedDateTime end, String guid) {
		//"Parameter conditions \"objectGuid, from, to, skip, limit\" OR \"type, state, from, to, skip, limit\" not met for actual request parameters: "
		MultiValueMap<String, String> map = getMap(start, end);
		map.add(param_objectGuid, guid);
		String url = UriComponentsBuilder.fromUriString(actions).queryParams(map).build().toUriString();
		return returnActions(url);
	}

	/**
	 * Получение всех действий по типу и состоянию за выбранный период
	 * @param start начальная дата
	 * @param end конечная дата
	 * @param type тип действий
	 * @param state состояние действий
	 * @return список действий
	 */
	public List<ActionDto> getActions(ZonedDateTime start, ZonedDateTime end, String type, String state) {
		MultiValueMap<String, String> map = getMap(start, end);
		map.add(param_type, type);
		map.add(param_state, state);
		String url = UriComponentsBuilder.fromUriString(actions).queryParams(map).build().toUriString();
		return returnActions(url);
	}

	/**
	 * Получение всех действий по указанному событию за период из настроек
	 * @param guid guid события
	 * @return список действий
	 */
	public List<ActionDto> getActions(String guid) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getActions(dateInterval.getStart(), dateInterval.getEnd(), guid);
	}

	/**
	 * Получение действия по ID
	 * @param id id действия
	 * @return действие
	 */
	public ActionDto getAction(Long id) {
		String url = String.format(action_id, id);
		return returnAction(url);
	}

	// + - @GetMapping({"assets/{id}"})
	// + - @GetMapping(value = {"assets"}, params = {"skip", "limit"})
	// + - @PostMapping({"assets"})
	// + - @PutMapping({"assets/{id}"})
	// + - @DeleteMapping({"assets/{id}"})
	// + - @GetMapping({"assets/types"})
	// + - @GetMapping({"assets/types/{idType}/attributes"})
	// + - @GetMapping({"assets/{id}/attributes/{idAttribute}"})
	// + - @PostMapping({"assets/{id}/attributes"})
	// + - @PutMapping({"assets/{id}/attributes"})
	// + - @DeleteMapping({"assets/{id}/attributes/{idAttribute}"}))


	// + + @PutMapping(value = {"attachments/{id-attachment}"}, params = {"commentary", "gati", "regulations", "calendar"})
	// + + @GetMapping({"attachments/types"})

	/**
	 * Обновление данных по вложению
	 * @param attachmentDto вложение для обновления
	 * @return обновленное вложение
	 */
	public AttachmentDto updateAttachment(AttachmentDto attachmentDto) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_commentary, attachmentDto.getCommentary());
		map.add(param_gati, attachmentDto.getGati());
		map.add(param_regulations, attachmentDto.getRegulations());
		map.add(param_calendar, attachmentDto.getCalendar());
		map.add(param_state, attachmentDto.getState());
		String url = String.format(attachment_id, attachmentDto.getId());
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		try {
			HttpEntity<AttachmentDto> httpEntity = new HttpEntity<>(attachmentDto);
			ResponseEntity<AttachmentDto> entity = initRestTemplate().exchange(url, HttpMethod.PUT, httpEntity, AttachmentDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	/**
	 * Получение типов вложений
	 * @return список типов вложений
	 */
	public List<AttachmentTypeDto> getAttachmentTypes() {
		try {
			ResponseEntity<AttachmentTypeDto[]> entity = initRestTemplate().getForEntity(attachment_types, AttachmentTypeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(attachment_types, e);
			return Collections.emptyList();
		}
	}

	// + + @GetMapping(value = {"brigades"}, params = {"skip", "limit"})
	// + + @GetMapping(value = {"brigades"}, params = {"login"})
	// + + @GetMapping({"brigades/{id}"})
	// + + @PostMapping({"brigades"})
	// + + @PutMapping({"brigades/{id}"})
	// + + @GetMapping({"brigades/{id}/employees"})
	// + - @PostMapping({"brigades/{id}/employees"})
	// + - @PutMapping({"brigades/{id}/employees"})
	// - - @DeleteMapping({"brigades/{id}/employees"})
	// + + @GetMapping({"brigades/{id}/vehicles"})
	// + - @PostMapping({"brigades/{id}/vehicles"})
	// + - @DeleteMapping({"brigades/{id}/vehicles/{id-vehicle}"})
	// + + @GetMapping(value = {"brigades/{id}/works"}, params = {"from", "to", "skip", "limit"})
	// + - @GetMapping(value = {"brigades/{id}/works"}, params = {"from", "to", "skip", "limit", "groupBy"})
	// + + @GetMapping(value = {"brigades"}, params = {"on", "skip", "limit"})
	// + + @GetMapping(value = {"brigades/{id}/plans"}, params = {"from", "to", "skip", "limit"})
	// + + @PostMapping({"brigades/{id}/plans"})
	// + - @GetMapping({"brigades/{id}/plans/{id-plan}/employees"})
	// + - @PostMapping({"brigades/{id}/plans/{id-plan}/employees"})

	/**
	 * Получение бригад
	 * @return список бригад
	 */
	public List<BrigadeDto> getBrigades() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(brigades).queryParams(map).build().toUriString();
		return returnBrigades(url);
	}

	/**
	 * Получение бригад для табличных данных
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список бригад и их общее количество
	 */
	public PagesDto<BrigadeJournalDto> getBrigadesForJournal(Integer skip, Integer limit) {
		MultiValueMap<String, String> map = getMap(skip, limit);
		map.add(param_on, DatesUtil.formatToUTC(ZonedDateTime.now()));
		String url = UriComponentsBuilder.fromUriString(brigades).queryParams(map).build().toUriString();
		return returnBrigadeJournalPage(url);
	}

	/**
	 * Получение общего количества бригад
	 * @return общее количество бригад
	 */
	public Long countBrigades() {
		PagesDto<BrigadeJournalDto> journal = getBrigadesForJournal(0, 1);
		return journal.getCount();
	}

	/**
	 * Получение бригады по id
	 * @param id id бригады
	 * @return бригада
	 */
	public BrigadeDto getBrigade(Long id) {
		String url = String.format(brigade_id, id);
		return returnBrigade(url);
	}

	/**
	 * Получение списка бригад для пользователя
	 * @param login логин пользователя
	 * @return список бригад
	 */
	public List<BrigadeDto> getUserBrigades(String login) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_login, login);
		String url = UriComponentsBuilder.fromUriString(brigades).queryParams(map).build().toUriString();
		return returnBrigades(url);
	}

	/**
	 * Сохранение бригады
	 * @param brigadeDto бригада
	 * @return сохраненная бригада в формате @BrigadeDto
	 */
	public BrigadeDto saveBrigade(BrigadeDto brigadeDto) {
		if (brigadeDto.getId() == null) {
			return postBrigade(brigades, brigadeDto);
		} else {
			String url = String.format(brigade_id, brigadeDto.getId());
			return putBrigade(url, brigadeDto);
		}
	}

	/**
	 * Удаление бригады
	 * @param id id бригады
	 */
	public void deleteBrigade(Long id) {
		String url = String.format(brigade_id, id);
		deleteEntity(url);
	}

	/**
	 * Получение работников бригады
	 * @param id id бригады
	 * @return список работников
	 */
	public List<EmployeeBrigadeDto> getBrigadeEmployees(Long id) {
		String url = String.format(brigade_employees, id);
		return returnBrigadeEmployees(url);
	}

	/**
	 * Получение работников бригады из плана
	 * @param brigadeId id бригады
	 * @param planId id плана
	 * @return список работников
	 */
	public List<EmployeeBrigadeDto> getBrigadeEmployeesFromPlan(Long brigadeId, Long planId) {
		String url = String.format(brigade_plan_employees, brigadeId, planId);
		return returnBrigadeEmployees(url);
	}

	/**
	 * Получение мастера бригады
	 * @param id id бригады
	 * @return мастер бригады
	 */
	public EmployeeBrigadeDto getBrigadeMaster(Long id) {
		List<EmployeeBrigadeDto> brigadeEmployees = getBrigadeEmployees(id);
		EmployeeBrigadeDto manager = null;
		for (EmployeeBrigadeDto employee : brigadeEmployees) {
			if (employee.getManager() != null && employee.getManager())
				manager = employee;
		}
		return manager;
	}

	/**
	 * Получение планов бригады
	 * @param id id бригады
	 * @return список планов
	 */
	public List<BrigadePlanDto> getBrigadePlans(Long id) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		MultiValueMap<String, String> map = getMap(dateInterval.getStart(), dateInterval.getEnd());
		String url = UriComponentsBuilder.fromUriString(String.format(brigade_plans, id)).queryParams(map).build().toUriString();
		return returnBrigadePlans(url);
	}
	public List<BrigadePlanDto> getBrigadePlans(Long id, int daysOffset) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		MultiValueMap<String, String> map = getMap(dateInterval.getStart().minusDays(daysOffset), dateInterval.getEnd().minusDays(daysOffset));
		String url = UriComponentsBuilder.fromUriString(String.format(brigade_plans, id)).queryParams(map).build().toUriString();
		return returnBrigadePlans(url);
	}

	/**
	 * Сохранение плана бригады
	 * @param id id бригады
	 * @param planDto план
	 * @return сохраненный план
	 */
	public BrigadePlanDto saveBrigadePlan(Long id, BrigadePlanDto planDto) {
		String url = String.format(brigade_plans, id);
		return postBrigadePlan(url, planDto);
	}

	/**
	 * Получение транспорта бригады
	 * @param id id бригады
	 * @return список транспорта бригады
	 */
	public List<VehicleBrigadeDto> getBrigadeVehicles(Long id) {
		String url = String.format(brigade_vehicles, id);
		try {
			ResponseEntity<VehicleBrigadeDto[]> entity = initRestTemplate().getForEntity(url, VehicleBrigadeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}
	public List<WorkDto> getWorksWithBrigadeAndOrganization(
			ZonedDateTime from,
			ZonedDateTime to,
			Long brigadeId,
			String organization){
		MultiValueMap map = getMap(0,1000);
		map.add("fromStartDatePlan", DatesUtil.formatToUTC(from));
		map.add("toStartDatePlan", DatesUtil.formatToUTC(to));
		map.add("idBrigade",brigadeId);
		//todo-k поправить, как только организации перестанут быть null
//		map.add("organization",organization);
		String url = UriComponentsBuilder.fromUriString(works).queryParams(map).build().toUriString();
		getLogger().debug(url);
		ResponseEntity<WorkDto[]> entity = initRestTemplate().getForEntity(url,WorkDto[].class);
		Arrays.asList(entity.getBody()).stream().forEach(e-> getLogger().debug(e.debugInfo()));
		return Arrays.asList(entity.getBody());

	}
	/**
	 * Получение работ бригады за период из настроек
	 * @param id id бригады
	 * @return список работ бригады
	 */
	public List<WorkDto> getBrigadeWorks(Long id) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getBrigadeWorks(id, dateInterval.getStart(), dateInterval.getEnd());
	}
	public List<WorkDto> getBrigadeWorks(Long id, int daysOffset) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();

		return getBrigadeWorks(id, dateInterval.getStart().minusDays(daysOffset), dateInterval.getEnd().minusDays(daysOffset));
	}

	/**
	 * Получение работ бригады за указанный период
	 * @param id id бригады
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список работ бригады
	 */
	public List<WorkDto> getBrigadeWorks(Long id, ZonedDateTime start, ZonedDateTime end) {
		MultiValueMap<String, String> map = getMap(start, end);
		String url = UriComponentsBuilder.fromUriString(String.format(brigade_works, id)).queryParams(map).build().toUriString();
		return returnWorks(url);
	}

	// + - @GetMapping({"brigades-plans/{id}/vehicles"})
	// + - @PostMapping({"brigades-plans/{id}/vehicles"})
	// + - @DeleteMapping({"brigades-plans/{id}/vehicles"})
	// + - @GetMapping({"brigades-plans/{id}/equipments"})
	// + - @PostMapping({"brigades-plans/{id}/equipments"})
	// + - @DeleteMapping({"brigades-plans/{id}/equipments"})
	// + - @GetMapping({"brigades-plans/{id}/mechanization"})
	// + - @PostMapping({"brigades-plans/{id}/mechanization"})
	// + - @DeleteMapping({"brigades-plans/{id}/mechanization"})
	// + - @GetMapping({"brigades-plans/{id}/inventory"})
	// + - @PostMapping({"brigades-plans/{id}/inventory"})
	// + - @DeleteMapping({"brigades-plans/{id}/inventory"})

	public List<EquipmentBrigadePlanDto> getBrigadeEquipments(Long id) {
		String url = String.format(brigadePlan_equipments, id);
		try {
			ResponseEntity<EquipmentBrigadePlanDto[]> entity = initRestTemplate().getForEntity(url, EquipmentBrigadePlanDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	public List<InventoryBrigadePlanDto> getBrigadeInventory(Long id) {
		String url = String.format(brigadePlan_inventory, id);
		try {
			ResponseEntity<InventoryBrigadePlanDto[]> entity = initRestTemplate().getForEntity(url, InventoryBrigadePlanDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	public List<MechanizationBrigadePlanDto> getBrigadeMechanization(Long id) {
		String url = String.format(brigadePlan_mechanization, id);
		try {
			ResponseEntity<MechanizationBrigadePlanDto[]> entity = initRestTemplate().getForEntity(url, MechanizationBrigadePlanDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// + - @GetMapping({"cameras/{id}"})

	public List<CameraDto> getCameras() {
		String url = MAVR_SERVER + "/dto/camera/activated";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Client-IP-Header", UI.getCurrent().getPage().getWebBrowser().getAddress());
		headers.add(HttpHeaders.AUTHORIZATION, MAVR_TOKEN);
		HttpEntity<String> httpEntity = new HttpEntity<>(headers);
		try {
			//todo Вернуть
//			return Collections.emptyList();
			ResponseEntity<CameraDto[]> entity = new RestTemplate().exchange(url, HttpMethod.GET, httpEntity, CameraDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	public CameraAddressDto getCameraAddress(Long addressId) {
		String url = MAVR_SERVER + "/dto/address/" + addressId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Client-IP-Header", UI.getCurrent().getPage().getWebBrowser().getAddress());
		headers.add(HttpHeaders.AUTHORIZATION, MAVR_TOKEN);
		HttpEntity<String> httpEntity = new HttpEntity<>(headers);
		try {
			ResponseEntity<CameraAddressDto> entity = new RestTemplate().exchange(url, HttpMethod.GET, httpEntity, CameraAddressDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	public String getCameraVideo(Long cameraId, CameraTokenDto tokenDto) {
		String url = MAVR_SERVER + "/dto/camera/" + cameraId + "/video";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Client-IP-Header", UI.getCurrent().getPage().getWebBrowser().getAddress());
		headers.add(HttpHeaders.AUTHORIZATION, MAVR_TOKEN);
		HttpEntity<CameraTokenDto> httpEntity = new HttpEntity<>(tokenDto, headers);
		try {
			ResponseEntity<String> entity = new RestTemplate().exchange(url, HttpMethod.POST, httpEntity, String.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	// + - @GetMapping(value = {"employees"}, params = {"skip", "limit"})
	// + - @GetMapping({"employees/{id}"})
	// + - @PostMapping({"employees"})
	// + - @PutMapping({"employees/{id}"})
	// + - @DeleteMapping({"employees/{id}"})
	// + - @GetMapping(value = {"employees/absence-reasons"}, params = {"skip", "limit"})
	// + - @GetMapping(value = {"employees/posts"}, params = {"skip", "limit"})

	/**
	 * Получение сотрудника по id
	 * @param id id сотрудника
	 * @return сотрудник
	 */
	public EmployeeDto getEmployee(Long id) {
		String url = String.format(employee_id, id);
		return returnEmployee(url);
	}

	/**
	 * Получение сотрудников по стандартным параметрам
	 * @return список сотрудников
	 */
	public List<EmployeeDto> getEmployees() {
		return getEmployees(custom_skip, custom_limit);
	}

	/**
	 * Получение сотрудников по заданным параметрам
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список сотрудников
	 */
	public List<EmployeeDto> getEmployees(Integer skip, Integer limit) {
		getLogger().debug("getEmployees({}, {})", skip, limit);
		MultiValueMap<String, String> map = getMap(skip, limit);
		String url = UriComponentsBuilder.fromUriString(employees).queryParams(map).build().toUriString();
		return returnEmployees(url);
	}

	/**
	 * Получение всех сотрудников
	 * @return список сотрудников
	 */
	public List<EmployeeDto> getAllEmployees() {
		List<EmployeeDto> result = new ArrayList<>();
		List<EmployeeDto> list = new ArrayList<>();
		int counter = 0;
		do {
			list.clear();
			Integer skip = custom_limit * counter;
			//getLogger().debug("getAllEmployees({}, {})", skip, custom_limit);
			list.addAll(getEmployees(skip, custom_limit));
			result.addAll(list);
			counter++;
			//getLogger().debug("getAllEmployees size: {}", list.size());
		} while (list.size() == custom_limit);
		result.sort(Comparator.comparing(EmployeeDto::getId));
		return result;
	}

	/**
	 * Получение количества всех сотрудников
	 * @return число всех сотрудников
	 */
	public Integer countEmployees() {
		int size = getAllEmployees().size();
		getLogger().debug("getAllEmployees size: {}", size);
		return size;
	}

	/**
	 * Удаление сотрудника
	 * @param id id сотрудника
	 */
	public void deleteEmployee(Long id) {
		String url = String.format(employee_id, id);
		deleteEntity(url);
	}

	// + - @GetMapping(value = {"equipments"}, params = {"type", "model", "number", "skip", "limit"})
	// + - @GetMapping(value = {"equipments/types"}, params = {"skip", "limit"})
	// + - @GetMapping(value = {"equipments/models"}, params = {"type", "skip", "limit"})
	// + - @GetMapping(value = {"equipments/numbers"}, params = {"type", "model", "skip", "limit"})

	/**
	 * Получение спецтехники по стандартным параметрам
	 * @return список спецтехники
	 */
	public List<EquipmentDto> getEquipments() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(equipments).queryParams(map).build().toUriString();
		return returnEquipments(url);
	}

	/**
	 * Получение моделей спецтехники по стандартным параметрам
	 * @return список моделей спецтехники
	 */
	public List<EquipmentModelDto> getEquipmentModels() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(equipment_models).queryParams(map).build().toUriString();
		try {
			ResponseEntity<EquipmentModelDto[]> entity = initRestTemplate().getForEntity(url, EquipmentModelDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение номеров спецтехники по стандартным параметрам
	 * @return список номеров спецтехники
	 */
	public List<EquipmentNumberDto> getEquipmentNumbers() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(equipment_numbers).queryParams(map).build().toUriString();
		try {
			ResponseEntity<EquipmentNumberDto[]> entity = initRestTemplate().getForEntity(url, EquipmentNumberDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение типов спецтехники по стандартным параметрам
	 * @return список типов спецтехники
	 */
	public List<EquipmentTypeDto> getEquipmentTypes() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(equipment_types).queryParams(map).build().toUriString();
		try {
			ResponseEntity<EquipmentTypeDto[]> entity = initRestTemplate().getForEntity(url, EquipmentTypeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// + - @GetMapping(value = {"events/{objectGuid}"}, params = {"from", "to", "skip", "limit"})
	// + - @GetMapping(value = {"events"}, params = {"type", "from", "to", "skip", "limit"})
	// + - @GetMapping(value = {"events"}, params = {"from", "to", "skip", "limit"})
	// + - @GetMapping({"events/types"})
	// + - @GetMapping(value = {"events/actions"}, params = {"from", "to", "skip", "limit"})

	/**
	 * Получение событий за указанный период
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список событий
	 */
	public List<EventDto> getEvents(ZonedDateTime start, ZonedDateTime end) {
		//"Parameter conditions \"from, to, skip, limit\" OR \"type, from, to, skip, limit\" not met for actual request parameters: "
		MultiValueMap<String, String> map = getMap(start, end);
		String url = UriComponentsBuilder.fromUriString(events).queryParams(map).build().toUriString();
		return returnEvents(url);
	}

	/**
	 * Получение событий за период из настроек
	 * @return список событий
	 */
	public List<EventDto> getEvents() {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getEvents(dateInterval.getStart(), dateInterval.getEnd());
	}

	/**
	 * Получение событий с действиями за указанные параметры
	 * @param start начальная дата
	 * @param end конечная дата
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список событий с действиями
	 */
	public PagesDto<EventWithActionDto> getEventsWithActions(ZonedDateTime start, ZonedDateTime end, Integer skip, Integer limit) {
		//"Parameter conditions \"from, to, skip, limit\" OR \"type, from, to, skip, limit\" not met for actual request parameters: "
		MultiValueMap<String, String> map = getMap(start, end, skip, limit);
		String url = UriComponentsBuilder.fromUriString(events_actions).queryParams(map).build().toUriString();
		return returnEventsWithActions(url);
	}

	/**
	 * Получение событий с действиями за указанные параметры и период из настроек
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список событий с действиями
	 */
	public PagesDto<EventWithActionDto> getEventsWithActions(Integer skip, Integer limit) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getEventsWithActions(dateInterval.getStart(), dateInterval.getEnd(), skip, limit);
		//return getEventsWithActions(DatesUtil.setStartDateTime(), DatesUtil.setTomorrowNullTime());
	}

	/**
	 * Получение событий с действиями за указанный период
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список событий с действиями
	 */
	public PagesDto<EventWithActionDto> getEventsWithActions(ZonedDateTime start, ZonedDateTime end) {
		//"Parameter conditions \"from, to, skip, limit\" OR \"type, from, to, skip, limit\" not met for actual request parameters: "
		MultiValueMap<String, String> map = getMap(start, end);
		String url = UriComponentsBuilder.fromUriString(events_actions).queryParams(map).build().toUriString();

		return returnEventsWithActions(url);
	}

	/**
	 * Получение событий с действиями за период из настроек
	 * @return список событий с действиями
	 */
	public PagesDto<EventWithActionDto> getEventsWithActions() {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getEventsWithActions(dateInterval.getStart(), dateInterval.getEnd());
		//return getEventsWithActions(DatesUtil.setStartDateTime(), DatesUtil.setTomorrowNullTime());
	}

	/**
	 * Получение общего количества событий
	 * @return общее количество событий
	 */
	public Long countEvents() {
		PagesDto<EventWithActionDto> journal = getEventsWithActions(0, 1);
		return journal.getCount();
	}

	// + + @GetMapping(value = {"gantt/brigades"}, params = {"on"})

	/**
	 * Получение данных для диаграммы Гантта
	 * @return данные для диаграммы Гантта
	 */
	public List<GanttItemDto> getGanttBrigades() {
		MultiValueMap<String, String> map = createMap();
		map.add(param_on, DatesUtil.formatToUTC(ZonedDateTime.now()));
		String url = UriComponentsBuilder.fromUriString(gantt_brigades).queryParams(map).build().toUriString();
		try {
			ResponseEntity<GanttItemDto[]> entity = initRestTemplate().getForEntity(url, GanttItemDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	public List<GanttItemDto> getDemoGanttBrigades() {

		try {
			StringBuilder json = new StringBuilder();
			FileReader fileReader = new FileReader(new File("files/gantt_20191209.json"));
			GanttItemDto[] ganttItemArray = objectMapper.readValue(fileReader, GanttItemDto[].class);
			return Arrays.asList(ganttItemArray);
		} catch (IOException e) {
			getLogger().error(e.getMessage());
			return Collections.emptyList();
		}
	}

	//+ -  @GetMapping(value = {"geocodes"}, params = {"longitude", "latitude"})
	// + - @GetMapping(value = {"geocodes"}, params = {"address"})
	// + - @GetMapping(value = {"geocodes/district"}, params = {"longitude", "latitude"})

	/**
	 * Получение списка адресов по строковому запросу
	 * @param address строковый запрос
	 * @return список адресов
	 */
	public List<GeocodeDto> getGeocodeAddress(String address) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_address, address);
		String url = UriComponentsBuilder.fromUriString(geocodes).queryParams(map).build().toUriString();
		return returnGeocodes(url);
	}

	/**
	 * Получение списка адресов по указанным координатам
	 * @param longitude долгота
	 * @param latitude широта
	 * @return список адресов
	 */
	public List<GeocodeDto> getGeocodesByCoordinates(Double longitude, Double latitude) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_latitude, latitude.toString());
		map.add(param_longitude, longitude.toString());
		String url = UriComponentsBuilder.fromUriString(geocodes).queryParams(map).build().toUriString();
		return returnGeocodes(url);
	}
	/**
	 * Получение списка адресов по указанным координатам
	 * @param address адрес
	 * @return координаты
	 */
	public List<GeocodeDto> getCoordinatesByAddress(String address) {
		MultiValueMap<String, String> map = createMap();
		map.add("address", address);
		String url = UriComponentsBuilder.fromUriString(geocodes).queryParams(map).build().toUriString();
		return returnGeocodes(url);
	}
	/**
	 * Получение района города по указанным координатам
	 * @param longitude долгота
	 * @param latitude широта
	 * @return район города
	 */
	public String getDistrictByCoordinates(Double longitude, Double latitude) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_latitude, latitude.toString());
		map.add(param_longitude, longitude.toString());
		String url = UriComponentsBuilder.fromUriString(geocode_district).queryParams(map).build().toUriString();
		return returnString(url);
	}

	// + - @GetMapping(value = {"inventory"}, params = {"type", "model", "number", "skip", "limit"})
	// + - @GetMapping(value = {"inventory/types"}, params = {"skip", "limit"})
	// + - @GetMapping(value = {"inventory/models"}, params = {"type", "skip", "limit"})
	// + - @GetMapping(value = {"inventory/numbers"}, params = {"type", "model", "skip", "limit"})
	// + - @GetMapping(value = {"inventory/units"}, params = {"skip", "limit"})

	/**
	 * Получение инвентаря по стандартным параметрам
	 * @return спсиок инвентаря
	 */
	public List<InventoryDto> getInventory() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(inventory).queryParams(map).build().toUriString();
		return returnInventory(url);
	}

	/**
	 * Получение моделей инвентаря по стандартным параметрам
	 * @return спсиок моделей инвентаря
	 */
	public List<InventoryDto> getInventoryModels() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(inventory_models).queryParams(map).build().toUriString();
		return returnInventory(url);
	}

	/**
	 * Получение номеров инвентаря по стандартным параметрам
	 * @return спсиок номеров инвентаря
	 */
	public List<InventoryDto> getInventoryNumbers() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(inventory_numbers).queryParams(map).build().toUriString();
		return returnInventory(url);
	}

	/**
	 * Получение типов инвентаря по стандартным параметрам
	 * @return спсиок типов инвентаря
	 */
	public List<InventoryDto> getInventoryTypes() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(inventory_types).queryParams(map).build().toUriString();
		return returnInventory(url);
	}

	/**
	 * Получение единиц измерения инвентаря по стандартным параметрам
	 * @return спсиок единиц измерения инвентаря
	 */
	public List<InventoryDto> getInventoryUnits() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(inventory_units).queryParams(map).build().toUriString();
		return returnInventory(url);
	}

	// + - @GetMapping(value = {"locations"}, params = {"skip", "limit"})

	public List<LocationDto> getLocations() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(locations).queryParams(map).build().toUriString();
		try {
			ResponseEntity<LocationDto[]> entity = initRestTemplate().getForEntity(url, LocationDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// + + @GetMapping(value = {"maps/entities"}, params = {"on"})

	public EntityMapCollectionDto getMapEntities() {
		MultiValueMap<String, String> map = createMap();
		map.add(param_on, DatesUtil.formatToUTC(ZonedDateTime.now()));
		String url = UriComponentsBuilder.fromUriString(map_entities).queryParams(map).build().toUriString();
		try {
			ResponseEntity<EntityMapCollectionDto> entity = initRestTemplate().getForEntity(url, EntityMapCollectionDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return new EntityMapCollectionDto();
		}
	}

	public EntityMapCollectionDto getDemoMapEntities() {
		String json = "{\n" +
				"    \"works\": [\n" +
				"        {\n" +
				"            \"id\": 45,\n" +
				"            \"type\": \"Обследование заявки\",\n" +
				"            \"description\": \"Обследование заявки\",\n" +
				"            \"address\": \" Кингисеппское шоссе; 53\",\n" +
				"            \"brigade\": \"УОР 6_Сутов А.Н.\",\n" +
				"            \"startDatePlan\": \"11.09.2019 14:01:00\",\n" +
				"            \"finishDatePlan\": \"11.09.2019 14:31:00\",\n" +
				"            \"startDateFact\": null,\n" +
				"            \"finishDateFact\": \"14.09.2019 20:00:43\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.060019,\n" +
				"                \"latitude\": 59.720876,\n" +
				"                \"description\": \"Красное Село, Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Кингисеппское шоссе, 53\"\n" +
				"            },\n" +
				"            \"status\": \"Закрыта в РВ\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 2,\n" +
				"            \"type\": \"Отбор проб воды на сетях\",\n" +
				"            \"description\": \"Проверка на утечку отдельного участка трубопровода\",\n" +
				"            \"address\": \"Оборонная 22\",\n" +
				"            \"brigade\": \"УОР 2_Канаштаров С.Л.\",\n" +
				"            \"startDatePlan\": \"03.09.2019 07:30:00\",\n" +
				"            \"finishDatePlan\": \"03.09.2019 09:15:00\",\n" +
				"            \"startDateFact\": null,\n" +
				"            \"finishDateFact\": \"14.09.2019 20:03:35\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.259499,\n" +
				"                \"latitude\": 59.893051,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Оборонная улица, 22\"\n" +
				"            },\n" +
				"            \"status\": \"Закрыта в РВ\"\n" +
				"        }\n" +
				"    ],\n" +
				"    \"requests\": [\n" +
				"        {\n" +
				"            \"id\": 5171,\n" +
				"            \"problemCode\": \"1\",\n" +
				"            \"problemName\": \"Отсутствие Х/В\",\n" +
				"            \"address\": \" Рощинская улица; 3\",\n" +
				"            \"description\": \"По информации заявителя Р=0.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.310074,\n" +
				"                \"latitude\": 59.885164,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Рощинская улица, 3\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5180,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" Сергиево; Спортивная улица; 18\",\n" +
				"            \"description\": \"открыт колодец, крышка вдавлена внутрь\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.081219,\n" +
				"                \"latitude\": 59.820648,\n" +
				"                \"description\": \"территория Сергиево, Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Спортивная улица, 18\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5169,\n" +
				"            \"problemCode\": \"21\",\n" +
				"            \"problemName\": \"Жалобы по работам бригад\",\n" +
				"            \"address\": \" Ленинский проспект; 118 к. 1\",\n" +
				"            \"description\": \"Со слов заявителя- заявитель возмущён, что бригада, когда протягивала рукав временного водоснабжения к ним в подвал не выключила свет, не предупредила, что уезжает,по хамски себя вела и не закрыла за собой дверь\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.249959,\n" +
				"                \"latitude\": 59.852484,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 118к1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5168,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" Ленинский проспект; 118 к. 2\",\n" +
				"            \"description\": \"Со слов заявителя: сильное вытекание из кодлодца.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.253049,\n" +
				"                \"latitude\": 59.853076,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 118к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5179,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" г.Санкт-Петербург;Воздухоплавательная улица; 13\",\n" +
				"            \"description\": \"Крышка  расколота  лежит  рядом . \",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34094,\n" +
				"                \"latitude\": 59.898765,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Воздухоплавательная улица, 13\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5178,\n" +
				"            \"problemCode\": \"36\",\n" +
				"            \"problemName\": \"Аварийный ремонт арматуры\",\n" +
				"            \"address\": \" улица Кондратенко; 2\",\n" +
				"            \"description\": null,\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.343375,\n" +
				"                \"latitude\": 59.885941,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Кондратенко, 2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5177,\n" +
				"            \"problemCode\": \"1\",\n" +
				"            \"problemName\": \"Отсутствие Х/В\",\n" +
				"            \"address\": \" Петергофское шоссе; 74 к. 6\",\n" +
				"            \"description\": \"Офисное здание жалуется на отсутствие Х/В.Договор с Водоканалам есть.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.121661,\n" +
				"                \"latitude\": 59.841559,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Петергофское шоссе, 74к6\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5176,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" Ленинский проспект; 96 к. 2\",\n" +
				"            \"description\": \"Вытекание х/в, предположительно ПГ\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.216514,\n" +
				"                \"latitude\": 59.85557,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 96к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5175,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" Ленинский проспект; 96 к. 2\",\n" +
				"            \"description\": \"накренился колодец\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.216514,\n" +
				"                \"latitude\": 59.85557,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 96к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5174,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" Ленинский проспект; 96 к. 2\",\n" +
				"            \"description\": \"Расколота крышка\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.216514,\n" +
				"                \"latitude\": 59.85557,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 96к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5173,\n" +
				"            \"problemCode\": \"1\",\n" +
				"            \"problemName\": \"Отсутствие Х/В\",\n" +
				"            \"address\": \" Ленинский проспект; 118\",\n" +
				"            \"description\": \"После аварии на Ленинском 118 19.09.2019. Отсутствует Х/В втечение суток.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.253507,\n" +
				"                \"latitude\": 59.852339,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 118\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5172,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" улица Зины Портновой; 17 к. 5\",\n" +
				"            \"description\": \"течь газон телефон заявителя Мильничук 272-05-66\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.251657,\n" +
				"                \"latitude\": 59.853532,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Зины Портновой, 17к5\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5162,\n" +
				"            \"problemCode\": \"42\",\n" +
				"            \"problemName\": \"Дефект комплекта на газоне\",\n" +
				"            \"address\": \" Ленинский проспект; 98 к. 1\",\n" +
				"            \"description\": \"Заявитель: крышка не закреплена,не устойчива.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.217574,\n" +
				"                \"latitude\": 59.853636,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 98к1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5167,\n" +
				"            \"problemCode\": \"29\",\n" +
				"            \"problemName\": \"Необходим ремонт водомерного узла (дома ГУЖА)\",\n" +
				"            \"address\": \" проспект Ветеранов; 114 к. 1\",\n" +
				"            \"description\": \"Со слов заявителя тел.: 8-921-369-93-09, не работает первая задвижка (не держит, счётчик крутит, опломбпровка на месте) на резервном вводе по 6-й парадной.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.198404,\n" +
				"                \"latitude\": 59.832543,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Ветеранов, 114к1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5166,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" Ленинский проспект; 118\",\n" +
				"            \"description\": \"Заявительсильное :вытекание из-под АБП в 4 местах,АБП выперло,растекается по м.проезду двора.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.253507,\n" +
				"                \"latitude\": 59.852339,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 118\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5165,\n" +
				"            \"problemCode\": \"13\",\n" +
				"            \"problemName\": \"Вытекание воды на газоне\",\n" +
				"            \"address\": \" Московское шоссе; 44 к. 7\",\n" +
				"            \"description\": \"Со слов заявителя тел.: 8-905-222-57-76, сильное вытекание, разливается по проезду.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.355107,\n" +
				"                \"latitude\": 59.830287,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Московское шоссе, 44к7\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5164,\n" +
				"            \"problemCode\": \"22\",\n" +
				"            \"problemName\": \"Подтапливание подвальных помещений\",\n" +
				"            \"address\": \" Ленинский проспект; 95\",\n" +
				"            \"description\": \"Со слов заявителя (собственник): сильная течь х/в на вводе. Тел. заявителя: 8-918-642-15-65.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.213963,\n" +
				"                \"latitude\": 59.85304,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 95к1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5163,\n" +
				"            \"problemCode\": \"13\",\n" +
				"            \"problemName\": \"Вытекание воды на газоне\",\n" +
				"            \"address\": \" набережная реки Екатерингофки; 19\",\n" +
				"            \"description\": \"Со слов заявителя: течь хвс из кол-ца на газоне. Тел. заявителя 8-921-329-80-71\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.254414,\n" +
				"                \"latitude\": 59.9064,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"набережная реки Екатерингофки, 19\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5170,\n" +
				"            \"problemCode\": \"8\",\n" +
				"            \"problemName\": \"Просадка на проезжей части\",\n" +
				"            \"address\": \" проспект Юрия Гагарина; 1\",\n" +
				"            \"description\": \"На пешеходном переходе просадка АБП у ковера ВС.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.33591,\n" +
				"                \"latitude\": 59.874693,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Юрия Гагарина, 1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5161,\n" +
				"            \"problemCode\": \"13\",\n" +
				"            \"problemName\": \"Вытекание воды на газоне\",\n" +
				"            \"address\": \" проспект Стачек; 98\",\n" +
				"            \"description\": \"Со слов заявителя;Вытекание холод/воды из под земли.Моб тел 8-911-295-23-05\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.262329,\n" +
				"                \"latitude\": 59.863427,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Стачек, 98А\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5160,\n" +
				"            \"problemCode\": \"20\",\n" +
				"            \"problemName\": \"Не благоустроено место после проведения работ\",\n" +
				"            \"address\": \" проспект Ветеранов; 58\",\n" +
				"            \"description\": \"со слов заявителя- разрыт АБП, валяется щебень, препятствует проезду автомобилей.Заявитель  нашла на сайте ордеров ГАТИ онлайн информацию, что с 27.04.19 по 26.06.19 проводилась прокладка водопровода по пр.Ветеранов от ул.Козлова до у.Танкиста Хрустицкого силами ГУП \\\"ВодоканалСанкт-Петербурга\\\".\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.234544,\n" +
				"                \"latitude\": 59.837286,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Ветеранов, 58\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5153,\n" +
				"            \"problemCode\": \"22\",\n" +
				"            \"problemName\": \"Подтапливание подвальных помещений\",\n" +
				"            \"address\": \" проспект Народного Ополчения; 173 к. 2\",\n" +
				"            \"description\": \"Со слов заявителя. Вода в подвале.Источник неизвестен.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.218913,\n" +
				"                \"latitude\": 59.826144,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Народного Ополчения, 173к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5159,\n" +
				"            \"problemCode\": \"22\",\n" +
				"            \"problemName\": \"Подтапливание подвальных помещений\",\n" +
				"            \"address\": \" проспект Ветеранов; 140\",\n" +
				"            \"description\": \"Сильная течь х/воды на в/узле, очень просят побыстрее\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.169551,\n" +
				"                \"latitude\": 59.832471,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Ветеранов, 140\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5158,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" Канонерский; 8\",\n" +
				"            \"description\": \"Течь из установленного гидранта на колодце. Жалобы от жителей через ГМЦ.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.213676,\n" +
				"                \"latitude\": 59.898327,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Канонерский остров, 8\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5157,\n" +
				"            \"problemCode\": null,\n" +
				"            \"problemName\": null,\n" +
				"            \"address\": \" проспект Стачек; 39\",\n" +
				"            \"description\": \"Нету крышки люка !\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.268114,\n" +
				"                \"latitude\": 59.887368,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Стачек, 39\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5156,\n" +
				"            \"problemCode\": \"9\",\n" +
				"            \"problemName\": \"Просадка колодца на проезжей части\",\n" +
				"            \"address\": \" проспект Юрия Гагарина; 8\",\n" +
				"            \"description\": \"яма у люка\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34165,\n" +
				"                \"latitude\": 59.869115,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Юрия Гагарина, 8\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5155,\n" +
				"            \"problemCode\": \"3\",\n" +
				"            \"problemName\": \"Плохое качество воды\",\n" +
				"            \"address\": \" проспект Юрия Гагарина; 25\",\n" +
				"            \"description\": \"Со слов заявителя;Холод/вода рыжая.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.334858,\n" +
				"                \"latitude\": 59.860111,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Юрия Гагарина, 25\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5154,\n" +
				"            \"problemCode\": \"3\",\n" +
				"            \"problemName\": \"Плохое качество воды\",\n" +
				"            \"address\": \" проспект Стачек; 142\",\n" +
				"            \"description\": \"х/в очень ржавая\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.247093,\n" +
				"                \"latitude\": 59.855145,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Стачек, 142\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5152,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" Ленинский проспект; 92 к. 3\",\n" +
				"            \"description\": \"Со слов заявителя: место вытекания ограждено.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.215059,\n" +
				"                \"latitude\": 59.856921,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 92к3\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4960,\n" +
				"            \"problemCode\": \"20\",\n" +
				"            \"problemName\": \"Не благоустроено место после проведения работ\",\n" +
				"            \"address\": \" улица Доблести; 26 к. 2\",\n" +
				"            \"description\": \"По информации заявителя после работ ВК не восстановлена газонная ограда,мало привезли плодородной земли\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.179854,\n" +
				"                \"latitude\": 59.85369,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Доблести, 26к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4898,\n" +
				"            \"problemCode\": \"8\",\n" +
				"            \"problemName\": \"Просадка на проезжей части\",\n" +
				"            \"address\": \" проспект Ветеранов; 38\",\n" +
				"            \"description\": \"со слов заявителя- просадка АБП у колодца, огр. нет\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.244605,\n" +
				"                \"latitude\": 59.841676,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Ветеранов, 38\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4717,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" улица Бурцева; 14\",\n" +
				"            \"description\": \"течь пр/ч ( источник информации  Neva TODAY)\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.208564,\n" +
				"                \"latitude\": 59.837246,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Бурцева, 14\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4698,\n" +
				"            \"problemCode\": \"8\",\n" +
				"            \"problemName\": \"Просадка на проезжей части\",\n" +
				"            \"address\": \" Московский проспект; 65\",\n" +
				"            \"description\": \"Рядом со старой проблемой ещё один провал образовывается, прошу принять меры.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.317835,\n" +
				"                \"latitude\": 59.907284,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Московский проспект, 65\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4865,\n" +
				"            \"problemCode\": \"3\",\n" +
				"            \"problemName\": \"Плохое качество воды\",\n" +
				"            \"address\": \" ПОГРАНИЧНИКА ГАРЬКАВОГО; 1\",\n" +
				"            \"description\": \"массовые жалобы на ржавую х/в \",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.146554,\n" +
				"                \"latitude\": 59.844334,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Пограничника Гарькавого, 1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4864,\n" +
				"            \"problemCode\": \"3\",\n" +
				"            \"problemName\": \"Плохое качество воды\",\n" +
				"            \"address\": \" улица Чекистов; 42\",\n" +
				"            \"description\": \"массовые жалобы на ржавую х/в \",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.145224,\n" +
				"                \"latitude\": 59.844646,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Чекистов, 42\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4798,\n" +
				"            \"problemCode\": \"2\",\n" +
				"            \"problemName\": \"Слабый напор х/в\",\n" +
				"            \"address\": \" проспект Ветеранов; 166\",\n" +
				"            \"description\": \"письмо прикреплено ( т. дисперчера 458-72-86)\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.141703,\n" +
				"                \"latitude\": 59.834931,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Ветеранов, 166\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4780,\n" +
				"            \"problemCode\": \"9\",\n" +
				"            \"problemName\": \"Просадка колодца на проезжей части\",\n" +
				"            \"address\": \" улица Доблести; 7 к. 2\",\n" +
				"            \"description\": \"Сильно просел люк (на всех фото отмечен графически).\\nТребуется произвести работы по регулировке уровня люка с последующим качественным ремонтом/восстановлением асфальтового покрытия в зоне работ.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.179127,\n" +
				"                \"latitude\": 59.863509,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Доблести, 7к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4796,\n" +
				"            \"problemCode\": \"2\",\n" +
				"            \"problemName\": \"Слабый напор х/в\",\n" +
				"            \"address\": \" проспект Ветеранов; 160 к. 1\",\n" +
				"            \"description\": \"АКТ прикреплен. (458-72-54)\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.144236,\n" +
				"                \"latitude\": 59.834587,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Ветеранов, 160к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4781,\n" +
				"            \"problemCode\": \"18\",\n" +
				"            \"problemName\": \"Дефект комплекта на проезжей части\",\n" +
				"            \"address\": \" улица Доблести; 7 к. 2\",\n" +
				"            \"description\": \"Крышка люка значительно выше уровня покрытия.\\nПроезд в тёмное время суток не освещается (нет фонарей).\\nОб такой выступ запросто можно споткнуться и упасть.\\nТребуется произвести работы по регулировке уровня люка (при необходимости - заменить комплект люка на новый).\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.179127,\n" +
				"                \"latitude\": 59.863509,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Доблести, 7к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4773,\n" +
				"            \"problemCode\": \"9\",\n" +
				"            \"problemName\": \"Просадка колодца на проезжей части\",\n" +
				"            \"address\": \" улица Доблести; 7 к. 2\",\n" +
				"            \"description\": \"Просел люк, вокруг него сильно повреждено дорожное покрытие (на всех фото люк отмечен графически).\\nТребуется произвести работы по регулировке уровня люка с последующим качественным восстановлением асфальтового покрытия в зоне работ.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.179127,\n" +
				"                \"latitude\": 59.863509,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Доблести, 7к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4730,\n" +
				"            \"problemCode\": \"9\",\n" +
				"            \"problemName\": \"Просадка колодца на проезжей части\",\n" +
				"            \"address\": \" Ленинский проспект; 129\",\n" +
				"            \"description\": \"Со слов заявителя тел.: 8-911-114-24-65, просадка водопроводного ковера с абп.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.270045,\n" +
				"                \"latitude\": 59.851376,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 129\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4487,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" улица Тамбасова; 4 к. 2\",\n" +
				"            \"description\": \"Заявитель: вытекание ХВ .\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.162175,\n" +
				"                \"latitude\": 59.84164,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Тамбасова, 4к2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4274,\n" +
				"            \"problemCode\": \"13\",\n" +
				"            \"problemName\": \"Вытекание воды на газоне\",\n" +
				"            \"address\": \" Петергофское шоссе; 73\",\n" +
				"            \"description\": \"Обращение в ГМЦ поступило от водителя тел.: 8-911-164-35-12.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.124635,\n" +
				"                \"latitude\": 59.856135,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Петергофское шоссе, 73\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4182,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" Сергиево; Трудовая улица; 7\",\n" +
				"            \"description\": \"Сильное вытекание Х/В на пр.части.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.108151,\n" +
				"                \"latitude\": 59.828102,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Трудовая улица, 7\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3869,\n" +
				"            \"problemCode\": \"18\",\n" +
				"            \"problemName\": \"Дефект комплекта на проезжей части\",\n" +
				"            \"address\": \" улица Пограничника Гарькавого; 6 к. 1\",\n" +
				"            \"description\": \"Со слов заявителя Гремит крышка\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.147703,\n" +
				"                \"latitude\": 59.842915,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Пограничника Гарькавого, 6к1\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4007,\n" +
				"            \"problemCode\": \"18\",\n" +
				"            \"problemName\": \"Дефект комплекта на проезжей части\",\n" +
				"            \"address\": \" улица Чекистов; 42\",\n" +
				"            \"description\": \"Гремит крышка люка. \",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.145224,\n" +
				"                \"latitude\": 59.844646,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Чекистов, 42\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3948,\n" +
				"            \"problemCode\": \"4\",\n" +
				"            \"problemName\": \"Не работает уличная в/колонка\",\n" +
				"            \"address\": \" Сергиево; улица Дзержинского; 2\",\n" +
				"            \"description\": \"Заявитель: не работает ВРК.Дог.есть.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.077141,\n" +
				"                \"latitude\": 59.833389,\n" +
				"                \"description\": \"территория Сергиево, Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Дзержинского, 4\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3934,\n" +
				"            \"problemCode\": \"2\",\n" +
				"            \"problemName\": \"Слабый напор х/в\",\n" +
				"            \"address\": \" улица Подводника Кузьмина; 17\",\n" +
				"            \"description\": \"По акту: давление без разбора3,2кг/см3,  с разбором 3,1. Имеются заявления на Портал и 004.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.262724,\n" +
				"                \"latitude\": 59.844126,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Подводника Кузьмина, 17\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3759,\n" +
				"            \"problemCode\": \"9\",\n" +
				"            \"problemName\": \"Просадка колодца на проезжей части\",\n" +
				"            \"address\": \" набережная Обводного канала; 96\",\n" +
				"            \"description\": \"Просадка асфальта\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.320216,\n" +
				"                \"latitude\": 59.908218,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"набережная Обводного канала, 96\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3760,\n" +
				"            \"problemCode\": \"9\",\n" +
				"            \"problemName\": \"Просадка колодца на проезжей части\",\n" +
				"            \"address\": \" набережная Обводного канала; 96\",\n" +
				"            \"description\": \"Просадка асфальта\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.320216,\n" +
				"                \"latitude\": 59.908218,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"набережная Обводного канала, 96\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3589,\n" +
				"            \"problemCode\": \"2\",\n" +
				"            \"problemName\": \"Слабый напор х/в\",\n" +
				"            \"address\": \" улица Емельянова; 12\",\n" +
				"            \"description\": \"Письмо с замерами давления в ГЛ.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.337688,\n" +
				"                \"latitude\": 59.896779,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Емельянова, 12\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3292,\n" +
				"            \"problemCode\": \"8\",\n" +
				"            \"problemName\": \"Просадка на проезжей части\",\n" +
				"            \"address\": \" Гапсальская улица; 2\",\n" +
				"            \"description\": \"Просадка асфальта у к/кол., крайний, правый ряд.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.259759,\n" +
				"                \"latitude\": 59.912409,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Гапсальская улица, 2\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 2970,\n" +
				"            \"problemCode\": \"1\",\n" +
				"            \"problemName\": \"Отсутствие Х/В\",\n" +
				"            \"address\": \" улица Косинова; 5\",\n" +
				"            \"description\": \"По информации заявителя нет ХВС во всем доме Р=0\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.271069,\n" +
				"                \"latitude\": 59.898056,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"улица Косинова, 5\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 2719,\n" +
				"            \"problemCode\": \"6\",\n" +
				"            \"problemName\": \"Вытекание воды на проезжей части\",\n" +
				"            \"address\": \" проспект Маршала Жукова; 45\",\n" +
				"            \"description\": \"Фонтанчик 10 см. Сообщено в СЦ.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.21523,\n" +
				"                \"latitude\": 59.850034,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Маршала Жукова, 45\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 1477,\n" +
				"            \"problemCode\": \"13\",\n" +
				"            \"problemName\": \"Вытекание воды на газоне\",\n" +
				"            \"address\": \" проспект Юрия Гагарина; 31\",\n" +
				"            \"description\": \"Между осн. пр. частью и местным проездом из-под земли.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.33546,\n" +
				"                \"latitude\": 59.854635,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"проспект Юрия Гагарина, 31\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 1196,\n" +
				"            \"problemCode\": \"13\",\n" +
				"            \"problemName\": \"Вытекание воды на газоне\",\n" +
				"            \"address\": \" Рыбинская улица; 9\",\n" +
				"            \"description\": \"Со слов заявителя: вытекает из-под земли,на территории электростанции, там проходят трубы ВК.\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.334796,\n" +
				"                \"latitude\": 59.910275,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Рыбинская улица, 7\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 1203,\n" +
				"            \"problemCode\": \"33\",\n" +
				"            \"problemName\": \"Работа с ДЗ\",\n" +
				"            \"address\": \" Заставская улица; 22\",\n" +
				"            \"description\": \"Служебная записка от 21.08.2019  № 2247/300 Мероприятие - Возобновить\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.326253,\n" +
				"                \"latitude\": 59.8886,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Заставская улица, 22А\"\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 938,\n" +
				"            \"problemCode\": \"10\",\n" +
				"            \"problemName\": \"Отсутствие крышки колодца на проезжей части\",\n" +
				"            \"address\": \" Кронштадтская улица; 6\",\n" +
				"            \"description\": \"на пр/ ч открыт к/ц, ограждение выставлено\",\n" +
				"            \"status\": \"Новая\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.254855,\n" +
				"                \"latitude\": 59.875849,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Кронштадтская улица, 6\"\n" +
				"            }\n" +
				"        }\n" +
				"    ],\n" +
				"    \"brigades\": [\n" +
				"        {\n" +
				"            \"id\": 1,\n" +
				"            \"name\": \"УОР 1_Каюров С.В.\",\n" +
				"            \"address\": \"Ленинский проспект, 118к2\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.253049,\n" +
				"                \"latitude\": 59.853076,\n" +
				"                \"description\": \"Санкт-Петербург, Россия\",\n" +
				"                \"name\": \"Ленинский проспект, 118к2\"\n" +
				"            },\n" +
				"            \"startDatePlanWork\": \"20.09.2019 16:30:00\",\n" +
				"            \"finishDatePlanWork\": \"20.09.2019 17:30:00\",\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 2,\n" +
				"            \"name\": \"УОР 2_Канаштаров С.Л.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3,\n" +
				"            \"name\": \"УОР 3_Николаев С.Л.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4,\n" +
				"            \"name\": \"УОР 4_Таммине П.Э.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5,\n" +
				"            \"name\": \"УОР 5_Усов Р.Б.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 6,\n" +
				"            \"name\": \"УОР 6_Сутов А.Н.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 7,\n" +
				"            \"name\": \"УОР 7_Мозгалев А.С.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 8,\n" +
				"            \"name\": \"УОР 8_Михайлов М.В.\",\n" +
				"            \"address\": null,\n" +
				"            \"geocode\": null,\n" +
				"            \"startDatePlanWork\": null,\n" +
				"            \"finishDatePlanWork\": null,\n" +
				"            \"startDateFactWork\": null,\n" +
				"            \"finishDateFactWork\": null\n" +
				"        }\n" +
				"    ],\n" +
				"    \"vehicles\": [\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в014ае178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:09:59\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.354713439941406,\n" +
				"                \"latitude\": 59.830162048339844,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в596ау178\",\n" +
				"            \"state\": \"движение \",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:50:42\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.11258316040039,\n" +
				"                \"latitude\": 59.81423568725586,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в015ае178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 14:06:39\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.348989486694336,\n" +
				"                \"latitude\": 59.844261169433594,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в213ас178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 12:39:56\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.349044799804688,\n" +
				"                \"latitude\": 59.844261169433594,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в235ас178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:14:02\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34919548034668,\n" +
				"                \"latitude\": 59.84415054321289,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в468ем178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 12:54:10\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.427154541015625,\n" +
				"                \"latitude\": 59.84456253051758,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в006ае178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 12:02:20\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34819793701172,\n" +
				"                \"latitude\": 59.844425201416016,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"А/фург\",\n" +
				"            \"model\": \"КамАЗ-4308\",\n" +
				"            \"number\": \"в931ао178\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:52:41\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34952163696289,\n" +
				"                \"latitude\": 59.844642639160156,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Фургон\",\n" +
				"            \"model\": \"Hino\",\n" +
				"            \"number\": \"а823ра198\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 14:09:31\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.349740982055664,\n" +
				"                \"latitude\": 59.844398498535156,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Фургон\",\n" +
				"            \"model\": \"ГАЗ-2705\",\n" +
				"            \"number\": \"в524хв98\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 12:03:39\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34934425354004,\n" +
				"                \"latitude\": 59.844451904296875,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Фургон\",\n" +
				"            \"model\": \"ГАЗ-2705\",\n" +
				"            \"number\": \"в536хв98\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 12:46:45\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.34818458557129,\n" +
				"                \"latitude\": 59.8427848815918,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB JS160LC\",\n" +
				"            \"number\": \"1540рк78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:50:40\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.351612091064453,\n" +
				"                \"latitude\": 59.93966293334961,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"ЭО-4112А1\",\n" +
				"            \"number\": \"3611рс78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:19:34\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.61387825012207,\n" +
				"                \"latitude\": 59.73369216918945,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB 4CX\",\n" +
				"            \"number\": \"3600рс78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:26:55\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.373537063598633,\n" +
				"                \"latitude\": 60.045860290527344,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB JS160NLC\",\n" +
				"            \"number\": \"4060рс78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:12:15\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.388385772705078,\n" +
				"                \"latitude\": 59.953651428222656,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB JS160W\",\n" +
				"            \"number\": \"4642ру78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:57:45\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.229612350463867,\n" +
				"                \"latitude\": 59.99755859375,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB 3CX SUPER\",\n" +
				"            \"number\": \"3614рс78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 14:04:38\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.512845993041992,\n" +
				"                \"latitude\": 59.92832565307617,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"TLB 825-RM\",\n" +
				"            \"number\": \"3539рк78\",\n" +
				"            \"state\": \"движение \",\n" +
				"            \"lastUpdated\": \"20.09.2019 14:10:15\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.382566452026367,\n" +
				"                \"latitude\": 59.959197998046875,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB 4CX\",\n" +
				"            \"number\": \"3421рк78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:54:06\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.120315551757812,\n" +
				"                \"latitude\": 60.00285720825195,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"JCB 1CX\",\n" +
				"            \"number\": \"2021рс78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 13:14:28\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.42970848083496,\n" +
				"                \"latitude\": 59.84404373168945,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        },\n" +
				"        {\n" +
				"            \"type\": \"Экскаватор\",\n" +
				"            \"model\": \"TLB 825-RM\",\n" +
				"            \"number\": \"3547рк78\",\n" +
				"            \"state\": \"остановка\",\n" +
				"            \"lastUpdated\": \"20.09.2019 14:06:16\",\n" +
				"            \"geocode\": {\n" +
				"                \"longitude\": 30.41057014465332,\n" +
				"                \"latitude\": 59.89084243774414,\n" +
				"                \"description\": null,\n" +
				"                \"name\": null\n" +
				"            }\n" +
				"        }\n" +
				"    ]\n" +
				"}";
		try {
			return objectMapper.readValue(json, EntityMapCollectionDto.class);
		} catch (IOException e) {
			getLogger().error(e.getMessage());
			return new EntityMapCollectionDto();
		}
	}

	public List<MapRegionDto> getMapRegion() {
		List<MapRegionDto> result = new ArrayList<>();
		/*
		result.add(new MapRegionDto(59.785339,30.294513));
		result.add(new MapRegionDto(59.797809,30.213232));
		result.add(new MapRegionDto(59.813203,30.223017));
		result.add(new MapRegionDto(59.835848,30.268679));
		result.add(new MapRegionDto(59.826883,30.230827));
		result.add(new MapRegionDto(59.823729,30.204906));
		result.add(new MapRegionDto(59.823988,30.176270));
		result.add(new MapRegionDto(59.825371,30.140316));
		result.add(new MapRegionDto(59.827024,30.126639));
		result.add(new MapRegionDto(59.829140,30.124046));
		result.add(new MapRegionDto(59.837518,30.123166));
		result.add(new MapRegionDto(59.841687,30.124850));
		result.add(new MapRegionDto(59.843129,30.124056));
		result.add(new MapRegionDto(59.844669,30.122303));
		result.add(new MapRegionDto(59.845812,30.122030));
		result.add(new MapRegionDto(59.847264,30.123028));
		result.add(new MapRegionDto(59.848851,30.124916));
		result.add(new MapRegionDto(59.849883,30.125532));
		result.add(new MapRegionDto(59.859898,30.124687));
		result.add(new MapRegionDto(59.861524,30.132927));
		result.add(new MapRegionDto(59.864988,30.147225));
		result.add(new MapRegionDto(59.871966,30.155445));
		result.add(new MapRegionDto(59.886565,30.167460));
		result.add(new MapRegionDto(59.872120,30.206273));
		result.add(new MapRegionDto(59.884115,30.225843));
		result.add(new MapRegionDto(59.887480,30.239919));
		result.add(new MapRegionDto(59.893570,30.242924));
		result.add(new MapRegionDto(59.892971,30.246595));
		result.add(new MapRegionDto(59.891116,30.249728));
		result.add(new MapRegionDto(59.887882,30.252861));
		result.add(new MapRegionDto(59.886380,30.257718));
		result.add(new MapRegionDto(59.884601,30.265945));
		result.add(new MapRegionDto(59.881704,30.278738));
		result.add(new MapRegionDto(59.882350,30.293551));
		result.add(new MapRegionDto(59.882976,30.319858));
		result.add(new MapRegionDto(59.883408,30.325249));
		result.add(new MapRegionDto(59.880000,30.347097));
		result.add(new MapRegionDto(59.880000,30.347097));
		result.add(new MapRegionDto(59.879507,30.352676));
		result.add(new MapRegionDto(59.874799,30.356093));
		result.add(new MapRegionDto(59.866021,30.359427));
		result.add(new MapRegionDto(59.854365,30.365178));
		result.add(new MapRegionDto(59.846577,30.369395));
		result.add(new MapRegionDto(59.841467,30.371369));
		result.add(new MapRegionDto(59.834296,30.374845));
		result.add(new MapRegionDto(59.822045,30.380616));
		result.add(new MapRegionDto(59.818861,30.381737));
		result.add(new MapRegionDto(59.817024,30.370708));
		result.add(new MapRegionDto(59.815382,30.363127));
		result.add(new MapRegionDto(59.810922,30.344715));
		result.add(new MapRegionDto(59.808955,30.341754));
		result.add(new MapRegionDto(59.808133,30.337935));
		result.add(new MapRegionDto(59.805993,30.334116));
		result.add(new MapRegionDto(59.803618,30.324949));
		result.add(new MapRegionDto(59.797696,30.324553));
		result.add(new MapRegionDto(59.797644,30.324541));
		result.add(new MapRegionDto(59.794325,30.327910));
		result.add(new MapRegionDto(59.790772,30.325464));
		result.add(new MapRegionDto(59.778044,30.325544));
		result.add(new MapRegionDto(59.777723,30.319276));
		result.add(new MapRegionDto(59.785339,30.294513));
		*/
		result.add(new MapRegionDto(59.838958, 30.067789));
		result.add(new MapRegionDto(59.838948, 30.067759));
		result.add(new MapRegionDto(59.838548, 30.06781));
		result.add(new MapRegionDto(59.829465, 30.070096));
		result.add(new MapRegionDto(59.826038, 30.070107));
		result.add(new MapRegionDto(59.825052, 30.072487));
		result.add(new MapRegionDto(59.819874, 30.073763));
		result.add(new MapRegionDto(59.818693, 30.072222));
		result.add(new MapRegionDto(59.815638, 30.073145));
		result.add(new MapRegionDto(59.815234, 30.074808));
		result.add(new MapRegionDto(59.81577, 30.080358));
		result.add(new MapRegionDto(59.816249, 30.085398));
		result.add(new MapRegionDto(59.814963, 30.09033));
		result.add(new MapRegionDto(59.814926, 30.090336));
		result.add(new MapRegionDto(59.814858, 30.090731));
		result.add(new MapRegionDto(59.814809, 30.090919));
		result.add(new MapRegionDto(59.814844, 30.091155));
		result.add(new MapRegionDto(59.817164, 30.104387));
		result.add(new MapRegionDto(59.808984, 30.127694));
		result.add(new MapRegionDto(59.806944, 30.125765));
		result.add(new MapRegionDto(59.805482, 30.124374));
		result.add(new MapRegionDto(59.804472, 30.123393));
		result.add(new MapRegionDto(59.801428, 30.120407));
		result.add(new MapRegionDto(59.7988, 30.125921));
		result.add(new MapRegionDto(59.797399, 30.135299));
		result.add(new MapRegionDto(59.800685, 30.150017));
		result.add(new MapRegionDto(59.800618, 30.149952));
		result.add(new MapRegionDto(59.79233, 30.172323));
		result.add(new MapRegionDto(59.789738, 30.177829));
		result.add(new MapRegionDto(59.784404, 30.169775));
		result.add(new MapRegionDto(59.780809, 30.173912));
		result.add(new MapRegionDto(59.778511, 30.194674));
		result.add(new MapRegionDto(59.791983, 30.195869));
		result.add(new MapRegionDto(59.793144, 30.193655));
		result.add(new MapRegionDto(59.793277, 30.194025));
		result.add(new MapRegionDto(59.793389, 30.194351));
		result.add(new MapRegionDto(59.793436, 30.194455));
		result.add(new MapRegionDto(59.79355, 30.194895));
		result.add(new MapRegionDto(59.793588, 30.195023));
		result.add(new MapRegionDto(59.793616, 30.195089));
		result.add(new MapRegionDto(59.793705, 30.195181));
		result.add(new MapRegionDto(59.793739, 30.195322));
		result.add(new MapRegionDto(59.793906, 30.195885));
		result.add(new MapRegionDto(59.794182, 30.19679));
		result.add(new MapRegionDto(59.794362, 30.197315));
		result.add(new MapRegionDto(59.794396, 30.19745));
		result.add(new MapRegionDto(59.794487, 30.197691));
		result.add(new MapRegionDto(59.794598, 30.19806));
		result.add(new MapRegionDto(59.794688, 30.198394));
		result.add(new MapRegionDto(59.794884, 30.199056));
		result.add(new MapRegionDto(59.795335, 30.200518));
		result.add(new MapRegionDto(59.795487, 30.201092));
		result.add(new MapRegionDto(59.795494, 30.201173));
		result.add(new MapRegionDto(59.795689, 30.201802));
		result.add(new MapRegionDto(59.795744, 30.202105));
		result.add(new MapRegionDto(59.795925, 30.202775));
		result.add(new MapRegionDto(59.795986, 30.203048));
		result.add(new MapRegionDto(59.796065, 30.203336));
		result.add(new MapRegionDto(59.796128, 30.203602));
		result.add(new MapRegionDto(59.796151, 30.203733));
		result.add(new MapRegionDto(59.796169, 30.203919));
		result.add(new MapRegionDto(59.796213, 30.204178));
		result.add(new MapRegionDto(59.796251, 30.204339));
		result.add(new MapRegionDto(59.796377, 30.204672));
		result.add(new MapRegionDto(59.796445, 30.204832));
		result.add(new MapRegionDto(59.796527, 30.205124));
		result.add(new MapRegionDto(59.796547, 30.205283));
		result.add(new MapRegionDto(59.796564, 30.205379));
		result.add(new MapRegionDto(59.796712, 30.206057));
		result.add(new MapRegionDto(59.79688, 30.206897));
		result.add(new MapRegionDto(59.796895, 30.207012));
		result.add(new MapRegionDto(59.797043, 30.207692));
		result.add(new MapRegionDto(59.797148, 30.208041));
		result.add(new MapRegionDto(59.797228, 30.208366));
		result.add(new MapRegionDto(59.797378, 30.209159));
		result.add(new MapRegionDto(59.797403, 30.209347));
		result.add(new MapRegionDto(59.797415, 30.209417));
		result.add(new MapRegionDto(59.79753, 30.209976));
		result.add(new MapRegionDto(59.797538, 30.210046));
		result.add(new MapRegionDto(59.797537, 30.210124));
		result.add(new MapRegionDto(59.797466, 30.21082));
		result.add(new MapRegionDto(59.797468, 30.210869));
		result.add(new MapRegionDto(59.797522, 30.211065));
		result.add(new MapRegionDto(59.797551, 30.211133));
		result.add(new MapRegionDto(59.797735, 30.211426));
		result.add(new MapRegionDto(59.79732, 30.217257));
		result.add(new MapRegionDto(59.79723, 30.21814));
		result.add(new MapRegionDto(59.797028, 30.221295));
		result.add(new MapRegionDto(59.796924, 30.222067));
		result.add(new MapRegionDto(59.795709, 30.230331));
		result.add(new MapRegionDto(59.794323, 30.239637));
		result.add(new MapRegionDto(59.793067, 30.248123));
		result.add(new MapRegionDto(59.791319, 30.259835));
		result.add(new MapRegionDto(59.790585, 30.264896));
		result.add(new MapRegionDto(59.789957, 30.269119));
		result.add(new MapRegionDto(59.790035, 30.269133));
		result.add(new MapRegionDto(59.794638, 30.269722));
		result.add(new MapRegionDto(59.795198, 30.27638));
		result.add(new MapRegionDto(59.79536, 30.282267));
		result.add(new MapRegionDto(59.792159, 30.287298));
		result.add(new MapRegionDto(59.790776, 30.287393));
		result.add(new MapRegionDto(59.787914, 30.283247));
		result.add(new MapRegionDto(59.787829, 30.283165));
		result.add(new MapRegionDto(59.782795, 30.275925));
		result.add(new MapRegionDto(59.781511, 30.274115));
		result.add(new MapRegionDto(59.778786, 30.270602));
		result.add(new MapRegionDto(59.777657, 30.273644));
		result.add(new MapRegionDto(59.777282, 30.273008));
		result.add(new MapRegionDto(59.774707, 30.268528));
		result.add(new MapRegionDto(59.774402, 30.269499));
		result.add(new MapRegionDto(59.774004, 30.270803));
		result.add(new MapRegionDto(59.770801, 30.263727));
		result.add(new MapRegionDto(59.762581, 30.258928));
		result.add(new MapRegionDto(59.762407, 30.258815));
		result.add(new MapRegionDto(59.75661, 30.274881));
		result.add(new MapRegionDto(59.747625, 30.300259));
		result.add(new MapRegionDto(59.747518, 30.300159));
		result.add(new MapRegionDto(59.742777, 30.314624));
		result.add(new MapRegionDto(59.762406, 30.33238));
		result.add(new MapRegionDto(59.771859, 30.339412));
		result.add(new MapRegionDto(59.77168, 30.332472));
		result.add(new MapRegionDto(59.772719, 30.33143));
		result.add(new MapRegionDto(59.782251, 30.328368));
		result.add(new MapRegionDto(59.78992, 30.328384));
		result.add(new MapRegionDto(59.789985, 30.334585));
		result.add(new MapRegionDto(59.797042, 30.334322));
		result.add(new MapRegionDto(59.799665, 30.337402));
		result.add(new MapRegionDto(59.800902, 30.337346));
		result.add(new MapRegionDto(59.801885, 30.33913));
		result.add(new MapRegionDto(59.804595, 30.341126));
		result.add(new MapRegionDto(59.8086, 30.340747));
		result.add(new MapRegionDto(59.812868, 30.349717));
		result.add(new MapRegionDto(59.816505, 30.368306));
		result.add(new MapRegionDto(59.817499, 30.368067));
		result.add(new MapRegionDto(59.817487, 30.366593));
		result.add(new MapRegionDto(59.81826, 30.365133));
		result.add(new MapRegionDto(59.818641, 30.365958));
		result.add(new MapRegionDto(59.818443, 30.366333));
		result.add(new MapRegionDto(59.818834, 30.367205));
		result.add(new MapRegionDto(59.818057, 30.368651));
		result.add(new MapRegionDto(59.818848, 30.370827));
		result.add(new MapRegionDto(59.819094, 30.371308));
		result.add(new MapRegionDto(59.818726, 30.372072));
		result.add(new MapRegionDto(59.818968, 30.372721));
		result.add(new MapRegionDto(59.818859, 30.372894));
		result.add(new MapRegionDto(59.818307, 30.372491));
		result.add(new MapRegionDto(59.817936, 30.371827));
		result.add(new MapRegionDto(59.817714, 30.37193));
		result.add(new MapRegionDto(59.815027, 30.375433));
		result.add(new MapRegionDto(59.816554, 30.382177));
		result.add(new MapRegionDto(59.81692, 30.381994));
		result.add(new MapRegionDto(59.844493, 30.368772));
		result.add(new MapRegionDto(59.860325, 30.361387));
		result.add(new MapRegionDto(59.874019, 30.354635));
		result.add(new MapRegionDto(59.880159, 30.347271));
		result.add(new MapRegionDto(59.886486, 30.344872));
		result.add(new MapRegionDto(59.888576, 30.345209));
		result.add(new MapRegionDto(59.891726, 30.345216));
		result.add(new MapRegionDto(59.892341, 30.345057));
		result.add(new MapRegionDto(59.895036, 30.343777));
		result.add(new MapRegionDto(59.90191, 30.340412));
		result.add(new MapRegionDto(59.913476, 30.335017));
		result.add(new MapRegionDto(59.913679, 30.334122));
		result.add(new MapRegionDto(59.913634, 30.334004));
		result.add(new MapRegionDto(59.913524, 30.33417));
		result.add(new MapRegionDto(59.913184, 30.333204));
		result.add(new MapRegionDto(59.913461, 30.332825));
		result.add(new MapRegionDto(59.913496, 30.332745));
		result.add(new MapRegionDto(59.913461, 30.33278));
		result.add(new MapRegionDto(59.913456, 30.332763));
		result.add(new MapRegionDto(59.913442, 30.332782));
		result.add(new MapRegionDto(59.91345, 30.332804));
		result.add(new MapRegionDto(59.913445, 30.332812));
		result.add(new MapRegionDto(59.91335, 30.33288));
		result.add(new MapRegionDto(59.913166, 30.33315));
		result.add(new MapRegionDto(59.912163, 30.330334));
		result.add(new MapRegionDto(59.91026, 30.325113));
		result.add(new MapRegionDto(59.90884, 30.32131));
		result.add(new MapRegionDto(59.908683, 30.320697));
		result.add(new MapRegionDto(59.908559, 30.319795));
		result.add(new MapRegionDto(59.908517, 30.31911));
		result.add(new MapRegionDto(59.908562, 30.316512));
		result.add(new MapRegionDto(59.908604, 30.309223));
		result.add(new MapRegionDto(59.908729, 30.301138));
		result.add(new MapRegionDto(59.9088, 30.297357));
		result.add(new MapRegionDto(59.90922, 30.297293));
		result.add(new MapRegionDto(59.908799, 30.297219));
		result.add(new MapRegionDto(59.908857, 30.294357));
		result.add(new MapRegionDto(59.908912, 30.289161));
		result.add(new MapRegionDto(59.908946, 30.286567));
		result.add(new MapRegionDto(59.909, 30.276517));
		result.add(new MapRegionDto(59.909398, 30.276483));
		result.add(new MapRegionDto(59.909405, 30.276482));
		result.add(new MapRegionDto(59.909405, 30.276419));
		result.add(new MapRegionDto(59.909002, 30.276423));
		result.add(new MapRegionDto(59.909012, 30.274746));
		result.add(new MapRegionDto(59.909419, 30.27471));
		result.add(new MapRegionDto(59.909012, 30.274644));
		result.add(new MapRegionDto(59.909064, 30.266084));
		result.add(new MapRegionDto(59.90947, 30.266043));
		result.add(new MapRegionDto(59.909469, 30.266014));
		result.add(new MapRegionDto(59.909471, 30.265989));
		result.add(new MapRegionDto(59.909065, 30.26595));
		result.add(new MapRegionDto(59.90909, 30.261136));
		result.add(new MapRegionDto(59.909517, 30.261208));
		result.add(new MapRegionDto(59.909732, 30.261149));
		result.add(new MapRegionDto(59.911316, 30.261809));
		result.add(new MapRegionDto(59.913687, 30.262156));
		result.add(new MapRegionDto(59.915892, 30.262092));
		result.add(new MapRegionDto(59.915983, 30.260695));
		result.add(new MapRegionDto(59.915917, 30.260687));
		result.add(new MapRegionDto(59.915881, 30.260432));
		result.add(new MapRegionDto(59.915453, 30.257699));
		result.add(new MapRegionDto(59.914988, 30.25471));
		result.add(new MapRegionDto(59.914539, 30.25187));
		result.add(new MapRegionDto(59.914454, 30.249197));
		result.add(new MapRegionDto(59.91436, 30.246521));
		result.add(new MapRegionDto(59.914269, 30.243845));
		result.add(new MapRegionDto(59.914157, 30.240786));
		result.add(new MapRegionDto(59.914143, 30.240765));
		result.add(new MapRegionDto(59.913986, 30.240791));
		result.add(new MapRegionDto(59.913932, 30.240799));
		result.add(new MapRegionDto(59.913833, 30.240507));
		result.add(new MapRegionDto(59.913775, 30.238978));
		result.add(new MapRegionDto(59.913711, 30.236942));
		result.add(new MapRegionDto(59.913614, 30.233759));
		result.add(new MapRegionDto(59.913615, 30.233461));
		result.add(new MapRegionDto(59.913624, 30.232542));
		result.add(new MapRegionDto(59.913733, 30.232352));
		result.add(new MapRegionDto(59.913649, 30.230633));
		result.add(new MapRegionDto(59.913595, 30.229541));
		result.add(new MapRegionDto(59.913558, 30.228783));
		result.add(new MapRegionDto(59.914145, 30.224));
		result.add(new MapRegionDto(59.914294, 30.22364));
		result.add(new MapRegionDto(59.914384, 30.223287));
		result.add(new MapRegionDto(59.914511, 30.22299));
		result.add(new MapRegionDto(59.914534, 30.222872));
		result.add(new MapRegionDto(59.914652, 30.222512));
		result.add(new MapRegionDto(59.914731, 30.222312));
		result.add(new MapRegionDto(59.914798, 30.222178));
		result.add(new MapRegionDto(59.914837, 30.222052));
		result.add(new MapRegionDto(59.914856, 30.221906));
		result.add(new MapRegionDto(59.914867, 30.221456));
		result.add(new MapRegionDto(59.914944, 30.221019));
		result.add(new MapRegionDto(59.914988, 30.220707));
		result.add(new MapRegionDto(59.915056, 30.220389));
		result.add(new MapRegionDto(59.915118, 30.220066));
		result.add(new MapRegionDto(59.91521, 30.219674));
		result.add(new MapRegionDto(59.915345, 30.21901));
		result.add(new MapRegionDto(59.91535, 30.218932));
		result.add(new MapRegionDto(59.915403, 30.218694));
		result.add(new MapRegionDto(59.915481, 30.218763));
		result.add(new MapRegionDto(59.91565, 30.218066));
		result.add(new MapRegionDto(59.91556, 30.217988));
		result.add(new MapRegionDto(59.916092, 30.215742));
		result.add(new MapRegionDto(59.916142, 30.21551));
		result.add(new MapRegionDto(59.916221, 30.215146));
		result.add(new MapRegionDto(59.916279, 30.214649));
		result.add(new MapRegionDto(59.916281, 30.214633));
		result.add(new MapRegionDto(59.916282, 30.214617));
		result.add(new MapRegionDto(59.916284, 30.214601));
		result.add(new MapRegionDto(59.916286, 30.214584));
		result.add(new MapRegionDto(59.916287, 30.214568));
		result.add(new MapRegionDto(59.916288, 30.214552));
		result.add(new MapRegionDto(59.916289, 30.214535));
		result.add(new MapRegionDto(59.91629, 30.214519));
		result.add(new MapRegionDto(59.916291, 30.214502));
		result.add(new MapRegionDto(59.916292, 30.214486));
		result.add(new MapRegionDto(59.916292, 30.214469));
		result.add(new MapRegionDto(59.916293, 30.214453));
		result.add(new MapRegionDto(59.916293, 30.214437));
		result.add(new MapRegionDto(59.916293, 30.21442));
		result.add(new MapRegionDto(59.916293, 30.214403));
		result.add(new MapRegionDto(59.916293, 30.214387));
		result.add(new MapRegionDto(59.916292, 30.21437));
		result.add(new MapRegionDto(59.916292, 30.214354));
		result.add(new MapRegionDto(59.916291, 30.214337));
		result.add(new MapRegionDto(59.91629, 30.214321));
		result.add(new MapRegionDto(59.91629, 30.214305));
		result.add(new MapRegionDto(59.916288, 30.214288));
		result.add(new MapRegionDto(59.916287, 30.214272));
		result.add(new MapRegionDto(59.916286, 30.214256));
		result.add(new MapRegionDto(59.916284, 30.214239));
		result.add(new MapRegionDto(59.916283, 30.214223));
		result.add(new MapRegionDto(59.916279, 30.214191));
		result.add(new MapRegionDto(59.916275, 30.214159));
		result.add(new MapRegionDto(59.916272, 30.214143));
		result.add(new MapRegionDto(59.91627, 30.214127));
		result.add(new MapRegionDto(59.916267, 30.214112));
		result.add(new MapRegionDto(59.916265, 30.214096));
		result.add(new MapRegionDto(59.916262, 30.214081));
		result.add(new MapRegionDto(59.916259, 30.214065));
		result.add(new MapRegionDto(59.916256, 30.21405));
		result.add(new MapRegionDto(59.916252, 30.214035));
		result.add(new MapRegionDto(59.916249, 30.21402));
		result.add(new MapRegionDto(59.916245, 30.214005));
		result.add(new MapRegionDto(59.916242, 30.21399));
		result.add(new MapRegionDto(59.916238, 30.213975));
		result.add(new MapRegionDto(59.916234, 30.213961));
		result.add(new MapRegionDto(59.91623, 30.213947));
		result.add(new MapRegionDto(59.916226, 30.213932));
		result.add(new MapRegionDto(59.916221, 30.213918));
		result.add(new MapRegionDto(59.916217, 30.213904));
		result.add(new MapRegionDto(59.916212, 30.213891));
		result.add(new MapRegionDto(59.916208, 30.213877));
		result.add(new MapRegionDto(59.916203, 30.213864));
		result.add(new MapRegionDto(59.916198, 30.21385));
		result.add(new MapRegionDto(59.916193, 30.213837));
		result.add(new MapRegionDto(59.916188, 30.213824));
		result.add(new MapRegionDto(59.916186, 30.213821));
		result.add(new MapRegionDto(59.916181, 30.213808));
		result.add(new MapRegionDto(59.916176, 30.213795));
		result.add(new MapRegionDto(59.91617, 30.213783));
		result.add(new MapRegionDto(59.916164, 30.213771));
		result.add(new MapRegionDto(59.916159, 30.213759));
		result.add(new MapRegionDto(59.916153, 30.213747));
		result.add(new MapRegionDto(59.916147, 30.213736));
		result.add(new MapRegionDto(59.916141, 30.213724));
		result.add(new MapRegionDto(59.916135, 30.213713));
		result.add(new MapRegionDto(59.916129, 30.213702));
		result.add(new MapRegionDto(59.916116, 30.213681));
		result.add(new MapRegionDto(59.916109, 30.213671));
		result.add(new MapRegionDto(59.916103, 30.213661));
		result.add(new MapRegionDto(59.916096, 30.213651));
		result.add(new MapRegionDto(59.916089, 30.213641));
		result.add(new MapRegionDto(59.916082, 30.213632));
		result.add(new MapRegionDto(59.915878, 30.213403));
		result.add(new MapRegionDto(59.914927, 30.21253));
		result.add(new MapRegionDto(59.91478, 30.212334));
		result.add(new MapRegionDto(59.914595, 30.212186));
		result.add(new MapRegionDto(59.9141, 30.21172));
		result.add(new MapRegionDto(59.914008, 30.211701));
		result.add(new MapRegionDto(59.912858, 30.210661));
		result.add(new MapRegionDto(59.912805, 30.210526));
		result.add(new MapRegionDto(59.912728, 30.210431));
		result.add(new MapRegionDto(59.912573, 30.210282));
		result.add(new MapRegionDto(59.912399, 30.210149));
		result.add(new MapRegionDto(59.912173, 30.209931));
		result.add(new MapRegionDto(59.911783, 30.209565));
		result.add(new MapRegionDto(59.911614, 30.209441));
		result.add(new MapRegionDto(59.911394, 30.209261));
		result.add(new MapRegionDto(59.911159, 30.209039));
		result.add(new MapRegionDto(59.910973, 30.208876));
		result.add(new MapRegionDto(59.910688, 30.208611));
		result.add(new MapRegionDto(59.910469, 30.208424));
		result.add(new MapRegionDto(59.91022, 30.208175));
		result.add(new MapRegionDto(59.910061, 30.208036));
		result.add(new MapRegionDto(59.909874, 30.207843));
		result.add(new MapRegionDto(59.909701, 30.207682));
		result.add(new MapRegionDto(59.909492, 30.20749));
		result.add(new MapRegionDto(59.909318, 30.207371));
		result.add(new MapRegionDto(59.908929, 30.207017));
		result.add(new MapRegionDto(59.908838, 30.206978));
		result.add(new MapRegionDto(59.908702, 30.206973));
		result.add(new MapRegionDto(59.908584, 30.206996));
		result.add(new MapRegionDto(59.908507, 30.207045));
		result.add(new MapRegionDto(59.908387, 30.207145));
		result.add(new MapRegionDto(59.908303, 30.207251));
		result.add(new MapRegionDto(59.908211, 30.20741));
		result.add(new MapRegionDto(59.908064, 30.207916));
		result.add(new MapRegionDto(59.907956, 30.208262));
		result.add(new MapRegionDto(59.907873, 30.208543));
		result.add(new MapRegionDto(59.907813, 30.208719));
		result.add(new MapRegionDto(59.907741, 30.208993));
		result.add(new MapRegionDto(59.907677, 30.209188));
		result.add(new MapRegionDto(59.907596, 30.209486));
		result.add(new MapRegionDto(59.907391, 30.21019));
		result.add(new MapRegionDto(59.907264, 30.210489));
		result.add(new MapRegionDto(59.907207, 30.21079));
		result.add(new MapRegionDto(59.907131, 30.211098));
		result.add(new MapRegionDto(59.907024, 30.211417));
		result.add(new MapRegionDto(59.906937, 30.211777));
		result.add(new MapRegionDto(59.906738, 30.212409));
		result.add(new MapRegionDto(59.906617, 30.212792));
		result.add(new MapRegionDto(59.906396, 30.213573));
		result.add(new MapRegionDto(59.906377, 30.213652));
		result.add(new MapRegionDto(59.906351, 30.213728));
		result.add(new MapRegionDto(59.9063, 30.213885));
		result.add(new MapRegionDto(59.906176, 30.214288));
		result.add(new MapRegionDto(59.906098, 30.214527));
		result.add(new MapRegionDto(59.90604, 30.214853));
		result.add(new MapRegionDto(59.906023, 30.215095));
		result.add(new MapRegionDto(59.906019, 30.215247));
		result.add(new MapRegionDto(59.906007, 30.215353));
		result.add(new MapRegionDto(59.905986, 30.215401));
		result.add(new MapRegionDto(59.905902, 30.215532));
		result.add(new MapRegionDto(59.905843, 30.21558));
		result.add(new MapRegionDto(59.905815, 30.215622));
		result.add(new MapRegionDto(59.905798, 30.215671));
		result.add(new MapRegionDto(59.90577, 30.215767));
		result.add(new MapRegionDto(59.905765, 30.215845));
		result.add(new MapRegionDto(59.905771, 30.215955));
		result.add(new MapRegionDto(59.90533, 30.216318));
		result.add(new MapRegionDto(59.905325, 30.216297));
		result.add(new MapRegionDto(59.905307, 30.216246));
		result.add(new MapRegionDto(59.905282, 30.216179));
		result.add(new MapRegionDto(59.905269, 30.216149));
		result.add(new MapRegionDto(59.905256, 30.21613));
		result.add(new MapRegionDto(59.90524, 30.21611));
		result.add(new MapRegionDto(59.905223, 30.216086));
		result.add(new MapRegionDto(59.905215, 30.216082));
		result.add(new MapRegionDto(59.9052, 30.216077));
		result.add(new MapRegionDto(59.905186, 30.216075));
		result.add(new MapRegionDto(59.905166, 30.216075));
		result.add(new MapRegionDto(59.905118, 30.216095));
		result.add(new MapRegionDto(59.905092, 30.216101));
		result.add(new MapRegionDto(59.905065, 30.216108));
		result.add(new MapRegionDto(59.905038, 30.216116));
		result.add(new MapRegionDto(59.905012, 30.216124));
		result.add(new MapRegionDto(59.904985, 30.216131));
		result.add(new MapRegionDto(59.904958, 30.216137));
		result.add(new MapRegionDto(59.904932, 30.216142));
		result.add(new MapRegionDto(59.904905, 30.216146));
		result.add(new MapRegionDto(59.904878, 30.21615));
		result.add(new MapRegionDto(59.904851, 30.216153));
		result.add(new MapRegionDto(59.904824, 30.216155));
		result.add(new MapRegionDto(59.904797, 30.216157));
		result.add(new MapRegionDto(59.90477, 30.216158));
		result.add(new MapRegionDto(59.904743, 30.216158));
		result.add(new MapRegionDto(59.904716, 30.216158));
		result.add(new MapRegionDto(59.904689, 30.216158));
		result.add(new MapRegionDto(59.904663, 30.216157));
		result.add(new MapRegionDto(59.904636, 30.216156));
		result.add(new MapRegionDto(59.904609, 30.216155));
		result.add(new MapRegionDto(59.904582, 30.216154));
		result.add(new MapRegionDto(59.904555, 30.216153));
		result.add(new MapRegionDto(59.904528, 30.216151));
		result.add(new MapRegionDto(59.904501, 30.21615));
		result.add(new MapRegionDto(59.904474, 30.216148));
		result.add(new MapRegionDto(59.904447, 30.216147));
		result.add(new MapRegionDto(59.90442, 30.216145));
		result.add(new MapRegionDto(59.904393, 30.216143));
		result.add(new MapRegionDto(59.904366, 30.216141));
		result.add(new MapRegionDto(59.90434, 30.216139));
		result.add(new MapRegionDto(59.904313, 30.216135));
		result.add(new MapRegionDto(59.904286, 30.216132));
		result.add(new MapRegionDto(59.904259, 30.216127));
		result.add(new MapRegionDto(59.904232, 30.216121));
		result.add(new MapRegionDto(59.904205, 30.216114));
		result.add(new MapRegionDto(59.904179, 30.216107));
		result.add(new MapRegionDto(59.904152, 30.216097));
		result.add(new MapRegionDto(59.904126, 30.216087));
		result.add(new MapRegionDto(59.9041, 30.216074));
		result.add(new MapRegionDto(59.904074, 30.21606));
		result.add(new MapRegionDto(59.904048, 30.216044));
		result.add(new MapRegionDto(59.904023, 30.216026));
		result.add(new MapRegionDto(59.903998, 30.216006));
		result.add(new MapRegionDto(59.903973, 30.215984));
		result.add(new MapRegionDto(59.903949, 30.21596));
		result.add(new MapRegionDto(59.903926, 30.215933));
		result.add(new MapRegionDto(59.903903, 30.215905));
		result.add(new MapRegionDto(59.90388, 30.215875));
		result.add(new MapRegionDto(59.903859, 30.215844));
		result.add(new MapRegionDto(59.903806, 30.215761));
		result.add(new MapRegionDto(59.903795, 30.215744));
		result.add(new MapRegionDto(59.903775, 30.215708));
		result.add(new MapRegionDto(59.903755, 30.215672));
		result.add(new MapRegionDto(59.903736, 30.215635));
		result.add(new MapRegionDto(59.903717, 30.215598));
		result.add(new MapRegionDto(59.903698, 30.21556));
		result.add(new MapRegionDto(59.903679, 30.215521));
		result.add(new MapRegionDto(59.90366, 30.215483));
		result.add(new MapRegionDto(59.903642, 30.215444));
		result.add(new MapRegionDto(59.903623, 30.215404));
		result.add(new MapRegionDto(59.903605, 30.215365));
		result.add(new MapRegionDto(59.903587, 30.215326));
		result.add(new MapRegionDto(59.903569, 30.215286));
		result.add(new MapRegionDto(59.90355, 30.215247));
		result.add(new MapRegionDto(59.903532, 30.215208));
		result.add(new MapRegionDto(59.903514, 30.215169));
		result.add(new MapRegionDto(59.903495, 30.21513));
		result.add(new MapRegionDto(59.903476, 30.215091));
		result.add(new MapRegionDto(59.903457, 30.215054));
		result.add(new MapRegionDto(59.903438, 30.215016));
		result.add(new MapRegionDto(59.903418, 30.214979));
		result.add(new MapRegionDto(59.903399, 30.214942));
		result.add(new MapRegionDto(59.903379, 30.214906));
		result.add(new MapRegionDto(59.903359, 30.214871));
		result.add(new MapRegionDto(59.903338, 30.214836));
		result.add(new MapRegionDto(59.903318, 30.214801));
		result.add(new MapRegionDto(59.903297, 30.214767));
		result.add(new MapRegionDto(59.903276, 30.214734));
		result.add(new MapRegionDto(59.903255, 30.214701));
		result.add(new MapRegionDto(59.903233, 30.214668));
		result.add(new MapRegionDto(59.903212, 30.214636));
		result.add(new MapRegionDto(59.90319, 30.214605));
		result.add(new MapRegionDto(59.903168, 30.214574));
		result.add(new MapRegionDto(59.903146, 30.214543));
		result.add(new MapRegionDto(59.903123, 30.214513));
		result.add(new MapRegionDto(59.903101, 30.214484));
		result.add(new MapRegionDto(59.903078, 30.214454));
		result.add(new MapRegionDto(59.903056, 30.214426));
		result.add(new MapRegionDto(59.903033, 30.214398));
		result.add(new MapRegionDto(59.90301, 30.21437));
		result.add(new MapRegionDto(59.902986, 30.214343));
		result.add(new MapRegionDto(59.902963, 30.214316));
		result.add(new MapRegionDto(59.90294, 30.214289));
		result.add(new MapRegionDto(59.902916, 30.214263));
		result.add(new MapRegionDto(59.902893, 30.214237));
		result.add(new MapRegionDto(59.902869, 30.214212));
		result.add(new MapRegionDto(59.902845, 30.214186));
		result.add(new MapRegionDto(59.902822, 30.214161));
		result.add(new MapRegionDto(59.902798, 30.214135));
		result.add(new MapRegionDto(59.902774, 30.214109));
		result.add(new MapRegionDto(59.902751, 30.214082));
		result.add(new MapRegionDto(59.902728, 30.214055));
		result.add(new MapRegionDto(59.902705, 30.214026));
		result.add(new MapRegionDto(59.902683, 30.213997));
		result.add(new MapRegionDto(59.902661, 30.213966));
		result.add(new MapRegionDto(59.902639, 30.213933));
		result.add(new MapRegionDto(59.902619, 30.213899));
		result.add(new MapRegionDto(59.902599, 30.213863));
		result.add(new MapRegionDto(59.90258, 30.213825));
		result.add(new MapRegionDto(59.902562, 30.213785));
		result.add(new MapRegionDto(59.902546, 30.213742));
		result.add(new MapRegionDto(59.902531, 30.213697));
		result.add(new MapRegionDto(59.902518, 30.21365));
		result.add(new MapRegionDto(59.902507, 30.213601));
		result.add(new MapRegionDto(59.902498, 30.213551));
		result.add(new MapRegionDto(59.90249, 30.2135));
		result.add(new MapRegionDto(59.902483, 30.213448));
		result.add(new MapRegionDto(59.902477, 30.213395));
		result.add(new MapRegionDto(59.902473, 30.213342));
		result.add(new MapRegionDto(59.902469, 30.213289));
		result.add(new MapRegionDto(59.902466, 30.213236));
		result.add(new MapRegionDto(59.902463, 30.213183));
		result.add(new MapRegionDto(59.90246, 30.21313));
		result.add(new MapRegionDto(59.902458, 30.213076));
		result.add(new MapRegionDto(59.902456, 30.213023));
		result.add(new MapRegionDto(59.902454, 30.212969));
		result.add(new MapRegionDto(59.902452, 30.212916));
		result.add(new MapRegionDto(59.90245, 30.212862));
		result.add(new MapRegionDto(59.902448, 30.212809));
		result.add(new MapRegionDto(59.902445, 30.212756));
		result.add(new MapRegionDto(59.902442, 30.212702));
		result.add(new MapRegionDto(59.902438, 30.212649));
		result.add(new MapRegionDto(59.902433, 30.212597));
		result.add(new MapRegionDto(59.902426, 30.212544));
		result.add(new MapRegionDto(59.902419, 30.212493));
		result.add(new MapRegionDto(59.90241, 30.212442));
		result.add(new MapRegionDto(59.902399, 30.212393));
		result.add(new MapRegionDto(59.902386, 30.212346));
		result.add(new MapRegionDto(59.902371, 30.212302));
		result.add(new MapRegionDto(59.902354, 30.212261));
		result.add(new MapRegionDto(59.902335, 30.212223));
		result.add(new MapRegionDto(59.902315, 30.212188));
		result.add(new MapRegionDto(59.902293, 30.212156));
		result.add(new MapRegionDto(59.90227, 30.212127));
		result.add(new MapRegionDto(59.902247, 30.212101));
		result.add(new MapRegionDto(59.902223, 30.212076));
		result.add(new MapRegionDto(59.902199, 30.212054));
		result.add(new MapRegionDto(59.902174, 30.212033));
		result.add(new MapRegionDto(59.902149, 30.212013));
		result.add(new MapRegionDto(59.902123, 30.211995));
		result.add(new MapRegionDto(59.902098, 30.211977));
		result.add(new MapRegionDto(59.902072, 30.21196));
		result.add(new MapRegionDto(59.902047, 30.211944));
		result.add(new MapRegionDto(59.902021, 30.211928));
		result.add(new MapRegionDto(59.901995, 30.211912));
		result.add(new MapRegionDto(59.90197, 30.211897));
		result.add(new MapRegionDto(59.901944, 30.211881));
		result.add(new MapRegionDto(59.901918, 30.211865));
		result.add(new MapRegionDto(59.901892, 30.211849));
		result.add(new MapRegionDto(59.901867, 30.211833));
		result.add(new MapRegionDto(59.901841, 30.211815));
		result.add(new MapRegionDto(59.901783, 30.211772));
		result.add(new MapRegionDto(59.901766, 30.211759));
		result.add(new MapRegionDto(59.901741, 30.211738));
		result.add(new MapRegionDto(59.901716, 30.211715));
		result.add(new MapRegionDto(59.901692, 30.211691));
		result.add(new MapRegionDto(59.901669, 30.211666));
		result.add(new MapRegionDto(59.901646, 30.211638));
		result.add(new MapRegionDto(59.901623, 30.211608));
		result.add(new MapRegionDto(59.901601, 30.211577));
		result.add(new MapRegionDto(59.90158, 30.211544));
		result.add(new MapRegionDto(59.901559, 30.21151));
		result.add(new MapRegionDto(59.901539, 30.211475));
		result.add(new MapRegionDto(59.901519, 30.21144));
		result.add(new MapRegionDto(59.901499, 30.211403));
		result.add(new MapRegionDto(59.90148, 30.211366));
		result.add(new MapRegionDto(59.90146, 30.211329));
		result.add(new MapRegionDto(59.901441, 30.211292));
		result.add(new MapRegionDto(59.901421, 30.211255));
		result.add(new MapRegionDto(59.901402, 30.211218));
		result.add(new MapRegionDto(59.901382, 30.211182));
		result.add(new MapRegionDto(59.901362, 30.211146));
		result.add(new MapRegionDto(59.901341, 30.211112));
		result.add(new MapRegionDto(59.90132, 30.211078));
		result.add(new MapRegionDto(59.901299, 30.211046));
		result.add(new MapRegionDto(59.901277, 30.211014));
		result.add(new MapRegionDto(59.901255, 30.210984));
		result.add(new MapRegionDto(59.901232, 30.210954));
		result.add(new MapRegionDto(59.901209, 30.210926));
		result.add(new MapRegionDto(59.901186, 30.210898));
		result.add(new MapRegionDto(59.901163, 30.210871));
		result.add(new MapRegionDto(59.90114, 30.210845));
		result.add(new MapRegionDto(59.901116, 30.210819));
		result.add(new MapRegionDto(59.901092, 30.210794));
		result.add(new MapRegionDto(59.901068, 30.21077));
		result.add(new MapRegionDto(59.901044, 30.210746));
		result.add(new MapRegionDto(59.90102, 30.210723));
		result.add(new MapRegionDto(59.900996, 30.2107));
		result.add(new MapRegionDto(59.900971, 30.210677));
		result.add(new MapRegionDto(59.900947, 30.210655));
		result.add(new MapRegionDto(59.900922, 30.210633));
		result.add(new MapRegionDto(59.900897, 30.210611));
		result.add(new MapRegionDto(59.900873, 30.21059));
		result.add(new MapRegionDto(59.900848, 30.210569));
		result.add(new MapRegionDto(59.900823, 30.210548));
		result.add(new MapRegionDto(59.900798, 30.210526));
		result.add(new MapRegionDto(59.900774, 30.210505));
		result.add(new MapRegionDto(59.900749, 30.210484));
		result.add(new MapRegionDto(59.900724, 30.210463));
		result.add(new MapRegionDto(59.900699, 30.210442));
		result.add(new MapRegionDto(59.900675, 30.210421));
		result.add(new MapRegionDto(59.90065, 30.210399));
		result.add(new MapRegionDto(59.900625, 30.210378));
		result.add(new MapRegionDto(59.900601, 30.210356));
		result.add(new MapRegionDto(59.900576, 30.210334));
		result.add(new MapRegionDto(59.900552, 30.210311));
		result.add(new MapRegionDto(59.900527, 30.210288));
		result.add(new MapRegionDto(59.900503, 30.210265));
		result.add(new MapRegionDto(59.900479, 30.210242));
		result.add(new MapRegionDto(59.900455, 30.210218));
		result.add(new MapRegionDto(59.900431, 30.210193));
		result.add(new MapRegionDto(59.900407, 30.210169));
		result.add(new MapRegionDto(59.900383, 30.210143));
		result.add(new MapRegionDto(59.90036, 30.210117));
		result.add(new MapRegionDto(59.900337, 30.21009));
		result.add(new MapRegionDto(59.900313, 30.210063));
		result.add(new MapRegionDto(59.90029, 30.210035));
		result.add(new MapRegionDto(59.900268, 30.210006));
		result.add(new MapRegionDto(59.900245, 30.209977));
		result.add(new MapRegionDto(59.900223, 30.209947));
		result.add(new MapRegionDto(59.9002, 30.209917));
		result.add(new MapRegionDto(59.900178, 30.209887));
		result.add(new MapRegionDto(59.900156, 30.209856));
		result.add(new MapRegionDto(59.900134, 30.209825));
		result.add(new MapRegionDto(59.900112, 30.209794));
		result.add(new MapRegionDto(59.90009, 30.209764));
		result.add(new MapRegionDto(59.900068, 30.209733));
		result.add(new MapRegionDto(59.900046, 30.209703));
		result.add(new MapRegionDto(59.899991, 30.20963));
		result.add(new MapRegionDto(59.899978, 30.209615));
		result.add(new MapRegionDto(59.899955, 30.209586));
		result.add(new MapRegionDto(59.899932, 30.209559));
		result.add(new MapRegionDto(59.899909, 30.209533));
		result.add(new MapRegionDto(59.899885, 30.209507));
		result.add(new MapRegionDto(59.899861, 30.209483));
		result.add(new MapRegionDto(59.899837, 30.20946));
		result.add(new MapRegionDto(59.899812, 30.209439));
		result.add(new MapRegionDto(59.899787, 30.209419));
		result.add(new MapRegionDto(59.899762, 30.209399));
		result.add(new MapRegionDto(59.899737, 30.209381));
		result.add(new MapRegionDto(59.899711, 30.209363));
		result.add(new MapRegionDto(59.899686, 30.209346));
		result.add(new MapRegionDto(59.89966, 30.209329));
		result.add(new MapRegionDto(59.899635, 30.209312));
		result.add(new MapRegionDto(59.899609, 30.209295));
		result.add(new MapRegionDto(59.899584, 30.209277));
		result.add(new MapRegionDto(59.899558, 30.209259));
		result.add(new MapRegionDto(59.899533, 30.209241));
		result.add(new MapRegionDto(59.899508, 30.209221));
		result.add(new MapRegionDto(59.899483, 30.209201));
		result.add(new MapRegionDto(59.899459, 30.209179));
		result.add(new MapRegionDto(59.899434, 30.209155));
		result.add(new MapRegionDto(59.899422, 30.209142));
		result.add(new MapRegionDto(59.899398, 30.2091));
		result.add(new MapRegionDto(59.899377, 30.209066));
		result.add(new MapRegionDto(59.899357, 30.20903));
		result.add(new MapRegionDto(59.899337, 30.208994));
		result.add(new MapRegionDto(59.899318, 30.208957));
		result.add(new MapRegionDto(59.899299, 30.208919));
		result.add(new MapRegionDto(59.89928, 30.20888));
		result.add(new MapRegionDto(59.899262, 30.20884));
		result.add(new MapRegionDto(59.899245, 30.208799));
		result.add(new MapRegionDto(59.899228, 30.208757));
		result.add(new MapRegionDto(59.899212, 30.208715));
		result.add(new MapRegionDto(59.899195, 30.208672));
		result.add(new MapRegionDto(59.899179, 30.208629));
		result.add(new MapRegionDto(59.899164, 30.208585));
		result.add(new MapRegionDto(59.899148, 30.208541));
		result.add(new MapRegionDto(59.899133, 30.208497));
		result.add(new MapRegionDto(59.899125, 30.208475));
		result.add(new MapRegionDto(59.899109, 30.208432));
		result.add(new MapRegionDto(59.899093, 30.20839));
		result.add(new MapRegionDto(59.899076, 30.208347));
		result.add(new MapRegionDto(59.89906, 30.208305));
		result.add(new MapRegionDto(59.899044, 30.208262));
		result.add(new MapRegionDto(59.899027, 30.208219));
		result.add(new MapRegionDto(59.899011, 30.208177));
		result.add(new MapRegionDto(59.898995, 30.208134));
		result.add(new MapRegionDto(59.898978, 30.208092));
		result.add(new MapRegionDto(59.898962, 30.208049));
		result.add(new MapRegionDto(59.898946, 30.208007));
		result.add(new MapRegionDto(59.898929, 30.207964));
		result.add(new MapRegionDto(59.898913, 30.207922));
		result.add(new MapRegionDto(59.898896, 30.207879));
		result.add(new MapRegionDto(59.89888, 30.207837));
		result.add(new MapRegionDto(59.898864, 30.207794));
		result.add(new MapRegionDto(59.898847, 30.207752));
		result.add(new MapRegionDto(59.89883, 30.20771));
		result.add(new MapRegionDto(59.898814, 30.207668));
		result.add(new MapRegionDto(59.898797, 30.207626));
		result.add(new MapRegionDto(59.89878, 30.207584));
		result.add(new MapRegionDto(59.898763, 30.207542));
		result.add(new MapRegionDto(59.898746, 30.207501));
		result.add(new MapRegionDto(59.898729, 30.207459));
		result.add(new MapRegionDto(59.898712, 30.207418));
		result.add(new MapRegionDto(59.898694, 30.207377));
		result.add(new MapRegionDto(59.898677, 30.207337));
		result.add(new MapRegionDto(59.898659, 30.207296));
		result.add(new MapRegionDto(59.898641, 30.207256));
		result.add(new MapRegionDto(59.898623, 30.207216));
		result.add(new MapRegionDto(59.898605, 30.207177));
		result.add(new MapRegionDto(59.898587, 30.207137));
		result.add(new MapRegionDto(59.898568, 30.207098));
		result.add(new MapRegionDto(59.89855, 30.20706));
		result.add(new MapRegionDto(59.898531, 30.207021));
		result.add(new MapRegionDto(59.898512, 30.206983));
		result.add(new MapRegionDto(59.898493, 30.206945));
		result.add(new MapRegionDto(59.898474, 30.206907));
		result.add(new MapRegionDto(59.898455, 30.206869));
		result.add(new MapRegionDto(59.898436, 30.206832));
		result.add(new MapRegionDto(59.898416, 30.206795));
		result.add(new MapRegionDto(59.898397, 30.206758));
		result.add(new MapRegionDto(59.898377, 30.206721));
		result.add(new MapRegionDto(59.898357, 30.206685));
		result.add(new MapRegionDto(59.898338, 30.206648));
		result.add(new MapRegionDto(59.898273, 30.206533));
		result.add(new MapRegionDto(59.898257, 30.206505));
		result.add(new MapRegionDto(59.898237, 30.20647));
		result.add(new MapRegionDto(59.898217, 30.206434));
		result.add(new MapRegionDto(59.898197, 30.206399));
		result.add(new MapRegionDto(59.898176, 30.206364));
		result.add(new MapRegionDto(59.898156, 30.20633));
		result.add(new MapRegionDto(59.898135, 30.206295));
		result.add(new MapRegionDto(59.898115, 30.20626));
		result.add(new MapRegionDto(59.898094, 30.206226));
		result.add(new MapRegionDto(59.898073, 30.206192));
		result.add(new MapRegionDto(59.898052, 30.206157));
		result.add(new MapRegionDto(59.898032, 30.206123));
		result.add(new MapRegionDto(59.898011, 30.206089));
		result.add(new MapRegionDto(59.897991, 30.206054));
		result.add(new MapRegionDto(59.89797, 30.206019));
		result.add(new MapRegionDto(59.89795, 30.205983));
		result.add(new MapRegionDto(59.89793, 30.205948));
		result.add(new MapRegionDto(59.89791, 30.205911));
		result.add(new MapRegionDto(59.897891, 30.205874));
		result.add(new MapRegionDto(59.897872, 30.205837));
		result.add(new MapRegionDto(59.897853, 30.205798));
		result.add(new MapRegionDto(59.897834, 30.205759));
		result.add(new MapRegionDto(59.897816, 30.205719));
		result.add(new MapRegionDto(59.897799, 30.205679));
		result.add(new MapRegionDto(59.897781, 30.205638));
		result.add(new MapRegionDto(59.897764, 30.205596));
		result.add(new MapRegionDto(59.897748, 30.205554));
		result.add(new MapRegionDto(59.897731, 30.205512));
		result.add(new MapRegionDto(59.897715, 30.205469));
		result.add(new MapRegionDto(59.897699, 30.205426));
		result.add(new MapRegionDto(59.897683, 30.205382));
		result.add(new MapRegionDto(59.897668, 30.205339));
		result.add(new MapRegionDto(59.897652, 30.205295));
		result.add(new MapRegionDto(59.897637, 30.205251));
		result.add(new MapRegionDto(59.897621, 30.205207));
		result.add(new MapRegionDto(59.897612, 30.205186));
		result.add(new MapRegionDto(59.897592, 30.205151));
		result.add(new MapRegionDto(59.897571, 30.205117));
		result.add(new MapRegionDto(59.897499, 30.205006));
		result.add(new MapRegionDto(59.897486, 30.204986));
		result.add(new MapRegionDto(59.897464, 30.204954));
		result.add(new MapRegionDto(59.897442, 30.204922));
		result.add(new MapRegionDto(59.897421, 30.20489));
		result.add(new MapRegionDto(59.8974, 30.204857));
		result.add(new MapRegionDto(59.897378, 30.204824));
		result.add(new MapRegionDto(59.897358, 30.20479));
		result.add(new MapRegionDto(59.897337, 30.204755));
		result.add(new MapRegionDto(59.897317, 30.20472));
		result.add(new MapRegionDto(59.897297, 30.204683));
		result.add(new MapRegionDto(59.897277, 30.204646));
		result.add(new MapRegionDto(59.897258, 30.204609));
		result.add(new MapRegionDto(59.897239, 30.204571));
		result.add(new MapRegionDto(59.897221, 30.204532));
		result.add(new MapRegionDto(59.897203, 30.204492));
		result.add(new MapRegionDto(59.897185, 30.204452));
		result.add(new MapRegionDto(59.897167, 30.204412));
		result.add(new MapRegionDto(59.89715, 30.204371));
		result.add(new MapRegionDto(59.897133, 30.204329));
		result.add(new MapRegionDto(59.897116, 30.204288));
		result.add(new MapRegionDto(59.897099, 30.204245));
		result.add(new MapRegionDto(59.897083, 30.204202));
		result.add(new MapRegionDto(59.897067, 30.204159));
		result.add(new MapRegionDto(59.897052, 30.204116));
		result.add(new MapRegionDto(59.897036, 30.204072));
		result.add(new MapRegionDto(59.897021, 30.204027));
		result.add(new MapRegionDto(59.897006, 30.203983));
		result.add(new MapRegionDto(59.896991, 30.203938));
		result.add(new MapRegionDto(59.896984, 30.203916));
		result.add(new MapRegionDto(59.896963, 30.203881));
		result.add(new MapRegionDto(59.896942, 30.203847));
		result.add(new MapRegionDto(59.896922, 30.203813));
		result.add(new MapRegionDto(59.896901, 30.203779));
		result.add(new MapRegionDto(59.89688, 30.203745));
		result.add(new MapRegionDto(59.89686, 30.20371));
		result.add(new MapRegionDto(59.896839, 30.203676));
		result.add(new MapRegionDto(59.896819, 30.203641));
		result.add(new MapRegionDto(59.896798, 30.203606));
		result.add(new MapRegionDto(59.896778, 30.203571));
		result.add(new MapRegionDto(59.896758, 30.203535));
		result.add(new MapRegionDto(59.896738, 30.203499));
		result.add(new MapRegionDto(59.896718, 30.203463));
		result.add(new MapRegionDto(59.896698, 30.203427));
		result.add(new MapRegionDto(59.896678, 30.20339));
		result.add(new MapRegionDto(59.896659, 30.203353));
		result.add(new MapRegionDto(59.89664, 30.203315));
		result.add(new MapRegionDto(59.896621, 30.203277));
		result.add(new MapRegionDto(59.896602, 30.203239));
		result.add(new MapRegionDto(59.896584, 30.2032));
		result.add(new MapRegionDto(59.896565, 30.20316));
		result.add(new MapRegionDto(59.896547, 30.20312));
		result.add(new MapRegionDto(59.89653, 30.20308));
		result.add(new MapRegionDto(59.896513, 30.203039));
		result.add(new MapRegionDto(59.896495, 30.202997));
		result.add(new MapRegionDto(59.896479, 30.202955));
		result.add(new MapRegionDto(59.896462, 30.202913));
		result.add(new MapRegionDto(59.896446, 30.20287));
		result.add(new MapRegionDto(59.89643, 30.202826));
		result.add(new MapRegionDto(59.896415, 30.202783));
		result.add(new MapRegionDto(59.896399, 30.202739));
		result.add(new MapRegionDto(59.896384, 30.202695));
		result.add(new MapRegionDto(59.896369, 30.20265));
		result.add(new MapRegionDto(59.896354, 30.202605));
		result.add(new MapRegionDto(59.896339, 30.202561));
		result.add(new MapRegionDto(59.896325, 30.202515));
		result.add(new MapRegionDto(59.896311, 30.20247));
		result.add(new MapRegionDto(59.896296, 30.202425));
		result.add(new MapRegionDto(59.896282, 30.202379));
		result.add(new MapRegionDto(59.896268, 30.202334));
		result.add(new MapRegionDto(59.896254, 30.202288));
		result.add(new MapRegionDto(59.89624, 30.202242));
		result.add(new MapRegionDto(59.896226, 30.202196));
		result.add(new MapRegionDto(59.896212, 30.202151));
		result.add(new MapRegionDto(59.896198, 30.202105));
		result.add(new MapRegionDto(59.896184, 30.202059));
		result.add(new MapRegionDto(59.896169, 30.202014));
		result.add(new MapRegionDto(59.896155, 30.201968));
		result.add(new MapRegionDto(59.896141, 30.201923));
		result.add(new MapRegionDto(59.896127, 30.201877));
		result.add(new MapRegionDto(59.896112, 30.201832));
		result.add(new MapRegionDto(59.896098, 30.201787));
		result.add(new MapRegionDto(59.896083, 30.201742));
		result.add(new MapRegionDto(59.896068, 30.201698));
		result.add(new MapRegionDto(59.896053, 30.201653));
		result.add(new MapRegionDto(59.896037, 30.201609));
		result.add(new MapRegionDto(59.896022, 30.201566));
		result.add(new MapRegionDto(59.896006, 30.201522));
		result.add(new MapRegionDto(59.89599, 30.201479));
		result.add(new MapRegionDto(59.895974, 30.201436));
		result.add(new MapRegionDto(59.895958, 30.201393));
		result.add(new MapRegionDto(59.895941, 30.201351));
		result.add(new MapRegionDto(59.895925, 30.201309));
		result.add(new MapRegionDto(59.895908, 30.201267));
		result.add(new MapRegionDto(59.895891, 30.201225));
		result.add(new MapRegionDto(59.895874, 30.201183));
		result.add(new MapRegionDto(59.895858, 30.201141));
		result.add(new MapRegionDto(59.895841, 30.201099));
		result.add(new MapRegionDto(59.895824, 30.201057));
		result.add(new MapRegionDto(59.895808, 30.201015));
		result.add(new MapRegionDto(59.895791, 30.200972));
		result.add(new MapRegionDto(59.895774, 30.20093));
		result.add(new MapRegionDto(59.895758, 30.200888));
		result.add(new MapRegionDto(59.895742, 30.200845));
		result.add(new MapRegionDto(59.895726, 30.200802));
		result.add(new MapRegionDto(59.89571, 30.200759));
		result.add(new MapRegionDto(59.895694, 30.200715));
		result.add(new MapRegionDto(59.895678, 30.200672));
		result.add(new MapRegionDto(59.895663, 30.200628));
		result.add(new MapRegionDto(59.895647, 30.200584));
		result.add(new MapRegionDto(59.895632, 30.20054));
		result.add(new MapRegionDto(59.895616, 30.200496));
		result.add(new MapRegionDto(59.895601, 30.200452));
		result.add(new MapRegionDto(59.895586, 30.200408));
		result.add(new MapRegionDto(59.895571, 30.200364));
		result.add(new MapRegionDto(59.895555, 30.20032));
		result.add(new MapRegionDto(59.89554, 30.200276));
		result.add(new MapRegionDto(59.895525, 30.200231));
		result.add(new MapRegionDto(59.89551, 30.200187));
		result.add(new MapRegionDto(59.895494, 30.200143));
		result.add(new MapRegionDto(59.895479, 30.200099));
		result.add(new MapRegionDto(59.895464, 30.200055));
		result.add(new MapRegionDto(59.895448, 30.200011));
		result.add(new MapRegionDto(59.895433, 30.199967));
		result.add(new MapRegionDto(59.895417, 30.199924));
		result.add(new MapRegionDto(59.895401, 30.19988));
		result.add(new MapRegionDto(59.895386, 30.199837));
		result.add(new MapRegionDto(59.89537, 30.199793));
		result.add(new MapRegionDto(59.895354, 30.19975));
		result.add(new MapRegionDto(59.895337, 30.199708));
		result.add(new MapRegionDto(59.895321, 30.199665));
		result.add(new MapRegionDto(59.895305, 30.199623));
		result.add(new MapRegionDto(59.895288, 30.19958));
		result.add(new MapRegionDto(59.895271, 30.199538));
		result.add(new MapRegionDto(59.895255, 30.199496));
		result.add(new MapRegionDto(59.895238, 30.199455));
		result.add(new MapRegionDto(59.895221, 30.199413));
		result.add(new MapRegionDto(59.895204, 30.199371));
		result.add(new MapRegionDto(59.895187, 30.19933));
		result.add(new MapRegionDto(59.895169, 30.199289));
		result.add(new MapRegionDto(59.895152, 30.199248));
		result.add(new MapRegionDto(59.895135, 30.199207));
		result.add(new MapRegionDto(59.895085, 30.19909));
		result.add(new MapRegionDto(59.895082, 30.199084));
		result.add(new MapRegionDto(59.895065, 30.199044));
		result.add(new MapRegionDto(59.895047, 30.199003));
		result.add(new MapRegionDto(59.89503, 30.198963));
		result.add(new MapRegionDto(59.895012, 30.198922));
		result.add(new MapRegionDto(59.894994, 30.198881));
		result.add(new MapRegionDto(59.894941, 30.19876));
		result.add(new MapRegionDto(59.894924, 30.198719));
		result.add(new MapRegionDto(59.894906, 30.198679));
		result.add(new MapRegionDto(59.894889, 30.198638));
		result.add(new MapRegionDto(59.894871, 30.198598));
		result.add(new MapRegionDto(59.894853, 30.198557));
		result.add(new MapRegionDto(59.894836, 30.198517));
		result.add(new MapRegionDto(59.894818, 30.198477));
		result.add(new MapRegionDto(59.8948, 30.198437));
		result.add(new MapRegionDto(59.894782, 30.198397));
		result.add(new MapRegionDto(59.894764, 30.198357));
		result.add(new MapRegionDto(59.894746, 30.198318));
		result.add(new MapRegionDto(59.894727, 30.198279));
		result.add(new MapRegionDto(59.894708, 30.19824));
		result.add(new MapRegionDto(59.89469, 30.198202));
		result.add(new MapRegionDto(59.89467, 30.198164));
		result.add(new MapRegionDto(59.894651, 30.198127));
		result.add(new MapRegionDto(59.894631, 30.19809));
		result.add(new MapRegionDto(59.894612, 30.198054));
		result.add(new MapRegionDto(59.894592, 30.198018));
		result.add(new MapRegionDto(59.894571, 30.197983));
		result.add(new MapRegionDto(59.894551, 30.197948));
		result.add(new MapRegionDto(59.89453, 30.197913));
		result.add(new MapRegionDto(59.89451, 30.197879));
		result.add(new MapRegionDto(59.894489, 30.197845));
		result.add(new MapRegionDto(59.894468, 30.197811));
		result.add(new MapRegionDto(59.894447, 30.197777));
		result.add(new MapRegionDto(59.894427, 30.197743));
		result.add(new MapRegionDto(59.894406, 30.197709));
		result.add(new MapRegionDto(59.894385, 30.197674));
		result.add(new MapRegionDto(59.894364, 30.19764));
		result.add(new MapRegionDto(59.894344, 30.197606));
		result.add(new MapRegionDto(59.894323, 30.197571));
		result.add(new MapRegionDto(59.894303, 30.197536));
		result.add(new MapRegionDto(59.894282, 30.197501));
		result.add(new MapRegionDto(59.894262, 30.197467));
		result.add(new MapRegionDto(59.894241, 30.197432));
		result.add(new MapRegionDto(59.89422, 30.197398));
		result.add(new MapRegionDto(59.8942, 30.197364));
		result.add(new MapRegionDto(59.894179, 30.197331));
		result.add(new MapRegionDto(59.894157, 30.197298));
		result.add(new MapRegionDto(59.894136, 30.197266));
		result.add(new MapRegionDto(59.894114, 30.197235));
		result.add(new MapRegionDto(59.894091, 30.197204));
		result.add(new MapRegionDto(59.894069, 30.197175));
		result.add(new MapRegionDto(59.894046, 30.197147));
		result.add(new MapRegionDto(59.894023, 30.197121));
		result.add(new MapRegionDto(59.893999, 30.197095));
		result.add(new MapRegionDto(59.893975, 30.19707));
		result.add(new MapRegionDto(59.893951, 30.197046));
		result.add(new MapRegionDto(59.893927, 30.197022));
		result.add(new MapRegionDto(59.893903, 30.196999));
		result.add(new MapRegionDto(59.893878, 30.196976));
		result.add(new MapRegionDto(59.893854, 30.196952));
		result.add(new MapRegionDto(59.89383, 30.196928));
		result.add(new MapRegionDto(59.893806, 30.196904));
		result.add(new MapRegionDto(59.893782, 30.196879));
		result.add(new MapRegionDto(59.893759, 30.196852));
		result.add(new MapRegionDto(59.893736, 30.196825));
		result.add(new MapRegionDto(59.893713, 30.196797));
		result.add(new MapRegionDto(59.89369, 30.196768));
		result.add(new MapRegionDto(59.893668, 30.196737));
		result.add(new MapRegionDto(59.893646, 30.196706));
		result.add(new MapRegionDto(59.893625, 30.196674));
		result.add(new MapRegionDto(59.893603, 30.196641));
		result.add(new MapRegionDto(59.893583, 30.196607));
		result.add(new MapRegionDto(59.893562, 30.196572));
		result.add(new MapRegionDto(59.893542, 30.196536));
		result.add(new MapRegionDto(59.893523, 30.196499));
		result.add(new MapRegionDto(59.893503, 30.196462));
		result.add(new MapRegionDto(59.893484, 30.196424));
		result.add(new MapRegionDto(59.893466, 30.196385));
		result.add(new MapRegionDto(59.893448, 30.196345));
		result.add(new MapRegionDto(59.89343, 30.196305));
		result.add(new MapRegionDto(59.893412, 30.196264));
		result.add(new MapRegionDto(59.893395, 30.196223));
		result.add(new MapRegionDto(59.893379, 30.196181));
		result.add(new MapRegionDto(59.893362, 30.196138));
		result.add(new MapRegionDto(59.893346, 30.196095));
		result.add(new MapRegionDto(59.893331, 30.196052));
		result.add(new MapRegionDto(59.893315, 30.196008));
		result.add(new MapRegionDto(59.8933, 30.195964));
		result.add(new MapRegionDto(59.893285, 30.195919));
		result.add(new MapRegionDto(59.89327, 30.195874));
		result.add(new MapRegionDto(59.893255, 30.195829));
		result.add(new MapRegionDto(59.893241, 30.195784));
		result.add(new MapRegionDto(59.893226, 30.195739));
		result.add(new MapRegionDto(59.893212, 30.195694));
		result.add(new MapRegionDto(59.893197, 30.195649));
		result.add(new MapRegionDto(59.893183, 30.195604));
		result.add(new MapRegionDto(59.893168, 30.195559));
		result.add(new MapRegionDto(59.893153, 30.195514));
		result.add(new MapRegionDto(59.893138, 30.19547));
		result.add(new MapRegionDto(59.893123, 30.195426));
		result.add(new MapRegionDto(59.893107, 30.195382));
		result.add(new MapRegionDto(59.893091, 30.195339));
		result.add(new MapRegionDto(59.893075, 30.195296));
		result.add(new MapRegionDto(59.892995, 30.195114));
		result.add(new MapRegionDto(59.892893, 30.194941));
		result.add(new MapRegionDto(59.892882, 30.194926));
		result.add(new MapRegionDto(59.892859, 30.194899));
		result.add(new MapRegionDto(59.892835, 30.194874));
		result.add(new MapRegionDto(59.892811, 30.194851));
		result.add(new MapRegionDto(59.892786, 30.194831));
		result.add(new MapRegionDto(59.892761, 30.194812));
		result.add(new MapRegionDto(59.892735, 30.194796));
		result.add(new MapRegionDto(59.892709, 30.194781));
		result.add(new MapRegionDto(59.892683, 30.194768));
		result.add(new MapRegionDto(59.892657, 30.194756));
		result.add(new MapRegionDto(59.89263, 30.194746));
		result.add(new MapRegionDto(59.892604, 30.194737));
		result.add(new MapRegionDto(59.892577, 30.194729));
		result.add(new MapRegionDto(59.89255, 30.194722));
		result.add(new MapRegionDto(59.892524, 30.194716));
		result.add(new MapRegionDto(59.892497, 30.194711));
		result.add(new MapRegionDto(59.89247, 30.194706));
		result.add(new MapRegionDto(59.892443, 30.194702));
		result.add(new MapRegionDto(59.892416, 30.194698));
		result.add(new MapRegionDto(59.892389, 30.194695));
		result.add(new MapRegionDto(59.892362, 30.194692));
		result.add(new MapRegionDto(59.892336, 30.194689));
		result.add(new MapRegionDto(59.892309, 30.194687));
		result.add(new MapRegionDto(59.892282, 30.194684));
		result.add(new MapRegionDto(59.892255, 30.194681));
		result.add(new MapRegionDto(59.892228, 30.194679));
		result.add(new MapRegionDto(59.892201, 30.194676));
		result.add(new MapRegionDto(59.892174, 30.194672));
		result.add(new MapRegionDto(59.892147, 30.194669));
		result.add(new MapRegionDto(59.892121, 30.194665));
		result.add(new MapRegionDto(59.892094, 30.19466));
		result.add(new MapRegionDto(59.892067, 30.194655));
		result.add(new MapRegionDto(59.89204, 30.194649));
		result.add(new MapRegionDto(59.892013, 30.194642));
		result.add(new MapRegionDto(59.891987, 30.194635));
		result.add(new MapRegionDto(59.89196, 30.194626));
		result.add(new MapRegionDto(59.891934, 30.194617));
		result.add(new MapRegionDto(59.891907, 30.194606));
		result.add(new MapRegionDto(59.891881, 30.194595));
		result.add(new MapRegionDto(59.891855, 30.194582));
		result.add(new MapRegionDto(59.891829, 30.194568));
		result.add(new MapRegionDto(59.891803, 30.194552));
		result.add(new MapRegionDto(59.891777, 30.194536));
		result.add(new MapRegionDto(59.891752, 30.194517));
		result.add(new MapRegionDto(59.891727, 30.194498));
		result.add(new MapRegionDto(59.891702, 30.194477));
		result.add(new MapRegionDto(59.891678, 30.194454));
		result.add(new MapRegionDto(59.891654, 30.19443));
		result.add(new MapRegionDto(59.89163, 30.194405));
		result.add(new MapRegionDto(59.891607, 30.194377));
		result.add(new MapRegionDto(59.891584, 30.194348));
		result.add(new MapRegionDto(59.891562, 30.194318));
		result.add(new MapRegionDto(59.891541, 30.194286));
		result.add(new MapRegionDto(59.891519, 30.194253));
		result.add(new MapRegionDto(59.891499, 30.194219));
		result.add(new MapRegionDto(59.891478, 30.194184));
		result.add(new MapRegionDto(59.891458, 30.194148));
		result.add(new MapRegionDto(59.891438, 30.194112));
		result.add(new MapRegionDto(59.891419, 30.194075));
		result.add(new MapRegionDto(59.891399, 30.194038));
		result.add(new MapRegionDto(59.89138, 30.194001));
		result.add(new MapRegionDto(59.891361, 30.193963));
		result.add(new MapRegionDto(59.891342, 30.193925));
		result.add(new MapRegionDto(59.891335, 30.193912));
		result.add(new MapRegionDto(59.891294, 30.193867));
		result.add(new MapRegionDto(59.891271, 30.193839));
		result.add(new MapRegionDto(59.891248, 30.193811));
		result.add(new MapRegionDto(59.891225, 30.193784));
		result.add(new MapRegionDto(59.891202, 30.193757));
		result.add(new MapRegionDto(59.891179, 30.19373));
		result.add(new MapRegionDto(59.891155, 30.193704));
		result.add(new MapRegionDto(59.891131, 30.19368));
		result.add(new MapRegionDto(59.891107, 30.193656));
		result.add(new MapRegionDto(59.891046, 30.193603));
		result.add(new MapRegionDto(59.891033, 30.193592));
		result.add(new MapRegionDto(59.891008, 30.193574));
		result.add(new MapRegionDto(59.890982, 30.193558));
		result.add(new MapRegionDto(59.890956, 30.193544));
		result.add(new MapRegionDto(59.89093, 30.193532));
		result.add(new MapRegionDto(59.890903, 30.193522));
		result.add(new MapRegionDto(59.890877, 30.193514));
		result.add(new MapRegionDto(59.89085, 30.193506));
		result.add(new MapRegionDto(59.890823, 30.193499));
		result.add(new MapRegionDto(59.890796, 30.193493));
		result.add(new MapRegionDto(59.89077, 30.193487));
		result.add(new MapRegionDto(59.890743, 30.19348));
		result.add(new MapRegionDto(59.890716, 30.193472));
		result.add(new MapRegionDto(59.89069, 30.193463));
		result.add(new MapRegionDto(59.890663, 30.193452));
		result.add(new MapRegionDto(59.890637, 30.19344));
		result.add(new MapRegionDto(59.890612, 30.193424));
		result.add(new MapRegionDto(59.890586, 30.193406));
		result.add(new MapRegionDto(59.890561, 30.193387));
		result.add(new MapRegionDto(59.890537, 30.193365));
		result.add(new MapRegionDto(59.890512, 30.193341));
		result.add(new MapRegionDto(59.890489, 30.193316));
		result.add(new MapRegionDto(59.890465, 30.193289));
		result.add(new MapRegionDto(59.890442, 30.193261));
		result.add(new MapRegionDto(59.89042, 30.193232));
		result.add(new MapRegionDto(59.890397, 30.193202));
		result.add(new MapRegionDto(59.890375, 30.193171));
		result.add(new MapRegionDto(59.890353, 30.19314));
		result.add(new MapRegionDto(59.890332, 30.193108));
		result.add(new MapRegionDto(59.89031, 30.193076));
		result.add(new MapRegionDto(59.890289, 30.193044));
		result.add(new MapRegionDto(59.890267, 30.193011));
		result.add(new MapRegionDto(59.890246, 30.192979));
		result.add(new MapRegionDto(59.890224, 30.192947));
		result.add(new MapRegionDto(59.890203, 30.192914));
		result.add(new MapRegionDto(59.890181, 30.192883));
		result.add(new MapRegionDto(59.890159, 30.192851));
		result.add(new MapRegionDto(59.890137, 30.19282));
		result.add(new MapRegionDto(59.890115, 30.19279));
		result.add(new MapRegionDto(59.890093, 30.192761));
		result.add(new MapRegionDto(59.89007, 30.192732));
		result.add(new MapRegionDto(59.890047, 30.192704));
		result.add(new MapRegionDto(59.890024, 30.192677));
		result.add(new MapRegionDto(59.890001, 30.192649));
		result.add(new MapRegionDto(59.889977, 30.192623));
		result.add(new MapRegionDto(59.889954, 30.192596));
		result.add(new MapRegionDto(59.88993, 30.19257));
		result.add(new MapRegionDto(59.889907, 30.192544));
		result.add(new MapRegionDto(59.889883, 30.192518));
		result.add(new MapRegionDto(59.88986, 30.192492));
		result.add(new MapRegionDto(59.889836, 30.192466));
		result.add(new MapRegionDto(59.889813, 30.19244));
		result.add(new MapRegionDto(59.889789, 30.192413));
		result.add(new MapRegionDto(59.889766, 30.192386));
		result.add(new MapRegionDto(59.889743, 30.192358));
		result.add(new MapRegionDto(59.88972, 30.19233));
		result.add(new MapRegionDto(59.889697, 30.192302));
		result.add(new MapRegionDto(59.889675, 30.192272));
		result.add(new MapRegionDto(59.889653, 30.192242));
		result.add(new MapRegionDto(59.889631, 30.192211));
		result.add(new MapRegionDto(59.889609, 30.192179));
		result.add(new MapRegionDto(59.889587, 30.192147));
		result.add(new MapRegionDto(59.889566, 30.192114));
		result.add(new MapRegionDto(59.889545, 30.19208));
		result.add(new MapRegionDto(59.889525, 30.192046));
		result.add(new MapRegionDto(59.889504, 30.192011));
		result.add(new MapRegionDto(59.889484, 30.191976));
		result.add(new MapRegionDto(59.889464, 30.191939));
		result.add(new MapRegionDto(59.889444, 30.191903));
		result.add(new MapRegionDto(59.889425, 30.191866));
		result.add(new MapRegionDto(59.889406, 30.191828));
		result.add(new MapRegionDto(59.889387, 30.19179));
		result.add(new MapRegionDto(59.889368, 30.191752));
		result.add(new MapRegionDto(59.88935, 30.191713));
		result.add(new MapRegionDto(59.889331, 30.191673));
		result.add(new MapRegionDto(59.889313, 30.191634));
		result.add(new MapRegionDto(59.889295, 30.191594));
		result.add(new MapRegionDto(59.889278, 30.191553));
		result.add(new MapRegionDto(59.88926, 30.191512));
		result.add(new MapRegionDto(59.889243, 30.191471));
		result.add(new MapRegionDto(59.889226, 30.19143));
		result.add(new MapRegionDto(59.889209, 30.191388));
		result.add(new MapRegionDto(59.889193, 30.191346));
		result.add(new MapRegionDto(59.889176, 30.191303));
		result.add(new MapRegionDto(59.88916, 30.19126));
		result.add(new MapRegionDto(59.889144, 30.191217));
		result.add(new MapRegionDto(59.889128, 30.191174));
		result.add(new MapRegionDto(59.889113, 30.19113));
		result.add(new MapRegionDto(59.889097, 30.191086));
		result.add(new MapRegionDto(59.889082, 30.191042));
		result.add(new MapRegionDto(59.889067, 30.190997));
		result.add(new MapRegionDto(59.889052, 30.190953));
		result.add(new MapRegionDto(59.889038, 30.190908));
		result.add(new MapRegionDto(59.889023, 30.190862));
		result.add(new MapRegionDto(59.889009, 30.190817));
		result.add(new MapRegionDto(59.888995, 30.190772));
		result.add(new MapRegionDto(59.88898, 30.190726));
		result.add(new MapRegionDto(59.888966, 30.19068));
		result.add(new MapRegionDto(59.888952, 30.190635));
		result.add(new MapRegionDto(59.888938, 30.190589));
		result.add(new MapRegionDto(59.888924, 30.190543));
		result.add(new MapRegionDto(59.88891, 30.190497));
		result.add(new MapRegionDto(59.888897, 30.190451));
		result.add(new MapRegionDto(59.888883, 30.190405));
		result.add(new MapRegionDto(59.88887, 30.190358));
		result.add(new MapRegionDto(59.888857, 30.190311));
		result.add(new MapRegionDto(59.888844, 30.190264));
		result.add(new MapRegionDto(59.888832, 30.190216));
		result.add(new MapRegionDto(59.888821, 30.190168));
		result.add(new MapRegionDto(59.888809, 30.190119));
		result.add(new MapRegionDto(59.888799, 30.19007));
		result.add(new MapRegionDto(59.888789, 30.19002));
		result.add(new MapRegionDto(59.888779, 30.18997));
		result.add(new MapRegionDto(59.888771, 30.189919));
		result.add(new MapRegionDto(59.888763, 30.189868));
		result.add(new MapRegionDto(59.888756, 30.189816));
		result.add(new MapRegionDto(59.88875, 30.189764));
		result.add(new MapRegionDto(59.888744, 30.189711));
		result.add(new MapRegionDto(59.888739, 30.189659));
		result.add(new MapRegionDto(59.888735, 30.189606));
		result.add(new MapRegionDto(59.888731, 30.189553));
		result.add(new MapRegionDto(59.888727, 30.1895));
		result.add(new MapRegionDto(59.888724, 30.189447));
		result.add(new MapRegionDto(59.888716, 30.189298));
		result.add(new MapRegionDto(59.888715, 30.189287));
		result.add(new MapRegionDto(59.888712, 30.189233));
		result.add(new MapRegionDto(59.88871, 30.18918));
		result.add(new MapRegionDto(59.888707, 30.189127));
		result.add(new MapRegionDto(59.888703, 30.189074));
		result.add(new MapRegionDto(59.8887, 30.189021));
		result.add(new MapRegionDto(59.888696, 30.188968));
		result.add(new MapRegionDto(59.888692, 30.188915));
		result.add(new MapRegionDto(59.888688, 30.188862));
		result.add(new MapRegionDto(59.888684, 30.188809));
		result.add(new MapRegionDto(59.888679, 30.188756));
		result.add(new MapRegionDto(59.888674, 30.188704));
		result.add(new MapRegionDto(59.888669, 30.188651));
		result.add(new MapRegionDto(59.888663, 30.188599));
		result.add(new MapRegionDto(59.888658, 30.188546));
		result.add(new MapRegionDto(59.888652, 30.188494));
		result.add(new MapRegionDto(59.888646, 30.188442));
		result.add(new MapRegionDto(59.88864, 30.188389));
		result.add(new MapRegionDto(59.888634, 30.188337));
		result.add(new MapRegionDto(59.888628, 30.188285));
		result.add(new MapRegionDto(59.888621, 30.188233));
		result.add(new MapRegionDto(59.888615, 30.188181));
		result.add(new MapRegionDto(59.888608, 30.188129));
		result.add(new MapRegionDto(59.888602, 30.188078));
		result.add(new MapRegionDto(59.888595, 30.188026));
		result.add(new MapRegionDto(59.888588, 30.187974));
		result.add(new MapRegionDto(59.888582, 30.187922));
		result.add(new MapRegionDto(59.888575, 30.18787));
		result.add(new MapRegionDto(59.888569, 30.187818));
		result.add(new MapRegionDto(59.888562, 30.187766));
		result.add(new MapRegionDto(59.888556, 30.187714));
		result.add(new MapRegionDto(59.88855, 30.187662));
		result.add(new MapRegionDto(59.888544, 30.18761));
		result.add(new MapRegionDto(59.888537, 30.187558));
		result.add(new MapRegionDto(59.888531, 30.187506));
		result.add(new MapRegionDto(59.888525, 30.187453));
		result.add(new MapRegionDto(59.888519, 30.187401));
		result.add(new MapRegionDto(59.888513, 30.187349));
		result.add(new MapRegionDto(59.888494, 30.187193));
		result.add(new MapRegionDto(59.888488, 30.18714));
		result.add(new MapRegionDto(59.888482, 30.187088));
		result.add(new MapRegionDto(59.888476, 30.187036));
		result.add(new MapRegionDto(59.88847, 30.186984));
		result.add(new MapRegionDto(59.888463, 30.186932));
		result.add(new MapRegionDto(59.888457, 30.18688));
		result.add(new MapRegionDto(59.888451, 30.186828));
		result.add(new MapRegionDto(59.888445, 30.186775));
		result.add(new MapRegionDto(59.888438, 30.186723));
		result.add(new MapRegionDto(59.888432, 30.186671));
		result.add(new MapRegionDto(59.888425, 30.186619));
		result.add(new MapRegionDto(59.888419, 30.186567));
		result.add(new MapRegionDto(59.888412, 30.186515));
		result.add(new MapRegionDto(59.888406, 30.186464));
		result.add(new MapRegionDto(59.888399, 30.186412));
		result.add(new MapRegionDto(59.888392, 30.18636));
		result.add(new MapRegionDto(59.888385, 30.186308));
		result.add(new MapRegionDto(59.888378, 30.186256));
		result.add(new MapRegionDto(59.888371, 30.186205));
		result.add(new MapRegionDto(59.888364, 30.186153));
		result.add(new MapRegionDto(59.888357, 30.186101));
		result.add(new MapRegionDto(59.88835, 30.186049));
		result.add(new MapRegionDto(59.888343, 30.185998));
		result.add(new MapRegionDto(59.888335, 30.185946));
		result.add(new MapRegionDto(59.888328, 30.185895));
		result.add(new MapRegionDto(59.888321, 30.185843));
		result.add(new MapRegionDto(59.888313, 30.185792));
		result.add(new MapRegionDto(59.888306, 30.18574));
		result.add(new MapRegionDto(59.888298, 30.185689));
		result.add(new MapRegionDto(59.888291, 30.185637));
		result.add(new MapRegionDto(59.888283, 30.185586));
		result.add(new MapRegionDto(59.888276, 30.185534));
		result.add(new MapRegionDto(59.888268, 30.185483));
		result.add(new MapRegionDto(59.888261, 30.185431));
		result.add(new MapRegionDto(59.888253, 30.18538));
		result.add(new MapRegionDto(59.888245, 30.185329));
		result.add(new MapRegionDto(59.888238, 30.185277));
		result.add(new MapRegionDto(59.88823, 30.185226));
		result.add(new MapRegionDto(59.888223, 30.185175));
		result.add(new MapRegionDto(59.888218, 30.185143));
		result.add(new MapRegionDto(59.888209, 30.185093));
		result.add(new MapRegionDto(59.8882, 30.185042));
		result.add(new MapRegionDto(59.888192, 30.184991));
		result.add(new MapRegionDto(59.888183, 30.18494));
		result.add(new MapRegionDto(59.888175, 30.184889));
		result.add(new MapRegionDto(59.888167, 30.184838));
		result.add(new MapRegionDto(59.888159, 30.184787));
		result.add(new MapRegionDto(59.888151, 30.184736));
		result.add(new MapRegionDto(59.888143, 30.184685));
		result.add(new MapRegionDto(59.888135, 30.184634));
		result.add(new MapRegionDto(59.888127, 30.184582));
		result.add(new MapRegionDto(59.888119, 30.184531));
		result.add(new MapRegionDto(59.888112, 30.18448));
		result.add(new MapRegionDto(59.888104, 30.184428));
		result.add(new MapRegionDto(59.888097, 30.184377));
		result.add(new MapRegionDto(59.888089, 30.184325));
		result.add(new MapRegionDto(59.888082, 30.184274));
		result.add(new MapRegionDto(59.888075, 30.184222));
		result.add(new MapRegionDto(59.888067, 30.184171));
		result.add(new MapRegionDto(59.88806, 30.184119));
		result.add(new MapRegionDto(59.888053, 30.184067));
		result.add(new MapRegionDto(59.888046, 30.184016));
		result.add(new MapRegionDto(59.888039, 30.183964));
		result.add(new MapRegionDto(59.888032, 30.183912));
		result.add(new MapRegionDto(59.888025, 30.18386));
		result.add(new MapRegionDto(59.888019, 30.183809));
		result.add(new MapRegionDto(59.888012, 30.183757));
		result.add(new MapRegionDto(59.888005, 30.183705));
		result.add(new MapRegionDto(59.887998, 30.183653));
		result.add(new MapRegionDto(59.887992, 30.183601));
		result.add(new MapRegionDto(59.887985, 30.183549));
		result.add(new MapRegionDto(59.887978, 30.183497));
		result.add(new MapRegionDto(59.887972, 30.183445));
		result.add(new MapRegionDto(59.887965, 30.183393));
		result.add(new MapRegionDto(59.887952, 30.183289));
		result.add(new MapRegionDto(59.887946, 30.183238));
		result.add(new MapRegionDto(59.887933, 30.183134));
		result.add(new MapRegionDto(59.887926, 30.183082));
		result.add(new MapRegionDto(59.88792, 30.18303));
		result.add(new MapRegionDto(59.887916, 30.183013));
		result.add(new MapRegionDto(59.887913, 30.182997));
		result.add(new MapRegionDto(59.887909, 30.18298));
		result.add(new MapRegionDto(59.887906, 30.182964));
		result.add(new MapRegionDto(59.887903, 30.182947));
		result.add(new MapRegionDto(59.887899, 30.18293));
		result.add(new MapRegionDto(59.887896, 30.182914));
		result.add(new MapRegionDto(59.887893, 30.182897));
		result.add(new MapRegionDto(59.88789, 30.18288));
		result.add(new MapRegionDto(59.887887, 30.182863));
		result.add(new MapRegionDto(59.887884, 30.182846));
		result.add(new MapRegionDto(59.887882, 30.182829));
		result.add(new MapRegionDto(59.887879, 30.182812));
		result.add(new MapRegionDto(59.887876, 30.182795));
		result.add(new MapRegionDto(59.887874, 30.182778));
		result.add(new MapRegionDto(59.887871, 30.182761));
		result.add(new MapRegionDto(59.887869, 30.182744));
		result.add(new MapRegionDto(59.887867, 30.182726));
		result.add(new MapRegionDto(59.887864, 30.182709));
		result.add(new MapRegionDto(59.887862, 30.182692));
		result.add(new MapRegionDto(59.88786, 30.182674));
		result.add(new MapRegionDto(59.887858, 30.182657));
		result.add(new MapRegionDto(59.887856, 30.182639));
		result.add(new MapRegionDto(59.887854, 30.182622));
		result.add(new MapRegionDto(59.887852, 30.182605));
		result.add(new MapRegionDto(59.887851, 30.182587));
		result.add(new MapRegionDto(59.887849, 30.182569));
		result.add(new MapRegionDto(59.887847, 30.182552));
		result.add(new MapRegionDto(59.887846, 30.182534));
		result.add(new MapRegionDto(59.887844, 30.182517));
		result.add(new MapRegionDto(59.887843, 30.182499));
		result.add(new MapRegionDto(59.887841, 30.182481));
		result.add(new MapRegionDto(59.88784, 30.182464));
		result.add(new MapRegionDto(59.887839, 30.182446));
		result.add(new MapRegionDto(59.887838, 30.182428));
		result.add(new MapRegionDto(59.887836, 30.182411));
		result.add(new MapRegionDto(59.887835, 30.182393));
		result.add(new MapRegionDto(59.887834, 30.182375));
		result.add(new MapRegionDto(59.887833, 30.182357));
		result.add(new MapRegionDto(59.887832, 30.18234));
		result.add(new MapRegionDto(59.887832, 30.182322));
		result.add(new MapRegionDto(59.887831, 30.182304));
		result.add(new MapRegionDto(59.88783, 30.182286));
		result.add(new MapRegionDto(59.887829, 30.182269));
		result.add(new MapRegionDto(59.887828, 30.182251));
		result.add(new MapRegionDto(59.887828, 30.182233));
		result.add(new MapRegionDto(59.887827, 30.182215));
		result.add(new MapRegionDto(59.887826, 30.182197));
		result.add(new MapRegionDto(59.887826, 30.18218));
		result.add(new MapRegionDto(59.887825, 30.182162));
		result.add(new MapRegionDto(59.887825, 30.182144));
		result.add(new MapRegionDto(59.887824, 30.182126));
		result.add(new MapRegionDto(59.887824, 30.182108));
		result.add(new MapRegionDto(59.887824, 30.18209));
		result.add(new MapRegionDto(59.887823, 30.182072));
		result.add(new MapRegionDto(59.887823, 30.182055));
		result.add(new MapRegionDto(59.887823, 30.182037));
		result.add(new MapRegionDto(59.887822, 30.182019));
		result.add(new MapRegionDto(59.887822, 30.182001));
		result.add(new MapRegionDto(59.887822, 30.181983));
		result.add(new MapRegionDto(59.887822, 30.181965));
		result.add(new MapRegionDto(59.887822, 30.181947));
		result.add(new MapRegionDto(59.887822, 30.18193));
		result.add(new MapRegionDto(59.887822, 30.181912));
		result.add(new MapRegionDto(59.887821, 30.181894));
		result.add(new MapRegionDto(59.887821, 30.181876));
		result.add(new MapRegionDto(59.887821, 30.181858));
		result.add(new MapRegionDto(59.887821, 30.18184));
		result.add(new MapRegionDto(59.887821, 30.181822));
		result.add(new MapRegionDto(59.887821, 30.181805));
		result.add(new MapRegionDto(59.887821, 30.181787));
		result.add(new MapRegionDto(59.887821, 30.181751));
		result.add(new MapRegionDto(59.887821, 30.181733));
		result.add(new MapRegionDto(59.887822, 30.181715));
		result.add(new MapRegionDto(59.887822, 30.181577));
		result.add(new MapRegionDto(59.887823, 30.181572));
		result.add(new MapRegionDto(59.887823, 30.181555));
		result.add(new MapRegionDto(59.887823, 30.181501));
		result.add(new MapRegionDto(59.887823, 30.181483));
		result.add(new MapRegionDto(59.887823, 30.181465));
		result.add(new MapRegionDto(59.887824, 30.18143));
		result.add(new MapRegionDto(59.887824, 30.181412));
		result.add(new MapRegionDto(59.887824, 30.181394));
		result.add(new MapRegionDto(59.887824, 30.181358));
		result.add(new MapRegionDto(59.887824, 30.181322));
		result.add(new MapRegionDto(59.887824, 30.181305));
		result.add(new MapRegionDto(59.887825, 30.181287));
		result.add(new MapRegionDto(59.887825, 30.181269));
		result.add(new MapRegionDto(59.887825, 30.181233));
		result.add(new MapRegionDto(59.887825, 30.181215));
		result.add(new MapRegionDto(59.887825, 30.181197));
		result.add(new MapRegionDto(59.887825, 30.18118));
		result.add(new MapRegionDto(59.887825, 30.181162));
		result.add(new MapRegionDto(59.887825, 30.181144));
		result.add(new MapRegionDto(59.887825, 30.181126));
		result.add(new MapRegionDto(59.887825, 30.181108));
		result.add(new MapRegionDto(59.887824, 30.18109));
		result.add(new MapRegionDto(59.887824, 30.181072));
		result.add(new MapRegionDto(59.887824, 30.181054));
		result.add(new MapRegionDto(59.887824, 30.181037));
		result.add(new MapRegionDto(59.887824, 30.181019));
		result.add(new MapRegionDto(59.887824, 30.181001));
		result.add(new MapRegionDto(59.887823, 30.180983));
		result.add(new MapRegionDto(59.887823, 30.180965));
		result.add(new MapRegionDto(59.887823, 30.180947));
		result.add(new MapRegionDto(59.887823, 30.180929));
		result.add(new MapRegionDto(59.887822, 30.180894));
		result.add(new MapRegionDto(59.887821, 30.180858));
		result.add(new MapRegionDto(59.887821, 30.18084));
		result.add(new MapRegionDto(59.887821, 30.180822));
		result.add(new MapRegionDto(59.88782, 30.180805));
		result.add(new MapRegionDto(59.88782, 30.180787));
		result.add(new MapRegionDto(59.88782, 30.180769));
		result.add(new MapRegionDto(59.887819, 30.180751));
		result.add(new MapRegionDto(59.887819, 30.180733));
		result.add(new MapRegionDto(59.887818, 30.180698));
		result.add(new MapRegionDto(59.887817, 30.18068));
		result.add(new MapRegionDto(59.887817, 30.180662));
		result.add(new MapRegionDto(59.887816, 30.180644));
		result.add(new MapRegionDto(59.887816, 30.180626));
		result.add(new MapRegionDto(59.887815, 30.180608));
		result.add(new MapRegionDto(59.887815, 30.180591));
		result.add(new MapRegionDto(59.887814, 30.180573));
		result.add(new MapRegionDto(59.887813, 30.180555));
		result.add(new MapRegionDto(59.887813, 30.180537));
		result.add(new MapRegionDto(59.887812, 30.180519));
		result.add(new MapRegionDto(59.887812, 30.180501));
		result.add(new MapRegionDto(59.887811, 30.180484));
		result.add(new MapRegionDto(59.88781, 30.180466));
		result.add(new MapRegionDto(59.887808, 30.180412));
		result.add(new MapRegionDto(59.887807, 30.180377));
		result.add(new MapRegionDto(59.887806, 30.180359));
		result.add(new MapRegionDto(59.887806, 30.180341));
		result.add(new MapRegionDto(59.887805, 30.180323));
		result.add(new MapRegionDto(59.887804, 30.180306));
		result.add(new MapRegionDto(59.887803, 30.180288));
		result.add(new MapRegionDto(59.887803, 30.18027));
		result.add(new MapRegionDto(59.887802, 30.180252));
		result.add(new MapRegionDto(59.887801, 30.180234));
		result.add(new MapRegionDto(59.8878, 30.180217));
		result.add(new MapRegionDto(59.887799, 30.180199));
		result.add(new MapRegionDto(59.887799, 30.180181));
		result.add(new MapRegionDto(59.887798, 30.180163));
		result.add(new MapRegionDto(59.887796, 30.180128));
		result.add(new MapRegionDto(59.887795, 30.18011));
		result.add(new MapRegionDto(59.887795, 30.180092));
		result.add(new MapRegionDto(59.887793, 30.180057));
		result.add(new MapRegionDto(59.887791, 30.180021));
		result.add(new MapRegionDto(59.887789, 30.179968));
		result.add(new MapRegionDto(59.887787, 30.179932));
		result.add(new MapRegionDto(59.887785, 30.179897));
		result.add(new MapRegionDto(59.887783, 30.179861));
		result.add(new MapRegionDto(59.887782, 30.179843));
		result.add(new MapRegionDto(59.887781, 30.179825));
		result.add(new MapRegionDto(59.88778, 30.179808));
		result.add(new MapRegionDto(59.88778, 30.17979));
		result.add(new MapRegionDto(59.887776, 30.179719));
		result.add(new MapRegionDto(59.887775, 30.179701));
		result.add(new MapRegionDto(59.887773, 30.179666));
		result.add(new MapRegionDto(59.887772, 30.179648));
		result.add(new MapRegionDto(59.887771, 30.17963));
		result.add(new MapRegionDto(59.88777, 30.179612));
		result.add(new MapRegionDto(59.887769, 30.179594));
		result.add(new MapRegionDto(59.887767, 30.179541));
		result.add(new MapRegionDto(59.887766, 30.179523));
		result.add(new MapRegionDto(59.887764, 30.179488));
		result.add(new MapRegionDto(59.887763, 30.17947));
		result.add(new MapRegionDto(59.887762, 30.179452));
		result.add(new MapRegionDto(59.887761, 30.179435));
		result.add(new MapRegionDto(59.88776, 30.179417));
		result.add(new MapRegionDto(59.887759, 30.179399));
		result.add(new MapRegionDto(59.887758, 30.179381));
		result.add(new MapRegionDto(59.887757, 30.179364));
		result.add(new MapRegionDto(59.887757, 30.179346));
		result.add(new MapRegionDto(59.887755, 30.17931));
		result.add(new MapRegionDto(59.887754, 30.179292));
		result.add(new MapRegionDto(59.887753, 30.179275));
		result.add(new MapRegionDto(59.887751, 30.179239));
		result.add(new MapRegionDto(59.887749, 30.179186));
		result.add(new MapRegionDto(59.887747, 30.17915));
		result.add(new MapRegionDto(59.887746, 30.179132));
		result.add(new MapRegionDto(59.887744, 30.179097));
		result.add(new MapRegionDto(59.887744, 30.179079));
		result.add(new MapRegionDto(59.887743, 30.179061));
		result.add(new MapRegionDto(59.88774, 30.179008));
		result.add(new MapRegionDto(59.88774, 30.17899));
		result.add(new MapRegionDto(59.887738, 30.178955));
		result.add(new MapRegionDto(59.887737, 30.178937));
		result.add(new MapRegionDto(59.887735, 30.178883));
		result.add(new MapRegionDto(59.887734, 30.178866));
		result.add(new MapRegionDto(59.887733, 30.17883));
		result.add(new MapRegionDto(59.887732, 30.178812));
		result.add(new MapRegionDto(59.887731, 30.178776));
		result.add(new MapRegionDto(59.88773, 30.178741));
		result.add(new MapRegionDto(59.887729, 30.178723));
		result.add(new MapRegionDto(59.887729, 30.178705));
		result.add(new MapRegionDto(59.887728, 30.178687));
		result.add(new MapRegionDto(59.887727, 30.17867));
		result.add(new MapRegionDto(59.887727, 30.178652));
		result.add(new MapRegionDto(59.887726, 30.178634));
		result.add(new MapRegionDto(59.887726, 30.178616));
		result.add(new MapRegionDto(59.887725, 30.178598));
		result.add(new MapRegionDto(59.887725, 30.17858));
		result.add(new MapRegionDto(59.887724, 30.178563));
		result.add(new MapRegionDto(59.887724, 30.178545));
		result.add(new MapRegionDto(59.887723, 30.178527));
		result.add(new MapRegionDto(59.887722, 30.178491));
		result.add(new MapRegionDto(59.887722, 30.178473));
		result.add(new MapRegionDto(59.887722, 30.178456));
		result.add(new MapRegionDto(59.887721, 30.178438));
		result.add(new MapRegionDto(59.887721, 30.17842));
		result.add(new MapRegionDto(59.88772, 30.178402));
		result.add(new MapRegionDto(59.88772, 30.178384));
		result.add(new MapRegionDto(59.88772, 30.178366));
		result.add(new MapRegionDto(59.88772, 30.178348));
		result.add(new MapRegionDto(59.887719, 30.178331));
		result.add(new MapRegionDto(59.887719, 30.178313));
		result.add(new MapRegionDto(59.887719, 30.178295));
		result.add(new MapRegionDto(59.887718, 30.178277));
		result.add(new MapRegionDto(59.887718, 30.178241));
		result.add(new MapRegionDto(59.887718, 30.178223));
		result.add(new MapRegionDto(59.887718, 30.178206));
		result.add(new MapRegionDto(59.887717, 30.178188));
		result.add(new MapRegionDto(59.887717, 30.17817));
		result.add(new MapRegionDto(59.887717, 30.178152));
		result.add(new MapRegionDto(59.887717, 30.178116));
		result.add(new MapRegionDto(59.887717, 30.178098));
		result.add(new MapRegionDto(59.887717, 30.178081));
		result.add(new MapRegionDto(59.887717, 30.178063));
		result.add(new MapRegionDto(59.887717, 30.178045));
		result.add(new MapRegionDto(59.887717, 30.178009));
		result.add(new MapRegionDto(59.887717, 30.177991));
		result.add(new MapRegionDto(59.887717, 30.177973));
		result.add(new MapRegionDto(59.887717, 30.177956));
		result.add(new MapRegionDto(59.887717, 30.177938));
		result.add(new MapRegionDto(59.887717, 30.177902));
		result.add(new MapRegionDto(59.887717, 30.177884));
		result.add(new MapRegionDto(59.887717, 30.177866));
		result.add(new MapRegionDto(59.887717, 30.177848));
		result.add(new MapRegionDto(59.887717, 30.177831));
		result.add(new MapRegionDto(59.887717, 30.177813));
		result.add(new MapRegionDto(59.887717, 30.177795));
		result.add(new MapRegionDto(59.887717, 30.177777));
		result.add(new MapRegionDto(59.887718, 30.177741));
		result.add(new MapRegionDto(59.887718, 30.177723));
		result.add(new MapRegionDto(59.887718, 30.177706));
		result.add(new MapRegionDto(59.887718, 30.17767));
		result.add(new MapRegionDto(59.887719, 30.177652));
		result.add(new MapRegionDto(59.887719, 30.177634));
		result.add(new MapRegionDto(59.887719, 30.177616));
		result.add(new MapRegionDto(59.887719, 30.177598));
		result.add(new MapRegionDto(59.88772, 30.177581));
		result.add(new MapRegionDto(59.88772, 30.177545));
		result.add(new MapRegionDto(59.887721, 30.177527));
		result.add(new MapRegionDto(59.887721, 30.177491));
		result.add(new MapRegionDto(59.887722, 30.177456));
		result.add(new MapRegionDto(59.887722, 30.177438));
		result.add(new MapRegionDto(59.887723, 30.17742));
		result.add(new MapRegionDto(59.887723, 30.177384));
		result.add(new MapRegionDto(59.887724, 30.177366));
		result.add(new MapRegionDto(59.887724, 30.177349));
		result.add(new MapRegionDto(59.887724, 30.177331));
		result.add(new MapRegionDto(59.887725, 30.177313));
		result.add(new MapRegionDto(59.887725, 30.177295));
		result.add(new MapRegionDto(59.887726, 30.177259));
		result.add(new MapRegionDto(59.887727, 30.177242));
		result.add(new MapRegionDto(59.887727, 30.177224));
		result.add(new MapRegionDto(59.887728, 30.177206));
		result.add(new MapRegionDto(59.887728, 30.177188));
		result.add(new MapRegionDto(59.887729, 30.17717));
		result.add(new MapRegionDto(59.887729, 30.177152));
		result.add(new MapRegionDto(59.88773, 30.177135));
		result.add(new MapRegionDto(59.887731, 30.177081));
		result.add(new MapRegionDto(59.887732, 30.177045));
		result.add(new MapRegionDto(59.887734, 30.176992));
		result.add(new MapRegionDto(59.887735, 30.176974));
		result.add(new MapRegionDto(59.887735, 30.176956));
		result.add(new MapRegionDto(59.887736, 30.176938));
		result.add(new MapRegionDto(59.887736, 30.176921));
		result.add(new MapRegionDto(59.887737, 30.176903));
		result.add(new MapRegionDto(59.887738, 30.176885));
		result.add(new MapRegionDto(59.887738, 30.176867));
		result.add(new MapRegionDto(59.887739, 30.176849));
		result.add(new MapRegionDto(59.887739, 30.176832));
		result.add(new MapRegionDto(59.88774, 30.176814));
		result.add(new MapRegionDto(59.887741, 30.176796));
		result.add(new MapRegionDto(59.887741, 30.176778));
		result.add(new MapRegionDto(59.887742, 30.17676));
		result.add(new MapRegionDto(59.887743, 30.176742));
		result.add(new MapRegionDto(59.887743, 30.176725));
		result.add(new MapRegionDto(59.887744, 30.176707));
		result.add(new MapRegionDto(59.887745, 30.176671));
		result.add(new MapRegionDto(59.887746, 30.176653));
		result.add(new MapRegionDto(59.887748, 30.1766));
		result.add(new MapRegionDto(59.887749, 30.176582));
		result.add(new MapRegionDto(59.88775, 30.176564));
		result.add(new MapRegionDto(59.88775, 30.176547));
		result.add(new MapRegionDto(59.887752, 30.176511));
		result.add(new MapRegionDto(59.887753, 30.176493));
		result.add(new MapRegionDto(59.887753, 30.176475));
		result.add(new MapRegionDto(59.887754, 30.176458));
		result.add(new MapRegionDto(59.887755, 30.17644));
		result.add(new MapRegionDto(59.887756, 30.176422));
		result.add(new MapRegionDto(59.887756, 30.176404));
		result.add(new MapRegionDto(59.887757, 30.176386));
		result.add(new MapRegionDto(59.887758, 30.176369));
		result.add(new MapRegionDto(59.88776, 30.176315));
		result.add(new MapRegionDto(59.887761, 30.176297));
		result.add(new MapRegionDto(59.887762, 30.17628));
		result.add(new MapRegionDto(59.887763, 30.176262));
		result.add(new MapRegionDto(59.887763, 30.176244));
		result.add(new MapRegionDto(59.887764, 30.176226));
		result.add(new MapRegionDto(59.887767, 30.176173));
		result.add(new MapRegionDto(59.88777, 30.176102));
		result.add(new MapRegionDto(59.887772, 30.176048));
		result.add(new MapRegionDto(59.887773, 30.176031));
		result.add(new MapRegionDto(59.887774, 30.176013));
		result.add(new MapRegionDto(59.887775, 30.175995));
		result.add(new MapRegionDto(59.887776, 30.175977));
		result.add(new MapRegionDto(59.887777, 30.175959));
		result.add(new MapRegionDto(59.887777, 30.175942));
		result.add(new MapRegionDto(59.887779, 30.175906));
		result.add(new MapRegionDto(59.88778, 30.175888));
		result.add(new MapRegionDto(59.887781, 30.175871));
		result.add(new MapRegionDto(59.887782, 30.175853));
		result.add(new MapRegionDto(59.887783, 30.175835));
		result.add(new MapRegionDto(59.887784, 30.1758));
		result.add(new MapRegionDto(59.887785, 30.175782));
		result.add(new MapRegionDto(59.887786, 30.175764));
		result.add(new MapRegionDto(59.887788, 30.175728));
		result.add(new MapRegionDto(59.88779, 30.175693));
		result.add(new MapRegionDto(59.887791, 30.175675));
		result.add(new MapRegionDto(59.887797, 30.175563));
		result.add(new MapRegionDto(59.887797, 30.175551));
		result.add(new MapRegionDto(59.887798, 30.175533));
		result.add(new MapRegionDto(59.887799, 30.175515));
		result.add(new MapRegionDto(59.8878, 30.175498));
		result.add(new MapRegionDto(59.887801, 30.17548));
		result.add(new MapRegionDto(59.887804, 30.175444));
		result.add(new MapRegionDto(59.887805, 30.175427));
		result.add(new MapRegionDto(59.887806, 30.175409));
		result.add(new MapRegionDto(59.887808, 30.175373));
		result.add(new MapRegionDto(59.887809, 30.175356));
		result.add(new MapRegionDto(59.887811, 30.17532));
		result.add(new MapRegionDto(59.887812, 30.175303));
		result.add(new MapRegionDto(59.887813, 30.175285));
		result.add(new MapRegionDto(59.887815, 30.175267));
		result.add(new MapRegionDto(59.887817, 30.175232));
		result.add(new MapRegionDto(59.887818, 30.175214));
		result.add(new MapRegionDto(59.887819, 30.175196));
		result.add(new MapRegionDto(59.88782, 30.175179));
		result.add(new MapRegionDto(59.887822, 30.175161));
		result.add(new MapRegionDto(59.887824, 30.175125));
		result.add(new MapRegionDto(59.887827, 30.17509));
		result.add(new MapRegionDto(59.887828, 30.175072));
		result.add(new MapRegionDto(59.887829, 30.175055));
		result.add(new MapRegionDto(59.887831, 30.175037));
		result.add(new MapRegionDto(59.887832, 30.175019));
		result.add(new MapRegionDto(59.887833, 30.175002));
		result.add(new MapRegionDto(59.887835, 30.174984));
		result.add(new MapRegionDto(59.887837, 30.174949));
		result.add(new MapRegionDto(59.887839, 30.174931));
		result.add(new MapRegionDto(59.88784, 30.174914));
		result.add(new MapRegionDto(59.887842, 30.174896));
		result.add(new MapRegionDto(59.887843, 30.174878));
		result.add(new MapRegionDto(59.887846, 30.174843));
		result.add(new MapRegionDto(59.887848, 30.174825));
		result.add(new MapRegionDto(59.887849, 30.174808));
		result.add(new MapRegionDto(59.887851, 30.17479));
		result.add(new MapRegionDto(59.887852, 30.174773));
		result.add(new MapRegionDto(59.887854, 30.174755));
		result.add(new MapRegionDto(59.887856, 30.174738));
		result.add(new MapRegionDto(59.887857, 30.17472));
		result.add(new MapRegionDto(59.887859, 30.174702));
		result.add(new MapRegionDto(59.88786, 30.174685));
		result.add(new MapRegionDto(59.887862, 30.174667));
		result.add(new MapRegionDto(59.887864, 30.17465));
		result.add(new MapRegionDto(59.887866, 30.174632));
		result.add(new MapRegionDto(59.887867, 30.174615));
		result.add(new MapRegionDto(59.887869, 30.174597));
		result.add(new MapRegionDto(59.887871, 30.17458));
		result.add(new MapRegionDto(59.887873, 30.174562));
		result.add(new MapRegionDto(59.887874, 30.174545));
		result.add(new MapRegionDto(59.887876, 30.174527));
		result.add(new MapRegionDto(59.887878, 30.17451));
		result.add(new MapRegionDto(59.88788, 30.174492));
		result.add(new MapRegionDto(59.887882, 30.174475));
		result.add(new MapRegionDto(59.887884, 30.174457));
		result.add(new MapRegionDto(59.887886, 30.17444));
		result.add(new MapRegionDto(59.887888, 30.174422));
		result.add(new MapRegionDto(59.88789, 30.174405));
		result.add(new MapRegionDto(59.887892, 30.174388));
		result.add(new MapRegionDto(59.887894, 30.17437));
		result.add(new MapRegionDto(59.887896, 30.174353));
		result.add(new MapRegionDto(59.887898, 30.174335));
		result.add(new MapRegionDto(59.8879, 30.174318));
		result.add(new MapRegionDto(59.887902, 30.174301));
		result.add(new MapRegionDto(59.887904, 30.174283));
		result.add(new MapRegionDto(59.887906, 30.174266));
		result.add(new MapRegionDto(59.887908, 30.174249));
		result.add(new MapRegionDto(59.887911, 30.174231));
		result.add(new MapRegionDto(59.887913, 30.174214));
		result.add(new MapRegionDto(59.887915, 30.174197));
		result.add(new MapRegionDto(59.887917, 30.17418));
		result.add(new MapRegionDto(59.88792, 30.174162));
		result.add(new MapRegionDto(59.887922, 30.174145));
		result.add(new MapRegionDto(59.887924, 30.174128));
		result.add(new MapRegionDto(59.887927, 30.174111));
		result.add(new MapRegionDto(59.887929, 30.174093));
		result.add(new MapRegionDto(59.887932, 30.174076));
		result.add(new MapRegionDto(59.887934, 30.174059));
		result.add(new MapRegionDto(59.887936, 30.174042));
		result.add(new MapRegionDto(59.887939, 30.174025));
		result.add(new MapRegionDto(59.887942, 30.174008));
		result.add(new MapRegionDto(59.887944, 30.17399));
		result.add(new MapRegionDto(59.887947, 30.173973));
		result.add(new MapRegionDto(59.887949, 30.173956));
		result.add(new MapRegionDto(59.887952, 30.173939));
		result.add(new MapRegionDto(59.887955, 30.173922));
		result.add(new MapRegionDto(59.887957, 30.173905));
		result.add(new MapRegionDto(59.88796, 30.173888));
		result.add(new MapRegionDto(59.887963, 30.173871));
		result.add(new MapRegionDto(59.887965, 30.173854));
		result.add(new MapRegionDto(59.887968, 30.173837));
		result.add(new MapRegionDto(59.887971, 30.17382));
		result.add(new MapRegionDto(59.887974, 30.173803));
		result.add(new MapRegionDto(59.887977, 30.173786));
		result.add(new MapRegionDto(59.88798, 30.173769));
		result.add(new MapRegionDto(59.887983, 30.173752));
		result.add(new MapRegionDto(59.887986, 30.173736));
		result.add(new MapRegionDto(59.887989, 30.173719));
		result.add(new MapRegionDto(59.887992, 30.173702));
		result.add(new MapRegionDto(59.887995, 30.173685));
		result.add(new MapRegionDto(59.887998, 30.173668));
		result.add(new MapRegionDto(59.888001, 30.173652));
		result.add(new MapRegionDto(59.888004, 30.173635));
		result.add(new MapRegionDto(59.888007, 30.173618));
		result.add(new MapRegionDto(59.88801, 30.173601));
		result.add(new MapRegionDto(59.888013, 30.173584));
		result.add(new MapRegionDto(59.888016, 30.173568));
		result.add(new MapRegionDto(59.888019, 30.173551));
		result.add(new MapRegionDto(59.888023, 30.173534));
		result.add(new MapRegionDto(59.888026, 30.173518));
		result.add(new MapRegionDto(59.888029, 30.173501));
		result.add(new MapRegionDto(59.888032, 30.173484));
		result.add(new MapRegionDto(59.888035, 30.173468));
		result.add(new MapRegionDto(59.888039, 30.173451));
		result.add(new MapRegionDto(59.888042, 30.173434));
		result.add(new MapRegionDto(59.888045, 30.173418));
		result.add(new MapRegionDto(59.888049, 30.173401));
		result.add(new MapRegionDto(59.888052, 30.173385));
		result.add(new MapRegionDto(59.888055, 30.173368));
		result.add(new MapRegionDto(59.888059, 30.173351));
		result.add(new MapRegionDto(59.888062, 30.173335));
		result.add(new MapRegionDto(59.888065, 30.173318));
		result.add(new MapRegionDto(59.888069, 30.173302));
		result.add(new MapRegionDto(59.888075, 30.173268));
		result.add(new MapRegionDto(59.888079, 30.173252));
		result.add(new MapRegionDto(59.888082, 30.173235));
		result.add(new MapRegionDto(59.888089, 30.173202));
		result.add(new MapRegionDto(59.888092, 30.173186));
		result.add(new MapRegionDto(59.888096, 30.173169));
		result.add(new MapRegionDto(59.888099, 30.173153));
		result.add(new MapRegionDto(59.888102, 30.173136));
		result.add(new MapRegionDto(59.888109, 30.173103));
		result.add(new MapRegionDto(59.888113, 30.173087));
		result.add(new MapRegionDto(59.888116, 30.17307));
		result.add(new MapRegionDto(59.888119, 30.173054));
		result.add(new MapRegionDto(59.888123, 30.173037));
		result.add(new MapRegionDto(59.88813, 30.173004));
		result.add(new MapRegionDto(59.888133, 30.172987));
		result.add(new MapRegionDto(59.888136, 30.172971));
		result.add(new MapRegionDto(59.88814, 30.172954));
		result.add(new MapRegionDto(59.888143, 30.172938));
		result.add(new MapRegionDto(59.888147, 30.172921));
		result.add(new MapRegionDto(59.88815, 30.172905));
		result.add(new MapRegionDto(59.888153, 30.172888));
		result.add(new MapRegionDto(59.888157, 30.172872));
		result.add(new MapRegionDto(59.888163, 30.172838));
		result.add(new MapRegionDto(59.888167, 30.172822));
		result.add(new MapRegionDto(59.88817, 30.172805));
		result.add(new MapRegionDto(59.888173, 30.172789));
		result.add(new MapRegionDto(59.88818, 30.172755));
		result.add(new MapRegionDto(59.888183, 30.172739));
		result.add(new MapRegionDto(59.888186, 30.172722));
		result.add(new MapRegionDto(59.88819, 30.172706));
		result.add(new MapRegionDto(59.888193, 30.172689));
		result.add(new MapRegionDto(59.888196, 30.172672));
		result.add(new MapRegionDto(59.888199, 30.172656));
		result.add(new MapRegionDto(59.888203, 30.172639));
		result.add(new MapRegionDto(59.888206, 30.172622));
		result.add(new MapRegionDto(59.888209, 30.172605));
		result.add(new MapRegionDto(59.888212, 30.172589));
		result.add(new MapRegionDto(59.888215, 30.172572));
		result.add(new MapRegionDto(59.888218, 30.172555));
		result.add(new MapRegionDto(59.888221, 30.172538));
		result.add(new MapRegionDto(59.888225, 30.172522));
		result.add(new MapRegionDto(59.888228, 30.172505));
		result.add(new MapRegionDto(59.888231, 30.172488));
		result.add(new MapRegionDto(59.888234, 30.172471));
		result.add(new MapRegionDto(59.888237, 30.172454));
		result.add(new MapRegionDto(59.88824, 30.172438));
		result.add(new MapRegionDto(59.888243, 30.172421));
		result.add(new MapRegionDto(59.888245, 30.172404));
		result.add(new MapRegionDto(59.888248, 30.172387));
		result.add(new MapRegionDto(59.888251, 30.17237));
		result.add(new MapRegionDto(59.888254, 30.172353));
		result.add(new MapRegionDto(59.888257, 30.172336));
		result.add(new MapRegionDto(59.88826, 30.172319));
		result.add(new MapRegionDto(59.888262, 30.172302));
		result.add(new MapRegionDto(59.888265, 30.172285));
		result.add(new MapRegionDto(59.888268, 30.172268));
		result.add(new MapRegionDto(59.888271, 30.172251));
		result.add(new MapRegionDto(59.888273, 30.172234));
		result.add(new MapRegionDto(59.888276, 30.172217));
		result.add(new MapRegionDto(59.888278, 30.1722));
		result.add(new MapRegionDto(59.888281, 30.172183));
		result.add(new MapRegionDto(59.888283, 30.172165));
		result.add(new MapRegionDto(59.888286, 30.172148));
		result.add(new MapRegionDto(59.888288, 30.172131));
		result.add(new MapRegionDto(59.888291, 30.172114));
		result.add(new MapRegionDto(59.888293, 30.172097));
		result.add(new MapRegionDto(59.888295, 30.172079));
		result.add(new MapRegionDto(59.888298, 30.172062));
		result.add(new MapRegionDto(59.8883, 30.172045));
		result.add(new MapRegionDto(59.888302, 30.172028));
		result.add(new MapRegionDto(59.888304, 30.17201));
		result.add(new MapRegionDto(59.888307, 30.171993));
		result.add(new MapRegionDto(59.888309, 30.171976));
		result.add(new MapRegionDto(59.888311, 30.171958));
		result.add(new MapRegionDto(59.888313, 30.171941));
		result.add(new MapRegionDto(59.888315, 30.171923));
		result.add(new MapRegionDto(59.888317, 30.171906));
		result.add(new MapRegionDto(59.888318, 30.171888));
		result.add(new MapRegionDto(59.88832, 30.171871));
		result.add(new MapRegionDto(59.888322, 30.171853));
		result.add(new MapRegionDto(59.888324, 30.171836));
		result.add(new MapRegionDto(59.888326, 30.171818));
		result.add(new MapRegionDto(59.888327, 30.171801));
		result.add(new MapRegionDto(59.888329, 30.171783));
		result.add(new MapRegionDto(59.88833, 30.171766));
		result.add(new MapRegionDto(59.888332, 30.171748));
		result.add(new MapRegionDto(59.888333, 30.17173));
		result.add(new MapRegionDto(59.888335, 30.171713));
		result.add(new MapRegionDto(59.888336, 30.171695));
		result.add(new MapRegionDto(59.888337, 30.171677));
		result.add(new MapRegionDto(59.888338, 30.17166));
		result.add(new MapRegionDto(59.88834, 30.171642));
		result.add(new MapRegionDto(59.888341, 30.171624));
		result.add(new MapRegionDto(59.888342, 30.171607));
		result.add(new MapRegionDto(59.888343, 30.171589));
		result.add(new MapRegionDto(59.888344, 30.171571));
		result.add(new MapRegionDto(59.888345, 30.171553));
		result.add(new MapRegionDto(59.888346, 30.171536));
		result.add(new MapRegionDto(59.888347, 30.171518));
		result.add(new MapRegionDto(59.888347, 30.1715));
		result.add(new MapRegionDto(59.888348, 30.171482));
		result.add(new MapRegionDto(59.888349, 30.171464));
		result.add(new MapRegionDto(59.88835, 30.171447));
		result.add(new MapRegionDto(59.88835, 30.171429));
		result.add(new MapRegionDto(59.888351, 30.171411));
		result.add(new MapRegionDto(59.888352, 30.171393));
		result.add(new MapRegionDto(59.888352, 30.171375));
		result.add(new MapRegionDto(59.888353, 30.171357));
		result.add(new MapRegionDto(59.888353, 30.17134));
		result.add(new MapRegionDto(59.888354, 30.171322));
		result.add(new MapRegionDto(59.888354, 30.171304));
		result.add(new MapRegionDto(59.888354, 30.171286));
		result.add(new MapRegionDto(59.888355, 30.171268));
		result.add(new MapRegionDto(59.888355, 30.17125));
		result.add(new MapRegionDto(59.888355, 30.171233));
		result.add(new MapRegionDto(59.888356, 30.171215));
		result.add(new MapRegionDto(59.888356, 30.171197));
		result.add(new MapRegionDto(59.888356, 30.171179));
		result.add(new MapRegionDto(59.888356, 30.171161));
		result.add(new MapRegionDto(59.888356, 30.171143));
		result.add(new MapRegionDto(59.888357, 30.171125));
		result.add(new MapRegionDto(59.888357, 30.171108));
		result.add(new MapRegionDto(59.888357, 30.17109));
		result.add(new MapRegionDto(59.888357, 30.171072));
		result.add(new MapRegionDto(59.888357, 30.171054));
		result.add(new MapRegionDto(59.888357, 30.171036));
		result.add(new MapRegionDto(59.888357, 30.171018));
		result.add(new MapRegionDto(59.888357, 30.171));
		result.add(new MapRegionDto(59.888357, 30.170983));
		result.add(new MapRegionDto(59.888357, 30.170965));
		result.add(new MapRegionDto(59.888357, 30.170947));
		result.add(new MapRegionDto(59.888357, 30.170929));
		result.add(new MapRegionDto(59.888356, 30.170911));
		result.add(new MapRegionDto(59.888356, 30.170893));
		result.add(new MapRegionDto(59.888356, 30.170875));
		result.add(new MapRegionDto(59.888356, 30.170858));
		result.add(new MapRegionDto(59.888356, 30.17084));
		result.add(new MapRegionDto(59.888356, 30.170822));
		result.add(new MapRegionDto(59.888355, 30.170804));
		result.add(new MapRegionDto(59.888355, 30.170786));
		result.add(new MapRegionDto(59.888355, 30.170768));
		result.add(new MapRegionDto(59.888355, 30.17075));
		result.add(new MapRegionDto(59.888354, 30.170715));
		result.add(new MapRegionDto(59.888354, 30.170697));
		result.add(new MapRegionDto(59.888353, 30.170643));
		result.add(new MapRegionDto(59.888353, 30.170625));
		result.add(new MapRegionDto(59.888353, 30.170608));
		result.add(new MapRegionDto(59.888352, 30.17059));
		result.add(new MapRegionDto(59.888352, 30.170572));
		result.add(new MapRegionDto(59.888352, 30.170554));
		result.add(new MapRegionDto(59.888351, 30.170536));
		result.add(new MapRegionDto(59.888351, 30.170518));
		result.add(new MapRegionDto(59.888344, 30.170386));
		result.add(new MapRegionDto(59.888343, 30.170297));
		result.add(new MapRegionDto(59.888342, 30.170207));
		result.add(new MapRegionDto(59.888343, 30.170118));
		result.add(new MapRegionDto(59.888344, 30.170029));
		result.add(new MapRegionDto(59.888348, 30.169902));
		result.add(new MapRegionDto(59.88835, 30.169851));
		result.add(new MapRegionDto(59.888354, 30.169762));
		result.add(new MapRegionDto(59.888358, 30.169673));
		result.add(new MapRegionDto(59.888363, 30.169584));
		result.add(new MapRegionDto(59.888367, 30.169495));
		result.add(new MapRegionDto(59.888372, 30.169406));
		result.add(new MapRegionDto(59.888375, 30.169317));
		result.add(new MapRegionDto(59.888378, 30.169228));
		result.add(new MapRegionDto(59.888379, 30.169139));
		result.add(new MapRegionDto(59.888377, 30.169031));
		result.add(new MapRegionDto(59.888355, 30.168958));
		result.add(new MapRegionDto(59.888937, 30.167764));
		result.add(new MapRegionDto(59.890563, 30.159609));
		result.add(new MapRegionDto(59.894156, 30.141709));
		result.add(new MapRegionDto(59.897797, 30.124162));
		result.add(new MapRegionDto(59.901594, 30.106004));
		result.add(new MapRegionDto(59.904504, 30.092218));
		result.add(new MapRegionDto(59.90341, 30.091339));
		result.add(new MapRegionDto(59.902333, 30.090515));
		result.add(new MapRegionDto(59.901126, 30.096515));
		result.add(new MapRegionDto(59.899411, 30.098047));
		result.add(new MapRegionDto(59.897924, 30.105189));
		result.add(new MapRegionDto(59.898504, 30.109246));
		result.add(new MapRegionDto(59.895511, 30.12385));
		result.add(new MapRegionDto(59.886868, 30.165564));
		result.add(new MapRegionDto(59.886466, 30.165167));
		result.add(new MapRegionDto(59.885002, 30.164856));
		result.add(new MapRegionDto(59.883597, 30.165639));
		result.add(new MapRegionDto(59.881893, 30.167782));
		result.add(new MapRegionDto(59.881859, 30.16785));
		result.add(new MapRegionDto(59.880183, 30.17125));
		result.add(new MapRegionDto(59.88017, 30.171361));
		result.add(new MapRegionDto(59.879575, 30.168434));
		result.add(new MapRegionDto(59.879247, 30.166658));
		result.add(new MapRegionDto(59.87423, 30.161377));
		result.add(new MapRegionDto(59.873962, 30.162194));
		result.add(new MapRegionDto(59.872127, 30.170582));
		result.add(new MapRegionDto(59.871882, 30.170325));
		result.add(new MapRegionDto(59.871644, 30.170036));
		result.add(new MapRegionDto(59.871599, 30.168747));
		result.add(new MapRegionDto(59.871516, 30.16779));
		result.add(new MapRegionDto(59.871569, 30.167002));
		result.add(new MapRegionDto(59.871426, 30.164705));
		result.add(new MapRegionDto(59.871236, 30.163173));
		result.add(new MapRegionDto(59.871214, 30.163148));
		result.add(new MapRegionDto(59.871145, 30.163072));
		result.add(new MapRegionDto(59.871065, 30.162091));
		result.add(new MapRegionDto(59.871174, 30.161138));
		result.add(new MapRegionDto(59.871188, 30.159885));
		result.add(new MapRegionDto(59.871142, 30.159529));
		result.add(new MapRegionDto(59.871073, 30.159452));
		result.add(new MapRegionDto(59.87107, 30.159032));
		result.add(new MapRegionDto(59.871114, 30.158911));
		result.add(new MapRegionDto(59.871296, 30.157398));
		result.add(new MapRegionDto(59.871504, 30.156415));
		result.add(new MapRegionDto(59.871719, 30.156083));
		result.add(new MapRegionDto(59.871922, 30.155611));
		result.add(new MapRegionDto(59.871668, 30.155191));
		result.add(new MapRegionDto(59.871381, 30.154889));
		result.add(new MapRegionDto(59.871271, 30.154834));
		result.add(new MapRegionDto(59.87091, 30.154943));
		result.add(new MapRegionDto(59.870461, 30.15432));
		result.add(new MapRegionDto(59.870412, 30.153878));
		result.add(new MapRegionDto(59.870275, 30.153478));
		result.add(new MapRegionDto(59.870014, 30.153297));
		result.add(new MapRegionDto(59.869587, 30.15254));
		result.add(new MapRegionDto(59.869448, 30.152443));
		result.add(new MapRegionDto(59.868867, 30.151997));
		result.add(new MapRegionDto(59.868097, 30.151277));
		result.add(new MapRegionDto(59.867059, 30.150155));
		result.add(new MapRegionDto(59.866635, 30.149616));
		result.add(new MapRegionDto(59.865632, 30.148058));
		result.add(new MapRegionDto(59.864993, 30.146137));
		result.add(new MapRegionDto(59.864351, 30.143951));
		result.add(new MapRegionDto(59.863899, 30.141909));
		result.add(new MapRegionDto(59.863692, 30.142056));
		result.add(new MapRegionDto(59.863535, 30.141792));
		result.add(new MapRegionDto(59.863261, 30.140839));
		result.add(new MapRegionDto(59.862747, 30.138128));
		result.add(new MapRegionDto(59.862595, 30.13799));
		result.add(new MapRegionDto(59.862511, 30.137083));
		result.add(new MapRegionDto(59.862202, 30.136234));
		result.add(new MapRegionDto(59.861896, 30.134411));
		result.add(new MapRegionDto(59.861835, 30.134172));
		result.add(new MapRegionDto(59.861836, 30.134028));
		result.add(new MapRegionDto(59.861822, 30.133902));
		result.add(new MapRegionDto(59.861743, 30.133541));
		result.add(new MapRegionDto(59.861671, 30.132673));
		result.add(new MapRegionDto(59.862512, 30.131627));
		result.add(new MapRegionDto(59.862544, 30.131554));
		result.add(new MapRegionDto(59.862566, 30.13132));
		result.add(new MapRegionDto(59.862573, 30.127285));
		result.add(new MapRegionDto(59.862634, 30.126986));
		result.add(new MapRegionDto(59.862637, 30.126741));
		result.add(new MapRegionDto(59.862573, 30.126133));
		result.add(new MapRegionDto(59.862558, 30.12484));
		result.add(new MapRegionDto(59.862575, 30.12402));
		result.add(new MapRegionDto(59.862555, 30.123648));
		result.add(new MapRegionDto(59.862543, 30.122707));
		result.add(new MapRegionDto(59.862561, 30.12034));
		result.add(new MapRegionDto(59.862611, 30.118344));
		result.add(new MapRegionDto(59.862595, 30.118255));
		result.add(new MapRegionDto(59.862589, 30.1181));
		result.add(new MapRegionDto(59.862703, 30.118064));
		result.add(new MapRegionDto(59.86287, 30.117978));
		result.add(new MapRegionDto(59.862958, 30.117911));
		result.add(new MapRegionDto(59.863263, 30.117733));
		result.add(new MapRegionDto(59.863416, 30.117641));
		result.add(new MapRegionDto(59.864904, 30.117511));
		result.add(new MapRegionDto(59.86506, 30.117411));
		result.add(new MapRegionDto(59.864703, 30.117339));
		result.add(new MapRegionDto(59.864262, 30.117405));
		result.add(new MapRegionDto(59.864073, 30.117426));
		result.add(new MapRegionDto(59.862956, 30.117529));
		result.add(new MapRegionDto(59.862752, 30.117532));
		result.add(new MapRegionDto(59.862587, 30.11757));
		result.add(new MapRegionDto(59.862475, 30.117559));
		result.add(new MapRegionDto(59.862283, 30.117548));
		result.add(new MapRegionDto(59.862149, 30.117494));
		result.add(new MapRegionDto(59.861912, 30.117348));
		result.add(new MapRegionDto(59.861822, 30.117293));
		result.add(new MapRegionDto(59.861526, 30.117245));
		result.add(new MapRegionDto(59.859634, 30.117342));
		result.add(new MapRegionDto(59.85932, 30.117252));
		result.add(new MapRegionDto(59.85886, 30.117372));
		result.add(new MapRegionDto(59.858413, 30.117377));
		result.add(new MapRegionDto(59.858352, 30.117288));
		result.add(new MapRegionDto(59.858345, 30.117131));
		result.add(new MapRegionDto(59.858341, 30.11697));
		result.add(new MapRegionDto(59.85835, 30.116937));
		result.add(new MapRegionDto(59.858516, 30.116856));
		result.add(new MapRegionDto(59.858884, 30.116836));
		result.add(new MapRegionDto(59.859162, 30.116859));
		result.add(new MapRegionDto(59.859257, 30.116677));
		result.add(new MapRegionDto(59.861823, 30.116518));
		result.add(new MapRegionDto(59.861912, 30.116527));
		result.add(new MapRegionDto(59.861973, 30.116533));
		result.add(new MapRegionDto(59.862302, 30.116309));
		result.add(new MapRegionDto(59.862476, 30.116256));
		result.add(new MapRegionDto(59.862652, 30.116189));
		result.add(new MapRegionDto(59.862864, 30.116201));
		result.add(new MapRegionDto(59.863121, 30.116241));
		result.add(new MapRegionDto(59.863414, 30.116131));
		result.add(new MapRegionDto(59.864214, 30.11612));
		result.add(new MapRegionDto(59.865071, 30.116065));
		result.add(new MapRegionDto(59.865683, 30.116029));
		result.add(new MapRegionDto(59.866225, 30.115972));
		result.add(new MapRegionDto(59.86652, 30.11595));
		result.add(new MapRegionDto(59.866833, 30.115895));
		result.add(new MapRegionDto(59.866938, 30.115898));
		result.add(new MapRegionDto(59.866957, 30.115864));
		result.add(new MapRegionDto(59.866961, 30.115826));
		result.add(new MapRegionDto(59.866959, 30.115727));
		result.add(new MapRegionDto(59.866949, 30.115663));
		result.add(new MapRegionDto(59.866927, 30.115606));
		result.add(new MapRegionDto(59.866895, 30.115578));
		result.add(new MapRegionDto(59.866345, 30.115582));
		result.add(new MapRegionDto(59.865905, 30.115559));
		result.add(new MapRegionDto(59.865559, 30.115424));
		result.add(new MapRegionDto(59.865323, 30.11546));
		result.add(new MapRegionDto(59.865158, 30.115557));
		result.add(new MapRegionDto(59.86508, 30.115619));
		result.add(new MapRegionDto(59.864786, 30.115597));
		result.add(new MapRegionDto(59.864645, 30.115682));
		result.add(new MapRegionDto(59.864265, 30.115666));
		result.add(new MapRegionDto(59.864067, 30.115759));
		result.add(new MapRegionDto(59.863836, 30.115754));
		result.add(new MapRegionDto(59.863619, 30.115561));
		result.add(new MapRegionDto(59.863118, 30.11553));
		result.add(new MapRegionDto(59.862865, 30.115397));
		result.add(new MapRegionDto(59.862668, 30.115243));
		result.add(new MapRegionDto(59.862578, 30.115211));
		result.add(new MapRegionDto(59.862462, 30.115183));
		result.add(new MapRegionDto(59.862325, 30.115297));
		result.add(new MapRegionDto(59.861947, 30.11579));
		result.add(new MapRegionDto(59.861866, 30.115808));
		result.add(new MapRegionDto(59.86182, 30.115412));
		result.add(new MapRegionDto(59.861736, 30.115208));
		result.add(new MapRegionDto(59.861608, 30.115165));
		result.add(new MapRegionDto(59.861395, 30.115228));
		result.add(new MapRegionDto(59.8611, 30.115413));
		result.add(new MapRegionDto(59.861086, 30.115017));
		result.add(new MapRegionDto(59.861196, 30.114753));
		result.add(new MapRegionDto(59.861352, 30.114479));
		result.add(new MapRegionDto(59.861419, 30.11429));
		result.add(new MapRegionDto(59.861486, 30.114076));
		result.add(new MapRegionDto(59.861646, 30.113369));
		result.add(new MapRegionDto(59.861736, 30.113181));
		result.add(new MapRegionDto(59.861977, 30.112942));
		result.add(new MapRegionDto(59.862086, 30.112618));
		result.add(new MapRegionDto(59.86213, 30.112439));
		result.add(new MapRegionDto(59.862124, 30.112304));
		result.add(new MapRegionDto(59.862092, 30.112115));
		result.add(new MapRegionDto(59.862044, 30.111893));
		result.add(new MapRegionDto(59.861983, 30.111739));
		result.add(new MapRegionDto(59.861917, 30.111616));
		result.add(new MapRegionDto(59.861714, 30.111142));
		result.add(new MapRegionDto(59.861635, 30.110865));
		result.add(new MapRegionDto(59.861619, 30.110588));
		result.add(new MapRegionDto(59.861682, 30.110284));
		result.add(new MapRegionDto(59.861783, 30.109936));
		result.add(new MapRegionDto(59.861918, 30.109467));
		result.add(new MapRegionDto(59.8621, 30.108735));
		result.add(new MapRegionDto(59.862222, 30.10843));
		result.add(new MapRegionDto(59.862343, 30.108323));
		result.add(new MapRegionDto(59.862456, 30.108297));
		result.add(new MapRegionDto(59.862942, 30.108309));
		result.add(new MapRegionDto(59.863125, 30.108291));
		result.add(new MapRegionDto(59.863164, 30.108282));
		result.add(new MapRegionDto(59.863206, 30.108243));
		result.add(new MapRegionDto(59.863234, 30.108175));
		result.add(new MapRegionDto(59.863235, 30.108106));
		result.add(new MapRegionDto(59.863219, 30.108045));
		result.add(new MapRegionDto(59.863173, 30.107981));
		result.add(new MapRegionDto(59.862929, 30.107978));
		result.add(new MapRegionDto(59.862698, 30.107983));
		result.add(new MapRegionDto(59.862529, 30.107961));
		result.add(new MapRegionDto(59.862349, 30.107917));
		result.add(new MapRegionDto(59.862385, 30.107657));
		result.add(new MapRegionDto(59.862453, 30.107579));
		result.add(new MapRegionDto(59.862491, 30.107526));
		result.add(new MapRegionDto(59.862538, 30.107419));
		result.add(new MapRegionDto(59.862629, 30.107212));
		result.add(new MapRegionDto(59.862639, 30.106912));
		result.add(new MapRegionDto(59.862581, 30.10663));
		result.add(new MapRegionDto(59.862497, 30.106287));
		result.add(new MapRegionDto(59.86241, 30.105862));
		result.add(new MapRegionDto(59.862302, 30.105337));
		result.add(new MapRegionDto(59.862073, 30.10443));
		result.add(new MapRegionDto(59.861923, 30.104065));
		result.add(new MapRegionDto(59.861711, 30.103554));
		result.add(new MapRegionDto(59.861421, 30.103185));
		result.add(new MapRegionDto(59.861213, 30.102739));
		result.add(new MapRegionDto(59.86105, 30.102381));
		result.add(new MapRegionDto(59.86091, 30.101856));
		result.add(new MapRegionDto(59.8609, 30.101335));
		result.add(new MapRegionDto(59.860997, 30.100636));
		result.add(new MapRegionDto(59.861095, 30.100111));
		result.add(new MapRegionDto(59.861086, 30.099847));
		result.add(new MapRegionDto(59.861013, 30.099604));
		result.add(new MapRegionDto(59.860936, 30.099276));
		result.add(new MapRegionDto(59.860912, 30.098994));
		result.add(new MapRegionDto(59.860922, 30.098605));
		result.add(new MapRegionDto(59.860924, 30.09828));
		result.add(new MapRegionDto(59.861114, 30.097895));
		result.add(new MapRegionDto(59.861061, 30.097542));
		result.add(new MapRegionDto(59.861075, 30.09734));
		result.add(new MapRegionDto(59.861101, 30.097157));
		result.add(new MapRegionDto(59.861095, 30.097077));
		result.add(new MapRegionDto(59.861046, 30.096924));
		result.add(new MapRegionDto(59.86104, 30.096892));
		result.add(new MapRegionDto(59.857649, 30.096883));
		result.add(new MapRegionDto(59.855953, 30.097359));
		result.add(new MapRegionDto(59.854984, 30.097838));
		result.add(new MapRegionDto(59.854499, 30.0988));
		result.add(new MapRegionDto(59.853771, 30.099761));
		result.add(new MapRegionDto(59.853774, 30.095908));
		result.add(new MapRegionDto(59.853051, 30.091091));
		result.add(new MapRegionDto(59.852809, 30.090609));
		result.add(new MapRegionDto(59.844572, 30.091549));
		result.add(new MapRegionDto(59.840696, 30.09202));
		result.add(new MapRegionDto(59.835852, 30.091526));
		result.add(new MapRegionDto(59.839206, 30.070307));
		result.add(new MapRegionDto(59.839126, 30.0688));
		result.add(new MapRegionDto(59.838958, 30.067789));
		return result;
	}

	public void getDemoMapRegion() {
		String response = "{\"type\":\"Polygon\",\"coordinates\":[[[30.067789,59.838958],[30.067759,59.838948],[30.06781,59.838548],[30.070096,59.829465],[30.070107,59.826038],[30.072487,59.825052],[30.073763,59.819874],[30.072222,59.818693],[30.073145,59.815638],[30.074808,59.815234],[30.080358,59.81577],[30.085398,59.816249],[30.09033,59.814963],[30.090336,59.814926],[30.090731,59.814858],[30.090919,59.814809],[30.091155,59.814844],[30.104387,59.817164],[30.127694,59.808984],[30.125765,59.806944],[30.124374,59.805482],[30.123393,59.804472],[30.120407,59.801428],[30.125921,59.7988],[30.135299,59.797399],[30.150017,59.800685],[30.149952,59.800618],[30.172323,59.79233],[30.177829,59.789738],[30.169775,59.784404],[30.173912,59.780809],[30.194674,59.778511],[30.195869,59.791983],[30.193655,59.793144],[30.194025,59.793277],[30.194351,59.793389],[30.194455,59.793436],[30.194895,59.79355],[30.195023,59.793588],[30.195089,59.793616],[30.195181,59.793705],[30.195322,59.793739],[30.195885,59.793906],[30.19679,59.794182],[30.197315,59.794362],[30.19745,59.794396],[30.197691,59.794487],[30.19806,59.794598],[30.198394,59.794688],[30.199056,59.794884],[30.200518,59.795335],[30.201092,59.795487],[30.201173,59.795494],[30.201802,59.795689],[30.202105,59.795744],[30.202775,59.795925],[30.203048,59.795986],[30.203336,59.796065],[30.203602,59.796128],[30.203733,59.796151],[30.203919,59.796169],[30.204178,59.796213],[30.204339,59.796251],[30.204672,59.796377],[30.204832,59.796445],[30.205124,59.796527],[30.205283,59.796547],[30.205379,59.796564],[30.206057,59.796712],[30.206897,59.79688],[30.207012,59.796895],[30.207692,59.797043],[30.208041,59.797148],[30.208366,59.797228],[30.209159,59.797378],[30.209347,59.797403],[30.209417,59.797415],[30.209976,59.79753],[30.210046,59.797538],[30.210124,59.797537],[30.21082,59.797466],[30.210869,59.797468],[30.211065,59.797522],[30.211133,59.797551],[30.211426,59.797735],[30.217257,59.79732],[30.21814,59.79723],[30.221295,59.797028],[30.222067,59.796924],[30.230331,59.795709],[30.239637,59.794323],[30.248123,59.793067],[30.259835,59.791319],[30.264896,59.790585],[30.269119,59.789957],[30.269133,59.790035],[30.269722,59.794638],[30.27638,59.795198],[30.282267,59.79536],[30.287298,59.792159],[30.287393,59.790776],[30.283247,59.787914],[30.283165,59.787829],[30.275925,59.782795],[30.274115,59.781511],[30.270602,59.778786],[30.273644,59.777657],[30.273008,59.777282],[30.268528,59.774707],[30.269499,59.774402],[30.270803,59.774004],[30.263727,59.770801],[30.258928,59.762581],[30.258815,59.762407],[30.274881,59.75661],[30.300259,59.747625],[30.300159,59.747518],[30.314624,59.742777],[30.33238,59.762406],[30.339412,59.771859],[30.332472,59.77168],[30.33143,59.772719],[30.328368,59.782251],[30.328384,59.78992],[30.334585,59.789985],[30.334322,59.797042],[30.337402,59.799665],[30.337346,59.800902],[30.33913,59.801885],[30.341126,59.804595],[30.340747,59.8086],[30.349717,59.812868],[30.368306,59.816505],[30.368067,59.817499],[30.366593,59.817487],[30.365133,59.81826],[30.365958,59.818641],[30.366333,59.818443],[30.367205,59.818834],[30.368651,59.818057],[30.370827,59.818848],[30.371308,59.819094],[30.372072,59.818726],[30.372721,59.818968],[30.372894,59.818859],[30.372491,59.818307],[30.371827,59.817936],[30.37193,59.817714],[30.375433,59.815027],[30.382177,59.816554],[30.381994,59.81692],[30.368772,59.844493],[30.361387,59.860325],[30.354635,59.874019],[30.347271,59.880159],[30.344872,59.886486],[30.345209,59.888576],[30.345216,59.891726],[30.345057,59.892341],[30.343777,59.895036],[30.340412,59.90191],[30.335017,59.913476],[30.334122,59.913679],[30.334004,59.913634],[30.33417,59.913524],[30.333204,59.913184],[30.332825,59.913461],[30.332745,59.913496],[30.33278,59.913461],[30.332763,59.913456],[30.332782,59.913442],[30.332804,59.91345],[30.332812,59.913445],[30.33288,59.91335],[30.33315,59.913166],[30.330334,59.912163],[30.325113,59.91026],[30.32131,59.90884],[30.320697,59.908683],[30.319795,59.908559],[30.31911,59.908517],[30.316512,59.908562],[30.309223,59.908604],[30.301138,59.908729],[30.297357,59.9088],[30.297293,59.90922],[30.297219,59.908799],[30.294357,59.908857],[30.289161,59.908912],[30.286567,59.908946],[30.276517,59.909],[30.276483,59.909398],[30.276482,59.909405],[30.276419,59.909405],[30.276423,59.909002],[30.274746,59.909012],[30.27471,59.909419],[30.274644,59.909012],[30.266084,59.909064],[30.266043,59.90947],[30.266014,59.909469],[30.265989,59.909471],[30.26595,59.909065],[30.261136,59.90909],[30.261208,59.909517],[30.261149,59.909732],[30.261809,59.911316],[30.262156,59.913687],[30.262092,59.915892],[30.260695,59.915983],[30.260687,59.915917],[30.260432,59.915881],[30.257699,59.915453],[30.25471,59.914988],[30.25187,59.914539],[30.249197,59.914454],[30.246521,59.91436],[30.243845,59.914269],[30.240786,59.914157],[30.240765,59.914143],[30.240791,59.913986],[30.240799,59.913932],[30.240507,59.913833],[30.238978,59.913775],[30.236942,59.913711],[30.233759,59.913614],[30.233461,59.913615],[30.232542,59.913624],[30.232352,59.913733],[30.230633,59.913649],[30.229541,59.913595],[30.228783,59.913558],[30.224,59.914145],[30.22364,59.914294],[30.223287,59.914384],[30.22299,59.914511],[30.222872,59.914534],[30.222512,59.914652],[30.222312,59.914731],[30.222178,59.914798],[30.222052,59.914837],[30.221906,59.914856],[30.221456,59.914867],[30.221019,59.914944],[30.220707,59.914988],[30.220389,59.915056],[30.220066,59.915118],[30.219674,59.91521],[30.21901,59.915345],[30.218932,59.91535],[30.218694,59.915403],[30.218763,59.915481],[30.218066,59.91565],[30.217988,59.91556],[30.215742,59.916092],[30.21551,59.916142],[30.215146,59.916221],[30.214649,59.916279],[30.214633,59.916281],[30.214617,59.916282],[30.214601,59.916284],[30.214584,59.916286],[30.214568,59.916287],[30.214552,59.916288],[30.214535,59.916289],[30.214519,59.91629],[30.214502,59.916291],[30.214486,59.916292],[30.214469,59.916292],[30.214453,59.916293],[30.214437,59.916293],[30.21442,59.916293],[30.214403,59.916293],[30.214387,59.916293],[30.21437,59.916292],[30.214354,59.916292],[30.214337,59.916291],[30.214321,59.91629],[30.214305,59.91629],[30.214288,59.916288],[30.214272,59.916287],[30.214256,59.916286],[30.214239,59.916284],[30.214223,59.916283],[30.214191,59.916279],[30.214159,59.916275],[30.214143,59.916272],[30.214127,59.91627],[30.214112,59.916267],[30.214096,59.916265],[30.214081,59.916262],[30.214065,59.916259],[30.21405,59.916256],[30.214035,59.916252],[30.21402,59.916249],[30.214005,59.916245],[30.21399,59.916242],[30.213975,59.916238],[30.213961,59.916234],[30.213947,59.91623],[30.213932,59.916226],[30.213918,59.916221],[30.213904,59.916217],[30.213891,59.916212],[30.213877,59.916208],[30.213864,59.916203],[30.21385,59.916198],[30.213837,59.916193],[30.213824,59.916188],[30.213821,59.916186],[30.213808,59.916181],[30.213795,59.916176],[30.213783,59.91617],[30.213771,59.916164],[30.213759,59.916159],[30.213747,59.916153],[30.213736,59.916147],[30.213724,59.916141],[30.213713,59.916135],[30.213702,59.916129],[30.213681,59.916116],[30.213671,59.916109],[30.213661,59.916103],[30.213651,59.916096],[30.213641,59.916089],[30.213632,59.916082],[30.213403,59.915878],[30.21253,59.914927],[30.212334,59.91478],[30.212186,59.914595],[30.21172,59.9141],[30.211701,59.914008],[30.210661,59.912858],[30.210526,59.912805],[30.210431,59.912728],[30.210282,59.912573],[30.210149,59.912399],[30.209931,59.912173],[30.209565,59.911783],[30.209441,59.911614],[30.209261,59.911394],[30.209039,59.911159],[30.208876,59.910973],[30.208611,59.910688],[30.208424,59.910469],[30.208175,59.91022],[30.208036,59.910061],[30.207843,59.909874],[30.207682,59.909701],[30.20749,59.909492],[30.207371,59.909318],[30.207017,59.908929],[30.206978,59.908838],[30.206973,59.908702],[30.206996,59.908584],[30.207045,59.908507],[30.207145,59.908387],[30.207251,59.908303],[30.20741,59.908211],[30.207916,59.908064],[30.208262,59.907956],[30.208543,59.907873],[30.208719,59.907813],[30.208993,59.907741],[30.209188,59.907677],[30.209486,59.907596],[30.21019,59.907391],[30.210489,59.907264],[30.21079,59.907207],[30.211098,59.907131],[30.211417,59.907024],[30.211777,59.906937],[30.212409,59.906738],[30.212792,59.906617],[30.213573,59.906396],[30.213652,59.906377],[30.213728,59.906351],[30.213885,59.9063],[30.214288,59.906176],[30.214527,59.906098],[30.214853,59.90604],[30.215095,59.906023],[30.215247,59.906019],[30.215353,59.906007],[30.215401,59.905986],[30.215532,59.905902],[30.21558,59.905843],[30.215622,59.905815],[30.215671,59.905798],[30.215767,59.90577],[30.215845,59.905765],[30.215955,59.905771],[30.216318,59.90533],[30.216297,59.905325],[30.216246,59.905307],[30.216179,59.905282],[30.216149,59.905269],[30.21613,59.905256],[30.21611,59.90524],[30.216086,59.905223],[30.216082,59.905215],[30.216077,59.9052],[30.216075,59.905186],[30.216075,59.905166],[30.216095,59.905118],[30.216101,59.905092],[30.216108,59.905065],[30.216116,59.905038],[30.216124,59.905012],[30.216131,59.904985],[30.216137,59.904958],[30.216142,59.904932],[30.216146,59.904905],[30.21615,59.904878],[30.216153,59.904851],[30.216155,59.904824],[30.216157,59.904797],[30.216158,59.90477],[30.216158,59.904743],[30.216158,59.904716],[30.216158,59.904689],[30.216157,59.904663],[30.216156,59.904636],[30.216155,59.904609],[30.216154,59.904582],[30.216153,59.904555],[30.216151,59.904528],[30.21615,59.904501],[30.216148,59.904474],[30.216147,59.904447],[30.216145,59.90442],[30.216143,59.904393],[30.216141,59.904366],[30.216139,59.90434],[30.216135,59.904313],[30.216132,59.904286],[30.216127,59.904259],[30.216121,59.904232],[30.216114,59.904205],[30.216107,59.904179],[30.216097,59.904152],[30.216087,59.904126],[30.216074,59.9041],[30.21606,59.904074],[30.216044,59.904048],[30.216026,59.904023],[30.216006,59.903998],[30.215984,59.903973],[30.21596,59.903949],[30.215933,59.903926],[30.215905,59.903903],[30.215875,59.90388],[30.215844,59.903859],[30.215761,59.903806],[30.215744,59.903795],[30.215708,59.903775],[30.215672,59.903755],[30.215635,59.903736],[30.215598,59.903717],[30.21556,59.903698],[30.215521,59.903679],[30.215483,59.90366],[30.215444,59.903642],[30.215404,59.903623],[30.215365,59.903605],[30.215326,59.903587],[30.215286,59.903569],[30.215247,59.90355],[30.215208,59.903532],[30.215169,59.903514],[30.21513,59.903495],[30.215091,59.903476],[30.215054,59.903457],[30.215016,59.903438],[30.214979,59.903418],[30.214942,59.903399],[30.214906,59.903379],[30.214871,59.903359],[30.214836,59.903338],[30.214801,59.903318],[30.214767,59.903297],[30.214734,59.903276],[30.214701,59.903255],[30.214668,59.903233],[30.214636,59.903212],[30.214605,59.90319],[30.214574,59.903168],[30.214543,59.903146],[30.214513,59.903123],[30.214484,59.903101],[30.214454,59.903078],[30.214426,59.903056],[30.214398,59.903033],[30.21437,59.90301],[30.214343,59.902986],[30.214316,59.902963],[30.214289,59.90294],[30.214263,59.902916],[30.214237,59.902893],[30.214212,59.902869],[30.214186,59.902845],[30.214161,59.902822],[30.214135,59.902798],[30.214109,59.902774],[30.214082,59.902751],[30.214055,59.902728],[30.214026,59.902705],[30.213997,59.902683],[30.213966,59.902661],[30.213933,59.902639],[30.213899,59.902619],[30.213863,59.902599],[30.213825,59.90258],[30.213785,59.902562],[30.213742,59.902546],[30.213697,59.902531],[30.21365,59.902518],[30.213601,59.902507],[30.213551,59.902498],[30.2135,59.90249],[30.213448,59.902483],[30.213395,59.902477],[30.213342,59.902473],[30.213289,59.902469],[30.213236,59.902466],[30.213183,59.902463],[30.21313,59.90246],[30.213076,59.902458],[30.213023,59.902456],[30.212969,59.902454],[30.212916,59.902452],[30.212862,59.90245],[30.212809,59.902448],[30.212756,59.902445],[30.212702,59.902442],[30.212649,59.902438],[30.212597,59.902433],[30.212544,59.902426],[30.212493,59.902419],[30.212442,59.90241],[30.212393,59.902399],[30.212346,59.902386],[30.212302,59.902371],[30.212261,59.902354],[30.212223,59.902335],[30.212188,59.902315],[30.212156,59.902293],[30.212127,59.90227],[30.212101,59.902247],[30.212076,59.902223],[30.212054,59.902199],[30.212033,59.902174],[30.212013,59.902149],[30.211995,59.902123],[30.211977,59.902098],[30.21196,59.902072],[30.211944,59.902047],[30.211928,59.902021],[30.211912,59.901995],[30.211897,59.90197],[30.211881,59.901944],[30.211865,59.901918],[30.211849,59.901892],[30.211833,59.901867],[30.211815,59.901841],[30.211772,59.901783],[30.211759,59.901766],[30.211738,59.901741],[30.211715,59.901716],[30.211691,59.901692],[30.211666,59.901669],[30.211638,59.901646],[30.211608,59.901623],[30.211577,59.901601],[30.211544,59.90158],[30.21151,59.901559],[30.211475,59.901539],[30.21144,59.901519],[30.211403,59.901499],[30.211366,59.90148],[30.211329,59.90146],[30.211292,59.901441],[30.211255,59.901421],[30.211218,59.901402],[30.211182,59.901382],[30.211146,59.901362],[30.211112,59.901341],[30.211078,59.90132],[30.211046,59.901299],[30.211014,59.901277],[30.210984,59.901255],[30.210954,59.901232],[30.210926,59.901209],[30.210898,59.901186],[30.210871,59.901163],[30.210845,59.90114],[30.210819,59.901116],[30.210794,59.901092],[30.21077,59.901068],[30.210746,59.901044],[30.210723,59.90102],[30.2107,59.900996],[30.210677,59.900971],[30.210655,59.900947],[30.210633,59.900922],[30.210611,59.900897],[30.21059,59.900873],[30.210569,59.900848],[30.210548,59.900823],[30.210526,59.900798],[30.210505,59.900774],[30.210484,59.900749],[30.210463,59.900724],[30.210442,59.900699],[30.210421,59.900675],[30.210399,59.90065],[30.210378,59.900625],[30.210356,59.900601],[30.210334,59.900576],[30.210311,59.900552],[30.210288,59.900527],[30.210265,59.900503],[30.210242,59.900479],[30.210218,59.900455],[30.210193,59.900431],[30.210169,59.900407],[30.210143,59.900383],[30.210117,59.90036],[30.21009,59.900337],[30.210063,59.900313],[30.210035,59.90029],[30.210006,59.900268],[30.209977,59.900245],[30.209947,59.900223],[30.209917,59.9002],[30.209887,59.900178],[30.209856,59.900156],[30.209825,59.900134],[30.209794,59.900112],[30.209764,59.90009],[30.209733,59.900068],[30.209703,59.900046],[30.20963,59.899991],[30.209615,59.899978],[30.209586,59.899955],[30.209559,59.899932],[30.209533,59.899909],[30.209507,59.899885],[30.209483,59.899861],[30.20946,59.899837],[30.209439,59.899812],[30.209419,59.899787],[30.209399,59.899762],[30.209381,59.899737],[30.209363,59.899711],[30.209346,59.899686],[30.209329,59.89966],[30.209312,59.899635],[30.209295,59.899609],[30.209277,59.899584],[30.209259,59.899558],[30.209241,59.899533],[30.209221,59.899508],[30.209201,59.899483],[30.209179,59.899459],[30.209155,59.899434],[30.209142,59.899422],[30.2091,59.899398],[30.209066,59.899377],[30.20903,59.899357],[30.208994,59.899337],[30.208957,59.899318],[30.208919,59.899299],[30.20888,59.89928],[30.20884,59.899262],[30.208799,59.899245],[30.208757,59.899228],[30.208715,59.899212],[30.208672,59.899195],[30.208629,59.899179],[30.208585,59.899164],[30.208541,59.899148],[30.208497,59.899133],[30.208475,59.899125],[30.208432,59.899109],[30.20839,59.899093],[30.208347,59.899076],[30.208305,59.89906],[30.208262,59.899044],[30.208219,59.899027],[30.208177,59.899011],[30.208134,59.898995],[30.208092,59.898978],[30.208049,59.898962],[30.208007,59.898946],[30.207964,59.898929],[30.207922,59.898913],[30.207879,59.898896],[30.207837,59.89888],[30.207794,59.898864],[30.207752,59.898847],[30.20771,59.89883],[30.207668,59.898814],[30.207626,59.898797],[30.207584,59.89878],[30.207542,59.898763],[30.207501,59.898746],[30.207459,59.898729],[30.207418,59.898712],[30.207377,59.898694],[30.207337,59.898677],[30.207296,59.898659],[30.207256,59.898641],[30.207216,59.898623],[30.207177,59.898605],[30.207137,59.898587],[30.207098,59.898568],[30.20706,59.89855],[30.207021,59.898531],[30.206983,59.898512],[30.206945,59.898493],[30.206907,59.898474],[30.206869,59.898455],[30.206832,59.898436],[30.206795,59.898416],[30.206758,59.898397],[30.206721,59.898377],[30.206685,59.898357],[30.206648,59.898338],[30.206533,59.898273],[30.206505,59.898257],[30.20647,59.898237],[30.206434,59.898217],[30.206399,59.898197],[30.206364,59.898176],[30.20633,59.898156],[30.206295,59.898135],[30.20626,59.898115],[30.206226,59.898094],[30.206192,59.898073],[30.206157,59.898052],[30.206123,59.898032],[30.206089,59.898011],[30.206054,59.897991],[30.206019,59.89797],[30.205983,59.89795],[30.205948,59.89793],[30.205911,59.89791],[30.205874,59.897891],[30.205837,59.897872],[30.205798,59.897853],[30.205759,59.897834],[30.205719,59.897816],[30.205679,59.897799],[30.205638,59.897781],[30.205596,59.897764],[30.205554,59.897748],[30.205512,59.897731],[30.205469,59.897715],[30.205426,59.897699],[30.205382,59.897683],[30.205339,59.897668],[30.205295,59.897652],[30.205251,59.897637],[30.205207,59.897621],[30.205186,59.897612],[30.205151,59.897592],[30.205117,59.897571],[30.205006,59.897499],[30.204986,59.897486],[30.204954,59.897464],[30.204922,59.897442],[30.20489,59.897421],[30.204857,59.8974],[30.204824,59.897378],[30.20479,59.897358],[30.204755,59.897337],[30.20472,59.897317],[30.204683,59.897297],[30.204646,59.897277],[30.204609,59.897258],[30.204571,59.897239],[30.204532,59.897221],[30.204492,59.897203],[30.204452,59.897185],[30.204412,59.897167],[30.204371,59.89715],[30.204329,59.897133],[30.204288,59.897116],[30.204245,59.897099],[30.204202,59.897083],[30.204159,59.897067],[30.204116,59.897052],[30.204072,59.897036],[30.204027,59.897021],[30.203983,59.897006],[30.203938,59.896991],[30.203916,59.896984],[30.203881,59.896963],[30.203847,59.896942],[30.203813,59.896922],[30.203779,59.896901],[30.203745,59.89688],[30.20371,59.89686],[30.203676,59.896839],[30.203641,59.896819],[30.203606,59.896798],[30.203571,59.896778],[30.203535,59.896758],[30.203499,59.896738],[30.203463,59.896718],[30.203427,59.896698],[30.20339,59.896678],[30.203353,59.896659],[30.203315,59.89664],[30.203277,59.896621],[30.203239,59.896602],[30.2032,59.896584],[30.20316,59.896565],[30.20312,59.896547],[30.20308,59.89653],[30.203039,59.896513],[30.202997,59.896495],[30.202955,59.896479],[30.202913,59.896462],[30.20287,59.896446],[30.202826,59.89643],[30.202783,59.896415],[30.202739,59.896399],[30.202695,59.896384],[30.20265,59.896369],[30.202605,59.896354],[30.202561,59.896339],[30.202515,59.896325],[30.20247,59.896311],[30.202425,59.896296],[30.202379,59.896282],[30.202334,59.896268],[30.202288,59.896254],[30.202242,59.89624],[30.202196,59.896226],[30.202151,59.896212],[30.202105,59.896198],[30.202059,59.896184],[30.202014,59.896169],[30.201968,59.896155],[30.201923,59.896141],[30.201877,59.896127],[30.201832,59.896112],[30.201787,59.896098],[30.201742,59.896083],[30.201698,59.896068],[30.201653,59.896053],[30.201609,59.896037],[30.201566,59.896022],[30.201522,59.896006],[30.201479,59.89599],[30.201436,59.895974],[30.201393,59.895958],[30.201351,59.895941],[30.201309,59.895925],[30.201267,59.895908],[30.201225,59.895891],[30.201183,59.895874],[30.201141,59.895858],[30.201099,59.895841],[30.201057,59.895824],[30.201015,59.895808],[30.200972,59.895791],[30.20093,59.895774],[30.200888,59.895758],[30.200845,59.895742],[30.200802,59.895726],[30.200759,59.89571],[30.200715,59.895694],[30.200672,59.895678],[30.200628,59.895663],[30.200584,59.895647],[30.20054,59.895632],[30.200496,59.895616],[30.200452,59.895601],[30.200408,59.895586],[30.200364,59.895571],[30.20032,59.895555],[30.200276,59.89554],[30.200231,59.895525],[30.200187,59.89551],[30.200143,59.895494],[30.200099,59.895479],[30.200055,59.895464],[30.200011,59.895448],[30.199967,59.895433],[30.199924,59.895417],[30.19988,59.895401],[30.199837,59.895386],[30.199793,59.89537],[30.19975,59.895354],[30.199708,59.895337],[30.199665,59.895321],[30.199623,59.895305],[30.19958,59.895288],[30.199538,59.895271],[30.199496,59.895255],[30.199455,59.895238],[30.199413,59.895221],[30.199371,59.895204],[30.19933,59.895187],[30.199289,59.895169],[30.199248,59.895152],[30.199207,59.895135],[30.19909,59.895085],[30.199084,59.895082],[30.199044,59.895065],[30.199003,59.895047],[30.198963,59.89503],[30.198922,59.895012],[30.198881,59.894994],[30.19876,59.894941],[30.198719,59.894924],[30.198679,59.894906],[30.198638,59.894889],[30.198598,59.894871],[30.198557,59.894853],[30.198517,59.894836],[30.198477,59.894818],[30.198437,59.8948],[30.198397,59.894782],[30.198357,59.894764],[30.198318,59.894746],[30.198279,59.894727],[30.19824,59.894708],[30.198202,59.89469],[30.198164,59.89467],[30.198127,59.894651],[30.19809,59.894631],[30.198054,59.894612],[30.198018,59.894592],[30.197983,59.894571],[30.197948,59.894551],[30.197913,59.89453],[30.197879,59.89451],[30.197845,59.894489],[30.197811,59.894468],[30.197777,59.894447],[30.197743,59.894427],[30.197709,59.894406],[30.197674,59.894385],[30.19764,59.894364],[30.197606,59.894344],[30.197571,59.894323],[30.197536,59.894303],[30.197501,59.894282],[30.197467,59.894262],[30.197432,59.894241],[30.197398,59.89422],[30.197364,59.8942],[30.197331,59.894179],[30.197298,59.894157],[30.197266,59.894136],[30.197235,59.894114],[30.197204,59.894091],[30.197175,59.894069],[30.197147,59.894046],[30.197121,59.894023],[30.197095,59.893999],[30.19707,59.893975],[30.197046,59.893951],[30.197022,59.893927],[30.196999,59.893903],[30.196976,59.893878],[30.196952,59.893854],[30.196928,59.89383],[30.196904,59.893806],[30.196879,59.893782],[30.196852,59.893759],[30.196825,59.893736],[30.196797,59.893713],[30.196768,59.89369],[30.196737,59.893668],[30.196706,59.893646],[30.196674,59.893625],[30.196641,59.893603],[30.196607,59.893583],[30.196572,59.893562],[30.196536,59.893542],[30.196499,59.893523],[30.196462,59.893503],[30.196424,59.893484],[30.196385,59.893466],[30.196345,59.893448],[30.196305,59.89343],[30.196264,59.893412],[30.196223,59.893395],[30.196181,59.893379],[30.196138,59.893362],[30.196095,59.893346],[30.196052,59.893331],[30.196008,59.893315],[30.195964,59.8933],[30.195919,59.893285],[30.195874,59.89327],[30.195829,59.893255],[30.195784,59.893241],[30.195739,59.893226],[30.195694,59.893212],[30.195649,59.893197],[30.195604,59.893183],[30.195559,59.893168],[30.195514,59.893153],[30.19547,59.893138],[30.195426,59.893123],[30.195382,59.893107],[30.195339,59.893091],[30.195296,59.893075],[30.195114,59.892995],[30.194941,59.892893],[30.194926,59.892882],[30.194899,59.892859],[30.194874,59.892835],[30.194851,59.892811],[30.194831,59.892786],[30.194812,59.892761],[30.194796,59.892735],[30.194781,59.892709],[30.194768,59.892683],[30.194756,59.892657],[30.194746,59.89263],[30.194737,59.892604],[30.194729,59.892577],[30.194722,59.89255],[30.194716,59.892524],[30.194711,59.892497],[30.194706,59.89247],[30.194702,59.892443],[30.194698,59.892416],[30.194695,59.892389],[30.194692,59.892362],[30.194689,59.892336],[30.194687,59.892309],[30.194684,59.892282],[30.194681,59.892255],[30.194679,59.892228],[30.194676,59.892201],[30.194672,59.892174],[30.194669,59.892147],[30.194665,59.892121],[30.19466,59.892094],[30.194655,59.892067],[30.194649,59.89204],[30.194642,59.892013],[30.194635,59.891987],[30.194626,59.89196],[30.194617,59.891934],[30.194606,59.891907],[30.194595,59.891881],[30.194582,59.891855],[30.194568,59.891829],[30.194552,59.891803],[30.194536,59.891777],[30.194517,59.891752],[30.194498,59.891727],[30.194477,59.891702],[30.194454,59.891678],[30.19443,59.891654],[30.194405,59.89163],[30.194377,59.891607],[30.194348,59.891584],[30.194318,59.891562],[30.194286,59.891541],[30.194253,59.891519],[30.194219,59.891499],[30.194184,59.891478],[30.194148,59.891458],[30.194112,59.891438],[30.194075,59.891419],[30.194038,59.891399],[30.194001,59.89138],[30.193963,59.891361],[30.193925,59.891342],[30.193912,59.891335],[30.193867,59.891294],[30.193839,59.891271],[30.193811,59.891248],[30.193784,59.891225],[30.193757,59.891202],[30.19373,59.891179],[30.193704,59.891155],[30.19368,59.891131],[30.193656,59.891107],[30.193603,59.891046],[30.193592,59.891033],[30.193574,59.891008],[30.193558,59.890982],[30.193544,59.890956],[30.193532,59.89093],[30.193522,59.890903],[30.193514,59.890877],[30.193506,59.89085],[30.193499,59.890823],[30.193493,59.890796],[30.193487,59.89077],[30.19348,59.890743],[30.193472,59.890716],[30.193463,59.89069],[30.193452,59.890663],[30.19344,59.890637],[30.193424,59.890612],[30.193406,59.890586],[30.193387,59.890561],[30.193365,59.890537],[30.193341,59.890512],[30.193316,59.890489],[30.193289,59.890465],[30.193261,59.890442],[30.193232,59.89042],[30.193202,59.890397],[30.193171,59.890375],[30.19314,59.890353],[30.193108,59.890332],[30.193076,59.89031],[30.193044,59.890289],[30.193011,59.890267],[30.192979,59.890246],[30.192947,59.890224],[30.192914,59.890203],[30.192883,59.890181],[30.192851,59.890159],[30.19282,59.890137],[30.19279,59.890115],[30.192761,59.890093],[30.192732,59.89007],[30.192704,59.890047],[30.192677,59.890024],[30.192649,59.890001],[30.192623,59.889977],[30.192596,59.889954],[30.19257,59.88993],[30.192544,59.889907],[30.192518,59.889883],[30.192492,59.88986],[30.192466,59.889836],[30.19244,59.889813],[30.192413,59.889789],[30.192386,59.889766],[30.192358,59.889743],[30.19233,59.88972],[30.192302,59.889697],[30.192272,59.889675],[30.192242,59.889653],[30.192211,59.889631],[30.192179,59.889609],[30.192147,59.889587],[30.192114,59.889566],[30.19208,59.889545],[30.192046,59.889525],[30.192011,59.889504],[30.191976,59.889484],[30.191939,59.889464],[30.191903,59.889444],[30.191866,59.889425],[30.191828,59.889406],[30.19179,59.889387],[30.191752,59.889368],[30.191713,59.88935],[30.191673,59.889331],[30.191634,59.889313],[30.191594,59.889295],[30.191553,59.889278],[30.191512,59.88926],[30.191471,59.889243],[30.19143,59.889226],[30.191388,59.889209],[30.191346,59.889193],[30.191303,59.889176],[30.19126,59.88916],[30.191217,59.889144],[30.191174,59.889128],[30.19113,59.889113],[30.191086,59.889097],[30.191042,59.889082],[30.190997,59.889067],[30.190953,59.889052],[30.190908,59.889038],[30.190862,59.889023],[30.190817,59.889009],[30.190772,59.888995],[30.190726,59.88898],[30.19068,59.888966],[30.190635,59.888952],[30.190589,59.888938],[30.190543,59.888924],[30.190497,59.88891],[30.190451,59.888897],[30.190405,59.888883],[30.190358,59.88887],[30.190311,59.888857],[30.190264,59.888844],[30.190216,59.888832],[30.190168,59.888821],[30.190119,59.888809],[30.19007,59.888799],[30.19002,59.888789],[30.18997,59.888779],[30.189919,59.888771],[30.189868,59.888763],[30.189816,59.888756],[30.189764,59.88875],[30.189711,59.888744],[30.189659,59.888739],[30.189606,59.888735],[30.189553,59.888731],[30.1895,59.888727],[30.189447,59.888724],[30.189298,59.888716],[30.189287,59.888715],[30.189233,59.888712],[30.18918,59.88871],[30.189127,59.888707],[30.189074,59.888703],[30.189021,59.8887],[30.188968,59.888696],[30.188915,59.888692],[30.188862,59.888688],[30.188809,59.888684],[30.188756,59.888679],[30.188704,59.888674],[30.188651,59.888669],[30.188599,59.888663],[30.188546,59.888658],[30.188494,59.888652],[30.188442,59.888646],[30.188389,59.88864],[30.188337,59.888634],[30.188285,59.888628],[30.188233,59.888621],[30.188181,59.888615],[30.188129,59.888608],[30.188078,59.888602],[30.188026,59.888595],[30.187974,59.888588],[30.187922,59.888582],[30.18787,59.888575],[30.187818,59.888569],[30.187766,59.888562],[30.187714,59.888556],[30.187662,59.88855],[30.18761,59.888544],[30.187558,59.888537],[30.187506,59.888531],[30.187453,59.888525],[30.187401,59.888519],[30.187349,59.888513],[30.187193,59.888494],[30.18714,59.888488],[30.187088,59.888482],[30.187036,59.888476],[30.186984,59.88847],[30.186932,59.888463],[30.18688,59.888457],[30.186828,59.888451],[30.186775,59.888445],[30.186723,59.888438],[30.186671,59.888432],[30.186619,59.888425],[30.186567,59.888419],[30.186515,59.888412],[30.186464,59.888406],[30.186412,59.888399],[30.18636,59.888392],[30.186308,59.888385],[30.186256,59.888378],[30.186205,59.888371],[30.186153,59.888364],[30.186101,59.888357],[30.186049,59.88835],[30.185998,59.888343],[30.185946,59.888335],[30.185895,59.888328],[30.185843,59.888321],[30.185792,59.888313],[30.18574,59.888306],[30.185689,59.888298],[30.185637,59.888291],[30.185586,59.888283],[30.185534,59.888276],[30.185483,59.888268],[30.185431,59.888261],[30.18538,59.888253],[30.185329,59.888245],[30.185277,59.888238],[30.185226,59.88823],[30.185175,59.888223],[30.185143,59.888218],[30.185093,59.888209],[30.185042,59.8882],[30.184991,59.888192],[30.18494,59.888183],[30.184889,59.888175],[30.184838,59.888167],[30.184787,59.888159],[30.184736,59.888151],[30.184685,59.888143],[30.184634,59.888135],[30.184582,59.888127],[30.184531,59.888119],[30.18448,59.888112],[30.184428,59.888104],[30.184377,59.888097],[30.184325,59.888089],[30.184274,59.888082],[30.184222,59.888075],[30.184171,59.888067],[30.184119,59.88806],[30.184067,59.888053],[30.184016,59.888046],[30.183964,59.888039],[30.183912,59.888032],[30.18386,59.888025],[30.183809,59.888019],[30.183757,59.888012],[30.183705,59.888005],[30.183653,59.887998],[30.183601,59.887992],[30.183549,59.887985],[30.183497,59.887978],[30.183445,59.887972],[30.183393,59.887965],[30.183289,59.887952],[30.183238,59.887946],[30.183134,59.887933],[30.183082,59.887926],[30.18303,59.88792],[30.183013,59.887916],[30.182997,59.887913],[30.18298,59.887909],[30.182964,59.887906],[30.182947,59.887903],[30.18293,59.887899],[30.182914,59.887896],[30.182897,59.887893],[30.18288,59.88789],[30.182863,59.887887],[30.182846,59.887884],[30.182829,59.887882],[30.182812,59.887879],[30.182795,59.887876],[30.182778,59.887874],[30.182761,59.887871],[30.182744,59.887869],[30.182726,59.887867],[30.182709,59.887864],[30.182692,59.887862],[30.182674,59.88786],[30.182657,59.887858],[30.182639,59.887856],[30.182622,59.887854],[30.182605,59.887852],[30.182587,59.887851],[30.182569,59.887849],[30.182552,59.887847],[30.182534,59.887846],[30.182517,59.887844],[30.182499,59.887843],[30.182481,59.887841],[30.182464,59.88784],[30.182446,59.887839],[30.182428,59.887838],[30.182411,59.887836],[30.182393,59.887835],[30.182375,59.887834],[30.182357,59.887833],[30.18234,59.887832],[30.182322,59.887832],[30.182304,59.887831],[30.182286,59.88783],[30.182269,59.887829],[30.182251,59.887828],[30.182233,59.887828],[30.182215,59.887827],[30.182197,59.887826],[30.18218,59.887826],[30.182162,59.887825],[30.182144,59.887825],[30.182126,59.887824],[30.182108,59.887824],[30.18209,59.887824],[30.182072,59.887823],[30.182055,59.887823],[30.182037,59.887823],[30.182019,59.887822],[30.182001,59.887822],[30.181983,59.887822],[30.181965,59.887822],[30.181947,59.887822],[30.18193,59.887822],[30.181912,59.887822],[30.181894,59.887821],[30.181876,59.887821],[30.181858,59.887821],[30.18184,59.887821],[30.181822,59.887821],[30.181805,59.887821],[30.181787,59.887821],[30.181751,59.887821],[30.181733,59.887821],[30.181715,59.887822],[30.181577,59.887822],[30.181572,59.887823],[30.181555,59.887823],[30.181501,59.887823],[30.181483,59.887823],[30.181465,59.887823],[30.18143,59.887824],[30.181412,59.887824],[30.181394,59.887824],[30.181358,59.887824],[30.181322,59.887824],[30.181305,59.887824],[30.181287,59.887825],[30.181269,59.887825],[30.181233,59.887825],[30.181215,59.887825],[30.181197,59.887825],[30.18118,59.887825],[30.181162,59.887825],[30.181144,59.887825],[30.181126,59.887825],[30.181108,59.887825],[30.18109,59.887824],[30.181072,59.887824],[30.181054,59.887824],[30.181037,59.887824],[30.181019,59.887824],[30.181001,59.887824],[30.180983,59.887823],[30.180965,59.887823],[30.180947,59.887823],[30.180929,59.887823],[30.180894,59.887822],[30.180858,59.887821],[30.18084,59.887821],[30.180822,59.887821],[30.180805,59.88782],[30.180787,59.88782],[30.180769,59.88782],[30.180751,59.887819],[30.180733,59.887819],[30.180698,59.887818],[30.18068,59.887817],[30.180662,59.887817],[30.180644,59.887816],[30.180626,59.887816],[30.180608,59.887815],[30.180591,59.887815],[30.180573,59.887814],[30.180555,59.887813],[30.180537,59.887813],[30.180519,59.887812],[30.180501,59.887812],[30.180484,59.887811],[30.180466,59.88781],[30.180412,59.887808],[30.180377,59.887807],[30.180359,59.887806],[30.180341,59.887806],[30.180323,59.887805],[30.180306,59.887804],[30.180288,59.887803],[30.18027,59.887803],[30.180252,59.887802],[30.180234,59.887801],[30.180217,59.8878],[30.180199,59.887799],[30.180181,59.887799],[30.180163,59.887798],[30.180128,59.887796],[30.18011,59.887795],[30.180092,59.887795],[30.180057,59.887793],[30.180021,59.887791],[30.179968,59.887789],[30.179932,59.887787],[30.179897,59.887785],[30.179861,59.887783],[30.179843,59.887782],[30.179825,59.887781],[30.179808,59.88778],[30.17979,59.88778],[30.179719,59.887776],[30.179701,59.887775],[30.179666,59.887773],[30.179648,59.887772],[30.17963,59.887771],[30.179612,59.88777],[30.179594,59.887769],[30.179541,59.887767],[30.179523,59.887766],[30.179488,59.887764],[30.17947,59.887763],[30.179452,59.887762],[30.179435,59.887761],[30.179417,59.88776],[30.179399,59.887759],[30.179381,59.887758],[30.179364,59.887757],[30.179346,59.887757],[30.17931,59.887755],[30.179292,59.887754],[30.179275,59.887753],[30.179239,59.887751],[30.179186,59.887749],[30.17915,59.887747],[30.179132,59.887746],[30.179097,59.887744],[30.179079,59.887744],[30.179061,59.887743],[30.179008,59.88774],[30.17899,59.88774],[30.178955,59.887738],[30.178937,59.887737],[30.178883,59.887735],[30.178866,59.887734],[30.17883,59.887733],[30.178812,59.887732],[30.178776,59.887731],[30.178741,59.88773],[30.178723,59.887729],[30.178705,59.887729],[30.178687,59.887728],[30.17867,59.887727],[30.178652,59.887727],[30.178634,59.887726],[30.178616,59.887726],[30.178598,59.887725],[30.17858,59.887725],[30.178563,59.887724],[30.178545,59.887724],[30.178527,59.887723],[30.178491,59.887722],[30.178473,59.887722],[30.178456,59.887722],[30.178438,59.887721],[30.17842,59.887721],[30.178402,59.88772],[30.178384,59.88772],[30.178366,59.88772],[30.178348,59.88772],[30.178331,59.887719],[30.178313,59.887719],[30.178295,59.887719],[30.178277,59.887718],[30.178241,59.887718],[30.178223,59.887718],[30.178206,59.887718],[30.178188,59.887717],[30.17817,59.887717],[30.178152,59.887717],[30.178116,59.887717],[30.178098,59.887717],[30.178081,59.887717],[30.178063,59.887717],[30.178045,59.887717],[30.178009,59.887717],[30.177991,59.887717],[30.177973,59.887717],[30.177956,59.887717],[30.177938,59.887717],[30.177902,59.887717],[30.177884,59.887717],[30.177866,59.887717],[30.177848,59.887717],[30.177831,59.887717],[30.177813,59.887717],[30.177795,59.887717],[30.177777,59.887717],[30.177741,59.887718],[30.177723,59.887718],[30.177706,59.887718],[30.17767,59.887718],[30.177652,59.887719],[30.177634,59.887719],[30.177616,59.887719],[30.177598,59.887719],[30.177581,59.88772],[30.177545,59.88772],[30.177527,59.887721],[30.177491,59.887721],[30.177456,59.887722],[30.177438,59.887722],[30.17742,59.887723],[30.177384,59.887723],[30.177366,59.887724],[30.177349,59.887724],[30.177331,59.887724],[30.177313,59.887725],[30.177295,59.887725],[30.177259,59.887726],[30.177242,59.887727],[30.177224,59.887727],[30.177206,59.887728],[30.177188,59.887728],[30.17717,59.887729],[30.177152,59.887729],[30.177135,59.88773],[30.177081,59.887731],[30.177045,59.887732],[30.176992,59.887734],[30.176974,59.887735],[30.176956,59.887735],[30.176938,59.887736],[30.176921,59.887736],[30.176903,59.887737],[30.176885,59.887738],[30.176867,59.887738],[30.176849,59.887739],[30.176832,59.887739],[30.176814,59.88774],[30.176796,59.887741],[30.176778,59.887741],[30.17676,59.887742],[30.176742,59.887743],[30.176725,59.887743],[30.176707,59.887744],[30.176671,59.887745],[30.176653,59.887746],[30.1766,59.887748],[30.176582,59.887749],[30.176564,59.88775],[30.176547,59.88775],[30.176511,59.887752],[30.176493,59.887753],[30.176475,59.887753],[30.176458,59.887754],[30.17644,59.887755],[30.176422,59.887756],[30.176404,59.887756],[30.176386,59.887757],[30.176369,59.887758],[30.176315,59.88776],[30.176297,59.887761],[30.17628,59.887762],[30.176262,59.887763],[30.176244,59.887763],[30.176226,59.887764],[30.176173,59.887767],[30.176102,59.88777],[30.176048,59.887772],[30.176031,59.887773],[30.176013,59.887774],[30.175995,59.887775],[30.175977,59.887776],[30.175959,59.887777],[30.175942,59.887777],[30.175906,59.887779],[30.175888,59.88778],[30.175871,59.887781],[30.175853,59.887782],[30.175835,59.887783],[30.1758,59.887784],[30.175782,59.887785],[30.175764,59.887786],[30.175728,59.887788],[30.175693,59.88779],[30.175675,59.887791],[30.175563,59.887797],[30.175551,59.887797],[30.175533,59.887798],[30.175515,59.887799],[30.175498,59.8878],[30.17548,59.887801],[30.175444,59.887804],[30.175427,59.887805],[30.175409,59.887806],[30.175373,59.887808],[30.175356,59.887809],[30.17532,59.887811],[30.175303,59.887812],[30.175285,59.887813],[30.175267,59.887815],[30.175232,59.887817],[30.175214,59.887818],[30.175196,59.887819],[30.175179,59.88782],[30.175161,59.887822],[30.175125,59.887824],[30.17509,59.887827],[30.175072,59.887828],[30.175055,59.887829],[30.175037,59.887831],[30.175019,59.887832],[30.175002,59.887833],[30.174984,59.887835],[30.174949,59.887837],[30.174931,59.887839],[30.174914,59.88784],[30.174896,59.887842],[30.174878,59.887843],[30.174843,59.887846],[30.174825,59.887848],[30.174808,59.887849],[30.17479,59.887851],[30.174773,59.887852],[30.174755,59.887854],[30.174738,59.887856],[30.17472,59.887857],[30.174702,59.887859],[30.174685,59.88786],[30.174667,59.887862],[30.17465,59.887864],[30.174632,59.887866],[30.174615,59.887867],[30.174597,59.887869],[30.17458,59.887871],[30.174562,59.887873],[30.174545,59.887874],[30.174527,59.887876],[30.17451,59.887878],[30.174492,59.88788],[30.174475,59.887882],[30.174457,59.887884],[30.17444,59.887886],[30.174422,59.887888],[30.174405,59.88789],[30.174388,59.887892],[30.17437,59.887894],[30.174353,59.887896],[30.174335,59.887898],[30.174318,59.8879],[30.174301,59.887902],[30.174283,59.887904],[30.174266,59.887906],[30.174249,59.887908],[30.174231,59.887911],[30.174214,59.887913],[30.174197,59.887915],[30.17418,59.887917],[30.174162,59.88792],[30.174145,59.887922],[30.174128,59.887924],[30.174111,59.887927],[30.174093,59.887929],[30.174076,59.887932],[30.174059,59.887934],[30.174042,59.887936],[30.174025,59.887939],[30.174008,59.887942],[30.17399,59.887944],[30.173973,59.887947],[30.173956,59.887949],[30.173939,59.887952],[30.173922,59.887955],[30.173905,59.887957],[30.173888,59.88796],[30.173871,59.887963],[30.173854,59.887965],[30.173837,59.887968],[30.17382,59.887971],[30.173803,59.887974],[30.173786,59.887977],[30.173769,59.88798],[30.173752,59.887983],[30.173736,59.887986],[30.173719,59.887989],[30.173702,59.887992],[30.173685,59.887995],[30.173668,59.887998],[30.173652,59.888001],[30.173635,59.888004],[30.173618,59.888007],[30.173601,59.88801],[30.173584,59.888013],[30.173568,59.888016],[30.173551,59.888019],[30.173534,59.888023],[30.173518,59.888026],[30.173501,59.888029],[30.173484,59.888032],[30.173468,59.888035],[30.173451,59.888039],[30.173434,59.888042],[30.173418,59.888045],[30.173401,59.888049],[30.173385,59.888052],[30.173368,59.888055],[30.173351,59.888059],[30.173335,59.888062],[30.173318,59.888065],[30.173302,59.888069],[30.173268,59.888075],[30.173252,59.888079],[30.173235,59.888082],[30.173202,59.888089],[30.173186,59.888092],[30.173169,59.888096],[30.173153,59.888099],[30.173136,59.888102],[30.173103,59.888109],[30.173087,59.888113],[30.17307,59.888116],[30.173054,59.888119],[30.173037,59.888123],[30.173004,59.88813],[30.172987,59.888133],[30.172971,59.888136],[30.172954,59.88814],[30.172938,59.888143],[30.172921,59.888147],[30.172905,59.88815],[30.172888,59.888153],[30.172872,59.888157],[30.172838,59.888163],[30.172822,59.888167],[30.172805,59.88817],[30.172789,59.888173],[30.172755,59.88818],[30.172739,59.888183],[30.172722,59.888186],[30.172706,59.88819],[30.172689,59.888193],[30.172672,59.888196],[30.172656,59.888199],[30.172639,59.888203],[30.172622,59.888206],[30.172605,59.888209],[30.172589,59.888212],[30.172572,59.888215],[30.172555,59.888218],[30.172538,59.888221],[30.172522,59.888225],[30.172505,59.888228],[30.172488,59.888231],[30.172471,59.888234],[30.172454,59.888237],[30.172438,59.88824],[30.172421,59.888243],[30.172404,59.888245],[30.172387,59.888248],[30.17237,59.888251],[30.172353,59.888254],[30.172336,59.888257],[30.172319,59.88826],[30.172302,59.888262],[30.172285,59.888265],[30.172268,59.888268],[30.172251,59.888271],[30.172234,59.888273],[30.172217,59.888276],[30.1722,59.888278],[30.172183,59.888281],[30.172165,59.888283],[30.172148,59.888286],[30.172131,59.888288],[30.172114,59.888291],[30.172097,59.888293],[30.172079,59.888295],[30.172062,59.888298],[30.172045,59.8883],[30.172028,59.888302],[30.17201,59.888304],[30.171993,59.888307],[30.171976,59.888309],[30.171958,59.888311],[30.171941,59.888313],[30.171923,59.888315],[30.171906,59.888317],[30.171888,59.888318],[30.171871,59.88832],[30.171853,59.888322],[30.171836,59.888324],[30.171818,59.888326],[30.171801,59.888327],[30.171783,59.888329],[30.171766,59.88833],[30.171748,59.888332],[30.17173,59.888333],[30.171713,59.888335],[30.171695,59.888336],[30.171677,59.888337],[30.17166,59.888338],[30.171642,59.88834],[30.171624,59.888341],[30.171607,59.888342],[30.171589,59.888343],[30.171571,59.888344],[30.171553,59.888345],[30.171536,59.888346],[30.171518,59.888347],[30.1715,59.888347],[30.171482,59.888348],[30.171464,59.888349],[30.171447,59.88835],[30.171429,59.88835],[30.171411,59.888351],[30.171393,59.888352],[30.171375,59.888352],[30.171357,59.888353],[30.17134,59.888353],[30.171322,59.888354],[30.171304,59.888354],[30.171286,59.888354],[30.171268,59.888355],[30.17125,59.888355],[30.171233,59.888355],[30.171215,59.888356],[30.171197,59.888356],[30.171179,59.888356],[30.171161,59.888356],[30.171143,59.888356],[30.171125,59.888357],[30.171108,59.888357],[30.17109,59.888357],[30.171072,59.888357],[30.171054,59.888357],[30.171036,59.888357],[30.171018,59.888357],[30.171,59.888357],[30.170983,59.888357],[30.170965,59.888357],[30.170947,59.888357],[30.170929,59.888357],[30.170911,59.888356],[30.170893,59.888356],[30.170875,59.888356],[30.170858,59.888356],[30.17084,59.888356],[30.170822,59.888356],[30.170804,59.888355],[30.170786,59.888355],[30.170768,59.888355],[30.17075,59.888355],[30.170715,59.888354],[30.170697,59.888354],[30.170643,59.888353],[30.170625,59.888353],[30.170608,59.888353],[30.17059,59.888352],[30.170572,59.888352],[30.170554,59.888352],[30.170536,59.888351],[30.170518,59.888351],[30.170386,59.888344],[30.170297,59.888343],[30.170207,59.888342],[30.170118,59.888343],[30.170029,59.888344],[30.169902,59.888348],[30.169851,59.88835],[30.169762,59.888354],[30.169673,59.888358],[30.169584,59.888363],[30.169495,59.888367],[30.169406,59.888372],[30.169317,59.888375],[30.169228,59.888378],[30.169139,59.888379],[30.169031,59.888377],[30.168958,59.888355],[30.167764,59.888937],[30.159609,59.890563],[30.141709,59.894156],[30.124162,59.897797],[30.106004,59.901594],[30.092218,59.904504],[30.091339,59.90341],[30.090515,59.902333],[30.096515,59.901126],[30.098047,59.899411],[30.105189,59.897924],[30.109246,59.898504],[30.12385,59.895511],[30.165564,59.886868],[30.165167,59.886466],[30.164856,59.885002],[30.165639,59.883597],[30.167782,59.881893],[30.16785,59.881859],[30.17125,59.880183],[30.171361,59.88017],[30.168434,59.879575],[30.166658,59.879247],[30.161377,59.87423],[30.162194,59.873962],[30.170582,59.872127],[30.170325,59.871882],[30.170036,59.871644],[30.168747,59.871599],[30.16779,59.871516],[30.167002,59.871569],[30.164705,59.871426],[30.163173,59.871236],[30.163148,59.871214],[30.163072,59.871145],[30.162091,59.871065],[30.161138,59.871174],[30.159885,59.871188],[30.159529,59.871142],[30.159452,59.871073],[30.159032,59.87107],[30.158911,59.871114],[30.157398,59.871296],[30.156415,59.871504],[30.156083,59.871719],[30.155611,59.871922],[30.155191,59.871668],[30.154889,59.871381],[30.154834,59.871271],[30.154943,59.87091],[30.15432,59.870461],[30.153878,59.870412],[30.153478,59.870275],[30.153297,59.870014],[30.15254,59.869587],[30.152443,59.869448],[30.151997,59.868867],[30.151277,59.868097],[30.150155,59.867059],[30.149616,59.866635],[30.148058,59.865632],[30.146137,59.864993],[30.143951,59.864351],[30.141909,59.863899],[30.142056,59.863692],[30.141792,59.863535],[30.140839,59.863261],[30.138128,59.862747],[30.13799,59.862595],[30.137083,59.862511],[30.136234,59.862202],[30.134411,59.861896],[30.134172,59.861835],[30.134028,59.861836],[30.133902,59.861822],[30.133541,59.861743],[30.132673,59.861671],[30.131627,59.862512],[30.131554,59.862544],[30.13132,59.862566],[30.127285,59.862573],[30.126986,59.862634],[30.126741,59.862637],[30.126133,59.862573],[30.12484,59.862558],[30.12402,59.862575],[30.123648,59.862555],[30.122707,59.862543],[30.12034,59.862561],[30.118344,59.862611],[30.118255,59.862595],[30.1181,59.862589],[30.118064,59.862703],[30.117978,59.86287],[30.117911,59.862958],[30.117733,59.863263],[30.117641,59.863416],[30.117511,59.864904],[30.117411,59.86506],[30.117339,59.864703],[30.117405,59.864262],[30.117426,59.864073],[30.117529,59.862956],[30.117532,59.862752],[30.11757,59.862587],[30.117559,59.862475],[30.117548,59.862283],[30.117494,59.862149],[30.117348,59.861912],[30.117293,59.861822],[30.117245,59.861526],[30.117342,59.859634],[30.117252,59.85932],[30.117372,59.85886],[30.117377,59.858413],[30.117288,59.858352],[30.117131,59.858345],[30.11697,59.858341],[30.116937,59.85835],[30.116856,59.858516],[30.116836,59.858884],[30.116859,59.859162],[30.116677,59.859257],[30.116518,59.861823],[30.116527,59.861912],[30.116533,59.861973],[30.116309,59.862302],[30.116256,59.862476],[30.116189,59.862652],[30.116201,59.862864],[30.116241,59.863121],[30.116131,59.863414],[30.11612,59.864214],[30.116065,59.865071],[30.116029,59.865683],[30.115972,59.866225],[30.11595,59.86652],[30.115895,59.866833],[30.115898,59.866938],[30.115864,59.866957],[30.115826,59.866961],[30.115727,59.866959],[30.115663,59.866949],[30.115606,59.866927],[30.115578,59.866895],[30.115582,59.866345],[30.115559,59.865905],[30.115424,59.865559],[30.11546,59.865323],[30.115557,59.865158],[30.115619,59.86508],[30.115597,59.864786],[30.115682,59.864645],[30.115666,59.864265],[30.115759,59.864067],[30.115754,59.863836],[30.115561,59.863619],[30.11553,59.863118],[30.115397,59.862865],[30.115243,59.862668],[30.115211,59.862578],[30.115183,59.862462],[30.115297,59.862325],[30.11579,59.861947],[30.115808,59.861866],[30.115412,59.86182],[30.115208,59.861736],[30.115165,59.861608],[30.115228,59.861395],[30.115413,59.8611],[30.115017,59.861086],[30.114753,59.861196],[30.114479,59.861352],[30.11429,59.861419],[30.114076,59.861486],[30.113369,59.861646],[30.113181,59.861736],[30.112942,59.861977],[30.112618,59.862086],[30.112439,59.86213],[30.112304,59.862124],[30.112115,59.862092],[30.111893,59.862044],[30.111739,59.861983],[30.111616,59.861917],[30.111142,59.861714],[30.110865,59.861635],[30.110588,59.861619],[30.110284,59.861682],[30.109936,59.861783],[30.109467,59.861918],[30.108735,59.8621],[30.10843,59.862222],[30.108323,59.862343],[30.108297,59.862456],[30.108309,59.862942],[30.108291,59.863125],[30.108282,59.863164],[30.108243,59.863206],[30.108175,59.863234],[30.108106,59.863235],[30.108045,59.863219],[30.107981,59.863173],[30.107978,59.862929],[30.107983,59.862698],[30.107961,59.862529],[30.107917,59.862349],[30.107657,59.862385],[30.107579,59.862453],[30.107526,59.862491],[30.107419,59.862538],[30.107212,59.862629],[30.106912,59.862639],[30.10663,59.862581],[30.106287,59.862497],[30.105862,59.86241],[30.105337,59.862302],[30.10443,59.862073],[30.104065,59.861923],[30.103554,59.861711],[30.103185,59.861421],[30.102739,59.861213],[30.102381,59.86105],[30.101856,59.86091],[30.101335,59.8609],[30.100636,59.860997],[30.100111,59.861095],[30.099847,59.861086],[30.099604,59.861013],[30.099276,59.860936],[30.098994,59.860912],[30.098605,59.860922],[30.09828,59.860924],[30.097895,59.861114],[30.097542,59.861061],[30.09734,59.861075],[30.097157,59.861101],[30.097077,59.861095],[30.096924,59.861046],[30.096892,59.86104],[30.096883,59.857649],[30.097359,59.855953],[30.097838,59.854984],[30.0988,59.854499],[30.099761,59.853771],[30.095908,59.853774],[30.091091,59.853051],[30.090609,59.852809],[30.091549,59.844572],[30.09202,59.840696],[30.091526,59.835852],[30.070307,59.839206],[30.0688,59.839126],[30.067789,59.838958]]]}";
		/*try {
			JSONObject node = objectMapper.readValue(response, JSONObject.class);
			getLogger().debug(node.toString());
		} catch (IOException e) {
			getLogger().debug(e.getMessage());
		}*/
	}

	// + - @GetMapping(value = {"mechanization"}, params = {"type", "model", "number", "skip", "limit"})
	// + - @GetMapping(value = {"mechanization/types"}, params = {"skip", "limit"})
	// + - @GetMapping(value = {"mechanization/models"}, params = {"type", "skip", "limit"})
	// + - @GetMapping(value = {"mechanization/numbers"}, params = {"type", "model", "skip", "limit"})

	/**
	 * Получение средств малой механизации
	 * @return список средств малой механизации
	 */
	public List<MechanizationDto> getMechanization() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(mechanization).queryParams(map).build().toUriString();
		return returnMechanization(url);
	}

	/**
	 * Получение моделей средств малой механизации
	 * @return список моделей средств малой механизации
	 */
	public List<MechanizationDto> getMechanizationModels() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(mechanization_models).queryParams(map).build().toUriString();
		return returnMechanization(url);
	}

	/**
	 * Получение номеров средств малой механизации
	 * @return список номеров средств малой механизации
	 */
	public List<MechanizationDto> getMechanizationNumbers() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(mechanization_numbers).queryParams(map).build().toUriString();
		return returnMechanization(url);
	}

	/**
	 * Получение типов средств малой механизации
	 * @return список типов средств малой механизации
	 */
	public List<MechanizationDto> getMechanizationTypes() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(mechanization_types).queryParams(map).build().toUriString();
		return returnMechanization(url);
	}

	// + - @GetMapping({"methodsFormations"})

	public List<MethodFormationDto> getMethodFormations() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(methodsFormations).queryParams(map).build().toUriString();
		try {
			ResponseEntity<MethodFormationDto[]> entity = initRestTemplate().getForEntity(url, MethodFormationDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// + - @GetMapping(value = {"notifications"}, params = {"from", "to", "skip", "limit"})
	// + - @PutMapping({"notifications/{id}"})

	// + + @GetMapping(value = {"organizations"}, params = {"skip", "limit"})
	// + + @GetMapping(value = {"organizations"}, params = {"type", "parent-id", "skip", "limit"})
	// + + @GetMapping({"organizations/id"})

	/**
	 * Получение организаций по стандартным параметрам
	 * @return список организаций
	 */
	public List<OrganizationDto> getOrganizations() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(organizations).queryParams(map).build().toUriString();
		return returnOrganizations(url);
	}

	/**
	 * Получение организации по id
	 * @param id id организации
	 * @return организация
	 */
	public OrganizationDto getOrganization(Long id) {
		String url = String.format(organization_id, id);
		return returnOrganization(url);
	}

	/**
	 * Получение организаций по типу и стандартным параметрам
	 * @param type тип организации
	 * @return список организаций
	 */
	public List<OrganizationDto> getOrganizationsByType(String type) {
		MultiValueMap<String, String> map = getMap();
		map.add(param_type, type);
		String url = UriComponentsBuilder.fromUriString(organizations).queryParams(map).build().toUriString();
		return returnOrganizations(url);
	}

	/**
	 * Получение организаций по родителю, типу и стандартным параметрам
	 * @param parentId id родительской организации
	 * @param type тип организации
	 * @return список организаций
	 */
	public List<OrganizationDto> getChildOrganizations(Long parentId, String type) {
		MultiValueMap<String, String> map = getMap();
		map.add(param_type, type);
		map.add(param_parent, parentId.toString());
		String url = UriComponentsBuilder.fromUriString(organizations).queryParams(map).build().toUriString();
		return returnOrganizations(url);
	}

	// + + @GetMapping({"problems"})

	/**
	 * Получение кодов проблем для заявок
	 * @return список кодов проблем
	 */

	public List<ProblemDto> getProblems() {
		try {
			ResponseEntity<ProblemDto[]> entity = initRestTemplate().getForEntity(problems, ProblemDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(problems, e);
			return Collections.emptyList();
		}
	}

	// + + @GetMapping({"roles"})

	/**
	 * Получение ролей для пользователей
	 * @return список ролей
	 */

	public List<RoleDto> getRoles() {
		try {
			ResponseEntity<RoleDto[]> entity = initRestTemplate().getForEntity(roles, RoleDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(roles, e);
			return Collections.emptyList();
		}
	}

	// + + @GetMapping(value = {"reports"}, params = {"skip", "limit"})
	// + - @GetMapping({"reports/{idReport}/data"})

	/**
	 * Получение отчетов
	 * @return список отчетов
	 */
	public List<ReportDto> getReports() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(reports).queryParams(map).build().toUriString();
		try {
			ResponseEntity<ReportDto[]> entity = initRestTemplate().getForEntity(url, ReportDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// + - @GetMapping(value = {"requests"}, params = {"skip", "limit"})
	// + - @GetMapping({"requests/{id}"})
	// + - @GetMapping(value = {"requests"}, params = {"from", "to", "skip", "limit"})
	// + - @GetMapping(value = {"requests"}, params = {"guid"})
	// - - @GetMapping({"requests"})
	// + - @GetMapping(value = {"requests"}, params = {"status", "skip", "limit"})
	// + - @GetMapping(value = {"requests"}, params = {"source", "skip", "limit"})
	// + - @PostMapping({"requests"})
	// + - @PutMapping({"requests/{id}"})
	// + - @DeleteMapping({"requests/{id}"})
	// + - @PutMapping({"requests/{id}/status/"})
	// + - @PutMapping({"requests/{id}/responsible"})
	// + - @GetMapping({"requests/{id}/works"})
	// + - @PutMapping({"requests/{id}/works"})
	// + - @DeleteMapping({"requests/{id}/works"})
	// + - @GetMapping({"requests/{id}/attachments"})
	// + - @PostMapping({"requests/{id}/attachments"})
	// + - @GetMapping({"requests/{id}/attachments/{idAttachment}"})
	// + - @DeleteMapping({"requests/{id}/attachments/{idAttachment}"})
	// - - @GetMapping({"requests/{id}/attachments/{idAttachment}/content"})
	// + - @GetMapping({"requests/{id}/comments"})
	// + - @PostMapping({"requests/{id}/comments"})
	// + - @GetMapping({"requests/{id}/comments/{idComment}"})
	// + - @DeleteMapping({"requests/{id}/comments/{idComment}"})
	// + - @GetMapping({"requests/statuses"})

	/**
	 * Получение заявок за указанный период со стандартными параметрами
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список заявок
	 */
	public List<RequestDto> getRequests(ZonedDateTime start, ZonedDateTime end) {
		MultiValueMap<String, String> map = getMap(start, end);
		String url = UriComponentsBuilder.fromUriString(requests).queryParams(map).build().toUriString();
		return returnRequests(url);
	}


	/**
	 * Получение заявок со стандартными параметрами
	 * @return список заявок
	 */
	public List<RequestDto> getRequests() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(requests).queryParams(map).build().toUriString();
		return returnRequests(url);
	}

	/**
	 * Получение заявок для табличных данных с определенного района
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список заявок и их общее количество
	 */
	public PagesDto<RequestJournalDto> getRequestsForJournal(Integer skip, Integer limit, String region, String status) {
		MultiValueMap<String, String> map = getMap(DatesUtil.setNullDateTime(), null, skip, limit);
		map.add(param_organization, region);
		map.add(param_status_hl, status);
		String url = UriComponentsBuilder.fromUriString(requests).queryParams(map).build().toUriString();
		return returnRequestJournalPage(url);
	}

	/**
	 * Получение заявок для табличных данных
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список заявок и их общее количество
	 */
	public PagesDto<RequestJournalDto> getRequestsForJournal(Integer skip, Integer limit) {
		return getRequestsForJournal(skip, limit, "РВ Юго-Западный", "");
	}

	/**
	 * Получение общего количества заявок с определенного района
	 * @return общее количество заявок
	 */
	public Long countRequests(String region) {
		PagesDto<RequestJournalDto> journal = getRequestsForJournal(0, 1, region, "");
		return journal.getCount();
	}

	/**
	 * Получение общего количества заявок
	 * @return общее количество заявок
	 */
	public Long countRequests() {
		PagesDto<RequestJournalDto> journal = getRequestsForJournal(0, 1);
		return journal.getCount();
	}

	/**
	 * Получение заявок по источнику со стандартными параметрами
	 * @param source источник заявок
	 * @return список заявок
	 */
	public List<RequestDto> getRequestsBySource(String source) {
		MultiValueMap<String, String> map = getMap();
		map.add(param_source, source);
		String url = UriComponentsBuilder.fromUriString(requests).queryParams(map).build().toUriString();
		return returnRequests(url);
	}

	/**
	 * Получение заявок по статусу со стандартными параметрами
	 * @param status статус заявок
	 * @return список заявок
	 */
	public List<RequestDto> getRequestsByStatus(String status) {
		MultiValueMap<String, String> map = getMap();
		map.add(param_status, status);
		String url = UriComponentsBuilder.fromUriString(requests).queryParams(map).build().toUriString();
		return returnRequests(url);
	}

	/**
	 * Получение заявки по id
	 * @param id id заявки
	 * @return заявка
	 */
	public RequestDto getRequest(Long id) {
		String url = String.format(request_id, id);
		return returnRequest(url);
	}

	/**
	 * Получение заявки по guid
	 * @param guid guid заявки
	 * @return заявка
	 */
	public RequestDto getRequestByGuid(String guid) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_guid, guid);
		String url = UriComponentsBuilder.fromUriString(requests).queryParams(map).build().toUriString();
		return returnRequest(url);
	}

	/**
	 * Получение заявок, привязанных по указанной работе
	 * @param id id работы
	 * @return список работ
	 */
	public List<RequestDto> getWorkRequests(Long id) {
		String url = String.format(work_request, id);
		return returnRequests(url);
	}

	public RequestDto getWorkRequest(Long id) {
		List<RequestDto> workRequests = getWorkRequests(id);
		if (workRequests.isEmpty())
			return null;
		else
			return workRequests.get(0);
	}

	/**
	 * Удаление заявки
	 * @param id id заявки
	 */
	public void deleteRequest(Long id) {
		String url = String.format(request_id, id);
		deleteEntity(url);
	}

	/**
	 * Сохранение заявки
	 * @param requestDto заявка
	 * @return сохраненная заявка
	 */
	public RequestDto saveRequest(RequestDto requestDto) {
		if (requestDto.getId() == null) {
			return postRequest(requests, requestDto);
		} else {
			String url = String.format(request_id, requestDto.getId());
			return putRequest(url, requestDto);
		}
	}

	/**
	 * Смена статуса заявки
	 * @param id id заявки
	 * @param status новый статус заявки
	 * @return обновленная заявка
	 */
	public RequestDto changeRequestStatus(Long id, String status) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_status, status);
		String url = String.format(request_id, id);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return putRequest(url, null);
	}

	public RequestDto closeRequest(RequestDto requestDto) {
		RequestDto request = changeRequestStatus(requestDto.getId(), RequestDto.Status.CLOSED.getCode());
		if (request != null && request.getExternalNumber() != null) {
			IncInfo info = createInfo(request, true);
			sendInfoToHotLine(info);
		}
		return request;
	}

	/**
	 * Получение вложений указанной заявки
	 * @param id id заявки
	 * @return список вложений
	 */
	public List<AttachmentDto> getRequestAttachments(Long id) {
		String url = String.format(request_attachments, id);
		return returnAttachments(url);
	}

	/**
	 * Прикрепление вложения к заявке
	 * @param id id заявки
	 * @param file выбранный файл
	 * @param type тип вложения
	 * @param description описание вложения
	 * @return сохраненное вложение
	 */
	public AttachmentDto postRequestAttachment(Long id, File file, String fileName, String type, String description, Boolean mobile) {
		String url = String.format(request_attachments, id);
		return postAttachment(url, file, fileName, type, description, mobile);
	}

	/**
	 * Удаление вложения заявки
	 * @param requestId id заявки
	 * @param attachmentId id вложения
	 */
	public void deleteRequestAttachment(Long requestId, Long attachmentId) {
		String url = String.format(request_attachment_id, requestId, attachmentId);
		deleteEntity(url);
	}

	/**
	 * Получение данных вложения заявки
	 * @param requestId id заявки
	 * @param attachmentId id вложения
	 * @return данные вложения в байтовой форме
	 */
	public AttachmentContentDto getRequestAttachmentContent(Long requestId, Long attachmentId) {
		String url = String.format(request_attachment_content, requestId, attachmentId);
		return returnAttachmentContent(url);
	}

	/**
	 * Получение комментариев к указанной заявке
	 * @param id id заявки
	 * @return список комментариев
	 */
	public List<CommentDto> getRequestComments(Long id) {
		String url = String.format(request_comments, id);
		return returnComments(url);
	}
	/**
	 * Получение элементов обслуживания указанной работы
	 * @param id id заявки
	 * @return список комментариев
	 */
	public List<ElementDto> getElements(Long id) {
		String url = String.format(work_elements, id);
		return returnElements(url);
	}

	/**
	 * Создание комментария к указанной заявке
	 * @param id id заявки
	 * @param commentRequestDto комментарий
	 * @return сохраненный комментарий
	 */
	public List<CommentDto> saveRequestComment(Long id, CommentDto commentRequestDto) {
		String url = String.format(request_comments, id);
		//return postRequestComment(url, commentRequestDto);
		HttpEntity<List<CommentDto>> httpEntity = new HttpEntity<>(Collections.singletonList(commentRequestDto));
		return makeListRequest(url, HttpMethod.POST, httpEntity, CommentDto.class);
	}

	/**
	 * Получение работ, созданных по указанной заявке
	 * @param id id заявки
	 * @return список работ
	 */
	public List<WorkDto> getRequestWorks(Long id) {
		String url = String.format(request_works, id);
		return returnWorks(url);
	}

	/**
	 * Функция для создания сессии при авторизации
	 * @return информация о сессии
	 */
	public SessionDto setSession() {
		try {
			ResponseEntity<SessionDto> entity = initRestTemplate().postForEntity(sessions, null, SessionDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(sessions, e);
			return null;
		}
	}

	/**
	 * Заканчивает текущую сессию
	 */
	public void deleteSession() {
		deleteEntity(sessions);
	}

	// + - @GetMapping({"sla/reaction-time"})
	// + - @GetMapping({"sla/control-accuracy"})
	// + - @GetMapping({"sla/user-action"})
	// + - @GetMapping({"sla/work-code"})

	/**
	 * Получение данных настройки для формы "Точность контроля"
	 * @return список данных
	 */
	public List<SlaControlAccuracyDto> getSlaControlAccuracy() {
		try {
			ResponseEntity<SlaControlAccuracyDto[]> entity = initRestTemplate().getForEntity(sla_control_accuracy, SlaControlAccuracyDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(sla_control_accuracy, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение данных настройки для формы "Сроки закрытия заявок в СОУ"
	 * @return список данных
	 */
	public List<SlaReactionTimeDto> getSlaReactionTime() {
		try {
			ResponseEntity<SlaReactionTimeDto[]> entity = initRestTemplate().getForEntity(sla_reaction_time, SlaReactionTimeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(sla_reaction_time, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение данных настройки для формы "Контроль действий пользователей"
	 * @return список данных
	 */
	public List<SlaUserActionDto> getSlaUserActions() {
		try {
			ResponseEntity<SlaUserActionDto[]> entity = initRestTemplate().getForEntity(sla_user_action, SlaUserActionDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(sla_user_action, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение данных настройки для формы "Перечень работ по кодам заявок ГЛ"
	 * @return список данных
	 */
	public List<SlaWorkCodeDto> getSlaWorkCodes() {
		try {
			ResponseEntity<SlaWorkCodeDto[]> entity = initRestTemplate().getForEntity(sla_work_code, SlaWorkCodeDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(sla_work_code, e);
			return Collections.emptyList();
		}
	}

	// + - @GetMapping({"sources"})
	// + - @GetMapping({"sources/requests"})
	// + - @GetMapping({"sources/works"})
	// + - @GetMapping({"sources/toir"})

	/**
	 * Получение всех возможных источников
	 * @return список источников
	 */
	public List<SourceDto> getSources() {
		return returnSources(sources);
	}

	/**
	 * Получение источников для заявок
	 * @return список источников
	 */
	public List<SourceDto> getRequestSources() {
		return returnSources(sources_requests);
	}

	/**
	 * Получение источников из справочника ТОИР
	 * @return список источников
	 */
	public List<SourceDto> getToirSources() {
		return returnSources(sources_toir);
	}

	/**
	 * Получение источников для работ
	 * @return список источников
	 */
	public List<SourceDto> getWorkSources() {
		return returnSources(sources_works);
	}

	// + - @GetMapping({"statuses"})
	// + - @GetMapping({"statuses/requests"})
	// + - @GetMapping({"statuses/works"})

	/**
	 * Получение всех возможных статусов
	 * @return список статусов
	 */
	public List<StatusDto> getStatuses() {
		return returnStatuses(statuses);
	}

	/**
	 * Получение всех ГЛ статусов
	 * @return список статусов
	 */
	public List<StatusDto> getHLStatuses() {
		return returnStatuses(status_hl);
	}

	/**
	 * Получение статусов для заявок
	 * @return список статусов
	 */
	public List<StatusDto> getRequestStatuses() {
		return returnStatuses(request_statuses);
	}

	/**
	 * Получение статусов для работ
	 * @return список статусов
	 */
	public List<StatusDto> getWorkStatuses() {
		return returnStatuses(work_statuses);
	}

	/**
	 * Получение статусов для этапов работ
	 * @return список статусов
	 */
	public List<StatusDto> getWorkItemStatuses() {
		return returnStatuses(workItem_statuses);
	}

	// + - @GetMapping({"types-works"})
	// + - @GetMapping({"types-works/{id}"})
	// + - @GetMapping({"types-works/{id}/items"})

	/**
	 * Получение типов работ из справочника
	 * @return список типов работ
	 */
	public List<TypeWorkDto> getTypeWorks() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(types_works).queryParams(map).build().toUriString();
		try {
		 	ResponseEntity<TypeWorkDto[]> entity = initRestTemplate().getForEntity(url, TypeWorkDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение типа работы по id
	 * @param id id типа работы
	 * @return тип работы
	 */
	public TypeWorkDto getTypeWork(Long id) {
		String url = String.format(types_work_id, id);
		try {
			ResponseEntity<TypeWorkDto> entity = initRestTemplate().getForEntity(url, TypeWorkDto.class);
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	/**
	 * Получение этапов для указанного типа работы
	 * @param id id типа работы
	 * @return список справочных этапов
	 */
	public List<TypeWorkItemDto> getTypeWorkItems(Long id) {
		String url = String.format(types_work_items, id);
		try {
			ResponseEntity<TypeWorkItemDto[]> entity = initRestTemplate().getForEntity(url, TypeWorkItemDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// + - @GetMapping({"urgencies"})
	// + - @GetMapping({"urgencies/requests"})

	/**
	 * Получение всех важностей
	 * @return список важностей
	 */
	public List<UrgencyDto> getUrgencyList() {
		return returnUrgencies(urgencies);
	}

	/**
	 * Получение важностей для заявок
	 * @return список важностей
	 */
	public List<UrgencyDto> getRequestUrgencyList() {
		return returnUrgencies(urgency_requests);
	}

	// + - @GetMapping({"users"})
	// + - @GetMapping({"users/{id}"})
	// + - @GetMapping(value = {"users"}, params = {"login"})
	// + - @PostMapping({"users/"})
	// + - @PutMapping({"users/{id}"})
	// + - @DeleteMapping({"users/{id}"})
	// + - @PostMapping({"users/{id}/photo"})
	// + - @GetMapping({"users/{id}/roles"})
	// + - @GetMapping({"users/{id}/groups"})
	// + - @GetMapping({"users/{idUser}/userData/{key}"})
	// + - @PostMapping({"users/{idUser}/userData"})
	// + - @PutMapping({"users/{idUser}/userData/{key}"})
	// + - @DeleteMapping({"users/{idUser}/userData/{key}"})
	// + - @GetMapping({"users/{id}/brigades"})

	/**
	 * Получение пользователей по стандартным параметрам
	 * @return список пользователей
	 */
	public List<UserDto> getUsers() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(users).queryParams(map).build().toUriString();
		return returnUsers(url);
	}
    /**
     * Добавление пользователя
     * @param user пользователь
     * @return добавленный пользователей (со сгенерированным id)
     */
    //TODO Выдает 500 при попытке добавить. Отправил Рустаму ошибку
    public UserDto addUser(UserDto user) {
        String url = users;
        ResponseEntity<UserDto> entity = initRestTemplate().postForEntity(url, user, UserDto.class);
        return entity.getBody();
    }
    /**
     * Редактирование пользователя
     * @param user пользователь
     */
    public void updateUser(UserDto user) {
        String url = users+"/"+user.getId();
        initRestTemplate().put(url, user);
    }
    /**
     * Удаление пользователя
     * @param user пользователь
     */
    public void deleteUser(UserDto user) {
        String url = users+"/"+user.getId();
        initRestTemplate().delete(url);
    }
	/**
	 * Удаление пользователя
	 * @param userId id пользователя
	 */
	public void deleteUser(Long userId) {
		String url = users+"/"+userId;
		initRestTemplate().delete(url);
	}

	/**
	 * Получение пользователя по логину (используется для авторизации)
	 * @param login логин пользователя
	 * @return пользователь
	 */
	public UserDto getUserByLogin(String login) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_login, login);
		String url = UriComponentsBuilder.fromUriString(users).queryParams(map).build().toUriString();
		return returnUser(url);
	}

	/**
	 * Получение организаций пользователя
	 * @param id id пользователя
	 * @return список организаций
	 */
	public List<OrganizationDto> getUserOrganizations(Long id) {
		String url = String.format(user_organizations, id);
		return returnOrganizations(url);
	}

	public List<OrganizationDto> getCurrentUserOrganizations() {
		UserDto currentUser = BrigadesUI.getCurrentUser();
		return currentUser == null ? Collections.emptyList() : getUserOrganizations(currentUser.getId());
	}

	/**
	 * Получение ролей пользователя
	 * @param id id пользователя
	 * @return список ролей
	 */
	public List<RoleDto> getUserRoles(Long id) {
		String url = String.format(user_roles, id);

		/*Map<String, RoleDto> lastnameToRole = new HashMap<>();
		lastnameToRole.put("Солнцева", new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));
		lastnameToRole.put("Замарина", new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));;
		lastnameToRole.put("Терентьева", new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));
		lastnameToRole.put("Ельникова", new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));;
		lastnameToRole.put("Аристархова", new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));
		lastnameToRole.put("Семенова", new RoleDto("Диспетчер СОУ","Диспетчер СОУ"));

		lastnameToRole.put("Галяс", new RoleDto("Руководитель СОУ","Руководитель СОУ"));
		lastnameToRole.put("Крайнeв", new RoleDto("Руководитель СОУ","Руководитель СОУ"));
		lastnameToRole.put("Васильева", new RoleDto("Руководитель СОУ","Руководитель СОУ"));
		lastnameToRole.put("Кадырова", new RoleDto("Руководитель СОУ","Руководитель СОУ"));
		lastnameToRole.put("Бодрова", new RoleDto("Руководитель СОУ","Руководитель СОУ"));

		lastnameToRole.put("Каюров", new RoleDto("Мастер бригады","Мастер бригады"));
		lastnameToRole.put("Канаштаров", new RoleDto("Мастер бригады","Мастер бригады"));
		lastnameToRole.put("Николаев", new RoleDto("Мастер бригады","Мастер бригады"));
		lastnameToRole.put("Таммине", new RoleDto("Мастер бригады","Мастер бригады"));
		lastnameToRole.put("Усов", new RoleDto("Мастер бригады","Мастер бригады"));
		lastnameToRole.put("Сутов", new RoleDto("Мастер бригады","Мастер бригады"));
		lastnameToRole.put("Мозгалев", new RoleDto("Мастер бригады","Мастер бригады"));

		lastnameToRole.put("Леута", new RoleDto("Руководитель РВ","Руководитель РВ"));
		lastnameToRole.put("Васильев", new RoleDto("Руководитель РВ","Руководитель РВ"));


		lastnameToRole.put("Савин", new RoleDto("Старший мастер РВ","Старший мастер РВ"));
		lastnameToRole.put("Сторчак", new RoleDto("Старший мастер РВ","Старший мастер РВ"));*/
		try {
			//getLogger().debug(initRestTemplate().getForEntity(url, String.class).getBody());
			ResponseEntity<RoleDto[]> entity = initRestTemplate().getForEntity(url, RoleDto[].class);
			//getLogger().debug(Arrays.asList(entity.getBody()));
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			/*String lastname = BrigadesUI.getRestTemplate().getUsers().stream().filter(e1->e1.getId().equals(id)).findFirst().get().getLastName();
			if (lastnameToRole.containsKey(lastname)){
				return Arrays.asList(lastnameToRole.get(lastname));
			}*/
			return Collections.emptyList();
		}
		/*Роли в системе:
	Мастер бригады
	Старший мастер РВ
	Руководитель РВ
	Диспетчер СОУ
	Руководитель СОУ
	Администратор

	Гость*/
	}
	public List<RoleDto> getCurrentUserRoles() {
		UserDto currentUser = BrigadesUI.getCurrentUser();
		return currentUser == null ? Collections.emptyList() : getUserRoles(currentUser.getId());
	}

	/**
	 * Добавление роли пользователя
	 * @param id id пользователя
	 * @param role роль
	 */
	//TODO-K Дописать
	public void addUserRole(Long id, RoleDto role) {
		String url = String.format(user_roles, id);


		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity entity = new HttpEntity(objectMapper.writeValueAsString(new RoleDto[]{role}), headers);
			getLogger().debug(entity.getHeaders().toString());
			getLogger().debug(entity.getBody().toString());
			initRestTemplate().exchange(url, HttpMethod.POST, entity,  RoleDto[].class);
		} catch (RestClientException e) {
			getLogger().debug(e.toString());
			getLogger().debug(e.getLocalizedMessage());
			getLogger().debug(e.getMessage());
			logExceptions(url, e);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	// + - @GetMapping({"user-groups"})
	// + - @GetMapping({"user-groups/{id}"})
	// + - @PostMapping({"user-groups"})
	// + - @PutMapping({"user-groups/{id}"})
	// + - @DeleteMapping({"user-groups/{id}"})
	// + - @GetMapping({"users/{id}/users"})


	// + - @GetMapping({"vehicles"})
	// + - @GetMapping({"vehicles/types"})
	// + - @GetMapping({"vehicles/models"})
	// + - @GetMapping({"vehicles/numbers"})
	// + - @GetMapping({"vehicles/{id}/position"})
	// + - @GetMapping({"vehicles/{id}/tracks"})
	// + - @GetMapping(value = {"vehicles"}, params = {"guid"})

	/**
	 * Получение транспорта по стандартным параметрам
	 * @return список транспорта
	 */
	public List<VehicleDto> getVehicles() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(vehicles).queryParams(map).build().toUriString();
		return returnVehicles(url);
	}

	/**
	 * Получение моделей транспорта
	 * @return список моделей транспорта
	 */
	public List<VehicleDto> getVehicleModels() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(vehicles_models).queryParams(map).build().toUriString();
		return returnVehicles(url);
	}

	/**
	 * Получение номеров транспорта
	 * @return список номеров транспорта
	 */
	public List<VehicleDto> getVehicleNumbers() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(vehicles_numbers).queryParams(map).build().toUriString();
		return returnVehicles(url);
	}

	/**
	 * Получение типов транспорта
	 * @return список типов транспорта
	 */
	public List<VehicleDto> getVehicleTypes() {
		MultiValueMap<String, String> map = getMap();
		String url = UriComponentsBuilder.fromUriString(vehicles_types).queryParams(map).build().toUriString();
		return returnVehicles(url);
	}

	public VehicleDto getVehicleByNumber(String number) {
		return getVehicles().stream().filter(vehicleDto ->
				vehicleDto.getNumber().equals(number)).findFirst().orElse(null);
	}

	/**
	 * Получение т/с по guid
	 * @param guid guid транспорта
	 * @return т/с
	 */
	public VehicleDto getVehicleByGuid(String guid) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_guid, guid);
		String url = UriComponentsBuilder.fromUriString(vehicles).queryParams(map).build().toUriString();
		return returnVehicle(url);
	}

	/**
	 * Получение текущего местоположения указанного т/с
	 * @param id id т/с
	 * @return местоположение
	 */
	public PositionDto getVehiclePosition(Long id) {
		String url = String.format(vehicle_position, id);
		return returnPosition(url);
	}

	/**
	 * Получение истории местоположений указанного т/с за указанный период
	 * @param id id т/с
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список местоположений
	 */
	public List<PositionDto> getVehicleTracks(Long id, ZonedDateTime start, ZonedDateTime end) {
		String url = String.format(vehicle_tracks, id);
		MultiValueMap<String, String> map = getMap(start, end);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return returnPositions(url);
	}

	public List<PositionDto> getVehicleTracks(Long id) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getVehicleTracks(id, dateInterval.getStart(), dateInterval.getEnd());
	}

	/**
	 * Получение истории местоположений указанного т/с за период из настроек
	 * @param number номер т/с
	 * @return список местоположений
	 */
	public List<PositionDto> getVehicleTracks(String number) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		String url = "http://" + factory.getTcIp() + ":" + factory.getTcPort() + "/api/v1/vehicles/tracks";
		MultiValueMap<String, String> map = getMap(dateInterval.getStart(), dateInterval.getEnd());
		map.add(param_number, number);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return returnPositions(url);
	}

	// + + @GetMapping(value = {"works"}, params = {"from", "to", "skip", "limit"})
	// + - @GetMapping(value = {"works"}, params = {"status", "from", "to", "skip", "limit"})
	// + - @GetMapping(value = {"works"}, params = {"prior", "from", "to", "skip", "limit"})
	// + + @PostMapping({"works"})
	// + + @PutMapping({"works/{id}"})
	// + + @DeleteMapping({"works/{id}"})
	// + - @PutMapping(value = {"works/{id}"}, params = {"status"})
	// + - @PutMapping({"works/{id}/brigades"})
	// + + @GetMapping({"works/{id}/requests"})
	// + - @GetMapping({"works/{id}/requests/{id-request}"})
	// + + @PostMapping({"works/{id}/requests"})
	// + - @DeleteMapping({"works/{id}/requests"})
	// + + @GetMapping({"works/{id}/work-items"})
	// + - @GetMapping({"works/{id}/work-items/{id-work-item}"})
	// + + @PostMapping({"works/{id}/work-items"})
	// + - @DeleteMapping({"works/{id}/work-items/roots"})
	// + + @GetMapping({"works/{id}/attachments"})
	// + + @PostMapping(value = {"works/{id}/attachments"}, params = {"type", "description", "file"})
	// + + @GetMapping({"works/{id}/attachments/{id-attachment}"})
	// + - @PutMapping(value = {"works/{id}/attachments/{id-attachment}"}, params = {"commentary", "gati", "regulations", "calendar"})
	// + - @DeleteMapping({"works/{id}/attachments/{id-attachment}"})
	// + + @GetMapping({"works/{id}/attachments/{id-attachment}/content"})
	// + - @GetMapping({"works/{id}/assets"})
	// + - @GetMapping({"works/{id}/assets/{id-asset}"})
	// + - @PostMapping({"works/{id}/assets"})
	// + - @DeleteMapping({"works/{id}/assets"})
	// + - @GetMapping({"works/{id}/comments"})
	// + - @PostMapping({"works/{id}/comments"})
	// + - @GetMapping({"works/{id}/comments/{id-comment}"})
	// + - @DeleteMapping({"works/{id}/comments/{id-comment}"})
	// + - @GetMapping({"works/{id}/inventory"})
	// + - @PostMapping({"works/{id}/inventory"})
	// + - @PutMapping({"works/{id}/inventory/{id-inventory-work}"})
	// + - @DeleteMapping({"works/{id}/inventory/{id-inventory}"})
	// + - @GetMapping({"works/{id}/equipments"})
	// + - @PostMapping({"works/{id}/equipments"})
	// + - @PutMapping({"works/{id}/equipments/{id-equipment-work}"})
	// + - @DeleteMapping({"works/{id}/equipments/{id-equipment-work}"})
	// + - @GetMapping({"works/{id}/mechanization"})
	// + - @PostMapping({"works/{id}/mechanization"})
	// + - @PutMapping({"works/{id}/mechanization/{id-mechanization-work}"})
	// + - @DeleteMapping({"works/{id}/mechanization/{id-mechanization-work}"})
	// + + @GetMapping({"works/statuses"})
	// + - @GetMapping({"works/{id}/employees"})
	// + - @PostMapping({"works/{id}/employees"})
	// + - @PutMapping({"works/{id}/employees"})
	// - - @DeleteMapping({"works/{id}/employees/{id-employee-work}"})
	// + + @GetMapping(value = {"works"}, params = {"guid"})

	/**
	 * Получение работ за указанный период по стандартным параметрам
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return список работ
	 */
	public List<WorkDto> getWorks(ZonedDateTime start, ZonedDateTime end) {
		MultiValueMap<String, String> map = getMap(start, end);
		String url = UriComponentsBuilder.fromUriString(works).queryParams(map).build().toUriString();
		return returnWorks(url);
	}
	public List<WorkDto> getAllWorks(ZonedDateTime start) {
		MultiValueMap<String, String> map = getMap(start, null);
		map.remove(null);
		String url = UriComponentsBuilder.fromUriString(works).queryParams(map).build().toUriString();
		return returnWorks(url);
	}

	/**
	 * Получение работ по стандартным параметрам
	 * @return список работ
	 */
	public List<WorkDto> getWorks() {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		return getWorks(dateInterval.getStart(), dateInterval.getEnd());
	}
	public List<WorkDto> getWorks(int dayOffset) {
		DateInterval dateInterval = DatesUtil.getViewDataPeriod();
		dateInterval.setStart(dateInterval.getStart().minusDays(dayOffset));
		dateInterval.setEnd(dateInterval.getEnd());
		return getWorks(dateInterval.getStart(), dateInterval.getEnd());
	}

	//Работники работавшие работу
	public List<EmployeeBrigadeDto> getWorkEmployees(Long workId){
		String url = String.format(work_employees,workId);
		return returnBrigadeEmployees(url);
	}

	/**
	 * Получение работ для табличных данных
	 * @param skip пропуск строк
	 * @param limit количество строк
	 * @return список работ и их общее количество
	 */
	public PagesDto<WorkJournalDto> getWorksForJournal(Integer skip, Integer limit) {
		MultiValueMap<String, String> map = getMap(DatesUtil.setNullDateTime(), null, skip, limit);
		String url = UriComponentsBuilder.fromUriString(works).queryParams(map).build().toUriString();
		return returnWorkJournalPage(url);
	}

	/**
	 * Получение общего количества работ
	 * @return общее количество работ
	 */
	public Long countWorks() {
		PagesDto<WorkJournalDto> journal = getWorksForJournal(0, 1);
		return journal.getCount();
	}

	/**
	 * Получение этапов для указанной работы
	 * @param id id работы
	 * @return список этапов
	 */
	public List<WorkItemDto> getWorkItems(Long id) {
		String url = String.format(work_items, id);
		return returnWorkItems(url);
	}

	/**
	 * Получение работы по id
	 * @param id id работы
	 * @return работа
	 */
	public WorkDto getWork(Long id) {
		String url = String.format(work_id, id);
		return returnWork(url);
	}

	public WorkDto getWorkByGuid(String guid) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_guid, guid);
		String url = UriComponentsBuilder.fromUriString(works).queryParams(map).build().toUriString();
		return returnWork(url);
	}

	/**
	 * Удаление работы
	 * @param id id работы
	 */
	public void deleteWork(Long id) {
		String url = String.format(work_id, id);
		deleteEntity(url);
	}

	/**
	 * Сохранение работы (создание новой или обновление существующей)
	 * @param workDto работа
	 * @return сохраненная работа
	 */
	public WorkDto saveWork(WorkDto workDto) {
		if (workDto.getId() == null) {
			return postWork(works, workDto);
		} else {
			String url = String.format(work_id, workDto.getId());
			return putWork(url, workDto);
		}
	}

	/**
	 * Смена статуса работы
	 * @param id id работы
	 * @param status новый статус
	 * @return обновленная работа
	 */
	public WorkDto changeWorkStatus(Long id, String status) {
		MultiValueMap<String, String> map = createMap();
		map.add(param_status, status);
		String url = String.format(work_id, id);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return putWork(url, null);
	}

	/**
	 * Сохранение этапа работы (создание нового или обновление существующего)
	 * @param workItemDto этап работы
	 * @param parentId id работы
	 * @return сохраненный этап работы
	 */
	public WorkItemDto saveWorkItem(WorkItemDto workItemDto, Long parentId) {
		String url = String.format(work_items, parentId);
		if (workItemDto.getId() == null) {
			return postWorkItem(url, workItemDto);
		} else {
			url = url + "/" + workItemDto.getId();
			putWorkItem(url, workItemDto);
			return workItemDto;
		}
	}

	/**
	 * Сохранение этапов работы (создание новых)
	 * @param workItemDtoList список этапов работы
	 * @param parentId id работы
	 */
	public void saveWorkItems(List<WorkItemDto> workItemDtoList, Long parentId) {
		String url = String.format(work_items, parentId);
		postWorkItems(url, workItemDtoList);
	}

	/**
	 * Сохранение этапов работы (обновление существующих)
	 * @param workItemDtoList список этапов работы
	 * @param parentId id работы
	 */
	public void updateWorkItems(List<WorkItemDto> workItemDtoList, Long parentId) {
		String url = String.format(work_items, parentId);
		putWorkItems(url, workItemDtoList);
	}

	/**
	 * Привязка работы к заявке
	 * @param workDto работа
	 * @param requestDto заявка
	 */
	public void attachWorkToRequest(WorkDto workDto, RequestDto requestDto) {
		String url = String.format(work_request, workDto.getId());
		//postRequest(url, requestDto);
		postRequests(url, Collections.singletonList(requestDto));
	}

	/**
	 * Привязка работы к бригаде
	 * @param workDto работа
	 * @param brigadeDto бригада
	 */
	public void attachWorkToBrigade(WorkDto workDto, BrigadeDto brigadeDto) {
		String url = String.format(work_brigades, workDto.getId());
		putWorkBrigade(url, brigadeDto);
	}

	/**
	 * Привязка спецтехники к работе
	 * @param equipmentWorkDto спецтехника
	 * @return список сохраненной техники
	 */
	public List<EquipmentWorkDto> attachVehicleToWork(EquipmentWorkDto equipmentWorkDto) {
		String url = String.format(work_equipments, equipmentWorkDto.getIdWork());
		HttpEntity<List<EquipmentWorkDto>> httpEntity = new HttpEntity<>(Collections.singletonList(equipmentWorkDto));
		return makeListRequest(url, HttpMethod.POST, httpEntity, EquipmentWorkDto.class);
	}

	public void detachVehicleFromWork(EquipmentWorkDto equipmentWorkDto) {
		String url = String.format(work_equipment_id, equipmentWorkDto.getIdWork(), equipmentWorkDto.getId());
		deleteEntity(url);
	}

	/**
	 * Получение вложений к указанной работе
	 * @param id id работы
	 * @return список вложений
	 */
	public List<AttachmentDto> getWorkAttachments(Long id) {
		String url = String.format(work_attachments, id);
		return returnAttachments(url);
	}

	/**
	 * Прикрепление вложения к работе
	 * @param id id работы
	 * @param file выбранный файл
	 * @param type тип вложения
	 * @param description описание вложения
	 * @return сохраненное вложение
	 */
	public AttachmentDto postWorkAttachment(Long id, File file, String fileName, String type, String description, Boolean mobile) {
		String url = String.format(work_attachments, id);
		return postAttachment(url, file, fileName, type, description, mobile);
	}

	/**
	 * Удаление вложения работы
	 * @param workId id работы
	 * @param attachmentId id вложения
	 */
	public void deleteWorkAttachment(Long workId, Long attachmentId) {
		String url = String.format(work_attachment_id, workId, attachmentId);
		deleteEntity(url);
	}

	/**
	 * Получение данных вложения работы
	 * @param workId id работы
	 * @param attachmentId id вложения
	 * @return данные вложения в байтовой форме
	 */
	public AttachmentContentDto getWorkAttachmentContent(Long workId, Long attachmentId) {
		String url = String.format(work_attachment_content, workId, attachmentId);
		return returnAttachmentContent(url);
	}

	/**
	 * Получение комментариев к указанной работе
	 * @param id id работы
	 * @return список комментариев
	 */
	public List<CommentDto> getWorkComments(Long id) {
		String url = String.format(work_comments, id);
		return returnComments(url);
	}

	public List<CommentDto> saveWorkComment(Long id, CommentDto commentDto) {
		String url = String.format(work_comments, id);
		HttpEntity<List<CommentDto>> httpEntity = new HttpEntity<>(Collections.singletonList(commentDto));
		return makeListRequest(url, HttpMethod.POST, httpEntity, CommentDto.class);
	}

	/**
	 * Получение спецтехники к указанной работе
	 * @param id id работы
	 * @return список спецтехники
	 */
	public List<EquipmentWorkDto> getWorkEquipments(Long id) {
		String url = String.format(work_equipments, id);
		try {
			ResponseEntity<EquipmentWorkDto[]> entity = initRestTemplate().getForEntity(url, EquipmentWorkDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение материалов к указанной работе
	 * @param id id работы
	 * @return список материалов
	 */
	public List<InventoryWorkDto> getWorkInventory(Long id) {
		String url = String.format(work_inventory, id);
		try {
			ResponseEntity<InventoryWorkDto[]> entity = initRestTemplate().getForEntity(url, InventoryWorkDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	/**
	 * Получение СММ к указанной работе
	 * @param id id работы
	 * @return список спецтехники
	 */
	public List<MechanizationWorkDto> getWorkMechanization(Long id) {
		String url = String.format(work_mechanization, id);
		try {
			ResponseEntity<MechanizationWorkDto[]> entity = initRestTemplate().getForEntity(url, MechanizationWorkDto[].class);
			return Arrays.asList(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
			return Collections.emptyList();
		}
	}

	// - - @GetMapping({"work-items"})
	// - - @GetMapping({"work-items/{id}/childs"})
	// + - @PutMapping(value = {"work-items/{id}"}, params = {"status"})
	// + - @GetMapping({"work-items/statuses"})

	// Взаимодействия с модулями интеграции

	private <T> void sendToIntegration(String url, T value) {
		try {
			getLogger().debug(StringUtil.writeValueAsString(value));
			ResponseEntity<String> entity = initRestTemplate().postForEntity(url, value, String.class);
			getLogger().debug(entity.getBody());
		} catch (RestClientException e) {
			logExceptions(url, e);
		}
	}

	public void sendAttToHotLine(IncAttach attDto) {
		String url = "http://" + factory.getHlIp() + ":" + factory.getHlPort() + "/api/v1/incs/attach";
		sendToIntegration(url, attDto);
	}

	public void sendInfoToHotLine(IncInfo infoDto) {
		String url = "http://" + factory.getHlIp() + ":" + factory.getHlPort() + "/api/v1/incs/info";
		sendToIntegration(url, infoDto);
	}

	public void sendItemToHotLine(IncItem itemDto) {
		String url = "http://" + factory.getHlIp() + ":" + factory.getHlPort() + "/api/v1/incs/item";
		sendToIntegration(url, itemDto);
	}

	public void sendNoteToHotLine(IncNote noteDto) {
		String url = "http://" + factory.getHlIp() + ":" + factory.getHlPort() + "/api/v1/incs/note";
		sendToIntegration(url, noteDto);
	}

	public IncInfo createInfo(RequestDto requestDto, Boolean close) {
		IncInfo incInfo = new IncInfo();
		incInfo.setpIncidentId(requestDto.getNumber());
		incInfo.setpIncidentNumber(requestDto.getExternalNumber());
		if (close) {
			incInfo.setpStatusId("106");
			incInfo.setpStatusName("Закрыта в СОУ");
		} else {
			incInfo.setpStatusName(requestDto.getStatusHL());
		}
		incInfo.setpFilialNum(requestDto.getAccessNumber());
		incInfo.setpAddAdrInfo(requestDto.getUpdatedAddress());
		if (requestDto.getCloseDate() != null)
			incInfo.setpClosedDate(DatesUtil.formatZoned(requestDto.getCloseDate()));
		incInfo.setpUpdatedByFio(BrigadesUI.getCurrentUser().getFullName());
		return incInfo;
	}

	private String getFromTOIR(String url) {
		try {
			ResponseEntity<String> entity = initRestTemplate().getForEntity(url, String.class);
			getLogger().debug(entity.getBody());
			return entity.getBody();
		} catch (RestClientException e) {
			logExceptions(url, e);
			return null;
		}
	}

	public TOIRCollectionDto getTOIRCollection(String code) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/collection";
		MultiValueMap<String, String> map = createMap();
		map.add(param_code, code);
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		return makeEntityRequest(url, HttpMethod.GET, null, TOIRCollectionDto.class);
	}

	public void sendHeaderToTOIR(WorkHeadDto headDto) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/workhead";
		sendToIntegration(url, headDto);
	}

	public void sendEOToTOIR(EODto eoDto) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/eo";
		sendToIntegration(url, eoDto);
	}

	public void sendMetricToTOIR(MetricDto metricDto) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/metric";
		sendToIntegration(url, metricDto);
	}

	public void sendOperationToTOIR(OperationDto operationDto) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/oprc";
		sendToIntegration(url, operationDto);
	}

	public void sendCalcToTOIR(String wip) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/workcalc/" + wip;
		sendToIntegration(url, wip);
	}

	public String getCalcNormFromTOIR(String wip) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/workcalc/" + wip;
		return getFromTOIR(url);
	}

	public String getCalcNormDetailFromTOIR(String wip) {
		String url = "http://" + factory.getToirIp() + ":" + factory.getToirPort() + "/api/v1/workcalc/" + wip + "/detail";
		return getFromTOIR(url);
	}
	public List<PhotoReportDto> getPhotoReport(ZonedDateTime from, ZonedDateTime to, BrigadeDto brigadeDto){
		String url = String.format(report_id,5);
		MultiValueMap<String, String> map = createMap();
		map.add(param_from, DatesUtil.formatToUTC(from));
		map.add(param_to, DatesUtil.formatToUTC(to));
		map.add("brigade", brigadeDto.getName());
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		ResponseEntity<PhotoReportDto[]> entity = initRestTemplate().getForEntity(url, PhotoReportDto[].class);

		return Arrays.asList(entity.getBody());
	}
	public List<UserWorksReportDto> getUserWorksReport(ZonedDateTime from, ZonedDateTime to, UserDto userDto){
		String url = String.format(report_id,4);
		MultiValueMap<String, String> map = createMap();
		map.add(param_from, DatesUtil.formatToUTC(from));
		map.add(param_to, DatesUtil.formatToUTC(to));
		map.add("id-user", userDto.getId()+"");
		url = UriComponentsBuilder.fromUriString(url).queryParams(map).build().toUriString();
		ResponseEntity<UserWorksReportDto[]> entity = initRestTemplate().getForEntity(url, UserWorksReportDto[].class);

		return Arrays.asList(entity.getBody());
	}
	/**
	 * Получение списка необходимости вложений для проблем
	 * @return список вложений
	 */
	public List<ProblemAttachmentDto> getProblemsAttachments() {
		String url = String.format(problems+"/attachments");
		return Arrays.asList(initRestTemplate().getForEntity(url, ProblemAttachmentDto[].class).getBody());
	}

	/**
	 * Добавить/изменить настройки вложения для проблемы
	 * @return
	 */
	public void sendProblemAttachments(Long problemId, String attachmentType, String require) {
		String url = String.format(problems+"/"+problemId+"/attachments");
		ProblemAttachmentDto problemAttachmentDto = new ProblemAttachmentDto();
		problemAttachmentDto.setAttachmentType(attachmentType);
		problemAttachmentDto.setRequire(require);
		problemAttachmentDto.setProblemCode(problemId);
		System.out.println(url);
		System.out.println(problemAttachmentDto.debugInfo());
		initRestTemplate().postForEntity(url, Arrays.asList(problemAttachmentDto), ProblemAttachmentDto[].class);
	}

}
