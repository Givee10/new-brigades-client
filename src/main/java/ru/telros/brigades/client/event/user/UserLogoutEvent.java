package ru.telros.brigades.client.event.user;

import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.event.ApplicationBaseEvent;

public class UserLogoutEvent extends ApplicationBaseEvent {
	private UserDto user;

	public UserLogoutEvent(Object source, UserDto user) {
		super(source);
		this.user = user;
	}

	public UserDto getUser() {
		return user;
	}
}
