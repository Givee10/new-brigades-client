package ru.telros.brigades.client.event;

import java.util.ArrayList;

/**
 * EntityEvent for Client UI EventBus
 */
public abstract class EntityEvent<T> extends ApplicationBaseEvent {
	private final T entity;

	public EntityEvent() {
		super(new ArrayList<>());
		this.entity = null;
	}

	public EntityEvent(final T entity) {
		super(entity);
		this.entity = entity;
	}

	public T getEntity() {
		return entity;
	}
}
