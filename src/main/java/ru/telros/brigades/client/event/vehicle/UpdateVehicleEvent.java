package ru.telros.brigades.client.event.vehicle;

import org.springframework.context.ApplicationEvent;

public class UpdateVehicleEvent extends ApplicationEvent {
	public UpdateVehicleEvent(Object source) {
		super(source);
	}
}
