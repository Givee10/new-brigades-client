package ru.telros.brigades.client.event.eventlog;

import ru.telros.brigades.client.dto.EventDto;
import ru.telros.brigades.client.event.EntityEvent;

public class UpdateEventLogEvent extends EntityEvent<EventDto> {

	public UpdateEventLogEvent(final EventDto event) {
		super(event);
	}

}
