package ru.telros.brigades.client.event;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;

/**
 * EventEntity bus for client UI
 */
public class BrigadesEventBus implements SubscriberExceptionHandler, HasLogger {
	private final EventBus eventBus = new EventBus(this);

	public static void post(final Object event) {
		BrigadesUI.getEventBus().eventBus.post(event);
	}

	public static void register(final Object object) {
		BrigadesUI.getEventBus().eventBus.register(object);
	}

	public static void unregister(final Object object) {
		BrigadesUI.getEventBus().eventBus.unregister(object);
	}

	@Override
	public final void handleException(final Throwable exception, final SubscriberExceptionContext context) {
		getLogger().error("Error in subscriber: " + context.getSubscriber(), exception);
	}
}
