package ru.telros.brigades.client.event.workorder;

import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.EntityEvent;

/**
 * Событие добавления/обновления работ(ы)
 */
@SuppressWarnings("serial")
public class UpdateWorkOrdersEvent extends EntityEvent<WorkDto> {

	public UpdateWorkOrdersEvent(final WorkDto workOrder) {
		super(workOrder);
	}

	public UpdateWorkOrdersEvent() {
		super();
	}

}
