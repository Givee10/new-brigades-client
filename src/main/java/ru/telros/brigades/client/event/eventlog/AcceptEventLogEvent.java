package ru.telros.brigades.client.event.eventlog;

import ru.telros.brigades.client.dto.EventWithActionDto;
import ru.telros.brigades.client.event.EntityEvent;

public class AcceptEventLogEvent extends EntityEvent<EventWithActionDto> {
	public AcceptEventLogEvent(final EventWithActionDto event) {
		super(event);
	}
}
