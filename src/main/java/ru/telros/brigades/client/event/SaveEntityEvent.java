package ru.telros.brigades.client.event;

public class SaveEntityEvent<T> extends EntityEvent {
	private boolean isPerformSave;

	@SuppressWarnings("unchecked")
	public SaveEntityEvent(T entity) {
		super(entity);
		isPerformSave = true;
	}

	public SaveEntityEvent(T entity, boolean isPerformSave) {
		this(entity);
		this.isPerformSave = isPerformSave;
	}

	public boolean isPerformSave() {
		return isPerformSave;
	}

	@SuppressWarnings("unchecked")
	public T getEntity() {
		return (T) super.getEntity();
	}

}
