package ru.telros.brigades.client.event;

import ru.telros.brigades.client.view.BrigadesViewType;

public class PostViewChangeEvent {
	private final BrigadesViewType view;

	public PostViewChangeEvent(final BrigadesViewType view) {
		this.view = view;
	}

	public BrigadesViewType getView() {
		return view;
	}
}
