package ru.telros.brigades.client.event.workgroup;

import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.event.EntityEvent;

/**
 * Событие добавления/обновления бригад(ы)
 */
@SuppressWarnings("serial")
public class UpdateWorkGroupsEvent extends EntityEvent<BrigadeDto> {

	public UpdateWorkGroupsEvent(final BrigadeDto workGroup) {
		super(workGroup);
	}

	public UpdateWorkGroupsEvent() {
		super();
	}

}
