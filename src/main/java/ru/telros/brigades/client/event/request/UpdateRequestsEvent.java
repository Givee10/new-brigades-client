package ru.telros.brigades.client.event.request;

import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.event.EntityEvent;

/**
 * Событие добавления/обновления заявки(ок)
 */
public class UpdateRequestsEvent extends EntityEvent<RequestDto> {

	public UpdateRequestsEvent(final RequestDto entity) {
		super(entity);
	}

	public UpdateRequestsEvent() {
		super();
	}

}
