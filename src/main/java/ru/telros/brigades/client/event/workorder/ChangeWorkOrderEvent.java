package ru.telros.brigades.client.event.workorder;

import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.EntityEvent;

public class ChangeWorkOrderEvent extends EntityEvent<WorkDto> {

	private Object sender;

	public ChangeWorkOrderEvent(final WorkDto workOrder) {
		this(workOrder, null);
	}

	public ChangeWorkOrderEvent(final WorkDto workOrder, final Object sender) {
		super(workOrder);
		this.sender = sender;
	}

	public Object getSender() {
		return sender;
	}

	public void setSender(Object sender) {
		this.sender = sender;
	}
}
