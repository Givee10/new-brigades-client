package ru.telros.brigades.client.event.request;

import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.event.EntityEvent;

/**
 * Событие закрытия заявки
 */
public class CloseRequestEvent extends EntityEvent<RequestDto> {

	public CloseRequestEvent(RequestDto entity) {
		super(entity);
	}

}
