package ru.telros.brigades.client.event.request;

import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.event.EntityEvent;

/**
 * Событие выбора заявки в журнале событий
 */
public class SelectRequestEvent extends EntityEvent<RequestDto> {

	private final Object sender;

	public SelectRequestEvent(final RequestDto request) {
		this(request, null);
	}

	public SelectRequestEvent(final RequestDto request, final Object sender) {
		super(request);
		this.sender = sender;
	}

	public Object getSender() {
		return sender;
	}
}
