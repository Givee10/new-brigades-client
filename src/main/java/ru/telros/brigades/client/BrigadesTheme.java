package ru.telros.brigades.client;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.themes.ValoTheme;

public class BrigadesTheme extends ValoTheme {
	public static final String THEME_NAME = "brigades";

	public static final String MENU_ITEMS = "valo-menuitems";
	public static final String MENU_ITEM_SELECTED = "selected";
	public static final String MENU_BADGEWRAPPER = "badgewrapper";
	public static final String GRID_CELL_ALIGN_RIGHT = "align-right";
	public static final String PANEL_CAPTION = "v-panel-caption";
	public static final String SINGLE_LINE = "v-singleline";
	public static final String INVISIBLE = "invisible";

	public static final String COLOR_INHERIT = "inherit";
	public static final String COLOR_PLAN = "#FFF8DC";
	public static final String COLOR_INFO = "#C8C8C8";
	public static final String COLOR_INWORK = "#aacbd9";
	public static final String COLOR_NORMAL = "#FFFFFF";
	public static final String COLOR_SUCCESS = "#cee0c0";
	public static final String COLOR_WARNING = "#fecc8c";
	public static final String COLOR_EMERGENCY = "#f78387";

	public static final String COLOR_DIR_IN = "#4ed986";
	public static final String COLOR_DIR_OUT = "#4682B4";

	public static final String STYLE_ET_PLAN = "et-plan";
	public static final String STYLE_ET_INFO = "et-info";
	public static final String STYLE_ET_INWORK = "et-inwork";
	public static final String STYLE_ET_NORMAL = "et-normal";
	public static final String STYLE_ET_SUCCESS = "et-success";
	public static final String STYLE_ET_WARNING = "et-warning";
	public static final String STYLE_ET_EMERGENCY = "et-emergency";

	public static final String STYLE_ET_DONE = "et-done";
	public static final String STYLE_ET_UNDONE = "et-undone";

	public static final String STYLE_ET_ICON = "et_icon";
	public static final String STYLE_ET_ICON_EMERGENCY = "et_icon_emergency";
	public static final String CLICKABLE = "clickable";
	public static final String STYLE_HAS_CHILD = "has-child";

	public static final String STYLE_TASK_INFO = "task_info";
	public static final String STYLE_TASK_INWORK = "task_inwork";
	public static final String STYLE_TASK_SUCCESS = "task_success";
	public static final String STYLE_TASK_WARNING = "task_warning";
	public static final String STYLE_TASK_EMERGENCY = "task_emergency";
	public static final String STYLE_TASK_PLAN = "task_plan";
	public static final String STYLE_TASK_PROGRESS = "task_progress";

	public static final VaadinIcons ICON_HOME = VaadinIcons.HOME;
	public static final VaadinIcons ICON_REPORT = VaadinIcons.NEWSPAPER;
	public static final VaadinIcons ICON_ARCHIVE = VaadinIcons.ARCHIVE;
	public static final VaadinIcons ICON_REFERENCE = VaadinIcons.LIST;
	public static final VaadinIcons ICON_SCHEDULE = VaadinIcons.CALENDAR_O;
	public static final VaadinIcons ICON_SETUP = VaadinIcons.COG;

	public static final VaadinIcons ICON_UNKNOWN = VaadinIcons.QUESTION_CIRCLE;
	public static final VaadinIcons ICON_ACCEPTED = VaadinIcons.CHECK_CIRCLE_O;
	public static final VaadinIcons ICON_NOT_ACCEPTED = VaadinIcons.CIRCLE_THIN;
	public static final VaadinIcons ICON_MOVE_UP = VaadinIcons.CARET_UP;
	public static final VaadinIcons ICON_MOVE_DOWN = VaadinIcons.CARET_DOWN;
	public static final VaadinIcons ICON_ATTACHMENT = VaadinIcons.CAMERA;
	public static final VaadinIcons ICON_FILE = VaadinIcons.FILE_O;
	public static final VaadinIcons ICON_MAP = VaadinIcons.MAP_MARKER;
	public static final VaadinIcons ICON_REQUEST = VaadinIcons.PHONE;// COMMENT;
	public static final VaadinIcons ICON_WORKORDER = VaadinIcons.TASKS;
	public static final VaadinIcons ICON_WORKGROUP = VaadinIcons.TRUCK;
	public static final VaadinIcons ICON_WORKPLAN = VaadinIcons.TASKS;
	public static final VaadinIcons ICON_REFRESH = VaadinIcons.REFRESH;
	public static final VaadinIcons ICON_SHOW = VaadinIcons.EYE;
	public static final VaadinIcons ICON_ADD = VaadinIcons.PLUS;
	public static final VaadinIcons ICON_EDIT = VaadinIcons.EDIT;
	public static final VaadinIcons ICON_FILTER = VaadinIcons.FILTER;
	public static final VaadinIcons ICON_REDIRECT = VaadinIcons.ARROWS_LONG_H;
	public static final VaadinIcons ICON_SUSPEND = VaadinIcons.PAUSE;
	public static final VaadinIcons ICON_DELETE = VaadinIcons.TRASH;
	public static final VaadinIcons ICON_DOWNLOAD = VaadinIcons.DOWNLOAD;
	public static final VaadinIcons ICON_UPLOAD = VaadinIcons.UPLOAD;
	public static final VaadinIcons ICON_COPY = VaadinIcons.COPY;
	public static final FontAwesome ICON_SEARCH = FontAwesome.SEARCH;
	public static final VaadinIcons ICON_PASTE = VaadinIcons.PASTE;
	public static final VaadinIcons ICON_INFO = VaadinIcons.INFO_CIRCLE;
	public static final VaadinIcons ICON_LEGEND = VaadinIcons.PAINTBRUSH;
	public static final VaadinIcons ICON_LEGEND_ITEM = VaadinIcons.CIRCLE;
	public static final VaadinIcons ICON_ACTION = VaadinIcons.WRENCH;
	public static final VaadinIcons ICON_PARENT = VaadinIcons.ARROW_CIRCLE_UP_O;
	public static final VaadinIcons ICON_WARNING = VaadinIcons.WARNING;
	public static final VaadinIcons ICON_ERROR = VaadinIcons.EXCLAMATION;
	public static final VaadinIcons ICON_OK = VaadinIcons.CHECK;
	public static final VaadinIcons ICON_CLOSE = VaadinIcons.CLOSE;
	public static final VaadinIcons ICON_PASS = VaadinIcons.LOCK;
	public static final VaadinIcons ICON_PERSON = VaadinIcons.USER;
	public static final VaadinIcons ICON_ADD_PERSON = VaadinIcons.PLUS_CIRCLE_O;
	public static final VaadinIcons ICON_REMOVE_PERSON = VaadinIcons.CLIPBOARD_CROSS;
	public static final VaadinIcons ICON_SEND_TEXT_MESSAGE = VaadinIcons.COMMENT;
	public static final VaadinIcons ICON_EXPAND = VaadinIcons.EXPAND;
	public static final VaadinIcons ICON_COMPRESS = VaadinIcons.COMPRESS;
	public static final VaadinIcons ICON_NOTIFICATION = VaadinIcons.BELL;
	public static final VaadinIcons ICON_DELAYED = VaadinIcons.TIMER;
	public static final VaadinIcons ICON_LAYOUT = VaadinIcons.LAYOUT;
	public static final VaadinIcons ICON_CALC = VaadinIcons.CALC_BOOK;

	public static final VaadinIcons ICON_EVENT_IN = VaadinIcons.ARROW_RIGHT;
	public static final VaadinIcons ICON_EVENT_OUT = VaadinIcons.ARROW_LEFT;
	// REQUEST
	public static final VaadinIcons ICON_EVENT_01 = ICON_REQUEST; //Поступление заявки
	public static final VaadinIcons ICON_EVENT_02 = ICON_EDIT; //Создание/изменение заявки
	public static final VaadinIcons ICON_EVENT_03 = VaadinIcons.EXCLAMATION_CIRCLE_O; //Нарушение сроков закрытия заявки
	public static final VaadinIcons ICON_EVENT_04 = ICON_EVENT_03; //Нарушение сроков реагирования диспетчером
	public static final VaadinIcons ICON_EVENT_05 = ICON_EVENT_03; //Нарушение сроков реагирования ст. мастером
	public static final VaadinIcons ICON_EVENT_06 = VaadinIcons.CHECK_SQUARE; //СОУ – ответственный по заявке
	public static final VaadinIcons ICON_EVENT_07 = VaadinIcons.ARROW_CIRCLE_RIGHT; //Поручена РВ
	public static final VaadinIcons ICON_EVENT_08 = ICON_EVENT_07; //РВ – ответственный по заявке
	public static final VaadinIcons ICON_EVENT_09 = ICON_EDIT; //Создание/изменение действия или комментария по заявке
	public static final VaadinIcons ICON_EVENT_10 = ICON_PASS; //Закрытие в СОУ заявки
	// WORK
	public static final VaadinIcons ICON_EVENT_11 = ICON_ACTION; //Создание/изменение работы
	public static final VaadinIcons ICON_EVENT_12 = VaadinIcons.EXTERNAL_LINK; //Отправка бригаде
	public static final VaadinIcons ICON_EVENT_13 = ICON_WORKPLAN; //Подтверждение получения бригадой
	public static final VaadinIcons ICON_EVENT_14 = ICON_WORKPLAN; //Нет подтверждения получения бригадой
	public static final VaadinIcons ICON_EVENT_15 = ICON_WORKGROUP; //Прибытие бригады
	public static final VaadinIcons ICON_EVENT_16 = ICON_WORKGROUP; //Убытие бригады
	public static final VaadinIcons ICON_EVENT_17 = ICON_WORKGROUP; //Нет прибытия бригады
	public static final VaadinIcons ICON_EVENT_18 = VaadinIcons.HOURGLASS_START; //Начало выполнения бригадой
	public static final VaadinIcons ICON_EVENT_19 = VaadinIcons.HOURGLASS_END; //Окончание выполнения
	public static final VaadinIcons ICON_EVENT_20 = VaadinIcons.PICTURE; //Поступление фото/документов по работе
	public static final VaadinIcons ICON_EVENT_21 = VaadinIcons.SPECIALIST; //Создание работы мастером бригады
	public static final VaadinIcons ICON_EVENT_22 = ICON_EVENT_19; //Завершение работы
	public static final VaadinIcons ICON_EVENT_23 = ICON_EVENT_03; //Нарушение сроков выполнения работы
	public static final VaadinIcons ICON_EVENT_24 = ICON_EVENT_20; //Не предоставлены фотоматериалы по работе
	public static final VaadinIcons ICON_EVENT_25 = ICON_WORKPLAN; //Нет информации о работе бригады

	public static final VaadinIcons ICON_EVENT_26 = VaadinIcons.USER_CARD; //Бригада зарегистрирована
	public static final VaadinIcons ICON_EVENT_27 = ICON_EVENT_19; //Бригада завершила смену

	public static final VaadinIcons ICON_EVENT_28 = VaadinIcons.UNLINK; //Связь с бригадой потеряна
	public static final VaadinIcons ICON_EVENT_29 = VaadinIcons.LINK; //Связь с бригадой восстановлена

	public static final FontAwesome ICON_EVENT_30 = FontAwesome.MAP; //Выезд бригады за пределы зоны обслуживания
	public static final VaadinIcons ICON_EVENT_31 = ICON_WORKGROUP; //Прибытие на адрес
	public static final VaadinIcons ICON_EVENT_32 = ICON_WORKGROUP; //Завершение работы на адресе
	public static final VaadinIcons ICON_EVENT_33 = ICON_WORKGROUP; //Отсутствие ТС
}
