package ru.telros.brigades.client;

import com.vaadin.server.VaadinServlet;

import javax.servlet.ServletException;

public class BrigadesServlet extends VaadinServlet {
	@Override
	protected final void servletInitialized() throws ServletException {
		super.servletInitialized();
		getService().addSessionInitListener(new BrigadesSessionInitListener());
	}
}
