package ru.telros.brigades.client;

import ru.telros.brigades.client.dto.*;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class BrigadeAdviser implements HasLogger {
    WorkDto work;
    //геогр. местополож. работы
    GeocodeDto geocodeDto;
    //Необходимые для выполнения работы снаряжение
    List<EquipmentWorkDto> equipmentWork;
    //время выполнения работы
    List<BrigadeInfo> brigadesInfo;
    Double norm;
    Integer delay;
    ZonedDateTime expectedEarlyFinish;
    ZonedDateTime expectedLateFinish;
    public BrigadeAdviser(WorkDto workDto, Integer delay, ZonedDateTime earlyStart, ZonedDateTime lateStart){
        work = workDto;
        this.delay = delay;
        geocodeDto = BrigadesUI.getRestTemplate().getCoordinatesByAddress(workDto.getAddress()).get(0);
        norm = BrigadesUI.getRestTemplate().getTypeWork(workDto.getIdTypeWork()).getNorm();
        expectedEarlyFinish = earlyStart.plusHours((long)(norm/1)).plusMinutes((long)(norm%1)*6);
        expectedLateFinish = lateStart.plusHours((long)(norm/1)).plusMinutes((long)(norm%1)*6);
        equipmentWork = BrigadesUI.getRestTemplate().getWorkEquipments(workDto.getId());
        brigadesInfo = BrigadesUI.getRestTemplate().getBrigades().stream().map(e->{
           return new BrigadeInfo(e);
        }).collect(Collectors.toList());

        StringBuilder sb= new StringBuilder("");
        sb.append("Early Start: ").append(earlyStart).append("\n");
        sb.append("expected Early Finish: ").append(expectedEarlyFinish).append("\n");
        sb.append("Late Start: ").append(lateStart).append("\n");
        sb.append("expected Late Finish: ").append(expectedLateFinish).append("\n");
        sb.append("Work\n").append(workDto.debugInfo()).append("\n");
        sb.append("Norm\n").append(norm).append("\n");
        sb.append("Equipment Work\n").append(equipmentWork.stream().map(e->e.debugInfo()+"\n").collect(Collectors.joining())).append("\n");
        brigadesInfo.stream().forEach(e->{
            sb.append("Brigade Info\n").append(e.toString()).append("\n");
        });
        /*BrigadesUI.getRestTemplate().getVehicles().stream().forEach(e->sb.append(e.debugInfo()));
        for (long i = 1; i < 10; i++) {
            BrigadesUI.getRestTemplate().getBrigadeVehicles(i).stream().forEach(e->sb.append(e.debugInfo()));
        }
        BrigadesUI.getRestTemplate().getEquipments().stream().filter(e->(e.getType().equals("Машины АВР")||e.getType().equals("Фургоны"))).forEach(e->{

            sb.append(BrigadesUI.getRestTemplate().getBrigadeVehicles(e.getId())).append("\n");
        });
*/
        getLogger().debug(sb.toString());

        /*sb.append("NORM").append("\n");
        BrigadesUI.getRestTemplate().getTypeWorks().stream().forEach(e->sb.append(e.getNorm()).append("\n"));
        BrigadesUI.getRestTemplate().getBrigades().stream().forEach(br->{
            sb.append("Brigade").append("\n");
            sb.append(br.debugInfo()).append("\n");
            sb.append("EQV").append("\n");

            // к плану привязываются либо машины АВР либо фургоны
            //получить привязанные к плану снаряжения - getBrigadeEquipments()
            //отсюда взять фургон или машину АВР
            //и
            //местоположение фургона или машины АВР (и соотв. бригады) - getVehicleTracks()
            sb.append("Brigade Eqv");
            *//*for (int i = 0; i < 10; i++) {

                BrigadesUI.getRestTemplate().getBrigadeEquipments(br.getId()).stream().forEach(e-> sb.append(e.debugInfo()).append("\n"));
            }*//*
            sb.append("Mech").append("\n");
            BrigadesUI.getRestTemplate().getBrigadeMechanization(br.getId()).stream().forEach(e-> sb.append(e.debugInfo()).append("\n"));
            sb.append("Inv").append("\n");
            BrigadesUI.getRestTemplate().getBrigadeInventory(br.getId()).stream().forEach(e-> sb.append(e.debugInfo()).append("\n"));
            sb.append("Veh").append("\n");
            BrigadesUI.getRestTemplate().getBrigadeVehicles(br.getId()).stream().forEach(e-> sb.append(e.debugInfo()).append("\n"));
            sb.append("Loc").append("\n");
            sb.append(BrigadesUI.getRestTemplate().getBrigade(br.getId()).getLocation()).append("\n");
            sb.append("Employees").append("\n");
            BrigadesUI.getRestTemplate().getBrigadeEmployees(br.getId()).stream().forEach(e->sb.append(e.debugInfo()).append("\n"));
            //время выполнения работы Тр (рассчитано ранее в АИС Бригады);

                //графики работы каждой из бригад;
            //оснащении необходимыми средствами малой механизации (СММ);
            sb.append("Plans").append("\n");
            sb.append(BrigadesUI.getRestTemplate().getBrigadePlans(br.getId(),4).stream().map(e->e.debugInfo()+"\n").collect(Collectors.joining())).append("\n");

            sb.append("Works").append("\n");
            sb.append(BrigadesUI.getRestTemplate().getBrigadeWorks(br.getId(),4).stream().map(e->e.debugInfo()+"\n").collect(Collectors.joining())).append("\n");

        });*/
//        BrigadesUI.getRestTemplate().getBrigadeEquipments()



    }
    public Map<BrigadeInfo,Calculations> getCalculations(Set<BrigadeInfo> brigadeInfos){
        double minDistance = brigadeInfos.stream().map(brigadeInfo -> brigadeInfo.distanceToTarget).min(Double::compareTo).get();
        Map<BrigadeInfo, Calculations> calculations = new HashMap<>();
        brigadeInfos.stream().forEach(e->{
            calculations.put(e,new Calculations(e, minDistance));
        });
        return calculations;
    }
    public List<BrigadeInfo> getBrigadesInfo(){
        return brigadesInfo;
    }
    public class Calculations{

        static final String distanceNormalizationFormula = "1/(%s/%s)";
        static final double distanceCoeffecient = 1;
        static final String priorityNormalizationFormula = "-1*%d/3+4/3";
        static final double priorityCoefficient = 1;
        static final String brigadeTypeNormalizationFormula = "%s/10";
        static final double brigadeTypeCoefficient = 1;
        private double distanceToTarget;
        private double distanceToTargetNormalized;
        private double distanceToTargetCalculated;
        private double priority;
        private double priorityNormalized;
        private double priorityCalculated;
        private double brigadeType;
        private double brigadeTypeNormalized;
        private double brigadeTypeCalculated;

        private double result;
        public Calculations(BrigadeInfo brigadeInfo, double minDistance) {
            final ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
            try {
                this.distanceToTarget = brigadeInfo.distanceToTarget;
                this.distanceToTargetNormalized = (Double) engine.eval(String.format(distanceNormalizationFormula, brigadeInfo.getDistanceToTarget(), minDistance));
                this.distanceToTargetCalculated = this.distanceToTargetNormalized * distanceCoeffecient;

                this.priority = brigadeInfo.getPriority();
                this.priorityNormalized = (Double) engine.eval(String.format(priorityNormalizationFormula, brigadeInfo.getPriority()));
                this.priorityCalculated = this.priorityNormalized * priorityCoefficient;

                this.brigadeType = 10;
                this.brigadeTypeNormalized = (Double) engine.eval(String.format(brigadeTypeNormalizationFormula, Double.parseDouble("10.0")));
                this.brigadeTypeCalculated = this.brigadeTypeNormalized * brigadeTypeCoefficient;

                result = this.distanceToTargetCalculated + this.priorityCalculated + this.brigadeTypeCalculated;
            } catch (ScriptException e) {
                e.printStackTrace();
            }

        }



        public double getDistanceToTarget() {
            return distanceToTarget;
        }

        public void setDistanceToTarget(double distanceToTarget) {
            this.distanceToTarget = distanceToTarget;
        }

        public double getDistanceToTargetNormalized() {
            return distanceToTargetNormalized;
        }

        public void setDistanceToTargetNormalized(double distanceToTargetNormalized) {
            this.distanceToTargetNormalized = distanceToTargetNormalized;
        }

        public double getDistanceToTargetCalculated() {
            return distanceToTargetCalculated;
        }

        public void setDistanceToTargetCalculated(double distanceToTargetCalculated) {
            this.distanceToTargetCalculated = distanceToTargetCalculated;
        }

        public double getPriority() {
            return priority;
        }

        public void setPriority(double priority) {
            this.priority = priority;
        }

        public double getPriorityNormalized() {
            return priorityNormalized;
        }

        public void setPriorityNormalized(double priorityNormalized) {
            this.priorityNormalized = priorityNormalized;
        }

        public double getPriorityCalculated() {
            return priorityCalculated;
        }

        public void setPriorityCalculated(double priorityCalculated) {
            this.priorityCalculated = priorityCalculated;
        }

        public double getBrigadeType() {
            return brigadeType;
        }

        public void setBrigadeType(double brigadeType) {
            this.brigadeType = brigadeType;
        }

        public double getBrigadeTypeNormalized() {
            return brigadeTypeNormalized;
        }

        public void setBrigadeTypeNormalized(double brigadeTypeNormalized) {
            this.brigadeTypeNormalized = brigadeTypeNormalized;
        }

        public double getBrigadeTypeCalculated() {
            return brigadeTypeCalculated;
        }

        public void setBrigadeTypeCalculated(double brigadeTypeCalculated) {
            this.brigadeTypeCalculated = brigadeTypeCalculated;
        }

        public double getResult() {
            return result;
        }

        public void setResult(double result) {
            this.result = result;
        }
    }
    public class BrigadeInfo{
        public BrigadeDto brigade;
        public Long delta1;
        public Long delta2;
        public List<BrigadePlanDto> workPlan;
        public WorkDto currentWork;
        public WorkItemDto lastAction;
        public String location;
        public Double distanceToTarget;
        public int priority;
        public BrigadeInfo(BrigadeDto brigade){
            this.brigade = brigade;
            this.workPlan = initWorkPlan();
            this.currentWork = initCurrentWork();
            this.lastAction = initLastAction();
            this.location = initLocation();
            this.distanceToTarget = initDistanceToTarget();
            if (workPlan.size()>0){
                getLogger().debug(String.format("Init deltas for %s \n expected early finish: %s, expected late finish: %s",brigade.getName(),expectedEarlyFinish,expectedLateFinish));
                delta1 = (workPlan.get(0).getEndDate().toEpochSecond() - expectedEarlyFinish.toEpochSecond());
                delta2 = (workPlan.get(0).getEndDate().toEpochSecond() - expectedLateFinish.toEpochSecond());
            }
            else {
                delta2 = -999998l;
                delta1 = -999999l;
            }
            this.priority = initPriority();


        }
        public String toString(){
            StringBuilder sb = new StringBuilder("");

            sb.append("Brigade: \n").append(brigade.debugInfo()).append("\n");
            sb.append("Work plan: \n").append(workPlan.stream().map(e->e.debugInfo()+"\n").collect(Collectors.joining())).append("\n");
            if (currentWork!=null){
                sb.append("Current Work: \n").append(currentWork.debugInfo()+"\n").append("\n");
            }
            sb.append("Location: \n").append(location+"\n").append("\n");
//            sb.append("Finish date: \n").append(getExpectedFinishDate()+"\n").append("\n");
            sb.append("Priority: \n").append(getPriority()+"\n").append("\n");
            return sb.toString();
        }
        // Текущая работа
        private WorkDto initCurrentWork(){
            getLogger().debug("Init current work for "+ brigade.debugInfo().replace("\n","; "));
            return BrigadesUI.getRestTemplate().getBrigadeWorks(brigade.getId()).stream().
                    peek(e-> getLogger().debug(e.debugInfo()))
                    .filter(e->{
                       return  ((e.getFinishDate()==null && e.getStartDate()!=null)     //  задана факт дата начала работы, но не задана дата окончания
                               ||((e.getStatus().equals(WorkDto.Status.ASSIGNED) || e.getStatus().contains("Назначена"))         //  Статус работы - "Назначена"
                               || (e.getStatus().equals(WorkDto.Status.IN_PROGRESS)) || e.getStatus().contains("Выполняется")));   //  Статус работы - "Выполняется"
                    }).distinct().findFirst().orElse(null);
        }
        //последнее выполненное действие
        //TODO-k Посмотреть условие поиска тек. действия
        private WorkItemDto initLastAction(){
            if (currentWork==null){return null;}
            return BrigadesUI.getRestTemplate().getWorkItems(currentWork.getId()).stream()
                    .filter(e->e.getFinishDate()!=null )
                    .sorted(Comparator.comparing(WorkItemDto::getFinishDate))
                    .findFirst().orElse(null);
        }
        //План работы бригады на день
        private List<BrigadePlanDto> initWorkPlan(){
            List<BrigadePlanDto> brigadePlanDtos;
            brigadePlanDtos = BrigadesUI.getRestTemplate().getBrigadePlans(brigade.getId()).stream().
                    filter(e-> ((!e.getBeginDate().equals(e.getEndDate())) &&
                            (ZonedDateTime.now().isBefore(e.getEndDate())&&ZonedDateTime.now().isAfter(e.getBeginDate())))).
                    collect(Collectors.toList());
            if (brigadePlanDtos==null){
                BrigadePlanDto planDto = new BrigadePlanDto();
                planDto.setBeginDate(brigade.getBeginWork());
                planDto.setEndDate(brigade.getEndWork());
                brigadePlanDtos = Arrays.asList(planDto);
            }
            return brigadePlanDtos;
        }
        //Todo-k переписать, когда появится нормальное местоположение
        //расстояние бригады до работы
        public Double initDistanceToTarget(){
            GeocodeDto curGeo;
            if (this.getLocation()!=null){
                getLogger().debug(this.getLocation());
                getLogger().debug(BrigadesUI.getRestTemplate().getCoordinatesByAddress(this.getLocation()).get(0).debugInfo());
                getLogger().debug(geocodeDto.debugInfo());
                curGeo = BrigadesUI.getRestTemplate().getCoordinatesByAddress(this.getLocation()).get(0);
                return distance(geocodeDto.getLatitude(),geocodeDto.getLongitude(),curGeo.getLatitude(),curGeo.getLongitude(),'K');
            }
            return null;


        }
        //Местоположение бригады
        public String initLocation(){
            if (currentWork!=null){
                if (currentWork.getStatus().equals(WorkDto.Status.IN_PROGRESS) || currentWork.getStatus().contains("Выполняется"))
                return currentWork.getAddress();
            }else {
                //TODO-k написат получение местоположения машины из трансконтроля

            }
            return null;
        }
        //ожидаемое время завершения текущей работы
        private String initExpectedFinishDate(){
            if (currentWork!=null) {
                // разница между фактическим началом работы и заплаарнгпрланированным
                long diff = currentWork.getStartDate().toEpochSecond() - currentWork.getStartDatePlan().toEpochSecond();
                diff += currentWork.getFinishDatePlan().toEpochSecond();
                Instant i = Instant.ofEpochSecond(diff);
                ZonedDateTime z = ZonedDateTime.ofInstant(i, currentWork.getFinishDatePlan().getZone());
                return z.toString();
            }else {
                return "Бригада свободна";
            }
        }
        //приоритет выполнения работы
        private int initPriority(){
            getLogger().debug(String.format("Init priority for %s brigage. delta1 = %s, delta2 = %s",brigade.getName(), delta1, delta2));
            if (delta2>=0 && delta1!=delta2){
                return 1;
            }else if ((delta2<0 && delta1>=0)||(delta1>=0||delta1==delta2)){
                return 2;
            }
            else if ((delta1<0 && delta2<0) && ((delta1+delay)>=0)){
                return 3;
            }
            else{
                return 4;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof BrigadeInfo)) return false;
            BrigadeInfo that = (BrigadeInfo) o;
            return getDelta1() == that.getDelta1() &&
                    getDelta2() == that.getDelta2() &&
                    getBrigade().equals(that.getBrigade()) &&
                    Objects.equals(getWorkPlan(), that.getWorkPlan()) &&
                    Objects.equals(getCurrentWork(), that.getCurrentWork());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getBrigade(), getDelta1(), getDelta2(), getWorkPlan(), getCurrentWork());
        }

        public BrigadeDto getBrigade() {
            return brigade;
        }

        public long getDelta1() {
            return delta1;
        }

        public long getDelta2() {
            return delta2;
        }

        public List<BrigadePlanDto> getWorkPlan() {
            return workPlan;
        }

        public WorkDto getCurrentWork() {
            return currentWork;
        }
        public String getCurrentWorkAndAction(){
            if (currentWork==null){
                return " -/- ";
            }else {
                WorkItemDto lastAction = this.getLastAction();
                if (lastAction==null){
                    return currentWork.getDescription()+"/- ";
                }else {
                    return currentWork.getDescription()+"/"+lastAction.getDescription();
                }
            }
        }

        public String getLocation() {
            return location;
        }
        private WorkItemDto getLastAction(){
            return this.lastAction;
        }
        public Double getDistanceToTarget() {
            return distanceToTarget;
        }

        public int getPriority() {
            return priority;
        }
    }
    private double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
