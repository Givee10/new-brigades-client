package ru.telros.brigades.client.view.reports.util;

import com.vaadin.ui.*;

public class GridWithButtonLayout extends HorizontalLayout {
    public GridWithButtonLayout(Grid grid, Button mainButton){
        Button closeButton = new Button("Закрыть");
        VerticalLayout popupLayout = new VerticalLayout(grid,closeButton);
        PopupView view = new PopupView(null, popupLayout);
        view.setHideOnMouseOut(false);
        closeButton.addClickListener(e->{
            view.setPopupVisible(false);
        });
        mainButton.addClickListener(e->{
            view.setPopupVisible(true);
        });
        addComponents(mainButton, view);
    }
}
