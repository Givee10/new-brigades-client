package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.EventWithActionDto;
import ru.telros.brigades.client.ui.eventlog.EventConstants;
import ru.telros.brigades.client.ui.eventlog.EventGrid;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class EventsReportWindow extends DynamicReportWindow<EventGrid, EventWithActionDto> {
    public EventsReportWindow(String title){
        super(title, new EventGrid(new ArrayList<>(),true), EventConstants.getReportColumns());
    }
    @Override
    public List<EventWithActionDto> getItems() {
        if (timeLayout.isValid()) {
            return BrigadesUI.getRestTemplate().getEventsWithActions(
                    timeLayout.getStart(), timeLayout.getEnd()).getCurrentContent();
        }else {
            Notification.show("Проверьте даты", Notification.Type.ERROR_MESSAGE);
            return null;
        }
    }

    @Override
    public void customUpdateButtonHandle(String reportType) {

    }

    @Override
    public Button initSaveReportButton() {
        Button button = new Button("Сохранить");
        button.addClickListener(clickEvent ->{
            if (timeLayout.isValid()) {
                saveReport("Отчет по Событиям\r\n" +
                        "Время: " + timeLayout.getStart().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)) +
                        " - " + timeLayout.getEnd().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)));
            }else {
                Notification.show("Проверьте даты", Notification.Type.ERROR_MESSAGE);
            }

        });
        return button;
    }

    @Override
    public void displayCustom(String name) {

    }

    @Override
    public void displayDynamic() {
        grid.leaveShownOnly(columnsGrid.getSelectedItems());
        columnsButton.setVisible(true);
    }
}
