package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.*;
import ru.telros.brigades.client.dto.report.InteractionIntensityDto;

import java.util.List;

public class InteractionIntensityReportWindow extends Window {
    TextField requestIdTextField;
    TextField requestAddressTextField;
    TextField requestCodeTextField;
    TextField requestRegistrationDateTextField;
    Button button;
    Grid<InteractionIntensityDto> grid;
    public InteractionIntensityReportWindow(){
        setCaption("Отчет об интенсивности инф. взаимодействия в рамках реагирования на заявки ГЛ");
        VerticalLayout mainLayout = new VerticalLayout();
        requestIdTextField = new TextField();
        requestIdTextField.setCaption("ID заявки");
        requestAddressTextField = new TextField();
        requestAddressTextField.setCaption("Адрес зявки");
        requestAddressTextField.setEnabled(false);
        requestCodeTextField = new TextField();
        requestCodeTextField.setCaption("Код заявки");
        requestCodeTextField.setEnabled(false);
        requestRegistrationDateTextField = new TextField();
        requestRegistrationDateTextField.setCaption("Дата регистрации заявки");
        requestRegistrationDateTextField.setEnabled(false);
        button = new Button("Обновить");
        button.addClickListener(clickEvent -> {
            Long value;
            try {
                value = Long.parseLong(requestIdTextField.getValue());
                List<InteractionIntensityDto> dtos = null; // Заменить на получение из базы
                grid.setItems(dtos);
            }catch (NumberFormatException e){
                Notification.show("Проверьте правильность ввода id заявки", Notification.Type.ERROR_MESSAGE);
            }
        });
        grid = initGrid();
        HorizontalLayout fields = new HorizontalLayout();
        fields.addComponents(requestIdTextField,
                requestAddressTextField,requestCodeTextField,requestRegistrationDateTextField,button);
        mainLayout.addComponents(fields,grid);
        grid.setWidth("100%");
        grid.setHeight("100%");
        mainLayout.setExpandRatio(grid,1);
        mainLayout.setHeight("100%");
        setContent(mainLayout);
        setSizeFull();
    }
    private Grid<InteractionIntensityDto> initGrid() {
        Grid<InteractionIntensityDto> grid = new Grid<>();
        grid.addColumn(e -> e.getEvent().getDate()).setCaption("Время события");
        grid.addColumn(e -> e.getEvent().getMessage()).setCaption("Событие");
        grid.addColumn(e -> e.getUser()).setCaption("Пользователь");
        grid.addColumn(e -> e.getRole()).setCaption("Роль");
        return grid;
    }
}
