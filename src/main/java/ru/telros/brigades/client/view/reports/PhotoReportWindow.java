package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.PhotoReportDto;
import ru.telros.brigades.client.view.reports.util.GridWithButtonLayout;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PhotoReportWindow extends Window {
    VerticalLayout mainLayout;
    HorizontalLayout parametersLayout;
    PhotoReportGrid resultGrid;
    ReportDateTimeLayout timeLayout;
    Grid<BrigadeDto> brigadeSelectionGrid;
    GridWithButtonLayout brigadeSelectionLayout;
    Button searchButton;
    public PhotoReportWindow(){
        resultGrid = new PhotoReportGrid();

        timeLayout = new ReportDateTimeLayout();

        brigadeSelectionGrid = new Grid<>();
        brigadeSelectionGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        brigadeSelectionGrid.addColumn(e->e.getName());
        brigadeSelectionGrid.setItems(BrigadesUI.getRestTemplate().getBrigades());
        brigadeSelectionLayout = new GridWithButtonLayout(brigadeSelectionGrid, new Button("Бригады"));

        searchButton = new Button("Поиск");
        searchButton.addClickListener(clickEvent -> {
            List<PhotoReportDto> resultDtos = new ArrayList<>();
            if (timeLayout.isValid() && brigadeSelectionGrid.getSelectedItems().size()!=0){
                brigadeSelectionGrid.getSelectedItems().stream().forEach(e->{
                    resultDtos.addAll(BrigadesUI.getRestTemplate().getPhotoReport(
                            timeLayout.getStart(),timeLayout.getEnd(),e
                    ));
                });
                resultGrid.setItems(resultDtos);
            }
        });


        parametersLayout = new HorizontalLayout(timeLayout,brigadeSelectionLayout,searchButton);
        parametersLayout.setComponentAlignment(timeLayout,Alignment.MIDDLE_CENTER);
        parametersLayout.setComponentAlignment(brigadeSelectionLayout,Alignment.MIDDLE_CENTER);
        parametersLayout.setComponentAlignment(searchButton,Alignment.MIDDLE_CENTER);
        mainLayout = new VerticalLayout(parametersLayout,resultGrid);
        mainLayout.setHeight("100%");
        resultGrid.setHeight("100%");
        parametersLayout.setHeight("100%");
        mainLayout.setExpandRatio(parametersLayout,15);
        mainLayout.setExpandRatio(resultGrid,85);
        setContent(mainLayout);
        setSizeFull();
        getUI().getCurrent().addWindow(this);
    }
    private class PhotoReportGrid extends Grid<PhotoReportDto>{
        public PhotoReportGrid(){
//            this.addColumn(e->e.getRecordNumber()).setCaption("№");
            this.addColumn(e->e.getBrigade()).setCaption("Бригада");
            this.addColumn(e->e.getAddress()).setCaption("Адрес");
            this.addColumn(e->e.getDescription()).setCaption("Описание");
            this.addColumn(e-> {
                if (e.getWorkDate()!=null){
                    return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(e.getWorkDate());
                }else {
                    return " - ";
                }
            }).setCaption("Дата работы");;
            this.addColumn(e->{
                if (e.getStartDateFact()!=null) {
                    return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(e.getStartDateFact());
                }else {
                    return " - ";
                }
            }).setCaption("Фактическое время начала");
            this.addColumn(e-> {
                if (e.getFinishDateFact() != null) {
                    return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(e.getFinishDateFact());
                }else {
                    return " - ";
                }

            }).setCaption("Фактическое время окончания");
            this.addColumn(e->{
                if (e.getAttachmentCreateDate()!=null){
                    return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(e.getAttachmentCreateDate());
                }else{
                    return " - ";
                }

            }).setCaption("Дата создания приложения");
            this.addColumn(e->{
                if (e.getPhotoDate()!=null){
                    return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(e.getPhotoDate());
                }else {
                    return "Нет";
                }
            }).setCaption("Дата фото");
            this.addColumn(e->e.getLostConnection()).setCaption("Потеря соединения");
            this.setWidth("100%");
        }
    }
}
