package ru.telros.brigades.client.view;

import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.ui.Grid;
import org.apache.commons.io.FileUtils;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrigadesRestTemplate;
import ru.telros.brigades.client.ui.component.HeaderLayout;
import ru.telros.brigades.client.ui.component.Title;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReferenceView extends HeaderLayout implements View, HasLogger {
	public static final String NAME = "reference";
	public static final String CAPTION = "Справка";

	//private EmployeeGrid grid;
	private Grid grid;
	private BrigadesRestTemplate restTemplate = BrigadesUI.getRestTemplate();

	public ReferenceView() {
		/*BrigadesUI.getRestTemplate().getBrigades().stream().forEach(brigadeDto -> {
			if (BrigadesUI.getRestTemplate().getAllWorks(ZonedDateTime.now().minusDays(30)).size()>0) {
				getLogger().debug(BrigadesUI.getRestTemplate().getBrigadeWorks(brigadeDto.getId()).stream().filter(e -> !e.getAddress().equals(null)).findFirst().get().getAddress());
				getLogger().debug(BrigadesUI.getRestTemplate().getCoordinatesByAddress(BrigadesUI.getRestTemplate().getBrigadeWorks(brigadeDto.getId()).stream().filter(e -> !e.getAddress().equals(null)).findFirst().get().getAddress()));

			}

		});*/
		List<Long> brigadesId = restTemplate.getBrigades().stream().map(e1->e1.getId()).collect(Collectors.toList());
		List<WorkDto> works = new ArrayList<>();
		for (int i = 0; i < brigadesId.size(); i++) {
			restTemplate.getBrigadeWorks(brigadesId.get(i),
					DatesUtil.localToZoned(LocalDateTime.now().minusDays(30)),
					DatesUtil.localToZoned(LocalDateTime.now())).stream().forEach(e2-> works.add(e2));
		}
			works.stream().filter(e->!e.getAddress().equals(null)).forEach(workDto -> {
				getLogger().debug(workDto.getAddress());
				getLogger().debug(StringUtil.returnDebugString(restTemplate.getCoordinatesByAddress(workDto.getAddress())));
			});
		addToHeader(new Title(CAPTION));
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		//grid = new EmployeeGrid(restTemplate.getAllEmployees());
		grid = initGrid();
		grid.setSizeFull();
		//VerticalLayout layout = new VerticalLayout(grid);
		//layout.setExpandRatio(grid,1);
		addComponentAndRatio(grid, 1);
	}
	private Grid initGrid(){
		Grid<String> grid = new Grid();
		Map<String, File> files = new HashMap<>();
		files.put("АИС БРИГАДЫ на ПК старшего мастера РВ",new File("files\\references\\Памятка_БРИГАДЫ_ПК_РВ(вариант4).pdf"));
		files.put("АИС БРИГАДЫ на ПК диспетчера СОУ",new File("files\\references\\Памятка_БРИГАДЫ_ПК_СОУ(вариант4).pdf"));
		files.put("АИС БРИГАДЫ на мобильном устройстве",new File("files\\references\\Памятка_БРИГАДЫ_планшет(вариант6).pdf"));


		grid.setItems(files.keySet());
		grid.addColumn(e->e).setCaption("Название памятки");
		grid.addItemClickListener(e->{

			File file = files.get(e.getItem());

			StreamResource streamResource =  new StreamResource(new StreamResource.StreamSource() {
				@Override
				public InputStream getStream() {
					try {

						return new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
					return null;
				}
			}, e.getItem());

			streamResource.setMIMEType("application/pdf");

			this.getUI().getPage().open(streamResource, "_new", 100, 100, BorderStyle.DEFAULT);
		});

		return grid;
	}
}
		//addComponentAndRatio(layout, 1);

		//getLogger().debug("Headers: {}", grid.getHeaderRowCount());
		//grid.removeHeaderRow(1);
		//update();

//		List<WorkDto> works = restTemplate.getWorks();
//		works.forEach(workDto -> {
//			getLogger().debug(workDto.debugInfo());
//			ZonedDateTime registrationDate = workDto.getRegistrationDate();
//			getLogger().debug("from JSON: {}", DatesUtil.zonedDateTimeToString(registrationDate, DatesUtil.STANDARD_DATE_FORMAT));
//			ZonedDateTime withZoneSameInstant = registrationDate.withZoneSameInstant(ZoneId.systemDefault());
//			getLogger().debug("with Zone Same Instant: {}", DatesUtil.zonedDateTimeToString(withZoneSameInstant, DatesUtil.STANDARD_DATE_FORMAT));
//		});
//		ZonedDateTime timeNow = ZonedDateTime.now();
//		getLogger().debug("ZonedDateTime now: {}", DatesUtil.zonedDateTimeToString(timeNow, DatesUtil.STANDARD_DATE_FORMAT));
//		ZonedDateTime withZoneSameInstant = timeNow.withZoneSameInstant(ZoneOffset.UTC);
//		getLogger().debug("Now with Zone Same Instant: {}", DatesUtil.zonedDateTimeToString(withZoneSameInstant, DatesUtil.STANDARD_DATE_FORMAT));
//		WorkDto workDto = new WorkDto();
//		workDto.setRegistrationDate(timeNow);
//		ObjectMapper objectMapper = BeanUtil.getBean(ObjectMapper.class);
//		try {
//			getLogger().debug(objectMapper.writeValueAsString(workDto));
//		} catch (JsonProcessingException e) {
//			getLogger().error(e.getMessage());
//		}

//		LoginDto getRequest = restTemplate.checkGetRequest();
//		if (getRequest != null) getLogger().debug(getRequest.toString());
//		List<LoginDto> getListRequest = restTemplate.checkGetListRequest();
//		getListRequest.forEach(loginDto -> getLogger().debug(loginDto.toString()));
//
//		LoginDto postRequest = restTemplate.checkPostRequest(getRequest);
//		if (postRequest != null) getLogger().debug(postRequest.toString());
//		List<LoginDto> postListRequest = restTemplate.checkPostListRequest(getListRequest);
//		postListRequest.forEach(loginDto -> getLogger().debug(loginDto.toString()));
//
//		LoginDto putRequest = restTemplate.checkPutRequest(postRequest);
//		if (putRequest != null) getLogger().debug(putRequest.toString());
//		List<LoginDto> putListRequest = restTemplate.checkPutListRequest(postListRequest);
//		putListRequest.forEach(loginDto -> getLogger().debug(loginDto.toString()));
//
//		restTemplate.checkDeleteRequest();



	/*private void update() {
		//employeeDataProvider.refreshAll();
		//grid.setItems(restTemplate.getAllEmployees());
		grid.update(getTestValues());
	}

	@Subscribe
	public void updateViewEvent(final UpdateViewEvent event) {
		if (getUI() != null && getUI().isAttached() && getUI().equals(event.getSource())) {
			getLogger().debug("Update function");
			update();
		}
	}

	private List<TestEntity> getTestValues() {
		List<TestEntity> result = new ArrayList<>();
		for (int i = 0; i < comboBox.getValue(); i++) {
			Integer rand = new Random().nextInt(100 + 1);
			TestEntity testEntity = new TestEntity();
			testEntity.setId(i + 1L);
			testEntity.setDate(ZonedDateTime.now());
			testEntity.setValue(rand);
			result.add(testEntity);
			getLogger().debug(testEntity.toString());
		}
		return result;
	}

	private List<GridColumn<TestEntity>> getTestColumns() {
		GridColumn<TestEntity> ID = GridColumn.createColumn("id", "№", TestEntity::getId);
		GridColumn<TestEntity> DATE = GridColumn.createDateColumn("date", "DATE", TestEntity::getDate);
		GridColumn<TestEntity> VALUE = GridColumn.createColumn("value", "VALUE", TestEntity::getValue);
		return Arrays.asList(ID, DATE, VALUE);
	}

	private class TestGrid extends BrigadesGrid<TestEntity> {
		public TestGrid(List<GridColumn<TestEntity>> gridColumns, List<TestEntity> items) {
			super(gridColumns, items);
		}

		@Override
		protected void onClick(TestEntity entity, Column column) {

		}

		@Override
		protected void onDoubleClick(TestEntity entity, Column column) {

		}
	}

	private class TestEntity extends AbstractEntity {
		private ZonedDateTime date;
		private Integer value;

		public ZonedDateTime getDate() {
			return date;
		}

		public void setDate(ZonedDateTime date) {
			this.date = date;
		}

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

		@Override
		public String toString() {
			final StringBuffer sb = new StringBuffer("TestEntity{");
			sb.append("id=").append(getId());
			sb.append(", date=").append(date);
			sb.append(", value=").append(value);
			sb.append('}');
			return sb.toString();
		}
	}*/

