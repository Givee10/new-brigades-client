package ru.telros.brigades.client.view;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.LoginDto;
import ru.telros.brigades.client.dto.RoleDto;
import ru.telros.brigades.client.dto.SettingsMapDto;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.user.UserLoginEvent;

import java.util.List;

@SuppressWarnings("serial")
public class LoginScreen extends VerticalLayout implements HasLogger {
	private TextField userName;
	private PasswordField passwordField;
	private Button login;

	public LoginScreen() {
		FormLayout loginForm = new FormLayout();
		loginForm.setSizeUndefined();
		userName = new TextField("Логин");
		userName.setIcon(BrigadesTheme.ICON_PERSON);
		userName.focus();
		passwordField = new PasswordField("Пароль");
		passwordField.setIcon(BrigadesTheme.ICON_PASS);
		login = new Button("Войти");
		loginForm.addComponent(userName);
		loginForm.addComponent(passwordField);
		loginForm.addComponent(login);
		login.addStyleName(ValoTheme.BUTTON_PRIMARY);
		login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		login.addClickListener($ -> login());

		VerticalLayout loginLayout = new VerticalLayout();
		loginLayout.setSizeUndefined();
		loginLayout.addComponent(loginForm);
		loginLayout.setComponentAlignment(loginForm, Alignment.TOP_CENTER);

		addComponent(loginLayout);
		setSizeFull();
		setComponentAlignment(loginLayout, Alignment.MIDDLE_CENTER);
	}

	private void login() {
		login.setEnabled(false);
		String user = userName.getValue();
		String pass = passwordField.getValue();
		passwordField.setValue("");

		if (user.trim().isEmpty() || pass.trim().isEmpty()) {
			Notification.show("Значение не может быть пустым", Notification.Type.WARNING_MESSAGE);
			login.setEnabled(true);
			return;
		}

		LoginDto loginDto = new LoginDto();
		loginDto.setUsername(user);
		loginDto.setPassword(pass);
		BrigadesUI.setCurrentLogin(loginDto);

		try {
			ResponseEntity<UserDto> entity = BrigadesUI.getRestTemplate().doLogin(loginDto);
			UserDto userDto = entity.getBody();
			List<RoleDto> userRoles = BrigadesUI.getRestTemplate().getUserRoles(userDto.getId());
			RoleDto role = userRoles.stream().filter(roleDto ->
					roleDto.getName().equalsIgnoreCase("Мастер бригады")).findFirst().orElse(null);

			//getLogger().debug("ROL: "+role);
			//role = null;
			if (role == null && !userDto.isBlocked()) {
				BrigadesUI.setCurrentUser(userDto);
				BrigadesUI.setCurrentSettings(new SettingsMapDto());
				BrigadesEventBus.post(new UserLoginEvent(getUI(), userDto));
			} else {
				Notification.show("Нет прав для входа в систему, обратитесь к администратору", Notification.Type.ERROR_MESSAGE);
				BrigadesUI.setCurrentLogin(null);
			}
		} catch (HttpStatusCodeException e) {
			HttpStatus httpStatus = e.getStatusCode();
			getLogger().warn("Exception: {}, {}", httpStatus.value(), httpStatus.getReasonPhrase());
			Notification.show("Неверный логин или пароль", Notification.Type.ERROR_MESSAGE);
			BrigadesUI.setCurrentLogin(null);
		} catch (ResourceAccessException e) {
			getLogger().error(e.getMessage());
			Notification.show("Сервер не ответил на запрос, обратитесь к администратору", Notification.Type.ERROR_MESSAGE);
			BrigadesUI.setCurrentLogin(null);
		} finally {
			login.setEnabled(true);
		}

		//BrigadesUI.getRestTemplate().addUserRole(1l, null);
	}
}
