package ru.telros.brigades.client.view.reports.util;

import com.vaadin.ui.DateTimeField;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.util.Locale;

public class ReportDateTimeField extends DateTimeField {
    public ReportDateTimeField(String caption){
        setCaption(caption);
        setLocale(new Locale("ru","RU"));
        setDateFormat(DatesUtil.GRID_DATE_FORMAT);
    }
}
