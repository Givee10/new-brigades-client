package ru.telros.brigades.client.view;

import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.HeaderLayout;
import ru.telros.brigades.client.ui.component.Title;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.view.archives.RequestsArchiveWindow;
import ru.telros.brigades.client.view.reports.EventsReportWindow;
import ru.telros.brigades.client.view.reports.WorksReportWindow;

import java.util.Arrays;
import java.util.List;

public class ArchiveView extends HeaderLayout implements View, HasLogger {
	public static final String NAME = "archive";
	public static final String CAPTION = "Архив";
	private TabSheet tabs;
	VerticalLayout layout;
	Grid<List<String>> archiveGrid;
	public ArchiveView() {
		addToHeader(new Title(CAPTION));
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);
		archiveGrid = new Grid<>();
		archiveGrid.addColumn(e->e.get(0)).setCaption("№").setId("id");
		archiveGrid.addColumn(e->e.get(1)).setCaption("Название").setId("name");
		archiveGrid.addColumn(e->e.get(2)).setCaption("Описание").setId("description");
		archiveGrid.setItems(Arrays.asList(
				Arrays.asList("1", "Архив по работам", "Составляет архив по работам"),
				Arrays.asList("2", "Архив по заявкам", "Составляет архив по заявкам"),
				Arrays.asList("3", "Архив по событиям", "Составляет архив по событиям")
		));
		archiveGrid.setSelectionMode(Grid.SelectionMode.NONE);
		archiveGrid.addItemClickListener(e ->{
			if(e.getMouseEventDetails().getButton() == MouseEventDetails.BUTTON_LEFT){
				int index = Integer.parseInt(e.getItem().get(0));
				switch (index){
					case 1:
						UI.getCurrent().addWindow(new WorksReportWindow("Архив по работам"));
						break;
					case 2:
						UI.getCurrent().addWindow(new RequestsArchiveWindow("Архив по заявкам"));
						break;
					case 3:
						UI.getCurrent().addWindow(new EventsReportWindow("Архив по событиям"));
						break;
					default:
						break;
				}
			}
		});
		archiveGrid.setSizeFull();
		VerticalLayout gridLayout = CompsUtil.getVerticalWrapperWithMargin(archiveGrid);
		addComponentAndRatio(gridLayout, 1);

	}
	private TabSheet initTabs(){
		TabSheet tabs = new TabSheet();
		tabs.addTab(initWorkTab());
		tabs.addTab(initRequestTab());
		tabs.addTab(initEventTab());
		return tabs;
	}
	private VerticalLayout initWorkTab(){
		VerticalLayout layout = CompsUtil.getVerticalWrapperNoMargin();
		Grid<WorkDto> grid;
		return layout;
	}
	private VerticalLayout initRequestTab(){
		VerticalLayout layout = CompsUtil.getVerticalWrapperNoMargin();
		return layout;
	}
	private VerticalLayout initEventTab(){
		VerticalLayout layout = CompsUtil.getVerticalWrapperNoMargin();
		return layout;
	}
}
