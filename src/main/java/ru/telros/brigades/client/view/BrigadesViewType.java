package ru.telros.brigades.client.view;

import com.vaadin.navigator.View;
import com.vaadin.server.Resource;
import ru.telros.brigades.client.BrigadesTheme;

public enum BrigadesViewType {
	DASHBOARD(DashboardView.NAME, DashboardView.CAPTION, DashboardView.class, BrigadesTheme.ICON_HOME, DashboardView.BADGE, true),
	REPORTS(ReportsView.NAME, ReportsView.CAPTION, ReportsView.class, BrigadesTheme.ICON_REPORT, null, true),
	ARCHIVE(ArchiveView.NAME, ArchiveView.CAPTION, ArchiveView.class, BrigadesTheme.ICON_ARCHIVE, null, true),
	REFERENCE(ReferenceView.NAME, ReferenceView.CAPTION, ReferenceView.class, BrigadesTheme.ICON_REFERENCE, null, true),
	SETUP(SetupView.NAME, SetupView.CAPTION, SetupView.class, BrigadesTheme.ICON_SETUP, null, true),
	ADMIN(AdminView.NAME, AdminView.CAPTION, AdminView.class, BrigadesTheme.ICON_SETUP, null, true);


	private final String viewName;
	private final String caption;
	private final Class<? extends View> viewClass;
	private final Resource icon;
	private final String badgeId;
	private final boolean enabled;

	BrigadesViewType(final String viewName, String caption, final Class<? extends View> viewClass, final Resource icon, final String badgeId, final boolean enabled) {
		this.viewName = viewName;
		this.caption = caption;
		this.viewClass = viewClass;
		this.icon = icon;
		this.badgeId = badgeId;
		this.enabled = enabled;
	}

	public static BrigadesViewType getByViewName(final String viewName) {
		BrigadesViewType result = null;
		for (BrigadesViewType viewType : values()) {
			if (viewType.getViewName().equals(viewName)) {
				result = viewType;
				break;
			}
		}
		return result;
	}

	public String getViewName() {
		return viewName;
	}

	public String getCaption() {
		return caption;
	}

	public Class<? extends View> getViewClass() {
		return viewClass;
	}

	public Resource getIcon() {
		return icon;
	}

	public String getBadgeId() {
		return badgeId;
	}

	public boolean isEnabled() {
		return enabled;
	}
}
