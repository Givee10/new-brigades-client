package ru.telros.brigades.client.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ru.telros.brigades.client.BrigadesTheme;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.PlayAudioEvent;
import ru.telros.brigades.client.event.PostViewChangeEvent;
import ru.telros.brigades.client.event.UpdateBadgeEvent;
import ru.telros.brigades.client.event.user.UserLogoutEvent;
import ru.telros.brigades.client.ui.utils.CompsUtil;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class MainMenu extends VerticalLayout {
	public static final String ID = "dashboard-menu";
	private final Map<String, Label> badgeLabels = new HashMap<>();
	private Audio audio = new Audio();

	public MainMenu() {
		BrigadesEventBus.register(this);
		setWidthUndefined();
		setDefaultComponentAlignment(Alignment.TOP_CENTER);
		setMargin(false);

		setPrimaryStyleName(BrigadesTheme.MENU_ROOT);
		setId(ID);
		addStyleName(BrigadesTheme.MENU_PART);
		addStyleName(BrigadesTheme.MENU_PART_LARGE_ICONS);

		addComponent(buildTitle());
		addComponent(buildMenuItems());
		addComponent(buildUserMenu());
		addComponent(buildAudio());
	}

	/**
	 * Создание аудио-компонента
	 *
	 * @return
	 */
	private Component buildAudio() {
		audio.setStyleName(BrigadesTheme.INVISIBLE);
		return audio;
	}

	/**
	 * Создание меню пользователя
	 *
	 * @return
	 */
	private Component buildUserMenu() {
		final MenuBar userMenu = new MenuBar();
		userMenu.addStyleName("user-menu");
		final UserDto user = BrigadesUI.getCurrentUser();
		MenuBar.MenuItem menuItem;
		Resource userIcon = null;
		/*if (user.getPhoto() != null) {
			try {
				userIcon = new FileResource(new File(user.getPhoto()));
			} catch (Exception e) {
				userIcon = null;
			}
		}*/
		if (userIcon == null) {
			userIcon = new ThemeResource("img/profile-pic-300px.jpg");
		}
		menuItem = userMenu.addItem("", userIcon, null);
		menuItem.setText(user.getFullName());
		menuItem.addItem("Настройки", (MenuBar.Command) selectedItem -> {
			//BrigadesEventBus.post(new PlayAudioEvent(new ThemeResource("sound/notify.wav")));
			//ProfilePreferencesWindow.open(user, true);
			//BrigadesEventBus.post(new UserPreferencesEvent(BrigadesUI.getCurrentUser()));
		});
		menuItem.addSeparator();
		menuItem.addItem("Выход", (MenuBar.Command) selectedItem -> BrigadesEventBus.post(new UserLogoutEvent(getUI(), user)));
		return userMenu;
	}

	/**
	 * Создание заголовка меню (логотип, название программы)
	 *
	 * @return Компонент-заголовок меню
	 */
	private Component buildTitle() {
		CssLayout title = new CssLayout();
		title.setStyleName(BrigadesTheme.STYLE_ET_NORMAL);

		Image logo = new Image(null, new ThemeResource("img/vodokanal_logo.png"));
		logo.setWidth(100, Unit.PERCENTAGE);

		Image telros = new Image(null, new ThemeResource("img/telros_logo.png"));
		telros.setWidth(100, Unit.PERCENTAGE);

		title.addComponent(CompsUtil.getVerticalWrapperWithMargin(logo, telros));

		return title;
	}

	/**
	 * Создание пунктов меню
	 *
	 * @return Компонент с пунктами меню
	 */
	private Component buildMenuItems() {
		CssLayout menuItems = new CssLayout();
		menuItems.addStyleName(BrigadesTheme.MENU_ITEMS);

		for (BrigadesViewType view : BrigadesViewType.values()) {
			Component menuItemComponent = new MenuButton(view);
			if (view.getBadgeId() != null) {
				Label label = new Label();
				label.setId(view.getBadgeId());
				badgeLabels.put(view.getBadgeId(), label);
				menuItemComponent = buildBadgeWrapper(menuItemComponent, label);
			}
			menuItems.addComponent(menuItemComponent);
		}

		return menuItems;
	}

	private Component buildBadgeWrapper(final Component menuItemButton, final Component badgeLabel) {
		CssLayout wrapper = new CssLayout(menuItemButton);
		wrapper.addStyleName(BrigadesTheme.MENU_BADGEWRAPPER);
		wrapper.addStyleName(ValoTheme.MENU_ITEM);
		badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
		badgeLabel.setWidthUndefined();
		badgeLabel.setVisible(false);
		wrapper.addComponent(badgeLabel);
		return wrapper;
	}

	@Subscribe
	public void updateBadge(final UpdateBadgeEvent event) {
		Label label = badgeLabels.get(event.getBadgeId());
		if (label != null) {
			String value = event.getBadgeValue();
			label.setValue(String.valueOf(value));
			label.setVisible(value != null && !"".equals(value) && !"0".equals(value));
		}
	}

	@Subscribe
	public void playAudio(final PlayAudioEvent event) {
		audio.setSource(event.getResource());
		audio.play();
	}

	private class MenuButton extends Button {
		private final BrigadesViewType view;

		public MenuButton(final BrigadesViewType view) {
			super(view.getCaption(), view.getIcon());
			this.view = view;
			BrigadesEventBus.register(this);
			setPrimaryStyleName(BrigadesTheme.MENU_ITEM);
			setEnabled(view.isEnabled());
			addClickListener(event -> BrigadesEventBus.post(new PostViewChangeEvent(view)));
		}

		@Subscribe
		public void changeView(PostViewChangeEvent event) {
			if (event.getView() == view) {
				addStyleName(BrigadesTheme.MENU_ITEM_SELECTED);
				UI.getCurrent().getNavigator().navigateTo(view.getViewName());
			} else {
				removeStyleName(BrigadesTheme.MENU_ITEM_SELECTED);
			}
		}
	}
}
