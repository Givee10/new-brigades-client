package ru.telros.brigades.client.view.reports;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.BrigadesUI.getRestTemplate;

public class AnalyticTermReportWindow implements HasLogger {
    public AnalyticTermReportWindow(){
        new OptionsWindow();
    }
private class OptionsWindow extends Window{
        ReportDateTimeLayout timeLayout;
        ComboBox<TypeWorkDto> workType;
        Button next;
        VerticalLayout mainLayout;
        public OptionsWindow(){
            setCaption("Параметры");
            timeLayout = new ReportDateTimeLayout();
            workType = initCombobox();
            next = initNextButton();
            mainLayout = new VerticalLayout();
            mainLayout.addComponents(timeLayout, workType,next);
            mainLayout.setExpandRatio(workType,1);
            setContent(mainLayout);
            center();
            UI.getCurrent().addWindow(this);
        }
        private ComboBox<TypeWorkDto> initCombobox(){
            ComboBox<TypeWorkDto> comboBox = new ComboBox<>();
            List<TypeWorkDto> temp = BrigadesUI.getRestTemplate().getTypeWorks().stream().sorted(((e1,e2)->{
                if (e1.getNumberToir()!=null && e2.getNumberToir()!=null){
                    return e1.getNumberToir().compareTo(e2.getNumberToir());
                }else if (e1.getNumberToir()==null && e2.getNumberToir()!=null){
                    return -1;
                }else if (e1.getNumberToir()!=null && e2.getNumberToir()==null){
                    return 1;
                }
                return 0;
            })).collect(Collectors.toList());

            comboBox.setItems(temp );
            comboBox.setCaption("Тип работы");
            comboBox.setWidth("100%");

            comboBox.setItemCaptionGenerator(e->e.getNumberToir()+" "+e.getDescription());
            comboBox.setPopupWidth("300%");
            return comboBox;
        }
        private Button initNextButton(){
            Button button = new Button("Далее");
            button.addClickListener(e->{
                if (checkInputFields() && timeLayout.isValid()){
                    List<Long> brigadesId = getRestTemplate().getBrigades().stream().map(e1->e1.getId()).collect(Collectors.toList());
                    List<WorkDto> works = new ArrayList<>();
                    for (int i = 0; i < brigadesId.size(); i++) {
                        //TODO-k Поменять на нормальное получение работ, как только появится

                        getRestTemplate().getBrigadeWorks(brigadesId.get(i),
                                 timeLayout.getStart(),
                                timeLayout.getEnd()).stream().forEach(e2-> works.add(e2));
                    }
                    getLogger().debug("Работы, полученные из базы по времени и типу работ");
                    List<WorkDto> workDtos = works.stream().peek(workDto -> {
                       getLogger().debug(workDto.debugInfo());
                    }).
                            filter(e1->{
                                return e1.getIdTypeWork()==workType.getSelectedItem().get().getId();
                            }).collect(Collectors.toList());
                    if (workDtos==null || workDtos.size()==0){
                        Notification.show("Работ данного типа в данном временном промежутке не найдено", Notification.Type.ERROR_MESSAGE);
                    }else {
                        UI.getCurrent().addWindow(new GridWindow(workDtos, workType.getValue(),
                                                                 timeLayout.getStart(),
                                                                timeLayout.getEnd()));
                        this.close();
                    }


                }
            });
            return button;
        }

        private boolean checkInputFields(){
            if (!workType.getSelectedItem().isPresent()){
                Notification.show("Выберете тип работы", Notification.Type.ERROR_MESSAGE);
                return false;
            }
            return true;
        }
}
private class GridWindow extends Window{
        Label titleLabel;
        Grid<GridItem> grid;
        Label TOIRNormLabel;
        Label normAISLabel;
        Label meanFactDurationOfWork;
        Label amountOfExcess;
        Label worstResult;
        double normAIS;
        VerticalLayout mainLayout;
        public GridWindow(List<WorkDto> workDtos, TypeWorkDto typeWork, ZonedDateTime start, ZonedDateTime end){
            setCaption("Результаты");
            setSizeFull();
            titleLabel = new Label();
            titleLabel.setWidth("100%");
            titleLabel.setValue("<h2 style='text-align: center'>Отчёт</h2>" +
                                "<p style='text-align: center'>по срокам выполнения работы «"+typeWork.getDescription()+"» " +
                                " за период с "+ DatesUtil.formatZonedtoDDMMYYY(start)+" по "+DatesUtil.formatZonedtoDDMMYYY(end)+"</p>");
            titleLabel.setContentMode(ContentMode.HTML);
            TOIRNormLabel = new Label("Норматив по ТОиР: "/*+BrigadesUI.getRestTemplate().getCalcNormFromTOIR(typeWork.getGuid())*/);
            normAISLabel = new Label("Принятый норматив в АИС БРИГАДЫ: "+typeWork.getNorm()+", ч");
            normAIS = typeWork.getNorm();
            grid = initGrid(workDtos, normAIS);

            List<GridItem> items = (ArrayList)((ListDataProvider<GridItem>) grid.getDataProvider()).getItems();
            double mean =
            items.stream().filter(e -> !e.getFactDuration().equals(" - ")).map(e -> DatesUtil.HHMMToMinutes(e.getFactDuration())).mapToDouble(Long::doubleValue).sum()
                    /
                    (double)items.stream().filter(e->!e.getFactDuration().equals(" - ")).count();
            meanFactDurationOfWork = new Label("Средняя факт. длительность работы по выборке (удельная): "+DatesUtil.minutesToHHMM((long)mean));
            long count = items.stream().filter(e->!e.getFactDuration().equals(" - ") && (DatesUtil.HHMMToMinutes(e.getFactDuration())>normAIS*60)).count();
            amountOfExcess = new Label("Число случаев превышения норматива по выборке: "+ count + " шт., "+
                    String.format(
                            "%.2f",(((double)count/(double)items.stream().filter(e->!e.getFactDuration().equals(" - ")).count())*100))+"%"
            );
            GridItem worstItem = items.stream().filter(e->!e.getFactDuration().equals(" - ")).
//                    peek(e-> getLogger().debug(e)).
                    max(Comparator.comparingLong(e-> DatesUtil.HHMMToMinutes(e.getFactDuration()))).orElse(null);
            if (worstItem!=null) {
                worstResult = new Label("Худший показатель: " +
                        "Длительность: " + worstItem.getFactDuration() + "; " +
                        "Дата: " + worstItem.getDate() + "; " +
                        "Время: " + worstItem.getFactStart() + "; " +
                        "Бригада: " + worstItem.getBrigade() + "; ");
            }else{
                worstResult = new Label("Отстутствуют данные для расчёта (время выполнения)");
            }
            mainLayout = new VerticalLayout(/*grid*/);
            mainLayout.addComponents(titleLabel, TOIRNormLabel, normAISLabel, grid,meanFactDurationOfWork, amountOfExcess, worstResult);
            grid.setWidth("100%");
            grid.setHeight("100%");
            mainLayout.setExpandRatio(grid,1);
            center();
            setContent(mainLayout);
        }
        private Grid<GridItem> initGrid(List<WorkDto> works, double normAIS){
            List<GridItem> gridItems = new ArrayList<>();
            works.stream().forEach(e->gridItems.add(new GridItem(e)));
            grid = new Grid<>();
            grid.addItemClickListener(e -> {
                if (e.getMouseEventDetails().isDoubleClick()){
                    WorkOrderLayout.showWorkOrder(e.getItem().getWorkDto());
                }
            });
            grid.addColumn(e->e.getWorkDto().getId()).setCaption("ID");
            grid.addColumn(e->e.getDate()).setCaption("Дата");
            grid.addColumn(e->e.getWorkNumber()).setCaption("№ работы");
            grid.addColumn(e->e.getFactStart()).setCaption("Факт. начало работы");
            grid.addColumn(e->e.getFactDuration()).setCaption("Факт. длительность работы, час");
            grid.addColumn(e->e.factDeviationTOIR).setCaption("Отклонение от норматива ТОиР, час");
            grid.addColumn(e->e.getFactDeviationAIS(normAIS)).setCaption("Отклонение от норматива АИС, час");
            grid.addColumn(e->e.getBrigade()).setCaption("Бригада");
            grid.addColumn(e->e.getBrigadeMembersCount()).setCaption("Число членов бригады");
            grid.addColumn(e->e.GLCode).setCaption("Код ГЛ");
            grid.addColumn(e->e.doneDetailsPercent).setCaption("% выполн. деталей работы");
            grid.addComponentColumn(e->{
                Button button = new Button(""+e.getServiceElementsCount());
                button.addClickListener(clickEvent -> {
                    if (!e.getServiceElementsCount().equals("0")){
                         new GridItemElementsWindow(e.getWorkDto().getId());
                    }
                });
                return button;
            }).setCaption("К-во эл. обслуж. факт.");
            grid.addColumn(e->e.getSMM()).setCaption("СММ");
            grid.addColumn(e->e.getSpecialTech()).setCaption("Спец. техника");
            grid.addColumn(e->e.getComment()).setCaption("Комментарий к работе");
            grid.setItems(gridItems);

            return grid;
        }

}
public class GridItem{
        private String date;
        private String workNumber;
        private String factStart;
        private String factDuration;
        private String factDeviationTOIR;
        private String factDeviationAIS;
        private BrigadeDto brigade;
        private String brigadeMembersCount;
        private String GLCode;
        private String doneDetailsPercent;
        private String serviceElementsCount;
        private String SMM;
        private String specialTech;
        private String comment;
        private WorkDto workDto;
        public GridItem(WorkDto workDto){
            setDate(workDto);
            setWorkNumber(workDto);
            setFactStart(workDto);
            setFactDuration(workDto);
            setFactDeviationTOIR(workDto);
//            setFactDeviationAIS(workDto);
            setBrigade(workDto);
            setBrigadeMembersCount(workDto);
            setGLCode(workDto);
            setDoneDetailsPercent(workDto);
            setServiceElementsCount(workDto);
            setSMM(workDto);
            setSpecialTech(workDto);
            setComment(workDto);
            setWorkDto(workDto);
        }

    public void setWorkDto(WorkDto workDto) {
        this.workDto = workDto;
    }

    public void setDate(WorkDto workDto) {
        this.date = DatesUtil.formatZonedtoDDMMYYY(workDto.getStartDate());
    }

    public void setWorkNumber(WorkDto workDto) {
        this.workNumber = workDto.getNumber();
    }

    public void setFactStart(WorkDto workDto) {
        this.factStart = DatesUtil.formatZonedtoMMHH(workDto.getStartDate());
    }

    public void setFactDuration(WorkDto workDto) {
            try {
                this.factDuration =
                        DatesUtil.secondsToHHMM((workDto.getFinishDate().toEpochSecond()-workDto.getStartDate().toEpochSecond()));
            }catch (NullPointerException e){
                this.factDuration = " - ";
            }
    }

    public void setFactDeviationTOIR(WorkDto workDto) {
            //TODO-K Спросить
        this.factDeviationTOIR = getRestTemplate().getCalcNormFromTOIR(workDto.getGuid());
//        this.factDeviationTOIR = "??";
    }

    public void setFactDeviationAIS(WorkDto workDto) {
        //сделано в get

    }

    public void setBrigade(WorkDto workDto) {
            try {
                this.brigade = getRestTemplate().getBrigade(workDto.getIdBrigade());
            }catch (NullPointerException e){
                this.brigade = null;
            }
    }

    public void setBrigadeMembersCount(WorkDto workDto) {
            try{
                int count;
                count = (int) getRestTemplate().getWorkEmployees(workDto.getId()).stream().filter(e->e.getManager().booleanValue()==false).count();
                if (count==0){
                    count = getRestTemplate().getBrigadeEmployees(workDto.getIdBrigade()).size();
                }
                this.brigadeMembersCount = count+" (по ТОиР)";
            }catch (NullPointerException e){
                this.brigadeMembersCount = " - ";
            }

    }

    public void setGLCode(WorkDto workDto) {
        this.GLCode = workDto.getCode();
    }

    public void setDoneDetailsPercent(WorkDto workDto) {
        List<WorkItemDto> all = getRestTemplate().getWorkItems(workDto.getId());
        if (all.size()!=0) {
            this.doneDetailsPercent = new DecimalFormat("#.0#").format(
                    ((double)all.stream().filter(e -> e!=null && e.getStatus()!=null && e.getStatus().equals("Действие выполнено")).count()
                         / (double)all.size()*100.0/100)*100);
        }else {
            this.doneDetailsPercent= " - ";
        }
    }

    public void setServiceElementsCount(WorkDto workDto) {
        this.serviceElementsCount = getRestTemplate().getElements(workDto.getId()).size()+"";
    }

    public void setSMM(WorkDto workDto) {
            try {
                this.SMM = getRestTemplate().getWorkMechanization(workDto.getId()).size()+"";
            }catch (NullPointerException e){
                this.SMM = "0";
            }
    }

    public void setSpecialTech(WorkDto workDto) {
        try {
            this.specialTech = getRestTemplate().getWorkEquipments(workDto.getId()).size()+"";
        }catch (NullPointerException e){
            this.specialTech = "0";
        }
    }

    public void setComment(WorkDto workDto) {
        StringBuilder desc = new StringBuilder("");
        List<CommentDto> comments = getRestTemplate().getWorkComments(workDto.getId());
        try {
            comments.stream().forEach(e->{
                if (!e.getContent().endsWith(".")){
                    desc.append(e.getContent()).append(".");
                }
                desc.append(" ");
            });
        }catch (NullPointerException ex){
        }
        this.comment = desc.toString();
    }

    public String getDate() {
        return date;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public String getFactStart() {
        return factStart;
    }

    public String getFactDuration() {
        return factDuration;
    }

    public String getFactDeviationTOIR() {
        return factDeviationTOIR;
    }

    public String getFactDeviationAIS(double normAIS) {
        if (this.getFactDuration().equals(" - ")){
            this.factDeviationAIS=(" - ");
        }else {
            double dif = DatesUtil.HHMMToMinutes(this.getFactDuration())-normAIS*60;
            if (dif>0){
                this.factDeviationAIS =" + " + DatesUtil.minutesToHHMM((long) dif);
            }else {
                this.factDeviationAIS = " - " + DatesUtil.minutesToHHMM((long) Math.abs(dif));
            }

        }
        return factDeviationAIS;
    }

    public String getBrigade() {
            return brigade==null ? " - ":brigade.getName();
    }

    public String getBrigadeMembersCount() {
        return brigadeMembersCount;
    }

    public String getGLCode() {
        return GLCode;
    }

    public String getDoneDetailsPercent() {
        return doneDetailsPercent;
    }

    public String getServiceElementsCount() {
        return serviceElementsCount;
    }

    public String getSMM() {
        return SMM;
    }

    public String getSpecialTech() {
        return specialTech;
    }

    public String getComment() {
        return comment;
    }

    public WorkDto getWorkDto() {
        return workDto;
    }
}
private class GridItemElementsWindow extends Window{
        Grid<ElementDto> grid;
        public GridItemElementsWindow(Long workId){
            grid = new Grid<>();
            AtomicLong counter = new AtomicLong();
            grid.addColumn(e->counter.incrementAndGet()).setCaption("№");
            grid.addColumn(e->e.getType()).setCaption("№");
            grid.addColumn(e->e.getLength()+", "+e.getLengthUnit()).setCaption("К-во, шт/" +"Длина, м");
            grid.addColumn(e->e.getDiameter()+", "+e.getDiameterUnit()).setCaption("Диаметр, мм");
            grid.addColumn(e->e.getMaterial()).setCaption("Материал");
            grid.setItems(getRestTemplate().getElements(workId));
        }
}
}

