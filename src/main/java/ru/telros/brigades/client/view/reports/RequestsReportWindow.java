package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.OrganizationDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.dto.StatusDto;
import ru.telros.brigades.client.ui.request.RequestConstants;
import ru.telros.brigades.client.ui.request.RequestGrid;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class RequestsReportWindow extends DynamicReportWindow<RequestGrid, RequestDto> implements HasLogger {
    //для возможности отображать заявки с определенным статусом
    public Grid<String> requestStatusGrid;
    public PopupView requestStatusView;
    public Button requestStatusButton;

    Grid<String> organizationGrid;
    PopupView organizationView;
    Button organizationButton;

    public Grid<StatusDto> statusGrid;
    public PopupView statusView;
    public Button statusButton;
    public RequestsReportWindow(String title) {
        super(title, new RequestGrid(RequestConstants.getMainColumnsAndStatus(), new ArrayList<>()), RequestConstants.getMainColumnsAndStatus());

        requestStatusGrid = initRequestStatusGrid();
        Button closeButton = new Button("Закрыть");
        closeButton.addClickListener(e->{
            requestStatusView.setPopupVisible(false);
        });
        VerticalLayout layout = new VerticalLayout(requestStatusGrid, closeButton);
        requestStatusView = new PopupView(null, layout);
        requestStatusView.setHideOnMouseOut(false);
        requestStatusButton = new Button("Показать:");
        requestStatusButton.addClickListener(e->{
            requestStatusView.setPopupVisible(true);
        });

        organizationGrid = initOrganizationGrid();
        Button closeButton1 = new Button("Закрыть");
        closeButton.addClickListener(e->{
            organizationView.setPopupVisible(false);
        });
        VerticalLayout layout1 = new VerticalLayout(organizationGrid, closeButton1);
        organizationView = new PopupView(null, layout1);
        organizationView.setHideOnMouseOut(false);
        organizationButton = new Button("Организации");
        organizationButton.addClickListener(e->{
            organizationView.setPopupVisible(true);
        });

        statusGrid = initStatusGrid();
        Button statusCloseButton = new Button("Закрыть");
        statusCloseButton.addClickListener(clickEvent -> {
            statusView.setPopupVisible(false);
        });
        VerticalLayout statusLayout = new VerticalLayout(statusGrid, statusCloseButton);
        statusView = new PopupView(null,statusLayout);
        statusView.setHideOnMouseOut(false);
        statusButton = new Button("Статус");
        statusButton.addClickListener(clickEvent -> {
            statusView.setPopupVisible(true);
        });
        this.parameters.addComponent(new HorizontalLayout(organizationButton,organizationView),this.parameters.getComponentCount()-2);
        this.parameters.addComponent(new HorizontalLayout(requestStatusButton,requestStatusView),this.parameters.getComponentCount()-2);
        this.parameters.addComponent(new HorizontalLayout(statusButton,statusView),this.parameters.getComponentCount()-2);
    }
    private Grid<StatusDto> initStatusGrid(){
        Grid<StatusDto> statusDtoGrid = new Grid<>();


        statusDtoGrid.setItems(BrigadesUI.getRestTemplate().getHLStatuses());
        statusDtoGrid.addColumn(e->e.getName());
        statusDtoGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        return statusDtoGrid;
    }
    private Grid<String> initRequestStatusGrid(){
        Grid<String> workStatusGrid = new Grid<>();
        workStatusGrid.addColumn(e -> e.toString()).setCaption("Статус").setId("Status");
        workStatusGrid.setItems("Было", "Поступило", "Выполнено","Остаток");
        workStatusGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        workStatusGrid.getSelectionModel().select("Было");
        workStatusGrid.getSelectionModel().select("Поступило");
        workStatusGrid.getSelectionModel().select("Выполнено");
        workStatusGrid.getSelectionModel().select("Остаток");
        workStatusGrid.addSelectionListener(e->{
        });
        return workStatusGrid;
    }

    public List<RequestDto> getItems(){
        List<RequestDto> allRequests = BrigadesUI.getRestTemplate().getRequests();
//        allRequests = allRequests.stream().filter(e-)
        getLogger().debug("IN DB: "+allRequests.size());
        List<RequestDto> filteredRequests = applyRequestHLStatusFilter(
                                            applyOrganizationFilter(
                                            applyRequestStatusFilter(requestStatusGrid.getSelectedItems(),allRequests)));
        return filteredRequests;
    }


    @Override
    public void customUpdateButtonHandle(String reportType) {

    }

    @Override
    public Button initSaveReportButton() {
        Button button = new Button("Сохранить");
        button.addClickListener(clickEvent ->{
                    saveReport("Отчет по заявкам\r\n"+
                            "Время: "+timeLayout.getStart().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT))+" - "+timeLayout.getEnd().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)));

        });
        return button;
    }
    public void displayCustom(String rep){

    }
    public void displayDynamic(){
        grid.leaveShownOnly(columnsGrid.getSelectedItems());
        columnsButton.setVisible(true);
    }
    public void updateGrid(){
        try {
                    List<RequestDto> items = getItems();
                    grid.setItems(items);

        }catch (NoSuchElementException e){
            Notification.show("Не задан вид отчета", Notification.Type.ERROR_MESSAGE);
        }

    }
    private Grid<String> initOrganizationGrid(){
        Grid<String> grid = new Grid<>();
        grid.addColumn(e->e.toString()).setId("organization").setCaption("Организация");
        Set<OrganizationDto> orgs = new HashSet<>(BrigadesUI.getRestTemplate().getOrganizations().stream().filter(e->e.getType().equals("REGION")).collect(Collectors.toList()));
        grid.setItems(orgs.stream().map(e->e.getName()).distinct().collect(Collectors.toList()));
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        return grid;
    }
    /*private List<RequestDto> applyDataFilter(List<RequestDto> requestDtos){
        ZonedDateTime from = timeLayout.getStart();
        ZonedDateTime to = timeLayout.getStart();
        requestDtos.stream().filter(e->{
            if ()
        });
    }*/
    private List<RequestDto> applyOrganizationFilter(List<RequestDto> requests){
        if (organizationGrid.getSelectedItems().size()==0){
            Notification.show("Выберите организации", Notification.Type.ERROR_MESSAGE);
            return null;
        }else {
            return  requests.stream().filter(e->organizationGrid.getSelectedItems().contains(e.getOrganization())).collect(Collectors.toList());
        }
    }
    //статусы из горячей линии
    protected List<RequestDto> applyRequestHLStatusFilter(List<RequestDto> requestsToFilter){
        List<String> statuses = statusGrid.getSelectedItems().stream().map(statusDto -> statusDto.getName()).collect(Collectors.toList());
        if (statusGrid.getSelectedItems().size()==0){
            Notification.show("Выберите статусы заявок", Notification.Type.ERROR_MESSAGE);
            return null;
        }
        return requestsToFilter.stream().filter(requestDto -> statuses.contains(requestDto.getStatusHL())).collect(Collectors.toList());
    }
    //статусы фактические
    private List<RequestDto> applyRequestStatusFilter(Set<String> selectedTypes, List<RequestDto> requestsToFilter){
        if (selectedTypes==null || selectedTypes.size()==0){
            return requestsToFilter;
        }
        //Set, т.к. запрос может иметь одновременно несколько статусов
        Set<RequestDto> outputRequests = new HashSet<>();
        ZonedDateTime givenStartTime = timeLayout.getStart();
        ZonedDateTime givenEndTime = timeLayout.getEnd();
        requestsToFilter.stream().forEach(e->{
            ZonedDateTime curRegDate = e.getRegistrationDate();
            ZonedDateTime curCloseDate = e.getCloseDate();
            if (selectedTypes.contains("Было")){
                if (curRegDate.isBefore(givenStartTime)&&(curCloseDate==null || curCloseDate.isAfter(curRegDate))){
                    outputRequests.add(e);
                }
            }
            if (selectedTypes.contains("Поступило")){
                if(curRegDate.isAfter(givenStartTime)&&curRegDate.isBefore(givenEndTime)){
                    outputRequests.add(e);
                }
            }
            if (selectedTypes.contains("Выполнено")){
                if (curCloseDate!=null && curCloseDate.isBefore(givenEndTime)&&curCloseDate.isAfter(givenStartTime)){
                    outputRequests.add(e);
                }
            }
            if (selectedTypes.contains("Остаток")){
                if (curRegDate!=null && curRegDate.isBefore(givenEndTime)&&(curCloseDate==null || curCloseDate.isAfter(givenEndTime))){
                    outputRequests.add(e);
                }
            }
        });
        return new ArrayList<>(outputRequests) ;
    }

}
