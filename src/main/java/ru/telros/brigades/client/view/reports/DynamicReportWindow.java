package ru.telros.brigades.client.view.reports;

import com.lowagie.text.DocumentException;
import com.vaadin.ui.*;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.AbstractEntity;
import ru.telros.brigades.client.reportbuilders.DOCXBuilder;
import ru.telros.brigades.client.reportbuilders.PDFBuilder;
import ru.telros.brigades.client.reportbuilders.XLSBuilder;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


/*
//РАБОТЫ
        WorkOrderConstants.getMainColumns();
        BrigadesUI.getRestTemplate().getWorks();
        //ЗАЯВКИ
        RequestConstants.getMainColumns();
        BrigadesUI.getRestTemplate().getRequests();
        //БРИГАДЫ
        WorkGroupColumn.getMainColumns();
        BrigadesUI.getRestTemplate().getBrigades();
        //СОТРУДНИКИ
        BrigadesUI.getRestTemplate().getEmployees();
        //СОБЫТИЯ
        EventConstants.getMainColumns();
        BrigadesUI.getRestTemplate().getEvents();
 */

//Шаблон для вывода настраиваемых отчетов
public abstract class DynamicReportWindow<GRID extends BrigadesGrid, DTO extends AbstractEntity> extends Window implements HasLogger {
    GRID grid;

    ComboBox<String> reportFormat;


    Label reportTypeText;
    VerticalLayout content;
    VerticalLayout buttons;
    HorizontalLayout parameters;
    Button refreshButton;
    Grid<GridColumn<DTO>> columnsGrid;
    Button columnsButton;
    PopupView columnsView;
    ReportDateTimeLayout timeLayout;


    HorizontalLayout saveOptions;
    Button print;
    Button watch;
    Button save;
    public DynamicReportWindow(String title, GRID grid, List<GridColumn<DTO>> columns){
        super(title);
        setSizeFull();
        center();
        setClosable(true);
        refreshButton = new Button("Обновить");

        refreshButton.addClickListener(clickEvent -> {
            getLogger().debug("Обновить ");
           updateGrid();
        });

        reportTypeText = new Label("Вид отчета");



        initColumnsGrid(columns);

        Button closeButton = new Button("Закрыть");
        closeButton.addClickListener(e->{
            columnsView.setPopupVisible(false);
        });
        VerticalLayout layout = new VerticalLayout(columnsGrid, closeButton);

//        columnsView = new PopupView(null, columnsGrid);
        columnsView = new PopupView(null, layout);

        columnsButton = new Button("Колонки");
        columnsButton.addClickListener(clickEvent -> {
            columnsView.setPopupVisible(true);
        });
        columnsView.setHideOnMouseOut(false);
        columnsButton.setWidth("100");
        refreshButton.setWidth("100");
        timeLayout = new ReportDateTimeLayout();

        buttons = new VerticalLayout();
//        buttons.addComponents(refreshButton);
//        buttons.setComponentAlignment(refreshButton, Alignment.MIDDLE_CENTER);
//        setItems();
        parameters = new HorizontalLayout();
//        parameters.addComponents(timeGap, columnsButton,columnsView,buttons);
        parameters.addComponents(timeLayout, columnsButton,columnsView,refreshButton);
        parameters.setHeight("100px");

        this.grid = grid;
        grid.hideColumns();
        saveOptions = initSaveOptionsPanel();

        content = new VerticalLayout();
        content.addComponents(parameters,grid/*,saveOptions*/);
        content.setExpandRatio(grid,1);
        grid.setWidth("100%");
        content.setHeight("100%");
        grid.setHeight("100%");
        setContent(content);

    }
    public void updateGrid(){
            try {

                        List<DTO> items = getItems();
                        if (items!=null) {

                            grid.update(items);
                        }

            }catch (NoSuchElementException e){
                Notification.show("Не задан вид отчета", Notification.Type.ERROR_MESSAGE);
            }catch (NullPointerException e1){
                //уведомления об отсутсвии чего-либо необходимого выбрасываются в классах-наследниках
            }

    }
    public HorizontalLayout initSaveOptionsPanel(){
        HorizontalLayout mainLayout = new HorizontalLayout();
        reportFormat = new ComboBox<>("Формат отчёта");
        reportFormat.setItems("XLS", "DOC", "PDF");
        reportFormat.setSelectedItem("XLS");
        reportFormat.setWidth("100");
        print = new Button("Печать");
        print.setWidth("100");
        print.addClickListener(e->{
            //обработка нажатия кнопки печати
        });
        watch= new Button("Просмотр");
        watch.setWidth("100");
        watch.addClickListener(e->{
            //обработка нажатия кнопки просмотра
        });
        save = initSaveReportButton();
        save.setWidth("100");
        VerticalLayout otherButtons = new VerticalLayout();
        otherButtons.addComponents(print,watch);
        VerticalLayout saveOptions = new VerticalLayout();
        saveOptions.addComponents(reportFormat, save);
        mainLayout.addComponents(otherButtons, saveOptions);
        return mainLayout;
    }
    public void saveReport (String title) {

        List<DTO> items = grid.getItems();
        List<Grid.Column<DTO, ?>> columns = grid.getShownColumns();
        //id - название поля на английском для получения значения через Reflection , caption - на русском для вывода в шапку таблицы
        List<String> tableHeaders = columns.stream().map(s -> s.getCaption()).collect(Collectors.toList());
        List<String> columnIDs = columns.stream().map(s -> s.getId()).collect(Collectors.toList());
        /*String title = "Отчет по работам\r\n"+
                "Бригада: "+brigadeName.getValue()+"\r\n"+
                "Время: "+startDateField.getValue().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT))+" - "+endDateField.getValue().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT));*/
        switch (reportFormat.getValue()) {
            case "PDF":
                try {
                    PDFBuilder<DTO> pdfBuilder = new PDFBuilder<DTO>("test.pdf", title, columns, items);
                    pdfBuilder.saveToFile();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }


                break;
            case "DOC":
               /* DOCXBuilder docxBuilder = DOCXBuilder.newBuilder().setDestination("test.docx").setTitle(title).setTable(tableHeaders, workList, columnIDs).build();
                docxBuilder.saveToFile();*/
               DOCXBuilder<DTO> docxBuilder = new DOCXBuilder<>("test.docx", title, columns,items);
               docxBuilder.saveToFile();
                break;
            case "XLS":
                /*XLSBuilder xlsBuilder = new XLSBuilder();
                xlsBuilder.saveToFile("test.xls", title, tableHeaders, workList, columnIDs);*/
                XLSBuilder<DTO> xlsBuilder = new XLSBuilder<>();
                xlsBuilder.saveToFile("test.xls", title, columns, items);
                break;

        }
    }
    public void initColumnsGrid(List<GridColumn<DTO>> columns){
        List<GridColumn<DTO>> columnNames = columns;

        columnsGrid = new Grid<GridColumn<DTO>>();
        columnsGrid.addColumn(GridColumn::getCaption);
        columnsGrid.setItems(columnNames);
        columnsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        columnsGrid.addSelectionListener(selectionEvent -> {
            grid.hideColumns();
            selectionEvent.getAllSelectedItems().stream().forEach(e->{
                grid.showColumns(e.getId());
            });
        });


    }

    public abstract List<DTO> getItems();
    public abstract void customUpdateButtonHandle(String reportType);
    public abstract Button initSaveReportButton();


    public   abstract void displayCustom(String name) ;
    public abstract void displayDynamic() ;


}
