package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupView;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.dto.OrganizationDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;
import ru.telros.brigades.client.ui.workorder.WorkOrderConstants;
import ru.telros.brigades.client.ui.workorder.WorkOrderGrid;
import ru.telros.brigades.client.view.reports.util.GridWithButtonLayout;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class WorksReportWindow extends DynamicReportWindow<WorkOrderGrid, WorkDto> implements HasLogger {
    //для выбора сразу нескольких бригад
    Grid<BrigadeDto> brigadeSelectionGrid;
    PopupView brigadesSelectionView;
    Button brigadesSelectionButton;
    //для возможности отображать работы с определенным статусом
    Grid<String> workStatusGrid;
    PopupView workStatusView;
    Button workStatusButton;

    Grid<OrganizationDto> organizationGrid;
    PopupView organizationView;
    Button organizationButton;

    public WorksReportWindow(String title) {
        super(title, new WorkOrderGrid(WorkOrderConstants.getReportColumns(), new ArrayList<>()), WorkOrderConstants.getReportColumns());

        brigadeSelectionGrid = initBrigadeSelectionGrid();
        brigadesSelectionButton = new Button("Бригады");
        GridWithButtonLayout brigadesLayout = new GridWithButtonLayout(brigadeSelectionGrid,brigadesSelectionButton);

        workStatusGrid = initWorkStatusGrid();
        workStatusButton = new Button("Показать: ");
        GridWithButtonLayout workStatusLayout = new GridWithButtonLayout(workStatusGrid,workStatusButton);

        organizationGrid = initOrganizationGrid();
        organizationButton = new Button("Организации");
        GridWithButtonLayout organizationLayout = new GridWithButtonLayout(organizationGrid,organizationButton);
        this.parameters.addComponent(organizationLayout,this.parameters.getComponentCount()-2);
        this.parameters.addComponent(brigadesLayout,this.parameters.getComponentCount()-2);
        this.parameters.addComponent(workStatusLayout,this.parameters.getComponentCount()-2);
    }

    //инит выпадающего грид для выбора бригад
    public Grid<BrigadeDto> initBrigadeSelectionGrid(){
        Grid<BrigadeDto> brigadeSelectionGrid = new Grid<>();
        brigadeSelectionGrid.addColumn(e -> e.getName()).setCaption("Бригада").setId("Brigade");

        brigadeSelectionGrid.setItems(BrigadesUI.getRestTemplate().getBrigades());
        brigadeSelectionGrid.setSelectionMode(Grid.SelectionMode.MULTI);

        return brigadeSelectionGrid;
    }
    public Grid<String> initWorkStatusGrid(){
        Grid<String> workStatusGrid = new Grid<>();
        workStatusGrid.addColumn(e -> e.toString()).setCaption("Статус").setId("Status");
        workStatusGrid.setItems("Начатые", "Активные", "Закрытые", "Законченные в РВ" );
        workStatusGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        workStatusGrid.getSelectionModel().select("Начатые");
        workStatusGrid.getSelectionModel().select("Активные");
        workStatusGrid.getSelectionModel().select("Закрытые");
        workStatusGrid.getSelectionModel().select("Законченные в РВ");
        workStatusGrid.addSelectionListener(e->{

        });
        return workStatusGrid;
    }
    //вызывается при выборе бригад в выпадающем списке
    /*private List<WorkDto> applyBrigadesSelectionFilter(List<WorkDto> works ){
        return works.stream().filter(e->brigadeSelectionGrid.getSelectedItems().contains(e.getBrigade())).collect(Collectors.toList());

    }*/
    //Вызывается при изменении выбора статуса работ в выпадающем списке
    private List<WorkDto> applyWorkStatusFilter(Set<String> selectedTypes, List<WorkDto> inputWorks){

            if (selectedTypes != null && selectedTypes.size() > 0) {
                List<WorkDto> displayedWorks = inputWorks;

                if (displayedWorks != null && displayedWorks.size() != 0) {
                    Set<WorkDto> filteredWorksToShow = new HashSet<>();
                    ZonedDateTime startTime = timeLayout.getStart();
                    ZonedDateTime endTime = timeLayout.getEnd();
                    displayedWorks.stream().forEach(e -> {
                        ZonedDateTime curWorkCloseDate = e.getCloseDate();
                        ZonedDateTime curWorkRegDate = e.getStartDate();
                        //Это тут потому, что в телрос-базе есть рег даты с null значениями -->
                        if (curWorkRegDate==null){
                            curWorkRegDate=ZonedDateTime.now();
                        }
                        ZonedDateTime curWorkFinishDate = e.getFinishDate();
                        StringBuilder statusString = new StringBuilder();
                        if (selectedTypes.contains("Начатые")) {
                            if ((curWorkRegDate.isAfter(startTime)) && (curWorkRegDate.isBefore(endTime))) {
                                statusString.append("(Начатые)");
                                filteredWorksToShow.add(e);
                            }
                        }
                        if (selectedTypes.contains("Активные")) {
                            if (
                                    //Работа не закончена на ЗВО (не закнчена вообще или закончена после ЗВО)
                                    //заменить на факт дату начана          законченные (ьригадой)
                                    ((curWorkRegDate.isBefore(endTime)) && (curWorkFinishDate == null || curWorkFinishDate.isAfter(endTime)))
                            ) {
                                statusString.append("(Активные)");
                                filteredWorksToShow.add(e);
                            }
                        }
                        if (selectedTypes.contains("Законченные")) {
                            //Варианты ниже от Елены Викторовны.
                            //дата начала до конца промежутка
                            if (curWorkRegDate.isBefore(endTime) && curWorkFinishDate!=null && curWorkFinishDate.isAfter(startTime) && curWorkFinishDate.isBefore(endTime)) {
                                statusString.append("(Законченные)");
                                filteredWorksToShow.add(e);
                            }
                        }
                        if (selectedTypes.contains("Закрытые в РВ")) {
                            //дата начала до конца промежутка
                            if (curWorkRegDate.isBefore(endTime) && curWorkCloseDate!=null && curWorkCloseDate.isAfter(startTime) && curWorkCloseDate.isBefore(endTime)) {
                                statusString.append("(Закрытые в РВ)");
                                filteredWorksToShow.add(e);
                            }
                        }
                        //Будет работать нормально, если хэшкод работает нормально
                        filteredWorksToShow.add(e);
                    });

                    return new ArrayList<>(filteredWorksToShow);
                }
            }
            return Collections.emptyList();


    }
    @Override
    public List<WorkDto> getItems() {
        getLogger().debug("get items");
        getLogger().debug(String.valueOf(checkBrigades()));
        getLogger().debug(StringUtil.returnDebugString(organizationGrid.getSelectedItems()));
        if (checkBrigades()&&
                organizationGrid.getSelectedItems().size() != 0) {
                /*brigadeDtos.stream().forEach(e -> allBrigadesWorks.addAll(BrigadesUI.getRestTemplate().getBrigadeWorks(e.getId(),DatesUtil.localToZoned(start), DatesUtil.localToZoned(end))));
                List<WorkDto> worksToDisplay = applyWorkStatusFilter(workStatusGrid.getSelectedItems(), allBrigadesWorks);*/

            getLogger().debug("entered");
            Set<OrganizationDto> selectedOrganizations = organizationGrid.getSelectedItems();
            Set<BrigadeDto> selectedBrigades = brigadeSelectionGrid.getSelectedItems();
            List<WorkDto> allBrigadesWorks = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            selectedBrigades.stream().forEach(brigadeDto -> {
//                selectedOrganizations.stream().forEach(organizationDto -> {
                    List<WorkDto> workDtos = BrigadesUI.getRestTemplate().getWorksWithBrigadeAndOrganization(
//                            timeLayout.getStart(),timeLayout.getEnd(),brigadeDto.getId(),organizationDto.getName());
                            timeLayout.getStart(),timeLayout.getEnd(),brigadeDto.getId(),null);
//                    sb.append(timeLayout.getStart()+"; "+timeLayout.getEnd()+"; "+brigadeDto.getId()+"; "+organizationDto.getName()+":\t"+workDtos.size()+"\n");
                    sb.append(timeLayout.getStart()+"; "+timeLayout.getEnd()+"; "+brigadeDto.getId()+"; "+null+":\t"+workDtos.size()+"\n");
                    allBrigadesWorks.addAll(workDtos);
//               });
            });
            getLogger().debug(sb.toString());
            List<WorkDto> buf = applyWorkStatusFilter(workStatusGrid.getSelectedItems(), allBrigadesWorks);
//                List<WorkDto> worksToDisplay = applyOrganizationFilter(buf);
//                return worksToDisplay;
            return buf;

        }
        return null;
    }



    @Override
    public void customUpdateButtonHandle(String reportType) {

    }

    @Override
    public Button initSaveReportButton() {

        Button button = new Button("Сохранить");
        button.addClickListener(clickEvent ->{
            Set<BrigadeDto> brigades = brigadeSelectionGrid.getSelectedItems();
            if (brigades.size()==0){
                Notification.show("Выберите бригаду", Notification.Type.ERROR_MESSAGE);

            }else {
                StringBuilder brigadesString = new StringBuilder("Бригады: ");
                brigades.stream().forEach(e -> {
                    brigadesString.append(e.getName());
                });
                saveReport("Отчет по работам\r\n" +
                        brigadesString + "\r\n" +
                        "Время: " + timeLayout.getStart().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)) + " - " + timeLayout.getEnd().format(DateTimeFormatter.ofPattern(DatesUtil.GRID_DATE_FORMAT)));
            }
        });
        return button;
    }

    @Override
    public void displayCustom(String name) {

    }

    @Override
    public void displayDynamic() {
        grid.leaveShownOnly(columnsGrid.getSelectedItems());
        columnsButton.setVisible(true);
    }
    private Grid<OrganizationDto> initOrganizationGrid(){
        Grid<OrganizationDto> grid = new Grid<>();
        grid.addColumn(e->e.getName()).setId("organization").setCaption("Организация");
        Set<OrganizationDto> orgs = new HashSet<>(BrigadesUI.getRestTemplate().getOrganizations().stream().filter(e->e.getType().equals("REGION")).collect(Collectors.toList()));
        grid.setItems(orgs);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        return grid;
    }
    private boolean checkBrigades(){
        if(brigadeSelectionGrid.getSelectedItems().size() != 0){
            return true;
        }else{
            Notification.show("Выберите бригады", Notification.Type.ERROR_MESSAGE);
            return false;
        }
    }


}
