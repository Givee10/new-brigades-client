package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.Button;
import ru.telros.brigades.client.dto.BrigadeDto;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;

import java.util.List;

public class BrigadesReportWindow extends DynamicReportWindow<BrigadesGrid, BrigadeDto> {

         //todo-k BrigadesGrid - abstract. Дописать методы
    public BrigadesReportWindow(String title) {
        super(null, null, null);

    }

    @Override
    public List<BrigadeDto> getItems() {
        return null;
    }

    @Override
    public void customUpdateButtonHandle(String reportType) {

    }

    @Override
    public Button initSaveReportButton() {
        return null;
    }

    @Override
    public void displayCustom(String name) {

    }

    @Override
    public void displayDynamic() {

    }
}
