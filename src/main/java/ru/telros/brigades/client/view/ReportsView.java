package ru.telros.brigades.client.view;

import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.HeaderLayout;
import ru.telros.brigades.client.ui.component.Title;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.view.reports.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ReportsView extends HeaderLayout implements View, HasLogger {
	public static final String NAME = "reports";
	public static final String CAPTION = "Отчеты";

//	private ResizableCssLayout gridWrapper = new ResizableCssLayout();
//	private ResizableCssLayout formWrapper = new ResizableCssLayout();
//	private AbsoluteLayout absoluteLayout = new AbsoluteLayout();

	private List<Report> reports = generateReports();
	private Grid<Report> reportGrid = new Grid<>();
	public ReportsView() {
		addToHeader(new Title(CAPTION));
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		reportGrid.addColumn(Report::getNumber).setId("id").setCaption("№");
		reportGrid.addColumn(Report::getName).setId("name").setCaption("Название");
		reportGrid.addColumn(Report::getDescription).setId("description").setCaption("Описание");
		reportGrid.setItems(reports);
		reportGrid.setSelectionMode(Grid.SelectionMode.NONE);
		reportGrid.addItemClickListener(e ->{
			if(e.getMouseEventDetails().getButton() == MouseEventDetails.BUTTON_LEFT){
                openReport(e.getItem());
            }


		});
//		reportGrid.asSingleSelect().addValueChangeListener(e ->{
//			openReport(e.getValue());
//			reportGrid.deselectAll();
//		});
		reportGrid.setSizeFull();
		VerticalLayout gridLayout = CompsUtil.getVerticalWrapperWithMargin(reportGrid);
		addComponentAndRatio(gridLayout, 1);

//		Grid grid = new Grid();
//		grid.setSizeFull();
//		Grid table = new Grid();
//		table.setSizeFull();
//
//		gridWrapper.setResizable(true);
//		//gridWrapper.setCaption("Resize from grid's edges");
//		gridWrapper.setHeight(500, Unit.PIXELS);
//		gridWrapper.setWidth(400, Unit.PIXELS);
//		gridWrapper.addComponent(grid);
//		gridWrapper.setResizeLocations(ResizeLocation.RIGHT);
//		//gridWrapper.setResizeLocationSize(700);
//
//		formWrapper.setResizable(false);
//		//formWrapper.setCaption("Resize form");
//		formWrapper.setHeight(400, Unit.PIXELS);
//		formWrapper.setWidth(400, Unit.PIXELS);
//		formWrapper.addComponent(table);
//		formWrapper.setResizeLocations(ResizeLocation.RIGHT);
//
//		//absoluteLayout.setSizeFull();
//		//absoluteLayout.addComponent(gridWrapper, "top:50px; left:0px");
//		//absoluteLayout.addComponent(formWrapper, "top:50px; left:400px");
//
//		ResizableCssLayout.ResizeListener listener = new ResizableCssLayout.ResizeListener() {
//			@Override
//			public void resizeStart(ResizableCssLayout.ResizeStartEvent resizeStartEvent) {
//				getLogger().debug("ResizeStartEvent");
//				getLogger().debug("Height: {}, Width: {}", resizeStartEvent.getHeight(), resizeStartEvent.getWidth());
//				getLogger().debug("Resize Location: {}", resizeStartEvent.getResizeLocation().name());
//			}
//
//			@Override
//			public void resizeEnd(ResizableCssLayout.ResizeEndEvent resizeEndEvent) {
//				getLogger().debug("ResizeEndEvent");
//				getLogger().debug("Height: {}, Width: {}", resizeEndEvent.getHeight(), resizeEndEvent.getWidth());
//				if (resizeEndEvent.getWidth() > 700) {
//					resizeCancel(new ResizableCssLayout.ResizeCancelEvent((ResizableCssLayout)resizeEndEvent.getSource()));
//				} else {
//					formWrapper.setWidth(800 - resizeEndEvent.getWidth(), Unit.PIXELS);
//				}
//				//getLogger().debug("Left Wrapper - Height: {}, Width: {}", gridWrapper.getState().height, gridWrapper.getState().width);
//				//getLogger().debug("Right Wrapper - Height: {}, Width: {}", formWrapper.getState().height, formWrapper.getState().width);
//			}
//
//			@Override
//			public void resizeCancel(ResizableCssLayout.ResizeCancelEvent resizeCancelEvent) {
//				getLogger().debug("ResizeCancelEvent");
//			}
//		};
//		gridWrapper.addResizeListener(listener);
//		//formWrapper.addResizeListener(listener);
//
//		HorizontalLayout layout = new HorizontalLayout(gridWrapper, formWrapper);
//		layout.setMargin(false);
//		layout.setSpacing(false);
//		layout.setWidthUndefined();
//		layout.setHeightUndefined();
		//addComponentAndRatio(layout, 1);
	}
	private void openReport(Report report) {

		String reportName = report.name;
		switch (reportName){
			case "Отчет по работам":
				UI.getCurrent().addWindow(new WorksReportWindow("Отчет по работам"));
				break;
			case "Отчет по заявкам":
				UI.getCurrent().addWindow(new RequestsReportWindow("Отчет по заявкам"));
				break;
			case "Отчет по бригадам":

//				UI.getCurrent().addWindow(new BrigadesReportWindow("Отчет по бригадам"));
				break;
			case "Отчет по сотрудникам":
//				UI.getCurrent().addWindow(new EmployeesReportWindow("Отчет по сотрудникам"));
				break;
			case "Отчет по событиям":
				UI.getCurrent().addWindow(new EventsReportWindow("Отчёт по событиям"));
				break;
			case "Сводка по заявкам":
				UI.getCurrent().addWindow(new RequestSummaryReportWindow("Сводка по заявкам"));
				break;
			case "Стандартный отчёт по заявкам":
				UI.getCurrent().addWindow(new RVSouthWestReportWindow("Стандартный отчёт по заявкам"));
				break;
			case "Аналитический отчёт по типам работ":
				new AnalyticTermReportWindow();
				break;
			case "Отчет об интенсивности инф. взаимодействия в рамках реагирования на заявки ГЛ":
				UI.getCurrent().addWindow(new InteractionIntensityReportWindow());
				break;
			case "Отчет о действиях пользователей в системе":
				UI.getCurrent().addWindow(new UserActionInSystemReportWindow());
				break;
			/*case "Отчет о работе пользователя системы":
				UI.getCurrent().addWindow(new UserWorkReportWindow());
				break;*/
			case "Отчет по фотографиям":
				new PhotoReportWindow();
				break;

			case "Протоколирование работы пользователей в АИС БРИГАДЫ":
				new UserWorksReportWindow();
			break;
			default:
				break;
		}



	}


	private List<Report> generateReports(){
		List<Report> reports = new ArrayList<>();
		AtomicLong counter = new AtomicLong(1);
		reports.add(new Report(counter.getAndIncrement(),"Отчет по работам", "Составляет отчет по работам"));
		reports.add(new Report(counter.getAndIncrement(),"Отчет по заявкам", "Составляет отчет по заявкам" ));
//		reports.add(new Report(counter.getAndIncrement(),"Отчет по событиям", "Составляет отчет по событиям"));
//		reports.add(new Report(3L,"Отчет по бригадам", "Составляет отчет по бригадам"));
//		reports.add(new Report(4L,"Отчет по сотрудникам", "Составляет отчет по сотрудникам"));
		reports.add(new Report(counter.getAndIncrement(),"Сводка по заявкам", "Выводит сводку по заявкам на текущий момент"));
		reports.add(new Report(counter.getAndIncrement(),"Стандартный отчёт по заявкам", "Выводит стандартный отчет по заявкам с 7:00 по текущее время"));
		reports.add(new Report(counter.getAndIncrement(),"Аналитический отчёт по типам работ", "Соответствие фактических сроков выполнения работ разного типа и их деталей (этапов) нормативным срокам"));
//		reports.add(new Report(counter.getAndIncrement(),"Отчет об интенсивности инф. взаимодействия в рамках реагирования на заявки ГЛ", "Составляет отчет об интенсивности инф. взаимодействия в рамках реагирования на заявки ГЛ"));
//		reports.add(new Report(counter.getAndIncrement(),"Отчет о действиях пользователей в системе", "Составляет отчет о действиях пользователей в системе"));
//		reports.add(new Report(counter.getAndIncrement(),"Отчет о работе пользователя системы", "Отчет о работе пользователя системы"));
		reports.add(new Report(counter.getAndIncrement(),"Отчет по фотографиям", "Отчет по фотографиям"));
		reports.add(new Report(counter.getAndIncrement(),"Протоколирование работы пользователей в АИС БРИГАДЫ", "Протоколирование работы пользователей в АИС БРИГАДЫ"));
		return reports;
	}

	//при переносе в основной проект вынести этот класс в отдельный файл, либо заменить на соответствующий существующий
	public class Report{
		private Long number;
		private String name;
		private String description;

		public Report(Long number, String name, String description) {
			this.number = number;
			this.name = name;
			this.description = description;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			Report report = (Report) o;

			if (name != null ? !name.equals(report.name) : report.name != null) return false;
			return description != null ? description.equals(report.description) : report.description == null;
		}

		@Override
		public int hashCode() {
			int result = name != null ? name.hashCode() : 0;
			result = 31 * result + (description != null ? description.hashCode() : 0);
			return result;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public Long getNumber() {
			return number;
		}

		public void setNumber(Long number) {
			this.number = number;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}
}

