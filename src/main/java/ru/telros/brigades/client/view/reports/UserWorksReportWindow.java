package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.dto.UserWorksReportDto;
import ru.telros.brigades.client.view.reports.util.GridWithButtonLayout;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class UserWorksReportWindow extends Window {
    VerticalLayout mainLayout;
    HorizontalLayout parametersLayout;
    UserWorksReportGrid resultGrid;
    ReportDateTimeLayout timeLayout;
    Grid<UserDto> userSelectionGrid;
    GridWithButtonLayout userSelectionLayout;
    Button searchButton;

    public UserWorksReportWindow() {
        resultGrid = new UserWorksReportGrid();

        timeLayout = new ReportDateTimeLayout();

        userSelectionGrid = new Grid<>();
        userSelectionGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        userSelectionGrid.addColumn(e -> e.getFullName());
        userSelectionGrid.setItems(BrigadesUI.getRestTemplate().getUsers());
        userSelectionLayout = new GridWithButtonLayout(userSelectionGrid, new Button("Пользователь"));

        searchButton = new Button("Поиск");
        searchButton.addClickListener(clickEvent -> {
            List<UserWorksReportDto> resultDtos = new ArrayList<>();
            if (timeLayout.isValid() && userSelectionGrid.getSelectedItems().size() != 0) {
                userSelectionGrid.getSelectedItems().stream().forEach(e -> {
                    resultDtos.addAll(BrigadesUI.getRestTemplate().getUserWorksReport(
                            timeLayout.getStart(), timeLayout.getEnd(), e
                    ));
                });
                resultGrid.setItems(resultDtos);
            }
        });


        parametersLayout = new HorizontalLayout(timeLayout, userSelectionLayout, searchButton);
        parametersLayout.setComponentAlignment(timeLayout, Alignment.MIDDLE_CENTER);
        parametersLayout.setComponentAlignment(userSelectionLayout, Alignment.MIDDLE_CENTER);
        parametersLayout.setComponentAlignment(searchButton, Alignment.MIDDLE_CENTER);
        mainLayout = new VerticalLayout(parametersLayout, resultGrid);
        mainLayout.setHeight("100%");
        resultGrid.setHeight("100%");
        parametersLayout.setHeight("100%");
        mainLayout.setExpandRatio(parametersLayout, 15);
        mainLayout.setExpandRatio(resultGrid, 85);
        setContent(mainLayout);
        setSizeFull();
        getUI().getCurrent().addWindow(this);
    }

    private class UserWorksReportGrid extends Grid<UserWorksReportDto> {
        public UserWorksReportGrid() {
//            this.addColumn(e->e.getRecordNumber()).setCaption("№");
            this.addColumn(e -> e.getUser()).setCaption("ФИО");
            this.addColumn(e -> e.getRole()).setCaption("Роль");
//            this.addColumn(e -> "").setCaption("Иконка???");
            this.addColumn(e -> new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(e.getCreateDate())).setCaption("Дата создания");
            this.addColumn(e -> e.getType()).setCaption("Тип события");
            this.addColumn(e -> e.getMessage()).setCaption("Событие");

            this.setWidth("100%");
        }
    }
}