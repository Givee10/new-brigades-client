package ru.telros.brigades.client.view;

import com.vaadin.annotations.StyleSheet;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.EventDto;
import ru.telros.brigades.client.dto.RoleDto;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.HeaderLayout;
import ru.telros.brigades.client.ui.component.Title;
import ru.telros.brigades.client.ui.grid.BrigadesGrid;
import ru.telros.brigades.client.ui.grid.GridColumn;
import ru.telros.brigades.client.ui.window.YesNoWindow;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class AdminView extends HeaderLayout implements View, HasLogger {
    /*Роли в системе:
	Мастер бригады
	Старший мастер РВ
	Руководитель РВ
	Диспетчер СОУ
	Руководитель СОУ
	Администратор

     */
    public static final String NAME = "admin";
    //todo-k Заменить текст
    public static final String MENU_CAPTION = "view.setupview.menuitem";
    public static final String CAPTION = "Панель администратора";
    AdminViewGrid usersGrid;
    List<UserDto> users;
    Map<Long, Set<RoleDto>> usersIdToRoles;
    public AdminView() {
        BrigadesEventBus.register(this);
        Responsive.makeResponsive(this);
        addToHeader(new Title("Панель администратора"));
        users = BrigadesUI.getRestTemplate().getUsers();
        usersIdToRoles = initUsersToRoles();
        usersGrid = new AdminViewGrid(getGridColumns(),users);
        usersIdToRoles.entrySet().stream().forEach(userDtoSetEntry -> {
            getLogger().debug(userDtoSetEntry.getKey().toString());
            getLogger().debug(userDtoSetEntry.getValue().stream().map(roleDto -> roleDto.getDescription()+" ").collect(Collectors.joining()));
        });
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(usersGrid);
        Button addUserButton = new Button("Добавить Пользователя");
        addUserButton.addClickListener(clickEvent -> {
            getUI().addWindow(new EditWindow(new UserDto()));
        });
        layout.addComponent(addUserButton);
        layout.setExpandRatio(usersGrid,1);
        usersGrid.setWidth("100%");
        usersGrid.setHeight("100%");
        layout.setHeight("100%");
        addComponentAndRatio(layout,1);
    }
    private List<GridColumn<UserDto>> getGridColumns(){
        GridColumn<UserDto> LOGIN = GridColumn.createColumn("login","Логин", e->e.getLogin());
        GridColumn<UserDto> MAIL = GridColumn.createColumn("mail","Почта", e->e.getEmail());
        GridColumn<UserDto> LASTNAME = GridColumn.createColumn("lastname","Фамилия", e->e.getLastName());
        GridColumn<UserDto> FIRSTNAME = GridColumn.createColumn("firstname","Имя", e->e.getFirstName());
        GridColumn<UserDto> MIDDLENAME = GridColumn.createColumn("middlename","Отчество", e->e.getMiddleName());
        return Arrays.asList(LOGIN, MAIL, LASTNAME, FIRSTNAME, MIDDLENAME);

    }
    private class EditWindow extends Window{
        Button save;
        Button cancel;
        TextField loginField;
        TextField passwordField;
        TextField mailField;
        TextField lastnameField;
        TextField firstnameField;
        TextField middlenameField;
        VerticalLayout mainLayout;
        HorizontalLayout buttonsLayout;
        //todo-k блокирование пользователей, удаление ролей
        public EditWindow(UserDto userDto){
            loginField = new TextField("Логин");
            passwordField = new TextField("Пароль");
            mailField = new TextField("Почта");
            lastnameField = new TextField("Фамилия");
            firstnameField = new TextField("Имя");
            middlenameField = new TextField("Отчество");
            if (userDto.getId()!=null){
                loginField.setEnabled(false);
                passwordField.setPlaceholder("Изменить для редактирования");
                loginField.setValue(userDto.getLogin());
                mailField.setValue(userDto.getEmail());
                lastnameField.setValue(userDto.getLastName());
                firstnameField.setValue(userDto.getFirstName());
                middlenameField.setValue(userDto.getMiddleName());
            }
            save = new Button("Сохранить");
            save.addClickListener(clickEvent -> {

                userDto.setLogin(loginField.getValue());
                userDto.setEmail(mailField.getValue());
                userDto.setFirstName(firstnameField.getValue());
                userDto.setLastName(lastnameField.getValue());
                userDto.setMiddleName(middlenameField.getValue());


                if (userDto.getId()!=null){
                    if (passwordField.getValue()!=null && !passwordField.getValue().equals("")){
                        userDto.setPassword(passwordField.getValue());
                    }
                    BrigadesUI.getRestTemplate().updateUser(userDto);
                    AdminView.this.usersGrid.refreshItem(userDto.getId());
                }else {
                    userDto.setPassword(passwordField.getValue());
                    userDto.setId(BrigadesUI.getRestTemplate().addUser(userDto).getId());
                    List<UserDto> buf = AdminView.this.usersGrid.getItems();
                    buf.add(userDto);
                    AdminView.this.usersGrid.setItems(buf);
                    AdminView.this.usersGrid.refreshAll();
                }
                this.close();
            });
            cancel = new Button("Отмена");
            cancel.addClickListener(clickEvent -> this.close());
            mainLayout = new VerticalLayout();
            buttonsLayout = new HorizontalLayout();
            buttonsLayout.addComponents(save,cancel);
            mainLayout.addComponents(loginField,passwordField,mailField,lastnameField,firstnameField,middlenameField,buttonsLayout);
            buttonsLayout.setWidth("100%");
            setContent(mainLayout);
            center();
        }
    }
    @StyleSheet("vaadin://UserStyles.css")
    private class AdminViewGrid extends BrigadesGrid<UserDto>{

        public AdminViewGrid(List<GridColumn<UserDto>> gridColumns, List<UserDto> items) {
            super(gridColumns, items);
            List<RoleDto> roles = BrigadesUI.getRestTemplate().getRoles();

            roles.stream().forEach(e -> initCheckBox(e, this));
            addComponentColumn(userDto -> {
                CheckBox checkBox = new CheckBox();

                checkBox.setValue(userDto.isBlocked());
                checkBox.addValueChangeListener(valueChangeEvent -> {
                    userDto.setBlocked(valueChangeEvent.getValue());
                    BrigadesUI.getRestTemplate().updateUser(userDto);
                });
                return checkBox;
            }).setCaption("Заблокирован");
            addComponentColumn(e -> {
                Button button = new Button("Удалить");
                button.addClickListener(clickEvent -> {
                    YesNoWindow confirmationWindow =
                            YesNoWindow.open("Внимание", "Вы действительно хотите удалить пользователя " + e.getLogin() + "?", false);
                    confirmationWindow.addCloseListener(event -> {
                        if (((YesNoWindow) event.getWindow()).isModalResult()) {
                            BrigadesUI.getRestTemplate().deleteUser(e.getId());
                            List<UserDto> buf = AdminView.this.usersGrid.getItems();
                            buf.remove(e);
                            AdminView.this.usersGrid.setItems(buf);
                            AdminView.this.usersGrid.refreshAll();
                        }
                    });
                });
                return button;
            });

            this.setItems(users);

            this.setStyleGenerator(userDto -> {
                EventDto curEvent;
                    curEvent = BrigadesUI.getRestTemplate().getEvents(ZonedDateTime.now().minusDays(1),ZonedDateTime.now()).stream().
                            filter(eventDto -> eventDto!=null && eventDto.getIdUser()!=null &&
                                    (eventDto.getIdUser().equals(userDto.getId())))
                            .sorted((o1, o2) -> o2.getDate().compareTo(o1.getDate())).findFirst().orElse(null);

                if (curEvent != null && !curEvent.getType().equals("Выход пользователя из системы")) {
                    return "authorized";
                } else {
                    return null;
                }

            });
        }


        @Override
        protected void onDoubleClick(UserDto entity, Column column) {
            UI.getCurrent().addWindow(new EditWindow(entity));
        }

        @Override
        protected void onClick(UserDto entity, Column column) {

        }
    }

    private Map<Long, Set<RoleDto>> initUsersToRoles(){
        Map<Long, Set<RoleDto>> map = new HashMap<>();
        for (int i = 0; i < users.size(); i++) {
            Set<RoleDto> roles =  new HashSet<>(BrigadesUI.getRestTemplate().getUserRoles(users.get(i).getId()));
            map.put(users.get(i).getId(),roles);
        }
        return map;
    }
    private void initCheckBox(RoleDto role, Grid<UserDto> grid){
        grid.addComponentColumn(s -> {
            CheckBox checkBox = new CheckBox();
            if (usersIdToRoles.get(s.getId())==null){
                usersIdToRoles.put(s.getId(), new HashSet<>());
            }
            checkBox.setValue(usersIdToRoles.get(s.getId()).contains(role));
            checkBox.addValueChangeListener(valueChangeEvent -> {
                if (valueChangeEvent.getValue()){
                    usersIdToRoles.get(s.getId()).add(role);
                    BrigadesUI.getRestTemplate().addUserRole(s.getId(),role);
                }else {
                    usersIdToRoles.get(s.getId()).remove(role);
                }
            });
            return checkBox;
        }).setCaption(role.getDescription());

    }



}
