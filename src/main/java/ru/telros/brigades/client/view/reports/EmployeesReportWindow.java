//package ru.telros.brigades.client.view.reports;
//
//import com.vaadin.ui.Button;
//import com.vaadin.ui.ComboBox;
//import com.vaadin.ui.Notification;
//import com.vaadin.ui.VerticalLayout;
//import ru.telros.brigades.client.BrigadesUI;
//import ru.telros.brigades.client.dto.BrigadeDto;
//import ru.telros.brigades.client.dto.EmployeeBrigadeDto;
//import ru.telros.brigades.client.ui.person.PersonColumn;
//import ru.telros.brigades.client.ui.person.PersonGrid;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.NoSuchElementException;
//
//public class EmployeesReportWindow extends DynamicReportWindow<PersonGrid, EmployeeBrigadeDto> {
//    ComboBox<BrigadeDto> brigadeSelection;
//    public EmployeesReportWindow(String title){
//        super(title, new PersonGrid(PersonColumn.getMainColumns(), new ArrayList<>()), PersonColumn.getMainColumns());
//        brigadeSelection = new ComboBox("Бригада");
//        brigadeSelection.setItemCaptionGenerator(BrigadeDto::getName);
//        brigadeSelection.setItems(BrigadesUI.getRestTemplate().getBrigades());
//        this.timeGap.removeAllComponents();
//        VerticalLayout params = (VerticalLayout) this.parameters.getComponent(1);
//        params.addComponent(brigadeSelection,2);
//    }
//    @Override
//    public List<EmployeeBrigadeDto> getItems() {
//
//        return BrigadesUI.getRestTemplate().getBrigadeEmployees(brigadeSelection.getValue().getId());
//    }
//
//    @Override
//    public void customUpdateButtonHandle(String reportType) {
//
//    }
//
//    @Override
//    public Button initSaveReportButton() {
//        Button button = new Button("Сохранить");
//        button.addClickListener(clickEvent ->{
//            String reportType = reportTypes.getSelectedItem().get();
//            BrigadeDto brigade = brigadeSelection.getSelectedItem().get();
//            switch (reportType) {
//                case "Динамический":
//                    saveReport("Отчет по бригадам\r\n"+
//                            "Бригада: "+ brigade.getName()+"\r\n");
//
//                    break;
//                default:
//
//                    break;
//            }
//        });
//        return button;
//    }
//
//    @Override
//    public void displayCustom(String name) {
//
//    }
//
//    @Override
//    public void displayDynamic() {
//
//    }
//    public void updateGrid(){
//        try {
//            String reportType = reportTypes.getSelectedItem().get();
//            switch (reportType) {
//                case "Динамический":
//                    List<EmployeeBrigadeDto> items = getItems();
//                    grid.setItems(items);
//                    break;
//                default:
//                    customUpdateButtonHandle(reportType);
//                    break;
//            }
//        }catch (NoSuchElementException e){
//            Notification.show("Не задан вид отчета", Notification.Type.ERROR_MESSAGE);
//        }
//
//    }
//}
