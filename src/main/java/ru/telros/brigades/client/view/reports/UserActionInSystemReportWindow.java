package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.dto.report.UserActionsInSystemDto;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

public class UserActionInSystemReportWindow extends Window {
    HorizontalLayout parametersLayout;
    ReportDateTimeLayout dateTimeLayout;
    ComboBox<UserDto> userIdTextField;
    Grid<UserActionsInSystemDto> grid;
    Button button;
    public UserActionInSystemReportWindow(){
        setCaption("Отчет о действиях пользователей в системе");
        userIdTextField = new ComboBox<>();
        userIdTextField.setEmptySelectionAllowed(true);
        userIdTextField.setItems(BrigadesUI.getRestTemplate().getUsers());
        userIdTextField.setItemCaptionGenerator(e->e.getFullName());
        userIdTextField.setPopupWidth("300%");
        userIdTextField.setCaption("Пользователь");
        dateTimeLayout = new ReportDateTimeLayout();
        button = new Button("Обновить");
        button.addClickListener(e->{
           UserDto user = userIdTextField.getSelectedItem().get();
            if (user!=null){
                grid.setItems();
            }else {
                grid.setItems();
            }
        });
        parametersLayout = new HorizontalLayout();
        parametersLayout.addComponents(dateTimeLayout,userIdTextField,button);
        grid = initGrid();
        VerticalLayout mainLayout = new VerticalLayout(parametersLayout,grid);
        dateTimeLayout.setHeight("100px");
        grid.setWidth("100%");
        grid.setHeight("100%");
        setContent(mainLayout);
        setSizeFull();
    }
    private Grid<UserActionsInSystemDto> initGrid() {
        Grid<UserActionsInSystemDto> grid = new Grid<>();
        grid.addColumn(e -> e.getName()).setCaption("ФИО");
        grid.addColumn(e -> e.getEvent().getType()).setCaption("Тип события");
        grid.addColumn(e -> e.getEvent().getMessage()).setCaption("Событие");
        grid.addColumn(e -> e.getEvent().getDate()).setCaption("Время события");
        return grid;
    }
}
