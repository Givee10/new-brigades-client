package ru.telros.brigades.client.view;

import com.thoughtworks.xstream.XStream;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.CustomDateTimeField;
import ru.telros.brigades.client.ui.component.DisabledTextField;
import ru.telros.brigades.client.ui.component.HeaderLayout;
import ru.telros.brigades.client.ui.component.Title;
import ru.telros.brigades.client.ui.setup.*;
import ru.telros.brigades.client.ui.utils.CompsUtil;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.StringUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class SetupView extends HeaderLayout implements View, HasLogger {
	public static final String NAME = "setup";
	public static final String MENU_CAPTION = "view.setupview.menuitem";
	public static final String CAPTION = "Настройки";
	ControlAccuracyGrid controlAccuracyGrid;
	ReactionTimeGrid reactionTimeGrid;
	UserActionGrid userActionGrid;
	WorkCodeGrid workCodeGrid;
	WorkStagesGrid workStagesGrid;
	BrigadeWorkPriorityGrid brigadeWorkPriorityGrid;
	public SetupView() {
		/*controlAccuracyGrid = new ControlAccuracyGrid(BrigadesUI.getRestTemplate().getSlaControlAccuracy());
		reactionTimeGrid = new ReactionTimeGrid(BrigadesUI.getRestTemplate().getSlaReactionTime());
		userActionGrid = new UserActionGrid(BrigadesUI.getRestTemplate().getSlaUserActions());
		workCodeGrid = new WorkCodeGrid(BrigadesUI.getRestTemplate().getSlaWorkCodes());*/

		initGrids();
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		addToHeader(new Title(CAPTION));

		//getLogger().debug(BrigadesUI.getRestTemplate().ping().debugInfo());

		VerticalLayout layout = CompsUtil.getVerticalWrapperNoMargin();

		CustomDateTimeField dateTimeField = new CustomDateTimeField("Дата, время начала ведения журналов");
		dateTimeField.setValue(DatesUtil.zonedToLocal(DatesUtil.setNullDateTime()));
		DisabledTextField soundField = new DisabledTextField("Длительность звукового сигнала");
		soundField.setValue("3");
		layout.addComponent(CompsUtil.getStatisticWrapper(dateTimeField));
		layout.addComponent(CompsUtil.getStatisticWrapper(soundField));

		TabSheet tabSheet = new TabSheet();
		tabSheet.addTab(CompsUtil.getVerticalWrapperNoMargin(controlAccuracyGrid), "Точность контроля");
//		tabSheet.addTab(CompsUtil.getVerticalWrapperNoMargin(userActionGrid), "Контроль действий пользователей");
		tabSheet.addTab(CompsUtil.getVerticalWrapperNoMargin(reactionTimeGrid), "Сроки закрытия заявок в СОУ");
//		tabSheet.addTab(CompsUtil.getVerticalWrapperNoMargin(workCodeGrid), "Перечень работ по кодам заявок ГЛ");
		//перенес все данные, которые должны быть в это вкладке в workStagesGrid
//		tabSheet.addTab(CompsUtil.buildEmptyTab(), "Контрольные сроки выполнения работ");
		workStagesGrid = new WorkStagesGrid();
		tabSheet.addTab(CompsUtil.getVerticalWrapperNoMargin(workStagesGrid), "Перечень работ и их этапов");
		brigadeWorkPriorityGrid = new BrigadeWorkPriorityGrid(itemsFromCSV());
		tabSheet.addTab(CompsUtil.getVerticalWrapperNoMargin(brigadeWorkPriorityGrid), "Приоритет выполнения работ");
		tabSheet.setSizeFull();
		layout.addComponent(tabSheet);
		layout.setExpandRatio(tabSheet, 1);

		addComponentAndRatio(layout, 1);
	}
	public void initGrids(){
		Map<String,Class> filesToInit = new HashMap<>();
		filesToInit.put("files/settings/"+"controlAccuracyGrid.xml", ControlAccuracyGrid.class);
		filesToInit.put("files/settings/"+"userActionGrid.xml",UserActionGrid.class);
		filesToInit.put("files/settings/"+"reactionTimeGrid.xml", ReactionTimeGrid.class);
		filesToInit.put("files/settings/"+"workCodeGrid.xml",WorkCodeGrid.class);

		filesToInit.entrySet().stream().forEach(e->
				//TODO-k Для кабинета администратора сохранять в бд при каждом изменении
				// Переделать метод сохранения в файл
//				initGrid(e.getKey(), new File(e.getKey()).exists(), e.getValue())
				initGrid(e.getKey(), false, e.getValue())
		);


	}
	private void initGrid(String settingsFile, boolean loaded, Class cl){
		List settingsList;
		if (cl==ControlAccuracyGrid.class){
			if (loaded){
				controlAccuracyGrid = new ControlAccuracyGrid(loadSettingsFromFile(settingsFile));
			}else{
				settingsList =  BrigadesUI.getRestTemplate().getSlaControlAccuracy();
				controlAccuracyGrid =new ControlAccuracyGrid(settingsList);
				saveSettingsToFile(settingsFile, settingsList);
			}
		}else if(cl==ReactionTimeGrid.class){
			if (loaded){
				reactionTimeGrid = new ReactionTimeGrid(/*loadSettingsFromFile(settingsFile)*/);
			}else{
				settingsList =  BrigadesUI.getRestTemplate().getSlaWorkCodes();
				reactionTimeGrid = new ReactionTimeGrid(/*settingsList*/);
//				saveSettingsToFile(settingsFile, settingsList);
			}
		}else if(cl==UserActionGrid.class){
			if (loaded){
				userActionGrid = new UserActionGrid(loadSettingsFromFile(settingsFile));
			}else{
				settingsList = BrigadesUI.getRestTemplate().getSlaUserActions();
				userActionGrid = new UserActionGrid(settingsList);
				saveSettingsToFile(settingsFile, settingsList);
			}
		}else if (cl==WorkCodeGrid.class){
			if (loaded){
				workCodeGrid = new WorkCodeGrid(loadSettingsFromFile(settingsFile));
			}else{
				settingsList = BrigadesUI.getRestTemplate().getSlaWorkCodes();
				workCodeGrid = new WorkCodeGrid(settingsList);
				saveSettingsToFile(settingsFile, settingsList);
			}
		}
	}
	private void saveSettingsToFile(String filePath, List settingsList){
		/*XStream xstream = new XStream();
		xstream.setMode(XStream.XPATH_ABSOLUTE_REFERENCES);

		File file = null;
		try {
			file = new File(filePath);
			file.createNewFile();
			FileUtils.write(file, xstream.toXML(settingsList));
		} catch (IOException e) {
			e.printStackTrace();
		} */
	}
	private List loadSettingsFromFile(String settingsFile){
		XStream xstream = new XStream();
		getLogger().debug(settingsFile);
		try {
			switch (settingsFile) {
				case "files/settings/" + "controlAccuracyGrid.xml":
					return (List<SlaControlAccuracyDto>) xstream.fromXML(new BufferedReader(new FileReader(settingsFile)));
				case "files/settings/" + "userActionGrid.xml":
					return (List<SlaUserActionDto>) xstream.fromXML(new BufferedReader(new FileReader(settingsFile)));
				case "files/settings/" + "reactionTimeGrid.xml":
					return (List<SlaReactionTimeDto>) xstream.fromXML(new BufferedReader(new FileReader(settingsFile)));
				case "files/settings/" + "workCodeGrid.xml":
					return (List<SlaWorkCodeDto>) xstream.fromXML(new BufferedReader(new FileReader(settingsFile)));
			}
		}catch (FileNotFoundException ex){
			ex.printStackTrace();
		}

		return null;
	}
	private List<BrigadeWorkPriorityDto> itemsFromCSV(){
		String csv =
				"1;1.1;Наружный осмотр сети;1;1;1;1;1\n" +
				"2;1.2;Проверка на утечку отдельного участка трубопровода;10;8;5;2;5\n" +
				"3;1.3;Диагностика дюкера на утечки;9;9;0;0;0\n" +
				"4;1.4;Внутренний осмотр камер ;9;10;5;0;1\n" +
				"5;1.5;Техническое обслуживание арматуры;9;10;5;1;5\n" +
				"6;1.5.1;Подготовка к зиме задвижек котельных;4;10;1;5;5\n" +
				"7;1.5.2;Подготовка к зиме задвижек Д более 500 мм;5;10;1;5;5\n" +
				"8;1.6;Обследование водоспусков;7;7;7;7;7\n" +
				"9;1.7;Техническое обслуживание ПГ;5;5;10;3;3\n" +
				"10;1.8;Техническое обслуживание вантуза;8;8;8;3;3\n" +
				"11;1.9;Техническое обслуживание водоразборной колонки;5;10;2;3;3\n" +
				"12;1.10;Обслуживание ПОВ;5;5;10;2;2\n" +
				"13;1.11;Отбор проб воды на сетях;10;10;5;10;5\n" +
				"14;2;Текущий ремонт трубопровода;10;10;5;0;10\n" +
				"15;2.1;Установка и замена координатной таблички ;10;10;10;10;10\n" +
				"16;2.2;Обновление координатной таблички;10;10;10;10;10\n" +
				"17;2.3;Промывка тупиковых участков трубопровода ;10;5;5;5;5\n" +
				"18;2.4.А;Отключение/включение участка водопроводной сети для локализации вытекания (характер повреждения не определен). при повреждении на арматуре;10;10;10;5;10\n" +
				"19;2.8.1;Замена крышки люка;10;10;10;10;10\n" +
				"20;2.8.2;Замена люка (комплекта) колодца;10;10;10;10;10\n" +
				"21;2.8.3;Замена регулировочных железобетонных колец колодцев;10;10;10;10;10\n" +
				"22;2.8.4;Замена ковера;10;10;10;10;10\n" +
				"23;2.9;Устранение провалов в асфальте;10;10;8;8;10\n" +
				"24;2.10;Текущий ремонт камер;8;10;5;2;10\n" +
				"25;2.11;Текущий ремонт колодезной арматуры;10;10;10;2;10\n" +
				"26;2.14;Текущий ремонт бесколодезной арматуры;10;10;7;2;10\n" +
				"27;2.15;Текущий ремонт ПГ;10;10;10;4;8\n" +
				"28;2.16;Текущий ремонт воздушника;10;10;5;4;10\n" +
				"29;2.17;Текущий ремонт вантуза;10;10;5;4;10\n" +
				"30;2.18;Текущий ремонт водоразборной колонки;10;10;5;5;10\n" +
				"31;2.19.1;Ремонт трубопровода с заменой участка сети траншейным способом;8;10;4;4;10\n" +
				"32;2.19.2;Ремонт трубопровода с заменой участка сети бестраншейным способом;8;10;3;3;10\n" +
				"33;2.20;Ликвидация прочих повреждений на водопроводной сети;10;10;3;3;10\n" +
				"34;3.1;Капитальный ремонт колодцев;10;10;4;3;10\n" +
				"35;3.2;Капитальный ремонт камер;10;10;3;3;10\n" +
				"36;3.3.1;Замена колодезной арматуры на колодезную арматуру;10;10;10;0;10\n" +
				"37;3.3.2;Замена колодезной арматуры на бесколодезную арматуру;10;10;5;0;10\n" +
				"38;3.3.3;Замена бесколодезной арматуры на бесколодезную арматуру;10;10;4;0;10\n" +
				"39;3.3.4;Отключение/включение участка водопроводной сети перекрытием задвижек для замены арматуры по лизингу, неосновной деятельности. при работах по Регламентам (регулировка зон, открытие водоводов и т.д.);10;10;3;0;5\n" +
				"40;3.3.5;Отключение/включение участка водопроводной сети одной задвижкой (замена приборов учета, по письму, ограничение водоснабжения и т.д.);10;10;5;0;5\n" +
				"41;3.4.1;Замена колодезного ПГ на колодезный;10;10;10;0;10\n" +
				"42;3.4.2;Замена колодезного ПГ на бесколодезный;10;10;8;0;10\n" +
				"43;3.4.3;Замена бесколодезного ПГ на бесколодезный;10;10;5;0;10\n" +
				"44;3.5;Капитальный ремонт вантуза;8;10;4;0;10\n" +
				"45;3.6.1;Капитальный ремонт ВРК: старого образца;10;10;3;0;10\n" +
				"46;3.6.2;Капитальный ремонт ВРК: без присоединения абонентов;10;10;3;0;10\n" +
				"47;3.6.3;Капитальный ремонт ВРК: с присоединением абонентов;10;10;3;0;10\n" +
				"48;3.7.1;Замена колодезного воздушника на колодезный;10;10;5;0;10\n" +
				"49;3.7.2;Замена колодезного воздушника на бесколодезный;10;10;5;0;10\n" +
				"50;3.7.3;Замена бесколодезного воздушника на бесколодезный;9;10;5;0;8\n" +
				"51;4.1;Промывка для достижения качества;10;9;5;4;7\n" +
				"52;4.2;Обеспечение работ по диагностике сетей;10;9;5;4;8\n" +
				"53;4.3;Очистка крышек колодцев от снега (льда, земли);10;10;10;10;10\n" +
				"54;4.4;Сколка наледей;10;10;10;10;10\n" +
				"55;4.5;Отогрев домового ввода с разборкой водомерного узла и отключением домового ввода;10;9;5;4;7\n" +
				"56;4.6;Отогрев сооружений (ПГ, водоразборной колонки, арматуры);10;10;10;7;8\n" +
				"57;4.7;Дополнительный осмотр ПГ (по приказам, на наличие воды и т.д.);9;8;10;5;5\n" +
				"58;4.8;Проверка ПГ, подверженных затоплению (спецучет);8;9;10;7;6\n" +
				"59;4.9;Обследование домового ввода по неподаче воды;10;9;5;5;6\n" +
				"60;4.10;Обследование водоснабжения объектов;9;10;7;6;8\n" +
				"61;4.11;Обследование заявки;10;10;7;9;6\n" +
				"62;4.12;Прочистка/проверка сетки водомерного узла;10;10;5;5;5\n" +
				"63;4.13;Прочистка домовых вводов;8;10;4;4;4\n" +
				"64;4.14;Заглушка домовых вводов;9;10;4;1;10\n" +
				"65;4.15;Замена водосчетчика;9;10;5;2;7\n" +
				"66;4.16;Ремонт водомерного узла;10;10;3;5;7\n" +
				"67;4.17;Ликвидация повреждения на стояке домового ввода (в подвале);9;10;3;3;8\n" +
				"68;4.18;Установка/снятие регистратора давления;10;9;4;5;6\n" +
				"69;4.19;Ликвидация просадки шурфов;10;10;5;7;9\n" +
				"70;4.20;Санитарная обработка сооружений;6;10;5;5;9\n" +
				"71;4.21;Выезд и работа на пожаре;10;9;10;5;5\n" +
				"72;4.22;Зачистка адреса;9;10;5;5;10\n" +
				"73;4.23;Комиссии по инженерным коммуникациям;10;10;7;9;8\n" +
				"74;4.24;Составление актов, схем, выдача предписаний (бригадиры);10;10;10;9;10\n" +
				"75;4.25;Хозработы. получение материалов;10;10;10;10;10\n" +
				"76;4.26;Ремонт первой задвижки на водомерном узле;10;10;7;8;6\n" +
				"77;4.27;Замена задвижки на водомерном узле;10;10;7;7;8\n" +
				"78;4.28;Замеры давления на сетях;10;10;7;8;5\n" +
				"79;4.29;Обследование частного сектора (в т.ч. для заключения договоров);8;10;7;6;5\n" +
				"80;4.30;Сдача/приемка колодцев при асфальтировке;9;10;7;8;6\n" +
				"81;4.31;Установка и заправка бочки, туалетов. подключение ПГСК;10;9;7;6;5\n" +
				"82;4.32;Обследование колодцев (без привязки к видам работ, в т.ч. поиск, очистка);10;10;8;7;6\n" +
				"83;4.33;Работы при замене редукторов запорной арматуры;9;10;8;0;10\n" +
				"84;4.34;Устройство временного водопровода;10;10;8;9;10\n" +
				"85;4.35;Регулировка крышки колодца;10;10;10;10;10\n" +
				"86;4.36;Ликвидация прочих просадок (газон, асфальт без колодца);10;10;9;9;10\n" +
				"87;4.37;Контроль производства работ подрядчиков, ПУАР;10;10;7;9;6\n" +
				"88;4.38;Установка ограждений(только Прочие работы);10;10;10;10;10\n" +
				"89;4.39;Снятие ограждений (только Прочие работы);10;10;10;10;10\n" +
				"90;4.40;Подготовительно/заключительные работы обходчика;10;10;5;5;5\n" +
				"91;4.41;Инструктаж;10;10;10;10;10\n" +
				"92;4.42;Погрузо-разгрузочные работы;10;10;10;10;10\n" +
				"93;4.43;Переезды, в т.ч. обходчика;10;10;10;10;10\n" +
				"94;4.44;Прочие работы;10;10;10;10;10\n" +
				"95;4.45;Подготовка основания шурфа под асфальтировку (выборка щебня, трамбовка);9;10;5;5;10\n" +
				"96;4.46;Разборка асфальтобетонных покрытий;10;10;5;5;10\n" +
				"97;4.47;Опиловка краев шурфа;9;10;5;5;10\n" +
				"98;4.48;Укладка асфальтобетонной смеси ;9;10;5;5;10\n" +
				"99;4.49;Подготовка основания под восстановление шурфа плодородным (растительным)  грунтом;9;10;5;5;10\n" +
				"100;4.50;Засыпка шурфа плодородным (растительным) грунтом;9;10;5;5;10\n" +
				"101;4.51;Посев семян трав;9;10;5;5;10\n" +
				"102;4.52;Резка труб (сталь, чугун, ПНД);10;10;9;9;10\n" +
				"103;4.53;Откачка воды из колодца/камеры, котлована;10;10;10;5;10\n" +
				"104;4.54;Сварка труб ПНД;8;10;5;5;10\n" +
				"105;4.55;Электрогазосварка;0;0;0;0;0\n" +
				"106;4.56;Освещение места работ ;10;9;5;5;10\n" +
				"107;4.57;Работы по подготовке укрепленного пластичного грунта;0;0;0;0;0\n" +
				"108;4.58;Очистка оборудования;8;10;10;10;9\n" +
				"109;4.59;Приемка арматуры при новом строительстве;9;10;10;5;3\n" +
				"110;4.60;Снятие показаний (ПОВ, скважины, водомерные узлы);8;9;10;7;6\n" +
				"111;4.61;Дополнительный осмотр водоразборной колонки;9;10;5;8;4\n" +
				"112;4.62;Приемка средств малой механизации;10;10;10;10;10\n" +
				"113;4.63;Выдача средств малой механизации;0;0;0;0;0\n" +
				"114;4.64;Обслуживание и ремонт средств малой механизации;5;5;5;5;5\n" +
				"115;4.65;Шурфовка инженерных коммуникаций вручную;9;10;5;5;10\n" +
				"116;4.66;Обустройство сооружений (в т.ч. камер, колодцев, в/узлов);8;10;5;4;9\n" +
				"117;4.67;Обслуживание Точек отбора проб;5;10;5;5;5\n" +
				"118;4.68;Мелкий ремонт ПГ;10;10;10;7;9\n" +
				"119;4.69;Подготовительные работы с использованием спецтехники (вывоз снега, вывоз грунта, завоз материалов и т.д.);9;10;8;6;10\n" +
				"120;4.70;Замена арматуры при замене ВРК;9;10;8;0;10\n" +
				"121;null;Монтаж арматуры бесколодезного типа для минимизации отключений;9;10;5;0;10\n" +
				"122;null;Монтаж арматуры колодезного типа для минимизации отключений;9;10;5;0;10\n" +
				"123;null;\"Установка арматуры для локализации вытекания (частный сектор) на сетях абонента\";9;10;5;0;10\n" +
				"124;null;Устранение провалов асфальта;10;10;9;9;10\n";
		List<BrigadeWorkPriorityDto> brigadeWorkPriorityDtos = new ArrayList<>();
		List<String> items = Arrays.asList(csv.split("\n"));
		items.stream().forEach(e->{
			String[] data = e.split(";");
			getLogger().debug(StringUtil.returnDebugString(Arrays.asList(data)));
			brigadeWorkPriorityDtos.add(new BrigadeWorkPriorityDto(Long.parseLong(data[0]),data[2],Integer.parseInt(data[3]),Integer.parseInt(data[4]),Integer.parseInt(data[5]),Integer.parseInt(data[6]),Integer.parseInt(data[7])));
		});
		return brigadeWorkPriorityDtos;
	}

}
