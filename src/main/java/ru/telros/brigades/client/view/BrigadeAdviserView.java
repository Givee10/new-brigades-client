package ru.telros.brigades.client.view;

import com.vaadin.annotations.StyleSheet;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.server.UserError;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadeAdviser;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.GeocodeDto;
import ru.telros.brigades.client.dto.SettingsMapDto;
import ru.telros.brigades.client.dto.WorkDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.ui.component.HeaderLayout;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.window.GeocodeWindow;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;

public class BrigadeAdviserView extends HeaderLayout implements View, HasLogger {

    BrigadeAdviser adviser;
    WorkDto workDto;
    VerticalLayout layout;
    public BrigadeAdviserView(WorkDto workDto){
        this.workDto = workDto;
        /*addToHeader(new Title("Советчик"));*/
        BrigadesEventBus.register(this);
        Responsive.makeResponsive(this);
//
//        Button button = new Button("Далее");
//        button.addClickListener(clickEvent -> {
            UI.getCurrent().addWindow(new StartOptionsWindow());
//
//        });
//
//        layout = new VerticalLayout(button);
////        layout.setExpandRatio(grid,1);
//        addComponent(layout);



    }

    @StyleSheet("vaadin://time-only.css")
    private class StartOptionsWindow extends Window{

        Button timeType;
        boolean isAbsolute = false;
        DateTimeField startTime;
        DateTimeField endTime;
        TextField delayField;
        Button go;
        public StartOptionsWindow(){
            center();
            setCaption("Параметры");
            SettingsMapDto settings = BrigadesUI.getCurrentSettings();
            if (settings.getSettingValue("brigadeAdviser.isAbsolute")!=null &&
                    settings.getSettingValue("brigadeAdviser.isAbsolute").equals("true")) {
                    timeType = new Button("Абсолютное");
                    isAbsolute = true;
            }else {
                timeType = new Button("Относительное");
                isAbsolute = false;
            }
            timeType.addClickListener(clickEvent -> {
                if (isAbsolute){
                    timeType.setCaption("Относительное");
                    startTime.setCaption("Начало через (ЧЧ:ММ): ");
                    endTime.setCaption("Крайнее время начала через (ЧЧ:ММ): ");
                    startTime.setValue(LocalDateTime.now().withHour(0).withMinute(0));
                    endTime.setValue(LocalDateTime.now().withHour(1).withMinute(0));
                }else {
                    timeType.setCaption("Абсолютное");
                    startTime.setCaption("Начало в (ЧЧ:ММ): ");
                    endTime.setCaption("Крайнее время начала в (ЧЧ:ММ): ");
                    startTime.setValue(LocalDateTime.now());
                    endTime.setValue(LocalDateTime.now().plusHours(1));

                }
                isAbsolute=!isAbsolute;
            });
            startTime = new DateTimeField();
            if (settings.getSettingValue("brigadeAdviser.startTime")!=null) {
                startTime.setValue(LocalDateTime.parse(settings.getSettingValue("brigadeAdviser.startTime")));
            }
            startTime.addStyleName("time-only");
            startTime.setDateFormat("HH:mm");
            startTime.setPlaceholder("ЧЧ:ММ");
            startTime.setCaption("Начало через (ЧЧ:ММ): ");
            startTime.setLocale(new Locale("ru","RU"));
            endTime = new DateTimeField();
            if (settings.getSettingValue("brigadeAdviser.endTime")!=null) {
                endTime.setValue(LocalDateTime.parse(settings.getSettingValue("brigadeAdviser.endTime")));
            }
            endTime.addStyleName("time-only");
            endTime.setDateFormat("HH:mm");
            endTime.setPlaceholder("ЧЧ:ММ");
            endTime.setCaption("Крайний срок начала через (ЧЧ:ММ): ");
            endTime.setLocale(new Locale("ru","RU"));
            go = new Button("Далее");
            go.addClickListener(clickEvent -> {
                if (timeIsValid() && delayIsValid()) {
                    List<ZonedDateTime> dateInterval = getDateInterval();
                    settings.addSettings("brigadeAdviser.isAbsolute",isAbsolute+"");
                    settings.addSettings("brigadeAdviser.startTime",startTime.getValue().toString());
                    settings.addSettings("brigadeAdviser.endTime",endTime.getValue().toString());
                    settings.addSettings("brigadeAdviser.delay", delayField.getValue());
                    adviser = new BrigadeAdviser(workDto,Integer.parseInt(delayField.getValue()),dateInterval.get(0),dateInterval.get(1));
//                    aUI.getCurrent().getWindows().stream().forEach(e->e.close());
                    UI.getCurrent().addWindow(new GridWindow(dateInterval.get(0), dateInterval.get(1), Double.parseDouble(delayField.getValue())));
                    this.close();

                }
            });

            delayField = new TextField("Допустимое время задержки, мин");

            if (settings.getSettingValue("brigadeAdviser.delay")!=null) {
                delayField.setValue(settings.getSettingValue("brigadeAdviser.delay"));
            }
            delayField.setPlaceholder("Количество минут");
            delayField.addListener(e -> {
                if (!delayField.getValue().matches("^\\d+$")){
                    delayField.setComponentError(new UserError("Введите число"));
                }
            });
            VerticalLayout verticalLayout = new VerticalLayout(timeType, startTime,endTime, delayField, go);
            setContent(verticalLayout);
        }
        private boolean delayIsValid(){
            if(!delayField.getValue().matches("^\\d+$")){
                return false;
            }else {
                return true;
            }
        }
        private boolean timeIsValid(){
            if (isAbsolute) {
                if (endTime.getValue() == null) {
                    if (startTime.getValue() == null) {
                        Notification.show("Введите раннее и позднее время начала работы", Notification.Type.ERROR_MESSAGE);
                        return false;
                    } else {
                        endTime.setValue(startTime.getValue());
                    }
                }
            }else {
                if (endTime.getValue() == null){
                    if (startTime.getValue() == null){
                        Notification.show("Введите раннее и позднее время начала работы", Notification.Type.ERROR_MESSAGE);
                        return false;
                    }else {
                        endTime.setValue(startTime.getValue());
                    }
                }
                if (startTime.getValue().isAfter(endTime.getValue())){
                    Notification.show("Относительное раннее время начала работы не может быть больше позднего времени начала", Notification.Type.ERROR_MESSAGE);
                    return false;
                }
            }
            return true;
        }
        private List<ZonedDateTime> getDateInterval(){
            List<ZonedDateTime> interval = new ArrayList<>();
            ZonedDateTime start;
            ZonedDateTime end;
            ZonedDateTime now =ZonedDateTime.now();
            //абсолютное время
            if (isAbsolute) {
                start = DatesUtil.localToZoned(startTime.getValue());
                end = DatesUtil.localToZoned(endTime.getValue());

                if (start.isBefore(now)) {
                    start=start.plusDays(1);
                }
                if (end.isBefore(now)) {
                    end=end.plusDays(1);
                }
                if (end.isBefore(start)) {
                    start = start.plusDays(1);
                }
                //относительное время
            }else {
                start = now;
                end = now;
                start=start.plusHours(startTime.getValue().getHour()).plusMinutes(startTime.getValue().getMinute());
                end=end.plusHours(endTime.getValue().getHour()).plusMinutes(endTime.getValue().getMinute());

            }
            interval.add(start);
            interval.add(end);
            return interval;
        }

    }

    //окно с таблицей бригад и их параметрами
    private class GridWindow extends Window {
        Grid<BrigadeAdviser.BrigadeInfo> grid;
        Set<BrigadeAdviser.BrigadeInfo> includedBrigades;
        Button go;
        VerticalLayout verticalLayout = new VerticalLayout();
        public GridWindow(ZonedDateTime start, ZonedDateTime end, Double delay){
            center();
            setCaption("Бригады");
            includedBrigades = new HashSet<>();
            grid = initGrid(adviser.getBrigadesInfo(), start, end, delay);
            go = new Button("Далее");
            go.addClickListener(clickEvent -> {
//                UI.getCurrent().getWindows().stream().forEach(e->e.close());
                if (includedBrigades.size()==0){
                    Notification.show("Для работы советчика необходимо выбрать хотя бы одну бригаду", Notification.Type.ERROR_MESSAGE);
                }else if(includedBrigades.stream().filter(e->e.getDistanceToTarget()==null).count()!=0){
                    Notification.show("Как минимум у одной из выбранных бригад отсутствует расстояние до точки назначения. Введите её адрес", Notification.Type.ERROR_MESSAGE);
                }else {
                    UI.getCurrent().addWindow(new ResultWindow(adviser.getCalculations(includedBrigades)));
                    this.close();
                }
            });
            grid.setWidth("100%");
            verticalLayout.addComponents(grid, go);
            verticalLayout.setWidth(800,Unit.PIXELS);
            verticalLayout.setExpandRatio(grid,1);
            verticalLayout.setExpandRatio(go,1);
            setContent(verticalLayout);
        }
        public Grid<BrigadeAdviser.BrigadeInfo> initGrid(List<BrigadeAdviser.BrigadeInfo> brigadeInfos, ZonedDateTime start, ZonedDateTime end, double delay){
            Grid<BrigadeAdviser.BrigadeInfo> grid = new Grid();
            grid.addComponentColumn(brigadeInfo -> {
                CheckBox checkBox = new CheckBox();
                if (brigadeInfo.getPriority()!=4){
                    checkBox.setValue(true);
                    includedBrigades.add(brigadeInfo);
                }
                checkBox.addValueChangeListener(valueChangeEvent -> {
                   if (valueChangeEvent.getValue()){
                       if (brigadeInfo != null && (brigadeInfo.getWorkPlan() == null || brigadeInfo.getWorkPlan().size() == 0)) {
                           Notification.show(String.format("Бригада %s не зарегистрирована", brigadeInfo.getBrigade().getName()), Notification.Type.TRAY_NOTIFICATION);
                           includedBrigades.add(brigadeInfo);
                       }else {
                           includedBrigades.remove(brigadeInfo);
                       }
                   }
                });
                return checkBox;
            }).setId("includeCheckBox");
            grid.addColumn(brigadeInfo -> brigadeInfo.getBrigade().getName()).setId("brigadeName").setCaption("Бригада");
            grid.addColumn(brigadeInfo -> {
                if (brigadeInfo.currentWork==null) return " - ";
                if (brigadeInfo.currentWork.getStatus().equals(WorkDto.Status.IN_PROGRESS) || brigadeInfo.currentWork.getStatus().contains("Выполняется")){
                    return brigadeInfo.currentWork.getAddress() + " (выполняется)";
                } else if (brigadeInfo.currentWork.getStatus().equals(WorkDto.Status.ASSIGNED) || brigadeInfo.currentWork.getStatus().contains("Назначена")){
                    return brigadeInfo.currentWork.getAddress() + " (в пути)";
                } else if (BrigadesUI.getCurrentSettingValue("brigadeAdviser.brigadeAddress."+brigadeInfo.getBrigade().getId())!=null){
                    brigadeInfo.location = BrigadesUI.getCurrentSettingValue("brigadeAdviser.brigadeAddress."+brigadeInfo.getBrigade().getId());
                    return brigadeInfo.location;
                }
                return " - ";
            }).setId("curWorkAddress").setCaption("Адрес выполняемой (назначенной) работы");
            grid.addComponentColumn(brigadeInfo -> {
                try {
                    return new Label(new DecimalFormat("#.0#").format(brigadeInfo.getDistanceToTarget()));
                }catch (IllegalArgumentException e){
                    Label distance = new Label();
                    distance.setVisible(false);
                    Button button = new Button("Ввести адрес вручную");
                    button.addClickListener(event -> {

                            GeocodeWindow geocodeWindow = GeocodeWindow.open(null);
                            geocodeWindow.addCloseListener(closeEvent -> {
                                if (geocodeWindow.isModalResult()) {
                                    GeocodeDto geocodeDto = geocodeWindow.getAddress();
                                    distance.setValue(geocodeDto.getName());
                                    brigadeInfo.location = geocodeDto.getName();
                                    brigadeInfo.distanceToTarget = brigadeInfo.initDistanceToTarget();
                                    distance.setValue(new DecimalFormat("#.0#").format(brigadeInfo.getDistanceToTarget()));
                                    button.setCaption("Изменить адрес");
                                    distance.setVisible(true);
                                    BrigadesUI.getCurrentSettings().addSettings("brigadeAdviser.brigadeAddress."+brigadeInfo.getBrigade().getId(),brigadeInfo.location);
                                }

                            });

                    });
                    HorizontalLayout layout = new HorizontalLayout(distance,button);
                    layout.setWidth("100%");
                    layout.setExpandRatio(button,1);
                    return layout;
                }
            }).setId("distance").setCaption("Расстояние до места планир. работы, км");
            grid.addColumn(brigadeInfo -> brigadeInfo.getCurrentWorkAndAction()).setId("curWork").setCaption("Работа/Последний выполненный этап");
            grid.addColumn(brigadeInfo -> {
                String text;
                String startTime = DatesUtil.formatZonedtoMMHH(start);
                String endTime = DatesUtil.formatZonedtoMMHH(end);
                switch (brigadeInfo.getPriority()){
                    case 1:
                        text =  String.format("В течение смены (начало работы в диапазоне %s - %s)",startTime, endTime);
                        break;
                    case 2:
                        text =  String.format("В течение смены (начало работы не позже чем в )",startTime);
                        break;
                    case 3:
                        text =  String.format("Задержка после смены на %d час (начало работы не позже чем в %s)",delay, startTime);;
                        break;
                    case 4:
                        text =  "Невозможно в течение смены";
                        break;
                    default:
                        text =  "";
                        break;

            }
                return text;
            }).setId("priority").setCaption("Возможность выполнения планир. работы");
            grid.setItems(brigadeInfos);
            grid.setSelectionMode(Grid.SelectionMode.NONE);

            return grid;
        }

    }
    //окно вывода результатов расчётов
    private class ResultWindow extends Window {
        VerticalLayout verticalLayout;
        Grid<BrigadeAdviser.BrigadeInfo> resultGrid;
        public ResultWindow(Map<BrigadeAdviser.BrigadeInfo, BrigadeAdviser.Calculations> resultMap){
            center();
            setCaption("Результаты");
            verticalLayout = new VerticalLayout();
            resultGrid = initGrid(resultMap);
            verticalLayout.addComponent(resultGrid);
            verticalLayout.setExpandRatio(resultGrid,1);
            setContent(verticalLayout);
        }
        private Grid<BrigadeAdviser.BrigadeInfo> initGrid(Map<BrigadeAdviser.BrigadeInfo, BrigadeAdviser.Calculations> resultMap){
            Grid<BrigadeAdviser.BrigadeInfo> grid = new Grid<>();
            grid.addColumn(e->e.getBrigade().getName()).setCaption("Бригада").setId("brigade");
            grid.addColumn(e->e.getCurrentWorkAndAction()).setCaption("Работа/Последний выполненный этап").setId("work");
            grid.addColumn(e->{
                if (e.getCurrentWork()!=null){
                     return e.getCurrentWork().getAddress();
                }else {
                    return " - ";
                }

            }).setCaption("Адрес выполняемой (назначенной) работы").setId("address");
            grid.addColumn(e->resultMap.get(e).getResult()).setCaption("Значение ЦФ").setId("result");
            grid.addColumn(e->resultMap.get(e).getDistanceToTarget()).setCaption("Расстояние до планир. работы, км").setId("distance");
            grid.addColumn(e->resultMap.get(e).getDistanceToTargetCalculated()).setCaption("Рассчитанный фактор расстояния").setId("distance_factor");

            grid.addColumn(e->resultMap.get(e).getBrigadeType()).setCaption("Тип бригады").setId("brigadeType");
            grid.addColumn(e->resultMap.get(e).getBrigadeTypeCalculated()).setCaption("Рассчитанный фактор типа бригады").setId("brigadeType_factor");

            grid.addColumn(e->resultMap.get(e).getPriority()).setCaption("Возможность выполнить планир. работу").setId("possibility");
            grid.addColumn(e->resultMap.get(e).getPriorityCalculated()).setCaption("Рассчитанный фактор возможности выполнить работу").setId("possibility_factor");
            grid.setItems(resultMap.keySet());
            return grid;
        }
    }
}

