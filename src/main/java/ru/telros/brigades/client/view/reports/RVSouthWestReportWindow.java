package ru.telros.brigades.client.view.reports;

import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import jxl.write.WriteException;
import ru.telros.brigades.client.AdvancedFileDownloader;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.OrganizationDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.reportbuilders.RV_SOUTHWEST;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.ui.utils.FileUtil;
import ru.telros.brigades.client.view.reports.util.GridWithButtonLayout;

import java.io.*;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.telros.brigades.client.BrigadesUI.getRestTemplate;

public class RVSouthWestReportWindow  extends Window implements HasLogger {
        RV_SOUTHWEST repBuilder;
        Grid<String> organizationGrid;
        PopupView organizationView;
        Button organizationButton;
        Button showButton;
        Button downloadButton;
        Label timeDisplay;
    public RVSouthWestReportWindow(String title) {

        super(title);

        center();
        setClosable(true);
        setResizable(false);

        ZonedDateTime currentTime = ZonedDateTime.now();
        ZonedDateTime fromTime = ZonedDateTime.of(currentTime.getYear(),currentTime.getMonthValue(),currentTime.getDayOfMonth(),7,0,0,0,currentTime.getZone());
        timeDisplay = new Label("Стандартный отчёт по заявкам \n"+ DatesUtil.formatZonedtoDDMMYYY(fromTime)+"\n"
                +"С  "+ DatesUtil.formatZonedtoMMHH(fromTime)+"\nПо "+DatesUtil.formatZonedtoMMHH(currentTime), ContentMode.PREFORMATTED);
        List<RequestDto>    requests = getRestTemplate().getRequests();
        organizationGrid = initOrganizationGrid();
        organizationButton = new Button("Организации");
      /*  organizationView = new PopupView(null, organizationGrid);
        organizationView.setHideOnMouseOut(false);

        organizationButton.addClickListener(e->{
            organizationView.setPopupVisible(true);
        });*/
        GridWithButtonLayout organizationLayout = new GridWithButtonLayout(organizationGrid, organizationButton);

        showButton = new Button("Показать");
        showButton.addClickListener(e->{
            if (checkOrganizationSelection()) {
                String filename = "RVSouthWest.html";
                repBuilder = new RV_SOUTHWEST(fromTime, currentTime, applyOrganizationFilter(requests), formOrganizationList());

                StreamResource streamResource = new StreamResource(new StreamResource.StreamSource() {
                    @Override
                    public InputStream getStream() {
                        try {
                            return new ByteArrayInputStream(repBuilder.openHTMLTable().getBytes("cp1251"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }, filename);

                streamResource.setMIMEType("text/html");
                getUI().getPage().open(streamResource, "_new", 100, 100, BorderStyle.DEFAULT);
                this.close();
            }else {
                Notification.show("Выберите Организацию", Notification.Type.ERROR_MESSAGE);
            }
        });
        downloadButton = new Button("Скачать");
        AdvancedFileDownloader downloader = new AdvancedFileDownloader();
        downloader.addAdvancedDownloaderListener(new AdvancedFileDownloader.AdvancedDownloaderListener() {
            @Override
            public void beforeDownload(AdvancedFileDownloader.DownloaderEvent downloadEvent) {
                File file = null;

                try {
                    if (checkOrganizationSelection()){
                        if (repBuilder==null){
                            repBuilder = new RV_SOUTHWEST(fromTime, currentTime, applyOrganizationFilter(requests), formOrganizationList());
                        }
                        file = repBuilder.saveRequestReport(FileUtil.reportsDirectory+"Zayavki_standart_"+DatesUtil.nowAsHH_MM_DD_MM_YYYY()+".xls");
                        String filePath = file.getPath();
                        downloader.setFilePath(filePath);
                        getLogger().debug("Starting downlad by button "
                                + filePath);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (WriteException e) {
                    e.printStackTrace();
                }


            }
        });
        downloader.extend(downloadButton);

        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(timeDisplay);
        layout.addComponent(organizationLayout);
        organizationLayout.addComponent(showButton);

        layout.setExpandRatio(organizationLayout,1);
        layout.addComponent(downloadButton);
        setContent(layout);

    }
    private StreamResource createResource(File file) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {

                try {
                    return new FileInputStream(file);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }

            }
        }, "report1.xls");
    }
    private Grid<String> initOrganizationGrid(){
        Grid<String> grid = new Grid<>();
        grid.addColumn(e->e.toString()).setId("organization").setCaption("Организация");
        Set<OrganizationDto> orgs = new HashSet<>(BrigadesUI.getRestTemplate().getOrganizations().stream().filter(e->e.getType().equals("REGION")).collect(Collectors.toList()));
        orgs.stream().forEach(e-> getLogger().debug(e.debugInfo()));
        grid.setItems(orgs.stream().map(e->e.getName()).distinct().collect(Collectors.toList()));
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        return grid;
    }
    private List<RequestDto> applyOrganizationFilter(List<RequestDto> requests){
        return  requests.stream().filter(e->organizationGrid.getSelectedItems().contains(e.getOrganization())).collect(Collectors.toList());
    }
    private boolean checkOrganizationSelection(){
        if (organizationGrid.getSelectedItems().size()>0){
            return true;
        }
        Notification.show("Выберите организации", Notification.Type.ERROR_MESSAGE);
        return false;
    }
    private String formOrganizationList(){
        StringBuilder sb = new StringBuilder();
        organizationGrid.getSelectedItems().stream().forEach(e ->sb.append(e+", "));
        sb.replace(sb.length()-2,sb.length(),".");
        return sb.toString();
    }


}
