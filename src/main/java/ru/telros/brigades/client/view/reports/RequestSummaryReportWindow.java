package ru.telros.brigades.client.view.reports;

import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.OrganizationDto;
import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.reportbuilders.RequestSummaryReportBuilder;
import ru.telros.brigades.client.ui.utils.DatesUtil;
import ru.telros.brigades.client.view.reports.util.GridWithButtonLayout;

import java.io.*;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

////Класс-ui для построения отчета "Сводка по заявкам" (По файлу "Сводная Северное 25.10.2017.xls")
public class RequestSummaryReportWindow extends Window implements HasLogger {
    RequestSummaryReportBuilder repBuilder;
    Button showButton;
    Label timeDisplay;

    Grid<String> organizationGrid;
    PopupView organizationView;
    Button organizationButton;


    public RequestSummaryReportWindow(String title){
        super(title);
        center();
        setClosable(true);
        setResizable(false);
        ZonedDateTime currentTime = ZonedDateTime.now();
        ZonedDateTime fromTime = ZonedDateTime.of(currentTime.getYear(),currentTime.getMonthValue(),currentTime.getDayOfMonth(),0,0,0,0,currentTime.getZone());
        timeDisplay = new Label("Сводка по заявкам за "+ DatesUtil.formatZonedtoDDMMYYY(fromTime)+"\n"
                +"С  "+ DatesUtil.formatZonedtoMMHH(fromTime)+"\nПо "+DatesUtil.formatZonedtoMMHH(currentTime), ContentMode.PREFORMATTED);

        organizationGrid = initOrganizationGrid();/*
        organizationView = new PopupView(null, organizationGrid);
        organizationView.setHideOnMouseOut(false);*/
        organizationButton = new Button("Организации");/*
        organizationButton.addClickListener(e->{
            organizationView.setPopupVisible(true);
        });*/
        GridWithButtonLayout organizationLayout = new GridWithButtonLayout(organizationGrid, organizationButton);
        showButton = new Button("Показать");
        showButton.addClickListener(e->{

            if (organizationGrid.getSelectedItems().size()!=0){

                List<RequestDto> requests = applyOrganizationFilter(BrigadesUI.getRestTemplate().getRequests());
                repBuilder = new RequestSummaryReportBuilder(requests,fromTime, currentTime,formOrganizationList());
                String filename = "summary.html";
                getLogger().debug(repBuilder.getHtmlText());
                StreamResource streamResource =  new StreamResource((StreamResource.StreamSource) () -> {
                    try {
                        return new ByteArrayInputStream(repBuilder.getHtmlText().getBytes("cp1251"));
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                    return null;
                }, filename);
                streamResource.setMIMEType("text/html");
                getUI().getPage().open(streamResource, "_new", 100, 100, BorderStyle.DEFAULT);
                this.close();
            }else {
                Notification.show("Выберите организации", Notification.Type.ERROR_MESSAGE);
            }

    });


//        Button downloadButton = new Button("Скачать");


//        GridLayout layout = new GridLayout(3,4);
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(timeDisplay);
        organizationLayout.addComponent(showButton);
        layout.addComponent(organizationLayout);
        layout.setExpandRatio(timeDisplay,1);
//        layout.addComponent(downloadButton,2,2);
        setContent(layout);

    }
    private StreamResource createResource(File file) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {

                try {
                    return new FileInputStream(file);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }

            }
        }, "report.xls");
    }
    private Grid<String> initOrganizationGrid(){
        Grid<String> grid = new Grid<>();
        grid.addColumn(e->e.toString()).setId("organization").setCaption("Организация");
        Set<OrganizationDto> orgs = new HashSet<>(BrigadesUI.getRestTemplate().getOrganizations().stream().filter(e->e.getType().equals("REGION")).collect(Collectors.toList()));
        orgs.stream().forEach(e-> getLogger().debug(e.debugInfo()));
        grid.setItems(orgs.stream().map(e->e.getName()).distinct().collect(Collectors.toList()));
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        return grid;
    }
    private List<RequestDto> applyOrganizationFilter(List<RequestDto> requests){
        return  requests.stream().filter(e->organizationGrid.getSelectedItems().contains(e.getOrganization())).collect(Collectors.toList());
    }
    private String formOrganizationList(){
        StringBuilder sb = new StringBuilder();
        organizationGrid.getSelectedItems().stream().forEach(e ->sb.append(e+", "));
        sb.replace(sb.length()-2,sb.length(),".");
        return sb.toString();
    }

}
