/*
package ru.telros.brigades.client.view.reports;

import com.vaadin.ui.*;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.RoleDto;
import ru.telros.brigades.client.dto.UserDto;
import ru.telros.brigades.client.dto.report.UserWorkDto;
import ru.telros.brigades.client.view.reports.util.ReportDateTimeLayout;

public class UserWorkReportWindow extends Window {

    ReportDateTimeLayout timeLayout;
    ComboBox<UserDto> user;
    Button button;
    Grid<UserWorkDto> grid;
    public UserWorkReportWindow(){
        setCaption("Отчет о работе пользователя системы");
        grid = initGrid();
        timeLayout = new ReportDateTimeLayout();
        user = new ComboBox<>();
        user.setPopupWidth("300%");
        user.setItemCaptionGenerator(e->e.getFullName());
        user.setEmptySelectionAllowed(false);
        user.setItems(BrigadesUI.getRestTemplate().getUsers());
        button = new Button("Обновить");
        button.addClickListener(e->{
            grid.setItems();
        });
        HorizontalLayout parameters = new HorizontalLayout(timeLayout,user,button);
        VerticalLayout mainLayout = new VerticalLayout(parameters,grid);
        timeLayout.setHeight("100px");
        grid.setWidth("100%");
        grid.setHeight("100%");
        setContent(mainLayout);
        setSizeFull();
    }
    private Grid<UserWorkDto> initGrid(){
        Grid<UserWorkDto> grid = new Grid<>();
        if (BrigadesUI.getRestTemplate().
                getUserRoles(BrigadesUI.getCurrentUser().getId()).contains(new RoleDto("Мастер бригады","Мастер бригады"))){
            grid.addColumn(e->e.getSendToBrigadeTime()).setCaption("Время отправки работы бригаде");
            grid.addColumn(e->e.getConfirmTime()).setCaption("Время квитирования события мастером");
            grid.addColumn(e->e.getArrivalTime()).setCaption("Время выполнения этапа прибытия на адрес");
        }else {
            grid.addColumn(e->e.getArrivalToHLTime()).setCaption("Время поступления заявки в ГЛ");
            grid.addColumn(e->e.getConfirmOfReqIncTime()).setCaption("Время квитирования события поступления заявки");
            grid.addColumn(e->e.getWorkByReqCreationTime()).setCaption("Время создания работы по заявке");
        }
        return grid;
    }
}
*/
