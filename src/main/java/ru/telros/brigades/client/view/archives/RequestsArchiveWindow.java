package ru.telros.brigades.client.view.archives;

import ru.telros.brigades.client.dto.RequestDto;
import ru.telros.brigades.client.view.reports.RequestsReportWindow;

import java.util.List;
import java.util.stream.Collectors;

public class RequestsArchiveWindow extends RequestsReportWindow {
    public RequestsArchiveWindow(String title) {
        super("Архив по заявкам");
        this.statusButton.setVisible(false);
        this.statusGrid.setVisible(false);
        this.statusView.setVisible(false);
        this.requestStatusButton.setVisible(false);
        this.requestStatusGrid.setVisible(false);
        this.requestStatusView.setVisible(false);
    }
    @Override
    public List<RequestDto> applyRequestHLStatusFilter(List<RequestDto> requestsToFilter){
        return requestsToFilter.stream().filter(requestDto -> requestDto.getStatusHL()!=null && requestDto.getStatusHL().equals("Архив")).collect(Collectors.toList());
    }
}
