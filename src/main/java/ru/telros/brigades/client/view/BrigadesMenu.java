//package ru.telros.brigades.client.view;
//
//import com.google.common.eventbus.Subscribe;
//import com.vaadin.server.ThemeResource;
//import com.vaadin.server.VaadinSession;
//import com.vaadin.shared.ui.ContentMode;
//import com.vaadin.ui.*;
//import com.vaadin.ui.themes.ValoTheme;
//import ru.telros.brigades.client.BrigadesTheme;
//import ru.telros.brigades.client.dto.UserDto;
//import ru.telros.brigades.client.event.BrigadesEventBus;
//import ru.telros.brigades.client.event.PostViewChangeEvent;
//
//public class BrigadesMenu extends CustomComponent {
//public static final String ID = "dashboard-menu";
//public static final String REPORTS_BADGE_ID = "dashboard-menu-reports-badge";
//public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
//private static final String STYLE_VISIBLE = "valo-menu-visible";
//private Label notificationsBadge;
//private Label reportsBadge;
//private MenuBar.MenuItem settingsItem;
//
//public BrigadesMenu() {
//setPrimaryStyleName("valo-menu");
//setId(ID);
//setSizeUndefined();
//
//// There's only one DashboardMenu per UI so this doesn't need to be
//// unregistered from the UI-scoped DashboardEventBus.
//BrigadesEventBus.register(this);
//
//setCompositionRoot(buildContent());
//}
//
//private Component buildContent() {
//final CssLayout menuContent = new CssLayout();
//menuContent.addStyleName("sidebar");
//menuContent.addStyleName(ValoTheme.MENU_PART);
//menuContent.addStyleName("no-vertical-drag-hints");
//menuContent.addStyleName("no-horizontal-drag-hints");
//menuContent.setWidth(null);
//menuContent.setHeight("100%");
//
////menuContent.addComponent(buildTitle());
//menuContent.addComponent(buildUserMenu());
//menuContent.addComponent(buildToggleButton());
//menuContent.addComponent(buildMenuItems());
//
//return menuContent;
//}
//
//private Component buildTitle() {
//Label logo = new Label("QuickTickets <strong>Dashboard</strong>", ContentMode.HTML);
//logo.setSizeUndefined();
//HorizontalLayout logoWrapper = new HorizontalLayout(logo);
//logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
//logoWrapper.addStyleName("valo-menu-title");
//logoWrapper.setSpacing(false);
//return logoWrapper;
//}
//
//private UserDto getCurrentUser() {
//return (User) VaadinSession.getCurrent().getAttribute(UserDto.class);
//}
//
//private Component buildUserMenu() {
//final MenuBar settings = new MenuBar();
//settings.addStyleName("user-menu");
//final User user = getCurrentUser();
//settingsItem = settings.addItem("",
//new ThemeResource("img/profile-pic-300px.jpg"), null);
////updateUserName(null);
//settingsItem.addItem("Edit Profile", (MenuBar.Command) selectedItem -> {
////ProfilePreferencesWindow.open(user, false);
//});
//settingsItem.addItem("Preferences", (MenuBar.Command) selectedItem -> {
////ProfilePreferencesWindow.open(user, true);
//});
//settingsItem.addSeparator();
//settingsItem.addItem("Sign Out", (MenuBar.Command) selectedItem ->
//BrigadesEventBus.post(new UserLogoutEvent(getUI(), getCurrentUser())));
//return settings;
//}
//
//private Component buildToggleButton() {
//Button valoMenuToggleButton = new Button("Menu", $ -> {
//if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
//getCompositionRoot().removeStyleName(STYLE_VISIBLE);
//} else {
//getCompositionRoot().addStyleName(STYLE_VISIBLE);
//}
//});
//valoMenuToggleButton.setIcon(BrigadesTheme.ICON_REFERENCE);
//valoMenuToggleButton.addStyleName("valo-menu-toggle");
//valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
//return valoMenuToggleButton;
//}
//
//private Component buildMenuItems() {
//CssLayout menuItemsLayout = new CssLayout();
//menuItemsLayout.addStyleName("valo-menuitems");
//
//for (final BrigadesViewType view : BrigadesViewType.values()) {
//Component menuItemComponent = new ValoMenuItemButton(view);
//
//if (view == BrigadesViewType.REPORTS) {
//// Add drop target to reports button
//DragAndDropWrapper reports = new DragAndDropWrapper(menuItemComponent);
//reports.setSizeUndefined();
//reports.setDragStartMode(DragAndDropWrapper.DragStartMode.NONE);
///*reports.setDropHandler(new DropHandler() {
//
//@Override
//public void drop(final DragAndDropEvent event) {
//UI.getCurrent().getNavigator().navigateTo(BrigadesViewType.REPORTS.getViewName());
//Table table = (Table) event.getTransferable()
//.getSourceComponent();
//BrigadesEventBus.post(new TransactionReportEvent(
//(Collection<Transaction>) table.getValue()));
//}
//
//@Override
//public AcceptCriterion getAcceptCriterion() {
//return AcceptItem.ALL;
//}
//
//});*/
//menuItemComponent = reports;
//}
//
//if (view == BrigadesViewType.DASHBOARD) {
//notificationsBadge = new Label();
//notificationsBadge.setId(NOTIFICATIONS_BADGE_ID);
//menuItemComponent = buildBadgeWrapper(menuItemComponent, notificationsBadge);
//}
//if (view == BrigadesViewType.REPORTS) {
//reportsBadge = new Label();
//reportsBadge.setId(REPORTS_BADGE_ID);
//menuItemComponent = buildBadgeWrapper(menuItemComponent, reportsBadge);
//}
//
//menuItemsLayout.addComponent(menuItemComponent);
//}
//return menuItemsLayout;
//
//}
//
//private Component buildBadgeWrapper(final Component menuItemButton, final Component badgeLabel) {
//CssLayout dashboardWrapper = new CssLayout(menuItemButton);
//dashboardWrapper.addStyleName("badgewrapper");
//dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
//badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
//badgeLabel.setWidthUndefined();
//badgeLabel.setVisible(false);
//dashboardWrapper.addComponent(badgeLabel);
//return dashboardWrapper;
//}
//
//@Override
//public void attach() {
//super.attach();
////updateNotificationsCount(null);
//}
//
//@Subscribe
//public void postViewChange(final PostViewChangeEvent event) {
//// After a successful view change the menu can be hidden in mobile view.
//getCompositionRoot().removeStyleName(STYLE_VISIBLE);
//}
///*
//@Subscribe
//public void updateNotificationsCount(final NotificationsCountUpdatedEvent event) {
//int unreadNotificationsCount = BrigadesUI.getDataProvider().getUnreadNotificationsCount();
//notificationsBadge.setValue(String.valueOf(unreadNotificationsCount));
//notificationsBadge.setVisible(unreadNotificationsCount > 0);
//}
//
//@Subscribe
//public void updateReportsCount(final ReportsCountUpdatedEvent event) {
//reportsBadge.setValue(String.valueOf(event.getCount()));
//reportsBadge.setVisible(event.getCount() > 0);
//}
//
//@Subscribe
//public void updateUserName(final ProfileUpdatedEvent event) {
//User user = getCurrentUser();
//settingsItem.setText(user.getFirstName() + " " + user.getLastName());
//}
//*/
//public final class ValoMenuItemButton extends Button {
//private static final String STYLE_SELECTED = "selected";
//
//private final BrigadesViewType view;
//
//public ValoMenuItemButton(final BrigadesViewType view) {
//this.view = view;
//setPrimaryStyleName("valo-menu-item");
//setIcon(view.getIcon());
//setCaption(view.getCaption().substring(0, 1).toUpperCase()
//+ view.getCaption().substring(1));
//BrigadesEventBus.register(this);
//addClickListener((ClickListener) event -> UI.getCurrent().getNavigator().navigateTo(view.getViewName()));
//}
//
//@Subscribe
//public void postViewChange(final PostViewChangeEvent event) {
//removeStyleName(STYLE_SELECTED);
//if (event.getView() == view) {
//addStyleName(STYLE_SELECTED);
//}
//}
//}
//}
