package ru.telros.brigades.client.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Notification;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.HasLogger;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.UpdateViewEvent;
import ru.telros.brigades.client.event.camera.ShowCameraEvent;
import ru.telros.brigades.client.event.eventlog.AcceptEventLogEvent;
import ru.telros.brigades.client.event.eventlog.UpdateEventLogEvent;
import ru.telros.brigades.client.event.request.SelectRequestEvent;
import ru.telros.brigades.client.event.request.UpdateRequestsEvent;
import ru.telros.brigades.client.event.vehicle.UpdateVehicleEvent;
import ru.telros.brigades.client.event.workgroup.UpdateWorkGroupsEvent;
import ru.telros.brigades.client.event.workorder.SelectWorkOrderEvent;
import ru.telros.brigades.client.event.workorder.UpdateWorkOrdersEvent;
import ru.telros.brigades.client.ui.camera.CameraLayout;
import ru.telros.brigades.client.ui.component.BrigadesPanel;
import ru.telros.brigades.client.ui.component.PanelWithMenuLayout;
import ru.telros.brigades.client.ui.eventlog.EventLogLayout;
import ru.telros.brigades.client.ui.eventlog.EventsPanel;
import ru.telros.brigades.client.ui.gantt.TreeGridGanttLayout;
import ru.telros.brigades.client.ui.maplayout.Coordinates;
import ru.telros.brigades.client.ui.maplayout.MapViewLayout;
import ru.telros.brigades.client.ui.maplayout.Marker;
import ru.telros.brigades.client.ui.request.RequestLayout;
import ru.telros.brigades.client.ui.vehicle.VehicleLayout;
import ru.telros.brigades.client.ui.workgroup.WorkGroupLayout;
import ru.telros.brigades.client.ui.workorder.WorkOrderLayout;

import java.util.List;

public class DashboardView extends PanelWithMenuLayout implements View, HasLogger {
	public static final String NAME = "dashboard";
	public static final String MENU_CAPTION = "view.dashboardview.menuitem";
	public static final String CAPTION = "Главная";
	public static final String BADGE = "dashboard-badge";

	private EventsPanel eventsPanel = new EventsPanel();
	private TreeGridGanttLayout ganttLayout = new TreeGridGanttLayout();
	private CameraLayout cameraLayout = new CameraLayout();
	private MapViewLayout mapViewLayout = new MapViewLayout();
	private RequestLayout requestLayout = new RequestLayout();
	private EventLogLayout eventLayout = new EventLogLayout();
	private VehicleLayout vehicleLayout = new VehicleLayout();
	private WorkOrderLayout workOrderLayout = new WorkOrderLayout();
	private WorkGroupLayout workGroupLayout = new WorkGroupLayout();

	public DashboardView() {
		super(CAPTION);
		addStyleName("dashboard-view");
		BrigadesEventBus.register(this);
		Responsive.makeResponsive(this);

		BrigadesPanel panel;

		addToHeader(eventsPanel, true);

		panel = addPanel("Текущие работы", ganttLayout, 0, true);
		panel.addToggleMaximizedListener((p, m) -> {  ganttLayout.update();});
		panel.addToggleVisibleListener((p, v) -> ganttLayout.setUpdateVisible(v));
		ganttLayout.addToolBarItems(panel);

		panel = addPanel("Мнемосхема", mapViewLayout.getComponent(), 1, true);
		panel.addToggleVisibleListener((p, v) -> mapViewLayout.setUpdateVisible(v));
		mapViewLayout.addToolBarItems(panel);

		panel = addPanel("Транспорт", vehicleLayout, 0, false);
		panel.addToggleMaximizedListener((p, m) -> vehicleLayout.setStatisticVisible(m));
		panel.addToggleVisibleListener((p, v) -> vehicleLayout.setUpdateVisible(v));
		vehicleLayout.addToolBarItems(panel);

//		panel = addPanel("ВЗГЛЯД", cameraLayout, 1, false);
//		cameraLayout.addToolBarItems(panel);

		panel = addPanel("Бригады", workGroupLayout, 0, true);
		panel.addToggleMaximizedListener((p, m) -> workGroupLayout.setStatisticVisible(m));
		panel.addToggleVisibleListener((p, v) -> workGroupLayout.setUpdateVisible(v));
		workGroupLayout.addToolBarItems(panel);

		panel = addPanel("Журнал оперативных работ", workOrderLayout, 1, true);
		panel.addToggleMaximizedListener((p, m) -> workOrderLayout.setStatisticVisible(m));
		panel.addToggleVisibleListener((p, v) -> workOrderLayout.setUpdateVisible(v));
		workOrderLayout.addToolBarItems(panel);

		panel = addPanel("Журнал событий", eventLayout, 0, true);
		panel.addToggleVisibleListener((p, v) -> eventLayout.setUpdateVisible(v));
		eventLayout.addToolBarItems(panel);

		panel = addPanel("Журнал заявок", requestLayout, 1, true);
		panel.addToggleMaximizedListener((p, m) -> requestLayout.setStatisticVisible(m));
		panel.addToggleVisibleListener((p, v) -> requestLayout.setUpdateVisible(v));
		requestLayout.addToolBarItems(panel);

		updateAllLayouts();
	}

	private void updateAllLayouts() {
		updateMapLayout();
		updateEventLayouts();
		updateWorkLayouts();
		updateRequestLayouts();
		updateBrigadeLayouts();
	}

	private void updateBrigadeLayouts() {
		try {
			vehicleLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating vehicle layout: {}", ex.getMessage());
		}
		try {
			workGroupLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating brigade layout: {}", ex.getMessage());
		}
	}

	private void updateEventLayouts() {
		try {
			eventsPanel.updateAll();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating notification layout: {}", ex.getMessage());
		}
		try {
			eventLayout.updateAll();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating event layout: {}", ex.getMessage());
		}
	}

	private void updateMapLayout() {
		try {
			mapViewLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating map layout: {}", ex.getMessage());
		}
	}

	private void updateRequestLayouts() {
		try {
			requestLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating request layout: {}", ex.getMessage());
		}
	}

	private void updateVehicleLayout() {
		try {
			vehicleLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating vehicle layout: {}", ex.getMessage());
		}
	}

	private void updateWorkLayouts() {
		try {
			ganttLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating gantt layout: {}", ex.getMessage());
		}
		try {
			workOrderLayout.update();
		} catch (Exception ex) {
			getLogger().debug("Exception while updating work layout: {}", ex.getMessage());
		}
	}

	private void goToAddress(String address) {
		if (address != null) {
			List<GeocodeDto> geocodeList = BrigadesUI.getRestTemplate().getGeocodeAddress(address);
			if (!geocodeList.isEmpty()) {
				GeocodeDto geocodeDto = geocodeList.get(0);
				Coordinates coordinates = new Coordinates(geocodeDto.getLatitude(), geocodeDto.getLongitude());
				//mapLayout.setCenter(coordinates);
				mapViewLayout.goToCoordinates(coordinates, geocodeDto.getName());
			}
		}
	}

	@Subscribe
	public void updateViewEvent(final UpdateViewEvent event) {
		if (getUI() != null && getUI().isAttached() && getUI().equals(event.getSource())) {
			Notification show = Notification.show("Идет обновление данных...", Notification.Type.TRAY_NOTIFICATION);
			PingDto ping = BrigadesUI.getRestTemplate().ping();
			if (ping != null) {
				getLogger().debug(ping.debugInfo());
				updateAllLayouts();
			}
			show.close();
		}
	}

	@Subscribe
	public void acceptEventLogEvent(final AcceptEventLogEvent event) {
		updateEventLayouts();
	}

	@Subscribe
	public void updateEventLogEvent(final UpdateEventLogEvent event) {
		updateEventLayouts();
	}

	@Subscribe
	public void updateRequestsEvent(final UpdateRequestsEvent event) {
		updateRequestLayouts();
		updateEventLayouts();
		updateMapLayout();
	}

	@Subscribe
	public void updateVehicleEvent(final UpdateVehicleEvent event) {
		updateVehicleLayout();
	}

	@Subscribe
	public void updateWorkGroupsEvent(final UpdateWorkGroupsEvent event) {
		updateBrigadeLayouts();
		//updateMapLayout();
	}

	@Subscribe
	public void updateWorkOrdersEvent(final UpdateWorkOrdersEvent event) {
		updateWorkLayouts();
		updateEventLayouts();
		updateBrigadeLayouts();
		updateMapLayout();
	}

	/**
	 * Обработка события выбора работы
	 *
	 * @param event
	 */
	@Subscribe
	public void handleSelectWorkOrder(final SelectWorkOrderEvent event) {
		WorkDto workOrder = event.getEntity();
		if (workOrder == null) return;
		//workOrderLayout.select(workOrder);
		if (event.getSender() instanceof Marker) return;
		goToAddress(workOrder.getAddress());
	}

	/**
	 * Обработка события выбора заявки
	 *
	 * @param event
	 */
	@Subscribe
	public void handleSelectRequestEvent(final SelectRequestEvent event) {
		RequestDto request = event.getEntity();
		if (request == null) return;
		//requestLayout.select(request);
		if (event.getSender() instanceof Marker) return;
		goToAddress(request.getAddress());
	}

	@Subscribe
	public void handleShowCamera(final ShowCameraEvent event) {
		CameraDto camera = event.getEntity();
		getLogger().debug("handleShowCamera({});", camera.debugInfo());
		getUI().access(() -> {
			if (camera.getStatus().equals(CameraDto.Status.UNAVAILABLE.name())) {
				getLogger().debug("unsetResource();");
				cameraLayout.unsetResource();
				Notification.show("Ошибка: камера не включена (нет сигнала)", Notification.Type.ERROR_MESSAGE);
			} else {
				if (camera.equals(cameraLayout.getCamera())) {
					getLogger().debug("unsetResource();");
					cameraLayout.unsetResource();
				} else {
					getLogger().debug("setResource();");
					cameraLayout.setResource(camera);
				}
			}
		});
	}
}
