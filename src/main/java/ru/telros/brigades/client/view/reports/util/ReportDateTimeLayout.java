package ru.telros.brigades.client.view.reports.util;


import com.vaadin.server.UserError;
import com.vaadin.ui.VerticalLayout;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Locale;

//Готовое поля выбора временного промежутка для отчётов
public class ReportDateTimeLayout extends VerticalLayout {
    ReportDateTimeField start;
    ReportDateTimeField end;
    public ReportDateTimeLayout(){
        LocalDateTime now = LocalDateTime.now();
        this.start = new ReportDateTimeField("С");
        this.end = new ReportDateTimeField("По");
        this.start.setLocale(new Locale("ru","RU"));
        this.start.setDateFormat(DatesUtil.GRID_DATE_FORMAT);
        this.end.setLocale(new Locale("ru","RU"));
        this.end.setDateFormat(DatesUtil.GRID_DATE_FORMAT);
        start.setDefaultValue(now.minusDays(1));
        start.setValue(now.minusDays(1));
        start.setRangeEnd(now);
        end.setDefaultValue(now);
        end.setValue(now);
        end.setRangeEnd(now);
        start.setPlaceholder("ЧЧ:ММ ДД.ММ.ГГГГ");
        end.setPlaceholder("ЧЧ:ММ ДД.ММ.ГГГГ");
        start.addValueChangeListener(valueChangeEvent -> {
            if (!start.isEmpty() && !end.isEmpty()) {
                StringBuilder result = new StringBuilder();
                if (start.getValue().isAfter(now)) {
                    result.append("Дата начала не может быть позже текущего времени. ");
                }
                if (start.getValue().isAfter(end.getValue())) {
                    result.append("Дата начала не может быть позже даты конца.");
                }
                if (result.length() > 1) {
                    start.setComponentError(new UserError(result.toString()));
                } else {
                    start.setComponentError(null);
                }
            }
        });
        end.addValueChangeListener(valueChangeEvent -> {
            if (!start.isEmpty() && !end.isEmpty()) {
                start.setRangeEnd(end.getValue());
                if (end.getValue().isAfter(now)) {
                    end.setComponentError(new UserError("Дата конца не может быть позже текущего времени"));
                }
                StringBuilder result = new StringBuilder();
                if (start.getValue().isAfter(now)) {
                    result.append("Дата начала не может быть позже текущего времени. ");
                }
                if (start.getValue().isAfter(end.getValue())) {
                    result.append("Дата начала не может быть позже даты конца.");
                }
                if (result.length() > 1) {
                    start.setComponentError(new UserError(result.toString()));
                } else {
                    start.setComponentError(null);
                }
            }
        });
        addComponents(start,end);
        setMargin(false);
        setSpacing(false);
        start.setWidth("100%");
        end.setWidth("100%");
    }
    public ZonedDateTime getStart(){
        return DatesUtil.localToZoned(start.getValue());
    }
    public ZonedDateTime getEnd(){
        return DatesUtil.localToZoned(end.getValue());
    }
    public boolean isValid(){
        try {
            if (getStart().isBefore(getEnd()) && getEnd().isBefore(ZonedDateTime.now())){
                return true;
            }else {
                return false;
            }
        }catch (NullPointerException e){
            return false;
        }
    }
    public void setEmpty(){
        this.start.clear();
        this.end.clear();
    }
    public boolean isEmpty(){
        return this.start.isEmpty() && this.end.isEmpty();
    }
}
