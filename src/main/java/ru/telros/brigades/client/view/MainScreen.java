package ru.telros.brigades.client.view;

import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings("serial")
public class MainScreen extends HorizontalLayout {

	public MainScreen() {
		setSizeFull();
		addStyleName("mainview");
		setSpacing(false);

		//addComponent(new BrigadesMenu());
		addComponent(new MainMenu());

		ComponentContainer content = new CssLayout();
		content.addStyleName("view-content");
		content.setSizeFull();
		addComponent(content);
		setExpandRatio(content, 1.0f);

		new BrigadesNavigator(content);
	}
}
