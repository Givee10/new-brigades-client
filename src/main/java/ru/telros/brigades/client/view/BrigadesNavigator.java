package ru.telros.brigades.client.view;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;
import ru.telros.brigades.client.BrigadesUI;
import ru.telros.brigades.client.dto.RoleDto;
import ru.telros.brigades.client.event.BrigadesEventBus;
import ru.telros.brigades.client.event.BrowserResizeEvent;
import ru.telros.brigades.client.event.CloseOpenWindowsEvent;
import ru.telros.brigades.client.event.PostViewChangeEvent;

public class BrigadesNavigator extends Navigator {
	private static final BrigadesViewType ERROR_VIEW = BrigadesViewType.DASHBOARD;
	private ViewProvider errorViewProvider;

	public BrigadesNavigator(final ComponentContainer container) {
		super(UI.getCurrent(), container);

		initViewChangeListener();
		initViewProviders();
	}

	private void initViewChangeListener() {
		addViewChangeListener(new ViewChangeListener() {

			@Override
			public boolean beforeViewChange(final ViewChangeEvent event) {
				// Since there's no conditions in switching between the views
				// we can always return true.
				return true;
			}

			@Override
			public void afterViewChange(final ViewChangeEvent event) {
				BrigadesViewType view = BrigadesViewType.getByViewName(event.getViewName());
				// Appropriate events get fired after the view is changed.
				BrigadesEventBus.post(new PostViewChangeEvent(view));
				BrigadesEventBus.post(new BrowserResizeEvent());
				BrigadesEventBus.post(new CloseOpenWindowsEvent());
			}
		});
	}

	private void initViewProviders() {
		// A dedicated view provider is added for each separate view type
		for (final BrigadesViewType viewType : BrigadesViewType.values()) {
			if (
					(!(viewType.getViewName().equals(AdminView.NAME)||viewType.getViewName().equals(SetupView.NAME)))
							||
							((viewType.getViewName().equals(AdminView.NAME) || viewType.getViewName().equals(SetupView.NAME)) &&
									BrigadesUI.getRestTemplate().getUserRoles(BrigadesUI.getCurrentUser().getId()).contains(new RoleDto("Администратор", "Администратор")))
			) {
				ViewProvider viewProvider = new ClassBasedViewProvider(
						viewType.getViewName(), viewType.getViewClass()) {

					// This field caches an already initialized view instance if the
					// view should be cached (stateful views).
					private View cachedInstance;

					@Override
					public View getView(final String viewName) {
						View result = null;
						if (viewType.getViewName().equals(viewName)) {
							if (cachedInstance == null) {
								cachedInstance = super.getView(viewType.getViewName());
							}
							result = cachedInstance;
						}
						return result;
					}
				};

				if (viewType == ERROR_VIEW) {
					errorViewProvider = viewProvider;
				}

				addProvider(viewProvider);
			}

			setErrorProvider(new ViewProvider() {
				@Override
				public String getViewName(final String viewAndParameters) {
					return ERROR_VIEW.getViewName();
				}

				@Override
				public View getView(final String viewName) {
					return errorViewProvider.getView(ERROR_VIEW.getViewName());
				}
			});
		}
	}
}
