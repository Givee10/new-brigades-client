package ru.telros.brigades.client;

import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.*;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.scheduling.TaskScheduler;
import ru.telros.brigades.client.dto.*;
import ru.telros.brigades.client.event.*;
import ru.telros.brigades.client.event.user.UserLoginEvent;
import ru.telros.brigades.client.event.user.UserLogoutEvent;
import ru.telros.brigades.client.view.LoginScreen;
import ru.telros.brigades.client.view.MainScreen;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@SpringUI()
@Title("АИС «Бригады»")
@Theme(BrigadesTheme.THEME_NAME)
@PushStateNavigation
//@PreserveOnRefresh
@Push
public class BrigadesUI extends UI implements HasLogger, Broadcaster.BroadcastListener {
	private BrigadesEventBus eventBus = new BrigadesEventBus();
	private BrigadesRestTemplate restTemplate = new BrigadesRestTemplate();
	private final Map<UI, ScheduledFuture<?>> scheduledTasks = new HashMap<>();

	@Autowired
	private EventManager eventManager;
	@Autowired
	private TaskScheduler taskScheduler;

	public static BrigadesEventBus getEventBus() {
		return ((BrigadesUI) getCurrent()).eventBus;
	}

	public static BrigadesRestTemplate getRestTemplate() {
		return ((BrigadesUI) getCurrent()).restTemplate;
	}

	public static UserDto getCurrentUser() {
		return VaadinSession.getCurrent().getAttribute(UserDto.class);
	}

	public static void setCurrentUser(UserDto currentUser) {
		VaadinSession.getCurrent().setAttribute(UserDto.class, currentUser);
	}

	public static LoginDto getCurrentLogin() {
		return VaadinSession.getCurrent().getAttribute(LoginDto.class);
	}

	public static void setCurrentLogin(LoginDto currentLogin) {
		VaadinSession.getCurrent().setAttribute(LoginDto.class, currentLogin);
	}

	public static SettingsMapDto getCurrentSettings() {
		return VaadinSession.getCurrent().getAttribute(SettingsMapDto.class);
	}

	public static void setCurrentSettings(SettingsMapDto currentSettings) {
		VaadinSession.getCurrent().setAttribute(SettingsMapDto.class, currentSettings);
	}

	public static void setCurrentSetting(String key, String value) {
		SettingsMapDto currentSettings = getCurrentSettings();
		currentSettings.addSettings(key, value);
		setCurrentSettings(currentSettings);
	}

	public static String getCurrentSettingValue(String key) {
		return getCurrentSettings().getSettingValue(key);
	}

	public static AttachmentDto getCurrentAttachment() {
		return VaadinSession.getCurrent().getAttribute(AttachmentDto.class);
	}

	public static void setCurrentAttachment(AttachmentDto attachmentDto) {
		VaadinSession.getCurrent().setAttribute(AttachmentDto.class, attachmentDto);
	}

	public static ExcelDataList getCurrentExcelDataList() {
		return VaadinSession.getCurrent().getAttribute(ExcelDataList.class);
	}

	public static void setCurrentExcelDataList(ExcelDataList excelDataList) {
		VaadinSession.getCurrent().setAttribute(ExcelDataList.class, excelDataList);
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		setLocale(Locale.US);
		setupMessageProvider(vaadinRequest);
		BrigadesEventBus.register(this);
		Broadcaster.register(this);
		Responsive.makeResponsive(this);
		WebBrowser webBrowser = Page.getCurrent().getWebBrowser();
		getLogger().debug("Address: {}", webBrowser.getAddress());
		getLogger().debug("Time Zone: {}", webBrowser.getTimeZoneId());
		getLogger().debug("Device too old: {}", webBrowser.isTooOldToFunctionProperly());
		getLogger().debug("Touch device: {}", webBrowser.isTouchDevice());
		getLogger().debug("BrowserApplication: {}", webBrowser.getBrowserApplication());
		getLogger().debug("Screen: {} x {}", webBrowser.getScreenWidth(), webBrowser.getScreenHeight());

		updateContent();

		Page.getCurrent().addBrowserWindowResizeListener(
				(Page.BrowserWindowResizeListener) event -> BrigadesEventBus.post(new BrowserResizeEvent()));
		JavaScript javaScript = Page.getCurrent().getJavaScript();
		javaScript.addFunction("closeMyApplication", (JavaScriptFunction) arguments -> close());
		if (webBrowser.isChrome() || webBrowser.isOpera()) {
			javaScript.execute("window.onunload = function (e) { var e = e || window.event; closeMyApplication(); return; };");
		} else {
			javaScript.execute("window.onbeforeunload = function (e) { var e = e || window.event; closeMyApplication(); return; };");
		}
	}

	private void updateContent() {
		//UserDto userDto = restTemplate.getUserByLogin("admin");
		//setCurrentUser(userDto);
		if (restTemplate.isAuthenticated()) {
			showMainScreen();
		} else {
			showLoginScreen();
		}
	}

	private void showLoginScreen() {
		setContent(new LoginScreen());
	}

	private void showMainScreen() {
		setContent(new MainScreen());
		getNavigator().navigateTo(getNavigator().getState());
		ScheduledFuture<?> future = taskScheduler.scheduleAtFixedRate(this::updateViews, 60000);
		getLogger().debug("Starting update task in UI: " + this.getSession().getCsrfToken());
		scheduledTasks.put(this, future);
	}

	private void updateViews() {
		eventManager.publishUpdateEvent(this);
	}

	@Override
	public void detach() {
		getLogger().debug("Detaching " + this.getSession().getCsrfToken());
		getLogger().debug("Stopping update task in UI: " + this.getSession().getCsrfToken());
		if (scheduledTasks.containsKey(this)) {
			ScheduledFuture<?> future = scheduledTasks.get(this);
			future.cancel(true);
			scheduledTasks.remove(this);
		}
		BrigadesEventBus.unregister(this);
		Broadcaster.unregister(this);
		super.detach();
	}

	@Override
	public void receiveBroadcast(Object object) {
		//logger.debug("Receiving new Broadcast");
		if (object instanceof ApplicationEvent) {
			ApplicationEvent event = (ApplicationEvent) object;
			//logger.debug("Event source: {}", ((UI) event.getSource()).getSession().getCsrfToken());
			if (event.getSource().equals(this)) {
				access(() -> BrigadesEventBus.post(object));
			}
		}
	}

	private void setupMessageProvider(VaadinRequest request) {
		String msg = "Для продолжения корректной работы необходимо обновить страницу (нажмите Esc, F5 или на этот баннер)";
		request.getService().setSystemMessagesProvider(
				(SystemMessagesProvider) systemMessagesInfo -> {
					CustomizedSystemMessages msgs = new CustomizedSystemMessages();
					msgs.setInternalErrorMessage(msg);
					msgs.setSessionExpiredMessage(msg);
					msgs.setCommunicationErrorMessage(msg);
					msgs.setAuthenticationErrorMessage(msg);
					msgs.setInternalErrorCaption("Внутрення ошибка");
					msgs.setSessionExpiredCaption("Время сессии истекло");
					msgs.setCommunicationErrorCaption("Проблема с соединением");
					msgs.setAuthenticationErrorCaption("Не авторизован");
					return msgs;
				});
	}

	@Subscribe
	public void handleLoginEvent(final UserLoginEvent event) {
		if (event.getSource().equals(this)) {
			getLogger().debug("Handle Login event: " + event.getUser());
			restTemplate.setSession();
			//access(() -> refresh(VaadinRequest.getCurrent()));
			updateContent();
		}
	}

	@Subscribe
	public void handleLogoutEvent(final UserLogoutEvent event) {
		if (event.getSource().equals(this)) {
			getLogger().debug("Handle Logout event: " + event.getUser());
			restTemplate.deleteSession();
//			access(() -> refresh(VaadinRequest.getCurrent()));
			getUI().getSession().close();
			getPage().reload();
		}
	}

	@Subscribe
	public void closeOpenWindows(final CloseOpenWindowsEvent event) {
		for (Window window : getWindows()) {
			window.close();
		}
	}
}
