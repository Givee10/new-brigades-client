package ru.telros.brigades.client.dto;

public class StatusWorkDto {
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("StatusWorkDto{");
		sb.append("value='").append(value).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return value;
	}
}
