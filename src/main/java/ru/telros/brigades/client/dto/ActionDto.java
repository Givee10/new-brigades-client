package ru.telros.brigades.client.dto;

public class ActionDto extends AbstractEntity {
	private String objectGuid, eventGuid, type, code, state, message;
	private UserDto user;

	public String getObjectGuid() {
		return objectGuid;
	}

	public void setObjectGuid(String objectGuid) {
		this.objectGuid = objectGuid;
	}

	public String getEventGuid() {
		return eventGuid;
	}

	public void setEventGuid(String eventGuid) {
		this.eventGuid = eventGuid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("ActionDto{");
		sb.append("id=").append(getId());
		sb.append(", objectGuid='").append(objectGuid).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", state='").append(state).append('\'');
		sb.append(", message='").append(message).append('\'');
		sb.append(", user=").append(user);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return message;
	}
}
