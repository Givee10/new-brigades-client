package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class RequestJournalDto extends AbstractEntity {
	private String status;
	private String problemCode;
	private String problemName;
	private String responsible;
	private String region;
	private String location;
	private String number;
	private String address;
	private String description;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime registrationDate;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDatePlan;
	@JsonProperty
	private Boolean isHasAttachment;
	private Boolean portal;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProblemCode() {
		return problemCode;
	}

	public void setProblemCode(String problemCode) {
		this.problemCode = problemCode;
	}

	public String getProblemName() {
		return problemName;
	}

	public void setProblemName(String problemName) {
		this.problemName = problemName;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(ZonedDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public Boolean getHasAttachment() {
		return isHasAttachment;
	}

	public void setHasAttachment(Boolean hasAttachment) {
		isHasAttachment = hasAttachment;
	}

	public Boolean getPortal() {
		return portal;
	}

	public void setPortal(Boolean portal) {
		this.portal = portal;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("RequestJournalDto{");
		sb.append(", id='").append(getId()).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", problemCode='").append(problemCode).append('\'');
		sb.append(", problemName='").append(problemName).append('\'');
		sb.append(", responsible='").append(responsible).append('\'');
		sb.append(", region='").append(region).append('\'');
		sb.append(", location='").append(location).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", registrationDate=").append(registrationDate);
		sb.append(", finishDatePlan=").append(finishDatePlan);
		sb.append(", isHasAttachment=").append(isHasAttachment);
		sb.append(", portal=").append(portal);
		sb.append('}');
		return sb.toString();
	}
}
