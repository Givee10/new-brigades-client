package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class WorkDto extends AbstractEntity {
	private String guid, number, description;
	private UserDto author, responsible, executor;
	private UserGroupDto executorGroup;
	private BrigadeDto brigade;
	private String location, status, stage, urgency, impact;
	private Number prior;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime registrationDate, startDate, startDatePlan, finishDate, finishDatePlan, closeDate;
	private String code, closeCode, organization;
	private Long idTypeWork, idBrigade, idPlan;
	private Boolean trafficOff, consumerOff;
	private String source, stausToir, address, updatedAddress;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public UserDto getAuthor() {
		return author;
	}

	public void setAuthor(UserDto author) {
		this.author = author;
	}

	public UserDto getResponsible() {
		return responsible;
	}

	public void setResponsible(UserDto responsible) {
		this.responsible = responsible;
	}

	public UserDto getExecutor() {
		return executor;
	}

	public void setExecutor(UserDto executor) {
		this.executor = executor;
	}

	public UserGroupDto getExecutorGroup() {
		return executorGroup;
	}

	public void setExecutorGroup(UserGroupDto executorGroup) {
		this.executorGroup = executorGroup;
	}

	public BrigadeDto getBrigade() {
		return brigade;
	}

	public void setBrigade(BrigadeDto brigade) {
		this.brigade = brigade;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public Number getPrior() {
		return prior;
	}

	public void setPrior(Number prior) {
		this.prior = prior;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(ZonedDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(ZonedDateTime finishDate) {
		this.finishDate = finishDate;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public ZonedDateTime getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(ZonedDateTime closeDate) {
		this.closeDate = closeDate;
	}

	public String getCloseCode() {
		return closeCode;
	}

	public void setCloseCode(String closeCode) {
		this.closeCode = closeCode;
	}

	public Long getIdTypeWork() {
		return idTypeWork;
	}

	public void setIdTypeWork(Long idTypeWork) {
		this.idTypeWork = idTypeWork;
	}

	public Long getIdBrigade() {
		return idBrigade;
	}

	public void setIdBrigade(Long idBrigade) {
		this.idBrigade = idBrigade;
	}

	public Long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getTrafficOff() {
		return trafficOff;
	}

	public void setTrafficOff(Boolean trafficOff) {
		this.trafficOff = trafficOff;
	}

	public Boolean getConsumerOff() {
		return consumerOff;
	}

	public void setConsumerOff(Boolean consumerOff) {
		this.consumerOff = consumerOff;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStausToir() {
		return stausToir;
	}

	public void setStausToir(String stausToir) {
		this.stausToir = stausToir;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUpdatedAddress() {
		return updatedAddress;
	}

	public void setUpdatedAddress(String updatedAddress) {
		this.updatedAddress = updatedAddress;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WorkDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", author=").append(author);
		sb.append(", responsible=").append(responsible);
		sb.append(", executor=").append(executor);
		sb.append(", executorGroup=").append(executorGroup);
		sb.append(", brigade=").append(brigade);
		sb.append(", location='").append(location).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", stage='").append(stage).append('\'');
		sb.append(", urgency='").append(urgency).append('\'');
		sb.append(", impact='").append(impact).append('\'');
		sb.append(", prior=").append(prior);
		sb.append(", registrationDate=").append(registrationDate);
		sb.append(", startDate=").append(startDate);
		sb.append(", startDatePlan=").append(startDatePlan);
		sb.append(", finishDate=").append(finishDate);
		sb.append(", finishDatePlan=").append(finishDatePlan);
		sb.append(", closeDate=").append(closeDate);
		sb.append(", code='").append(code).append('\'');
		sb.append(", closeCode='").append(closeCode).append('\'');
		sb.append(", idTypeWork=").append(idTypeWork);
		sb.append(", idBrigade=").append(idBrigade);
		sb.append(", trafficOff=").append(trafficOff);
		sb.append(", consumerOff=").append(consumerOff);
		sb.append(", source='").append(source).append('\'');
		sb.append(", stausToir='").append(stausToir).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", updatedAddress='").append(updatedAddress).append('\'');
		sb.append(", organization='").append(organization).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return number;
	}

	public enum Status {
		NEW("Новая"),
		ASSIGNED("Назначена"),
		IN_PROGRESS("Выполняется"),
		CLOSED("Закрыта в РВ"),
		COMPLETE("Завершена бригадой");

		private final String code;

		Status(String code) {
			this.code = code;
		}

		public boolean is(String code) {
			return code != null && this.code.equalsIgnoreCase(code);
		}

		public String getCode() {
			return code;
		}

		@Override
		public String toString() {
			return code;
		}

		public boolean isIn(Status... status) {
			for (Status s : status) {
				if (this.equals(s)) return true;
			}
			return false;
		}
	}
}
