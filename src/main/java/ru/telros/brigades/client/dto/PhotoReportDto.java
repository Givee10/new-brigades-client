package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PhotoReportDto  {
    private Long recordNumber;
    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date workDate;

    private String brigade;

    private String address;

    private String description;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date startDateFact;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date finishDateFact;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date photoDate;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date attachmentCreateDate;

    private String lostConnection;

    public Long getRecordNumber() {
        return recordNumber;
    }

    public void setRecordNumber(Long recordNumber) {
        this.recordNumber = recordNumber;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public String getBrigade() {
        return brigade;
    }

    public void setBrigade(String brigade) {
        this.brigade = brigade;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDateFact() {
        return startDateFact;
    }

    public void setStartDateFact(Date startDateFact) {
        this.startDateFact = startDateFact;
    }

    public Date getFinishDateFact() {
        return finishDateFact;
    }

    public void setFinishDateFact(Date finishDateFact) {
        this.finishDateFact = finishDateFact;
    }

    public Date getPhotoDate() {
        return photoDate;
    }

    public void setPhotoDate(Date photoDate) {
        this.photoDate = photoDate;
    }

    public Date getAttachmentCreateDate() {
        return attachmentCreateDate;
    }

    public void setAttachmentCreateDate(Date attachmentCreateDate) {
        this.attachmentCreateDate = attachmentCreateDate;
    }

    public String getLostConnection() {
        return lostConnection;
    }

    public void setLostConnection(String lostConnection) {
        this.lostConnection = lostConnection;
    }

    public String debugInfo() {
        return "PhotoReportDto{" +
                "recordNumber=" + recordNumber +
                ", workDate=" + workDate +
                ", brigade='" + brigade + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", startDateFact=" + startDateFact +
                ", finishDateFact=" + finishDateFact +
                ", photoDate=" + photoDate +
                ", attachmentCreateDate=" + attachmentCreateDate +
                ", lostConnection='" + lostConnection + '\'' +
                '}';
    }

}
