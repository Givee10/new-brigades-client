package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class AttachmentDto extends AbstractEntity {
	private String description;
	private String fileName;
	private String fileType;
	private String path;
	private String type;
	private String commentary;
	private Long size;
	private String gati;
	private String regulations;
	private String calendar;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime createDate;
	private String source;
	private String fullName;
	@JsonProperty
	private Boolean isMobile;
	private String state;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getGati() {
		return gati;
	}

	public void setGati(String gati) {
		this.gati = gati;
	}

	public String getRegulations() {
		return regulations;
	}

	public void setRegulations(String regulations) {
		this.regulations = regulations;
	}

	public String getCalendar() {
		return calendar;
	}

	public void setCalendar(String calendar) {
		this.calendar = calendar;
	}

	public ZonedDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(ZonedDateTime createDate) {
		this.createDate = createDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonIgnore
	public Boolean getMobile() {
		return isMobile;
	}

	public void setMobile(Boolean mobile) {
		isMobile = mobile;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("AttachmentDto{");
		sb.append("description='").append(description).append('\'');
		sb.append(", fileName='").append(fileName).append('\'');
		sb.append(", fileType='").append(fileType).append('\'');
		sb.append(", path='").append(path).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", commentary='").append(commentary).append('\'');
		sb.append(", size=").append(size);
		sb.append(", gati='").append(gati).append('\'');
		sb.append(", regulations='").append(regulations).append('\'');
		sb.append(", calendar='").append(calendar).append('\'');
		sb.append(", createDate=").append(createDate);
		sb.append(", source='").append(source).append('\'');
		sb.append(", fullName='").append(fullName).append('\'');
		sb.append(", isMobile=").append(isMobile);
		sb.append(", state='").append(state).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return fileName;
	}

	public enum Status {
		CANCEL("0 - Не отправлять в ГЛ"),
		SUBMIT("1 - Отправить в ГЛ"),
		SENT("2 - Отправлено в ГЛ");

		private final String code;

		Status(String code) {
			this.code = code;
		}

		public boolean is(String code) {
			return code != null && this.code.equalsIgnoreCase(code);
		}

		public String getCode() {
			return code;
		}

		@Override
		public String toString() {
			return code;
		}

		public boolean isIn(Status... status) {
			for (Status s : status) {
				if (this.equals(s)) return true;
			}
			return false;
		}
	}
}
