package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class PositionDto extends AbstractEntity {
	private String number;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime gmt;
	private String fNumber;
	private String model;
	private Float x;
	private Float y;
	private String condition;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public ZonedDateTime getGmt() {
		return gmt;
	}

	public void setGmt(ZonedDateTime gmt) {
		this.gmt = gmt;
	}

	public String getfNumber() {
		return fNumber;
	}

	public void setfNumber(String fNumber) {
		this.fNumber = fNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Float getX() {
		return x;
	}

	public void setX(Float x) {
		this.x = x;
	}

	public Float getY() {
		return y;
	}

	public void setY(Float y) {
		this.y = y;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("PositionDto{");
		sb.append("id=").append(getId());
		sb.append(", number='").append(number).append('\'');
		sb.append(", gmt=").append(gmt);
		sb.append(", fNumber='").append(fNumber).append('\'');
		sb.append(", model='").append(model).append('\'');
		sb.append(", x=").append(x);
		sb.append(", y=").append(y);
		sb.append(", condition='").append(condition).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return number;
	}
}
