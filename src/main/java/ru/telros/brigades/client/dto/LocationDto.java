package ru.telros.brigades.client.dto;

public class LocationDto extends AbstractEntity {
	private String name, description, code, type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("LocationDto{");
		sb.append("id=").append(getId());
		sb.append(", name='").append(name).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
