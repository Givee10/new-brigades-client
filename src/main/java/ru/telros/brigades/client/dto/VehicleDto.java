package ru.telros.brigades.client.dto;

public class VehicleDto extends AbstractEntity {
	private String guid, type, number, model;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("VehicleDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", model='").append(model).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return number;
	}
}
