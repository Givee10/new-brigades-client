package ru.telros.brigades.client.dto;

public class EquipmentTypeDto {
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}
}
