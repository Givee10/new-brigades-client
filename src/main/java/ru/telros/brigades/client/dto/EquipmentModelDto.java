package ru.telros.brigades.client.dto;

public class EquipmentModelDto {
	private String model;

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return model;
	}
}
