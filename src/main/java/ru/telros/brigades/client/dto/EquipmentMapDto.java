package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class EquipmentMapDto {
	private String type;
	private String model;
	private String number;
	private String state;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime lastUpdated;
	private GeocodeDto geocode;
	private String brigade;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ZonedDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(ZonedDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public GeocodeDto getGeocode() {
		return geocode;
	}

	public void setGeocode(GeocodeDto geocode) {
		this.geocode = geocode;
	}

	public String getBrigade() {
		return brigade;
	}

	public void setBrigade(String brigade) {
		this.brigade = brigade;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		EquipmentMapDto that = (EquipmentMapDto) o;

		return number != null ? number.equals(that.number) : that.number == null;
	}

	@Override
	public int hashCode() {
		return number != null ? number.hashCode() : 0;
	}
}
