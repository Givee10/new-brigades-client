package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class BrigadeJournalDto extends AbstractEntity {
	private String name;
	private String master;
	private String brigadier;
	private String phone;
	private String register;
	private String address;
	private String typeWork;
	private String timeWork;
	private String vehicle;
	private Boolean mavr;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime lastNotifyDate;
	private String shift;
	private String filial;
	private String territory;
	private String region;
	private Long idBrigadePlan;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public String getBrigadier() {
		return brigadier;
	}

	public void setBrigadier(String brigadier) {
		this.brigadier = brigadier;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTypeWork() {
		return typeWork;
	}

	public void setTypeWork(String typeWork) {
		this.typeWork = typeWork;
	}

	public String getTimeWork() {
		return timeWork;
	}

	public void setTimeWork(String timeWork) {
		this.timeWork = timeWork;
	}

	public String getVehicle() {
		return vehicle;
	}

	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}

	public Boolean getMavr() {
		return mavr;
	}

	public void setMavr(Boolean mavr) {
		this.mavr = mavr;
	}

	public ZonedDateTime getLastNotifyDate() {
		return lastNotifyDate;
	}

	public void setLastNotifyDate(ZonedDateTime lastNotifyDate) {
		this.lastNotifyDate = lastNotifyDate;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Long getIdBrigadePlan() {
		return idBrigadePlan;
	}

	public void setIdBrigadePlan(Long idBrigadePlan) {
		this.idBrigadePlan = idBrigadePlan;
	}
}
