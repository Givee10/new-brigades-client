package ru.telros.brigades.client.dto;

public class EquipmentDto extends AbstractEntity {
	private String guid;
	private String type;
	private String model;
	private String number;
	private String description;
	private String code;
	private String unit;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("EquipmentDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", model='").append(model).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", unit='").append(unit).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return type;
	}
}
