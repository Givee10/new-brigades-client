package ru.telros.brigades.client.dto;

import java.util.List;

public class PagesDto<T> {
	private List<T> currentContent;
	private Long count;

	public List<T> getCurrentContent() {
		return currentContent;
	}

	public void setCurrentContent(List<T> currentContent) {
		this.currentContent = currentContent;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}
