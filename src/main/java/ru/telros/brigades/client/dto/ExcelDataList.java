package ru.telros.brigades.client.dto;

import java.util.ArrayList;
import java.util.List;

public class ExcelDataList {
	private List<ExcelData> list = new ArrayList<>();

	public List<ExcelData> getList() {
		return list;
	}

	public void setList(List<ExcelData> list) {
		this.list = list;
	}
}
