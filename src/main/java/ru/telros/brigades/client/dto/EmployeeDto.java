package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EmployeeDto extends AbstractEntity {





	private Long idUser;
	private String lastName, firstName, middleName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("EmployeeDto{");
		sb.append("id=").append(getId());
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", firstName='").append(firstName).append('\'');
		sb.append(", middleName='").append(middleName).append('\'');
		sb.append(", idUser=").append(idUser);
		sb.append('}');
		return sb.toString();
	}

	@JsonIgnore
	public String getFullName() {
		return lastName + " " + firstName + " " + middleName;
	}

	@Override
	public String toString() {
		return getFullName();
	}
}
