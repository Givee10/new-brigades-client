package ru.telros.brigades.client.dto;

public class AddressDto {
	private String location, description;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("AddressDto{");
		sb.append("location='").append(location).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return description;
	}
}
