package ru.telros.brigades.client.dto;

public class GeocodeDto {
	private Double longitude, latitude;
	private String description;
	private String name;

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("GeocodeDto{");
		sb.append("longitude=").append(longitude);
		sb.append(", latitude=").append(latitude);
		sb.append(", description='").append(description).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		GeocodeDto that = (GeocodeDto) o;

		if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
		return latitude != null ? latitude.equals(that.latitude) : that.latitude == null;

	}

	@Override
	public int hashCode() {
		int result = longitude != null ? longitude.hashCode() : 0;
		result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "[" + longitude + ", " + latitude + "]";
	}
}
