package ru.telros.brigades.client.dto;

public class VehicleBrigadeDto extends AbstractEntity {
	private Long brigadeId;
	private Long vehicleId;
	private String type;
	private String number;

	public Long getBrigadeId() {
		return brigadeId;
	}

	public void setBrigadeId(Long brigadeId) {
		this.brigadeId = brigadeId;
	}

	public Long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("VehicleBrigadeDto{");
		sb.append("id=").append(getId());
		sb.append(", brigadeId=").append(brigadeId);
		sb.append(", vehicleId=").append(vehicleId);
		sb.append(", type='").append(type).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return number;
	}
}
