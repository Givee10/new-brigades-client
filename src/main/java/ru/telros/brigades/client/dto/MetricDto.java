package ru.telros.brigades.client.dto;

public class MetricDto extends AbstractEntity {
	private String wipEntityId;
	private String metricsCode;
	private String quantity;

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getMetricsCode() {
		return metricsCode;
	}

	public void setMetricsCode(String metricsCode) {
		this.metricsCode = metricsCode;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
