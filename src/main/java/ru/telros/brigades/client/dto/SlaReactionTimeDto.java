package ru.telros.brigades.client.dto;

public class SlaReactionTimeDto extends AbstractEntity {
	private String codeHl;
	private String timeWorkByRequestSou, timeWorkByWork;

	public String getCodeHl() {
		return codeHl;
	}

	public void setCodeHl(String codeHl) {
		this.codeHl = codeHl;
	}

	public String getTimeWorkByRequestSou() {
		return timeWorkByRequestSou;
	}

	public void setTimeWorkByRequestSou(String timeWorkByRequestSou) {
		this.timeWorkByRequestSou = timeWorkByRequestSou;
	}

	public String getTimeWorkByWork() {
		return timeWorkByWork;
	}

	public void setTimeWorkByWork(String timeWorkByWork) {
		this.timeWorkByWork = timeWorkByWork;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("SlaReactionTimeDto{");
		sb.append("id=").append(getId());
		sb.append(", codeHl='").append(codeHl).append('\'');
		sb.append(", timeWorkByRequestSou='").append(timeWorkByRequestSou).append('\'');
		sb.append(", timeWorkByWork='").append(timeWorkByWork).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return codeHl;
	}
}
