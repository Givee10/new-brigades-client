package ru.telros.brigades.client.dto;

public class ElementDto extends AbstractEntity {
    private Long idWork;
    private String type, name, code, length, lengthUnit, diameter, diameterUnit, material;

    public Long getIdWork() {
        return idWork;
    }

    public void setIdWork(Long idWork) {
        this.idWork = idWork;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getLengthUnit() {
        return lengthUnit;
    }

    public void setLengthUnit(String lengthUnit) {
        this.lengthUnit = lengthUnit;
    }

    public String getDiameter() {
        return diameter;
    }

    public void setDiameter(String diameter) {
        this.diameter = diameter;
    }

    public String getDiameterUnit() {
        return diameterUnit;
    }

    public void setDiameterUnit(String diameterUnit) {
        this.diameterUnit = diameterUnit;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
    public String debugInfo() {
        final StringBuffer sb = new StringBuffer("ElementDto{");
        sb.append("id=").append(getId());
        sb.append(", code=").append(getCode());
        sb.append(", diameter=").append(getDiameter());
        sb.append(", diameterUnit='").append(getDiameterUnit()).append('\'');
        sb.append(", idWork='").append(getIdWork()).append('\'');
        sb.append(", length='").append(getLength()).append('\'');
        sb.append(", lengthUnit='").append(getLengthUnit()).append('\'');
        sb.append(", material='").append(getMaterial()).append('\'');
        sb.append(", name='").append(getName()).append('\'');
        sb.append(", type='").append(getType()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
