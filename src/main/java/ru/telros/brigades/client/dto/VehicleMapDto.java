package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class VehicleMapDto {
	private String type;
	private String model;
	private String number;
	private String state;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime lastUpdated;
	private GeocodeDto geocode;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ZonedDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(ZonedDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public GeocodeDto getGeocode() {
		return geocode;
	}

	public void setGeocode(GeocodeDto geocode) {
		this.geocode = geocode;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("VehicleMapDto{");
		sb.append("type='").append(type).append('\'');
		sb.append(", model='").append(model).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", state='").append(state).append('\'');
		sb.append(", lastUpdated=").append(lastUpdated);
		sb.append(", geocode=").append(geocode);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return number;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof VehicleMapDto)) return false;

		VehicleMapDto that = (VehicleMapDto) o;

		if (type != null ? !type.equals(that.type) : that.type != null) return false;
		if (model != null ? !model.equals(that.model) : that.model != null) return false;
		return number.equals(that.number);
	}

	@Override
	public int hashCode() {
		int result = type != null ? type.hashCode() : 0;
		result = 31 * result + (model != null ? model.hashCode() : 0);
		result = 31 * result + number.hashCode();
		return result;
	}
}
