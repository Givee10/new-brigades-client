package ru.telros.brigades.client.dto;

public class InventoryWorkDto extends AbstractEntity {
	private String guid;
	private Long idWork;
	private String type;
	private String description;
	private String unit;
	private Double quantity;
	private String comments;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getIdWork() {
		return idWork;
	}

	public void setIdWork(Long idWork) {
		this.idWork = idWork;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("InventoryWorkDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", idWork=").append(idWork);
		sb.append(", type='").append(type).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", unit='").append(unit).append('\'');
		sb.append(", quantity=").append(quantity);
		sb.append(", comments='").append(comments).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return type;
	}
}
