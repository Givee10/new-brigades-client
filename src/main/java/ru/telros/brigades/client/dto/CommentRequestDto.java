package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class CommentRequestDto extends AbstractEntity {
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime createDate, changeDate;
	private String stage, content, note, source, registeredBy;

	public ZonedDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(ZonedDateTime createDate) {
		this.createDate = createDate;
	}

	public ZonedDateTime getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(ZonedDateTime changeDate) {
		this.changeDate = changeDate;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("CommentRequestDto{");
		sb.append("id=").append(getId());
		sb.append(", createDate=").append(createDate);
		sb.append(", changeDate=").append(changeDate);
		sb.append(", stage='").append(stage).append('\'');
		sb.append(", content='").append(content).append('\'');
		sb.append(", note='").append(note).append('\'');
		sb.append(", source='").append(source).append('\'');
		sb.append(", registeredBy='").append(registeredBy).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return content;
	}
}
