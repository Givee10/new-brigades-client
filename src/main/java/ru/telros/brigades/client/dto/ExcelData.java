package ru.telros.brigades.client.dto;

public class ExcelData {
	private String typeWork, address, equipment, number, service, comment, time, note, responsible;

	public String getTypeWork() {
		return typeWork;
	}

	public void setTypeWork(String typeWork) {
		this.typeWork = typeWork;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("ExcelData{");
		sb.append("typeWork='").append(typeWork).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", equipment='").append(equipment).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", service='").append(service).append('\'');
		sb.append(", comment='").append(comment).append('\'');
		sb.append(", time='").append(time).append('\'');
		sb.append(", note='").append(note).append('\'');
		sb.append(", responsible='").append(responsible).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
