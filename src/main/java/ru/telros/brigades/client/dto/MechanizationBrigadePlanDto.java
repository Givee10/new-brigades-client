package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class MechanizationBrigadePlanDto extends AbstractEntity {
	private String guid;
	private Long idBrigadePlan;
	private String type;
	private String model;
	private String number;
	private String description;
	private String code;
	private String unit;
	private Double quantity;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDateFact;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDatePlan;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDateFact;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDatePlan;
	private String comments;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getIdBrigadePlan() {
		return idBrigadePlan;
	}

	public void setIdBrigadePlan(Long idBrigadePlan) {
		this.idBrigadePlan = idBrigadePlan;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public ZonedDateTime getStartDateFact() {
		return startDateFact;
	}

	public void setStartDateFact(ZonedDateTime startDateFact) {
		this.startDateFact = startDateFact;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDateFact() {
		return finishDateFact;
	}

	public void setFinishDateFact(ZonedDateTime finishDateFact) {
		this.finishDateFact = finishDateFact;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("MechanizationBrigadePlanDto{");
		sb.append("guid='").append(guid).append('\'');
		sb.append(", idBrigadePlan=").append(idBrigadePlan);
		sb.append(", type='").append(type).append('\'');
		sb.append(", model='").append(model).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", unit='").append(unit).append('\'');
		sb.append(", quantity=").append(quantity);
		sb.append(", startDateFact=").append(startDateFact);
		sb.append(", startDatePlan=").append(startDatePlan);
		sb.append(", finishDateFact=").append(finishDateFact);
		sb.append(", finishDatePlan=").append(finishDatePlan);
		sb.append(", comments='").append(comments).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
