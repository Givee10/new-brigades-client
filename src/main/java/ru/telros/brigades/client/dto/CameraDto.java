package ru.telros.brigades.client.dto;

public class CameraDto extends AbstractEntity {
	//private Long ownerId, workId;
	private Long addressId, personId, workGroupId, regionId;
	private String connection, container, extChannel, localChannel, localIp;
	private String macAddress, name, status, stream, address;
	private String number, tripod, charger;

	public String getConnection() {
		return connection;
	}

	public void setConnection(String connection) {
		this.connection = connection;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public String getExtChannel() {
		return extChannel;
	}

	public void setExtChannel(String extChannel) {
		this.extChannel = extChannel;
	}

	public String getLocalChannel() {
		return localChannel;
	}

	public void setLocalChannel(String localChannel) {
		this.localChannel = localChannel;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTripod() {
		return tripod;
	}

	public void setTripod(String tripod) {
		this.tripod = tripod;
	}

	public String getCharger() {
		return charger;
	}

	public void setCharger(String charger) {
		this.charger = charger;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getWorkGroupId() {
		return workGroupId;
	}

	public void setWorkGroupId(Long workGroupId) {
		this.workGroupId = workGroupId;
	}

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("CameraDto{");
		sb.append("addressId=").append(addressId);
		sb.append(", personId=").append(personId);
		sb.append(", workGroupId=").append(workGroupId);
		sb.append(", regionId=").append(regionId);
		sb.append(", connection='").append(connection).append('\'');
		sb.append(", container='").append(container).append('\'');
		sb.append(", extChannel='").append(extChannel).append('\'');
		sb.append(", localChannel='").append(localChannel).append('\'');
		sb.append(", localIp='").append(localIp).append('\'');
		sb.append(", macAddress='").append(macAddress).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", stream='").append(stream).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", tripod='").append(tripod).append('\'');
		sb.append(", charger='").append(charger).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}

	public enum Connection {
		NOT_CONNECTED("НЕ ПОДКЛЮЧЕНА"), CONNECTED("ПОДКЛЮЧЕНА"), SYNCHRONIZING("СИНХРОНИЗАЦИЯ");

		private String displayName;

		Connection(final String displayName) {
			this.displayName = displayName;
		}

		public String getDisplayName() {
			return displayName;
		}

		@Override
		public String toString() {
			return displayName;
		}
	}

	public enum Status {
		UNAVAILABLE("ВЫКЛЮЧЕНА"), WORKING("ВКЛЮЧЕНА"), RESERVE("РЕЗЕРВ");

		private String displayName;

		Status(final String displayName) {
			this.displayName = displayName;
		}

		public String getDisplayName() {
			return displayName;
		}

		@Override
		public String toString() {
			return displayName;
		}
	}
}
