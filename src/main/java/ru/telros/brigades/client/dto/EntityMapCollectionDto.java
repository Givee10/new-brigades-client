package ru.telros.brigades.client.dto;

import java.util.ArrayList;
import java.util.List;

public class EntityMapCollectionDto {
	private List<WorkMapDto> works = new ArrayList<>();
	private List<RequestMapDto> requests = new ArrayList<>();
	private List<BrigadeMapDto> brigades = new ArrayList<>();
	private List<VehicleMapDto> vehicles = new ArrayList<>();
	private List<EquipmentMapDto> equipments = new ArrayList<>();

	public List<WorkMapDto> getWorks() {
		return works;
	}

	public void setWorks(List<WorkMapDto> works) {
		this.works = works;
	}

	public List<RequestMapDto> getRequests() {
		return requests;
	}

	public void setRequests(List<RequestMapDto> requests) {
		this.requests = requests;
	}

	public List<BrigadeMapDto> getBrigades() {
		return brigades;
	}

	public void setBrigades(List<BrigadeMapDto> brigades) {
		this.brigades = brigades;
	}

	public List<VehicleMapDto> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<VehicleMapDto> vehicles) {
		this.vehicles = vehicles;
	}

	public List<EquipmentMapDto> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<EquipmentMapDto> equipments) {
		this.equipments = equipments;
	}
}
