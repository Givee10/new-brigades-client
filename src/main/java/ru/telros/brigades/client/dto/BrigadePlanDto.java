package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class BrigadePlanDto extends AbstractEntity {
	private Long idBrigade;
	private String phone;
	private Boolean isMavr;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime lastNotifyDate;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime beginDate;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime endDate;

	public Long getIdBrigade() {
		return idBrigade;
	}

	public void setIdBrigade(Long idBrigade) {
		this.idBrigade = idBrigade;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getMavr() {
		return isMavr;
	}

	public void setMavr(Boolean mavr) {
		isMavr = mavr;
	}

	public ZonedDateTime getLastNotifyDate() {
		return lastNotifyDate;
	}

	public void setLastNotifyDate(ZonedDateTime lastNotifyDate) {
		this.lastNotifyDate = lastNotifyDate;
	}

	public ZonedDateTime getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(ZonedDateTime beginDate) {
		this.beginDate = beginDate;
	}

	public ZonedDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("BrigadePlanDto{");
		sb.append("id=").append(getId());
		sb.append(", idBrigade=").append(idBrigade);
		sb.append(", phone='").append(phone).append('\'');
		sb.append(", isMavr=").append(isMavr);
		sb.append(", lastNotifyDate=").append(lastNotifyDate);
		sb.append(", beginDate=").append(beginDate);
		sb.append(", endDate=").append(endDate);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return idBrigade.toString();
	}
}
