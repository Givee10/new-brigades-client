package ru.telros.brigades.client.dto;

public class TypeWorkDto extends AbstractEntity {
	private String guid, code, description, numberToir;
	private Double norm;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getNorm() {
		return norm;
	}

	public void setNorm(Double norm) {
		this.norm = norm;
	}

	public String getNumberToir() {
		return numberToir;
	}

	public void setNumberToir(String numberToir) {
		this.numberToir = numberToir;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("TypeWorkDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", norm=").append(norm);
		sb.append(", numberToir=").append(numberToir);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return description;
	}
}
