package ru.telros.brigades.client.dto;

public class SourceDto extends AbstractEntity {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("SourceDto{");
		sb.append("id=").append(getId());
		sb.append(", name='").append(name).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
