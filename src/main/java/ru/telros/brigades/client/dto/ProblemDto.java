package ru.telros.brigades.client.dto;

public class ProblemDto extends AbstractEntity {
	private String name, code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String debugInfo() {
		return "ProblemDto{" +
				"name='" + name + '\'' +
				", code='" + code + '\'' +
				'}';
	}

	@Override
	public String toString() {
		return getId() + ". " + name;
	}
}
