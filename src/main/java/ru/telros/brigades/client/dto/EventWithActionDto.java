package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class EventWithActionDto extends AbstractEntity {
	private String guid;
	private String objectGuid;
	private String source;
	private String type;
	private String address;
	private String message;
	private Integer level;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime date;
	private Long idAction;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime lastChangeDateAction;
	private String lastStateAction;
	private UserDto user;
	private UserDto actionUser;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime fromHlDate;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getObjectGuid() {
		return objectGuid;
	}

	public void setObjectGuid(String objectGuid) {
		this.objectGuid = objectGuid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getIdAction() {
		return idAction;
	}

	public void setIdAction(Long idAction) {
		this.idAction = idAction;
	}

	public ZonedDateTime getLastChangeDateAction() {
		return lastChangeDateAction;
	}

	public void setLastChangeDateAction(ZonedDateTime lastChangeDateAction) {
		this.lastChangeDateAction = lastChangeDateAction;
	}

	public String getLastStateAction() {
		return lastStateAction;
	}

	public void setLastStateAction(String lastStateAction) {
		this.lastStateAction = lastStateAction;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public UserDto getActionUser() {
		return actionUser;
	}

	public void setActionUser(UserDto actionUser) {
		this.actionUser = actionUser;
	}

	public ZonedDateTime getFromHlDate() {
		return fromHlDate;
	}

	public void setFromHlDate(ZonedDateTime fromHlDate) {
		this.fromHlDate = fromHlDate;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("EventWithActionDto{");
		sb.append("guid='").append(guid).append('\'');
		sb.append(", objectGuid='").append(objectGuid).append('\'');
		sb.append(", source='").append(source).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", message='").append(message).append('\'');
		sb.append(", date=").append(date);
		sb.append(", level=").append(level);
		sb.append(", idAction=").append(idAction);
		sb.append(", lastChangeDateAction=").append(lastChangeDateAction);
		sb.append(", lastStateAction='").append(lastStateAction).append('\'');
		sb.append(", user=").append(user);
		sb.append(", actionUser=").append(actionUser);
		sb.append(", fromHlDate=").append(fromHlDate);
		sb.append(", id=").append(getId());
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return message;
	}
}
