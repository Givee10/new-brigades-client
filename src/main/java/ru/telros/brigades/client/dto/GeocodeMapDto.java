package ru.telros.brigades.client.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeocodeMapDto<T extends AbstractEntity> {
	private Map<GeocodeDto, List<T>> geocodeMap = new HashMap<>();

	public Map<GeocodeDto, List<T>> getGeocodeMap() {
		return geocodeMap;
	}

	public void setGeocodeMap(Map<GeocodeDto, List<T>> geocodeMap) {
		this.geocodeMap = geocodeMap;
	}

	public void addGeocodeValue(GeocodeDto key, T value) {
		if (geocodeMap.containsKey(key)) {
			List<T> list = geocodeMap.get(key);
			if (!list.contains(value)) list.add(value);
			geocodeMap.put(key, list);
		} else {
			List<T> list = new ArrayList<>();
			list.add(value);
			geocodeMap.put(key, list);
		}
	}

	public List<T> getGeocodeValues(GeocodeDto key) {
		return geocodeMap.getOrDefault(key, new ArrayList<>());
	}
}
