package ru.telros.brigades.client.dto;

public class EquipmentNumberDto {
	private String number;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return number;
	}
}
