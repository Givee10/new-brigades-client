package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class WorkJournalDto extends AbstractEntity {
	private String status;
	private String source;
	private String problemCode;
	private String type;
	private String description;
	private String address;
	private String brigade;
	private String region;
	private String location;
	@JsonProperty
	private Boolean isHasAttachment;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDatePlan;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDatePlan;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDateFact;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDateFact;
	private String timeout;
	private String linked;
	private String numberRequest;
	private String numberToir;
	private String numberAis;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime registrationDate;
	@JsonProperty
	private Boolean isHasEquipment;
	@JsonProperty
	private Boolean isConsumerOff;
	@JsonProperty
	private Boolean isGati;
	@JsonProperty
	private Boolean isScheduled;
	@JsonProperty
	private Boolean isRegulated;
	@JsonProperty
	private Boolean isTrafficOff;
	@JsonProperty
	private Boolean isMavr;
	@JsonProperty
	private Boolean isCreatedByBrigade;
	@JsonProperty
	private Boolean isBrigadeOnAddress;
	private String organization;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getProblemCode() {
		return problemCode;
	}

	public void setProblemCode(String problemCode) {
		this.problemCode = problemCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBrigade() {
		return brigade;
	}

	public void setBrigade(String brigade) {
		this.brigade = brigade;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public ZonedDateTime getStartDateFact() {
		return startDateFact;
	}

	public void setStartDateFact(ZonedDateTime startDateFact) {
		this.startDateFact = startDateFact;
	}

	public ZonedDateTime getFinishDateFact() {
		return finishDateFact;
	}

	public void setFinishDateFact(ZonedDateTime finishDateFact) {
		this.finishDateFact = finishDateFact;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public String getLinked() {
		return linked;
	}

	public void setLinked(String linked) {
		this.linked = linked;
	}

	public String getNumberRequest() {
		return numberRequest;
	}

	public void setNumberRequest(String numberRequest) {
		this.numberRequest = numberRequest;
	}

	public String getNumberToir() {
		return numberToir;
	}

	public void setNumberToir(String numberToir) {
		this.numberToir = numberToir;
	}

	public String getNumberAis() {
		return numberAis;
	}

	public void setNumberAis(String numberAis) {
		this.numberAis = numberAis;
	}

	public ZonedDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(ZonedDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Boolean getHasAttachment() {
		return isHasAttachment;
	}

	public void setHasAttachment(Boolean hasAttachment) {
		isHasAttachment = hasAttachment;
	}

	public Boolean getHasEquipment() {
		return isHasEquipment;
	}

	public void setHasEquipment(Boolean hasEquipment) {
		isHasEquipment = hasEquipment;
	}

	public Boolean getConsumerOff() {
		return isConsumerOff;
	}

	public void setConsumerOff(Boolean consumerOff) {
		isConsumerOff = consumerOff;
	}

	public Boolean getGati() {
		return isGati;
	}

	public void setGati(Boolean gati) {
		isGati = gati;
	}

	public Boolean getScheduled() {
		return isScheduled;
	}

	public void setScheduled(Boolean scheduled) {
		isScheduled = scheduled;
	}

	public Boolean getRegulated() {
		return isRegulated;
	}

	public void setRegulated(Boolean regulated) {
		isRegulated = regulated;
	}

	public Boolean getTrafficOff() {
		return isTrafficOff;
	}

	public void setTrafficOff(Boolean trafficOff) {
		isTrafficOff = trafficOff;
	}

	public Boolean getMavr() {
		return isMavr;
	}

	public void setMavr(Boolean mavr) {
		isMavr = mavr;
	}

	public Boolean getCreatedByBrigade() {
		return isCreatedByBrigade;
	}

	public void setCreatedByBrigade(Boolean createdByBrigade) {
		isCreatedByBrigade = createdByBrigade;
	}

	public Boolean getBrigadeOnAddress() {
		return isBrigadeOnAddress;
	}

	public void setBrigadeOnAddress(Boolean brigadeOnAddress) {
		isBrigadeOnAddress = brigadeOnAddress;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("WorkJournalDto{");
		sb.append("id=").append(getId());
		sb.append(", source='").append(source).append('\'');
		sb.append(", problemCode='").append(problemCode).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", brigade='").append(brigade).append('\'');
		sb.append(", region='").append(region).append('\'');
		sb.append(", location='").append(location).append('\'');
		sb.append(", isHasAttachment=").append(isHasAttachment);
		sb.append(", startDatePlan=").append(startDatePlan);
		sb.append(", finishDatePlan=").append(finishDatePlan);
		sb.append(", startDateFact=").append(startDateFact);
		sb.append(", finishDateFact=").append(finishDateFact);
		sb.append(", timeout='").append(timeout).append('\'');
		sb.append(", linked='").append(linked).append('\'');
		sb.append(", numberRequest='").append(numberRequest).append('\'');
		sb.append(", numberToir='").append(numberToir).append('\'');
		sb.append(", numberAis='").append(numberAis).append('\'');
		sb.append(", registrationDate=").append(registrationDate);
		sb.append(", isHasEquipment=").append(isHasEquipment);
		sb.append(", isConsumerOff=").append(isConsumerOff);
		sb.append(", isGati=").append(isGati);
		sb.append(", isScheduled=").append(isScheduled);
		sb.append(", isRegulated=").append(isRegulated);
		sb.append(", isTrafficOff=").append(isTrafficOff);
		sb.append(", isMavr=").append(isMavr);
		sb.append(", isCreatedByBrigade=").append(isCreatedByBrigade);
		sb.append(", isBrigadeOnAddress=").append(isBrigadeOnAddress);
		sb.append(", organization='").append(organization).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
