package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class RequestDto extends AbstractEntity {
	private Long parentId;
	private String topic, description, info;
	private String guid, number;

	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime incidentDate, registrationDate, openDate, changeDate;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDate, finishDate, closeDate, createDate, startDatePlan, finishDatePlan;

	private String operator, company, district, urgency, problem, statusHL;

	private String feedback, organization, responsible, repeat, phone;
	private String author, executor, executorGroup, initiator, owner, status;
	private String source, address, updatedAddress, type, city, location, flat;

	//------------------------------
	private String externalNumber, accessNumber, ownership, problemCreate, registeredBy;
	private String additionalInfo, category, closeCode, impact, prior, problemCode, portal, commentPortal;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public ZonedDateTime getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(ZonedDateTime incidentDate) {
		this.incidentDate = incidentDate;
	}

	public ZonedDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(ZonedDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public ZonedDateTime getOpenDate() {
		return openDate;
	}

	public void setOpenDate(ZonedDateTime openDate) {
		this.openDate = openDate;
	}

	public ZonedDateTime getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(ZonedDateTime changeDate) {
		this.changeDate = changeDate;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(ZonedDateTime finishDate) {
		this.finishDate = finishDate;
	}

	public ZonedDateTime getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(ZonedDateTime closeDate) {
		this.closeDate = closeDate;
	}

	public ZonedDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(ZonedDateTime createDate) {
		this.createDate = createDate;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusHL() {
		return statusHL;
	}

	public void setStatusHL(String statusHL) {
		this.statusHL = statusHL;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUpdatedAddress() {
		return updatedAddress;
	}

	public void setUpdatedAddress(String updatedAddress) {
		this.updatedAddress = updatedAddress;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExternalNumber() {
		return externalNumber;
	}

	public void setExternalNumber(String externalNumber) {
		this.externalNumber = externalNumber;
	}

	public String getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(String accessNumber) {
		this.accessNumber = accessNumber;
	}

	public String getOwnership() {
		return ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getProblemCreate() {
		return problemCreate;
	}

	public void setProblemCreate(String problemCreate) {
		this.problemCreate = problemCreate;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getProblemCode() {
		return problemCode;
	}

	public void setProblemCode(String problemCode) {
		this.problemCode = problemCode;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public String getCommentPortal() {
		return commentPortal;
	}

	public void setCommentPortal(String commentPortal) {
		this.commentPortal = commentPortal;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getExecutor() {
		return executor;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	public String getExecutorGroup() {
		return executorGroup;
	}

	public void setExecutorGroup(String executorGroup) {
		this.executorGroup = executorGroup;
	}

	public String getInitiator() {
		return initiator;
	}

	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFlat() {
		return flat;
	}

	public void setFlat(String flat) {
		this.flat = flat;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCloseCode() {
		return closeCode;
	}

	public void setCloseCode(String closeCode) {
		this.closeCode = closeCode;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getPrior() {
		return prior;
	}

	public void setPrior(String prior) {
		this.prior = prior;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("RequestDto{");
		sb.append("id=").append(getId());
		sb.append(", parentId=").append(parentId);
		sb.append(", topic='").append(topic).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", info='").append(info).append('\'');
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", incidentDate=").append(incidentDate);
		sb.append(", registrationDate=").append(registrationDate);
		sb.append(", openDate=").append(openDate);
		sb.append(", changeDate=").append(changeDate);
		sb.append(", startDate=").append(startDate);
		sb.append(", finishDate=").append(finishDate);
		sb.append(", closeDate=").append(closeDate);
		sb.append(", createDate=").append(createDate);
		sb.append(", startDatePlan=").append(startDatePlan);
		sb.append(", finishDatePlan=").append(finishDatePlan);
		sb.append(", operator='").append(operator).append('\'');
		sb.append(", company='").append(company).append('\'');
		sb.append(", district='").append(district).append('\'');
		sb.append(", urgency='").append(urgency).append('\'');
		sb.append(", problem='").append(problem).append('\'');
		sb.append(", feedback='").append(feedback).append('\'');
		sb.append(", organization='").append(organization).append('\'');
		sb.append(", responsible='").append(responsible).append('\'');
		sb.append(", repeat='").append(repeat).append('\'');
		sb.append(", phone='").append(phone).append('\'');
		sb.append(", author='").append(author).append('\'');
		sb.append(", executor='").append(executor).append('\'');
		sb.append(", executorGroup='").append(executorGroup).append('\'');
		sb.append(", initiator='").append(initiator).append('\'');
		sb.append(", owner='").append(owner).append('\'');
		sb.append(", statusHL='").append(statusHL).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", source='").append(source).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", updatedAddress='").append(updatedAddress).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", city='").append(city).append('\'');
		sb.append(", location='").append(location).append('\'');
		sb.append(", flat='").append(flat).append('\'');
		sb.append(", externalNumber='").append(externalNumber).append('\'');
		sb.append(", accessNumber='").append(accessNumber).append('\'');
		sb.append(", ownership='").append(ownership).append('\'');
		sb.append(", problemCreate='").append(problemCreate).append('\'');
		sb.append(", registeredBy='").append(registeredBy).append('\'');
		sb.append(", additionalInfo='").append(additionalInfo).append('\'');
		sb.append(", category='").append(category).append('\'');
		sb.append(", closeCode='").append(closeCode).append('\'');
		sb.append(", impact='").append(impact).append('\'');
		sb.append(", prior='").append(prior).append('\'');
		sb.append(", problemCode='").append(problemCode).append('\'');
		sb.append(", portal='").append(portal).append('\'');
		sb.append(", commentPortal='").append(commentPortal).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		if (externalNumber != null)
			return externalNumber + ". " + address;
		else if (problemCode != null)
			return problemCode + ". " + problem + ", " + address;
		else
			return address + ", " + description;
	}

	public enum Status {
		NEW("Новая"),
		IN_WORK("В работе"),
		CLOSED("Закрытая");

		private final String code;

		Status(String code) {
			this.code = code;
		}

		public boolean is(String code) {
			return code != null && this.code.equalsIgnoreCase(code);
		}

		public String getCode() {
			return code;
		}

		@Override
		public String toString() {
			return code;
		}

		public boolean isIn(Status... status) {
			for (Status s : status) {
				if (this.equals(s)) return true;
			}
			return false;
		}
	}
}
