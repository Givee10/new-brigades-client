package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkCalcNormDto {
	@JsonProperty("WIP_ENTITY_ID")
	private String wipEntityId;
	@JsonProperty("RESOURCE_CODE")
	private String resourceCode;
	@JsonProperty("CALC_NORM")
	private String calcNorm;

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	public String getCalcNorm() {
		return calcNorm;
	}

	public void setCalcNorm(String calcNorm) {
		this.calcNorm = calcNorm;
	}
}
