package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.tltv.gantt.client.shared.Step;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class GanttItemDto {
	private Long id;
	private String name;
	private String caption;
	private String description;
	private String status;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDatePlan;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDatePlan;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDateFact;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDateFact;
	private List<GanttItemDto> children;
	@JsonIgnore
	private Step step;
	@JsonIgnore
	private Integer stepIndex;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ZonedDateTime getStartDatePlan() {
		return startDatePlan;
	}

	public void setStartDatePlan(ZonedDateTime startDatePlan) {
		this.startDatePlan = startDatePlan;
	}

	public ZonedDateTime getFinishDatePlan() {
		return finishDatePlan;
	}

	public void setFinishDatePlan(ZonedDateTime finishDatePlan) {
		this.finishDatePlan = finishDatePlan;
	}

	public ZonedDateTime getStartDateFact() {
		return startDateFact;
	}

	public void setStartDateFact(ZonedDateTime startDateFact) {
		this.startDateFact = startDateFact;
	}

	public ZonedDateTime getFinishDateFact() {
		return finishDateFact;
	}

	public void setFinishDateFact(ZonedDateTime finishDateFact) {
		this.finishDateFact = finishDateFact;
	}

	public List<GanttItemDto> getChildren() {
		return children;
	}

	public void setChildren(List<GanttItemDto> children) {
		this.children = children;
	}

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public Integer getStepIndex() {
		return stepIndex;
	}

	public void setStepIndex(Integer stepIndex) {
		this.stepIndex = stepIndex;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof GanttItemDto)) return false;
		GanttItemDto that = (GanttItemDto) o;
		return getId().equals(that.getId()) &&
				Objects.equals(getName(), that.getName()) &&
				Objects.equals(getCaption(), that.getCaption()) &&
				Objects.equals(getDescription(), that.getDescription());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getName(), getCaption(), getDescription());
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("GanttItemDto{");
		sb.append("name='").append(name).append('\'');
		sb.append(", caption='").append(caption).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", startDatePlan=").append(startDatePlan);
		sb.append(", finishDatePlan=").append(finishDatePlan);
		sb.append(", children=").append(children);
		sb.append(", step=").append(step);
		sb.append(", stepIndex=").append(stepIndex);
		sb.append('}');
		return sb.toString();
	}
}
