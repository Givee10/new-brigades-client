package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class BrigadeMapDto extends AbstractEntity {
	//private Long id;
	private String name;
	private String address;
	private GeocodeDto geocode;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDatePlanWork;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDatePlanWork;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime startDateFactWork;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime finishDateFactWork;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public GeocodeDto getGeocode() {
		return geocode;
	}

	public void setGeocode(GeocodeDto geocode) {
		this.geocode = geocode;
	}

	public ZonedDateTime getStartDatePlanWork() {
		return startDatePlanWork;
	}

	public void setStartDatePlanWork(ZonedDateTime startDatePlanWork) {
		this.startDatePlanWork = startDatePlanWork;
	}

	public ZonedDateTime getFinishDatePlanWork() {
		return finishDatePlanWork;
	}

	public void setFinishDatePlanWork(ZonedDateTime finishDatePlanWork) {
		this.finishDatePlanWork = finishDatePlanWork;
	}

	public ZonedDateTime getStartDateFactWork() {
		return startDateFactWork;
	}

	public void setStartDateFactWork(ZonedDateTime startDateFactWork) {
		this.startDateFactWork = startDateFactWork;
	}

	public ZonedDateTime getFinishDateFactWork() {
		return finishDateFactWork;
	}

	public void setFinishDateFactWork(ZonedDateTime finishDateFactWork) {
		this.finishDateFactWork = finishDateFactWork;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("BrigadeMapDto{");
		sb.append("id=").append(getId());
		sb.append(", name='").append(name).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", geocode=").append(geocode);
		sb.append(", startDatePlanWork=").append(startDatePlanWork);
		sb.append(", finishDatePlanWork=").append(finishDatePlanWork);
		sb.append(", startDateFactWork=").append(startDateFactWork);
		sb.append(", finishDateFactWork=").append(finishDateFactWork);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
