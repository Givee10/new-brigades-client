package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeBrigadeDto extends AbstractEntity {
	private Long idBrigade, idEmployee;
	private String lastName, firstName, middleName, post, phone, absenceReason;
	@JsonProperty
	private Boolean isManager, isBrigadier;

	public Long getIdBrigade() {
		return idBrigade;
	}

	public void setIdBrigade(Long idBrigade) {
		this.idBrigade = idBrigade;
	}

	public Long getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Long idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAbsenceReason() {
		return absenceReason;
	}

	public void setAbsenceReason(String absenceReason) {
		this.absenceReason = absenceReason;
	}

	@JsonIgnore
	public Boolean getManager() {
		return isManager;
	}

	public void setManager(Boolean manager) {
		isManager = manager;
	}

	@JsonIgnore
	public Boolean getBrigadier() {
		return isBrigadier;
	}

	public void setBrigadier(Boolean brigadier) {
		isBrigadier = brigadier;
	}

	@JsonIgnore
	public String getFullName() {
		return lastName + " " + firstName + " " + middleName;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("EmployeeBrigadeDto{");
		sb.append("id=").append(getId());
		sb.append(", idBrigade=").append(idBrigade);
		sb.append(", idEmployee=").append(idEmployee);
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", firstName='").append(firstName).append('\'');
		sb.append(", middleName='").append(middleName).append('\'');
		sb.append(", post='").append(post).append('\'');
		sb.append(", phone='").append(phone).append('\'');
		sb.append(", absenceReason='").append(absenceReason).append('\'');
		sb.append(", isManager=").append(isManager);
		sb.append(", isBrigadier=").append(isBrigadier);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return getFullName();
	}
}
