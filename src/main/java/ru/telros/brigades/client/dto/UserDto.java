package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

public class UserDto extends AbstractEntity {
	private String firstName, middleName, lastName, login, email, password;
	private boolean blocked;
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password){
		this.password = password;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("UserDto{");
		sb.append("id=").append(getId());
		sb.append(", firstName='").append(firstName).append('\'');
		sb.append(", middleName='").append(middleName).append('\'');
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", login='").append(login).append('\'');
		sb.append(", email='").append(email).append('\'');
		sb.append(", blocked='").append(blocked).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof UserDto)) return false;
		if (!super.equals(o)) return false;
		UserDto userDto = (UserDto) o;
		return Objects.equals(getFirstName(), userDto.getFirstName()) &&
				Objects.equals(getMiddleName(), userDto.getMiddleName()) &&
				Objects.equals(getLastName(), userDto.getLastName()) &&
				getLogin().equals(userDto.getLogin()) &&
				Objects.equals(getEmail(), userDto.getEmail());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getFirstName(), getMiddleName(), getLastName(), getLogin(), getEmail());
	}

	@JsonIgnore
	public String getFullName() {
		return lastName + " " + firstName + " " + middleName;
	}

	@Override
	public String toString() {
		return getFullName();
	}
}
