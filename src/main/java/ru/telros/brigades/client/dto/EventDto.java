package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class EventDto {
	private String guid, object, message, type, source, address;
	private Integer level;
	private Long idUser;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime date;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("EventDto{");
		sb.append("guid='").append(guid).append('\'');
		sb.append(", object='").append(object).append('\'');
		sb.append(", message='").append(message).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", source='").append(source).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", level=").append(level);
		sb.append(", date=").append(date);
		sb.append(", idUser=").append(idUser);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return message;
	}
}
