package ru.telros.brigades.client.dto;

import java.util.HashMap;
import java.util.Map;

public class SettingsMapDto {
	private Map<String, String> settingsMap = new HashMap<>();

	public Map<String, String> getSettingsMap() {
		return settingsMap;
	}

	public void setSettingsMap(Map<String, String> settingsMap) {
		this.settingsMap = settingsMap;
	}

	public void addSettings(String key, String value) {
		if (settingsMap.containsKey(key)) {
			settingsMap.replace(key, value);
		} else {
			settingsMap.put(key, value);
		}
	}

	public String getSettingValue(String key) {
		return settingsMap.getOrDefault(key, null);
	}
}
