package ru.telros.brigades.client.dto;

public class AssetDto extends AbstractEntity {
	private String guid, name, code, version, state;
	private AssetTypeDto type;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AssetTypeDto getType() {
		return type;
	}

	public void setType(AssetTypeDto type) {
		this.type = type;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("AssetDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", type=").append(type);
		sb.append(", version='").append(version).append('\'');
		sb.append(", state='").append(state).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
