package ru.telros.brigades.client.dto;


public class BrigadeWorkPriorityDto extends AbstractEntity {
    private String idCodeHl;
    private int YOR;
    private int LY;
    private int BTO;
    private int flying;
    private int linear;

    public BrigadeWorkPriorityDto(Long id, String idCodeHl, int YOR, int LY, int BTO, int flying, int linear) {
        setId(id);
        this.idCodeHl = idCodeHl;
        this.YOR = YOR;
        this.LY = LY;
        this.BTO = BTO;
        this.flying = flying;
        this.linear = linear;
    }



    public String getIdCodeHl() {
        return idCodeHl;
    }

    public void setIdCodeHl(String idCodeHl) {
        this.idCodeHl = idCodeHl;
    }

    public int getYOR() {
        return YOR;
    }

    public void setYOR(int YOR) {
        this.YOR = YOR;
    }

    public int getLY() {
        return LY;
    }

    public void setLY(int LY) {
        this.LY = LY;
    }

    public int getBTO() {
        return BTO;
    }

    public void setBTO(int BTO) {
        this.BTO = BTO;
    }

    public int getFlying() {
        return flying;
    }

    public void setFlying(int flying) {
        this.flying = flying;
    }

    public int getLinear() {
        return linear;
    }

    public void setLinear(int linear) {
        this.linear = linear;
    }
    public String debugInfo() {
        final StringBuffer sb = new StringBuffer("BrigadeWorkPriorityDto{");
        sb.append(", id='").append(getId()).append('\'');
        sb.append(", idCodeHl='").append(idCodeHl).append('\'');
        sb.append(", YOR='").append(YOR).append('\'');
        sb.append(", LY='").append(LY).append('\'');
        sb.append(", BTO='").append(BTO).append('\'');
        sb.append(", flying='").append(flying).append('\'');
        sb.append(", linear='").append(linear).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
