package ru.telros.brigades.client.dto;

public class RequestMapDto extends AbstractEntity {
	private String problemCode;
	private String problemName;
	private String address;
	private String description;
	private String status;
	private GeocodeDto geocode;

	public String getProblemCode() {
		return problemCode;
	}

	public void setProblemCode(String problemCode) {
		this.problemCode = problemCode;
	}

	public String getProblemName() {
		return problemName;
	}

	public void setProblemName(String problemName) {
		this.problemName = problemName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public GeocodeDto getGeocode() {
		return geocode;
	}

	public void setGeocode(GeocodeDto geocode) {
		this.geocode = geocode;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("RequestMapDto{");
		sb.append("id=").append(getId());
		sb.append(", problemCode='").append(problemCode).append('\'');
		sb.append(", problemName='").append(problemName).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", geocode=").append(geocode);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return problemCode + ". " + problemName + ", " + address;
	}
}
