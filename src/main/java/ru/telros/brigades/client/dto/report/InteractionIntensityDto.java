package ru.telros.brigades.client.dto.report;

import ru.telros.brigades.client.dto.EventDto;

public class InteractionIntensityDto {
    EventDto event;
    public String user;
    public String role;

    public EventDto getEvent() {
        return event;
    }

    public void setEvent(EventDto event) {
        this.event = event;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
