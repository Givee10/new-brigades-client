package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class BrigadeDto extends AbstractEntity {
	private String description;
	private String name;
	private String location;
	private String phone;
	@JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
	private ZonedDateTime beginWork, endWork, lastNotifyDate;
	@JsonProperty
	private Boolean isMavr;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public ZonedDateTime getBeginWork() {
		return beginWork;
	}

	public void setBeginWork(ZonedDateTime beginWork) {
		this.beginWork = beginWork;
	}

	public ZonedDateTime getEndWork() {
		return endWork;
	}

	public void setEndWork(ZonedDateTime endWork) {
		this.endWork = endWork;
	}

	public ZonedDateTime getLastNotifyDate() {
		return lastNotifyDate;
	}

	public void setLastNotifyDate(ZonedDateTime lastNotifyDate) {
		this.lastNotifyDate = lastNotifyDate;
	}

	@JsonIgnore
	public Boolean getMavr() {
		return isMavr;
	}

	public void setMavr(Boolean mavr) {
		isMavr = mavr;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("BrigadeDto{");
		sb.append("id=").append(getId());
		sb.append(", description='").append(description).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", location='").append(location).append('\'');
		sb.append(", phone='").append(phone).append('\'');
		sb.append(", beginWork=").append(beginWork);
		sb.append(", endWork=").append(endWork);
		sb.append(", lastNotifyDate=").append(lastNotifyDate);
		sb.append(", isMavr=").append(isMavr);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
