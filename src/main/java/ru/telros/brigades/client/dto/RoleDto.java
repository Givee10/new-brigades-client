package ru.telros.brigades.client.dto;

import java.util.Objects;

public class RoleDto extends AbstractEntity {

	private String name;
	private String description;
	public RoleDto(){}
	public RoleDto(Long id, String name, String description) {
		this.setId(id);
		this.name = name;
		this.description = description;
	}
	public RoleDto(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		RoleDto roleDto = (RoleDto) o;
		if (roleDto.getDescription().equals(getDescription())&& getName().equals(roleDto.getName())){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), getDescription());
	}
	/*Роли в системе:
	Мастер бригады
	Старший мастер РВ
	Руководитель РВ
	Диспетчер СОУ
	Руководитель СОУ
	Администратор

	Гость*/
	public String debugInfo(){
		return String.format("'%s','%s'",getName(),getDescription());
	}
}
