package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class UserWorksReportDto {
    private Long recordNumber;
    private Long idUser;
    private String user;
    private String role;
    private Long idEvent;
    private String eventGuid;
    private String objectGuid;
    private String source;
    private String type;
    private String address;
    private String message;
    private String eventIdUser;
    private String level;
    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date fromHlDate;
    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Date createDate;
    private Long idAction;
    private String actionLastState;
    private String actionIdUser;

    public Long getRecordNumber() {
        return recordNumber;
    }

    public void setRecordNumber(Long recordNumber) {
        this.recordNumber = recordNumber;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Long idEvent) {
        this.idEvent = idEvent;
    }

    public String getEventGuid() {
        return eventGuid;
    }

    public void setEventGuid(String eventGuid) {
        this.eventGuid = eventGuid;
    }

    public String getObjectGuid() {
        return objectGuid;
    }

    public void setObjectGuid(String objectGuid) {
        this.objectGuid = objectGuid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEventIdUser() {
        return eventIdUser;
    }

    public void setEventIdUser(String eventIdUser) {
        this.eventIdUser = eventIdUser;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getFromHlDate() {
        return fromHlDate;
    }

    public void setFromHlDate(Date fromHlDate) {
        this.fromHlDate = fromHlDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getIdAction() {
        return idAction;
    }

    public void setIdAction(Long idAction) {
        this.idAction = idAction;
    }

    public String getActionLastState() {
        return actionLastState;
    }

    public void setActionLastState(String actionLastState) {
        this.actionLastState = actionLastState;
    }

    public String getActionIdUser() {
        return actionIdUser;
    }

    public void setActionIdUser(String actionIdUser) {
        this.actionIdUser = actionIdUser;
    }

    public String debugInfo() {
        return "UserWorksReportDto{" +
                "recordNumber=" + recordNumber +
                ", idUser=" + idUser +
                ", user='" + user + '\'' +
                ", role='" + role + '\'' +
                ", idEvent=" + idEvent +
                ", eventGuid='" + eventGuid + '\'' +
                ", objectGuid='" + objectGuid + '\'' +
                ", source='" + source + '\'' +
                ", type='" + type + '\'' +
                ", address='" + address + '\'' +
                ", message='" + message + '\'' +
                ", eventIdUser='" + eventIdUser + '\'' +
                ", level='" + level + '\'' +
                ", fromHlDate=" + fromHlDate +
                ", createDate=" + createDate +
                ", idAction=" + idAction +
                ", actionLastState='" + actionLastState + '\'' +
                ", actionIdUser='" + actionIdUser + '\'' +
                '}';
    }
}
