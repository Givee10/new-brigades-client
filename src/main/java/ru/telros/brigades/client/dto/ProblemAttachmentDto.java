package ru.telros.brigades.client.dto;

//для формы настроек "Сроки закрытия заявок в СОУ
/**
 * @see ru.telros.brigades.client.ui.setup.ReactionTimeGrid
 */
public class ProblemAttachmentDto extends AbstractEntity {
    private Long problemCode;
    private String attachmentType;
    private String require;

    public Long getProblemCode() {
        return problemCode;
    }

    public void setProblemCode(Long problemCode) {
        this.problemCode = problemCode;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    public String debugInfo() {
        return "ProblemAttachmentDto{" +
                "problemCode=" + problemCode +
                ", attachmentType='" + attachmentType + '\'' +
                ", require='" + require + '\'' +
                '}';
    }
}
