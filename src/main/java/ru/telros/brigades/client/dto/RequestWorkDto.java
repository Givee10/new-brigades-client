package ru.telros.brigades.client.dto;

public class RequestWorkDto extends AbstractEntity {
	private String description, info, guid, number, district, urgency, problem;
	private String owner, status, source, address, updatedAddress, type;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUpdatedAddress() {
		return updatedAddress;
	}

	public void setUpdatedAddress(String updatedAddress) {
		this.updatedAddress = updatedAddress;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("RequestWorkDto{");
		sb.append("id=").append(getId());
		sb.append(", description='").append(description).append('\'');
		sb.append(", info='").append(info).append('\'');
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", number='").append(number).append('\'');
		sb.append(", district='").append(district).append('\'');
		sb.append(", urgency='").append(urgency).append('\'');
		sb.append(", problem='").append(problem).append('\'');
		sb.append(", owner='").append(owner).append('\'');
		sb.append(", status='").append(status).append('\'');
		sb.append(", source='").append(source).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", updatedAddress='").append(updatedAddress).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return number;
	}
}
