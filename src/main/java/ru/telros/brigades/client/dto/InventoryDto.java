package ru.telros.brigades.client.dto;

public class InventoryDto extends AbstractEntity {
	private String guid;
	private String type;
	private String description;
	private String unit;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("InventoryDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", unit='").append(unit).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return type;
	}
}
