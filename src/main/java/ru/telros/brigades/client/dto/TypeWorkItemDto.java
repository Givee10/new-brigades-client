package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeWorkItemDto extends AbstractEntity {
	private String guid, code, description;
	private Double norm;
	@JsonProperty
	private Boolean isPhotoRequire, isCallRequire, isActRequire;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getNorm() {
		return norm;
	}

	public void setNorm(Double norm) {
		this.norm = norm;
	}

	@JsonIgnore
	public Boolean getPhotoRequire() {
		return isPhotoRequire;
	}

	public void setPhotoRequire(Boolean photoRequire) {
		isPhotoRequire = photoRequire;
	}

	@JsonIgnore
	public Boolean getCallRequire() {
		return isCallRequire;
	}

	public void setCallRequire(Boolean callRequire) {
		isCallRequire = callRequire;
	}

	@JsonIgnore
	public Boolean getActRequire() {
		return isActRequire;
	}

	public void setActRequire(Boolean actRequire) {
		isActRequire = actRequire;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("TypeWorkItemDto{");
		sb.append("id=").append(getId());
		sb.append(", guid='").append(guid).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", norm=").append(norm);
		sb.append(", isPhotoRequire=").append(isPhotoRequire);
		sb.append(", isCallRequire=").append(isCallRequire);
		sb.append(", isActRequire=").append(isActRequire);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return "TypeWorkItemDto{" + "\n" +
				"\tguid='" + guid + '\'' +"\n" +
				"\t, code='" + code + '\'' +"\n" +
				"\t, description='" + description + '\'' +"\n" +
				"\t, norm=" + norm +"\n" +
				"\t, isPhotoRequire=" + isPhotoRequire +"\n" +
				"\t, isCallRequire=" + isCallRequire +"\n" +
				"\t, isActRequire=" + isActRequire +"\n" +
				'}';
	}
}
