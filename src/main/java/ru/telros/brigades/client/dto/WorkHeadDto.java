package ru.telros.brigades.client.dto;

public class WorkHeadDto extends AbstractEntity {
	private String wipEntityId;
	private String inventoryItemId;
	private String scheduledStartDate;

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getInventoryItemId() {
		return inventoryItemId;
	}

	public void setInventoryItemId(String inventoryItemId) {
		this.inventoryItemId = inventoryItemId;
	}

	public String getScheduledStartDate() {
		return scheduledStartDate;
	}

	public void setScheduledStartDate(String scheduledStartDate) {
		this.scheduledStartDate = scheduledStartDate;
	}
}
