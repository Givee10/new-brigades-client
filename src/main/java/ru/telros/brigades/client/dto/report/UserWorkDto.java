package ru.telros.brigades.client.dto.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.telros.brigades.client.ui.utils.DatesUtil;

import java.time.ZonedDateTime;

public class UserWorkDto {
    //для мастера
    @JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
    public ZonedDateTime sendToBrigadeTime;
    @JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
    public ZonedDateTime confirmTime;
    @JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
    public ZonedDateTime arrivalTime;
    //для диспетчера
    @JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
    public ZonedDateTime arrivalToHLTime;
    @JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
    public ZonedDateTime confirmOfReqIncTime;
    @JsonFormat(pattern = DatesUtil.NORMAL_DATE_FORMAT, timezone = "UTC")
    public ZonedDateTime workByReqCreationTime;

    public ZonedDateTime getSendToBrigadeTime() {
        return sendToBrigadeTime;
    }

    public void setSendToBrigadeTime(ZonedDateTime sendToBrigadeTime) {
        this.sendToBrigadeTime = sendToBrigadeTime;
    }

    public ZonedDateTime getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(ZonedDateTime confirmTime) {
        this.confirmTime = confirmTime;
    }

    public ZonedDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(ZonedDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public ZonedDateTime getArrivalToHLTime() {
        return arrivalToHLTime;
    }

    public void setArrivalToHLTime(ZonedDateTime arrivalToHLTime) {
        this.arrivalToHLTime = arrivalToHLTime;
    }

    public ZonedDateTime getConfirmOfReqIncTime() {
        return confirmOfReqIncTime;
    }

    public void setConfirmOfReqIncTime(ZonedDateTime confirmOfReqIncTime) {
        this.confirmOfReqIncTime = confirmOfReqIncTime;
    }

    public ZonedDateTime getWorkByReqCreationTime() {
        return workByReqCreationTime;
    }

    public void setWorkByReqCreationTime(ZonedDateTime workByReqCreationTime) {
        this.workByReqCreationTime = workByReqCreationTime;
    }
}
