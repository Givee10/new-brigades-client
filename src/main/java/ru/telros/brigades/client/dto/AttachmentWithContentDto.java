package ru.telros.brigades.client.dto;

public class AttachmentWithContentDto {
	private byte[] content;

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
}
