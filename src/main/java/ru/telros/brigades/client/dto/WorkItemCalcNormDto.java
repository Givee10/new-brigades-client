package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkItemCalcNormDto {
	@JsonProperty("WIP_ENTITY_ID")
	private String wipEntityId;
	@JsonProperty("ACTION_SEQ_ID")
	private String actionSeqId;
	@JsonProperty("OPERATION_DESCRIPTION")
	private String operationDescription;
	@JsonProperty("EO_TYPE")
	private String eoType;
	@JsonProperty("EO_TYPE_DESCR")
	private String eoTypeDescription;
	@JsonProperty("METRICS_CODE")
	private String metricsCode;
	@JsonProperty("RESOURCE_ID")
	private String resourceId;
	@JsonProperty("RESOURCE_CODE")
	private String resourceCode;
	@JsonProperty("CALC_NORM")
	private String calcNorm;
	@JsonProperty("UOM")
	private String uom;

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getActionSeqId() {
		return actionSeqId;
	}

	public void setActionSeqId(String actionSeqId) {
		this.actionSeqId = actionSeqId;
	}

	public String getOperationDescription() {
		return operationDescription;
	}

	public void setOperationDescription(String operationDescription) {
		this.operationDescription = operationDescription;
	}

	public String getEoType() {
		return eoType;
	}

	public void setEoType(String eoType) {
		this.eoType = eoType;
	}

	public String getEoTypeDescription() {
		return eoTypeDescription;
	}

	public void setEoTypeDescription(String eoTypeDescription) {
		this.eoTypeDescription = eoTypeDescription;
	}

	public String getMetricsCode() {
		return metricsCode;
	}

	public void setMetricsCode(String metricsCode) {
		this.metricsCode = metricsCode;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	public String getCalcNorm() {
		return calcNorm;
	}

	public void setCalcNorm(String calcNorm) {
		this.calcNorm = calcNorm;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}
}
