package ru.telros.brigades.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PingDto {
	private String serverDate, serverTime, info;
	@JsonProperty
	private Boolean isAuthorized;

	public String getServerDate() {
		return serverDate;
	}

	public void setServerDate(String serverDate) {
		this.serverDate = serverDate;
	}

	public String getServerTime() {
		return serverTime;
	}

	public void setServerTime(String serverTime) {
		this.serverTime = serverTime;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@JsonIgnore
	public Boolean getAuthorized() {
		return isAuthorized;
	}

	public void setAuthorized(Boolean authorized) {
		isAuthorized = authorized;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("PingDto{");
		sb.append("serverDate='").append(serverDate).append('\'');
		sb.append(", serverTime='").append(serverTime).append('\'');
		sb.append(", info='").append(info).append('\'');
		sb.append(", isAuthorized=").append(isAuthorized);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return info;
	}
}
