package ru.telros.brigades.client.dto;

public class SlaUserActionDto extends AbstractEntity {
	private String name;
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("SlaUserActionDto{");
		sb.append("id=").append(getId());
		sb.append(", name='").append(name).append('\'');
		sb.append(", value='").append(value).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
