package ru.telros.brigades.client.dto;

public class OrganizationDto extends AbstractEntity {
	private String type, name, location, address, description, code;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("OrganizationDto{");
		sb.append("id=").append(getId());
		sb.append(", type='").append(type).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", location='").append(location).append('\'');
		sb.append(", address='").append(address).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", code='").append(code).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return name;
	}
}
