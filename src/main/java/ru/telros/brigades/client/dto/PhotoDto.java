package ru.telros.brigades.client.dto;

public class PhotoDto {
	private String login;

	private byte[] content;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return login;
	}
}
