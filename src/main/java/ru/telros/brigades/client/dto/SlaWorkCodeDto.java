package ru.telros.brigades.client.dto;

public class SlaWorkCodeDto extends AbstractEntity {
	private String idCodeHl;
	private String nameWork;

	public String getIdCodeHl() {
		return idCodeHl;
	}

	public void setIdCodeHl(String idCodeHl) {
		this.idCodeHl = idCodeHl;
	}

	public String getNameWork() {
		return nameWork;
	}

	public void setNameWork(String nameWork) {
		this.nameWork = nameWork;
	}

	public String debugInfo() {
		final StringBuffer sb = new StringBuffer("SlaWorkCodeDto{");
		sb.append("id=").append(getId());
		sb.append(", idCodeHl='").append(idCodeHl).append('\'');
		sb.append(", nameWork='").append(nameWork).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public String toString() {
		return nameWork;
	}
}
