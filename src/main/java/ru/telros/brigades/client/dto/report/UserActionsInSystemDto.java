package ru.telros.brigades.client.dto.report;

import ru.telros.brigades.client.dto.EventDto;

public class UserActionsInSystemDto {
    public String name;
    public EventDto event;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventDto getEvent() {
        return event;
    }

    public void setEvent(EventDto event) {
        this.event = event;
    }
}
