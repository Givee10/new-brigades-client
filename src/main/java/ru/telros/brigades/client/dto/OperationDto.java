package ru.telros.brigades.client.dto;

public class OperationDto extends AbstractEntity {
	private String wipEntityId;
	private String operationSequenceId;
	private String operationCode;
	private String operationName;
	private String calcResult;

	public String getWipEntityId() {
		return wipEntityId;
	}

	public void setWipEntityId(String wipEntityId) {
		this.wipEntityId = wipEntityId;
	}

	public String getOperationSequenceId() {
		return operationSequenceId;
	}

	public void setOperationSequenceId(String operationSequenceId) {
		this.operationSequenceId = operationSequenceId;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getCalcResult() {
		return calcResult;
	}

	public void setCalcResult(String calcResult) {
		this.calcResult = calcResult;
	}
}
