package ru.telros.brigades.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Messages manager for application (for read messages from messages_xx.properties)
 */
@Component
public class MessageManager {
	private static final Locale locale = Locale.forLanguageTag("ru");
	private static MessageSource messageSource;
	// TODO:LED: configured for use from within IDE, use
	// Locale.defaultLocate() instead for production env.

	@Autowired
	public MessageManager(MessageSource messageSource) {
		MessageManager.messageSource = messageSource;
	}

	public static String msg(String name) {
		return (messageSource != null) ? messageSource.getMessage(name, null, locale) : null;
	}

	public static String msg(String name, Object[] params) {
		return (messageSource != null) ? messageSource.getMessage(name, params, locale) : null;
	}

}
