package ru.telros.brigades.client;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class DateInterval implements Serializable {
	private ZonedDateTime start;
	private ZonedDateTime end;

	public DateInterval() {
	}

	public DateInterval(ZonedDateTime start, ZonedDateTime end) {
		this.start = start;
		this.end = end;

		if (end.isBefore(start)) {
			throw new IllegalArgumentException("Illegal date interval :: the end can not precede the start");
		}
	}

	public void set(ZonedDateTime start, ZonedDateTime end) {
		this.start = start;
		this.end = end;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DateInterval)) return false;

		DateInterval that = (DateInterval) o;

		if (!start.equals(that.start)) return false;
		return end.equals(that.end);
	}

	@Override
	public int hashCode() {
		int result = start.hashCode();
		result = 31 * result + end.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "DateInterval{" +
				"start=" + start +
				", end=" + end +
				'}';
	}

}