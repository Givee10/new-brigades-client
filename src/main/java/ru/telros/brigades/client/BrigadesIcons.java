package ru.telros.brigades.client;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.FontIcon;

public class BrigadesIcons implements FontIcon {
	private int codePoint;
	private String fontFamily, name, color;

	public BrigadesIcons(FontAwesome icon, String color) {
		this.codePoint = icon.getCodepoint();
		this.fontFamily = icon.getFontFamily();
		this.name = icon.name();
		this.color = color;
	}

	public BrigadesIcons(VaadinIcons icon, String color) {
		this.codePoint = icon.getCodepoint();
		this.fontFamily = icon.getFontFamily();
		this.name = icon.name();
		this.color = color;
	}

	@Override
	public String getFontFamily() {
		return fontFamily;
	}

	@Override
	public int getCodepoint() {
		return codePoint;
	}

	@Override
	public String getHtml() {
		String c = "v-icon v-icon-" + this.name.toLowerCase();
		String s = "font-family: " + this.fontFamily + "; ";
		String h = this.color == null ? "color: inherit;" : "color: " + this.color + ";";
		return "<span class=\"" + c + "\" style=\"" + s + h + "\">&#x" + Integer.toHexString(this.codePoint) + ";</span>";
	}

	@Override
	public String getMIMEType() {
		throw new UnsupportedOperationException(FontIcon.class.getSimpleName() + " should not be used where a MIME type is needed.");
	}
}
